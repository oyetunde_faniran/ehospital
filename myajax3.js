/** When = 1, then the new hospital number would be used to search for a patient, else the old hospital number is used */
selected_type = 1;

function setNumberType(t){
    selected_type = t;
    search_text = document.getElementById('search-box').value;
    if (search_text != ''){
        showMesg(search_text);
    }
}


// JavaScript Document
function showMesg(str){
    //document.getElementById("txtHint").innerHTML=str
    //return
    if (str.length==0){
        document.getElementById("message").innerHTML="Empty";
        return;
    }
    x=GetAjax();
    if (x==null){
        //alert('null')
        document.GetElementById("message").innerHTML="No Ajax Support";
        return;
    }
    url="myajaxserver3.php" + "?q=" + str + "&t=" + selected_type;
//    alert (url);
    x.onreadystatechange = stateChanged;
    x.open("GET",url,true);
    x.send(null);
}
function stateChanged(){
 if (x.readyState==4 || x.readyState=='complete'){
  //alert(document.getElementById("message").innerHTML)
  document.getElementById("message").innerHTML=x.responseText
 }else{
  //document.getElementById("message").innerHTML="<font color='red'>Getting details...</font>"
 // alert(document.getElementById("message").innerHTML)
 }
}
function GetAjax(){
 var x=null;
 try {
  x=new XMLHttpRequest();
 }catch (e) {
  try {
   x=new ActiveXObject("Msxml2.XMLHTTP");
  }catch(e){
   x=new ActiveXObject("Microsoft.XMLHTTP");
  }
 }
 return x;
}
