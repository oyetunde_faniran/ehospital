<?php
	function __autoload($class_name) {
        global $root_dir;
		$classFile = $root_dir . "./classes/" . $class_name . ".php";
		if (file_exists($classFile)){
			include_once ($classFile);
		}
	}
?>