<?php
function mres($value){
    $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
    $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
    return str_replace($search, $replace, $value);
}
function get_id($table,$pk="id"){
    $conn = new db_conn();
    $result = $conn->query("select if((select min($pk) from $table) > 1, 1, u.$pk + 1) as aid from $table u left join $table u1 on u1.$pk =u.$pk+1 where u1.$pk is null order by u.$pk limit 0,1");
    if($result){
        $id = 1;
        if($result->num_rows > 0){
            $row = $result->fetch_assoc();
            $id = $row['aid'];
        }
        return $id;
    }
}
function feedback($text,$res=true){return include("function_lib/"."feedback.php");}
function queryfn($fn,$query,$fail,$key){
    $fnDir = "function_lib/";
	$conn = new db_conn();
	$result = $conn->query($query);
	$conn->close();
	if($result) return include($fnDir.$fn.".php");
	else return $fail;
}
function alert($text){
    echo'
		<script>
		alert("'.$text.'");
		</script>
		';
}

/**
 * @param $query
 * @return mixed
 */
function array_from_db($query){return queryfn("array_from_db",$query,$fail=false,$key=false);}
/**
 * @param $src tableName name
 * @param $src_id field primary key
 * @param $initiator Process(script name)
 * @param $status_code Code
 * @param $status_description Message
 * @return boolean
 */
function logger($src,$src_id,$initiator,$status_code,$status_description){
    $log_table = "payment_logs";
    //echo("insert into $log_table values (".get_id($log_table).",'$src',$src_id,'$initiator','$status_code','$status_description',NOW())");
    return insert("insert into $log_table values (".get_id($log_table).",'$src',$src_id,'$initiator','$status_code','$status_description',NOW())");
}

/**
 * @param $query
 * @return mixed
 */
function array_from_db_var($query){return queryfn("array_from_db_var",$query,$fail=false,$key=false);}
function array_from_db_check($query){return queryfn("query",$query,$fail=false,$key=false);}
function query($query,$fail=false){return queryfn("query",$query,$fail,$key=false);}
function simple_query($query,$fail=false){return queryfn("simple_query",$query,$fail,$key=false);}
function multi_query($query,$fail=false){return queryfn("multi_query",$query,$fail,$key=false);}
function multi_query2($query,$fail=false){return queryfn("multi_query2",$query,$fail,$key=false);}
function simple_row_query($query,$fail=false){return queryfn("simple_row_query",$query,$fail,$key=false);}
function multi_row_query($query,$fail=false){return queryfn("multi_row_query",$query,$fail,$key=false);}
function multi_row_query2($query,$fail=false){return queryfn("multi_row_query2",$query,$fail,$key=false);}
function multi_row_query3($query,$fail=false){return queryfn("multi_row_query3",$query,$fail,$key=false);}
function multi_row_query_key($query,$key='id',$fail=false){return queryfn("multi_row_query_key",$query,$fail,$key);}
function multi_row_query_keyg($query,$key='id',$fail=false){return queryfn("multi_row_query_keyg",$query,$fail,$key);}
function multi_row_query_keyag($query,$key='id',$fail=false){return queryfn("multi_row_query_keyag",$query,$fail,$key);}
function multi_row_query_keya($query,$key='id',$fail=false){return queryfn("multi_row_query_keya",$query,$fail,$key);}
function multi_row_query_keya2($query,$key='id',$fail=false){return queryfn("multi_row_query_keya2",$query,$fail,$key);}
function insert($query){return queryfn("insert",$query,$fail=false,$key=false);}
function update($query){return queryfn("update",$query,$fail=false,$key=false);}
function dd($v){var_dump($v);die('');}
function str_date($time = false,$pad=false){
    if(!$time)$time = time() + time_offset();
    else $time = strtotime($time);
    if($pad)$pad = ". g:i A";
    else $pad="";
    return date('l, F jS, Y'.$pad,$time );
}

function notify($msg){
    return "<embed style='visibility: hidden;height:0;width: 0' src='swf/peace.swf'></embed> <div class='alert alert-warning'><i class='glyphicon glyphicon-plus'></i> $msg</div>";
}


function options_sql($query,$indexes,$selected=null,$compared_index=0){
    $conn = new db_conn();
    $index = explode(",",$indexes);
    $result = $conn->query($query);
    $var = "";
    if($result and $result->num_rows > 0){
        while($row = $result->fetch_assoc()){
            $select = "";
            if(strtolower($selected) == strtolower($row[$index[$compared_index]])){
                $select = "selected";
            }
            $var = $var."<option $select value='".$row["$index[0]"]."'>";
            if(count($index) > 1){
                $i = 1;
                while($i < count($index)){
                    if($i > 1){
                        $var = $var." ";
                    }
                    $var = $var.trim($row["$index[$i]"]);
                    $i++;
                }
            }
            else{
                $var = $var.trim($row["$index[0]"]);
            }
            $var = $var."</option>";
        }
        return $var;
    }
}
function options_arr($arr,$indexes,$selected=null,$compared_index=0){
    $index = explode(",",$indexes);
    $v = "";
    if(!empty($arr)){
        foreach($arr as $row){
            $select = "";
            if(strtolower($selected) == strtolower($row[$index[$compared_index]])){
                $select = "selected";
            }
            $v .= "<option $select value='".$row["$index[0]"]."'>";
            if(count($index) > 1){
                $i = 1;
                while($i < count($index)){
                    $v .= $row["$index[$i]"]." ";
                    $i++;
                }
            }
            else{
                $v .= $row["$index[0]"];
            }
            $v .= "</option>";
        }
    }
    return $v;
}

?>