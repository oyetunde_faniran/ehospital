<?php
    die ("Road Closed");
    require_once ('./includes/config.inc.php');
    $skye = array ('appointment',
                    'audittrail',
                    'audittrail_menu',
                    'casenote',
                    'casenote_files',
                    'casenote_history',
                    'casenote_labtest',
                    'casenote_labtestrequest',
                    'casenote_nursereport',
                    'casenote_prescription',
                    'casenote_prescriptionlog',
                    'casenote_treatment',
                    'drug',
                    'inventory_batch',
                    'inventory_batch_details',
                    //'inventory_categories',
                    'inventory_dispense_item',
                    'inventory_dispense_item_details',
                    'inventory_forms',
                    'inventory_items',
                    'inventory_items_reorder_level',
                    'inventory_item_categories',
                    'inventory_item_extras',
                    'inventory_item_prices',
                    'inventory_locations',
                    'inventory_locations_users',
                    'inventory_manufacturers',
                    //'inventory_medical_names',
                    'inventory_presentations',
                    'inventory_received_items',
                    'inventory_stock_received_details',
                    'inventory_transfers',
                    'inventory_transfer_details',
                    'inventory_units',
                    'inventory_unit_items',
                    'inventory_vendors',
                    'patient_admission',
                    'patient_vitalsigns',
                    'pat_conditiontrack',
                    'registry',
                    'registry_extra',
                    'registry_nhis',
                    'registry_retainership',
                    'pat_transitem',
                    'pat_transtotal',
                    'mediluth_branchcollect.transaction',
                    'mediluth_chat.chat',
                    'mediluth_lab.lab_registry',
                    'mediluth_lab.lab_results',
                    'mediluth_lab.lab_trans',
                    'mediluth_lab.lab_trans_specimen'
            );
    $conn = new DBConf();
    foreach ($skye as $table){
        $query = "TRUNCATE TABLE $table";
        $result = $conn->run($query);
        echo "<p><strong>'$table'</strong> was emptied successfully.</p>";
    }
?>
