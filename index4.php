<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LUTH</title>
<link href="menu/support.css" rel="stylesheet" type="text/css">
<link href="menu/main.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery/jquery.js"></script>
<script type="text/javascript" src="js/jquery/jquery.dropdown.js"></script>
<link href="css/luth.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="header" class="width"></div>
<div id="nav-bar" class="width">
<ul class="dropdown dropdown-horizontal" name="nav">
      <li><a href="#">Home</a></li>
<li><span class="dir">Modules</span>
          <ul>
            <li class="first"><a href="#" class="dir">System Administration</a>
                <ul>
                  <li><a href="#">Manage Drug Category</a></li>
				  				<li><a href="#">Manage Drug Sub Category</a></li>
								<li><a href="#">Manage Drugs</a></li>
								<li><a href="#">Manage Service Items</a></li>
								<li><a href="#">Manage Service Fees</a></li>
								<li><a href="#">Manage Drug Priority</a></li>
								<li><a href="#">Manage wards</a></li>
		        </ul>
            </li>
            <li class="first"><a href="#" class="dir">Patient Care</a>
                <ul>
                  <li><a href="#">Patient Registration</a></li>
                  <li><a href="#">Treatment</a></li>
                  <li><a href="#">View Appointments</a></li>
				  <li><a href="#">Patient Admission</a></li>
                </ul>
            </li>
            <li><a href="index.php?p=reports&m=reports" class="dir">Reports</a>
                <ul>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Billing</a>
                <ul>
                  <li><a href="index.php?p=billing">Billing</a></li>
                </ul>
            </li>
             <li><a href="#" class="dir">Pharmacy</a>
                <ul>
                  <li><a href="index.php?p=pharmacybill">Drug Dispensing</a></li>
                </ul>
            </li>
		<li><a href="#" class="dir">Laboratory</a>
                <ul>
                   <li><a href="index.php?p=labtestbill">Laboratory Services</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Account/Audit/Finance</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Human Resources</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Tender</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Message Centre</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Fixed Asset Manager</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Document Manager</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
        </ul>
      </li>
      <li ><a href="#" class="dir">Support</a>
          <ul>
            <li><a href="#">Link 1</a></li>
          </ul>
      </li>
      <li ><a href="#" class="dir">Alerts</a>
          <ul>
            <li><a href="#">News 1</a></li>
          </ul>
      </li>
      </li>
  </ul>

</div>
<div class="content width">
  <div class="content-column"> <span class="contentColumnSpan">Drug Dispensary</span>
    <div class="splash-box">
      <p>Welcome, You have 15 new entries for Drug recomendations:</p>
      <form action="" method="get" class="form2 dash-board-form-position">
        <input type="text" name="search-box" id="search-box" />
        <input name="search-btn" type="button" id="search-btn" />
      </form>
 <table width="0" border="0" cellspacing="0" cellpadding="0" class="display-set">
   <tr class="title-row">
    <td width="37%" valign="top">Patient Name</td>
    <td width="14%" valign="top">Volume</td>
    <td width="16%" valign="top">Drug type</td>
    <td width="33%" valign="top">Drug Name</td>
  </tr>
  <tr class="tr-row">
    <td valign="top"><a href="#">Todd Turner</a></td>
    <td valign="top">33</td>
    <td valign="top">Syrup</td>
    <td valign="top">Vitamin C</td>
  </tr>
  <tr class="tr-row2">
    <td valign="top">Michael Johnson</td>
    <td valign="top">23</td>
    <td valign="top">Syrup</td>
    <td valign="top">Vitamin X</td>
  </tr>
  <tr class="tr-row">
    <td valign="top">Kelvin Maxwell</td>
    <td valign="top">3</td>
    <td valign="top">Syrup</td>
    <td valign="top">Vitamin Y</td>
  </tr>
  <tr class="tr-row2">
    <td valign="top">John Konolsky</td>
    <td valign="top">56</td>
    <td valign="top">tablets</td>
    <td valign="top">Vitamin C</td>
  </tr>
   <tr class="tr-row">
    <td valign="top">Cole Smith</td>
    <td valign="top">3</td>
    <td valign="top">Syrup</td>
    <td valign="top">Cloroquine</td>
  </tr>
  <tr class="tr-row2">
    <td valign="top">Murine Tessy</td>
    <td valign="top">34</td>
    <td valign="top">injection</td>
    <td valign="top">Anti AIDs Vaccine</td>
  </tr>
  <tr class="tr-row">
    <td valign="top">Kelvin Maxwell</td>
    <td valign="top">3</td>
    <td valign="top">injection</td>
    <td valign="top">Anti River blindness parasite</td>
  </tr>
  <tr class="tr-row2">
    <td valign="top">John Konolsky</td>
    <td valign="top">56</td>
    <td valign="top">syrup</td>
    <td valign="top">Vitamin M</td>
  </tr>
   <tr class="tr-row">
    <td valign="top">Cole Smith</td>
    <td valign="top">3</td>
    <td valign="top">injection</td>
    <td valign="top">Vitamin E</td>
  </tr>
  <tr class="tr-row2">
    <td valign="top">Murine Tessy</td>
    <td valign="top">34</td>
    <td valign="top">injection</td>
    <td valign="top">Viatamin  C++</td>
  </tr>
  </table>     


    </div>
  </div>
  <div class="side-bar">
    <h3>Related Links</h3>
    <a href="#">Medical Records</a> <a href="#">More Records</a> <a href="#">And More Records</a></div>
</div>
<div class="footer width">Lagos University Teaching Hospital Credit: Upperlink Limited &copy; 2010</div>
</body>
</html>
