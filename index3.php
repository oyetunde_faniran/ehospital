<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LUTH</title>
<link href="menu/support.css" rel="stylesheet" type="text/css">
<link href="menu/main.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery/jquery.js"></script>
<script type="text/javascript" src="js/jquery/jquery.dropdown.js"></script>
<link href="css/luth.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="header" class="width"></div>
<div id="nav-bar" class="width">
<ul class="dropdown dropdown-horizontal" name="nav">
      <li><a href="#">Home</a></li>
<li><span class="dir">Modules</span>
          <ul>
            <li class="first"><a href="#" class="dir">System Administration</a>
                <ul>
                  <li><a href="#">Manage Drug Category</a></li>
				  				<li><a href="#">Manage Drug Sub Category</a></li>
								<li><a href="#">Manage Drugs</a></li>
								<li><a href="#">Manage Service Items</a></li>
								<li><a href="#">Manage Service Fees</a></li>
								<li><a href="#">Manage Drug Priority</a></li>
								<li><a href="#">Manage wards</a></li>
		        </ul>
            </li>
            <li class="first"><a href="#" class="dir">Patient Care</a>
                <ul>
                  <li><a href="#">Patient Registration</a></li>
                  <li><a href="#">Treatment</a></li>
                  <li><a href="#">View Appointments</a></li>
				  <li><a href="#">Patient Admission</a></li>
                </ul>
            </li>
            <li><a href="index.php?p=reports&m=reports" class="dir">Reports</a>
                <ul>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Billing</a>
                <ul>
                  <li><a href="index.php?p=billing">Billing</a></li>
                </ul>
            </li>
             <li><a href="#" class="dir">Pharmacy</a>
                <ul>
                  <li><a href="index.php?p=pharmacybill">Drug Dispensing</a></li>
                </ul>
            </li>
		<li><a href="#" class="dir">Laboratory</a>
                <ul>
                   <li><a href="index.php?p=labtestbill">Laboratory Services</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Account/Audit/Finance</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Human Resources</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Tender</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Message Centre</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Fixed Asset Manager</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Document Manager</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
        </ul>
      </li>
      <li ><a href="#" class="dir">Support</a>
          <ul>
            <li><a href="#">Link 1</a></li>
          </ul>
      </li>
      <li ><a href="#" class="dir">Alerts</a>
          <ul>
            <li><a href="#">News 1</a></li>
          </ul>
      </li>
      </li>
  </ul>

</div>
<div class="content width">
  <div class="content-column"> <span class="contentColumnSpan">Welcome</span>
    <div class="splash-box">
      <p>Please Enter your Username and Password below</p>
      <form action="" method="get" class="form">
        <label>Username</label>
        <input name="username" type="text" />
        <br  />
        <label>Password</label>
        <input name="password" type="password" />
        <br  />
        <label>Family Name</label>
        <input name="username" type="text" />
        <br  />
        <label>History</label>
        <input name="username" type="text" />
        <br  />
        <p>
          <label>Nationality</label>
          <input name="nigeria" type="radio" value="" />
          <i>Nigerian</i>
          <input name="" type="radio" value="" />
          <i>Foreigner</i></p>
        <label>Home Address</label>
        <textarea name="username"></textarea>
        <br  />
        <label>Cool Friend</label>
        <input name="username" type="text" />
        <br  />
        <p>
          <label>Tribe</label>
          <select name="tribe">
            <option>-  Select Your Tribe -</option>
            <option>Yoruba</option>
            <option>Igbo</option>
            <option>Hausa</option>
            <option>Fulani</option>
            <option>Tiv</option>
            <option>Ijaw</option>
            <option>And So Much More</option>
          </select>
          <br  />
        </p>
        <p>
          <label>Religion</label>
          <select name="tribe2">
            <option>-  Select Your Faith -</option>
            <option>Christian</option>
            <option>Muslim</option>
            <option>Buda</option>
            <option>And So Much More</option>
          </select>
          <br  />
        </p>
        <label>Phone</label>
        <input name="username" type="text" />
        <br  />
        <div class="welcome-btn-position"><input name="submitt" type="button" value="Sign Up" class="btn" /></div>
      </form>
    </div>
  </div>
  <div class="side-bar">
    <h3>Related Links</h3>
    <a href="#">Medical Records</a> <a href="#">More Records</a> <a href="#">And More Records</a></div>
</div>
<div class="footer width">Lagos University Teaching Hospital Credit: Upperlink Limited &copy; 2010</div>
</body>
</html>
