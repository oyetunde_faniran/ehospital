<?php 

/* 
 *	This is the default language setting for the application.
 *	This page is included by includes/config.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')){

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit();
	
} // End of defined() IF.

$appTitle = "LAGOS UNIVERSITY TEACHING HOSPITAL";

$curLangField = "lang1";
$sex_male = "Male";
$sex_female = "Female";





/*//image error start here
//These are the error outputs of the uploadImage class
$ERR["UNABLE_TO_OUTPUT"]		= "Unable to output: ";
$ERR["FILE_DOESNOT_EXSIT"]		= "This file does not exist: ";
$ERR["FUNCTION_DOESNOT_EXIST"]	= "This function does not exist: ";
$ERR["GD2_NOT_CREATED"]			=	"GD2 is installed, function ImageCreateTruecolor() exists, but image is not created";
$ERR["IMG_NOT_CREATED"]			=	"Image is not created ImageCreate(). {GD2 suppor is OFF}";
$ERR["GD2_UNAVALABLE"]			=	"You specified to use GD2, but not all GD2 functions are present.";
$ERR["GD2_NOT_RESIZED"]			=	"GD2 is installed, function ImageCopyResampled() exists, but image is not resized";
$ERR["IMG_NOT_RESIZED"]			=	"Image was not resized. {GD2 suppor is OFF}";
$ERR["UNKNOWN_OUTPUT_FORMAT"]	=	"This image format cannot bu output: ";
$ERR["NO_IMAGE_FOR_OUTPUT"]		=	"Image you are trying to output does not exist. ";
$ERR["IMG_NOT_SUPPORTED"]		=	"Can not create image. Sorry, this image type is not supported yet.";
//image errors end here*/

//*** BEGIN REPORT STUFFS
	$report_male = "Male";
	$report_female = "Female";
	$report_show = "Show";
	$report_total = "Total";
	$report_gTotal = "Grand Total";
	$report_adm = "Admissions";
	$report_discharge = "Discharges";
	$report_death = "Deaths";
	$report_sno = "S/No";
	$report_dept = "Department";
	$report_fromdate = "FROM DATE";
	$report_todate = "TO DATE";
	$report_date = "DATE";
	$report_showReport = "Show Report";
	$report_selectDate = "Please, click the icon beside the date field below and choose any day in a month / year for which reports would be generated.";
	$report_unknownReport = "UNKNOWN REPORT!!!. Please, close this window, then click 'Print View' from the side bar of any report."; 
	$report_failedExport = "An error occurred while trying to complete the export. Please, contact the support group for this application.";
	$report_exportNotAvailable = "The feature you selected is not available for the current report.";
	$report_exportNotAvailable_javascript = "It seems the export format you selected is not available for this report.";
	$report_unknownTitle = "Unknown Title";
	$report_noData = "No data was found for the selected period.";

	//BEGIN stuffs for reports.php
		$report_general_header = "ALL REPORTS";
		$report_general_story = "Please, click any of the links below to view the associated report.";
		$report_general_pagetitle = "All Reports Page | Select Report";
	
		//Links
		$report_evening_report = "Evening Report";
		$report_morning_report = "Morning Report";
		$report_adm_book = "Admission Book For The Wards";
		$report_account_book = "Accounts Book For The Wards";
		$report_bedstatewards = "Bed State (Summary for all wards)";
		$report_bedstate = "Bed State (For Each Ward, With Patient Details)";
		$report_dailybed_statements = "Daily Bed Statements (Summary)";
		$report_appointment_listing = "Patient Appointment Listing";
		$report_accident = "Accident and Emergency Department (Summary of Cases)";
		$report_patient_summary_wards = "Patient Summary Per Ward";
		$report_monthly_attendances_outpatient = "Attendances at Out-Patient Clinic (Logistics Form for Doctor's Consultation)";
		$report_monthlystats = "Monthly Statistical Reports";
	//END stuffs for reports.php

	//BEGIN stuffs for eveningreport.php
		$report_evening_header = "EVENING REPORT";
		$report_evening_story1 = "Please, select the nurse and date for which evening reports will be viewed.";
		$report_evening_title = "Evening Report for ";
		$report_evening_unknown = "[Unknown Nurse]";
		$report_evening_allnurses = "All Nurses";
		$report_evening_daterange = "DATE RANGE";
		$report_evening_alltime = "All Time";
		$report_evening_searchbynumber = "Search by Staff Number";
		$report_evening_searchbyname = "Search by Name";
		$report_evening_form_nursefield = "NURSE";
		$report_evening_form_fromdatefield = "FROM (DATE / TIME)";
		$report_evening_form_todatefield = "TO (DATE / TIME)";
		$report_evening_error_noresult = "Zero (0) result(s) matching your search criteria was found";
		$report_evening_tableheader_name = "NAME";
		$report_evening_tableheader_age = "AGE";
		$report_evening_tableheader_bp = "BLOOD PRESSURE";
		$report_evening_tableheader_temp = "TEMPERATURE";
		$report_evening_tableheader_pulse = "PULSE";
		$report_evening_tableheader_resp = "RESPIRATION";
		$report_evening_tableheader_cond = "CONDITION";
		$report_evening_tableheader_diag = "DIAGNOSIS";
		$report_evening_sno = "S/NO";
	//END stuffs for eveningreport.php

	//BEGIN stuffs for morningreport.php
		$report_morning_header = "MORNING REPORT";
		$report_morning_story1 = "Please, select the nurse and date for which morning reports will be viewed.";
		$report_morning_title = "Morning Report for ";
		$report_morning_unknown = "<Unknown Nurse>";
		$report_morning_allnurses = "All Nurses";
		$report_morning_form_nursefield = "NURSE";
		$report_morning_form_fromdatefield = "FROM (DATE / TIME)";
		$report_morning_form_todatefield = "TO (DATE / TIME)";
		$report_morning_error_noresult = "Zero (0) result(s) matching your search criteria was found";
	//END stuffs for morningreport.php

	//BEGIN stuffs for admissionbook.php
		$report_admissionbook_header = "ADMISSION BOOK FOR THE WARDS";
		$report_admissionbook_form_fromdatefield = "FROM (DATE)";
		$report_admissionbook_form_todatefield = "TO (DATE)";
		$report_admissionbook_sno = "S/NO";
		$report_admissionbook_admdate = "DATE";
		$report_admissionbook_no = "HOSPITAL NO.";
		$report_admissionbook_sname = "SURNAME";
		$report_admissionbook_oname = "OTHER NAMES";
		$report_admissionbook_addy = "ADDRESS";
		$report_admissionbook_sex = "SEX";
		$report_admissionbook_age = "AGE";
		$report_admissionbook_cstate = "CIVIL STATE";
		$report_admissionbook_ward = "WARD";
		$report_admissionbook_consultant = "CONSULTANT";
		$report_admissionbook_ddate = "DISCHARGE DATE";
		$report_admissionbook_diagnosis = "DIAGNOSIS";
		$report_admissionbook_daterange = "DATE RANGE: ";
		$report_admissionbook_daterange_alltime = "DATE RANGE: ALL TIME";
		$report_admissionbook_error_noresult = "Zero (0) result(s) matching your search criteria was found";
	//END stuffs for admissionbook.php

	//BEGIN stuffs for accountbook.php
		$report_accountbook_header = "ACCOUNTS BOOK FOR THE WARDS";
	//END stuffs for accountbook.php

	//BEGIN stuffs for bedstatewards.php
		$report_bedstatewards_header = "BED STATE (Summary for all wards)";
		$report_bedstatewards_story1 = "Please, enter / choose the date for which bed statements are needed.";
		$report_bedstatewards_dateformat = "(FORMAT: YYYY-MM-DD)";
		$report_bedstatewards_insertdate = "Insert Date";
		$report_bedstatewards_story2 = "BED STATE FOR ALL WARDS AS AT: ";
		$report_bedstatewards_tableheader_sno = "S/NO";
		$report_bedstatewards_tableheader_spec = "SPECIALITY";
		$report_bedstatewards_tableheader_wards = "WARDS";
		$report_bedstatewards_tableheader_inpat = "IN-PATIENT";
		$report_bedstatewards_tableheader_vac = "VACANT BEDS";
		$report_bedstatewards_adm = "ON ADMISSION";
		$report_bedstatewards_discharged = "DISCHARGED";
		$report_bedstatewards_deaths = "DEATHS";
		$report_bedstatewards_sideroom = "SIDE ROOMS";
		$report_bedstatewards_paying = "PAYING";
		$report_bedstatewards_npaying = "NON-PAYING";
	//END stuffs for bedstatewards.php

	//BEGIN stuffs for bedstateward.php
		$report_bedstateward_header = "BED STATE (For each ward, with patient details)";
		$report_bedstateward_ward = "Ward";
		$report_bedstateward_story1 = "Please, enter / choose the date for which bed statements are needed.";
		$report_bedstateward_story2 = "BED STATE AS AT MIDNIGHT ON: <strong>%s</strong> FOR WARD: <strong>%s</strong>";
		$report_bedstateward_error_noresult = "No Bed Statement for ";
		$report_bedstateward_dateformat = "(FORMAT: YYYY-MM-DD)";
		$report_bedstateward_insertdate = "Insert Date";
		$report_bedstateward_error_noadmission = "Nobody was admitted in the selected ward on the chosen date";
		$report_bedstateward_error_nodischarges = "Nobody was discharged in the selected ward on the chosen date";
		$report_bedstateward_error_nodeath = "Nobody died in the selected ward on the chosen date";
		$report_bedstateward_sno = "S/NO";
		$report_bedstateward_no = "Hospital Number";
		$report_bedstateward_sname = "Surname";
		$report_bedstateward_onames = "Other Names";
		$report_bedstateward_sex = "Sex";
		$report_bedstateward_age = "Age";
		$report_bedstateward_cons = "Consultant";
		$report_bedstateward_diagnosis = "Diagnosis";
		$report_bedstateward_adm = "ADMISSIONS";
		$report_bedstateward_discharges = "DISCHARGES";
		$report_bedstateward_deaths = "DEATHS";
		
	//END stuffs for bedstateward.php

	//BEGIN stuffs for dailybed.php
		$report_dailybed_header = "SUMMARY OF DAILY BED STATEMENTS FOR ALL WARDS";
		$report_dailybed_story1 = "Please, choose the date for which bed statements are needed.";
		$report_dailybed_story2 = "SUMMARY OF DAILY BED STATEMENTS AS AT: ";
		$report_dailybed_dateformat = "(FORMAT: YYYY-MM-DD)";
		$report_dailybed_insertdate = "Insert Date";
		$report_dailybed_datefield = "DATE";
		$report_dailybed_error_noresult = "No Bed Statement for ";
		$report_dailybed_tableheader_spec = "SPECIALITY";
		$report_dailybed_tableheader_wards = "WARDS";
		$report_dailybed_tableheader_alloc = "ALLOCATED BEDS";
		$report_dailybed_tableheader_occ = "OCCUPIED BEDS";
		$report_dailybed_tableheader_vac = "VACANT BEDS";
		$report_dailybed_tableheader_sno = "S/NO";
	//END stuffs for dailybed.php

	//BEGIN stuffs for appointment.php
		$report_appointment_header = "PATIENT APPOINTMENT LISTING";
		$report_appointment_story1 = "Please, select the department and date interval for which appointment listing will be viewed.";
		$report_appointment_title = "Patient Appointment Listing for ";
		$report_appointment_unknown = "<Unknown Doctor>";
		$report_appointment_allnurses = "All Doctors";
		$report_appointment_form_deptfield = "DEPARTMENT";
		$report_appointment_form_doctorfield = "DOCTOR";
		$report_appointment_form_fromdatefield = "FROM (DATE / TIME)";
		$report_appointment_form_todatefield = "TO (DATE / TIME)";
		$report_appointment_error_noresult = "Zero (0) result(s) matching your search criteria was found";
		$report_appointment_sno = "S/NO";
		$report_appointment_date = "DATE";
		$report_appointment_patientcode = "PATIENT CODE";
		$report_appointment_patientname = "PATIENT NAME";
		$report_appointment_sex = "SEX";
		$report_appointment_age = "AGE";
		$report_appointment_transcode = "TRANS. CODE";
		$report_appointment_apptype = "APPOINTMENT TYPE";
		$report_appointment_starttime = "START TIME";
		$report_appointment_endtime = "END TIME";
		$report_appointment_dept = "DEPARTMENT";
		$report_appointment_error_noresult = "Zero (0) result(s) matching your search criteria was found";
		$report_appointment_error_nodoctor = "No consultant was found in the appointment records";
		$report_appointment_error_unknown_doctor = "Unknown doctor";
	//END stuffs for appointment.php

	//BEGIN stuffs for accident.php
		$report_accident_header = "ACCIDENT AND EMERGENCY DEPARTMENT (Patient Details)";
		$report_accident_story1 = "Please, select the date interval and the patient whose details will be viewed.";
		$report_accident_form_fromdatefield = "FROM (DATE / TIME)";
		$report_accident_form_todatefield = "TO (DATE / TIME)";
		$report_accident_form_patient = "PATIENT";
		$report_accident_tableheader_date = "DATE";
		$report_accident_tableheader_deets = "DETAILS OF CASE";
		$report_accident_tableheader_treatment = "TREATMENT";
		$report_accident_tableheader_surname = "SURNAME:";
		$report_accident_tableheader_othernames = "OTHER NAMES:";
		$report_accident_tableheader_age = "AGE:";
		$report_accident_tableheader_sex = "SEX:";
		$report_accident_searchbynumber = "Search by Hospital Number";
		$report_accident_searchbyname = "Search by Name";
		$report_accident_tableheader_address = "ADDRESS:";
		$report_accident_error_noresult1 = "Zero (0) result(s) matching your search criteria was found";
		$report_accident_error_noresult2 = "Zero (0) result(s) matching the dates you chose for the selected patient was found";
	//END stuffs for accident.php

	//BEGIN stuffs for patientsummaryward.php
		$report_patientsummaryward_header = "PATIENT SUMMARY PER WARD";
	//END stuffs for patientsummaryward.php

	//BEGIN stuffs for outpatmonthlyatt.php
		$report_outpatmonthlyatt_header = "ATTENDANCES AT OUT-PATIENT CLINICS (Logistics Form for Doctor's Consultation)";
		$report_outpatmonthlyatt_story1 = "Please, select the month and year for which attendances will be viewed.";
		$report_outpatmonthlyatt_form_month = "MONTH";
		$report_outpatmonthlyatt_form_year = "YEAR";
		$report_outpatmonthlyatt_noresult = "Zero (0) result(s) matching your search criteria was found";
		$report_outpatmonthlyatt_jan = "January";
		$report_outpatmonthlyatt_feb = "February";
		$report_outpatmonthlyatt_mar = "March";
		$report_outpatmonthlyatt_apr = "April";
		$report_outpatmonthlyatt_may = "May";
		$report_outpatmonthlyatt_jun = "June";
		$report_outpatmonthlyatt_jul = "July";
		$report_outpatmonthlyatt_aug = "August";
		$report_outpatmonthlyatt_sep = "September";
		$report_outpatmonthlyatt_oct = "October";
		$report_outpatmonthlyatt_nov = "November";
		$report_outpatmonthlyatt_dec = "December";
		$report_outpatmonthlyatt_unknown = "Unknown";
	//END stuffs for outpatmonthlyatt.php


	//BEGIN stuffs for monthlystats.php
		$report_monthlystats_header = "Monthly Statistical Reports";
		$report_monthlystats_story = "Please, select a report category below and click the report to be generated.";
		$report_monthlystats_sno = "S/NO";
		$report_monthlystats_dept = "DEPARTMENT";
		$report_monthlystats_unit = "UNIT";
		$report_monthlystats_newpatient = "NEW PATIENTS";
		$report_monthlystats_oldpatient = "OLD PATIENTS";
		$report_monthlystats_totalatt = "TOTAL ATTENDANCES";
		$report_monthlystats_clinicsessions = "TOTAL CLINIC SESSIONS";
		$report_monthlystats_dept_error = "Zero(0) results found matching your search criteria.";
		$report_monthlystats_unit_error = "Zero(0) results found matching your search criteria.";
		$report_monthlystats_inp = "Inpatient Reports";
		$report_monthlystats_outp = "Outpatient Reports";
		$report_monthlystats_others = "Other Reports";

		//In-Patient Summary
		$report_monthlystats_inpsummary_header = "Inpatient Summary";
		$report_monthlystats_inpsummary_for = " for ";
		$report_monthlystats_inpsummary_to = " to ";
		$report_monthlystats_bedcomplement = "Bed Complement";
		$report_monthlystats_avgdailybeds = "Average Daily Number of Beds Available";
		$report_monthlystats_avgdailybedsoccupied = "Average Daily Number of Beds Occupied";
		$report_monthlystats_avglengthofdays = "Average Length of Stay per Patient in Days";
		$report_monthlystats_occuperc = "Percentage of Occupancy";
		$report_monthlystats_monthadmission = "Patients Admitted During the Month";
		$report_monthlystats_monthdischarges = "Patients Discharged During the Month";
		$report_monthlystats_monthdeaths = "Number of Deaths During the Month";
		$report_monthlystats_patsmonthbegin = "Patients at the Beginning of the Month";
		$report_monthlystats_patsmonthend = "Patients at the End of the Month";
		$report_monthlystats_admissions = "Admissions";
		$report_monthlystats_discharges = "Discharges";
		$report_monthlystats_deaths = "Deaths";
		$report_monthlystats_male = "Male";
		$report_monthlystats_female = "Female";
		$report_monthlystats_total = "Total";
		$report_monthlystats_back = "Back to List of Monthly Statistics";

		//Monthly Department Activity Analysis
		$report_monthlystats_deptact = "Monthly Department Activity Analysis";
		$report_monthlystats_deptact_bedsavail = "Beds Available";
		$report_monthlystats_deptact_avgdoccu = "Average Daily Occupancy";
		$report_monthlystats_deptact_bedsvac = "Beds Vacant as at Month End";
		$report_monthlystats_deptact_poccu = "% of Occupancy";

		$report_monthlystats_deptact_pdoveradm = "% of Death Over Admission";
		$report_monthlystats_deptact_pdovertdeaths = "% of Deaths Over Total Deaths";
		$report_monthlystats_deptact_tbeddays = "Total Available Bed Days";
		$report_monthlystats_deptact_tpatdays = "Total Patient Days";
		$report_monthlystats_deptact_avgstaylength = "Average Length of Stays";
		$report_monthlystats_deptact_turnoverinterval = "Turn-Over Interval";


		//Out-Patient Attendance
		$report_monthlystats_outpatattendance = "Attendances at Out-Patient Clinic (Consultant)";

		//Out-Patient Attendance by age group
		$report_monthlystats_outpatattendance_age = "Attendances at Out-Patient Clinic by Age Group";
		$report_monthlystats_outpatattendance_age_col1 = "0 - 28 Months";
		$report_monthlystats_outpatattendance_age_col2 = "1 - 59 Months";
		$report_monthlystats_outpatattendance_age_col3 = "5 - 9 Years";
		$report_monthlystats_outpatattendance_age_col4 = "10 - 19 Years";
		$report_monthlystats_outpatattendance_age_col5 = "20 - 40 Years";
		$report_monthlystats_outpatattendance_age_col6 = "40 and above";
		$report_monthlystats_outpatattendance_age_total = "Total";
		$report_monthlystats_outpatattendance_age_gtotal = "Grand Total";
		$report_monthlystats_outpatattendance_age_csessions = "Clinic Sessions";

		//In-Patient Admission by age group
		$report_monthlystats_inpatattendance_age = "In-Patient Admission by Age Group";
		$report_monthlystats_inpatattendance_age_col1 = "0 - 28 Days";
		$report_monthlystats_inpatattendance_age_col2 = "1 - 59 Months";
		$report_monthlystats_inpatattendance_age_col3 = "5 - 9 Years";
		$report_monthlystats_inpatattendance_age_col4 = "10 - 19 Years";
		$report_monthlystats_inpatattendance_age_col5 = "20 - 40 Years";
		$report_monthlystats_inpatattendance_age_col6 = "40 and above";
		$report_monthlystats_inpatattendance_age_total = "Total";
		$report_monthlystats_inpatattendance_age_gtotal = "Grand Total";
		$report_monthlystats_inpatattendance_age_csessions = "Clinic Sessions";


		//In-Patient Admission Analysis showing admissions, discharges & deaths
		$report_monthlystats_inpat_adm = "In-Patient Admission Analysis Showing Admissions, Discharges & Deaths";



		//OutPat Unit by Unit analysis
		$report_monthlystats_outpatunits = "Out-Patient Unit-by-Unit Analysis";
		$report_monthlystats_inpatunits = "In-Patient Unit-by-Unit Analysis";
		$report_monthlystats_inpatunitsadm = "In-Patient Unit-by-Unit Analysis Showing Admissions, Discharges & Deaths";
		$report_monthlystats_outpatunits_story = "Please, select the department and month / year for which the analysis will be viewed.";


		//Source of New Patients
		$report_monthlystats_patientsource = "Source of New Patients";
		$report_monthlystats_ps_source = "SOURCE";
		$report_monthlystats_ps_new = "NEW PATIENTS";
		$report_monthlystats_ps_ptotal = "% OF TOTAL";
		$report_monthlystats_ps_allpats = "ALL PATIENTS";


		//In-patient and out-patient attendances
		$report_monthlystats_inoutdepts = "In-patient and Out-Patient Attendances ";
		$report_monthlystats_ps_source = "SOURCE";
		$report_monthlystats_ps_new = "NEW PATIENTS";
		$report_monthlystats_ps_ptotal = "% OF TOTAL";
		$report_monthlystats_ps_allpats = "ALL PATIENTS";

	//END stuffs for monthlystats.php

//*** END REPORT STUFFS


//*** BEGIN SYS ADMIN STUFFS


	//Home Page (main.inc.php)
	$home_pageTitle = "Welcome";
	$home_welcome = "Welcome";
	$home_welcomestory = "Please, enter your username and password below to continue.";
	$home_welcomestory2 = "Please, click any of the links above to start performing actions.";
	$home_errorstory1 = "Sorry!";
	$home_errorstory2 = "You are not allowed to access that page.";
	$home_username = "USERNAME";
	$home_password = "PASSWORD";
	$home_loggedinuser = "logged-in user";
	$home_dept = "DEPARTMENT";
	$home_bank = "BANK";
	$home_usergroup = "USER GROUP";
	$home_register = "Have no password? Register here";
	$home_idleMsg = "You were temporarily logged out because of ";
	$home_minutes = " minute(s) ";
	$home_seconds = " second(s) ";
	$home_idleMsg2 = " of inactivity. Please, enter your username and password to continue or ";
	$home_idleMsg1 = " click here to log out permanently";
	
	//Staff Registration Page
	$staffreg_staffstory = "This form is for hospital staff only. Please, click the 'BANK TELLER' link below if you are a bank teller.";
	$staffreg_bankstory = "This form is for bank tellers only. Please, click the 'HOSPITAL STAFF' link above if you are a hospital staff.";
	$staffreg_title = "Staff Registration";
	$staffreg_no = "Staff No.";
	$staffreg_stafftitle = "Title";
	$staffreg_sname = "Surname";
	$staffreg_onames = "Other Names";
	$staffreg_phone = "Phone No. (GSM)";
	$staffreg_email = "E-Mail";
	$staffreg_bank = "Bank";
	$staffreg_dept = "Department";
	$staffreg_register = "Register";
	$staffreg_back = "Back to the Login Page";
	$staffreg_username = "Username";
	$staffreg_password = "Password";
	$staffreg_bankstaff = "BANK TELLER (Please, click here if you are a bank staff working in the hospital)";
	$staffreg_staff = "HOSPITAL STAFF (Please, click here if you are a staff of the hospital)";
	$staffreg_cpassword = "Confirm Password";
	$staffreg_failure = "An error occurred while trying to register you. Please, try again.";
	$staffreg_error_uniqueID = "Someone has registered with this staff number before. Please, enter another one.";
	$staffreg_error_emptyUP = "Please, enter your username and password";
	$staffreg_error_2shortUP = "Your username and password must be between 8 and 30 characters";
	$staffreg_error_uniqueuser = "Someone has registered with this username before. Please, enter another one.";
	$staffreg_error_cpass = "Your password is not the same as your confirmation password.";
	$staffreg_error_allfields = "Please, fill all fields before submitting the form.";
	$staffreg_success = "Your registration was successful.<br />
							You would receive an SMS alert when your registration has been approved. <br />
							Thereafter, you would be able to login with your username &amp; password to perform tasks on this portal.";

	//Sys Admin Home page
	$admin_button_savechanges = "Save Changes";
	$admin_header = "System Administrator Home Page";
	$admin_story = "Please, select an action to perform from the list below or use the sub-menu under System Administration in the main menu above.";
	$admin_menumanagement = "Menu Management";
	$admin_menumanagement_edit = "Edit Menu";
	$admin_menumanagement_add = "Add Menu";
	$admin_groupmanagement = "Groups Management";
	$admin_rightmanagement = "Rights Management";
	$admin_usermanagement = "User Management";
	$admin_audittrail = "Audit Trail";
	$admin_editsuccess = "Your changes were successfully saved";
	$admin_editfailure = "A problem occurred while trying to save your changes.";
	$admin_menucreatesuccess = "The menu was created successfully.";
	$admin_menucreatefailure = "A problem occurred while trying to create a new menu.";
	
	//Menu Management
	$admin_menumanagement_story = "Please, click the '+' / '-' image beside each menu item to traverse it or click the menu item to edit its properties.";
	$admin_menumanagement_invalidlink1 = "The link that brought you to this page is faulty. Please, ";
	$admin_menumanagement_invalidlink2 = " click here to choose a menu item to edit.";
	$admin_menumanagement_menu = "MENU NAME:";
	$admin_menumanagement_url = "URL:";
	$admin_menumanagement_order = "ORDER:";
	$admin_menumanagement_parent = "PARENT MENU:";
	$admin_menumanagement_module = "MODULE:";
	$admin_menumanagement_nomenu = "No menu has been created yet.";
	$admin_menumanagement_parentdesc = "(This menu item would become part of the sub-menu under any Parent Menu chosen here)";
	$admin_menumanagement_orderdesc = "(The position in which this menu item will appear relative to other menu items on the same level)";
	$admin_menumanagement_mainmenu = "Main Menu";
	$admin_menumanagement_publish = "Publish this menu item";
	$admin_menumanagement_dropdown = "Show as a drop-down in the top navigation bar";
	$admin_menumanagement_addlink = "Add a New Menu Item";
	$admin_menumanagement_back = "Back to Menu Management Page";
	$admin_menumanagement_all = "All";
	$admin_menumanagement_filter = "Filter";
	$admin_menumanagement_error_allformfields = "ERROR! All fields except the module field must be filled.";
	$admin_menumanagement_error_allfields = "ERROR! All fields must be filled.";


	//Group Management
	$admin_groupmanagement_story = "Please, click 'Create a New User Group' to create new groups or click any group that has been created before to edit its properties.";
	$admin_groupmanagement_addlink = "Create a New User Group";
	$admin_groupmanagement_nogroups = "No group has been created yet.";
	$admin_groupmanagement_addtitle = "Create User Group";
	$admin_groupmanagement_group = "GROUP";
	$admin_groupmanagement_desc = "DESCRIPTION";
	$admin_groupmanagement_error = "Please, enter the group name to be created.";
	$admin_groupmanagement_createsuccess = "The User Group was created successfully.";
	$admin_groupmanagement_creatfailure = "User Group Creation Failed";
	$admin_groupmanagement_back = "Back to Group Management";
	$admin_groupmanagement_edittitle = "Edit User Group";
	$admin_groupmanagement_invalidid = "Ple";
	$admin_menumanagement_invalidid1 = "The link that brought you to this page is faulty. Please, ";
	$admin_menumanagement_invalidid2 = " click here to choose a user group to edit.";


	//User Management
	$admin_usermanagement_story = "Please, click the name of a member of staff below to ";
//	$admin_usermanagement_story1 = "create a username / password for him / her";
	$admin_usermanagement_story2 = "enable / disable his / her account";
	$admin_usermanagement_story3 = "assign him / her to a user group";
	$admin_usermanagement_sno = "S/NO";
	$admin_usermanagement_staffNo = "STAFF NO";
	$admin_usermanagement_staffName = "STAFF NAME";
	$admin_usermanagement_dept = "DEPARTMENT";
	$admin_usermanagement_bank = "BANK";
	$admin_usermanagement_bankteller = "BANK TELLERS";
	$admin_usermanagement_userConfig = "USERNAME / PASSWORD";
	$admin_usermanagement_usergroup = "USER GROUP";
	$admin_usermanagement_config = "Modify User Group / Enable / Disable User";
	$admin_usermanagement_back = "Back to User Management Page";
	$admin_usermanagement_username = "USERNAME";
	$admin_usermanagement_password = "PASSWORD";
	$admin_usermanagement_cpassword = "CONFIRM PASSWORD";
	$admin_usermanagement_enableuser = "Enable this user";
	$admin_usermanagement_tellername = "TELLER NAME";

	$admin_usermanagement_error_user = "Please, enter a username to be configured for this user.";
	$admin_usermanagement_error_userpass = "Your username and password must be between 8 and 30 characters.";
	$admin_usermanagement_error_pass1 = "Please, enter a password to be configured for this user.";
	$admin_usermanagement_error_cpass = "Please, enter your password again in the confirm password field.";
	$admin_usermanagement_error_passcpass = "Your password must be the same as your confirmation password.";
	$admin_usermanagement_error_group = "Please, choose the user group to which this user will belong or click 'Group Management' to create user groups if none appears in the list.";


	//Right Management
	$admin_rightmanagement_story = "Please, click a user group below to configure the menu that users of the group have access to.";
	$admin_rightmanagement_story2 = "Please, note that you must add a menu to be able to add sub-menus under it.";
	$admin_rightmanagement_edit = "Edit User Group Rights";
	$admin_rightmanagement_edit_story = "Please, click the '+' / '-' image beside each menu item to traverse it or click the menu item to add it to the rights of the current group.";
	$admin_rightmanagement_editing = "Editing User Group ";
	$admin_rightmanagement_unknowngroup = "Unknown User Group";
	$admin_rightmanagement_add = "Add";
	$admin_rightmanagement_addc = "Add & Add Direct Sub-Menus";
	$admin_rightmanagement_remove = "Remove";
	$admin_rightmanagement_added = "Added";
	$admin_rightmanagement_addfailed = "Add Failed!";
	$admin_rightmanagement_editerror = "RIGHTS EDIT FAILED! Invalid group and / or menu";
	$admin_rightmanagement_back = "Back to Rights Management Page";
	$admin_rightmanagement_removefailed = "Remove Failed";
	$admin_rightmanagement_adding = "Adding...";
	$admin_rightmanagement_removing = "Removing...";

	//Change Password Page
	$admin_changepass_title = "Change Login Details";
	$admin_changepass_curuser = "CURRENT USERNAME";
	$admin_changepass_curpass = "CURRENT PASSWORD";
	$admin_changepass_newuser = "NEW USERNAME";
	$admin_changepass_newpass = "NEW PASSWORD";
	$admin_changepass_newcpass = "CONFIRM NEW PASSWORD";


	//Audit Trail Page
	$audittrail_story = "You can enter / select parameters to use in the audit trail search. Click the 
							<ul>
								<li><strong>'Search By User'</strong> tab to modify the search by selecting specific users <em>(All users shown in the result by default)</em>.</li>
								<li><strong>'Search By Date'</strong> tab to modify the search by selecting a date range <em>(Last three(3) days selected by default)</em>.</li>
								<li><strong>'Search By Pages'</strong> tab to modify the search by selecting specific pages <em>(All pages shown in the result by default)</em>.</li>
							</ul>";
	$audittrail_story_page = "All pages are shown in the result by default, but you can select pages to limit the result.";
	$audittrail_story_page2 = "You can also click the '+' / '-' sign beside each menu to view its sub-menus.";
	$audittrail_story_users = "All users are shown in the result by default, but you can select users to limit the result.";
	$audittrail_story_date = "The last three(3) days are shown in the result by default, but you can select a different date interval below.";
	$audittrail_usersearch = "Search By User";
	$audittrail_datesearch = "Search By Date";
	$audittrail_pagesearch = "Search By Pages";
	$audittrail_from = "FROM (DATE): ";
	$audittrail_to = "TO (DATE): ";
	$audittrail_showAudit = "Show Audit Trail";
	$audittrail_sno = "S/NO";
	$audittrail_staffno = "STAFF NO.";
	$audittrail_staffname = "STAFF NAME";
	$audittrail_nouser = "No user was found.";
	$audittrail_dept = "DEPARTMENT";

	//Audit Trail Search Result Page
	$admin_audittrail_view = "View Audit Trail Search Results";
	$audittrail_noResult = "There are no results matching your search criteria.";
	$audittrail_searchCriteria = "No search criteria was found. Please, ";
	$audittrail_searchCriteria1 = " click here to enter search criteria";
	$audittrail_newsearch = "New Audit Trail Search";

	//Audit Trail Page Names
	$admin_audittrail_allpagenames = "Possible Page Names (Audit Trail)";
	$admin_audittrail_allpagenames_none = "No Page Names were found for the selected URL";
	$admin_audittrail_allpagenames_story = "The possible name(s) for the URL ";
	$admin_audittrail_allpagenames_are = " is/are ";
	$admin_audittrail_postdata_fieldname = " FIELD NAME ";
	$admin_audittrail_postdata_fieldvalue = " FIELD VALUE ";
	$admin_audittrail_postdata_story = "The submitted data are as shown below ";
	$admin_audittrail_postdata_nopost = " Posted data not available. ";
	

	//Access Denied
	$admin_accessdenied = "Access Denied";


//*** END SYS ADMIN STUFFS





//*********************Dede Language Starts**********************/

//Patient Care Session Variables
if ($_SESSION[session_id() . "luth_loggedin"]){
/*	$myUserid = session_id()."userID";
	$myUserid = $$myUserid;
	$myDeptid = session_id()."deptID";
	$myDeptid = $$myDeptid;
	$myClinicid = session_id()."clinicID";
	$myClinicid = $$myClinicid;
	$myStaffid = session_id()."staffID";
	$myStaffid = $$myStaffid;
	$myConsultantid = session_id()."consultantID";
	$myConsultantid = $$myConsultantid;
	$myStaffname = session_id()."staffName";
	$myStaffname = $$myStaffname;*/


	$myUserid = $_SESSION[session_id() . "userID"];
	$myDeptid = $_SESSION[session_id() . "deptID"];
	$myClinicid = $_SESSION[session_id() . "clinicID"];
	$myStaffid = $_SESSION[session_id() . "staffID"];
	$myConsultantid = $_SESSION[session_id() . "consultantID"];
	$myStaffname = $_SESSION[session_id() . "staffName"];
}
//session variables end here

	
	//General Labels
	$submit_label = "Go";
	$save_label = "Save";
	$edit_label = "Edit";
	$continue_label = "Continue";
	$others_label = "Others";
	$consultant_label = "Consultant";
	$assigned_consultant_label = "Assigned Consultant";
	$clinic_label = "Clinic";
	$department_label = "Department";
	$ward_label = "Ward";
	$hospital_no_label = "Hospital No";
	$old_hospital_no_label = "Old Hospital No";
	$patient_label = "Patient";
	$patients_label = "Patients";
	$yes_label = "Yes";
	$no_label = "No";
	$select_option_label = "Select Option";
	$select_label = "Select";
	$update_label = "Update";
	$patient_condition_label = "Patient's Condition";
	$logged_in_as_label = "Logged in as";
	$date_label = "Date";
	$select_country_label = "Select Country";
	$select_state_label = "Select State";
	$select_department_label = "Select Department";
	$select_clinic_label = "Select Clinic";
	$select_patient_type_label = "Select Patient Type";
	$age_label = "Age";
	//General Labels end here
	
	//date labels starts here
	$date_title_label = "Today";
	$date_alt_text_label = "Open calendar...";
	$date_month_array = "'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'";
	$date_day_array = "'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'";
	$sun_label = "Sun";
	$mon_label = "Mon";
	$tue_label = "Tue";
	$wed_label = "Wed";
	$thu_label = "Thu";
	$fri_label = "Fri";
	$sat_label = "Sat";
	//date labels ends here
	
	//labels for patient registration form starts here
	$reg_header_label = "PATIENT REGISTRATION";
	$personal_detail_label = "Personal Details";
	$next_of_kin_label = "Next of Kin";
	$med_record_label = "Medical Record";
	$reg_type_label = "Registration Type";
	$emergency_label = "Emergency";
	$maternity_label = "Maternity";
	$surname_label = "Surname";
	$othernames_label = "Other Names";
	$address_label = "Address";
	$dob_label = "Date of Birth";
	$gender_label = "Gender";
	//gender options start here
	$gender_0_label = "Male";
	$gender_1_label = "Female";
	//gender options end here
	
	$phone_label = "Telephone";
	$email_label = "Email";
	$nationality_label = "Nationality";
	$state_label = "State";
	$civilstate_label = "Civil State";
	$occupation_label = "Occupation";
	$placeofwork_label = "Place of Work";
	$religion_label = "Religion";
	$name_label = "Name";
	$nokrelationship_label = "Relationship";
	$bloodgrp_label = "Blood Group";
	$genotype_label = "Genotype";
	$labrefno_label = "Lab. Ref. No";
	$rh_label = "Rh";
	$hb_label = "Hb";
	$condition_label = "Condition";
	$passport_label = "Passport";
	$retainership_label = "Retainership Scheme";
	$company_label = "Company";
	$nhis_label = "NHIS";
	$hmo_label = "HMO";
	$dependant_label = "Dependant";
	$employee_name_label = "Employee Name";
	$bookingcategory_label = "Booking Category";
	$identity_card_no_label = "Identity Card No.";
	$card_no_label = "Card No.";
	$single_label = "Single";
	$married_label = "Married";
	$divorced_label = "Divorced";
	$widowed_label = "Widowed";
	$selectReligion_label = "Select Religion";
	$xtian_label = "Christianity";
	$islam_label = "Islam";
	$others_label = "Others";
	$outside_nigeria_label = "Outside Nigeria";
	$select_company_label = "Select Company";
	$select_hmo_label = "Select HMO";
	$plan_label = "Plan";
	$select_plan_label = "Select Plan";
	$confirm_reg_label = "Confirm Registration";
	$edit_details_label = 'Edit Patient Details';
	$success_reg_label = "Patient has been successfully registered";
	$patient_type_label = "Patient Type";
	$old_label = "Existing Patient";
	$new_label = "New Patient";
	$loading_label = "Loading...";
	//field labels for patient registration form ends here
	
	//field labels for Patient Admission
	$dateattended_label = "Date Attended";
	$referrer_label = "Referrer";
	$seenby_label = "Seen by";
	$authorizedBy_label = "Authorized by";
	$referring_consultant_label = "Referring Physician";
	$source_referrer_label = "Source of Referrer";
	//ends here
	
	//Search Labels start here
	$search_label = "Search";
	$advancedsearch_label = "Advanced Search";
	$search_text = "search to see if patient has been formerly registered, you can search by hospital number or name";
	$advanced_search_label = "Advanced Search";
	$search_not_found_label = "No Result Found";
	$search_field_label = "Enter Search Item";
	//ends here
	
	//Error Messages Start Here
	$error_msg_0 = "<p>OH NO! An Unknown Error Occured! If this error persists, Please contact the System administrator.</p>";
	$error_msg_1 = "<p>Oops, looks like an error occurred while trying to retrieve user detail, please try again later.</p>";
	$error_msg_2 = "<p>Oops, looks like this user does not exist.</p>";
	$error_msg_3 = "<p>Sorry, you are not authorised to view this page. Please contact the System Administrator</p>";
	$error_msg_4 = "<p>Sorry, you are not authorised to view this page.</p>";
	$error_msg_5 = "<p>OH NO! An Unknown Error Occured! If this error persists, Please contact the System administrator.</p>";
	$error_msg_6 = "<p>Sorry, the hospital number does not exist</p>";
	
	//Appointment Label
	$today_appointment_label = "Today's Appointments";
	$no_appointments_label = "No Appointments";
		//BEGIN stuffs for appointment.php
			$view_appointment_header = "PATIENT APPOINTMENTS";
			$appointment_text_label = "Please, select the date range for which patients' appointments will be viewed.";
			$appointment_text_label2 = "Please, select department and the date range for which patients' appointments will be viewed.";
			$pat_appointment_title = "Patient Appointment Listing for ";
			$pat_appointment_unknown = "<Unknown Doctor>";
			$pat_appointment_allnurses = "All Doctors";
			$pat_appointment_form_deptfield = "DEPARTMENT";
			$pat_appointment_form_doctorfield = "DOCTOR";
			$pat_appointment_form_fromdatefield = "FROM";
			$pat_appointment_form_todatefield = "TO";
			$pat_appointment_error_noresult = "Zero (0) result(s) matching your search criteria was found";
			$pat_appointment_sno = "S/no";
			$pat_appointment_date = "Date";
			$pat_appointment_patientcode = "PATIENT CODE";
			$pat_appointment_patientname = "Patient Name";
			$pat_appointment_sex = "Sex";
			$pat_appointment_age = "Age";
			$pat_appointment_transcode = "TRANS. CODE";
			$pat_appointment_apptype = "Type";
			$pat_appointment_appstatus = "Status";
			$pat_appointment_vitalsigns_appstatus = "Vital Signs Status";
			$pat_appointment_starttime = "Start Time";
			$pat_appointment_endtime = "End Time";
			$pat_appointment_dept = "Department";
			$pat_appointment_action = "Action";
			$pat_appointment_status = "Status";
			$pat_appointment_paid = "Paid";
			$pat_appointment_unpaid = "Not Paid";
			$pat_appointment_start = "Start Treatment";
			$pat_appointment_nil = "Pending";
			$pat_appointment_error_noresult = "There is no patient on the appointment list";
			$pat_appointment_error_nodoctor = "No consultant was found in the appointment records";
			$pat_appointment_error_unknown_doctor = "Unknown doctor";
			$pat_appointment_consultation = "Consultation";
			$pat_appointment_followup = "Follow-up";
			$pat_appointment_list = "Appointment Listing";
		//END stuffs for appointment.php
		
	//patient treatment - pat_medicalrecord.php, new_appointment.php, new_appointment_form.php, new_appointment_page.php
	$examination_label = "Examination/Diagnosis";
	$history_taking_label = "History Taking";
	$plan_label = "Plan";
	$general_exam_label = "General Physical Examination";
	$system_examination_label = "Systemic Examination";
	$diagnosis_label = "Diagnosis";
	$differential_diagnosis_label = "Differential Diagnosis";
	$complaint_label = "Presenting Complaint";
	$complaint_history_label = "History of Presenting Complaint";
	$medical_history_label = "Past Medical History";
	$social_history_label = "Family and Social History";
	$drug_history_label = "Drug History";
	$system_review_label = "Review of Systems";
	$schedule_appointment_label = "New Appointment";
	$transfer_label = "Transfer";
	$admit_label = "Admit";
	$discharge_label = "Discharge";
	$view_medical_history_label = "Medical History";
	$future_appointment_label = "Sorry, this patient has already been booked for a future appointment.";
	$missed_appointment_label = "Sorry, this appointment was missed by the patient.";
	$remark_label = "Remark";
	$click_here_reschedule_appointment_label = "Click Here to Reschedule Appointment";
	$prescription_label = "Prescription";
	$investigation_label = "Investigation";
	$treatment_label = "Treatment";
	$previous_appointment_label = "Previous Appointments";
	$clear_label = "Clear";
	$drug_name_label = "Drug Name";
	$other_treatment_label = "Other Treatment";
	$no_previous_visits = "No Previous Visits";
	$search_result_label = "Search Result";
	$patient_not_found_label = "Patient not found";
	$patient_not_on_appointment_list = "Sorry this patient is not on the appointment list";
	$hospital_no_msg_label = "Please enter patient's hospital number to commence treatment";
	$patreg_printnow_label = "Print Now";
	$patreg_backbtn = "Back to Registration Home";
	$pat_yet_to_pay = "Payment of this Service is pending, please advise patient to make payment";
	$dosage_label = "Dosage";
	$patient_transfer_label = "Patient Transfer";
	$future_transfer_label = "Sorry, this patient already exist on your outgoing transfer list.";
	$patient_admission_label = "Patient Admission";
	$future_admission_label = "Sorry, this patient already exist on your pending admission list.";
	$date_attended_label= "Date Attended";
	$date_discharged_label = "Date Discharged";
	


/*******Added by Tunde*/
	$vitalsigns_story1 = "Please, select a clinic below so as to view the appointment list for today and be able to enter the vital signs of patients by clicking their names / hospital number.";
//	$vitalsigns_story1 = "Please, click the name or hospital number of any patient below to enter / edit the vital signs of the patient.";
	$vitalsigns_entrypage_story = "Please, enter the appropriate vital signs for the selected patient.";
	$vitalsigns_appointments = " Clinic Appointments for Today, ";
	$vitalsigns_noappointment = "Appointments are yet to be fixed for today in this clinic.";
	$vitalsigns_invalidPatient = "Sorry! The selected patient is not on the appointment list for today. Please, select another one.";
	$vitalsigns_usedByDoctor = "Sorry! you can not edit (but can only view) the vital signs of this patient again for today because it has been used by a doctor to treat the patient.";
	$vitalsigns_temperature = "TEMPERATURE";
	$vitalsigns_pulse = "PULSE RATE";
	$vitalsigns_bloodpressure = "BLOOD PRESSURE";
	$vitalsigns_weight = "WEIGHT";
	$vitalsigns_respiration = "RESPIRATION";
	$vitalsigns_others = "OTHERS";
	$vitalsigns_saved = "Vital Signs Saved Successfully";
	$vitalsigns_saveFailed = "Saving of Vital Signs Failed";

/*******/

//*********************Dede Language Ends**********************/







//**************BEGIN BILLING / ADMISSION&DISCHARGE / DRUGS / WARDS language variables
$lang_form_title ="Form Title:  " ;
//Form name = drug
$lang_drug_no ="ID" ;
$lang_drugcat ="Drug Category" ;
$lang_drug_desc   ="Drug Desc." ;
$lang_drug_expirydate    ="Drug Exp. Date" ;
$lang_drug_reorderlevel ="Re-order Level" ;   
$lang_drug_manufacturer    ="Manufacturer" ;
$lang_drug_unitpack    ="Drug Unit Pack" ;
$lang_drug_price   ="Drug Unit Price" ; 
$lang_drug_qtyinstock    ="Drug Quantity in Stock" ;
$lang_title_value_pdrug="Drugs Specification";
$lang_PageTitle_pdrug="Add Drugs";
$lang_PageTitle_editpdrug="Edit Drug";
$lang_drugtype_drop="Drug Form";
$lang_drugItemno="Item No";
$lang_doctor_prescription="Doctor Prescription";
$lang_drugfromdb="Drugs from Inventory";
$lang_patientwaiting4drug="Patients Awaiting Drugs Collection ";
$lang_invoice_items="Invoice Items";
//$lang_PageTitle_druddispensing="Drugs Dispensing";
$lang_qty="Quantity";
//Form name =drug_interaction
$lang_drugint_id ="ID" ;  
$lang_drug   ="Drug" ;
$lang_drugint_sideffect  ="Drug Interaction Side Effect" ;
$lang_drugint_contraindication   ="Drug Contra Indication" ;
$lang_drugint_indication  ="Drug Interaction" ; 
$lang_user_id ="User ID" ;
$lang_title_value_drugint="Drug Interaction";
$lang_PageTitle_drugint="Add Drug Interaction";
//Form name =drugcategory

$lang_drugcat_id ="ID" ;
$lang_drugcat_name  ="Category Name" ;
$lang_PageTitle_drug="Add Drug Category";
$lang_PageTitle_editdrug="Edit Drug";
$lang_title_value_drug="Drug Categories";

//Form name =drugsubcategory
$lang_drugsubcat_name="Sub Category Name";
$lang_title_value_subdrug="Drug Sub Category";
$lang_PageTitle_subdrug="Add Drug Sub Category";
$lang_drugsubcat_id="ID";
$lang_drugcat_name="Category Name";
$lang_PageTitle_editdrugsub="Edit Drug Sub Category";


//Form name =servicecategory
$lang_PageTitle_scat="Add Service Category";
$lang_servicecat_id ="ID" ;
$lang_servicecat_name  ="Category Name" ;

//Form name =service
$lang_PageTitle_service="Service Fees";
$lang_button_serv="Add Service";
$lang_service_id ="ID" ;
$lang_service_name  ="Service Name" ;
$lang_service_amount  ="Amount" ;
$lang_servicecat_id ="Category" ;
$lang_division ="Division" ;
$lang_edit_service ="Edit Service";
$lang_select_unitservice ="Select UNITS or BILLING PARAMETER";
$lang_radio_option1="Entry Option";
$lang_radio_option2="Billing Parameter or Service name & Amount";
$lang_PageTitle_service="Add Service Fees"; 
$lang_PageTitle_editservice="Edit Service / Billing Parameter"; 
$lang_instruction="Enter amount if there are no other billing parameter affecting the billing.";
$lang_service_desc="Service Description";
$lang_PageTitle_editscat="Edit Service Category";
$lang_title_value_scat="Service Categories";
$lang_service_amount="SERVICE AMOUNT (NGN)";

//Form name =inpatient_admission
$lang_PageTitle_inpatient="New Patient Admission form ";
$lang_PageTitle_inpatientedit="Patient On Admission Editting";
$lang_PageTitle_inpatientview="Patient On Admission";
$lang_title_value_inpatient="List of Patients On Admission";
$lang_inp_id   ="ID" ;
$lang_inpatadm_id   ="Hospital No." ;
$lang_inp_dateattended  ="Date attended" ;
$lang_inp_datedischarged ="Date Discharge" ; 
$lang_inp_dischargedto   ="Discharge to" ;
$lang_inp_conddischarged   ="Discharge Condition" ;
$lang_inp_refferer   ="Refferer" ;
$lang_ward   ="Ward" ;
$lang_ct_id ="Condition On entry" ;  
$lang_inp_reffererdesc="Refferer Desc";
$lang_inpatdept="Department";
$lang_inpat="Patient";
$lang_inpatname="Name";
$lang_inp_fullname="Name of Patient";
$lang_PageTitle_inpatientdischarge="Patient Discharge";
$lang_inp_datedischarged="Discharge Date";
$lang_inp_dischargedto="Discharge to";
$lang_ct_id_discharge="Condition on Discharge";
$lang_inp_conddischargeddesc="Discharge Description";
$lang_inpatclinic="Clinic Name";
$lang_inpatconsultant="Consultant";
$lang_inpataddr="Address";
$lang_inpatsex="Sex";
$lang_inpatsexfemale="Female";
$lang_inpatsexmale="Male";
$lang_inpatage="Age";

//Form name =Patient Deposit
 $lang_PageTitle_inpatientdeposit="Patient Admission";
$lang_patinp_datedeposit  ="Date Admission Initiated" ;  
$lang_deposit  ="Deposit" ;
$lang_withdrwal ="Withdrawal" ; 
$lang_account_type   ="Account Type" ;
$lang_adm_regular  ="Regular Admission(1 week) " ;
$lang_adm_shortstay  ="Shortstay(2 days)" ;
$lang_adm_sideward  ="Side Ward" ;
$lang_adm_icuward  ="I.C.U Ward" ;
$lang_adm_icutheatre  ="I.C.U Theatre" ;
$lang_adm_burns  ="Burns" ;
$lang_adm_tetanus  ="Tetanus" ;
$lang_user_id   ="" ;
$lang_adm_purpose="Admission Purpose";
$lang_patient_invoices="Patient's Invoices";
//Form name =maternity_admission



$lang_matadm_id  ="" ;  
$lang_mat_id  ="" ;
$lang_matadm_admittedate ="" ; 
$lang_matadm_maturity   ="" ;
$lang_matadm_dischargedate  ="" ;
$lang_user_id   ="" ;


//Form name =Billing


$lang_PageTitle_labservice="Laboratory Services";
$lang_PageTitle_druddispensing="Drugs Dispensing";
$lang_PageTitle_billing="Billing Centre";
$lang_PageTitle_admcharges="Admission Charges";
$lang_admcharges_invoicetype="Invoice Type";
$lang_invoice_no="Invoice No";
$lang_itemdesc="Items/ Description";
$lang_sno="S/No.";
$lang_discounttype="Discount Type";
$lang_discountamt="Discount Amount";
$lang_price="Unit Price";
$lang_Amount="Amount";
$lang_PageTitle_invoicing="Invoicing";
$lang_PageTitle_admcharges="Invoice";
$lang_PageTitle_receipting="Receipting";
$lang_PageTitle_admchargesReceipt="Receipt";
$lang_tick="Tick";
//$lang_PageTitle_invoicingserv="";
//Form name =patient_admission


$lang_PageTitle_patient="Generate Hospital No";
$lang_patadm_id ="ID" ;
$lang_form_title='Title   ';
$lang_reg_hospital_no  ="Hospital Number" ;
$lang_ctc_id  ="Condition Type Category" ;
$lang_title_value_patient="Patient Specifications";
//Form name =treatment_prescription



$lang_treatpre_id  ="" ;
$lang_drug_id  ="" ;
$lang_treatment_id  ="" ;
$lang_treatpre_qty ="" ;
$lang_treatpre_dosage  ="" ; 
$lang_treatpre_transno   ="" ;
$lang_treatpre_status  ="" ;
$lang_user_id  ="" ;

//Form name =wards

$lang_title_key="Title";
$lang_title_value_ward="Ward Specifications";
$lang_PageTitle_ward="Add Ward";
$lang_ward_id ="ID" ;
$lang_clinic_name  ="Clinic Name" ; 
$lang_ward_name   ="Ward Name" ;
$lang_ward_bedspaces  ="Ward Bedspaces" ;
$lang_ward_siderooms   ="Ward Side rooms " ;
$lang_PageTitle_editward="Edit Ward";


//Form name =Drug Order
$lang_title_key="Title";
$lang_title_value_drugtype="Drug Priority";
$lang_drugtype_title="Add Drug Priority";
$lang_drugtype_id ="ID" ;
$lang_drugtype_name  ="<b>Drug Form</b>" ; 
$lang_drugtype_edit="Edit Drug Priority";
$lang_drugtype_order  ="Drug Order No." ;
$lang_drugtypeReorder_title="Adjust Drug Priority";
$lang_drugtype_success="Saved Successfully";

//Form name =service name
$lang_servicename_title="Add Service Item ";
$lang_servicename_id ="ID" ;
$lang_service_name  ="Service Name" ;

$lang_title_value_servicename=" Service Item";
$lang_PageTitle_servicename="Add Service Item";
$lang_PageTitle_editservicename='Edit Service Item';
//$lang_PageTitle_druddispensing="Laboratory Services";
//General message 
$lang_mesg_update   ="Record Updated Successfully" ;
$lang_mesg_save  ="Record saved Successfully" ;
$lang_mesg_delete  ="Record deleted Successfully" ;
$lang_search_button_label="Search";
$lang_search_by="Search by";
$lang_search_for="Search for";
$lang_PageTitle_registry="Registry";
$lang_action="Action";
//**************END BILLING / ADMISSION&DISCHARGE / DRUGS / WARDS language variables




//LABELS FOR LAB SECTION***********************************************************************************************

//user registration
$lab_reg_search="Search User records";
$userregsearch="Search For User";
$search_option_labpin="Lab PIN";
$search_option_hospitalno="Hospital no";
$search_option_name="Name";
$noresults="No match found";
$nosearchpara="No search word specified!";
$resultsfound="record(s) found.";
//$searchtitle="Search Results: ";

$lab_reg_label_labpin="Lab Pin";
$lab_reg_label_hospital="Hospital no";
$lab_reg_label_name="Name";
$lab_reg_label_age="Age";
$lab_reg_label_sex="Sex";
$lab_reg_choose_sex="Choose Sex";
$lab_reg_day="Day";
$lab_reg_year="Year";
$lab_reg_month="Month";
$lab_reg_jan="Jan";
$lab_reg_feb="Feb";
$lab_reg_mar="Mar";
$lab_reg_apr="Apr";
$lab_reg_may="May";
$lab_reg_jun="Jun";
$lab_reg_jul="Jul";
$lab_reg_aug="Aug";
$lab_reg_sep="Sep";
$lab_reg_oct="Oct";
$lab_reg_nov="Nov";
$lab_reg_dec="Dec";

$lab_reg_header = "Click here to Register Non-LUTH-patient (For external lab requests)";
$lab_reg_title = "Laboratory User Records";
$lab_reg_lab_pin = "Lab PIN";
$lab_reg_hospital_no= "Hospital no";
$lab_reg_surname = "Surname";
$lab_reg_othernames = "Other Names";
$lab_reg_yob = "Year of Birth";
$lab_reg_sex = "Sex";
$lab_reg_male = "Male";
$lab_reg_female = "Female";
$lab_reg_staff_employee_id= "Staff in charge";
$lab_reg_staff_employee_id_hint= "Please enter your staff number";
$lab_reg_user_comment="Comment";
$lab_reg_submit = "Register";
$lab_reg_reset = "Cancel";
$lab_reg_cmdmodify= "Edit";
$lab_reg_cmddelete= "Delete";
$lab_reg_error_id="You must enter either hospital no (for luth patients) or Name (for Non-luth patients).";
$lab_reg_lab_pin_error="You must enter Lab PIN";
$error_reg_hospital_no_text="The Hospital number you tried to register is invalid. Please contact Health Records!";
$error_reg_hospital_exists_text="A Lab-User with that Hospital number has been previously registered!";
$regsuccessmsg="New user was successfully registered. Lab PIN: ";
$mod_rec_header="Click here to MODIFY User Record";
$lab_reg_update="Update";
$modsuccessmsg="User record was successfully updated!";
$lab_luthpat="Register LUTH-patient";
$lab_luthpat_modify="Modify LUTH-patient record";
$lab_nonluthpat="Register Non-LUTH-patient (For external lab requests)";
$lab_nonluthpat_modify="Modify Non-LUTH-patient record";
//$lab_nonluth="External Laboratory Users <span class=\"lab-formhint\">(Already registered LUTH patients should ignore this section)</span>";

//Lab tests home
$lab_test_title = "Laboratory Test Records";
$lab_test_home_welcome= "Welcome";
$lab_test_home_reg = "Request a NEW Laboratory Test";
$lab_test_home_instruct="Please Select a Test to proceed";
$lab_tests_home_getunits = "Proceed";
$lab_tests_home_register= "New";
$lab_tests_home_update="Update";
$lab_tests_home_validate= "Validate";
$lab_tests_home_report="Report";
$lab_test_lab_pin="Lab PIN";
$lab_test_search="Lab Test Requests";
$labtestsearchbutton="Search";
$lab_test_specimen="Specimen(s)";
$lab_test_other_specimen="Other Specimen(s)";
$lab_test_comments="Other Comments";
$lab_test_analytes="Select required analytes";
$lab_test_regrequest="Register Test Request";
$lab_having="Having";
$search_option_pending="Pending";
$search_option_preliminary="Preliminary";
$search_option_validated="Validated";
$search_option_labno="lab no";
$search_option_reg_hospital_no="Hospital no";
$search_option_searchresults="Search Result";
$lab_displaying="Currently displaying";
$lab_test_records="test records";
$labtestlisttext="List";
$lab_test_details_btn="Details";
$lab_details_title="Laboratory Test Records";
$lab_jumptorequest="Click here to enter a NEW Test Request";
$lab_test_positive="Lab-test request was successfully made with lab no";
$lab_test_negative="Lab-test request failed! Please try again!";
$lab_test_negative_test="Reason: Test was not specified!";
$lab_test_negative_pin="Reason: Unregistered Lab pin specified!";
$lab_test_negative_number="Reason: Unregistered hospital number specified!";
$lab_test_labno="Lab no";
$lab_test_labpin="Lab PIN";
$lab_test_labtest="Lab Test";
$lab_test_labname="Name";
$lab_test_hospitalno="Hospital no";
$lab_test_labdate="Date";
$lab_test_labstatus="Status";
$lab_test_paystatus="Payment";
$lab_test_labact="Actions";
$lab_test_proceed="Proceed to Test details and Result";
$lab_test_unpaid="not paid";
$lab_test_paid="paid";
$lab_result_pay_status="Payment Status";

//Lab Test Results
$lab_detail_view="View Test Details";
$lab_result_view="Click here to view Test Results";
$lab_summary_view="Lab Test Summary";
$lab_result_validate="Validate Result";
$lab_result_save="Save Changes";
$lab_result_labno="Lab no";
$lab_result_labpatient="Lab Patient";
$lab_result_requeststaff="Request Doctor";
$lab_result_requestdate="Request Date";
$lab_result_labtest="Lab Test";
$lab_result_sample="Sample/Specimen";
$lab_result_positive="Test result was successfully recorded!";
$lab_result_negative="Test result was NOT successfully recorded!";
$lab_validate_positive="Test result was successfully validated!";
$lab_validate_negative="Test result was NOT successfully validated!";
$lab_test_result="Test Result";
$lab_test_reg_hospitalno="Hospital no";
$lab_result_comments="Request Comments";
$lab_result_status="Test Status";
$lab_result_comments="General Summary/Comments";
$lab_result_cancel="Cancel Changes";


//Daily Records
$lab_rec_header = "Daily Laboratory Records";
$lab_rec_lab_no_search = "Lab no / Hospital no";
$lab_rec_submit_search = "Search for record";

$lab_rec_lab_no = "Lab no";
$lab_rec_lab_no_hint = "No Lab no yet? Please <a href=\"index.php?p=register&m=lab\">register new lab user</a>";
$lab_rec_submit = "Search for record";
$lab_rec_staff_employee_id= "Staff in charge";
$lab_rec_staff_employee_id_hint= "Please enter your staff number";
$lab_rec_results= "Results";
$lab_rec_request= "Request";
$lab_rec_submit = "Submit";
$lab_rec_reset = "Reset";
$lab_rec_origin = "Test Origin";
$lab_rec_clinical_summary = "Clinical Summary";
$lab_rec_ward= "Ward";
$lab_rec_clinic= "Clinic";
$lab_rec_outside= "Outside";
$lab_rec_specimen= "Specimen";

$lab_rec_searchhead = "Update an existing record";
$lab_rec_search = "Search Results: Select a Lab record to update.";
$lab_daily_rec_head = "Enter a new daily record";

//LAB SECTION ENDS***********************************************************************************************






/***    BEGIN: Pharmacy Inventory    ***/
$inventory_welcomeMsg="Welcome to the pharmacy inventory unit";
$inventory_failed_to_search_location="Failed to search";
$unknownLocation="Unknown location";
$inventory_index_page_title = "Pharmacy Inventory";
$inventory_folder_list="List items";
$inventory_confirm_delete_question="Do you really want to delete this ";
$inventory_folder_new="Add new item";
$inventory_add_prompt="Add new...";
$inventory_item="Inventory item";
$inventory_edit="Edit";
$inventory_invalid_operation="Invalid operation";
$inventory_nothing_found="Nothing as such found";
$inventory_add_subcategory="Add subcategory";
$inventory_items="Inventory items";
$inventory_vendor="Inventory vendor";
$inventory_transfer="Inventory transfer";
$inventory_purchase_order="Inventory purchase order";
$inventory_category="Item category";
$inventory_presentation="Presentation";
$inventory_form="Form";
$inventoryItemCreationSuccessNews="New item successfully created";
$inventory_strength="Strength";
$inventory_section="Inventory section";
$inventory_manufacturer="Manufacturer";
$inventory_rejection_reason="Rejection reason";
$inventory_select_action_prompt="Please select an action on the left hand side to continue";
$inventory_list_prompt="View existing...";
$inventory_item_list_prompt="Stock items";
$inventory_vendor_list_prompt="Inventory item vendors";
$inventory_transfers_list_prompt="Inventory transfers";
$inventory_purchase_order_list_prompt="Purchase orders";
//-----new category creation variables----
$inv_categoryForm_createLabel_prompt="Create new category";
$inv_categoryForm_name_label="Category name";
$inv_categoryForm_description_label="Category description";
$inventoryCategoryCreationSuccessNews="New category successfully added";
$inventoryCategoryCreationFailureNews="System Error! Unable to add the new category. Please retry";
$inventory_subcategories="Sub categories";
$inventory_remove_category="Remove Category";
$inventory_add_category="Add category";
$inventory_no_categoryName_error_msg="Please indicate the name of the new category to continue";
$inv_categoryForm_parent_label="Parent category";
$inventory_none_in_existence="None in existence yet";
$inventory_categories="Categories";
$inventory_quantity="Quantity";
$inventory_total="Total";
$inv_itemForm_create_prompt="Add a new item";
$inventorySubcategorySystemFailureMsg="System error: Unable to search for  sub-categories";
$inventorySubcategoryNoneMsg="No sub-categories";
$inventory_click_to_edit="Click to edit";
$inventory_category_update_failure_message="System Error! Unable to update category this time. Please retry";
$inventory_editingCategory="Editing category";
$inventory_category_successfully_updated="Category successfully updated";
$inventory_category_describe_the_category="Describe the category";
$inv_SubmitBtnValue="Setup";
$inventory_update="Update";
$inventory_click_to_add_category="Click to add a new category under this category";
$inventory_no_category_found_msg="No categories found";
//-------new Section creation variables------
$inv_sectionForm_createLabel_prompt="Create new section";
$inv_sectionForm_name_label="Section name";
$inventory_category_delete_success_msg="Category successfully removed";
$inventory_category_delete_error_msg="System Error! Unable to remove inventory category. Please retry";
$inventory_click_to_remove_category="Click to remove this category";
//-------------Location variables
$inventory_location="Inventory location";
$inventory_location_list_prompt="Inventory locations";
$inv_locationForm_createLabel_prompt="Create new location";
$inventoryLocationCreationSuccessNews="New location successfully created";
$inventoryLocationCreationFailureNews="Sytem error! Unable to create new inventory location at this time. Please retry";
$inventory_location_describe_the_location="Describe the location";
$report=$inventory_location_dbconnnect_error="System error! Unable to connect to find inventory locations at this time. Please retry";
$inventory_no_location_msg="No existing locations";
$inventory_no_locationName_error_msg="Please indicate the name of the new location";
$inv_locationForm_name_label="Location name";
$inv_locationForm_description_label="Location description";
$inventory_location_successfully_updated="Location successfully updated";
$inventory_location_update_failure_message="System Error! Unable to update location at this time. Please retry";
$inventory_add_new_location="Add new location";
$inventory_edit="Edit";
$inventory_remove="Remove";
$inventory_click_to_remove_location="Click to delete this location";
$inventory_location_delete_error_msg="Unable to remove location at this time. Please retry";
$inventory_add_new_location="Add new location";
$inventory_location_delete_success_msg="Location successfully removed";
$inventory_locations="Inventory locations";
$inventoryEditingLocation="Editing inventory location";
$inventory_location_notFound_msg="No such location found";
$inv_sectionForm_description_label="Section description";
//---create new rejection reason variables -----------
$inv_reasonForm_createLabel_prompt="Create new rejection reason";
$inv_rejectionForm_name_label="Rejection reason";
$inv_rejectionForm_details_label="Rejection details";
//---create new manufacturer variables -----------
$inv_manufacturerForm_createLabel_prompt="Create new manufacturer";
$inv_manufacturerForm_name_label="Manufacturer name";
$inventory_click_to_remove_manufacturer="Click to remove this manufacturer";
$inventory_add_new_manufacturer="Add new manufacturer";
$inventory_manufacturer_updated_successfully="Manufacturer information successfully updated";
$inv_manufacturerForm_rcno_label="RC. No";
$inventory_click_to_remove_manufacturer="Click to remove this manufacturer";
$inventory_click_to_edit_manufacturer="Click to edit this manufacturer's information";
$inv_manufacturerForm_updateLabel_prompt="Update manufacturer";
$inv_manufacturerForm_address_label="Address";
$inventory_manufacturer_list_failure_news="No manufacturer found yet";
$inventoryManufacturerSuccessfullyAdded="Inventory manufacturer successfully addded";
$inventorySystemFailureAddingManufacturer="System Failure: Unable to add manufacturer. Please retry!";
$inventory_manufacturer_list_failure_news="System failure: Unable to find item manufacturers this time. Please retry";
$inventory_manufacturers="Inventory manufacturers";
$inventorySystemFailureUpdateManufacturer="System failure: Unable to update manufacturer information this time. Please retry";
$inventory_no_manufacturerName_error_msg="Please indicate the name of the manufacturer";
$inventory_manufacturer_updated_successfully="Manufacturer information successfully updated";
$inventory_please_indicate_manufacturers_name="Please indicate manufacturer's name";
//---create new vendor variables -----------
$inv_vendorForm_createLabel_prompt="Create new vendor";
$inv_vendorForm_updateLabel_prompt="Update vendor information";
$inventory_vendor_info_failure_msg="System failure! Unable to get the selected vendor details right now. Please retry!";
$inventory_noVendor_msg="No registered vendors";
$inventory_click_to_edit_vendor="Click to edit this vendor's details";
$inventorySystemFailureUpdateVendor="System failure! Unable to update vendor informationat this time. Please retry.";
$inventory_add_vendor="Add vendor";
$inventory_list_vendors="List vendors";
$inventory_vendor_notFound_msg="No such vendor found in record. Please retry";
$inventory_vendor_updated_successfully="Vendor information successfully updated";
$inv_vendorForm_name_label="Vendor name";
$inv_vendorForm_address_label="Vendor address";
$inv_vendorIncYr_label="Year of Incorporation";
$inv_vendorRCNO_label="RC. No.";
$inv_vendorLicense_label="License No.";
$inv_vendorTel_label="Telephone";
$inv_vendorMobile_label="Mobile";
$inventory_vendor_delete_error_msg="System Error! Unable to delete this vendor this time. Please retry";
$inv_vendorEmail_label="Email";
$inv_vendorWebsite_label="Website";
$inventory_vendor_delete_success_msg="Vendor successfully removed";
$inventory_Vendors="Vendors' list";
$inventory_click_to_remove_vendor="Click to remove this vendor";
$inventorySystemFailureAddingVendor="System Failure: Unable to add vendor this time. Please retry!";
$inventory_vendor_added_successfully="Vendor successfully added";
$inventory_no_vendorName_error_msg="Please indicate the name of the vendor and retry";
$inventory_vendor_list_failure_msg="System failure! Unable to find registered vendors at this time. Please retry";
$inventory_add_new_vendor="Add a new vendor";
//------create new item variables-------
$inv_itemForm_create_prompt="Add new item";
$inventory_item_locations_distribution="Item distribution in locations";
$inventory_beginning_bals="Beginning quantities";
$inventory_describe_the_category="Describe the category";
$inv_itemForm_name_label="Item name";
$inventoryItemCreationSuccessNews="Inventory item successfully created";
$inventoryItemCreationFailureNews="System error: Unable to create inventory item at this time. Please retry";
$inventoryItemExtraCreationFailureNews="System error: Unable to create full information for the item.";
$inventory_drug_search_failure_msg="System Error: Unable to list existing items. Please retry";
$inventory_no_item_found_msg="No item found in stock";
$inventory_reorder_level="Reorder level";
$inventory_no_item_found_msg="No item found in stock";
$inventory_noCategory_msg="You have not indicated any category for this item";
$inventory_categorizing_failure_msg="System error: Unable to properly categorize the item";
$inventory_no_itemName_error_msg="Please indicate the name of the item";
$inv_itemForm_description_label="Item description";
$inventory_add_item="Add item";
$inventory_stock_bal="Stock balance";
$inventory_item_commercial_name="Trade name";
$inventory_remove_item="Remove item";
$inventory_item_medical_name="Generic name";
$inventoy_problem_inserting_begining_quantities="There was a problem indicating the beginning quantities";
$inventory_medical_name_query_error_msg="System Error! Unable to search for generic names. Please retry";
$inventory_reorder_level="Reorder level";
$inventory_expiry_date="Expiry date";
$inventory_incomplete_field_msg="You are leaving out a required field - none of Trade name, Generic name or Categories can be left uncompleted";
//-----create new transfer variables------
$inv_transferForm_create_prompt="Document inventory transfer";
$inv_transferForm_Source_label="Source location";
$inventory_transfers="Transfers";
$inventory_stock_receipt_acknowledged="Stock receipt acknowledged";
$inventory_transfer_details_receipt_failure_msg="Items quantity receipt details could not be recorded";
$inventory_batch_record_success="Batch information successfully recorded";
$inventory_receipt_success_msg="Items quantities details successfully recorded";
$inventory_new_transfer="New transfer";
$inventory_batch_receipt_record_failure_msg="Unable to record batch records";
$inventory_batch_receipt_record_success_msg="Batch receipt successfully recorded";
$inventory_batch_expiry="Batch no.(Expiry date)";
$inventory_move="Move";
$inventory_action="Action";
$inventory_receive="Receive";
$inventory_click_to_edit="Click to edit";
$inventory_click_to_receive="Click to receive";
$inventory_edit="Edit";
$inventory_invalid_transfer_qty_alert="Invalid transfer quantity. Please change the figure";
$inventory_transfer_date="Transfer date";
$inventory_transfer_details_record_failure_msg="System Error:Unable to record the details of the transfer. please retry";
$inventory_edit_transfer="Edit transfer";
$inventory_withdraw="Withdraw";
$inventory_withdraw_transfer="Withdraw transfer";
$inventory_transfer_details_success_msg="Transfer details successfully recorded";
$inventory_receive_transfer="Receive transfer";
$inventory_transfer_details_db_error_msg="System Error! The details of this transfer has not been stored. Please retry";
$inventory_transfer_details_success_msg="Transfer details successfully stored";
$inventory_transfer_initiate_success_msg="Transfer process successfully initialized";
$inv_transferForm_destination_label="Destination location";
$inventory_transfer_qty="Transfer quantity";
$inventory_sent_by="Sent by";
$inventory_received_by="Received by";
$inventory_status="Status";
$inventory_view_transfer_details="View transfer details";
$inventory_target_location="Target location";
//---create new purchase order variables -----
$inv_purchaseOrderForm_create_prompt="Create new purchase order";
$inv_purchaseOrderForm_Source_label="Purchase order for";
//----create new presentation variables----
$inventory_presentations="Presentations";
$inv_presentationForm_create_prompt="Create new presentation";
$inv_presentationForm_name_label="Presentation name";
$inv_presentation_description_label="Description";
$inventory_presentation_delete_success_msg="Presentation successfully removed";
$inventory_presentation_delete_error_msg="System failure! Unable to remove presentation. Please retry!";
$inventory_presentation_describe_the_presentation="Describe the presentation";
$inventory_presentation_update_failure_message="System failure! Unable to update presentation. Please retry!";
$inventory_presentation_successfully_updated="Presentation information successfully updated";
$inventory_no_presentationName_error_msg="Please, indicate the name of the presentation";
$inventory_presentation_dbconnnect_error="System Error! Unable to find inventory presentation. Please retry.";
$inventory_no_presentation_msg="No item presentations found!!!";
$inventory_add_new_presentation="Add new presentation";
$inventoryPresentationCreationSuccessNews="New presentation successfully created";
$inv_presentationForm_update_prompt="Update presentation";
$inventoryEditingpresentation="Editing inventory presentation";
//------create new item form variables -----
$inv_ItemForm_create_prompt="Create new form";
$inv_itemName_label="Name of form";
$inv_newFormDescription_label="Form description";
$inventory_no_formName_error_msg="Please indicate the name of the new form you are creating";
$inventory_form_successfully_updated="Form successfully updated";
$inventoryFormCreationFailureNews="System Error! Unable to create form this time. Please retry";
$inventoryFormCreationSuccessNews="Inventory item form successfully created";
$inventory_forms="Inventory forms";
$inventory_dbconnnect_error="System failure! Unable to perform the requested operation. Please retry!";
$inventory_balance="Balance";
$inventory_no_form_msg="No such form found";
$inventory_form_delete_success_msg="Form successfully removed";
$inventory_form_delete_error_msg="Unable to remove form at this time. Please retry";
$inventory_form_delete_success_msg="Form successfully removed";
$inv_formsForm_name_label="Form name";
$inventoryEditingForm="Editing inventory form";
$inv_formForm_description_label="Form description";
$inventory_form_describe_the_form="Describe the form";
$inventory_click_to_remove_form="Click to remove this form";
$inventory_add_new_form="Add new form";
//---create new item strength variables----
$inv_itemStrength_create_prompt="Create new item strength";
$inv_itemstrength_label="Strength name";

//------inventory item units variables-----
$inventoryUnitCreationSuccessNews="Item unit successfully created";
$inventoryUnitCreationFailureNews="System Error: Unable to create item unit this time. Please retry";
$inventory_unit="Unit";
$inventory_stock="Stock";
$inventory_tranferQty="Transfer Qty";
$inventory_qty="Qty";
$inventory_units="Inventory units";
$inventory_unit_add="Add inventory unit";
$inv_unitForm_name_label="Unit name";
$inv_unitForm_parent_label="Parent unit";
$inventory_no_unitName_error_msg="Please indicate the name of the unit you are creating";
$inventory_unit_successfully_updated="Unit successfully updated";
$inventory_unit_update_failure_message="System Error: Unable to update unit at this time. Please retry";
$inventory_editingUnit="Editing inventory item unit";
$inventory_unit="Unit";
$inventory_item_units="Item units";
$inventory_add_unit="Add unit";
$inventory_no_unit_msg="No unit defined yet";
$inventory_error_getting_existing_units="System Error getting existing units.";
//---inventory medical names variables----
$inventory_click_to_remove_medicalName="Click here to remove this generic name";
$inventory_add_new_medicalName="Add new generic name";
$inv_medicalNameForm_name_label="Generic name";
$inventory_medicalNames="Generic names";
$inventory_medical_name_successfully_updated="Generic name successfully updated";
$inventory_medical_name_update_failure_message="System Error: Unable to update generic name right now. Please retry";
$inventoryEditingMedicalName="Editing  generic name";
$inv_medicalForm_name_label="Generic name";
$inventoryMedicalNameCreationSuccessNews="GEneric name successfully created";
$inv_medicalNameForm_createLabel_prompt="Add new generic name";
$inv_medicalNameForm_name_label="Generic name";
$inventory_new_medical_name="New generic name";
$inventory_no_medical_name_msg="No existing generic names found";
$inventory_medical_name_dbconnnect_error="System Error: Unable to search for generic names right now. Please retry";
$inventory_medical_name="Generic name";
$inventory_no_medicalName_error_msg="Please indicate the generic name you want to create";
$inventory_medical_name_delete_success_msg="Generic name successfully removed";
//---------------- dispense items -------------------------
$inventory_dispense_item="Dispense item";

$inventory_setup="Setup";
$inventory_no_parent="No parent";
$none_in_existence="None in existence";
$inventory_click_to_see_details="Click to see details";

//-------Restock terms-------
$inventory_received_stock_items="Received stock items";
$inventory_new_stock_form="New stock form";
$inventory_receivingQty="Receiving Qty";
$inventory_receiver="Receiver";
$inventory_supplyDate="Supply Date";
$unknown="Unknown parameter";
$inventory_view="View";
$inventory_supply_details="Supply details";
$inventory_unrecognized_supply_id="Unrecognized supply number";
$inventory_supplied_item="Supplied Item";
$inventory_restock_success = "Restock was successful";
$inventory_restock_failure = "Restock failed";

$inventory_stock_avail_bal="Available";
$inventory_stock_in_transit="In transit";

/**********BEGIN INVENTORY STUFFS ADDED BY TUNDE***********/
$inventory_item_unit_name = "item unit name";
$inventory_item_unit_desc = "item unit description";
$inventory_item_unit_submit_button = "add item unit";
$inventory_item_unit_name_label = "UNIT";
$inventory_item_unit_desc_label = "DESCRIPTION";
$inventory_item_unit_enabled_label = "ENABLED";
$inventory_item_unit_edit_label = "Edit";
$inventory_item_unit_no_units = "No units have been added yet.";
$inventory_item_unit_unknown_unit = "Unknown item unit.";

$inventory_item_info_label = "Item Information";
$inventory_item_category_label = "Item Category";
$inventory_item_unit_label = "Item Unit";
$inventory_item_next_label = "Next";
$inventory_item_prev_label = "Previous";
/**********END INVENTORY STUFFS ADDED BY TUNDE***********/

$inventory_edit_item="Edit item";
$inventory_unknown="Unknown";
$inventory_item_balance_label="Stock balances";
$inventory_manage_reorder_level="Manage item reorder level";
$inventory_reorder_level_action_prompt="Please indicate the re-order level for this location for the following item:";
$inventory_reorder_level_successfully_updated="Reorder level successfully set";
$inventory_categories_successfully_updated="<br />Categories successfully updated!";
$inventory_unable_to_update_item_basic_info="<br />System Error! Unable to update the item's basic information";
$inventory_unable_to_update_item_extra_info="<br />System Error! Unable to update item extra information";
$inventory_item_successfully_updated="<br />Item successfully updated!";
$inventory_unit_quantities_successfully_updated="<br />Unit quantites successfully updated";
$inventory_no_unit_qty_selected="No unit quantity selected";
$inventory_not_successfully_updated="not successfully updated";

/***    END: Pharmacy Inventory    ***/









?>
