<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="menu/support.css" rel="stylesheet" type="text/css">
<link href="menu/main.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery/jquery.js"></script>
<script type="text/javascript" src="js/jquery/jquery.dropdown.js"></script>
</head>

<body>
<ul class="dropdown dropdown-horizontal" name="nav">
      <li><a href="#">Home</a></li>
<li><span class="dir">Modules</span>
          <ul>
            <li class="first"><a href="#" class="dir">System Administration</a>
                <ul>
                  <li><a href="#">Manage Drug Category</a></li>
				  				<li><a href="#">Manage Drug Sub Category</a></li>
								<li><a href="#">Manage Drugs</a></li>
								<li><a href="#">Manage Service Items</a></li>
								<li><a href="#">Manage Service Fees</a></li>
								<li><a href="#">Manage Drug Priority</a></li>
								<li><a href="#">Manage wards</a></li>
		        </ul>
            </li>
            <li class="first"><a href="#" class="dir">Patient Care</a>
                <ul>
                  <li><a href="#">Patient Registration</a></li>
                  <li><a href="#">Treatment</a></li>
                  <li><a href="#">View Appointments</a></li>
				  <li><a href="#">Patient Admission</a></li>
                </ul>
            </li>
            <li><a href="index.php?p=reports&m=reports" class="dir">Reports</a>
                <ul>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
                  <li><a href="#">Drugs</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Billing</a>
                <ul>
                  <li><a href="index.php?p=billing">Billing</a></li>
                </ul>
            </li>
             <li><a href="#" class="dir">Pharmacy</a>
                <ul>
                  <li><a href="index.php?p=pharmacybill">Drug Dispensing</a></li>
                </ul>
            </li>
		<li><a href="#" class="dir">Laboratory</a>
                <ul>
                   <li><a href="index.php?p=labtestbill">Laboratory Services</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Account/Audit/Finance</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Human Resources</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Tender</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Message Centre</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Fixed Asset Manager</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Document Manager</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
        </ul>
      </li>
      <li ><a href="#" class="dir">Support</a>
          <ul>
            <li><a href="#">Link 1</a></li>
          </ul>
      </li>
      <li ><a href="#" class="dir">Alerts</a>
          <ul>
            <li><a href="#">News 1</a></li>
          </ul>
      </li>
      </li>
  </ul>
</body>
</html>