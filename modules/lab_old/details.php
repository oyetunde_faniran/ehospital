<script type="text/javascript" src="library/lab/checks.js"></script>
<?php
require_once("././library/lab/checks.php");
$labdb=new DBConf(); //initialize a dbconnection object
$maindbname=$labdb->dbname;
$labdbname=$labdb->labdbname;
$labdb->labConnect();//point to the lab db
$check=new checks();//retrieve all post vars
?>

<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
<script type="text/javascript">
$(function() {
    $("#accordion").accordion({
								  collapsible:true,
								  autoHeight: false, 
								  <?php
								  if (isset($resultsubmitsuccess) || isset($resultvalidatesuccess)){echo "active: 1,";} 
								  else {echo "active: 0,";} ?>
								  clearStyle: true
							  });
});
</script>


<!-- --------------------------------------------------------------------------------Test Details-->

<div id="accordion">
<h3 class="lab-section-head" style="color:#003366; font-size:13px; font-style:normal; padding:7px;"><span style="padding-left:1.6em;"><?php echo $lab_detail_view; ?></span></h3>
<div class="tabContent labdiv" id="tabs-1" style="padding-top:1.2em;">
<?php
			$userquery="select lab_trans.lab_trans_id as the_lab_trans_id, lab_trans.lab_pin as thelabpin, lab_no, comments, staff_employee_id, DATE_FORMAT(request_date,'%D %b %Y') as thedate, DATE_FORMAT(request_date,'%l:%i %p') as thetime, status, lab_trans.reg_hospital_no as thehospitalno, result_comments
					FROM lab_trans
					WHERE lab_trans.lab_trans_id='{$_REQUEST['transid']}'";

			$searchres=$labdb->execute($userquery);
			$details=mysql_fetch_array($searchres);
?>
<p><?php echo $lab_result_labno; ?>: <?php echo "<span>".$details['lab_no']."</span>"; ?></p>
<?php
//get Test owner's name
		  //fetch name from lab registry (Out-patient)
		  $userquery="select concat(othernames,' ',surname) as thename FROM lab_registry WHERE lab_registry.lab_pin='{$details['thelabpin']}'";
		  $namesearchres=$labdb->execute($userquery);
		  $titles['thename']=mysql_result($namesearchres,0,'thename');
		  if(empty($titles['thename']) || is_null($titles['thename']))
		  {//in-patient, so get name from main hospital registry
		  $userquery="select concat(reg_othernames,' ',reg_surname) as thename FROM mediluth_skye.registry WHERE reg_hospital_no='{$details['thehospitalno']}'";
		  $namesearchresult=$labdb->execute($userquery);
		  $test=mysql_fetch_array($namesearchresult);//mysql_result($namesearchresult,0, 'thename');
		  $titles['thename']=$test['thename'];
		  }
?>
<p><?php echo $lab_result_labpatient; ?>: <?php echo "<span>".$titles['thename']."</span>"; ?></p>
<p><?php echo $lab_result_requeststaff; ?>: 
<?php
//Get Requesting Staff info
$query="SELECT CONCAT(staff_title,' ',staff_othernames) as staffname FROM $maindbname.staff WHERE staff_id='{$details['staff_employee_id']}'";
$staffquery=mysql_query($query);
if(mysql_num_rows($staffquery)==1){$staff=mysql_fetch_array($staffquery);} else {$staff['staffname']="Unknown";}
echo "<span>".$staff['staffname']."</span>";
?>
</p>
<p><?php echo $lab_result_requestdate; ?>: <?php echo "<span>".$details['thedate'].". ".$details['thetime']."</span>"; ?></p>
<p><?php echo $lab_result_labtest; ?>:
<?php
//Get test name
$testquery="select DISTINCT lang1 as lab_test_name from analytes, lab_results, lab_test, mediluth_skye.language_content l, lab_dept where l.langcont_id=lab_test.langcont_id AND lab_trans_id={$_REQUEST['transid']} AND lab_results.analyte_id=analytes.analyte_id AND lab_test.lab_test_id=analytes.lab_test_id";
$testsearchres=$labdb->execute($testquery);
if(mysql_num_rows($testsearchres)>0){$titles['thetest']=mysql_result($testsearchres,0,'lab_test_name');} else {$titles['thetest']="Unknown";}
echo "<span>{$titles['thetest']}</span>";
?>
</p>
<p><?php echo $lab_result_sample; ?>:
<?php
$specimenquery=mysql_query("SELECT specimen_id, other from lab_trans_specimen WHERE lab_trans_id='{$_REQUEST['transid']}'");
$specimenlist=array();
if(mysql_num_rows($specimenquery)!=0){
	while ($specimens=mysql_fetch_array($specimenquery)){
		if(!empty($specimens['other']) OR !is_null($specimens['other']))//the null test is included cos mysql NULL cannot be tested with empty()
			{$specimenlist[]=$specimens['other'];}
		else{//retrieve specimen from specimen table
			$thequery=mysql_query("SELECT specimen from lab_trans_specimen, specimen WHERE lab_trans_id='{$_REQUEST['transid']}' AND specimen.specimen_id=lab_trans_specimen.specimen_id AND lab_trans_specimen.specimen_id='{$specimens['specimen_id']}'");
			$specimenlist[]=mysql_result($thequery,0,'specimen');
		}//retrieve specimen from specimen table
	}//while
}
$cmspecimens=implode(",",$specimenlist);
echo "<span>".$cmspecimens."</span>";
?>
</p>
<?php if(!empty($details['comments'])){echo "<p>".$lab_result_comments; ?>: <?php echo "<span>".$details['comments']."</span></p>";} ?>
<p><?php echo $lab_result_status; ?>: <?php echo "<span>".strtoupper($details['status'])."</span>"; ?></p>
</div>
<!-- --------------------------------------------------------------------------------Test Details Ends-->

<h3 class="lab-section-head" style="color:#003366; font-size:13px; font-style:normal; padding:7px;"><span style="padding-left:1.6em;"><?php if(isset($modrecordfound)){echo $mod_rec_header;} else{echo $lab_result_view;} ?></span></h3>
<div  class="tabContent  labdiv" style="padding-top:2em; display: <?php if((isset($modsuccess) && $modsuccess==true) || (isset($regsuccess) && $regsuccess==true)){echo "none";} else {echo "block";} ?>" id="regdiv">
<?php //Form submissiion responses display
if(isset($resultsubmitsuccess)){
	if($resultsubmitsuccess==true){$color="color:blue;";} else {$color="color:red";}
	echo "<ul style=\"$color\">";
	if($resultsubmitsuccess==true){echo "<strong>$lab_result_positive</strong>";}
	else{//request has failed
		echo "<strong>$lab_result_negative</strong>";
		if($error_test){echo "<li> </li>";}
		}
	echo "</ul>";
}
if(isset($resultvalidatesuccess)){
	if($resultvalidatesuccess==true){$color="color:blue;";} else {$color="color:red";}
	echo "<ul style=\"$color\">";
	if($resultvalidatesuccess==true){echo "<strong>$lab_validate_positive</strong>";}
	else{//request has failed
		echo "<strong>$lab_validate_negative</strong>";
		if($error_test){echo "<li> </li>";}
		}
	echo "</ul>";
}
?>
<form action="index.php?p=details&m=lab&transid=<?php echo $_REQUEST['transid']; ?>" method="post" onsubmit="return checkResult();">
<?php
	echo "<p style=\"margin-bottom:10px; margin-left:20px; margin-right: 20px; border:1px solid #CCC; width:640px; padding:10px; text-align:center;\">$lab_result_comments<br />";
	echo "<textarea name=\"result_comments\" rows=\"5\" style=\"width:100%;\">";
	if(!empty($details['result_comments'])){
	echo $details['result_comments'];
	}
	echo "</textarea>";
	echo "</p>";

$analytes=mysql_query("SELECT lab_results.analyte_id as analyteid, analyte, result FROM analytes,lab_results WHERE lab_results.lab_trans_id='{$_REQUEST['transid']}' AND analytes.analyte_id=lab_results.analyte_id;");
while($analytes_results=mysql_fetch_array($analytes)){
	echo "<p style=\"margin-bottom:10px; margin-left:20px; border:1px solid #CCC; width:300px; float:left; padding:10px;\">{$analytes_results['analyte']}<br />";
	if(strtoupper($details['status'])=="VALIDATED"){//Already been validated, so disable input
		echo "<textarea name=\"{$analytes_results['analyteid']}\" rows=\"5\" style=\"width:100%;\" readonly=\"readonly\">";}
	else{//not validated yet
		echo "<textarea name=\"{$analytes_results['analyteid']}\" rows=\"5\" style=\"width:100%;\">";}
	if(!empty($analytes_results['result'])){
	echo $analytes_results['result'];
	}
	echo "</textarea>";
	echo "</p>";
}
?>
<p style="clear:both; border-top:1px solid #999; margin-top:30px; padding-top:10px; ">

<?php
if(strtoupper($details['status'])!="VALIDATED"){//not yet validated
	echo "<input type=\"submit\" value=\"$lab_result_save\" class=\"btn\" name=\"submitresult\" />";
	}

//determine if user is consultant. If not, hide the validate button
$desgnquery=mysql_query("SELECT DISTINCT group_id FROM {$maindbname}.users WHERE user_id='$labstaff'") or die(mysql_error());
$desgn=mysql_result($desgnquery,0);
if($desgn==$consultantdesignation && $details['status']!="validated"){
	echo "<input type=\"submit\" value=\"$lab_result_validate\" class=\"btn\" name=\"validateresult\" style=\"position:relative; left:15px;\" />";
	}
?>
<input type="button" value="<?php echo $lab_result_cancel; ?>" class="btn" name="result_cancel" id="result_cancel" style="float:right;" onclick="onClick=history.go(-1)">
</p>
</form>
</div>

</div> <!-- accordion -->