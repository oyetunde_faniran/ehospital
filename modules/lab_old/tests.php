<script type="text/javascript" src="library/lab/checks.js"></script>
<?php
require_once("././library/lab/checks.php");
$labdb=new DBConf(); //initialize a dbconnection object
$maindbname=$labdb->dbname;
$labdbname=$labdb->labdbname;
$labdb->labConnect();//point to the lab db
$check=new checks();//retrieve all post vars
?>
<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
<script type="text/javascript">
$(function() {
    $("#accordion").accordion({
								  <?php if (isset($_POST['lab_test_id']) OR isset($requestsuccess) OR (isset($section) && $section="request")) {echo "active: 1,";} else {echo "active: 0,";} ?>
								  autoHeight: false,
								  clearStyle: true,
								  collapsible: true,
							  });
});

</script>
<a href="<?php echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."&section=request";  ?>#testreg" style="text-decoration:none;"><strong><?php echo $lab_jumptorequest; ?></strong></a>
<div id="accordion" style="margin-top:10px;">
<h3 class="lab-section-head" style="color:#003366; font-size:13px; font-style:normal; padding:7px;"><span style="padding-left:1.6em;"><?php echo $lab_test_search ?></span></h3>
<div class="tabContent labdiv" id="tabs-1" style="padding-top:1.2em; margin-bottom:20px;">

<form action="index.php?p=tests&m=lab" method="post" onsubmit="return searchTestCheck();">
  <table width="98%" border="0" class="lab-form-table"> 
<tr>
	<td width="7%">
    <?php echo $lab_having; ?> 
    
    </td>
    <td width="93%">
    <select name="test_search_option"> 
	<option <?php if(isset($_POST['test_search_option']) && $_POST['test_search_option']=="lab_no"){echo "selected=\"selected\"";} ?> value="lab_no"><?php echo $search_option_labno; ?></option>
	<option <?php if(isset($_POST['test_search_option']) && $_POST['test_search_option']=="lab_pin"){echo "selected=\"selected\"";} ?> value="lab_pin"><?php echo $search_option_labpin; ?></option>
	<option  <?php if(isset($_POST['test_search_option']) && $_POST['test_search_option']=="lab_user"){echo "selected=\"selected\"";} ?>value="lab_user"><?php echo $search_option_name; ?></option>
    <option  <?php if(isset($_POST['test_search_option']) && $_POST['test_search_option']=="reg_hospital_no"){echo "selected=\"selected\"";} ?>value="reg_hospital_no"><?php echo $search_option_reg_hospital_no; ?></option>
    </select>
    
    <input name="search_lab_test_text" type="text" id="search_lab_test_text" value="<?php if(isset($_POST['search_lab_test_text'])){echo $search_lab_test_text;} ?>" />
    <span class="error"><?php if(isset($error_lab_pin) && $error_lab_pin==true) {echo "$nosearchpara";} ?></span></td>
 
  </tr> 

  <tr>
	<td width="7%">&nbsp;</td>
    <td width="93%"><input name="checked" id="checked" type="hidden" value="false" />
    <input class="btn" name="labtestsearch" id="labtestsearch" type="submit" value="<?php echo "$labtestsearchbutton"; ?>" /> </td>
  </tr>
</table>
</form>

<?php
	if (!isset($searchres)){// no search yet, so get default listing
			if(!isset($status)){$status="pending";}
			$maindbregistry="$maindbname.registry";
			$labdbregistry="$labdbname.lab_registry";
			$userquery="select lab_trans.lab_trans_id as the_lab_trans_id, lab_trans.lab_pin as thelabpin, lab_no, DATE_FORMAT(request_date,'%D %b %Y') as thedate, DATE_FORMAT(request_date,'%l:%i %p') as thetime, status, lab_trans.reg_hospital_no as thehospitalno
					FROM lab_trans
					WHERE status='{$status}' ORDER BY the_lab_trans_id DESC";
		
			$searchres=$labdb->execute($userquery);
			$searchcaption="(".mysql_num_rows($searchres).").";
	}
?>
  <table border="0" class="display-set" style="width:100%;">

    <caption style="background-color:#CCF; padding:7px;">
    <?php echo $lab_displaying; ?>
	<form action="index.php?p=tests&m=lab" method="post" style="display:inline;" name="labtestlistform" id="labtestlistform">

	<?php if (!isset($_POST['labtestsearch'])){//no search yet, so display SELECT BOX   ?>
	<select name="status" onchange="document.getElementById('labtestlistform').submit();"> 
	<option <?php if(isset($_POST['status']) && $_POST['status']=="pending"){echo "selected=\"selected\"";} ?> value="pending"><?php echo $search_option_pending; ?></option>
	<option  <?php if(isset($_POST['status']) && $_POST['status']=="preliminary"){echo "selected=\"selected\"";} ?>value="preliminary"><?php echo $search_option_preliminary; ?></option>
   	<option  <?php if(isset($_POST['status']) && $_POST['status']=="validated"){echo "selected=\"selected\"";} ?>value="validated"><?php echo $search_option_validated; ?></option>
    </select>
    <?php } ?>

    <?php echo $lab_test_records; ?>

    <span><?php if(isset($searchcaption)){echo $searchcaption;}
		?>
        <noscript><input class="btn" name="labtestlist" id="labtestlist" type="submit" value="<?php echo "$labtestlisttext"; ?>" /></noscript>
    </span>
    
    </form>
    </caption>
  <thead>
  <tr class="title-row">
  <th width="12%"><?php echo $lab_test_labno; ?></th>
  <th width="8%"><?php echo $lab_test_labpin; ?></th>
  <th width="24%"><?php echo $lab_test_labtest; ?></th>
  <th width="24%"><?php echo $lab_test_labname; ?></th>    
  <th width="12%"><?php echo $lab_test_labdate; ?></th>    
  <th width="10%"><?php echo $lab_test_labstatus; ?></th>    
  <th width="10%"></th>
  </tr>
 </thead>

<?php

	  $i=0;
	  
	  while($titles=mysql_fetch_array($searchres,MYSQL_ASSOC)){
      $titles=$check->removeslashes($titles);
	  if ($i%2==0){$class="class=\"tr-row\"";} else {$class="class=\"tr-row2\"";} $i++; //to handle zebra stripes
      echo "<tr $class>";
      echo "<form method=\"post\" action=\"index.php?p=details&m=lab&transid=".$titles['the_lab_trans_id']."\" >";
	  echo "<td style=\"padding-left: .5em;\">".$titles['lab_no']."</td>";
      echo "<td style=\"padding-left: .5em;\">".$titles['thelabpin']."</td>";
	  		
//Get the test name
$testquery="select DISTINCT analytes.lab_test_id, lab_test.dept_id, lang1 as lab_test_name from analytes, lab_results, lab_test, mediluth_skye.language_content l, lab_dept where l.langcont_id=lab_test.langcont_id AND lab_trans_id={$titles['the_lab_trans_id']} AND lab_results.analyte_id=analytes.analyte_id AND lab_test.lab_test_id=analytes.lab_test_id";

		  $testsearchres=$labdb->execute($testquery);
		  if(mysql_num_rows($testsearchres)>0){$titles['thetest']=mysql_result($testsearchres,0,'lab_test_name');} else {$titles['thetest']="unknown";}
	  echo "<td style=\"padding-left: .5em;\">".$titles['thetest']."</td>";

		  //fetch name from lab registry (Out-patient)
		  $userquery="select concat(othernames,' ',surname) as thename FROM lab_registry WHERE lab_registry.lab_pin='{$titles['thelabpin']}'"; //die ($userquery);
		  $namesearchres=$labdb->execute($userquery);
		  $ds=mysql_fetch_array($namesearchres);
		  $titles['thename']=$ds['thename'];
		  if(empty($titles['thename']))
		  {//in-patient, so get name from main hospital registry
		  $userquery="select concat(reg_othernames,' ',reg_surname) as thename FROM mediluth_skye.registry WHERE reg_hospital_no='{$titles['thehospitalno']}'";
		  $namesearchresult=$labdb->execute($userquery);
		  $test=mysql_fetch_array($namesearchresult);//mysql_result($namesearchresult,0, 'thename');
		  $titles['thename']=$test['thename'];
		  }

	  echo "<td style=\"padding-left: .5em;\">".$titles['thename']."</td>";
      echo "<td style=\"padding-left: .5em;\">".$titles['thedate']."<br />{$titles['thetime']}</td>";
      echo "<td style=\"padding-left: .5em;\">".$titles['status']."</td>";	  
      echo "<td style=\"padding-left: .5em;\">";
      echo "<input name=\"test_details\" id=\"test_details\" type=\"submit\" value=\"$lab_test_details_btn\"";
	  echo "title=\"$lab_test_proceed\" class=\"btn\" />";
     // echo "<input style=\"background-color:#333; color:#FFF;\" name=\"lab_user_delete\" type=\"submit\" value=\"$lab_reg_cmddelete\" title=\"Delete User record\" onclick=\"return confirmDeleteUser();\" />";
      echo "</td>";
	  echo "</form";
	  echo "</tr>";
	  }

	?>
  </table>
</div>

<h3 class="lab-section-head" style="color:#003366; font-size:13px; font-style:normal; padding:7px;" id="testreg"><span style="padding-left:1.6em;"><?php echo $lab_test_home_reg; ?></span></h3>
<div>
<?php //Form submissiion responses display
if(isset($requestsuccess)){
	if($requestsuccess==true){$color="color:blue;";} else {$color="color:red";}
	echo "<ul style=\"$color\">";
	if($requestsuccess==true){echo "<strong>$lab_test_positive : <span style=\"font-size:larger;\" >$lab_no</span>.</strong>";}
	else{//request has failed
		echo "<strong>$lab_test_negative</strong>";
		if($error_test){echo "<li>$lab_test_negative_test</li>";}
		if($bad_lab_pin){echo "<li>$lab_test_negative_pin</li>";}
		if($bad_reg_hospital_no){echo "<li>$lab_test_negative_number</li>";}
		}
	echo "</ul>";
}
?>

    <?php
	$lsdeptquery="select lang1, dept_id from {$maindbname}.language_content, mediluth_lab.lab_dept where
{$maindbname}.language_content.langcont_id = mediluth_lab.lab_dept.langcont_id";
	//$lsdept=new DBConf(true);
	$depts=$labdb->execute($lsdeptquery);

	If (isset($_POST['lab_test_id']) and !empty($_POST['lab_test_id'])){//page was previously submitted to get analytes
		$lab_test_id=$_POST['lab_test_id'];
		//$complete="and dept_id='"."$dept_id'"; //string to complete query
		$displayformrow="";
		$displaygetunits="display:none;";
		}
	else {
		$lab_test_id=0;
		//$complete="";
		$displayformrow="display:none;";
		$displaygetunits="";
		}

?>

  <form action="index.php?p=tests&m=lab" method="post" id="getanalytesform" name="getanalytesform" onsubmit="return testCheck();">
 <table width="100%" border="0" class="lab-form-table">
  <tr>
	<td width="33%" style="text-align:right; margin-right:10px;"><label><?php echo $lab_test_home_instruct; ?></label></td>
    <td width="67%" align="left"><select name="lab_test_id" id="lab_test_id" onchange="return testCheck();">
      <?php 
	   echo "<option value=\"0\">Select a Test</value>";
	   while($deptsrow=mysql_fetch_array($depts, MYSQL_ASSOC)){
		   $dept_id=$deptsrow['dept_id'];
		   $testsquery="select lang1, lab_test_id from {$maindbname}.language_content, mediluth_lab.lab_test where
				{$maindbname}.language_content.langcont_id = mediluth_lab.lab_test.langcont_id AND
				mediluth_lab.lab_test.dept_id='$dept_id'";
			$tests=$labdb->execute($testsquery);
            echo "<optgroup label=\"{$deptsrow['lang1']}\"";
			while($testsrow=mysql_fetch_array($tests, MYSQL_ASSOC)){
					if (isset($_POST['lab_test_id']) AND $lab_test_id==$testsrow['lab_test_id']){$selecteddept="selected=\"selected\"";} else {$selecteddept="";}
					echo "<option value=\"".$testsrow['lab_test_id']."\"$selecteddept>".$testsrow['lang1']."</value>";
					}//end inner while
			echo "</optgroup>";
           }
		  ?>
    </select></td>
  </tr>

 <noscript>
  <tr style="<?php echo $displaygetunits; ?>">
      <td>&nbsp;</td>
      <td align="left"><input class="btn" name="getunits" type="submit" value="<?php echo $lab_tests_home_getunits; ?>" /></td>
  </tr>
</noscript>

<tr style="<?php echo $displayformrow; ?> vertical-align:top;">
<td style="text-align:right; margin-right:10px;"><?php echo $lab_test_lab_pin; ?></td>
<td><input type="text" id="lab_pin" name="lab_pin" /></td>
</tr>
  
  
<?php
//get currently selected test id and use in query to get analytes
$lstitlequery="select specimen_id, specimen from specimen where lab_test_id='$lab_test_id' order by specimen";
$lsspecimens=$labdb->execute($lstitlequery);
if (mysql_num_rows($lsspecimens)<1){$displayspecimen="display:none;";} else {$displayspecimen="";}
?>

<tr style="<?php echo $displayspecimen; ?> vertical-align:top;">
<td style="text-align:right; margin-right:10px;"><?php echo $lab_test_specimen; ?></td>
<td>
  <table width="300px" border="0" class="lab-table" style="<?php echo $displayspecimen; ?>">

<?php
	  $i=0;
	  while($specimentitles=mysql_fetch_array($lsspecimens)){
		  if ($i%2==0){$class="class=\"tr-row\"";} else {$class="class=\"tr-row2\"";} $i++; //to handle zebra stripes
      echo "<tr $class>";
      echo "<td><input type=\"checkbox\" name=\"specimen_{$specimentitles['specimen_id']}\" id=\"specimen_{$specimentitles['specimen_id']}\" value=\"{$specimentitles['specimen_id']}\"  />".$specimentitles['specimen']."</td>";
	  echo "</tr>";
	  }
	?>
 </table>
</td>
</tr>

<tr style="<?php echo $displayformrow; ?> vertical-align:top;">
<td style="text-align:right; margin-right:10px;"><?php echo $lab_test_other_specimen; ?></td>
<td><input type="text" id="other" name="other" style="width:250px" /></td>
</tr>

<tr style="<?php echo $displayformrow; ?> vertical-align:top;">
<td style="text-align:right; margin-right:10px;"><?php echo $lab_test_comments; ?></td>
<td><textarea id="comments" name="comments" rows="5" style="width:250px"></textarea></td>
</tr>

	<?php
//get currently selected test id and use in query to get analytes
$lstitlequery="select analyte_id, analyte from analytes where lab_test_id='$lab_test_id' order by analyte";
$lstitle=$labdb->execute($lstitlequery);
if (mysql_num_rows($lstitle)<1){$displayanalytes="display:none;";} else {$displayanalytes="";}
?>
<tr style="<?php echo $displayanalytes; ?> vertical-align:top;">
<td style="text-align:right; margin-right:10px;"><?php echo $lab_test_analytes; ?></td>
<td>

  <table width="300px" border="0" class="lab-table" style="<?php echo $displayanalytes; ?>">

<?php
	  $i=0;
	  while($titles=mysql_fetch_array($lstitle)){
		  if ($i%2==0){$class="class=\"tr-row\"";} else {$class="class=\"tr-row2\"";} $i++; //to handle zebra stripes
      echo "<tr $class>";
      echo "<td><input type=\"checkbox\" name=\"analyte_{$titles['analyte_id']}\" id=\"analyte_{$titles['analyte_id']}\" value=\"{$titles['analyte_id']}\"  />".$titles['analyte']."</td>";
	  echo "</tr>";
	  }
	?>
 </table>
  
</td>
</tr>

  <tr style="<?php echo $displayformrow; ?> vertical-align:top;">
      <td>&nbsp;</td>
      <td align="left"><input class="btn" name="regrequest" id="regrequest" type="submit" value="<?php echo $lab_test_regrequest; ?>" style="margin-top:20px;" /></td>
  </tr>
</table>
</form>

 </div>
 </div>
