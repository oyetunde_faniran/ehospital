﻿# HeidiSQL Dump 
#
# --------------------------------------------------------
# Host:                         127.0.0.1
# Database:                     mediluth_lab
# Server version:               5.0.45-community-nt-log
# Server OS:                    Win32
# Target compatibility:         ANSI SQL
# HeidiSQL version:             4.0
# Date/time:                    2009-12-07 12:59:56
# --------------------------------------------------------

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ANSI,NO_BACKSLASH_ESCAPES';*/
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;*/


#
# Database structure for database 'mediluth_lab'
#

CREATE DATABASE /*!32312 IF NOT EXISTS*/ "mediluth_lab" /*!40100 DEFAULT CHARACTER SET utf8 */;

USE "mediluth_lab";


#
# Table structure for table 'lab_units'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ "lab_units" (
  "unit_id" int(10) unsigned NOT NULL auto_increment,
  "dept_id" int(10) unsigned default NULL,
  "langcont_id" int(10) unsigned default NULL,
  PRIMARY KEY  ("unit_id")
);



#
# Dumping data for table 'lab_units'
#

# No data found.

/*!40101 SET SQL_MODE=@OLD_SQL_MODE;*/
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;*/
