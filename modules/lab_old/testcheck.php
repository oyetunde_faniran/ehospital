<?php
$labdb=new DBConf(); //initialize a dbconnection object
$maindbname=$labdb->dbname;
$labdbname=$labdb->labdbname;
$labdb->labConnect();//point to the lab db
require_once("././library/lab/checks.php");
$check=new checks();//retrieve all post vars


//Search for user
if(isset($_POST['labtestsearch'])){
	$searchsuccess=true;
	$error_search_param=false;

	if ($checked!="true"){//client side validation wasn't successful
		if(empty($search_lab_test_text)){$error_search_param=true; $searchsuccess=false;}
		}
	if($searchsuccess){
		if($test_search_option=="lab_user"){
			$userquery="select lab_trans.lab_trans_id as the_lab_trans_id, lab_registry.lab_pin as thelabpin, concat(othernames,' ',surname) as thename, lab_no, DATE_FORMAT(request_date,'%D %b %Y') as thedate, DATE_FORMAT(request_date,'%l:%i %p') as thetime, status
					FROM lab_registry, lab_trans
					WHERE (surname LIKE '%$search_lab_test_text%' or othernames LIKE '%$search_lab_test_text%')
					AND lab_registry.lab_pin=lab_trans.lab_pin";
			$searchres=$labdb->execute($userquery);
			if (mysql_num_rows($searchres)==0){//then check main luth registry table since Luth patient' names are not stored in the lab registry
				$maindbregistry="$maindbname.registry";
				$labdbregistry="$labdbname.lab_registry";
				$userquery="select lab_trans.lab_trans_id as the_lab_trans_id, {$labdbname}.lab_trans.lab_pin as thelabpin, concat(reg_othernames,' ',reg_surname) as thename, lab_no, DATE_FORMAT(request_date,'%D %b %Y') as thedate, DATE_FORMAT(request_date,'%l:%i %p') as thetime, status
					FROM {$maindbname}.registry, {$labdbname}.lab_trans
					WHERE (reg_surname LIKE '%$search_lab_test_text%' or reg_othernames LIKE '%$search_lab_test_text%')
					AND {$maindbname}.registry.reg_hospital_no=lab_trans.reg_hospital_no";
					}
		}

			elseif($test_search_option=="reg_hospital_no"){
			$maindbregistry="$maindbname.registry";
			$labdbregistry="$labdbname.lab_registry";
			$userquery="select lab_trans.lab_trans_id as the_lab_trans_id, lab_trans.lab_pin as thelabpin, concat(othernames,' ',surname) as thename, lab_no, DATE_FORMAT(request_date,'%D %b %Y') as thedate, DATE_FORMAT(request_date,'%l:%i %p') as thetime, status
					FROM lab_trans, lab_registry
					WHERE lab_trans.reg_hospital_no='$search_lab_test_text'
					AND lab_trans.lab_pin=lab_registry.lab_pin";

			//proceed to know if name was fetched from lab registry, if not, then (in-patient) go fetch name from main luth registry
				$searchres=$labdb->execute($userquery);
				$returnedrows=mysql_num_rows($searchres);
				$returnedname="";
				if($returnedrows!=0){$returnedname=mysql_result($searchres,0,'thename');}
				if (empty($returnedname)){
					$maindbregistry="$maindbname.registry";
					$labdbregistry="$labdbname.lab_registry";
					//requery with attempt to get name from main registry
					$userquery="select lab_trans.lab_trans_id as the_lab_trans_id, lab_trans.lab_pin as thelabpin, concat(reg_othernames,' ',reg_surname) as thename, lab_no, DATE_FORMAT(request_date,'%D %b %Y') as thedate, DATE_FORMAT(request_date,'%l:%i %p') as thetime, status
					FROM lab_trans, {$maindbregistry}
					WHERE lab_trans.reg_hospital_no='$search_lab_test_text'
					AND lab_trans.reg_hospital_no={$maindbregistry}.reg_hospital_no";
					}
		}
		
		elseif($test_search_option=="lab_no"){
		$userquery="select lab_trans.lab_trans_id as the_lab_trans_id, lab_trans.lab_pin as thelabpin, concat(othernames,' ',surname) as thename, lab_no, DATE_FORMAT(request_date,'%D %b %Y') as thedate, DATE_FORMAT(request_date,'%l:%i %p') as thetime, status
					FROM lab_trans, lab_registry
					WHERE lab_trans.lab_no='$search_lab_test_text'
					AND lab_trans.lab_pin=lab_registry.lab_pin";
				//proceed to know if name was fetched from lab registry, if not, then (in-patient) go fetch name from main luth registry
				$searchres=$labdb->execute($userquery);
				$returnedrows=mysql_num_rows($searchres);
				$returnedname="";
				if($returnedrows!=0){$returnedname=mysql_result($searchres,0,'thename');}
				if (empty($returnedname)){
					$maindbregistry="$maindbname.registry";
					$labdbregistry="$labdbname.lab_registry";
					//requery with attempt to get name from main registry
					$userquery="select lab_trans.lab_pin as thelabpin, concat(reg_othernames,' ',reg_surname) as thename, lab_no, DATE(request_date) as thedate, status
					FROM lab_trans, {$maindbregistry}
					WHERE lab_trans.lab_no='$search_lab_test_text'
					AND lab_trans.reg_hospital_no={$maindbregistry}.reg_hospital_no";
					}
		}
			
		elseif($test_search_option=="lab_pin"){
		$userquery="select lab_trans.lab_trans_id as the_lab_trans_id, lab_trans.lab_pin as thelabpin, concat(othernames,' ',surname) as thename, lab_no, DATE_FORMAT(request_date,'%D %b %Y') as thedate, DATE_FORMAT(request_date,'%l:%i %p') as thetime, status
					FROM lab_trans, lab_registry
					WHERE lab_trans.lab_pin='$search_lab_test_text'
					AND lab_trans.lab_pin=lab_registry.lab_pin";
				//proceed to know if name was fetched from lab registry, if not, then (in-patient) go fetch name from main luth registry
				$searchres=$labdb->execute($userquery);
				$returnedrows=mysql_num_rows($searchres);
				$returnedname="";
				if($returnedrows!=0){$returnedname=mysql_result($searchres,0,'thename');}
				if (empty($returnedname)){
					$maindbregistry="$maindbname.registry";
					$labdbregistry="$labdbname.lab_registry";
					//requery with attempt to get name from main registry
					$userquery="select lab_trans.lab_trans_id as the_lab_trans_id, lab_trans.lab_pin as thelabpin, concat(reg_othernames,' ',reg_surname) as thename, lab_no, DATE(request_date) as thedate, status
					FROM lab_trans, {$maindbregistry}
					WHERE lab_trans.lab_pin='$search_lab_test_text'
					AND lab_trans.reg_hospital_no={$maindbregistry}.reg_hospital_no";
					}
		}
			
		}//if search success
		$searchres=$labdb->execute($userquery);
		if (mysql_num_rows($searchres)==0){$searchcaption=$noresults; $searched=true;} else {$searchcaption="(".mysql_num_rows($searchres).")"; $searched=true;}
	}



// /////////////////////////////////////////////////////////////////////////////////////Test Request placement
if(isset($regrequest) && $regrequest==$lab_test_regrequest){	
	$error_test=false;//Whether test ID was specified 
	$requestsuccess=true;
	$bad_parameters=false;
	$bad_lab_pin=false;
	$bad_reg_hospital_no=false;
	$lab_no="";

	if ($lab_test_id=="0"){$error_test=true; $requestsuccess=false;}//client side validation
	if ($requestsuccess){
		
		//get analytes array
		$analytes=array();
		foreach($_POST as $var => $val){
		if (strstr($var,"analyte")){$analytes[]=$val;}
		}
		
		//get SPEcimen array
		$specimen=array();
		foreach($_POST as $var => $val){
		if (strstr($var,"specimen")){$specimen[]=$val;}
		}		
		
		//submit request and get response
		if (isset($reghospitalno)){$check->sendLabRequest($lab_test_id, "", $reg_hospital_no, $labstaff, $comments, $analytes, $other, $specimen);}//for doctors interface only where lab PIN is not known
		else {$check->sendLabRequest($lab_test_id, $lab_pin, "", $labstaff, $comments, $analytes, $other, $specimen);}
		
		$bad_reg_hospital_no=$check->bad_reg_hospital_no;
		$bad_lab_pin=$check->bad_lab_pin;
		$requestsuccess=!$check->fatalerror;
		$bad_parameters=$check->bad_parameters;
		$lab_no=$check->returned_lab_no;
		}

$returnurl="&error_test=$error_test&requestsuccess=$requestsuccess&bad_parameters=$bad_parameters&bad_lab_pin=$bad_lab_pin&bad_reg_hospital_no=$bad_reg_hospital_no&lab_no=$lab_no";
header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}$returnurl");
exit;
}
?>