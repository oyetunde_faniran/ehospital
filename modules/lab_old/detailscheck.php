<?php
require_once("././library/lab/checks.php");
$labdb=new DBConf(); //initialize a dbconnection object
$maindbname=$labdb->dbname;
$labdbname=$labdb->labdbname;
$labdb->labConnect();//point to the lab db
$check=new checks();//retrieve all post vars

//Submission of Test Results
if(isset($submitresult) && $submitresult==$lab_result_save){	
$error_id=false; $resultsubmitsuccess=true; $error_reg_hospital_no=false;
$transid=$_REQUEST['transid'];
$i=0;
//Get the analyte list 
$analytes=mysql_query("SELECT lab_results.analyte_id as analyteid, analyte, result FROM analytes,lab_results WHERE lab_results.lab_trans_id='{$_REQUEST['transid']}' AND analytes.analyte_id=lab_results.analyte_id;");
while($analytes_results=mysql_fetch_array($analytes)){//update results
	$theanalyte=$analytes_results['analyteid'];
	mysql_query("UPDATE lab_results SET result='{$_POST[$theanalyte]}' WHERE lab_trans_id=$transid AND analyte_id='{$analytes_results['analyteid']}'");
	if(!empty($_POST[$theanalyte])){$i++;}//count non-empty results
}
if($i>0){//ie one or more analyte results were recorded
	mysql_query("UPDATE lab_trans SET status='preliminary', result_by='$labstaff', result_date=curdate(), result_comments='$result_comments' WHERE lab_trans_id=$transid");//update results: change status to preliminary and indicate reporting staff
}
else {
	mysql_query("UPDATE lab_trans SET status='preliminary', result_by='$labstaff', result_date=curdate(), result_comments='$result_comments' WHERE lab_trans_id=$transid");//update results: change status to preliminary and indicate reporting staff
	}
$returnurl="&resultsubmitsuccess=$resultsubmitsuccess";
header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}$returnurl");
exit;
}

//Validation of Test Results
if(isset($validateresult) && $validateresult==$lab_result_validate){	
$error_id=false; $resultvalidatesuccess=true;
$transid=$_REQUEST['transid'];
$i=0;
//Get the analyte list 
$analytes=mysql_query("SELECT lab_results.analyte_id as analyteid, analyte, result FROM analytes,lab_results WHERE lab_results.lab_trans_id='{$_REQUEST['transid']}' AND analytes.analyte_id=lab_results.analyte_id;");
while($analytes_results=mysql_fetch_array($analytes)){//update results for various analytes
	$theanalyte=$analytes_results['analyteid'];
	mysql_query("UPDATE lab_results SET result='{$_POST[$theanalyte]}' WHERE lab_trans_id=$transid AND analyte_id='{$analytes_results['analyteid']}'");
	if(!empty($_POST[$theanalyte])){$i++;}//count non-empty results
}
//Update result metadata in lab_trans also
if($i>0){//ie one or more analyte results were recorded
	mysql_query("UPDATE lab_trans SET status='validated', authorised_by='$labstaff', authorised_date=curdate(), result_comments='$result_comments' WHERE lab_trans_id=$transid");//update results: change status to validated and indicate reporting staff
}
else {
	mysql_query("UPDATE lab_trans SET status='validated', authorised_by='$labstaff', authorised_date=curdate(), result_comments='$result_comments' WHERE lab_trans_id=$transid");//update results: change status to validated and indicate reporting staff
	}
$returnurl="&resultvalidatesuccess=$resultvalidatesuccess";
header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}$returnurl");
exit;
}

?>