<script type="text/javascript" src="library/lab/checks.js"></script>
<?php
require_once("././library/lab/checks.php");
$labdb=new DBConf(); //initialize a dbconnection object
$maindbname=$labdb->dbname;
$labdbname=$labdb->labdbname;
$labdb->labConnect();//point to the lab db
$check=new checks();//retrieve all post vars


//Search for user
if(isset($_POST['userregsearch'])){
	$searchsuccess=true;
	$labispatient=false;//keeps track of whether the lab records is for a luth patient or outpatient
	$checked = isset($checked) ? $checked : "false";
	if ($checked!="true"){//client side validation wasn't successful
		if(empty($search_lab_pin)){$error_lab_pin=true; $searchsuccess=false;}
		}
	if($searchsuccess){
		if($reg_search_option=="lab_user"){
			$userquery="select lab_pin, reg_hospital_no, concat(othernames,' ',surname) as thename, age, sex
					FROM lab_registry
					WHERE surname LIKE '%$search_lab_pin%' or othernames LIKE '%$search_lab_pin%'";
			$searchres=$labdb->execute($userquery);
			if (mysql_num_rows($searchres)==0){//then check main luth registry table since Luth patient' names are not stored in the lab registry
				$maindbregistry="$maindbname.registry";
				$labdbregistry="$labdbname.lab_registry";
				$userquery="SELECT lab_pin, l.reg_hospital_no, concat(reg_othernames,' ',reg_surname) as thename, YEAR(curdate())-YEAR(reg_dob) as age, reg_gender as sex FROM {$labdbregistry} l, {$maindbregistry} p
						WHERE l.reg_hospital_no=p.reg_hospital_no AND (reg_surname LIKE '%$search_lab_pin%' or reg_othernames LIKE '%$search_lab_pin%')";
				$labispatient=true;}
		}
		elseif($reg_search_option=="reg_hospital_no"){
			$maindbregistry="$maindbname.registry";
			$labdbregistry="$labdbname.lab_registry";
			$userquery="SELECT lab_pin, l.reg_hospital_no, concat(reg_othernames,' ',reg_surname) as thename, YEAR(curdate())-YEAR(reg_dob) as age, reg_gender as sex FROM {$labdbregistry} l, {$maindbregistry} p
						WHERE l.reg_hospital_no='$search_lab_pin' AND l.reg_hospital_no=p.reg_hospital_no";
			$labispatient=true;
			}
	
		else{//i.e $reg_search_option is lab_pin
			$userquery="select lab_pin, reg_hospital_no, concat(othernames,' ',surname) as thename, age, sex
				FROM lab_registry
				WHERE $reg_search_option='$search_lab_pin'";
				$searchres=$labdb->execute($userquery);
				$returnedrows=mysql_num_rows($searchres);
				$returnedhospital_no="";
				if($returnedrows!=0){$returnedhospital_no=mysql_result($searchres,0,'reg_hospital_no');}
				if ($returnedrows==0 || !empty($returnedhospital_no)){
					$maindbregistry="$maindbname.registry";
					$labdbregistry="$labdbname.lab_registry";
					$userquery="SELECT lab_pin, l.reg_hospital_no, concat(reg_othernames,' ',reg_surname) as thename, YEAR(curdate())-YEAR(reg_dob) as age, reg_gender as sex FROM {$labdbregistry} l, {$maindbregistry} p
						WHERE l.reg_hospital_no=p.reg_hospital_no AND l.lab_pin='$search_lab_pin'";
					$labispatient=true;}
		}
		$searchres=$labdb->execute($userquery);
		if (mysql_num_rows($searchres)==0){$searchcaption=$noresults;} else {$searchcaption=mysql_num_rows($searchres)." $resultsfound";}
	}
}

//List lab user record selected for modification
if(isset($_POST['lab_user_modify'])){
		$lsmodquery="select lab_pin, reg_hospital_no, surname, othernames, age, staff_employee_id, user_comment,sex
					FROM lab_registry
					WHERE lab_pin='$labpin'";
		$modrec=$labdb->execute($lsmodquery);
		if (mysql_num_rows($modrec)==1){$modrecordfound=true; $modrecord=mysql_fetch_array($modrec, MYSQL_ASSOC); $modrecord=$check->removeslashes($modrecord);}
}

?>

<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
<script type="text/javascript">
$(function() {
    $("#accordion").accordion({
								  collapsible:true,
								  autoHeight: false, 
								  <?php
								  if (isset($modsuccess) || isset($regsuccess)){echo "active: 1,";} 
								  else {echo "active: 0,";} ?>
								  clearStyle: true
							  });
});

$(function() {
    $("#accordion2").accordion({
							   	  collapsible:true,
								  autoHeight: false, 
								  <?php if (isset($modrecordfound)){echo "active: 1,";} else {echo "active: false,";} ?>
								  clearStyle: true
							  });
});

</script>


<!-- --------------------------------------------------------------------------------Search Form-->

<div id="accordion">
<h3 class="lab-section-head" style="color:#003366; font-size:13px; font-style:normal; padding:7px;"><span style="padding-left:1.6em;"><?php echo $lab_reg_search ?></span></h3>
<div class="tabContent labdiv" id="tabs-1" style="padding-top:1.2em;">
<form action="index.php?p=register&m=lab" method="post" onsubmit="return searchCheck();">
  <table width="98%" border="0" class="lab-form-table"> 
<tr>
	<td width="22%"><select name="reg_search_option"> 
	<option <?php if(isset($_POST['reg_search_option']) && $_POST['reg_search_option']=="reg_hospital_no"){echo "selected=\"selected\"";} ?> value="reg_hospital_no"><?php echo $search_option_hospitalno; ?></option>
	<option <?php if(isset($_POST['reg_search_option']) && $_POST['reg_search_option']=="lab_pin"){echo "selected=\"selected\"";} ?> value="lab_pin"><?php echo $search_option_labpin; ?></option>
	<option  <?php if(isset($_POST['reg_search_option']) && $_POST['reg_search_option']=="lab_user"){echo "selected=\"selected\"";} ?>value="lab_user"><?php echo $search_option_name; ?></option>
    </select></td>
    <td width="78%"><input name="search_lab_pin" type="text" id="search_lab_pin" value="<?php if(isset($_POST['search_lab_pin'])){echo $search_lab_pin;} ?>" /><span class="error"><?php if(isset($error_lab_pin) && $error_lab_pin==true) {echo "$nosearchpara";} ?></span></td>
 
  </tr> 

  <tr>
	<td width="22%"></td>
    <td width="78%"><input name="checked" id="checked" type="hidden" value="false" /><input class="btn" name="userregsearch" type="submit" value="<?php echo "$userregsearch"; ?>" /></td>
  </tr>
</table>
</form>

<?php
if (isset($searchcaption) || isset($delsuccess)){$displayresults="table; width:100%;\"> <hr />";} else {$displayresults="none; width:100%;\"> "; }
?>

  <table border="0" class="display-set" style="display:<?php echo $displayresults; ?>
    <caption>
    <span><?php if(isset($searchcaption)){echo $searchcaption;} 
				if(isset($delsuccess)){echo $delsuccess;} 
		?>
    </span></caption>
  <thead>
  <tr class="title-row">
  <th width="16%"><?php echo $lab_reg_label_labpin; ?></th>
  <th width="15%"><?php echo $lab_reg_label_hospital; ?></th>
  <th width="37%"><?php echo $lab_reg_label_name; ?></th>    
  <th width="6%"><?php echo $lab_reg_label_age; ?></th>    
  <th width="5%"><?php echo $lab_reg_label_sex; ?></th>    
  <th width="21%">&nbsp;</th>
  </tr>
 </thead>

<?php
		if (isset($searchres)){
			$i=0;
	  while($titles=mysql_fetch_array($searchres,MYSQL_ASSOC)){
	  $titles=$check->removeslashes($titles);
	  if ($titles['sex']==0){$titles['sex']="Male";} else {$titles['sex']="Female";}
	  if ($i%2==0){$class="class=\"tr-row\"";} else {$class="class=\"tr-row2\"";} $i++; //to handle zebra stripes
      echo "<tr $class>";
      echo "<form method=\"post\" action=\"index.php?p=register&m=lab&labpin=".$titles['lab_pin']."#accordion\" >";
	  echo "<td style=\"padding-left: .5em;\">".$titles['lab_pin']."</td>";
      echo "<td style=\"padding-left: .5em;\">".$titles['reg_hospital_no']."</td>";
      echo "<td style=\"padding-left: .5em;\">".$titles['thename']."</td>";
      echo "<td style=\"padding-left: .5em;\">".$titles['age']."</td>";
      echo "<td style=\"padding-left: .5em;\">".$titles['sex']."</td>";	  
      echo "<td style=\"padding-left: .5em;\">";
      echo "<input name=\"lab_user_modify\" type=\"submit\" value=\"$lab_reg_cmdmodify\"";
	  if($labispatient){echo "class=\"btn\" style=\"display:none;\" />";} else{echo "title=\"Modify User record\" class=\"btn\" />";}
     // echo "<input style=\"background-color:#333; color:#FFF;\" name=\"lab_user_delete\" type=\"submit\" value=\"$lab_reg_cmddelete\" title=\"Delete User record\" onclick=\"return confirmDeleteUser();\" />";
      echo "</td>";
	  echo "</form";
	  echo "</tr>";
	  }
		}
	?>
  </table>
</div>
<!-- --------------------------------------------------------------------------------Search Form Ends-->

<h3 class="lab-section-head" style="color:#003366; font-size:13px; font-style:normal; padding:7px;"><span style="padding-left:1.6em;"><?php if(isset($modrecordfound)){echo $mod_rec_header;} else{echo $lab_reg_header;} ?></span></h3>
<div  class="tabContent  labdiv" style="padding-top:2em; display: <?php if((isset($modsuccess) && $modsuccess==true) || (isset($regsuccess) && $regsuccess==true)){echo "none";} else {echo "block";} ?>" id="regdiv">
<ul style="font-size:14px;">
<?php 
	if(isset($regsuccess) && $regsuccess==true)//Registration success message
		{echo "<strong>".$regsuccessmsg."<span style=\"font-size:larger; text-decoration:underline;\">".$lab_gen_pin."</span></strong>";}
	
	if(isset($modsuccess) && $modsuccess==true)//modification success message
		{echo "<strong>".$modsuccessmsg."</strong>";}
	
	if(isset($error_id) && $error_id==true){echo "<li class=\"error\">$lab_reg_error_id</li>";} 
	if(isset($error_reg_hospital_no) && $error_reg_hospital_no==true){echo "<li class=\"error\">$error_reg_hospital_no_text</li>";} 
	if(isset($error_reg_hospital_exists) && $error_reg_hospital_exists==true){echo "<li class=\"error\">$error_reg_hospital_exists_text</li>";} 	
	?>
</ul>
<form action="index.php?p=register&m=lab" method="post" onsubmit="return registerCheck();">
<div id="accordion2">

<?php if (isset($modrecordfound)){$display_mod_inpatient_record="display:none;";} else {$display_mod_inpatient_record="display:block;";}?>
<h3 class="lab-section-head" style="color:#003366; font-size:13px; font-style:normal; padding:5px; <?php echo $display_mod_inpatient_record; ?>"><span style="padding-left:1.6em;"><?php if(isset($modrecordfound)) {echo $lab_luthpat_modify;} else {echo $lab_luthpat;} ?></span></h3>
<div style="margin-bottom:5px;">
  <table width="100%" border="0" class="lab-form-table"> 

	<input name="lab_pin" id="lab_pin" type="hidden" value="<?php if(isset($modrecordfound) && $modrecordfound==true){echo $modrecord['lab_pin'];} ?>" /></td>

  <tr>
	<td width="22%"><label for="reg_hospital_no"><?php echo $lab_reg_hospital_no; ?></label></td>
    <td width="78%"><input name="reg_hospital_no" id="reg_hospital_no" type="text" value="<?php if(isset($modrecordfound) && $modrecordfound==true){echo $modrecord['reg_hospital_no'];} ?>" /></td>
  </tr>
</table>
  <table width="100%" border="0" class="lab-form-table">
	<tr>
    <input type="hidden" name="staff_employee_id" id="staff_employee_id" value="<?php echo $labstaff; ?>" />
      <td width="22%">&nbsp;</td>
      <td class="lab-buttonrow" width="78%"><input type="hidden" name="checked" id="checked" value="false" /><input name="submit_lab_reg" type="submit" value="<?php if(isset($modrecordfound)){echo $lab_reg_update;} else{echo $lab_reg_submit;} ?>" class="btn" style="margin-right:10px;" /><input name="cancel" type="reset" value="<?php echo $lab_reg_reset; ?>" class="btn" onclick="history.go(<?php if(empty($_POST)) {echo "-1";} else {echo "-2";} ?>);" /></td>
  </tr>
</table>

</div>

<h3 class="lab-section-head" style="color:#003366; font-size:13px; font-style:normal; padding:5px;"><span style="padding-left:1.6em;"><?php if(isset($modrecordfound)){echo $lab_nonluthpat_modify;} else{echo $lab_nonluthpat;} ?></span></h3>
<div>

  <table width="100%" border="0" class="lab-form-table">
  <tr>
    <td width="22%"><label for="surname"><?php echo $lab_reg_surname; ?></label></td>
    <td width="78%">
      <input type="text" name="surname" id="surname" value="<?php if(isset($modrecordfound) && $modrecordfound==true){echo $modrecord['surname'];} ?>" />
    </td>
  </tr>
  <tr>
    <td><label for="othernames"><?php echo $lab_reg_othernames; ?></label></td>
    <td><input type="text" name="othernames" id="othernames" value="<?php if(isset($modrecordfound) && $modrecordfound==true){echo $modrecord['othernames'];} ?>" /></td>
  </tr>
  <tr>
    <td><label for="sex"><?php echo $lab_reg_sex; ?></label></td>
    <td><select name="sex" id="sex">
    	<option value="Male"><?php echo $lab_reg_choose_sex; ?></option>
    	<option <?php if(isset($modrecord['sex']) && $modrecord['sex']=="Male"){ echo "selected=\"selected\"";} echo "value=\"$lab_reg_male\" "; ?>><?php echo $lab_reg_male; ?></option>
       	<option <?php if(isset($modrecord['sex']) && $modrecord['sex']=="Female"){ echo "selected=\"selected\" value=\"$lab_reg_female;\" ";} ?>><?php echo $lab_reg_female; ?></option>
        </select></td>
  </tr>
  <tr>
    <td><label for="age"><?php echo $lab_reg_yob; ?></label></td>
    <td><select>
    <option><?php echo $lab_reg_day; ?></option>
    <?php
    for ($i=1; $i<32; $i++){
		echo "<option>$i</option>";
	}
	?>
    </select>
    <select>
    	<option><?php echo "$lab_reg_month"; ?></option>
    	<option><?php echo $lab_reg_jan; ?></option>
        <option><?php echo $lab_reg_feb; ?></option>
        <option><?php echo $lab_reg_mar; ?></option>
        <option><?php echo $lab_reg_apr; ?></option>
        <option><?php echo $lab_reg_may; ?></option>
        <option><?php echo $lab_reg_jun; ?></option>
        <option><?php echo $lab_reg_jul; ?></option>
        <option><?php echo $lab_reg_aug; ?></option>
        <option><?php echo $lab_reg_sep; ?></option>
        <option><?php echo $lab_reg_oct; ?></option>
        <option><?php echo $lab_reg_nov; ?></option>
        <option><?php echo $lab_reg_dec; ?></option>
    </select>
    <select name="age" id="age">
    <option value="<?php echo date('Y'); ?>"><?php echo $lab_reg_year; ?></option>
    	<?php 
			for($i=date("Y"); $i>=1900; $i--)
				{ if(isset($modrecord['age']) && date("Y")-$modrecord['age']==$i){ echo "<option selected=\"selected\" value=\"$i\">$i</option>";}
					else {echo "<option value=\"$i\">$i</option>";}
				}
		?>
        </select></td>
  </tr>
    <tr>
    <td style="vertical-align:top;"><label for="user_comment"><?php echo $lab_reg_user_comment; ?></label></td>
    <td><textarea name="user_comment" cols="35" rows="3"><?php if(isset($modrecordfound) && $modrecordfound==true){echo $modrecord['user_comment'];} ?></textarea></td>
  </tr>
	</table>
    
  <table width="100%" border="0" class="lab-form-table">
	<tr>
    <input type="hidden" name="staff_employee_id" id="staff_employee_id" value="<?php echo $labstaff; ?>" />
      <td width="22%">&nbsp;</td>
      <td class="lab-buttonrow" width="78%"><input type="hidden" name="checked" id="checked" value="false" /><input name="submit_lab_reg" type="submit" value="<?php if(isset($modrecordfound)){echo $lab_reg_update;} else{echo $lab_reg_submit;} ?>" class="btn" style="margin-right:10px;" /><input name="cancel" type="reset" value="<?php echo $lab_reg_reset; ?>" class="btn" onclick="history.go(<?php if(empty($_POST)) {echo "-1";} else {echo "-2";} ?>);" /></td>
  </tr>
</table>
    </div>
   </div> <!-- accordion2 -->
</form>

</div> <!-- regdiv -->
</div> <!-- accordion -->
