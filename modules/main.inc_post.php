<?php
	if (isset($_POST["username"], $_POST["password"])){
		$user = new admin_user(0, $_POST, $curLangField);
		if ($user->doLogin()){
			//Set Session Variables here
			//die ("<pre>" . print_r($user, true) . "</pre>");
			$_SESSION[session_id() . "luth_loggedin"] 	= true;
			$_SESSION[session_id() . "userID"]			= $user->userID;
			$_SESSION[session_id() . "groupID"]			= $user->groupID;
			$_SESSION[session_id() . "staffID"]			= $user->staffID;
			$_SESSION[session_id() . "deptID"]			= $user->deptID;
			$_SESSION[session_id() . "unitID"]			= $user->unitID;
			$_SESSION[session_id() . "clinicID"]		= $user->clinicID;
			$_SESSION[session_id() . "deptName"]		= $user->getDCUGName($_SESSION[session_id() . "deptID"], "dept");
			$_SESSION[session_id() . "unitName"]		= $user->getDCUGName($_SESSION[session_id() . "unitID"], "unit");
			$_SESSION[session_id() . "clinicName"]		= $user->getDCUGName($_SESSION[session_id() . "clinicID"], "clinic");
			$_SESSION[session_id() . "groupName"]		= $user->getDCUGName($_SESSION[session_id() . "groupID"], "group");
			$_SESSION[session_id() . "nurseID"]			= $user->nurseID;
			$_SESSION[session_id() . "consultantID"]	= $user->consultantID;
			$_SESSION[session_id() . "staffName"]		= $user->staffName;
			$_SESSION[session_id() . "menu"]			= $user->getUserMenu();
			$_SESSION[session_id() . "urlArray"]		= $user->urlArray;
			$_SESSION[session_id() . "bank"]			= $user->bank;
			$_SESSION[session_id() . "bankTellerID"]	= $user->bankTellerID;
            $_SESSION[session_id() . 'chat_username']   = admin_Tools::cleanUp4Chat($user->staffName);

			$langFields	= $user->getCurLangFields();

			$_SESSION[session_id() . "langField"]		= $langFields["lang_field"];
			$_SESSION[session_id() . "langName"]		= $langFields["lang_name"];
			//die ( "<pre>" . print_r($_SESSION, true) . "</pre>");
			$g = new admin_group ($_SESSION[session_id() . "langField"], $_SESSION[session_id() . "groupID"]);
			$dPage = $g->getDefaultGroupPage();
			
			//Get the default page that users in the user group of this current user would be redirected to on logging in
			$dPage = !empty($dPage) ? $dPage : "index.php";
			
			//Get the last page (if available) that a user was on before going idle
			$cont = !empty($_POST["cont"]) ? urldecode($_POST["cont"]) : "index.php";
			
			//If the user was not idle initially idle (That is, just logging in) choose the default page as the redirect page
			$cont = $_SESSION[session_id() . "luth_idle"] ? $cont : $dPage;
			
			//Set a cookie to monitor the idle time of a user.
			//N.B: $idleTime is defined in config.inc.php in the includes folder
			setcookie(session_id() . "eldi", md5(time()), time() + $idleTime);
			$_SESSION[session_id() . "luth_idle"] = false;	//Set the idle status of a user to false
            
            
            //***CHAT STUFF:    Store this log-in session into the users_session table
            $user->loginSession_Store($_SESSION[session_id() . "userID"]);
            
            //***INVENTORY LOCATION STUFF:  Get any assigned locations and set the first one as the current one
            $assignedLocations = $user->getCurrentUserLocations($user->userID, $user->deptID);
            if (count($assignedLocations) > 0){
                $locationIDs = array_keys($assignedLocations);
                $_SESSION[session_id() . "inventory_locationID"] = $locationIDs[0];
                $locs = new inventory_location_class();
                $location = $locs->getLocationProperties($locationIDs[0]);
                if (is_array($location)){
                    $_SESSION[session_id() . "inventory_locationName"] = $location["inventory_location_name"];
                } else {
                    $_SESSION[session_id() . "inventory_locationName"] = "";
                }
            } else {
                $_SESSION[session_id() . "inventory_locationID"] = 0;
                $_SESSION[session_id() . "inventory_locationName"] = "";
            }

			header ("Location: $cont");
		} else $_GET["c"] = "Invalid Username / Password";
		
	}
?>