<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>

<!--	  <h2><?php //echo $home_welcome; ?></h2>-->

<?php
	if (!$_SESSION[session_id() . "luth_loggedin"] || $_SESSION[session_id() . "luth_idle"]){
		echo "<span>$home_welcomestory</span><div>&nbsp;</div>";
		if (isset($_GET["c"]))
			echo "<p class=\"comments\">{$_GET['c']}</p>";
		$disTime = $idleTime / 60;
		$disTime = $disTime >= 1 ? ($disTime . $home_minutes) : ($idleTime . $home_seconds);
		
		//Show idle message if user has been idle for the specified period of time
		if ($_SESSION[session_id() . "luth_idle"])
			echo "<p class=\"comments\">$home_idleMsg $disTime $home_idleMsg2 <a href=\"logout.php\">$home_idleMsg1</a>.</p>";

		$cont = !empty($_GET["next"]) ? $_GET["next"] : "index.php";
		echo "<form action=\"\" method=\"post\" name=\"loginform\" autocomplete=\"off\">
				<table border=\"0\">
				  <tr>
					<th scope=\"row\"><label for=\"username\">$home_username:</label></th>
					<td><input name=\"username\" id=\"username\" type=\"text\" size=\"20\" maxlength=\"30\" /></td>
				  </tr>
				  <tr>
					<th scope=\"row\"><label for=\"password\">$home_password:</label></th>
					<td><input name=\"password\" id=\"password\" type=\"password\" size=\"20\" maxlength=\"30\" /></td>
				  </tr>
				  <tr>
					<th scope=\"row\">&nbsp;</th>
					<td>
						<input name=\"sButton\" class=\"btn\" id=\"sButton\" type=\"submit\" value=\"Log In\" />
						<input name=\"cont\" id=\"cont\" type=\"hidden\" value=\"$cont\" />
					</td>
				  </tr>
				  <tr>
					<th scope=\"row\">&nbsp;</th>
					<td>
						<a href=\"index.php?p=staffregister&m=sysadmin\">$home_register</a>
					</td>
				  </tr>
				</table>
			</form>
			<script language=\"javascript\">
				try{
					document.getElementById(\"username\").focus();
				} catch (e) {}
			</script>";
	} else echo $home_welcomestory2;
?>