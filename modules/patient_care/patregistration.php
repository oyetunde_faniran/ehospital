<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

    // Need the BASE_URL, defined in the config file:
    require_once ('../../includes/config.inc.php');

    // Redirect to the index page:
    $url = BASE_URL . 'index.php';
    header ("Location: $url");
    exit;

} // End of defined() IF.
?>
<h3>
<?php
    echo $department_label . ": ";
    $dept_detail = $select->selectDepartment($reg_dept, $curLangField);
    if(is_array($dept_detail)){
        extract($dept_detail);
        echo $dept_name;
    }
?>
</h3>

<h4>
<?php
    echo $clinic_label . ": ";
    $clinic_detail = $select->selectClinic($reg_clinic, $curLangField);
    if(is_array($clinic_detail)){
        extract($clinic_detail);
        echo $clinic_name;
    }
?>
</h4>



<form action="index.php" method="post" name="register_patient" enctype="multipart/form-data">
    <input name="m" type="hidden" id="m" value="patient_care" />
    <input name="reg_dept" type="hidden" id="reg_dept" value="<?php echo $reg_dept ?>" />
    <input name="reg_clinic" type="hidden" id="reg_clinic" value="<?php echo $reg_clinic ?>" />
    <input name="reg[user_id]" type="hidden" id="user_id" value="<?php echo $myUserid ?>" />
    <input name="temp" type="hidden" id="temp" value="patient_care/upload/" />
    <input name="dest" type="hidden" id="dest" value="patient_care/passport/" />
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1"><?php echo $personal_detail_label ?> I</a></li>
            <li><a href="#tabs-2"><?php echo $personal_detail_label ?> II</a></li>
            <li><a href="#tabs-3"><?php echo $next_of_kin_label ?></a></li>
            <li><a href="#tabs-4"><?php echo $others_label ?></a></li>
            <li style="float:right"><input name="register" type="submit" value="<?php echo $continue_label ?>" /></li>
        </ul>
        <div id="tabs-1">
            <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
                <tr>
                    <td align="left" valign="top"><strong>Old Hospital Number:</strong></td>
                    <td align="left" valign="top"><input type="text" name="reg[hospital_oldno]" id="hospital-number" /></td>
                    <td align="left" valign="top"><strong><?php echo $passport_label ?>:</strong></td>
                    <td align="left" valign="top"><input type="file" name="passport" /></td>
                </tr>

                <tr>
                    <td width="19%" align="left" valign="top"><strong><?php echo $surname_label ?>:</strong></td>
                    <td width="34%" align="left" valign="top">
                        <input required type="text" name="reg[surname]" />
                    </td>
                    <td width="15%" align="left" valign="top"><strong><?php echo $othernames_label ?>:</strong></td>
                    <td width="32%" required align="left" valign="top"><input type="text" name="reg[othernames]" /></td>
                </tr>
                <tr>
                    <td align="left" valign="top"><strong><?php echo $dob_label ?>:</strong></td>
                    <td align="left" valign="top">
                    <input type="text" required name="reg[dob]" id="dob" />		  </td>
                    <td align="left" valign="top"><strong><?php echo $gender_label ?>:</strong></td>
                    <td align="left" valign="top"><select name="reg[gender]">
                            <option value=" " selected="selected"><?php echo $select_label ?> <?php echo $gender_label ?></option>
                            <option value="0"><?php echo $gender_0_label ?></option>
                            <option value="1"><?php echo $gender_1_label ?></option>
                    </select>                 </td>
                </tr>
                <tr>
                    <td align="left" valign="top"><strong><?php echo $phone_label ?>:</strong></td>
                    <td align="left" valign="top"><input required type="text" name="reg[phone]" /></td>
                    <td align="left" valign="top"><strong><?php echo $email_label ?>:</strong></td>
                    <td align="left" valign="top"><input type="text" name="reg[email]" /></td>
                </tr>
                <tr>
                    <td align="left" valign="top"><strong><?php echo $address_label ?>:</strong></td>
                    <td colspan="3" align="left" valign="top"><textarea required name="reg[address]" cols="20" rows="5"></textarea></td>
                </tr>
            </table>
        </div>

<?php
    include_once("modules/$m/patreg_others_inc.php");
?>

        <input name="p" type="hidden" id="p" value="insert_registry" />


        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top">&nbsp;</td>
            </tr>
        </table>
    </div>
</form>

<script type="text/javascript" src="library/doubleSelect.1.2/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/doubleSelect.1.2/jquery.doubleSelect.min.js"></script>
<script type="text/JavaScript">

    function confirmHospNo(hospNo, disForm){
        try{
            hospNo = document.getElementById(hospNo).value;
            if (hospNo == "")
                alert ("Please, enter the hospital number of the patient to be registered.");
            else disForm.submit();
        } catch (e) {}
    }

    function submitForm(disForm){
        try{
            disForm.submit();
        } catch (e) {}
    }

    $(document).ready(function()
    {
        var selectoptions = {
<?php
    $select->selectJqueryNationalityAndState($outside_nigeria_label);
?>
        };

        var options = {
            preselectFirst :158,
            preselectSecond : 0
        };
        $('#first').doubleSelect('second', selectoptions, options);
    });
</script>
<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.sortable.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.tabs.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
<script type="text/javascript">
    $(function() {
        $("#tabs").tabs().find(".ui-tabs-nav");
    });

    $(function() {
        $('#dob').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            yearRange: "<?php echo (date('Y') - 150) . ':' . date('Y'); ?>"
        });
    });

    $(function() {
        $("#accordion").accordion({
            collapsible: true,
            active: false,
            clearStyle: true
        });
    });

</script>

