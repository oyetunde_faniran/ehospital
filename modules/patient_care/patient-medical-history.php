<?php
$patient_id = isset($_GET['h']) ? $_GET['h'] : '';
$treat = new patTreatment();
$pat = new Patient();
$pat_info = $pat->getFullPatientInfo($patient_id);
//die('<pre>' . print_r($pat_info, true));
//[reg_surname] => ASDF
//[reg_othernames] => Qwerty

if(!empty($pat_info)){
    echo "<h2>PATIENT NAME: <em>{$pat_info['reg_othernames']} {$pat_info['reg_surname']}</em></h2>
          <h2>PATIENT NUMBER: <em>{$pat_info['reg_hospital_no']}</em></h2>";
    $visits = $treat->getOtherAppointment($pat_info['reg_hospital_no']);
    if ($visits != "") {
        ?>
        <table width="100%" border="1" cellpadding="5" cellspacing="0" style="border: 1px solid #AAA; border-collapse: collapse; margin-top: 10px;">
            <tr style="background: #333; color: #FFF; font-size: 16px; font-weight: bold;">
                <td><strong><?php echo $date_label ?></strong></td>
                <td><strong><?php echo $diagnosis_label ?></strong></td>
                <td><strong><?php echo $prescription_label ?></strong></td>
                <td><strong><?php echo $investigation_label ?></strong></td>
                <td><strong><?php echo $treatment_label ?></strong></td>
                <td>&nbsp;</td>
            </tr>
            <?php
            echo $visits;
            ?>
        </table>
        <?php
    } else {
        echo $no_previous_visits;
    }
} else {
    echo 'Unknown Patient!!!';
}
?>