<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');

	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;

} // End of defined() IF.
	$case_id = isset($_GET["a"]) ? (int)$_GET["a"] : 0;
	$treats = new patTreatment;
	$history = $treats->getHistory($case_id);

	if(is_array($history))
		extract($history);

	$casenote = $treats->getCasenote($case_id);

	if(is_array($casenote))
		extract($casenote);

	$treatment = $treats->selectTreatment($case_id);
	$labtest = $treats->selectLabtest($case_id);
?>




<!--<h3><u><strong>PATIENT'S PERSONAL DETAILS</strong></u></h3>-->







<div id="accordion">
	<h3 style="padding: 10px 10px 10px 25px;">Click to view <strong>TYPED</strong> casenote details</h3>
	<div>
        <div id="tabs" >
        <ul>
              <li><a href="#tabs-1"><?php echo $history_taking_label ?></a></li>
              <li><a href="#tabs-2"><?php echo $examination_label ?></a></li>
              <li><a href="#tabs-4">Others</a></li>
              <li><a href="#tabs-3"><?php echo $plan_label ?></a></li>
          </ul>
              <div  class="tabContent" id="tabs-1">
                <table border="0" cellspacing="1" cellpadding="5" width="100%">
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td ><strong><?php echo $complaint_label ?>:</strong></td>
                        <td><?php echo $casehist_complaint?></td>
                        <td><strong><?php echo $complaint_history_label ?>:</strong></td>
                        <td><?php echo $casehist_complainthist?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td ><strong><?php echo $medical_history_label ?>:</strong></td>
                        <td><?php echo $casehist_medhist?></td>
                        <td><strong><?php echo $social_history_label ?>:</strong></td>
                        <td><?php echo $casehist_sochist?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>

                    </tr>
                    <tr>
                        <td><strong><?php echo $drug_history_label ?>:</strong></td>
                        <td><?php echo $casehist_drughist?></td>
                        <td><strong><?php echo $system_review_label ?>:</strong></td>
                        <td><?php echo $casehist_sysreview?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><strong><?php echo $others_label ?>:</strong></td>
                        <td><?php echo $casehist_others ?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
              </div>
              <div  class="tabContent" id="tabs-2">
                <table border="0" cellspacing="1" cellpadding="5" width="100%">
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><strong><?php echo $general_exam_label; ?>:</strong></td>
                        <td><?php echo $casenote_genexam; ?></td>
                        <td><strong><?php echo $diagnosis_label; ?>:</strong></td>
                        <td><?php echo $casenote_diagnosis; ?></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><strong><?php echo $system_examination_label ?>:</strong></td>
                        <td><?php echo $casenote_sysexam?></td>
                        <td><strong><?php echo $differential_diagnosis_label ?>:</strong></td>
                        <td><?php echo $casenote_diffdiagnosis?></td>
                    </tr>
                </table>
              </div>
              <div  class="tabContent" id="tabs-3">
                <table border="0" cellspacing="1" cellpadding="5" width="100%">
                <tr>
                  <td valign="top">&nbsp;</td>
                  <td valign="top">&nbsp;</td>
                  </tr>
                <tr>
                  <td valign="top"><strong><?php echo $drug_name_label; ?>:</strong></td>
                <td valign="top"><table border='0' cellpadding='0'><?php echo $treats->selectPrescription($case_id);
         ?></table></td>
                </tr>

                <tr>
                  <td valign="top">&nbsp;</td>
                  <td valign="top">&nbsp;</td>
                  </tr>
                <tr>
                      <td valign="top"><strong><?php echo $investigation_label ?>:</strong></td>
                      <td valign="top">
                            <?php echo $labtest ?>
                            <!--<span style="padding-left: 10px;"><a target="_blank" href="index.php?p=getlabtestresult4doc&m=patient_care&cnoteid=<?php //echo $case_id; ?>">View Results</a></span>-->
                      </td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                  <td valign="top">&nbsp;</td>
                  </tr>
                <tr>
                  <td valign="top"><strong><?php echo $other_treatment_label ?>:</strong></td>
                  <td valign="top"><?php echo !empty($treatment) ? $treatment : ""; ?></td>
                  </tr>
                </table>
              </div>
            <div class="tabContent" id="tabs-4">
<?php
    //Get custom fields if any
    $fBuilder = new FormBuilder();
    $entries = $fBuilder->getFormEntries($case_id, 2);
    $custom_entries = '<table cellpadding="3" cellspacing="3">';
//        die('<pre>' . print_r($entries, true));
    if (!empty($entries)){
        $row_style1 = 'tr-row';
        $row_style2 = 'tr-row2';
        $change_style = true;
        foreach ($entries as $entry){
            $row_style = $change_style ? $row_style1 : $row_style2;
            $change_style = !$change_style;
            $custom_entries .= "<tr class=\"$row_style\">
                                    <td><strong>" . strtoupper($entry['customfield_name']) . "</strong>:</td>
                                    <td>" . nl2br($entry['customentry_value']) . "</td>
                                </tr>";
        }
    }
    $custom_entries .= '</table>';
    echo $custom_entries;
?>
            </div>
        </div>
	</div>

    <h3 style="padding: 10px 10px 10px 25px;">Click to view <strong>SCANNED</strong> casenote details</h3>
	<div>
<?php
	$uploader = new FileUploader();
	$appID = $uploader->getAppIDFromCaseNoteID($case_id);
	echo $uploader->getUploadedDocuments($appID);
?>
    </div>

</div>

<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/doubleSelect.1.2/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.tabs.js"></script>
<script type="text/javascript">
	$(function() {
		$("#tabs").tabs().find(".ui-tabs-nav");
	});

	$(function() {
		$("#accordion").accordion({
			collapsible: true,
			active: false,
			clearStyle: true
		});
	});

</script>