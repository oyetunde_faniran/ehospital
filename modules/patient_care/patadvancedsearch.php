<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.

$select = new patSelectOptions;
$hospital_no = isset($_POST['hospital_no'])? $_POST["hospital_no"] : "";
$othernames = isset($_POST['othernames'])? $_POST['othernames'] : "";
$surname =  isset($_POST["surname"])?  $_POST["surname"] : "";
$gender = isset($_POST["gender"])? $_POST["gender"] : "";
$dob = isset($_POST["dob"])? $_POST["dob"] : "";
$nationality_id = isset($_POST["nationality"])? $_POST["nationality"] : 0;
$state_id = isset($_POST["state"])? $_POST["state"] :0;
?>

<form action="index.php?p=advsearch" method="post" name="register_patient" enctype="multipart/form-data">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="10" >
        <tr>
          <td colspan="4" align="left" valign="top">&nbsp;</td>
      </tr>
		<tr>
		  <td align="left" valign="top"><?php echo $hospital_no_label ?></td>
		  <td align="left" valign="top"><input type="text" name="hospital_no" value="<?php echo $hospital_no ?>" /></td> 
		  <td align="left" valign="top">&nbsp;</td>
		  <td align="left" valign="top">&nbsp;</td>
	  </tr>
		<tr>
            <td width="17%" align="left" valign="top"><?php echo $surname_label ?></td>
        <td width="32%" align="left" valign="top">
      <input type="text" name="surname" value="<?php echo $surname ?>" />
          <input name="m" type="hidden" id="m" value="patient_care" /> 
            <td width="19%" align="left" valign="top"><?php echo $othernames_label ?></td>
            <td width="32%" align="left" valign="top"><input type="text" name="othernames" value="<?php echo $othernames ?>" /></td>
        </tr>
        
        <tr>
          <td align="left" valign="top"><?php echo $dob_label ?></td>
          <td align="left" valign="top"> 	  
            <input type="text" name="dob" id="f_date" value="<?php echo $dob ?>">            </td>
          <td align="left" valign="top"><?php echo $gender_label ?></td>
          <td align="left" valign="top">
            <label>
              <input type="radio" name="gender" value="0" id="gender_0" <?php if ($gender=='0') echo "checked" ?>/>
              <?php echo $gender_0_label ?></label>
            <label>
              <input type="radio" name="gender" value="1" id="gender_1" <?php if ($gender=='1') echo "checked" ?>/>
              <?php echo $gender_1_label ?></label>
            <br />                 </td>
        </tr>
        
        <tr>
          <td align="left" valign="top"><?php echo $nationality_label ?></td>
          <td align="left" valign="top"><select name="nationality" id="first">
		  <option value="0">---</option>
          </select>          </td>
          <td align="left" valign="top"><?php echo $state_label ?></td>
          <td align="left" valign="top"><select name="state" id="second">
		  <option value="0">---</option>
          </select></td>
        </tr>
        
        <tr>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">
              <input type="submit" name="search" id="search" value="<?php echo $submit_label ?>" /> </td>
            <td align="left" valign="top"><input type="reset" name="Reset" value="Reset" /></td>
            <td align="left" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
</table>
</form>
    <script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() }
      });
      cal.manageFields("f_date", "f_date", "%Y-%m-%d");
    //]]></script>
<?php
if(isset($_POST['search'])){
$hospital_no = sprintf("%07d",$_POST['hospital_no']);
$othernames = $_POST['othernames'];
$surname =  $_POST["surname"];
$gender = isset($_POST["gender"])? $_POST["gender"] : "";
$dob = $_POST["dob"];
$nationality = $_POST["nationality"];
$state = $_POST["state"];


$db2 = new DBConf();

		$sql="SELECT reg.reg_id, reg_hospital_no, reg_hospital_oldno, reg_surname, reg_othernames, reg_gender 
				FROM registry reg LEFT JOIN registry_extra re ON reg.reg_id = re.reg_id
				WHERE true";
if($hospital_no != 0){
		$sql .= " AND reg_hospital_no = '".$hospital_no."'";

}				
				
if($othernames  != ""){
		$sql .= " AND reg_othernames LIKE '%".$othernames ."%'";

}				
if($surname != ""){
		$sql .= " AND reg_surname LIKE '%".$surname."%'";

}				
if($gender != ""){
		$sql .= " AND reg_gender = '".$gender."'";

}				
if($dob != ""){
		$sql .= " AND reg_dob = '".$dob."'";

}				
if($nationality != 0){
		$sql .= " AND nationality_id = '".$nationality."'";

}				
if($state != 0){
		$sql .= " AND state_id = '".$state."'";

}				

				
		$res = $db2->execute($sql);
		echo $sql;
		$num = mysql_num_rows($res);
		
		
		if($num>0){
?>
<div class="headerText"><strong><?php echo $search_result_label ?></strong></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><strong><?php echo $hospital_no_label ?></strong></td>
    <td><strong><?php echo $old_hospital_no_label ?></strong></td>
    <td><strong><?php echo $name_label ?></strong></td>
    <td><strong><?php echo $gender_label ?></strong></td>
    <td>&nbsp;</td>
  </tr>

<?php
		for ($i=0; $i<$num; $i++){
		$row = mysql_fetch_array($res);
		$hospital_no = $row['reg_hospital_no'];
		$oldhospital_no = $row['reg_hospital_oldno'];
		$reg_surname = $row['reg_surname'];
		$reg_othernames = $row['reg_othernames'];
		$reg_gender = $row['reg_gender'];
		$reg_id = $row['reg_id'];
		
		
		if($reg_gender =='0') $reg_gender = $gender_0_label;
		if($reg_gender =='1') $reg_gender = $gender_1_label;
		
		$reg_name = $reg_surname.' '.$reg_othernames;
		$reg_name = strtoupper($reg_name);
		
		?>
          <tr>
            <td><?php echo $hospital_no; ?></td>
            <td><?php echo $oldhospital_no; ?></td>
            <td><?php echo  $reg_name; ?></td>
            <td><?php echo $reg_gender; ?></td>
    		<td><a href="#"  onClick="display('regform',<?php echo $reg_id  ?>); return false"><?php echo $continue_label ?></a></td>
          </tr>
		
		<?php
		}
?>
</table>
<?php			}else{
?>
			<div class="redpad"><strong><?php echo $search_not_found_label ?></strong></div>
					<a href="#"  onClick="display('regform',''); return false"><?php echo $continue_label ?></a><br />
<?php			
			}
}
?>
		<script type="text/javascript" src="library/doubleSelect.1.2/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="library/doubleSelect.1.2/jquery.doubleSelect.min.js"></script>
		<script type="text/JavaScript">
 $(document).ready(function()
 {
	    var selectoptions = {
     		"Select Country":{
				  					"key":0,
									"values" : {"Select State":0 	}				 },
	   		<?php 
				$select->selectJqueryNationalityAndState($outside_nigeria_label);
			?>
    };

    var options = {
                    preselectFirst :<?php echo $nationality_id ?>, 
                    preselectSecond :<?php echo $state_id ?>, 
                   };
    $('#first').doubleSelect('second', selectoptions, options);      
 });
   </script>
