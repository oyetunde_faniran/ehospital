<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/smoothness/ui.core.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/smoothness/ui.base.css" rel="stylesheet" />
<script type="text/javascript" src="library/doubleSelect.1.2/jquery-1.3.2.min.js"></script>

<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.tabs.js"></script>

<script type="text/javascript" src="library/admin_jquery/ui/ui.draggable.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.resizable.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.dialog.js"></script>

<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>

<script type="text/javascript" src="library/admin_jquery/ui/effects.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/effects.highlight.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/effects.explode.js"></script>
<script type="text/javascript" src="library/admin_jquery/external/bgiframe/jquery.bgiframe.js"></script>


<script type="text/javascript">

	//Show the different treatment sheet entry options in an accordion
	$(function() {
		$("#accordion").accordion({
			collapsible: true,
			active: false,
			clearStyle: true
		});
	});

	//Show the treatment sheet in tabs
	$(function() {
		$("#tabs").tabs(/*{
				fx: {
					opacity: 'toggle',
					duration: 'fast'
				}
		}*/).find(".ui-tabs-nav");
	});


//***BEGIN DIALOG INIT.
	//"More Actions" menu dialog
	$("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 320,
		width: 500,
		modal: true,
		hide: 'explode',
		buttons: {
			'Save Treatment': function() {
				if (confirm ("Are you sure you have entered all necessary data, carried out all necessary actions and ready to save the treatment details of this patient?")){
					document.getElementById("casenote_treatment_form").submit();
					//alert ("TEMPORARY: Form Submitted");
				}
				//$(this).dialog('close');
			},
			'Cancel': function() {
				$(this).dialog('close');
			}
		},

		close: function() {
		}
	});


	/*	index.php?p=ajax&m=sysadmin&t=schedule-new-appointment	*/
	//New Appointment Dialog
	$("#new-app-dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 370,
		width: 300,
		modal: true,
		hide: 'explode',
		buttons: {
			'Schedule Appointment': function() {
				$.ajax({
				  type: 'POST',
				  url: 'admin_ajaxTOC.php?t=schedule-new-appointment',
				  data: $("#new-appointment-form").serialize(),
				  success: function(data){
								$('#app-saving-div').fadeOut();
								$('#app-completed-div').fadeIn();
								try{
									reportContainer = document.getElementById('app-status-report');
									reportContainer.innerHTML = data;
									if (data == "Maximum number of appointments exceeded for the chosen date."){
										$('#try-again-container').fadeIn();
									} else {
										$('#try-again-container').fadeOut();
									}
								} catch (e) {}
							},
				  dataType: "html"
				});
				$('#app-form-container').fadeOut();
				$('#app-saving-div').fadeIn();
				//$(this).dialog('close');
			},
			'Cancel': function() {
				$(this).dialog('close');
			}
		},

		close: function() {}
	});
	
	$('#app-completed-button').click(function(){
			$("#new-app-dialog").dialog('close');
	});
	
	$('#try-again-button').click(function(){
			$('#app-form-container').fadeIn();
			$('#app-completed-div').fadeOut();
	});


	//Admit Patient Dialog
	$("#admit-dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 220,
		width: 300,
		modal: true,
		hide: 'explode',
		buttons: {
			'Admit Into Ward': function() {
				$.ajax({
				  type: 'POST',
				  url: 'admin_ajaxTOC.php?t=admit-patient',
				  data: $("#admission-form").serialize(),
				  success: function(data){
								try{
									reportContainer = document.getElementById('admit-status-report');
									reportContainer.innerHTML = data;
								} catch (e) {}
								$('#admit-saving-div').fadeOut();
								$('#admit-completed-div').fadeIn();
							},
				  dataType: "html"
				});
				$('#admit-form-container').fadeOut();
				$('#admit-saving-div').fadeIn();
				//$(this).dialog('close');
			},
			'Cancel': function() {
				$(this).dialog('close');
			}
		},

		close: function() {}
	});
	
	$('#admit-completed-button').click(function(){
			$("#admit-dialog").dialog('close');
	});
	
	
	//Refer Patient Dialog
	$("#refer-dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 200,
		width: 300,
		modal: true,
		hide: 'explode',
		buttons: {
			'Refer Patient': function() {
				$(this).dialog('close');
			},
			'Cancel': function() {
				$(this).dialog('close');
			}
		},

		close: function() {}
	});
	
	
	//Transfer Patient Dialog
	$("#transfer-dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 440,
		width: 500,
		modal: true,
		hide: 'explode',
		buttons: {
			'Transfer Patient': function() {
				$(this).dialog('close');
			},
			'Cancel': function() {
				$(this).dialog('close');
			}
		},

		close: function() {}
	});
//***END DIALOG INIT.






//***BEGIN BUTTONS TO SHOW DIALOGS OF MORE ACTIONS / MOVE TO TABS
	//To show more action buttons dialog
	$('#sButton').click(function() {
		//if (confirm("If you have not filled the treatment details under the 'History Taking', 'Examination / Diagnosis' & 'Plan' tabs. Please, click the 'Cancel' button to go back and do so, or click the 'OK' button to proceed to more actions.")){
			$('#dialog').dialog('open');
		//}
	});



	//***BEGIN Tab Navigation
		//Move to the 2nd tab (with the 'Next' button)
		$('#next-to-2').click(function(){
			$("#tabs").tabs("select", 1);
		});
		
		//Move to the 3rd tab (with the 'Next' button)
		$('#next-to-3').click(function(){
			$("#tabs").tabs("select", 2);
		});
		
		//Move to the 2nd tab (with the 'Previous' button)
		$('#prev-to-2').click(function(){
			$("#tabs").tabs("select", 1);
		});
		
		//Move to the 1st tab (with the 'Previous' button)
		$('#prev-to-1').click(function(){
			$("#tabs").tabs("select", 0);
		});
	//***END Tab Navigation









	//New Appointment
	$('#new-app-button').click(function(){
		$('#new-app-dialog').dialog('open');
	});

	//Admit into Ward Dialog
	$('#admit-button').click(function(){
		$('#admit-dialog').dialog('open');
	});
	
	//Refer to another consultant/clinic/dept. Dialog
	$('#refer-button').click(function(){
		$('#refer-dialog').dialog('open');
	});
	
	//Transfer to another consultant/clinic/dept. Dialog
	$('#transfer-button').click(function(){
		$('#transfer-dialog').dialog('open');
	});
//***BEGIN BUTTONS TO SHOW DIALOGS OF MORE ACTIONS



	//DATE PICKER for new appointment//show-date
	$(function() {
		$('#date_ui').datepicker({
			changeMonth:	true,
			changeYear:		true,
			dateFormat:		'yy-mm-dd',
			minDate:		+1,
			altField:		'#f_date',
			altFormat:		'yy-mm-dd'
		});
	});




<?php
	$dialogCount = 13;
	for ($k=1; $k<=$dialogCount; $k++){
		echo "$(\"#treat_dialog$k\").dialog({bgiframe: true, autoOpen: false, height: 480, width: 620, modal: true,
					buttons: {
						'OK': function() {
                            try{
                                statusMonitor = document.getElementById(\"dataStatus$k\");
                                if (document.getElementById(\"treat_dialog_cont$k\").value != \"\"){
                                    statusMonitor.innerHTML = \"[Edit Data]\";
                                    statusMonitor.style.color = \"#AAA\";
                                } else {
                                        statusMonitor.innerHTML = \"[Click link to enter data]\";
                                        statusMonitor.style.color = \"#F99\";
                                }
                                dTextBox = document.getElementById(\"treat_dialog_cont$k\");
                                dH = document.getElementById(dTextBox.title);
//                                alert('Gatch ya --- ' + dH);
                                dH.value = dTextBox.value;
                                $(this).dialog('close');
                            } catch (e) { }
						},
						'Cancel': function() {
							document.getElementById(\"treat_dialog_cont$k\").value = tempDialogContents$k;
							$(this).dialog('close');
						}
					},
					close: function() {
						//document.getElementById(\"treat_dialog_cont$k\").value = tempDialogContents$k;
					}
				});
			
				$('#igniter$k').click(function() {
					tempDialogContents$k = document.getElementById(\"treat_dialog_cont$k\").value;
					$('#treat_dialog$k').dialog('open');
				});";
	}
?>



            $(function() {
                $("#hand-writing-presenting-complaint").dialog({
                    bgiframe: true,
                    autoOpen: false,
                    height: 630,
                    width: 650,
                    modal: true,
                    buttons: {
//                        'Cancel': function() {
//                            $(this).dialog('close');
//                        },
						'OK': function() {
                            $(this).dialog('close');
                        }
                    },
                    close: function() {
                    }
                });
            });
            $('#hand-writing-presenting-complaint-dialog-igniter').click(function() {
                $('#hand-writing-presenting-complaint').dialog('open');
            });
            
            
            $(function() {
                $("#hand-writing-history-presenting-complaint").dialog({
                    bgiframe: true,
                    autoOpen: false,
                    height: 630,
                    width: 650,
                    modal: true,
                    buttons: {
//                        'Cancel': function() {
//                            $(this).dialog('close');
//                        },
						'OK': function() {
                            $(this).dialog('close');
                        }
                    },
                    close: function() {
                    }
                });
            });
            $('#hand-writing-history-presenting-complaint-dialog-igniter').click(function() {
                $('#hand-writing-history-presenting-complaint').dialog('open');
            });


function showFormDeets(disForm){
	elemCount = disForm.length;
	dFinal = "";
	for (k=0; k<elemCount; k++){
		elem = disForm.elements[k];
		dFinal = dFinal + "NAME: " + elem.name + " <--> VALUE: " + elem.value + "\n";
	}
	alert (dFinal);
}


function changeInputType(id, type){
    try {
        switch (type){
            case 1: $('#checkboxes-container-' + id).fadeOut();
                    $('#typingbox-container-' + id).fadeIn();
                    break;
            case 2: $('#checkboxes-container-' + id).fadeIn();
                    $('#typingbox-container-' + id).fadeOut();
        }
    } catch (e) {}
}




    $("#diagnosis-search-dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 600,
		width: 800,
		modal: true,
		hide: 'explode',
		buttons: {
			"Done Searching": function() {
				$(this).dialog('close');
			}/*,
			'Cancel': function() {
				$(this).dialog('close');
			}*/
		},

		close: function() {}
	});
    
    dialog_selected_index = 0;
    lastTimeOut = null;
function showDiagnosisSearchDialog(index){
    dialog_selected_index = index;
    $("#diagnosis-search-dialog").dialog('open');
}


function getMatchingDiagnosis(){
    search_text = document.getElementById('diagnosis-search-box').value;
    search_result = document.getElementById('diagnosis-search-result');
    if(search_text != ''){
        $('#diagnosis-search-loader-image').fadeIn();
        dis_url = 'index.php?p=ajax&t=diagnosis_search&q=' + search_text;
        $.ajax({
          type: 'GET',
          url: dis_url,
          success: function(data){
                        $('#diagnosis-search-loader-image').fadeOut();
                        search_result.innerHTML = data;
                   },
          dataType: "html"
        });
    }
}


function doDiagnosisSelect(row_id, value, question_id){
    //Remove the disease from the current view
    current_row = document.getElementById(row_id);
    current_row.parentNode.removeChild(current_row);

    try {
        //Create a new checkbox for the selected diagnosis
        dis_id = 'treatment-question-diagnosis-' + dialog_selected_index + '-' + question_id;
        dis_name = 'treatment_question[' + dialog_selected_index + '][' + question_id + ']';

        //Create a new label for it
        newLabel = document.createElement("label");
        newLabel.innerHTML = value;

        newDiv = document.createElement("div");
        newDiv.style = 'margin: 10px;';
        newDiv.id = dis_id;
        newDiv.innerHTML = newDiv.innerHTML + ' <img src="images/b_drop.png" onclick="removeSelectedDiagnosis(\'' + dis_id + '\');" />';
        newDiv.appendChild(newLabel);      //Put the Label in the new DIV

        //Get the CheckBoxes Container
        containerID = 'checkboxes-container-' + dialog_selected_index;
        container = document.getElementById(containerID);
        container.appendChild(newDiv);  //Put the DIV in the main container


        //Put this value in the list of hidden form values
        newCB2 = document.createElement("input");
        newCB2.id = dis_id + '-new';
        newCB2.value = value;
        newCB2.type = 'hidden';
        newCB2.name = dis_name;
        document.getElementById('casenote_treatment_form').appendChild(newCB2);
    } catch (e) { alert(e); }
}



function removeSelectedDiagnosis(id){
    if (confirm('Are you sure you want to remove the selected diagnosis?')){
        disDiv = document.getElementById(id);
        disDiv.parentNode.removeChild(disDiv);

        disCB = document.getElementById(id + '-new');
        disCB.parentNode.removeChild(disCB);
    }
}


function saveCheckBoxInForm(cb_id, cb_name, cb_value){
    //checkbox_id
    try {
        if (document.getElementById(cb_id).checked == true){
            newCB = document.createElement("input");
            newCB.id = cb_id + '-new';
            newCB.value = cb_value;
            newCB.type = 'hidden';
            newCB.name = cb_name;
            document.getElementById('casenote_treatment_form').appendChild(newCB);
        } else {
            disCB = document.getElementById(cb_id + '-new');
            disCB.parentNode.removeChild(disCB);
        }
    } catch (e) { /*alert(e);*/ }
}


    $("#new-checkbox-entry-dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 250,
		width: 500,
		modal: true,
		hide: 'explode',
		buttons: {
			'Close': function() {
				$(this).dialog('close');
			}
		},

		close: function() {}
	});
    
    
    $("#delete-checkbox-entry-dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 200,
		width: 500,
		modal: true,
		hide: 'explode',
		buttons: {
			'Close': function() {
				$(this).dialog('close');
			}
		},

		close: function() {}
	});


    function addNewCheckBoxEntry(field){
        document.getElementById('new-checkbox-entry-field').value = field;
        $("#new-checkbox-entry-server-response").hide();
        $("#new-checkbox-entry-dialog").dialog('open');
    }
    
    
//    function editCheckBoxEntry(id){
//        alert(id);
//    }
    
    
    delete_question_id = 0;
    function deleteCheckBoxEntry(id){
        delete_question_id = id;
        dis_question = document.getElementById('treatment-question-label-' + id).innerHTML;
        document.getElementById('delete-checkbox-entry-confirm-question').innerHTML = ' <strong>(' + dis_question + ')</strong>';
        document.getElementById('delete-checkbox-entry-question-id').value = id;
        $("#delete-checkbox-entry-server-response").hide();
        $("#delete-checkbox-entry-progress-image").hide();
        $("#delete-checkbox-entry-actual").show();
        $("#delete-checkbox-entry-dialog").dialog('open');
    }
    
    
    function addNewCheckBoxEntry2Form(entry, field, id){
        dis_field_entry_id = "treatment-question-" + id;
        container_id = 'treatment-question-container-' . id;
        dis_field_entry = '<div id="' + container_id + '">';
        dis_field_entry += ' <input id="' + dis_field_entry_id + '" type="checkbox" value="' + entry + '" name="treatment_question[' + field + '][' + id + ']" onclick="saveCheckBoxInForm(\'' + dis_field_entry_id + '\', \'treatment_question[' + field + '][' + id + ']\', \'' + entry + '\');" />';
        dis_field_entry += ' <label for="' + dis_field_entry_id + '">' + entry + '</label>';
//        dis_field_entry += ' <span><img style="cursor: pointer;" src="images/edit.png" alt="Edit" title="Edit" onclick="editCheckBoxEntry(' + id + ');" /></span>';
//        dis_field_entry += ' <span><img style="cursor: pointer;" src="images/delete.png" alt="Delete" title="Delete" onclick="deleteCheckBoxEntry(' + id + ', \'' + entry + '\');" /></span>';
        dis_field_entry += ' <span><img style="cursor: pointer;" src="images/delete.png" alt="Delete" title="Delete" onclick="deleteCheckBoxEntry(' + id + ');" /></span>';
        dis_field_entry += '</div>'
        dis_cb_container = document.getElementById('checkboxes-container-' + field);
        dis_cb_container.innerHTML = dis_cb_container.innerHTML + dis_field_entry;
    }
    
    
    function deleteNewCheckBoxEntryFromForm(id){
        container_id = 'treatment-question-container-' + id;
        dis_container = document.getElementById(container_id);
        dis_container.parentNode.removeChild(dis_container);
    }


    $('#new-checkbox-entry-button-add').click(function(){
        $('#new-checkbox-entry-button-add').fadeOut();
        $('#new-checkbox-entry-progress-image').fadeIn();
        $.ajax({
          type: 'POST',
          url: 'admin_ajaxTOC.php?t=new-checkbox-entry',
          data: $("#new-checkbox-entry-form").serialize(),
          success: function(data){
                        if (data != 0){
                            display_message = 'Entry added successfully.';
                            
                            entry = document.getElementById('new-checkbox-entry-text-entry').value;
                            field = document.getElementById('new-checkbox-entry-field').value;
                            
//                            document.getElementById('new-checkbox-entry-field').value = 0;
                            document.getElementById('new-checkbox-entry-text-entry').value = '';
                            document.getElementById('new-checkbox-entry-clinic-own').checked = true;
                            addNewCheckBoxEntry2Form(entry, field, data);
                        } else {
                            display_message = 'Entry addition failed.';
                        }
                        document.getElementById('new-checkbox-entry-add-server-response').innerHTML = display_message;
                        $('#new-checkbox-entry-button-add').fadeIn();
                        $('#new-checkbox-entry-progress-image').fadeOut();
                        $('#new-checkbox-entry-add-server-response').fadeIn();
                    },
          dataType: "html"
        });
    });
    
    
    
    $('#delete-checkbox-entry-button-no').click(function(){
        $('#delete-checkbox-entry-dialog').dialog('close');
    });
    
    
    $('#delete-checkbox-entry-button-yes').click(function(){
        $('#delete-checkbox-entry-actual').fadeOut();
        $('#delete-checkbox-entry-progress-image').fadeIn();
        $.ajax({
          type: 'POST',
          url: 'admin_ajaxTOC.php?t=new-checkbox-entry-delete',
          data: $("#delete-checkbox-entry-form").serialize(),
          success: function(data){
                        if (data == 1){
                            display_message = 'Entry deleted successfully.';
                            container_id = 'treatment-question-container-' + delete_question_id;
                            deleteNewCheckBoxEntryFromForm(delete_question_id);
                        } else {
                            display_message = 'Entry deletion failed.';
                        }
                        document.getElementById('delete-checkbox-entry-server-response').innerHTML = display_message;
                        $("#delete-checkbox-entry-server-response").fadeIn();
                        $("#delete-checkbox-entry-progress-image").fadeOut();
                    },
          dataType: "html"
        });
    });








</script>
<!--<script type="text/javascript" src="library/sketch.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#colors_sketch1').sketch({defaultSize: 1, toolLinks: false});
    $('#colors_sketch2').sketch({defaultSize: 1, toolLinks: false});
  });
</script>-->