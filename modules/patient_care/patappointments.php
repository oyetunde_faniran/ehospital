<?php # Script 2.6 - search.inc.php
global $ddept, $dclinic;

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
} // End of defined() IF.

//instantiate Class

//This gets today's date
$date =time () ;

//This puts the day, month, and year in seperate variables
$day = date('d', $date) ;
$month = date('m', $date) ;
$year = date('Y', $date) ;

    if(isset($_GET['d'])){
	$day=$_GET['d'];
	}
    if(isset($_GET['mt'])){
	$month=$_GET['mt'];
	}
    if(isset($_GET['y'])){
	$year=$_GET['y'];
	}

//create the links to next and previous months

    $next = $month+1;
    $x = $year;

//if month is 13 then new year

    if($next==13){
        $next=1;
        $x = $x+1;
    }
    $prev = $month-1;
    $y = $year;

//if month is 0, then previous year

    if($prev==0){
        $prev=12;
        $y=$y-1;
    }


//Here we generate the first day of the month
$first_day = mktime(0,0,0,$month, 1, $year) ;

//This gets us the month name
$title = date('F', $first_day) ;
//Here we find out what day of the week the first day of the month falls on
$day_of_week = date('D', $first_day) ;

//Once we know what day of the week it falls on, we know how many blank days occure before it. If the first day of the week is a Sunday then it would be zero
switch($day_of_week){
case "Sun": $blank = 0; break;
case "Mon": $blank = 1; break;
case "Tue": $blank = 2; break;
case "Wed": $blank = 3; break;
case "Thu": $blank = 4; break;
case "Fri": $blank = 5; break;
case "Sat": $blank = 6; break;
}

//We then determine how many days are in the current month
$days_in_month = cal_days_in_month(0, $month, $year);
//Here we start building the table heads
?>
		<script type="text/javascript" src="library/doubleSelect.1.2/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="library/doubleSelect.1.2/jquery.doubleSelect.min.js"></script>
		<script type="text/JavaScript">
 $(document).ready(function()
 {
	    var selectoptions = {
    		<?php 
			
			$select->selectJqueryDepartAndClinic($curLangField);
			
              ?>
    };

	    $('#first').doubleSelect('second', selectoptions);      
 });
   </script><form action="index.php" method="get">
 <fieldset style="padding:5px; width:685px; margin-bottom:5px;">
					<legend>
						View Appointments by Clinic
					</legend>
					<div style="margin: 10px">	<label for="first" style="width: 140px;	display: block;	float: left; padding-left: 4px;">
							Select Department:
						</label>       	<select id="first" name="dept" style="border: 1px 0 1px 1px solid #666; margin: 1px;">
  				<option value="" >-------</option>
			</select></div>
            <div style="margin: 10px">
            <label for="second" style="width: 140px; display: block;	float: left; padding-left: 4px;">
							Select Clinic:
						</label> 		<select id="second" name="clinic" style="border: 1px 0 1px 1px solid #666; margin: 1px;">
          <option value="">-------</option>
			
            </select>
            </div>
 					<div style="margin: 10px">
                    <input type="hidden" name="p" value="viewallapp" />
                    <input type="hidden" name="m" value="patient_care" />
						<input type="submit" name="submit" value="<?php echo $submit_label ?>" />
					</div>
           </fieldset></form>     
<?php 
if(isset($_GET["dept"]) && isset($_GET["clinic"])){
$ddept = $_GET["dept"];
$dclinic = $_GET["clinic"];
}
?>
<table width='700' border='0' align='left' cellpadding='5' cellspacing='0'>
<tr  bgcolor='#ed1d1d' height="40">
		<td width='200' colspan=2 align='right'><a href='index.php?p=viewallapp&m=patient_care&mt=<?php echo $prev ?>&y=<?php echo $y ?>&d=<?php echo $day ?>&ddept=<?php echo $ddept ?>&dclinic=<?php echo $dclinic ?>' style='color:#FFFFFF'><strong>&lt;&lt; </strong></a></td>
		<td colspan=3  width='300' align='center'><font color='#ffffff'><strong><?php echo  $title." ".$year;  ?></strong></font></td>
		<td colspan=2 width='200' align='left'><a href='index.php?p=viewallapp&m=patient_care&mt=<?php echo $next ?>&y=<?php echo $x ?>&d=<?php echo $day ?>&ddept=<?php echo $ddept ?>&dclinic=<?php echo $dclinic ?>' style='color:#FFFFFF'><strong>&gt;&gt; </strong></a></td>
	</tr>
    <tr bgcolor='#EDBD01' height="40">
		<td width='100' align='center'><strong><?php echo $sun_label ?></strong></td>
		<td width='100' align='center'><strong><?php echo $mon_label ?></strong></td>
		<td width='100' align='center'><strong><?php echo $tue_label ?></strong></td>
		<td width='100' align='center'><strong><?php echo $wed_label ?></strong></td>
		<td width='100' align='center'><strong><?php echo $thu_label ?></strong></td>
		<td width='100' align='center'><strong><?php echo $fri_label ?></strong></td>
		<td width='100' align='center'><strong><?php echo $sat_label ?></strong></td>
	</tr>
 <?php

//This counts the days in the week, up to 7
$day_count = 1;

?><tr>
<?php
//first we take care of those blank days
while ( $blank > 0 )
{
echo "<td bgcolor='#D2D2D2'>&nbsp;</td>";
$blank = $blank-1;
$day_count++;
} 
//sets the first day of the month to 1
$day_num = 1;
		

//count up the days, untill we've done all of them in the month
while ( $day_num <= $days_in_month )
{
		if ($day_num%2) {
			$bgcolor = '#C3C3C3';
		} else {
			$bgcolor = '#EAEAEA';
		}
		if($day_num==$day) $bgcolor = '#858900';
$ddate ="$year-$month-$day_num " ;
?>
<td bgcolor='<?php echo $bgcolor ?>' height="60"> <?php echo $day_num ?><br />
<?php 
if($ddept !=0 && $dclinic != 0){
$view = $appoint->viewAppointment($ddate, $ddept, $dclinic);

}else{
$view = $appoint->viewAppointment($ddate,0,0);
}
extract($view);
if($num ==1){

echo $num." ".$patient_label;
}
if($num > 1){
echo $num." ".$patients_label;
}
 
?> 
</td>
<?php
$day_num++;
$day_count++;

//Make sure we start a new row every week
if ($day_count > 7)
{
?>
</tr><tr>
<?php
$day_count = 1;
}
} 
//Finaly we finish out the table with some blank details if needed
while ( $day_count >1 && $day_count <=7 )
{
?>
<td bgcolor='#D2D2D2'>&nbsp;</td>
<?php
$day_count++;
}
?>
</tr></table>
