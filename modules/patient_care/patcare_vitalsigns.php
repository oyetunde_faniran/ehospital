<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>

<p><?php echo $vitalsigns_story1; ?></p>
<div>
	<form action="" method="post">
    	<select name="clinic">
        	<option>--Select Clinic--</option>
<?php
	$selected = isset($_POST["clinic"]) ? (int)$_POST["clinic"] : 0;
	$clinics = new Clinic_New($_SESSION[session_id() . "langField"]);
	$allClinics = $clinics->getAllClinics4DD($selected);
	echo $allClinics;
?>
        </select>
        <input type="submit" value="Show Appointments" class="btn" />
    </form>
</div>



<?php
	if ($_POST){
?>

<div>
<?php
		$query = "SELECT DATE(app.app_starttime) AS date,
							reg.reg_hospital_no AS hospital_no, app.app_status, app.patadm_id,
							CONCAT_WS(' ', UPPER(reg.reg_surname), LOWER(reg.reg_othernames)) AS name,
							(
								CASE reg.reg_gender
									WHEN '0' THEN '$gender_0_label'
									WHEN '1' THEN '$gender_1_label'
								END
							) AS sex,
							YEAR(CURDATE()) - YEAR(reg.reg_dob) AS age,
							TIME_FORMAT(TIME(app.app_starttime),'%r') AS starttime,
							TIME_FORMAT(TIME(app.app_endtime),'%r') AS endtime, 
							(
								CASE app.app_type
									WHEN '0' THEN '$pat_appointment_consultation'
									WHEN '1' THEN '$pat_appointment_consultation'
									WHEN '2' THEN '$pat_appointment_followup'
								END
							) AS appoint_type,
							trans.pattotal_status AS 'others', 
							app.app_id AS app_id,
							trans.pattotal_status 'paid'	# = 1 if patient has paid for this consultation, 0 if not
					FROM appointment app 
					LEFT JOIN pat_transtotal trans ON (trans.patadm_id = app.patadm_id AND trans.pattotal_servicerendered='0' AND trans.pattotal_newstatus != '')
						INNER JOIN registry reg 
						INNER JOIN department dept 
						INNER JOIN language_content lc2 
						INNER JOIN patient_admission pat
					 ON app.patadm_id = pat.patadm_id 
						AND pat.reg_hospital_no = reg.reg_hospital_no
						AND app.dept_id = dept.dept_id
						AND dept.langcont_id = lc2.langcont_id
					WHERE app.clinic_id = '" . $selected /*$_SESSION[session_id() . "clinicID"]*/ . "' AND DATE(app.app_starttime) = CURDATE()
					ORDER BY name, hospital_no"; //die ("<pre>$query</pre>");
	$app = new patAppointment();
	$appointments = $app->getAppointments4Nurses($query, $selected);	//Get all the appointments for today in the current clinic
	//$disClinic = $_SESSION[session_id() . "clinicName"];	//Get the name of the clinic the currently logged-in nurse is attached to
	$disClinic = $clinics->getClinicName($selected);
	$date_today = date("F d, Y", time());
	$final = "<!--<div>$vitalsigns_story1</div>-->
				<div>&nbsp;</div>
				<h3><strong><em>$disClinic</em> $vitalsigns_appointments $date_today </strong></h3>
				<div>&nbsp;</div>";
	if ($appointments !== false){
		$final .= "<table border=\"1\" width=\"100%\">
					<tr class=\"title-row\">
						<td align=\"center\"><strong>$pat_appointment_sno</strong></td>
						<td align=\"center\"><strong>$hospital_no_label</strong></td>
						<td align=\"center\"><strong>$pat_appointment_patientname</strong></td>
						<td align=\"center\"><strong>$pat_appointment_sex</strong></td>
						<td align=\"center\"><strong>$pat_appointment_age</strong></td>
						<td align=\"center\"><strong>$pat_appointment_date</strong></td>
						<td align=\"center\"><strong>$pat_appointment_starttime</strong></td>
						<td align=\"center\"><strong>$pat_appointment_endtime</strong></td>
						<td align=\"center\"><strong>$pat_appointment_apptype</strong></td>
						<td align=\"center\"><strong>$pat_appointment_vitalsigns_appstatus</strong></td>
					 </tr>"
					 . $appointments . 
			 "</table>";
	} else $final = "<div>$vitalsigns_noappointment</div>";
	echo $final;
?>
</div>

<?php
	}	//END if ($_POST)
?>