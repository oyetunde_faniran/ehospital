<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
$select = new patSelectOptions;
?>
<!-- 
===============================================
	Simple Search Starts  
=============================================== 
-->
<div id="simple"  <?php if(isset($_POST['search'])) echo "style='display:none'";?>>
 <form action="index.php?p=register_patient" method="post" name="search_patient">
      <table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" >
      	<tr>
          <td align="left" valign="top"><strong><?php echo $search_label ?>:&nbsp;&nbsp;</strong>
            <input name="patsearch" type="text" id="patsearch" size="25" />
            &nbsp;
           <input name="search1" id="search1" type="submit" value="<?php echo $search_label ?>" onClick="MM_validateForm('patsearch','','R');return document.MM_returnValue" />
            <a href="#" class="redpad" onclick="display('adv','simple','regform',''); return false"><?php echo $advancedsearch_label ?></a><br />
            <input type="hidden" name="m" value="patient_care" /> 		  </td>
        </tr>
 		<tr>
          <td class="rightsText">(<?php echo $search_text ?>)</td>
        </tr>
	  </table>
 </form>
<?php 

if(isset($_POST['patsearch'])){
$db2 = new DBConf();

?>
<?php
$search = strtoupper($_POST['patsearch']);
$patsearch = explode (" ", $search);

foreach($patsearch as $key => $value){
		$sql="SELECT reg_id, reg_hospital_no, reg_surname, reg_othernames, reg_gender, YEAR(CURDATE()) - YEAR(reg_dob) AS age
			 FROM registry 
			WHERE reg_hospital_no LIKE '%".$value."' 
				   OR reg_surname LIKE '%".$value."%' 
				   OR reg_othernames LIKE '%".$value."%' 
				   OR reg_hospital_oldno LIKE '".$value."'";
				
		$res = $db2->execute($sql);
		$num = mysql_num_rows($res);
		
		
		if($num>0){
?>
<div class="headerText"><strong><?php echo $search_result_label ?></strong></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="simple">
  <tr>
    <td><strong><?php echo $hospital_no_label ?></strong></td>
    <td><strong><?php echo $name_label ?></strong></td>
    <td><strong><?php echo $age_label ?></strong></td>
    <td><strong><?php echo $gender_label ?></strong></td>
    <td>&nbsp;</td>
  </tr>

<?php
		for ($i=0; $i<$num; $i++){
		$row = mysql_fetch_array($res);
		$hospital_no[$key] = $row['reg_hospital_no'];
		$age = $row['age'];
		$reg_surname = $row['reg_surname'];
		$reg_othernames = $row['reg_othernames'];
		$reg_gender = $row['reg_gender'];
		$reg_id = $row['reg_id'];
		
		
		if($reg_gender =='0') $reg_gender = $gender_0_label;
		if($reg_gender =='1') $reg_gender = $gender_1_label;
		
		$reg_name = $reg_surname.' '.$reg_othernames;
		$reg_name = strtoupper($reg_name);
		$reg_name2 = "<font color='#D80000'>".$reg_name."</font>";
		
		$value2 = "<font color='#D80000'>".$value."</font>";
		if($key==0){
		?>
          <tr>
            <td><?php print str_ireplace ("$value", "$value2", $hospital_no[$key]); ?></td>
            <td><?php if($reg_name == $search){
					print str_ireplace ("$reg_name", "$reg_name2", $reg_name) ;
				}else{
				print str_ireplace ("$value", "$value2", $reg_name) ; 
				}
				?>
        </td>
            <td><?php echo $age; ?></td>
            <td><?php echo $reg_gender; ?></td>
    		<td><a href="#"  onClick="display('regform','simple','adv',<?php echo $reg_id  ?>); return false"><?php echo $continue_label ?></a></td>
          </tr>
		
		<?php
		}else{
				for($j=0; $j<$i; $j++){
				if($hospital_no[$key] == $hospital_no[$j]){
				}else{
		?>
          <tr>
            <td><?php print str_ireplace ("$value", "$value2", $hospital_no[$key]); ?></td>
            <td><?php print str_ireplace ("$value", "$value2", $reg_name) ; ?></td>
            <td><?php echo $age; ?></td>
            <td><?php echo $reg_gender; ?></td>
     		<td><a href="#"  onClick="display('regform','simple','adv',<?php echo $reg_id  ?>); return false"><?php echo $continue_label ?></a></td>
         </tr>
		
		<?php
				}
				}
		}
		}
?>
</table>
<?php			}else{
?>
			<div class="redpad"><strong><?php echo $search_not_found_label ?></strong></div>
					<a href="#"  onClick="display('regform',''); return false"><?php echo $continue_label ?></a><br />
<?php			
			}
	  }
?>
<?php
}
?>
</div>
<!-- 
===============================================
	Simple search ends  
=============================================== 

===============================================
	Advanced Search Starts
=============================================== 
 -->
<div  id="adv" <?php if(isset($_POST['search'])) echo "style='display:block'"; else echo'style="display:none"'?>>
<?php 
$hospital_no = isset($_POST['hospital_no'])? $_POST["hospital_no"] : "";
$othernames = isset($_POST['othernames'])? $_POST['othernames'] : "";
$surname =  isset($_POST["surname"])?  $_POST["surname"] : "";
$gender = isset($_POST["gender"])? $_POST["gender"] : "";
$dob = isset($_POST["dob"])? $_POST["dob"] : "";
$nationality_id = isset($_POST["nationality"])? $_POST["nationality"] : 0;
$state_id = isset($_POST["state"])? $_POST["state"] :0;
?>
<form action="index.php?p=register_patient" method="post" name="advance_search">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2">
        <tr>
          <td colspan="4" align="left" valign="top"><h2><?php echo $advancedsearch_label ?></h2></td>
      </tr>
		<tr>
		  <td align="left" valign="top"><strong><?php echo $hospital_no_label ?></strong></td>
		  <td align="left" valign="top"><input type="text" name="hospital_no" value="<?php echo $hospital_no ?>" /></td> 
		  <td align="left" valign="top">&nbsp;</td>
		  <td align="left" valign="top">&nbsp;</td>
	  </tr>
		<tr>
            <td width="17%" align="left" valign="top"><strong><?php echo $surname_label ?>:</strong></td>
        <td width="32%" align="left" valign="top">
      <input type="text" name="surname" value="<?php echo $surname ?>" />
          <input name="m" type="hidden" id="m" value="patient_care" /> 
          <td width="19%" align="left" valign="top"><strong><?php echo $othernames_label ?>:</strong></td>
            <td width="32%" align="left" valign="top"><input type="text" name="othernames" value="<?php echo $othernames ?>" /></td>
        </tr>
        
        <tr>
          <td align="left" valign="top"><strong><?php echo $dob_label ?>:</strong></td>
          <td align="left" valign="top"> 	  
            <input type="text" name="dob" id="dob" value="<?php echo $dob ?>">            </td>
          <td align="left" valign="top"><strong><?php echo $gender_label ?>:</strong></td>
          <td align="left" valign="top">
            <label>
              <input type="radio" name="gender" value="0" id="gender_0" <?php if ($gender=='0') echo "checked" ?>/>
              <?php echo $gender_0_label ?></label>
            <label>
              <input type="radio" name="gender" value="1" id="gender_1" <?php if ($gender=='1') echo "checked" ?>/>
              <?php echo $gender_1_label ?></label>
            <br />                 </td>
        </tr>
        
        <tr>
          <td align="left" valign="top"><strong><?php echo $nationality_label ?>:</strong></td>
          <td align="left" valign="top"><select name="nationality" id="third">
		  <option value="0">---</option>
          </select>          </td>
          <td align="left" valign="top"><strong><?php echo $state_label ?>:</strong></td>
          <td align="left" valign="top"><select name="state" id="fourth">
		  <option value="0">---</option>
          </select></td>
        </tr>
        
        <tr>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">
              <input type="submit" name="search" id="search" value="<?php echo $search_label ?>" onclick="return document.MM_returnValue"/> </td>
            <td align="left" valign="top"><input type="reset" name="Reset" value="<?php echo $clear_label; ?>" /></td>
            <td align="left" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
</table>
</form>

<?php
if(isset($_POST['search'])){
$hospital_no = sprintf("%07d",$_POST['hospital_no']);
$othernames = $_POST['othernames'];
$surname =  $_POST["surname"];
$gender = isset($_POST["gender"])? $_POST["gender"] : "";
$dob = $_POST["dob"];
$nationality = $_POST["nationality"];
$state = $_POST["state"];


$db2 = new DBConf();

		$sql="SELECT reg.reg_id, reg_hospital_no, reg_surname, reg_othernames, reg_gender, YEAR(CURDATE()) - YEAR(reg.reg_dob) AS age 
				FROM registry reg LEFT JOIN registry_extra re ON reg.reg_id = re.reg_id
				WHERE true";
if($hospital_no != 0){
		$sql .= " AND reg_hospital_no = '".$hospital_no."'";

}				
				
if($othernames  != ""){
		$sql .= " AND reg_othernames LIKE '%".$othernames ."%'";

}				
if($surname != ""){
		$sql .= " AND reg_surname LIKE '%".$surname."%'";

}				
if($gender != ""){
		$sql .= " AND reg_gender = '".$gender."'";

}				
if($dob != ""){
		$sql .= " AND reg_dob = '".$dob."'";

}				
if($nationality != 0){
		$sql .= " AND nationality_id = '".$nationality."'";

}				
if($state != 0){
		$sql .= " AND state_id = '".$state."'";

}				

				
		$res = $db2->execute($sql);
		$num = mysql_num_rows($res);
		
		
		if($num>0){
?>
<div class="headerText"><strong><?php echo $search_result_label ?></strong></div>
<table width="100%" id="theTable" cellpadding="0" cellspacing="0" class="sortable-onload-3 no-arrow rowstyle-alt colstyle-alt paginate-10 max-pages-7 paginationcallback-callbackTest-calculateTotalRating paginationcallback-callbackTest-displayTextInfo sortcompletecallback-callbackTest-calculateTotalRating">
  <tr>
    <td><strong><?php echo $hospital_no_label ?></strong></td>
    <td><strong><?php echo $name_label ?></strong></td>
    <td><strong><?php echo $age_label ?></strong></td>
    <td><strong><?php echo $gender_label ?></strong></td>
    <td>&nbsp;</td>
  </tr>

<?php
		for ($i=0; $i<$num; $i++){
		$row = mysql_fetch_array($res);
		$hospital_no = $row['reg_hospital_no'];
		$age = $row['age'];
		$reg_surname = $row['reg_surname'];
		$reg_othernames = $row['reg_othernames'];
		$reg_gender = $row['reg_gender'];
		$reg_id = $row['reg_id'];
		
		
		if($reg_gender =='0') $reg_gender = $gender_0_label;
		if($reg_gender =='1') $reg_gender = $gender_1_label;
		
		$reg_name = $reg_surname.' '.$reg_othernames;
		$reg_name = strtoupper($reg_name);
		
		?>
          <tr>
            <td><?php echo $hospital_no; ?></td>
            <td><?php echo  $reg_name; ?></td>
            <td><?php echo $age; ?></td>
            <td><?php echo $reg_gender; ?></td>
    		<td><a href="#"  onClick="display('regform','simple','adv',<?php echo $reg_id  ?>); return false"><?php echo $continue_label ?></a></td>
          </tr>
		
		<?php
		}
?>
</table>
<?php			}else{
?>
			<div class="redpad"><strong><?php echo $search_not_found_label ?></strong></div>
					<a href="#"  onClick="display('regform','simple','adv',''); return false"><?php echo $continue_label ?></a><br />
<?php			
			}
}
?>
<a href="index.php?p=register_patient&m=patient_care"><?php echo $patreg_backbtn ?></a>
</div>
<!-- 
===============================================
	Advanced Search Ends  
=============================================== 
 -->
<br />
<div id = "regform"<?php if(isset($_POST['patsearch']) || isset($_POST['search'])) echo "style='display:none'" ?>>
<form action="index.php?p=reg_form" method="post" name="register_patient">
	<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" > 
    	<tr>
        	<td width="20%"  align="left" valign="top"><strong><?php echo $department_label ?>:</strong></td>
          	<td width="80%"  align="left" valign="top">
            	<select id="first" name="reg_dept">
  					<option value="0" >-------</option>
				</select>
    			<input type="hidden" name="m" value="patient_care" />
				<input type="hidden" name="reg_id" id="reg_id" value="" />			</td>
		</tr>
        
        <tr>
        	<td  align="left" valign="top"><strong><?php echo $clinic_label ?>:</strong></td>
            <td  align="left" valign="top">
            	<select id="second" name="reg_clinic">
                	<option value="0">-------</option>
             	</select>        	</td>
    	</tr>
        
        <tr>
          <td  align="left" valign="top"><strong><?php echo $patient_type_label ?>:</strong></td>
          <td  align="left" valign="top"><select name="patype" id="patype">
             <option value=""><?php echo $select_patient_type_label; ?></option>
           <option value="0"><?php echo $new_label; ?></option>
            <option value="1" <?php if(isset($num) && $num !=0){ ?> selected="selected"<?php } ?>><?php echo $old_label; ?></option>
          </select>          </td>
        </tr>
        
        <tr>
          <td  align="left" valign="top">&nbsp;</td>
          <td  align="left" valign="top"><input type="submit" name="button" id="button" value="<?php echo $submit_label ?>" onclick="return checkForm()" /></td>
        </tr>
	</table>
</form>
</div>
<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="library/doubleSelect.1.2/jquery.doubleSelect.min.js"></script>
<script type="text/JavaScript">
 $(document).ready(function()
 {
	    var selectoptions = {
		"<?php echo $select_department_label ?>":{
				  					"key":0,
									"values" : {"<?php echo $select_clinic_label ?>":0 	}				 },
    		<?php 
			
			$select->selectJqueryDepartAndClinic($curLangField);
			
              ?>
    };

<?php
 if(isset($myDeptid)&& $myDeptid!= "" && isset($myClinicid)&& $myClinicid !=""){?>
    var options = {
                    preselectFirst : <?php echo $myDeptid  ?>, 
                    preselectSecond : <?php echo $myClinicid ?>, 
                   };
    $('#first').doubleSelect('second', selectoptions, options);      
<?php  }else{ ?>                 
	    $('#first').doubleSelect('second', selectoptions);  
		<?
		}
		?>    
 });
 
 $(document).ready(function()
 {
	    var selectoptions = {
     		"<?php echo $select_country_label;  ?>":{
				  					"key":0,
									"values" : {"<?php echo $select_state_label ?>":0 	}				 },
	   		<?php 
				$select->selectJqueryNationalityAndState($outside_nigeria_label);
			?>
    };

    var options = {
                    preselectFirst :<?php echo $nationality_id ?>, 
                    preselectSecond :<?php echo $state_id ?>, 
                   };
    $('#third').doubleSelect('fourth', selectoptions, options);      
 });


function display(id1,id2,id3,id4){
		document.getElementById(id1).style.display = 'block';
		document.getElementById(id2).style.display = 'none';
		document.getElementById(id3).style.display = 'none';
		document.getElementById('reg_id').value = id4;
}

<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('<?php echo $search_field_label; ?>');
  document.MM_returnValue = (errors == '');
}
//-->

function checkForm(){
	if(document.register_patient.reg_dept.value=="0"){
		alert("Please select a department");
		document.register_patient.reg_dept.focus();
		return false;
	}
	
	if(document.register_patient.reg_clinic.value=="0"){
		alert("Please select a clinic");
		document.register_patient.reg_clinic.focus();
		return false;
	}

	if(document.register_patient.patype.value==""){
		alert("Please select a patient type");
		document.register_patient.patype.focus();
		return false;
	}
}
</script>

<script type="text/javascript">
	$(function() {
		$('#dob').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});
	});


</script>
