<?php
    if (!empty($_GET["comments"])){
        echo "<div class=\"comments\" style=\"padding-bottom: 20px;\">" . $_GET["comments"] . "</div>";
    }
?>

    <div>
<?php
    $appID = isset($_GET["aa"]) ? (int)$_GET["aa"] : 0;
    
    //Get the appointment & patient details
    $app = new patAppointment();
    $app_deets = $app->getAppointment($appID);
//    echo '<pre>' . print_r($app_deets, TRUE) . '</pre>';

    if (!empty($app_deets)){
        $display = "<table cellspacing=\"3\" cellpadding=\"3\" style=\"margin-bottom: 20px;\">
                        <tr>
                            <td><h3><strong>PATIENT'S NAME:</strong></h3></td>
                            <td>{$app_deets['patName']}</td>
                        </tr>
                        <tr>
                            <td><h3><strong>PATIENT'S HOSPITAL NUMBER:</strong></h3></td>
                            <td>{$app_deets['reg_hospital_no']}</td>
                        </tr>
                        <tr>
                            <td><h3><strong>APPOINTMENT DATE:</strong></h3></td>
                            <td>{$app_deets['appDate']}</td>
                        </tr>
                    </table>";
        $uploader = new FileUploader();
        $display .= $uploader->getUploadedDocuments($appID);
    } else $display = 'Appointment Not Found!';
    echo $display;
?>
    </div>