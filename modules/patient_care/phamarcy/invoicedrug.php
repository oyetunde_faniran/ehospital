<?php
/*
 Array
(
    [hosp_id] => 0000003
    [p] => invoicedrug
    [invoicetype] => 2
    [patadm_id] => 28
    [doctorpresciption] => 1. PARACETAMOL TABLET
	500mg 7/7
2. Aspirin
	200mg 2/12

    [hosp] => n
    [select30] => Array
        (
            [0] => 128
            [1] => 126
        )

    [invoicedrugs] => Open Invoice
)
 */
    if ($_POST){
        //echo "<pre>" . print_r ($_POST, true) . "</pre>";
        $drugList = isset($_POST["select30"]) ? $_POST["select30"] : "";
        $drugList = is_array($drugList) ? $drugList : array();
        $hospitalNo = isset($_POST["hosp_id"]) ? $_POST["hosp_id"] : "";
        $pat = new Patient();
        $patInfo = $pat->getPatientInfo($hospitalNo);
        if (count($drugList) > 0){
            $itemObj = new inventory_dispense();
            $dUnits = new inventory_units();
            $drugDisplay = "<table cellpadding=\"3\" cellspacing=\"3\">
                                <tr class=\"title-row\">
                                    <th align=\"left\">S/No</th>
                                    <th align=\"left\">" . $inventory_item_commercial_name . "</th>
                                    <th align=\"left\">" . $inventory_item_medical_name . "</th>
                                    <th align=\"left\">" . $inventory_manufacturer . "</th>
                                    <th align=\"left\">" . $inventory_strength . "</th>
                                    <th align=\"center\">Unit Price / Quantity</th>
                               </tr>";
            $sNo = 0;
            $juggle = true;
            $rowStyle1 = " class=\"tr-row\" ";
            $rowStyle2 = " class=\"tr-row2\" ";
            foreach ($drugList as $drugID){
                //Get the basic info about the drug
                $disDrug = $itemObj->getDrugForDispense($drugID, $inventory_locationID);

                //Get info about the units
                $disUnits = $dUnits->showInAllUnits($disDrug["quantity_in_stock"], $drugID);
                $disUnitDisplay = "<table cellpadding=\"3\" cellspacing=\"3\">
                                        <tr>
                                            <td><label>Unit</label></td>
                                            <td align=\"right\"><label>In Stock</label></td>
                                            <td align=\"right\"><label>Unit Price (&#8358;)</label></td>
                                            <td><label>Quantity</label></td>
                                        </tr>";
                foreach ($disUnits as $unit){
                    $disUnitDisplay .= "<tr>
                                            <td>" . stripslashes($unit["name"]) . "</td>
                                            <td align=\"right\">" . ($unit["quantity"]) . "</td>
                                            <td align=\"right\">" . number_format($unit["price"], 2) . "</td>
                                            <td><input type=\"text\" size=\"5\" name=\"prices[" . $drugID . "][" . $unit["id"] . "]\" id=\"prices-" . $drugID . "\" value=\"0\" /></td>
                                        </tr>";
                }
                $disUnitDisplay .= "</table>";

                $rowStyle = $juggle ? $rowStyle1 : $rowStyle2;
                $juggle = !$rowStyle1;
                $drugDisplay .= "<tr $rowStyle>
                                    <th align=\"left\">" . (++$sNo) . ".</th>
                                    <th align=\"left\">" . stripslashes($disDrug["drug_name"]) . "</th>
                                    <th align=\"left\">" . stripslashes($disDrug["drug_desc"]) . "</th>
                                    <th align=\"left\">" . stripslashes($disDrug["drug_manufacturer"]) . "</th>
                                    <th align=\"left\">" . stripslashes($disDrug["drug_strength"]) . "</th>
                                    <th align=\"center\">" . $disUnitDisplay . "</th>
                               </tr>";
            }   //END foreach drug
            $drugDisplay .= "</table>";

            $existingVars = "";
            foreach ($_POST as $key => $value){
                if (is_array($value)){
                    foreach ($value as $arrayKey => $arrayValue){
                        $existingVars .= "<input type=\"hidden\" name=\"" . $key . "[" . $arrayKey . "]\" value=\"$arrayValue\" />";
                    }
                } else {
                    $existingVars .= "<input type=\"hidden\" name=\"$key\" value=\"$value\" />";
                }
            }

            $display = "<fieldset>
                            <legend>Patient Details</legend>
                            <table cellpadding=\"3\" cellspacing=\"3\">
                                <tr>
                                    <td><label>NAME OF PATIENT:</label></td>
                                    <td>" . stripslashes($patInfo["reg_surname"] . " " . $patInfo["reg_othernames"]) . "</td>
                                </tr>
                                <tr>
                                    <td><label>HOSPITAL NUMBER OF PATIENT:</label></td>
                                    <td>$hospitalNo</td>
                                </tr>
                                <tr>
                                    <td><label>INVOICE TYPE:</label></td>
                                    <td>Drug Advice</td>
                                </tr>
                                <tr>
                                    <td><label>DATE:</label></td>
                                    <td>" . date("F d, Y") . "</td>
                                </tr>
                            </table>
                        </fieldset>

                            <fieldset>
                                <legend>Drug Details</legend>
                                <form action=\"raise-pharmacy-invoice.php\" method=\"post\">
                                    $drugDisplay
                                    $existingVars
                                    <div style=\"margin-top: 10px;\"><input type=\"submit\" class=\"btn\" name=\"sButton\" value=\"Save Invoice\"  /></div>
                                </form>
                            </fieldset>";
        } else $display = "Please, select at least one drug to be invoiced.";
    }
    echo $display;
?>

<script type="text/javascript">
    function confirmSubmit(disForm){
        if (confirm("Are you sure you want to save the invoice.\n\nYou would not be allowed to change it after saving.")){
            disForm.submt();
        }
    }
</script>