<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
$select = new patSelectOptions;
$patuser = new patUsers;
$stafftype = $patuser->getStaffType($myUserid);



//***BEGIN: Get the staff ID of the consultant to which this doctor is assigned (if any)
$thisDoc = new Doctor($curLangField, $_SESSION[session_id() . "staffID"]);

$consultantEmpIDAssignedTo = $thisDoc->getConsultantEmpIDAttachedTo();
$myStaffid = !empty($consultantEmpIDAssignedTo) ? $consultantEmpIDAssignedTo : $myStaffid;

$consultantIDAssignedTo = $thisDoc->getConsultantIDAttachedTo();
$myConsultantid = !empty($consultantIDAssignedTo) ? $consultantIDAssignedTo : $myConsultantid;
//***END



$consult_details = $patuser->isconsultant($myStaffid);
if ($stafftype == '1'){
echo $error_msg_3;
}elseif(is_array($consult_details) == FALSE){
$msg = "error_msg_".$consult_details;
echo $$msg;
}else{
extract($consult_details);
?>
<p><strong><?php echo $consultant_label ?>: <?php echo $consult_name ?></strong><br />
<strong><?php echo $clinic_label ?></strong>: <?php $clinic_detail = $select->selectClinic($myClinicid, $curLangField);
if(is_array($clinic_detail)){
	extract($clinic_detail);
	echo $clinic_name; 
}
$patdetails = $treats->getPatientDetails($app_id, $myConsultantid);
		if($patdetails === FALSE){

		}
		else{
		extract($patdetails);
		
?>
<?php 
/*	$subLinks = "<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=med01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow1\", \"scrollbars=yes, status=no, toolbar =no, menubar = no, fullscreen=yes \"); return false'>".$view_medical_history_label."</a>
					<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=app01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow2\", \"scrollbars=no, width=500, status=no, toolbar =no, menubar = no, height=400, resizeable=no\"); return false'>".$schedule_appointment_label."</a>
					<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=admit01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow3\", \"scrollbars=no, status=no, toolbar =no, menubar = no, width=600, height=300\"); return false'>".$admit_label."</a>
					<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=transfer01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow4\", \"scrollbars=no, toolbar =no, menubar = no, status=no, width=600, height=300\"); return false'>".$transfer_label."</a>";*/
	$subLinks_notNeeded = "<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=app01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow2\", \"scrollbars=no, width=500, status=no, toolbar =no, menubar = no, height=400, resizeable=no\"); return false'>".$schedule_appointment_label."</a>
					<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=admit01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow3\", \"scrollbars=no, status=no, toolbar =no, menubar = no, width=600, height=300\"); return false'>".$admit_label."</a>";

?> 
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="16%" rowspan="2" valign="top"><?php if ($passport !="") {?><img src="modules/<?php echo $m ?>/passport/<?php echo $passport ?>" border="0" />
    	<?php }else{  ?>
        <img src="modules/<?php echo $m ?>/passport/default.jpg" border="0" />
        <?php 
			}
		?>    </td>
    <td width="84%" align="left" valign="top">
    	<table cellpadding="3" cellspacing="3">
<?php
	//Patient information
	$sexLabel = $sex == "1" ? $gender_1_label : $gender_0_label;
	$patientInfo = "<tr>
						<td><strong>$hospital_no_label:</strong></td>
						<td>$hospital_no</td>
					</tr>
					<tr>
						<td><strong>$name_label:</strong></td>
						<td>$name</td>
					</tr>
					<tr>
						<td><strong>$report_appointment_age:</strong></td>
						<td>$age</td>
					</tr>
					<tr>
						<td><strong>$gender_label:</strong></td>
						<td>$sexLabel</td>
					</tr>
					<tr>
						<td><strong>$report_appointment_date:</strong></td>
						<td>$appdate</td>
					</tr>
					<tr>
						<td><strong>$report_appointment_starttime:</strong></td>
						<td>$starttime</td>
					</tr>
					<tr>
						<td><strong>$report_appointment_endtime:</strong></td>
						<td>$endtime</td>
					</tr>";
	echo $patientInfo;
?>
        </table>

      <div id="status">&nbsp;</div>

	</td>
  </tr>
</table>

<!--- Show prescription if any -->
<?php  
	$pres_row = $treats->selectPrescription($casenote_id);
	if ($pres_row !=""){
?>
	<table cellspacing="5" cellpadding="5" border="1" style="border-color: #DDD; border-collapse: collapse;">
	  <tr>
		<td colspan="3" align="left" valign="top"><h3><strong>DRUG PRESCRIPTION</strong></h3></td>
	  </tr>
	  <tr>
		<td align="left" valign="top"><strong>S/N</strong></td>
		<td align="left" valign="top"><strong>Drug</strong></td>
		<td align="left" valign="top"><strong>Dosage</strong></td>
	  </tr>
		<?php echo 	$pres_row;?>	
	</table>
<?php 
}//end prescription
?>

<!--- Show lab  test if any -->
<?php  
$labtest = $treats->selectLabtest($casenote_id);
if ($labtest !==0){ ?>
<div>&nbsp;</div>
	<table width="100%" border="0" cellspacing="5" cellpadding="5">
	  <tr>
		<td align="left"><h3><strong>LAB TEST</strong></h3></td>
	  </tr>
	  <tr>
		<td align="left"><?php echo $labtest ?></td>
	  </tr>
	</table>
<?php
}//end lab test
?>

<!--- Show other treatment if any -->
<?php  
$othertreat = $treats->selectTreatment($casenote_id);
if ($othertreat !==0){ ?>
	<table width="100%" border="0" cellspacing="5" cellpadding="5">
	  <tr>
		<td align="left"><strong>OTHER TREATMENT</strong></td>
	  </tr>
	  <tr>
		<td align="left"><?php echo $othertreat; ?></td>
	  </tr>
	</table>
<?php
}//end other treat
?>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
			  <!--<tr>
				<td width="57%" style="padding-top: 20px;"><a href="#" onclick='JAVASCRIPT: window.open ("patnewwindow_index.php?q=treat01&a=<?php //echo $patadmid ?>&b=<?php //echo $app_id ?>&c=<?php //echo $consult_id ?>&d=<?php //echo $myClinicid ?>&e=<?php //echo $casenote_id ?>", "newwindow", "scrollbars=yes, toolbar =no, menubar = no, status=no, width=600, height=300"); return false'>Print Treatment Sheet</a></td>
			  </tr>-->
              <tr>
              	<td><a href="#" onclick="window.print();">Print Treatment Sheet</a></td>
              </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
		</table>

<?
}
}
?>