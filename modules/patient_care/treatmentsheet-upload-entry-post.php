<?php
    //die ("<pre>" . print_r ($_POST, true) . print_r ($_FILES, true) . "</pre>");
	$app_id = isset($_GET["aa"]) ? (int)$_GET["aa"] : 0;
    $bigImageSaved = $thumbSaved = $logSaved = false;
    $savedDocs = $docsCount = 0;
	if (isset($_FILES["docs"]["error"]) && !empty($app_id)){
		$docsCount = count($_FILES["docs"]["error"]);
		for ($counter = 0; $counter < $docsCount; $counter++){
			if ($_FILES["docs"]["error"][$counter] == 0){
				//Resize the main file
				$fileName = $app_id . "_" . $_SESSION[session_id() . "userID"] . "_" . ($counter + 1) . "_" . md5(microtime() . admin_Tools::getRandNumber(rand(6, 20)));
				$newFile = "modules/patient_care/casenotes/$fileName";
				$thumbFile = "modules/patient_care/casenotes/thumbs/$fileName";
				$uploadHandler = new FileUploader($_FILES["docs"]["tmp_name"][$counter], $newFile);
				$bigImageSaved = $uploadHandler->saveImage($_FILES["docs"]["type"][$counter]);
				$logSaved = $uploadHandler->logCaseNote($app_id, $_POST["docsname"][$counter], $fileName . "." . $uploadHandler->getImageExtension($_FILES["docs"]["type"][$counter]));
				
				//Create a thumbnail also
				$uploadHandler->newImageLoc = $thumbFile;
				$thumbSaved = $uploadHandler->saveImage($_FILES["docs"]["type"][$counter], true, 100, 100);

                //Set the success flag to true if at least one document was saved successfully
                if ($bigImageSaved && $thumbSaved && $logSaved){
                    $savedDocs++;
                }
			}
		}
	}

    if ($savedDocs > 0){
        if ($savedDocs == $docsCount){
            $msg = "All the documents you uploaded were successfully saved.";
        } else {
            $msg = "Only $savedDocs document(s) out of $docsCount document(s) were successfully saved.";
        }

        //Set the status of the appointment to "Treated"
        $pAppoint = new patAppointment();
        $pAppoint->setAppointmentStatus($app_id);
    } else {
        $msg = "No document was saved. Please, ensure that you have selected at least one document (not more than " . FILES_MAX_SIZE_TEXT . " in size) for upload and that it is one of the following file types " . FILES_ALLOWED_TYPES . ".";
    }

    header ("Location: index.php?p=treatmentsheet_entry&m=patient_care&aa=$app_id&comments=" . urlencode($msg));

?>