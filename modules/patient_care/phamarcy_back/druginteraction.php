<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>

<?php  
$drugint = new drug_interaction();
$drug= new drug();
$hospital=new patient_admission();

//delete row with ID
if (isset($id))
 $drugint->Delete_row_from_key($_REQUEST['id']);
 
 //save new record
 if (isset($adddrugint) || !empty($adddrugint)){
 	$i=0;
	foreach ($_POST as $key=>$value) {	 
		if($key=='p' or  $key=='adddrugint' or $key=='reset'){
				
		}else{  $drugint->drugint[$i]=$key;
		
			$drugint->drugint2[$i]=$value;
			$i++;
		  }
		 
		 }
   // exit();

	$drugint->Save_Active_Row_as_New();
	}

//edit existing record
if(isset($edit)|| !empty($edit)){
$i=0;
 foreach ($_POST as $key=>$value) {	if($key=='p' or  $key=='editdrugint' or $key=='reset' or $key=='edit'){
				
		}else{  $drugint->drugint[$i]=$key;
		
			$drugint->drugint2[$i]=$value;
			$i++;
		  }
		  
		   }
		   
    $message=$drugint->Save_Active_Row($drugint_id,'drug_interaction');

}

?>


<script language="javascript" type="text/javascript">
function display(id1,id2,id3){  //show only 1st
		document.getElementById(id1).style.display = 'block';
		document.getElementById(id2).style.display = 'block';
		document.getElementById(id3).style.display = 'none';		
}

</script>
<script language="javascript" type="text/javascript">
<?php /*?>function validate(form){
var  msg='';
  if (document.form.ward_id.value=="") msg="You Need to select the Clinic.";
 if (document.form.clinic_id.value=="") msg="You Need to select the Clinic.";
  if (document.form.ward_name.value=="") msg="You Need to type the Ward name.";
   if (document.form.ward_bedspaces.value=="") msg="You Need to type the Bed Spaces.";
    if (document.form.siderooms.value=="") msg="You Need to type the Side Rooms.";
 if (!msg=''){
  alert(msg);
  return false;
 }
  alert(msg);

 document.frmWard.submit();
 return;
}<?php */?>
function validate(form){
  if (document.form.clinic_id.value == "") {
    alert( "Please select Clinic " );
    document.form.clinic_id.focus();
    return false ;
  }
   if (document.form.ward_name.value == "") {
    alert( "Please enter ward description" );
    document.form.ward_name.focus();
    return false ;
  }
  }
function doHandleAll() {
		with (document.standardView) {
			if(elements['allCheck'].checked == false){
				doUnCheckAll();
			}
			else if(elements['allCheck'].checked == true){
				doCheckAll();
			}
		}
	}

	function doCheckAll() {
		with (document.standardView) {
			for (var i=0; i < elements.length; i++) {
				if (elements[i].type == 'checkbox') {
					elements[i].checked = true;
				}
			}
		}
	}

	function doUnCheckAll() {
		with (document.standardView) {
			for (var i=0; i < elements.length; i++) {
				if (elements[i].type == 'checkbox') {
					elements[i].checked = false;
				}
			}
		}
	}

	function clear_form() {
		document.standardView.search_value.value='';
		document.standardView.search_value.options[0].selected=true;
	}
	function returnSearch() {

		if (document.standardView.ward_field.value == -1) {
			alert("Select the field to search!");
			document.standardView.ward_field.Focus();
			return false;
		};
		<?php /*?>document.standardView.capMode.value = 'SearchMode';
		document.standardView.submit();<?php */?>
		
	}
	
	function returnDelete() {
		$check = 0;
		with (document.standardView) {
			for (var i=0; i < elements.length; i++) {
				if ((elements[i].type == 'checkbox') && (elements[i].checked == true) && (elements[i].name == 'chkLocID[]')){
					$check = 1;
				}
			}
		}

		if ($check == 1){

			var res = confirm("Deletion might affect employee information, Job Titles. Do you want to delete ?");

			if(!res) return;

			document.standardView.delState.value = 'DeleteMode';
			document.standardView.pageNO.value=1;
			document.standardView.submit();
		}else{
			alert("Select at least one record to delete");
		}
	}
	
</script>
<script type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>

<BR />


	<!--<input type="hidden"  id="addWard" name="capMode" value="addWard" />-->
		
			
<table  id="addform" style="display:none" width="500"> 
<form action="./index.php" method="post" name="form"  id="form" onsubmit="MM_validateForm('drugint_sideffect','','R','drugint_contraindication','','R','drugint_indication','','R');return document.MM_returnValue"  > 
 <input type="hidden"  id="p" name="p" value="drugint" />


<?php /*?><tr>
<td align=right width="200" class="formdetails"><?php echo $ ?>:</td><td width="10"></td><td align=left width="200" class="formvalue"><input type="text" name="drugint_id" value="" size="" maxlength="" class=""></input></td>
</tr><?php */?>
<tr>
<td align=right width="200" class="formdetails"><?php echo $lang_drug ?>:</td><td width="10"></td><td align=left width="200" class="formvalue"><select type="text" name="drug_id" ><?php $drug->getdrug()?></select></td>
</tr>
<tr>
<td align=right width="200" class="formdetails"><?php echo $lang_reg_hospital_no ?></td><td width="10"></td><td align=left width="200" class="formvalue"><select name="patadm_id" ><?php $hospital->get_admNodropdown()?></select></td>
</tr>
<tr>
<td align=right width="200" class="formdetails"><?php echo $lang_drugint_sideffect ?></td><td width="10"></td><td align=left width="200" class="formvalue"><input type="text" name="drugint_sideffect" value="" size="" maxlength="" class=""></input></td>
</tr>
<tr>
<td align=right width="200" class="formdetails"><?php echo $lang_drugint_contraindication ?></td><td width="10"></td><td align=left width="200" class="formvalue"><input type="text" name="drugint_contraindication" value="" size="" maxlength="" class=""></input></td>
</tr>
<tr>
<td align=right width="200" class="formdetails"><?php echo $lang_drugint_indication ?></td><td width="10"></td><td align=left width="200" class="formvalue"><input type="text" name="drugint_indication" value="" size="" maxlength="" class=""></input></td>
</tr>
<?php /*?><tr>

<td align=right width="200" class="formdetails"><?php echo $lang_user_id ?></td><td width="10"></td><td align=left width="200" class="formvalue"><input type="text" name="user_id" value="" size="" maxlength="" class=""></input></td>
</tr><?php */?>
<tr>
<td width="200" align=right><input type=reset value="Reset" id="reset" name="reset"><input name="adddrugint"  type="submit"  id="adddrugint"value="Save"  onClick="return  validate(this)"/></td><td width="10"></td><td width="200"></td></tr>

<tr><td colspan=3></form></td></tr></table>

<form name="standardView" method="post" action="./index.php?p=drugint">

			<BR /><BR />
	<h2><?php echo $lang_title_key ?> :  <?php echo $lang_title_value_drugint ?></h2>
		
			<img 
						title="Add" 
						alt="Add" 
						src="./images/btn_add.gif" 
						style="border: none"
						onClick="display('addform','addform1','atminter');" 
						onMouseOut="this.src='./images/btn_add.gif" ;" 
						onMouseOver="this.src='./images/btn_add_02.gif';" /><?php /*?><img 
							title="Delete" 
							alt="Delete" 
							style="border: none;" 
							src="./images/btn_delete.gif"
							onclick="returnDelete();" 
							onmouseout="this.src='./images/btn_delete.gif';" 
							onmouseover="this.src='./images/btn_delete_02.gif';" />	<?php */?>											
		
		
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="22%" style="white-space: nowrap;"><h3><?php echo $lang_PageTitle_drugint ?></h3></td>
				<td width='78%' align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			</table>
	
	
			<table  border="0" cellpadding="5" cellspacing="0" class="">
				<tr>
					<td width="200" >
						<label for="loc_code" style="float: left; padding-right: 10px;">Search By:</label><br>
						<select  name="search_field">
							<?php echo $drugint->GetSearchFields()	 ?>				</select>					</td>
					<td width="300" class="dataLabel" style="white-space: nowrap;">
						<label for="search_value" style="float: left; padding-right: 10px;">Search For:</label><br>
						<input type=text size="20" name="search_value"  value="" />					</td>
					<td align="right" width="180" >
<?php /*?>						<img 
							title="Search" 
							alt="Search" 
							src="./images/btn_search.gif" 
							onclick="returnSearch();" 
							onmouseout="this.src='./images/btn_search.gif';" 
							onmouseover="this.src='./images/btn_search_02.gif';" /><?php */?>&nbsp;&nbsp;
						<img 
							title="Clear" 
							alt="Clear"
							src="./images/btn_clear.gif"
							onclick="clear_form();" 
							onmouseout="this.src='./images/btn_clear.gif';" 
							onmouseover="this.src='./images/btn_clear_02.gif';" /><input name="search" type="submit" value="<?php echo $lang_search_button_label ?>" onClick="returnSearch();"/>					</td>
				</tr>
			</table>
	
	
		<table width="100%" border="0" cellpadding="5" cellspacing="0" id="addform1" style="display:block">
			<tr style="white-space: nowrap">
				<td width="50"  style="white-space: nowrap;">
									<input type="checkbox" class="checkbox" name="allCheck" value="" onClick="doHandleAll();" /><?php //echo $lang_sno; ?></td>
				<td scope="col" width="250" ><?php echo $lang_drugint_id  ?>
					</td>
				<td  ><?php echo $lang_drugint_id	 ?>			</td>
				<?php /*?><td  >	<?php echo $lang_reg_hospital_no	 ?>			</td>	<?php */?>			
				<td  ><?php echo $lang_drugint_sideffect  ?></td>
    			<td  ><?php echo $lang_drugint_contraindication  ?></td>					
			</tr>
			<?php
			if(empty($search_field)||empty($search_value)){
			//list all
			$drugint->allrows($patadm_id);
			}else
			//return search
			$drugint->rowSearch($search_field,$search_value) ;
			?>
			 

</form>

</body>
</html>
