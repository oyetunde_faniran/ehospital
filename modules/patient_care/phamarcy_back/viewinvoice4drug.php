<?php ob_start(); ?>
<script   type="text/javascript" src="myajax4.js"></script>
<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>
<script language="JavaScript" type="text/JavaScript">
<!--
/**
 * Displays an confirmation box before to submit a "DROP DATABASE" query.
 * This function is called while clicking links
 *
 * @param   object   the link
 * @param   object   the sql query to submit
 *
 * @return  boolean  whether to run the query or not
 */
function confirmLinkDropDB(theLink, theSqlQuery)
{
    // Confirmation is not required in the configuration file
    // or browser is Opera (crappy js implementation)
    if (confirmMsg == '' || typeof(window.opera) != 'undefined') {
        return true;
    }

    var is_confirmed = confirm(confirmMsgDropDB + '\n' + confirmMsg + ' :\n' + theSqlQuery);
    if (is_confirmed) {
        theLink.href += '&is_js_confirmed=1';
    }

    return is_confirmed;
} // end of the 'confirmLink()' function

/**
 * Displays an confirmation box beforme to submit a "DROP/DELETE/ALTER" query.
 * This function is called while clicking links
 *
 * @param   object   the link
 * @param   object   the sql query to submit
 *
 * @return  boolean  whether to run the query or not
 */
function confirmLink(theLink, theSqlQuery)
{
    // Confirmation is not required in the configuration file
    // or browser is Opera (crappy js implementation)
    if (confirmMsg == '' || typeof(window.opera) != 'undefined') {
        return true;
    }

    var is_confirmed = confirm(confirmMsg + ' \n' + theSqlQuery);
    if (is_confirmed) {
        theLink.href += '&is_js_confirmed=1';
    }

    return is_confirmed;
} // end of the 'confirmLink()' function

    // js form validation stuff
    var confirmMsg  = 'Do you really want to ';

//-->
function doCheckAll() {
		//var drugqty="drugqty";
		with (document.form) {
			for (var i=0; i < elements.length; i++) {
			 
    			if (elements[i].type == "text") {
				   //alert( elements[i].type );
					 if (elements[i].value == "") {
   					 alert( "Please Fill in all the Quantity Boxes" );
  					  elements[i].focus();
  					  return false ;
  					}
				
				}
   

				
				
			}
			
		}
	}

</script>

<?php  
//constructors

$patinp= new patinp_account();
$hospital= new patient_admission();
$inp = new inpatient_admission();
$invoice = new invoice();
$invoice2 = new invoice();
$wards= new wards();
$transno=$hospital->get_tran_id();
//$Numword= new Numword();
$servicename= new service();

if(isset($_POST['savereceipt'])){
		$updateInvoice=$invoice2->updatePattotal4receipt($_POST);
		
		if(!empty($updateInvoice[2])){ $pattotal_id=$updateInvoice[2];$hosp_id=$updateInvoice[3];  }
		if($updateInvoice[1]){
			header ("Location: {$_SERVER['PHP_SELF']}?p=viewreceipt&c=Success&hosp_id=$hosp_id&pattotal_id=$pattotal_id");
		}else{
		$c="Insertion failed";
		echo $c ;
		}
}	



if(!empty($_GET['checkinvoice']))
$checkinvoice=$_GET['checkinvoice'];
$pattotal_id=$_GET['pattotal_id'];
$invoice=$invoice->getTotalpay($pattotal_id);

$Dbconnect=new DBConf();

 $sql = "Select * from   pat_serviceitem pservice inner join
 						 pat_transitem pitem inner join 
                         pat_transtotal ptotal
						 ON 
						 pitem.patservice_id=pservice.patservice_id  AND
						 pitem.pattotal_id=ptotal.pattotal_id 
		   where pitem.pattotal_id='$pattotal_id' AND ptotal.pattotal_invoice_type=2";
            $res20 = $Dbconnect->execute($sql);
			
			
$sql_nhis="SELECT rn.nhisplan_id FROM  registry r 
	INNER JOIN registry_nhis rn ON  r.reg_id=rn.reg_id 
	WHERE  r.reg_hospital_no='$hosp_id'
	";
		$result_nhis= $Dbconnect->execute($sql_nhis);
if ($Dbconnect->hasRows($result_nhis, 1)){

	$row_plan_id= mysql_fetch_array ($result_nhis);
	$nhisplan_id=$row_plan_id['nhisplan_id'];
	
}
	//echo $sql;		
$hosp_id=$_GET['hosp_id'];

$med_id=$hospital->getCurrentmedical_trans_id($hosp_id);
$deposit_purpose_id=$hospital->getDeposit_purpose($med_id);
$clinic_id=$hospital->getClinic_id2($hosp_id,$lang_curDB,$lang_curTB);
//$transno=$hospital->get_tran_id();
?>


<script language="javascript" type="text/javascript">
function display(id1,id2,id3){  //show only 1st
		document.getElementById(id1).style.display = 'block';
		document.getElementById(id3).style.display = 'none';
		document.getElementById(id2).style.display = 'block';
}

</script>




<table width="600" border="0" cellspacing="0" cellpadding="0" class="tableborder">
  <tr>
    <td><strong><?php echo $lang_invoice_no ?></strong>:<br /><?php if(!empty($med_id)) echo $invoice['pattotal_transno'] ?></td> 
	<td><strong>Date:</strong><br /><?php if(!empty($med_id)) //echo  date("M j, Y", mktime(0, 0, 0, $invoice['TRANSACTION MONTH'], $invoice['TRANSACTION DAY'],$invoice['TRANSACTION YEAR'])) 
	echo $invoice['TRANSACTION DATE'] ; ?> </td>
  </tr>
   <tr>
      <td><strong><?php echo $lang_inpat ?>:</strong><br /><?php //echo $lang_inpatname ?><?php if(!empty($med_id)) echo $hospital->getPatient_name($med_id); ?><br /><?php //echo $lang_inpataddr ?><?php if(!empty($med_id)) echo $hospital->getPatient_address($med_id,$lang_curDB,$lang_curTB); ?></td>
	   <td valign="top"><strong><?php echo $lang_inpatclinic ?>:</strong><br /><?php if(!empty($med_id))echo $hospital->getClinic_name2($hosp_id,$lang_curDB,$lang_curTB); ?><br /><br /><strong>Invoice Type:</strong><?php $lang_admcharges_invoicetype?>:<br /><?php 
	    if($invoice['pattotal_invoice_type']==1)
			$type="Admission Advice";
		 if($invoice['pattotal_invoice_type']==2)
			$type="Drug Prescriptions";
		if($invoice['pattotal_invoice_type']==3)
			$type="Registration";
		 if($invoice['pattotal_invoice_type']==4)
			$type="Lab Test";
			if(isset($type))
			echo $type;
	  ?></td>
  </tr>
  <tr>
      <td><?php //echo $lang_inpataddr ?><?php //if(!empty($med_id)) echo $hospital->getPatient_address($med_id,$lang_curDB,$lang_curTB); ?></td> <td valign="top"></td>
  </tr>
  <tr>
      <td colspan="3"><table width="95%" border="0" cellspacing="0" cellpadding="0" class="border">
  <tr class="border">
    <td class="bold"><?php echo $lang_sno ?></td>
    <td class="bold"><?php echo $lang_itemdesc ?></td>
    <td class="bold"><?php echo $lang_price ?>(&#8358;)</td>
	<td class="bold"><?php echo $lang_qty ?></td>
    <td class="bold"><?php echo $lang_Amount ?>(&#8358;)</td>
	<?php if (!empty($nhisplan_id)){?>
	 <td class="bold">Payable by NHIS<?php //echo $lang_Amount ?>(&#8358;)</td>
	 	<td class="bold">NHIS Status</td>
	 <td class="bold">Payable by patient  <?php //echo $lang_Amount ?>(&#8358;)</td>

	<?php }?>
  </tr>
 
   <?php 
   $cnt2=1;
   while ($rowservicename = mysql_fetch_array($res20)) {
    $amount=$rowservicename['patitem_totalqty']*$rowservicename['patitem_amount'];
	$amount_nhis=$rowservicename['patitem_amount_nhis'];
	?>
	
	
	<tr>
								<td ><?php echo $cnt2 ?></td>
								<td ><?php echo $rowservicename['patservice_name'] ?></td>
								<td  class="value"><?php echo number_format($rowservicename['patitem_amount'],2) ?></td>
								<td  align="center"><?php echo number_format($rowservicename['patitem_totalqty'],0) ?></td>
								<td class="value"><?php echo number_format($amount,2) ?></td>
								<?php if (!empty($nhisplan_id)){?>
										
										
										<?php if($rowservicename['patitem_paymentsource']=='1') {?>
										<td class="value"><?php echo number_format($amount - $amount_nhis,2) ?></td>
										<? }else{?>
										<td class="value">-</td>
										<? }?>
										<?php if($rowservicename['patitem_paymentsource']=='1') {
										
										?>
										
										<td class="value">Covered</td>
										<td class="value"><?php echo number_format($amount_nhis,2) ?></td>
										
										<?php }else{?>
										<td class="value">-<?php // echo number_format($amount,2) ?></td>		
										<td class="value"><?php echo number_format($amount,2) ?></td>
										
										<?php }?>		
								<?php }?>
	    	</tr>
	
	
	<?php  
	$discount=0;
	$discount=$discount+$rowservicename['patitem_discount'];
	if($rowservicename['patitem_paymentsource']=='1') 
	$thisamount=$thisamount+$amount_nhis;
	else
	$thisamount=$thisamount+$amount;
	$cnt2=$cnt2+1; 
	
	
	}?>

  
  
  <tr class="border1">
    <td colspan="4">Gross Amount</td>
   
	
	
	
    	<td class="value">&#8358;<?php echo  number_format($thisamount,2) ?></td>
		
  </tr>
  <tr>
    <td colspan="4">Less: Discount</td>
    
	
    <td class="value">&#8358;<?php echo '('.number_format($discount,2).')'; ?></td>
  </tr>
  <tr>
    <td colspan="4">Net Total</td>
	
	<td class="value">&#8358;<?php echo number_format(($thisamount-$discount),2); ?></td>
  </tr>
  <tr >
    <td  colspan="5">Amount In Word:<br /><?php if($thisamount < 1000000) echo Numword::currency($thisamount) ; ?></td>
    
  </tr>
  <?php //header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=Success");?>
  
 
</table>
</td>
  </tr>
    <tr>
    <td >Raised By: <?php echo $a3 ?></td>
    <?php  
	
	if(isset($checkinvoice) && $checkinvoice=='show'){
	?>
	<td ><?php if($invoice['pattotal_status']==0){?>
		<form action="./index.php?p=viewinvoice4drug" method="post" name="form"  id="form"> 
		
	<input type="hidden"  id="pattotal_id" name="pattotal_id" value="<?php if(!empty($pattotal_id))echo $pattotal_id ?>" />		        <input type="hidden"  id="p" name="p" value="viewinvoice4drug" />
	     <input type="hidden"  id="pattotal_paymthd" name="pattotal_paymthd" value="Branch Collect" />
		<input type="hidden"  id="transno" name="transno" value="<?php if(!empty($hosp_id))echo $transno ?>" />
		<input type="hidden"  id="hosp_id" name="hosp_id" value="<?php if(!empty($hosp_id))echo $hosp_id ?>" />
		<input type="hidden"  id="checkinvoice" name="checkinvoice" value="show" />
	    <input name="savereceipt" type="submit" value="Process Payment via Branch Collect" id="btn" onClick="return confirmLink(this, 'process this invoice and generate receipt? you may not be allowed to reverse the receipt again.')"></form>
	<?php /*?><a href="./index.php?p=viewinvoice4drug&pattotal_id=<?php echo $pattotal_id ?>&hosp_id=<?php echo $hosp_id ?>&checkinvoice=show" title="Process Payment" >Process Payment</a><?php */?>
	<?php }else{echo "Payment Status: Paid"; } ?>
	</td>
	
	<?php 
	//$checkinvoice="" ;
		 } else{
	 ?>
  <td >&nbsp;</td><?php 
  
  	} ?> 
  </tr>
   
  </table>
  

  <table>
  
  <tr>
    <td colspan="2">&nbsp;</td>
    
  </tr>
  
    
</table>	

<!--<SCRIPT type="text/javascript" src="./includes/js/jquery-1.3.2.min.js"></SCRIPT>
        <script type="text/javascript">
            $(function(){
                $('#editbtn').click(function(){
                    var da = $('#ward_id').val();
					var da2 = $('#p').val();
					var da3 = $('#clinic_id').val();
					var da4 = $('#ward_name').val();
					var da5 = $('#ward_bedspaces').val();
					var da6 = $('#ward_siderooms').val();
                    $('#mytable').load('ajax.php',{dateval:da,dateval2:da2,dateval3:da3,dateval4:da4,dateval5:da5,dateval6:da6});});
            });

 </script>
-->
<?php ob_end_flush();?>