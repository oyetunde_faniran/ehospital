<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
} // End of defined() IF.

			$reg = new patRegistration();

			$reg_id = isset($_GET["regid"]) ? (int)$_GET["regid"] : 0;
			$reg_clinic = isset($_GET["regclinic"]) ? (int)$_GET["regclinic"] : 0;
			$reg_dept = isset($_GET["regdept"]) ? (int)$_GET["regdept"] : 0;
			$patype = isset($_POST['patype']) ? $_POST['patype'] : "1";

			$reg->Load_from_key($reg_id);
			//$consultant = $reg->getConsultant($reg_clinic);

			$select = new patSelectOptions();
?>
<h3><?php echo $department_label ?>: <?php $dept_detail = $select->selectDepartment($reg_dept, $curLangField);
if(is_array($dept_detail)){
extract($dept_detail);
echo $dept_name;
}
 ?></h3>
<h4><?php echo $clinic_label ?>: <?php $clinic_detail = $select->selectClinic($reg_clinic, $curLangField);
if(is_array($clinic_detail)){
extract($clinic_detail);
echo $clinic_name;
}
 ?></h4>


<?php
	$msg = !empty($_GET["msg"]) ? $_GET["msg"] : "";
	if (!empty($msg)){
		echo "<div style=\"padding: 10px 0px 10px 0px; color: #900; font-weight: bold;\">$msg</div>";
	}
?>


<form action="index.php?p=insert_patreffer" method="post" name="outpatientadmission" enctype="multipart/form-data">
             <input name="reg_dept" type="hidden" id="reg_dept" value="<?php echo $reg_dept ?>" />
             <input name="reg_id" type="hidden" id="reg_id" value="<?php echo $reg_id ?>" />
             <input name="reg_clinic" type="hidden" id="reg_clinic" value="<?php echo $reg_clinic ?>" />
             <input name="m" type="hidden" id="m" value="patient_care" />
             <input type="hidden" name="reg[patype]" id="reg[patype]" value="<?php echo $patype ?>" />
             <input name="reg[app_starttime]" type="hidden" id="app_starttime" value="<?php echo date('Y-m-d H:i:s') ?>" />
             <input name="reg[app_endtime]" type="hidden" id="app_endtime" value="<?php echo date("Y-m-d").' '.(date("H")+1).date(":i:s") ?>" />
   <table width="600" border="0" align="center" cellpadding="0" cellspacing="10" >
		
<tr>
  <td width="158" align="left" valign="top"><strong><?php echo $hospital_no_label ?>:</strong></td>
  <td width="412" align="left" valign="top">
<?php
	$hospitalNo = $reg->getReg_hospital_no();
	if (empty($hospitalNo))
		echo "<input onkeyup=\"showMesg(this.value)\"  type=\"text\" name=\"reg[hospital_no]\" id=\"hospital_no\" />";
	else {
		echo $hospitalNo;
		echo "<input onkeyup=\"showMesg(this.value)\" type=\"hidden\" name=\"reg[hospital_no]\" value=\"$hospitalNo\" />";
	}
?>
	  <div id="message"></div>
  </td>
</tr>
<tr>
  <td align="left" valign="top"><strong><?php echo $source_referrer_label ?>:</strong></td>
  <td align="left" valign="top"><select name="reg[referrer]" id="referrer">
     <option value=""><?php echo $select_label ?> <?php echo  $referrer_label ?></option>
    <?php
				$select->selectReferrer($curLangField);

		  ?>
  </select>  </td>
</tr>
<tr>
  <td align="left" valign="top"><strong><?php echo $referring_consultant_label ?>:</strong></td>
  <td align="left" valign="top"><input type="text" name="reg[referrerdesc]" id="referrerdesc" /></td>
</tr>

<!--Patient's condition on entry-->
<!--<tr>
  <td align="left" valign="top"><strong><?php //echo $patient_condition_label ?>:</strong></td>
  <td align="left" valign="top"><select name="reg[ct_id]" id="patcondition">
     <option value=""><?php //echo $select_label ?> <?php //echo  $condition_label ?></option>
   <?php
				//$select->selectCondition($curLangField);

		  ?>
  </select>  </td>
</tr>-->


<tr>
  <td align="left" valign="top"><strong><?php echo $authorizedBy_label ?>:</strong></td>
  <td align="left" valign="top">
	<select name="reg[consultant_id]">
    	<option value=""><?php echo $select_label ?> <?php echo  $consultant_label ?></option>
<?php
	$consultantsInClinic = $select->getClinicConsultants4DD($reg_clinic);
	echo $consultantsInClinic;
?>
	</select>
  </td>
</tr>

<tr>
  <td align="left" valign="top"><strong>Consultant Assigned To:</strong></td>
  <td align="left" valign="top">
	<select name="reg[consultant_id2]">
    	<option value=""><?php echo $select_label ?> <?php echo  $consultant_label ?></option>
<?php
	echo $consultantsInClinic;
?>
	</select>
  </td>
</tr>



<tr>
	<td colspan="2">
    	<input type="checkbox" name="max_check" id="max_check" checked="checked" value="1" />
        <label for="max_check">Check if maximum number of appointments for the chosen date has been reached.</label>
    </td>
</tr>


<tr>
  <td align="left" valign="top"><strong>Appointment Date:</strong></td>
  <td align="left" valign="top">
  	<div id="date_ui"></div>
    <input type="hidden" name="app_date" id="f_date" value="" />
  </td>
</tr>



<tr>
  <td align="left" valign="top">&nbsp;</td>
  <td align="left" valign="top">
  	<input type="hidden" name="reg[user_id]"  value="<?php echo $myUserid; ?>"/>
    <input type="hidden" name="reg[ct_id]" value="0" />
    <!--<input type="hidden" name="reg[consultant_id2]"  value="<?php echo $consultant; ?>"/>
    <input type="hidden" name="reg[consultant_id]" value="1" />
    <input type="hidden" name="reg[consultant_id2]" value="1" />-->
    <input type="submit" name="submit" class="btn" id="submit" value="<?php echo $continue_label ?>" onclick="return checkForm()" />
  </td>
</tr>
<tr>
  <td align="left" valign="top">&nbsp;</td>
  <td align="left" valign="top">&nbsp;</td>
</tr>
</table>
</form>

<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/smoothness/ui.core.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/smoothness/ui.base.css" rel="stylesheet" />
<script type="text/javascript" src="library/doubleSelect.1.2/jquery-1.3.2.min.js"></script>

<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>

<script type="text/javascript" src="library/gen_validatorv31.js"></script>
<script type="text/javascript">

	var frmvalidator  = new Validator("outpatientadmission");

	frmvalidator.addValidation("reg[referrer]", "req", "Please, select the patients' source of referrer.");
	frmvalidator.addValidation("reg[referrerdesc]", "req", "Please, enter the name of the referring physician.");
	frmvalidator.addValidation("reg[consultant_id]", "req", "Please, select the name of the consultant that authorized the registration of the patient.");
	frmvalidator.addValidation("reg[consultant_id2]", "req", "Please, select the consultant to which the patient would be attached.");
/*	frmvalidator.addValidation("FirstName","alpha","Alphabetic chars only");
	
	frmvalidator.addValidation("LastName","req","Please enter your Last Name");
	frmvalidator.addValidation("LastName","maxlen=20","Max length is 20");
	
	frmvalidator.addValidation("Email","maxlen=50");
	frmvalidator.addValidation("Email","req");
	frmvalidator.addValidation("Email","email");*/


	function checkForm(){
		if(document.outpatientadmission.hospital_no.value==""){
			alert("Please enter Hospital Number");
			document.outpatientadmission.hospital_no.focus();
			return false;
		}
		
		if(document.outpatientadmission.patcondition.value==""){
			alert("Please select patient's state");
			document.outpatientadmission.patcondition.focus();
			return false;
		}
	}
	
	$(function() {
		$('#date_ui').datepicker({
			changeMonth:	true,
			changeYear:		true,
			minDate:		0,
			dateFormat:		'yy-mm-dd',
			altField:		'#f_date',
			altFormat:		'yy-mm-dd'
		});
	});
	
</script>