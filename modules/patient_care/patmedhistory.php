<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
$reg = new patRegistration;
$treat = new patTreatment;
$patuser = new patUsers;
$stafftype = $patuser->getStaffType($myUserid);
$patdetails = $reg->loadPatientDetails($bb);
$consult_details = $patuser->isconsultant($myStaffid);
if ($stafftype == '1'){
echo $error_msg_3;
}elseif(is_array($consult_details) == FALSE){
$msg = "error_msg_".$consult_details;
echo $$msg;
}else{
extract($consult_details);
?>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="16%" rowspan="2"><?php if ($reg->getReg_passport() !="") {?><img src="modules/<?php echo $m ?>/passport/<?php echo $reg->getReg_passport() ?>" border="0" />
    	<?php }else{  ?>
        <img src="modules/<?php echo $m ?>/passport/default.jpg" border="0" />
        <?php 
			}
		?>    </td>
    <td width="84%" align="left" valign="top"><strong><?php echo $name_label ?>: </strong> <?php echo $reg->getReg_surname()." ".$reg->getReg_othernames() ?>&nbsp;&nbsp;
	<strong><?php echo $hospital_no_label ?>: </strong> <?php echo $reg->getReg_hospital_no(); ?><br />      
    <strong><?php echo $report_appointment_age ?>: </strong><?php echo $reg->getReg_age() ?>&nbsp;&nbsp;<strong><?php echo $gender_label ?>: </strong>
              <?php if($reg->getReg_gender()==0) echo $gender_0_label; ?>
           <?php if($reg->getReg_gender()==1) echo $gender_1_label; ?>                 
      <span id="status">&nbsp;</span>
    </td>
  </tr>
</table>
<div class="headerText"><strong><?php echo "Medical History" ?></strong></div>
  
<?php 
	$visits = $treat->getCases($hospital_no, $consult_id);
	if($visits != ""){
?>
<table width="100%" border="0" cellpadding="5" cellspacing="0">
  <tr>
    <td><strong><?php echo $department_label ?></strong></td>
    <td><strong><?php echo $consultant_label ?></strong></td>
    <td><strong><?php echo $patient_type_label ?></strong></td>
    <td><strong><?php echo $date_attended_label ?></strong></td>
    <td><strong><?php echo $date_discharged_label ?></strong></td>
   <td>&nbsp;</td>
  </tr>
  <?php
	echo $visits;
?>
</table>
<?php
	
}else{
 echo $no_previous_visits;
}
}
?>