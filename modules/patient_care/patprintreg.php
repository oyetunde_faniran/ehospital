<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
			
$hospital_no = $_GET['a'] ? $_GET['a'] : 0;
$clinic_id = $_GET['b'] ? $_GET['b'] : 0;
$dept_id = $_GET['c'] ? $_GET['c'] : 0;
$consultant_id = $_GET['d'] ? $_GET['d'] : 0;
$select = new patSelectOptions;
$reg = new patRegistration;
$reg->loadPatientDetails($hospital_no);
$patuser = new patUsers;
$consult_details = $patuser->isconsultant($consultant_id);
if(is_array($consult_details)){
extract($consult_details);
}
?>
<table width="100%" border="0">
   <tr>
    <td colspan="2" align="center"><h2><?php echo $appTitle; ?></h2>
								  <h3> <?php echo $department_label; ?>: 
								  <?php $dept_detail = $select->selectDepartment($dept_id, $curLangField);
										if(is_array($dept_detail)){
										extract($dept_detail);
										echo $dept_name; 
										}
								?></h3>
	                              <?php echo date('F d, Y'); ?>
								  </td>
  </tr>
  <tr>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="26%"><strong><?php echo $hospital_no_label; ?>:</strong></td>
    <td width="74%"><?php echo $hospital_no ?></td>
  </tr>
  <tr>
    <td><strong><?php echo $name_label; ?>:</strong></td>
    <td><?php echo strtoupper($reg->getReg_surname())." ".$reg->getReg_othernames(); ?></td>
  </tr>
  <tr>
    <td><strong><?php echo $clinic_label; ?>:</strong></td>
    <td><?php $clinic_detail = $select->selectClinic($clinic_id, $curLangField);
				if(is_array($clinic_detail)){
				extract($clinic_detail);
				echo $clinic_name; 
				}
		?>
	</td>
  </tr>
  <tr>
    <td><strong><?php echo $consultant_label; ?>:</strong></td>
    <td><?php echo $consult_name; ?></td>
  </tr>
</table>
    <div style="padding: 10px 10px 0 0; float:right;"><a href="#" onclick="window.print(); return false"><?php echo $patreg_printnow_label; ?></a></div>
