<?php
   //echo ("<pre>" . print_r ($_GET, true) . "</pre>");
   include_once ("./includes/pagination-init-health-records.php");
   $pats = new Patient();
   $sort = isset($_GET["sort"]) ? (int)$_GET["sort"] : "";
   $sortDirection = isset($_GET["d"]) ? (int)$_GET["d"] : "";
   $searchArray = array(
                        "sort" => $sort,
                        "sort-direction" => $sortDirection,
                        "getArray" => isset($_GET) ? $_GET : array()
                    );
   //For Pagination
   $totalRecords = $args["total"] = $pats->getPatients4Preview($searchArray, true);
   $paginationText = Paginator::doPagination($args);
   
   $allPats = $pats->getPatients4Preview($searchArray);
   if (count($allPats) > 0){
       $display = "<table cellpadding=\"3\" cellspacing=\"3\" border=\"1\" style=\"margin-top: 20px;\">
                        <tr class=\"title-row\">
                            <td>S/NO</td>
                            <td>
                                HOSPITAL NUMBER
                                <a href=\"" . $_SERVER["REQUEST_URI"] . "&sort=1&d=1" . "\"><img src=\"images/sort-asc.png\" alt=\"&or;\" title=\"Sort Ascending\" /></a>
                                <a href=\"" . $_SERVER["REQUEST_URI"] . "&sort=1&d=2" . "\"><img src=\"images/sort-desc.png\" alt=\"&and;\" title=\"Sort Descending\" /></a>
                            </td>
                            <td>
                                SURNAME
                                <a href=\"" . $_SERVER["REQUEST_URI"] . "&sort=2&d=1" . "\"><img src=\"images/sort-asc.png\" alt=\"&or;\" title=\"Sort Ascending\" /></a>
                                <a href=\"" . $_SERVER["REQUEST_URI"] . "&sort=2&d=2" . "\"><img src=\"images/sort-desc.png\" alt=\"&and;\" title=\"Sort Descending\" /></a>
                            </td>
                            <td>
                                OTHER NAMES
                                <a href=\"" . $_SERVER["REQUEST_URI"] . "&sort=3&d=1" . "\"><img src=\"images/sort-asc.png\" alt=\"&or;\" title=\"Sort Ascending\" /></a>
                                <a href=\"" . $_SERVER["REQUEST_URI"] . "&sort=3&d=2" . "\"><img src=\"images/sort-desc.png\" alt=\"&and;\" title=\"Sort Descending\" /></a>
                            </td>
                            <td>
                                GENDER
                                <a href=\"" . $_SERVER["REQUEST_URI"] . "&sort=4&d=1" . "\"><img src=\"images/sort-asc.png\" alt=\"&or;\" title=\"Sort Ascending\" /></a>
                                <a href=\"" . $_SERVER["REQUEST_URI"] . "&sort=4&d=2" . "\"><img src=\"images/sort-desc.png\" alt=\"&and;\" title=\"Sort Descending\" /></a>
                            </td>
                            <td>
                                DATE OF BIRTH
                                <a href=\"" . $_SERVER["REQUEST_URI"] . "&sort=5&d=1" . "\"><img src=\"images/sort-asc.png\" alt=\"&or;\" title=\"Sort Ascending\" /></a>
                                <a href=\"" . $_SERVER["REQUEST_URI"] . "&sort=5&d=2" . "\"><img src=\"images/sort-desc.png\" alt=\"&and;\" title=\"Sort Descending\" /></a>
                            </td>
                            <td>
                                TELEPHONE
                                <a href=\"" . $_SERVER["REQUEST_URI"] . "&sort=6&d=1" . "\"><img src=\"images/sort-asc.png\" alt=\"&or;\" title=\"Sort Ascending\" /></a>
                                <a href=\"" . $_SERVER["REQUEST_URI"] . "&sort=6&d=2" . "\"><img src=\"images/sort-desc.png\" alt=\"&and;\" title=\"Sort Descending\" /></a>
                            </td>
                            <td>
                                <em>Full Details / Edit</em>
                            </td>
                        </tr>";
       $styleFlag = true;
       $rowStyle1 = " class=\"tr-row\" ";
       $rowStyle2 = " class=\"tr-row2\" ";
       $sNo = $limitStart;
       $disRequest = urlencode($_SERVER["REQUEST_URI"]);
       foreach ($allPats as $pat){
           //echo ("<pre>" . print_r ($pat, true) . "</pre>");
           $rowStyle = $styleFlag ? $rowStyle1 : $rowStyle2;
           $styleFlag = !$styleFlag;
           $display .= "<tr $rowStyle>
                            <td>" . (++$sNo) . ".</td>
                            <td>" . stripslashes($pat["reg_hospital_no"]) . "</td>
                            <td>" . stripslashes($pat["reg_surname"]) . "</td>
                            <td>" . stripslashes($pat["reg_othernames"]) . "</td>
                            <td>" . ($pat["reg_gender"] == 1 ? "Female" : "Male") . "</td>
                            <td>" . stripslashes($pat["dob"]) . "</td>
                            <td>" . stripslashes($pat["reg_phone"]) . "</td>
                            <td>
                                <a href=\"index.php?p=patients_list_view_individual&m=patient_care&id=" . $pat["reg_id"] . "&patients_prev=$disRequest\"><img src=\"images/view.png\" alt=\"View\" title=\"View Full Details\" /></a>
               
                                <a target=\"_blank\" href=\"index.php?m=patient_care&p=edit_reg&regid=" . $pat["reg_id"] . "&jed=1\"><img src=\"images/edit.png\" alt=\"View\" title=\"Edit Details\" /></a>
                            </td>
                        </tr>";
       }
       $display .= "</table>";
       $paginationText = "<div class=\"pagination-container\">" . $paginationText . "</div>";
       $display = $paginationText . $display . $paginationText;

       $display = "<h4>$totalRecords total record(s) found.</h4>" . $display;

       //$display = "<div style=\"margin: 10px 0px;\"><a href=\"index.php?p=patients_list&m=patient_care\">Show All Records (Do Not Filter)</a></div>" . $display;

       //include_once (dirname(__FILE__) . "\\patients-list-all-search-inc-file.php");
       //echo dirname(__FILE__) . "/patients-list-all-search-inc-file.php";
   } else $display = "No patients added yet or none matched your search criteria.";

   include_once ("./modules/patient_care/patients-list-all-search-inc-file.php");
   $display = "<div style=\"margin: 10px 0px;\"><a href=\"index.php?p=patients_list&m=patient_care\">Show All Records (Do Not Filter)</a></div>" . $display;
   echo $display;
?>

