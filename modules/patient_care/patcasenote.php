<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>
<h4><?php echo $page_title; ?></h4>

<?php 
extract($patuser_detail);
$consult_details = $patuser->isconsultant($patuser_empid);
if ($patuser_stafftype == '1'){
echo $error_msg_3;
}elseif(is_array($consult_details) == FALSE){
$msg = "error_msg_".$consult_details;
echo $$msg;
}else{
extract($consult_details); 
 ?>
 
<p><strong><?php echo $logged_in_as_label ?> <?php echo $patuser_name ?></strong><br />
<strong><?php echo $clinic_label ?></strong>: <?php echo $clinic_label ?>: <?php $clinic_detail = $select->selectClinic($reg_clinic, $curLangField);
if(is_array($clinic_detail)){
extract($clinic_detail);
echo $clinic_name;
} ?></p>
 
<form action="index.php" method="post" name="search_patient" enctype="multipart/form-data">
      <table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" >
      	<tr>
          <td align="left" valign="top"><?php echo $hospital_no_label ?>:&nbsp;&nbsp;
            <input name="patsearch" type="text" id="patsearch" size="25" />&nbsp;
           <input name="search1" id="search1" type="submit" value="<?php echo $submit_label ?>" />
            <input type="hidden" name="m" value="patient_care" />
            <input type="hidden" name="p" value="register_patient" />
 		  </td>
        </tr>
 		<tr>
          <td>&nbsp;
          
          </td>
        </tr>
 		<tr>
          <td>&nbsp;
          
          </td>
        </tr>
	  </table>
 </form>
 
 
      <table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" >
      	<tr>
          <td colspan="4" align="left" valign="top"><strong><?php echo $today_appointment_label ?></strong></td>
        </tr>
		<?php 
        $app_details = viewAppointment(date("Y-m-d"), $dept_id=0, $consult_clinic, $consult_id);
		$view = count($app_details );
        if ($view>0){
		extract($view);
		?>
        <tr>
          <td align="left">Name</td>
          <td align="left">Start Time</td>
          <td align="left">End Time</td>
          <td align="left">Action</td>
  		</tr>
		<?php
		for($i=0; $i<$view; $i++){
         ?>
  		<tr>
          <td align="left">&nbsp;</td>
          <td align="left"><?php echo $app_start ?></td>
          <td align="left"><?php echo $app_end ?></td>
          <td align="left">&nbsp;</td>
  		</tr>
        <?php
		}
		}else{
		?>
 		<tr>
          <td colspan="4" align="center">
          	<?php echo $no_appointments_label ?>          </td>
        </tr>
        <?php
		}
		?>
	  </table>
 
 
 
<?php 
}
?>
