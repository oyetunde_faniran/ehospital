<?php
    $patID = isset($_GET["id"]) ? $_GET["id"] : 0;
    $id_type = isset($_GET["idt"]) ? (int)$_GET["idt"] : 0;
    $show_back_link = isset($_GET["s"]) ? (int)$_GET["s"] : 1;
    $actual_id_type = $id_type == 0 ? 'reg_id' : 'hospital-number';
    $pat = new Patient();
    $disPat = $pat->getFullPatientInfo($patID, $actual_id_type);
    //echo ("<pre>" . print_r ($disPat, true) . "</pre>");
    if (is_array($disPat)){
?>
            <div>
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1"><?php echo $personal_detail_label ?> I</a></li>
                        <li><a href="#tabs-2"><?php echo $personal_detail_label ?> II</a></li>
                        <li><a href="#tabs-3"><?php echo $next_of_kin_label ?></a></li>
                    </ul>
                    <div id="tabs-1">
                        <table cellpadding="2" cellspacing="0">
                            <tr>
                                <td width="15%" align="left" valign="top"><strong><?php echo $surname_label ?>:</strong></td>
                                <td width="35%" align="left" valign="top"><?php echo stripslashes($disPat["reg_surname"]); ?></td>
                                <td width="15%" align="left" valign="top"><strong><?php echo $othernames_label ?>:</strong></td>
                                <td width="35%" align="left" valign="top"><?php echo stripslashes($disPat["reg_othernames"]); ?></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong><?php echo $dob_label ?>:</strong></td>
                                <td align="left" valign="top"><?php echo stripslashes($disPat["dob"]); ?></td>
                                <td align="left" valign="top"><strong><?php echo $gender_label ?>:</strong></td>
                                <td align="left" valign="top"><?php echo stripslashes($disPat["sex"]); ?></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong><?php echo $phone_label ?>:</strong></td>
                                <td align="left" valign="top"><?php echo stripslashes($disPat["reg_phone"]); ?></td>
                                <td align="left" valign="top"><strong><?php echo $email_label ?>:</strong></td>
                                <td align="left" valign="top"><?php echo stripslashes($disPat["reg_email"]); ?></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong><?php echo $address_label ?>:</strong></td>
                                <td colspan="3" align="left" valign="top"><?php echo nl2br($disPat["reg_address"]); ?></td>
                            </tr>
                        </table>
                    </div>



                    <div id="tabs-2">
                         <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
                            <tr>
                                  <td width="20%" align="left" valign="top"><strong><?php echo $nationality_label ?>:</strong></td>
                                  <td width="80%" align="left" valign="top"><?php echo stripslashes($disPat["nationality"]); ?></td>
                            </tr>

                            <tr>
                              <td width="20%" align="left" valign="top"><strong><?php echo $state_label ?>:</strong></td>
                              <td width="80%" align="left" valign="top"><?php echo stripslashes($disPat["state"]); ?></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top"><strong><?php echo $civilstate_label ?>:</strong></td>
                              <td align="left" valign="top"><?php echo stripslashes($disPat["maritalStatus"]); ?></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top"><strong><?php echo $religion_label ?>:</strong></td>
                              <td align="left" valign="top"><?php echo stripslashes($disPat["regextra_religion"]); ?></td>
                            </tr>

                            <tr>
                              <td align="left" valign="top"><strong><?php echo $occupation_label ?>:</strong></td>
                              <td align="left" valign="top"><?php echo stripslashes($disPat["regextra_occupation"]); ?></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top"><strong><?php echo $placeofwork_label ?>:</strong></td>
                              <td align="left" valign="top"><?php echo nl2br($disPat["regextra_placeofwork"]); ?></textarea></td>
                            </tr>
                        </table>
                    </div>


                     <div id="tabs-3">
                         <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
                            <tr>
                              <td width="20%" align="left" valign="top"><strong><?php echo $name_label ?>:</strong></td>
                              <td width="80%" align="left" valign="top"><?php echo stripslashes($disPat["regextra_nextofkin"]); ?></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top"><strong><?php echo $nokrelationship_label ?>:</strong></td>
                              <td align="left" valign="top"><?php echo stripslashes($disPat["regextra_nokrelationship"]); ?></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top"><strong><?php echo $phone_label ?>:</strong></td>
                              <td align="left" valign="top"><?php echo stripslashes($disPat["regextra_nokphone"]); ?></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top"><strong><?php echo $email_label ?>:</strong></td>
                              <td align="left" valign="top"><?php echo stripslashes($disPat["regextra_nokemail"]); ?></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top"><strong><?php echo $address_label ?>:</strong></td>
                              <td align="left" valign="top"><?php echo nl2br($disPat["regextra_nokaddress"]); ?></td>
                            </tr>
                        </table>
                    </div>



                </div>
            </div>










        <link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
        <script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/ui.sortable.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/ui.tabs.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
        <script type="text/javascript">
            $(function() {
                $('#fromdate').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd'
                });
            });

            $(function() {
                $('#todate').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd'
                });
            });

            $(function() {
                $('#dob-from').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd'
                });
            });

            $(function() {
                $('#dob-to').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd'
                });
            });

            $(function() {
                $("#accordion").accordion({
                                              collapsible: true,
                                              active: false,
                                              clearStyle: true
                                          });
            });

            $(function() {
                $("#tabs").tabs().find(".ui-tabs-nav");
            });
        </script>
<?php
    } else echo "Unknown Patient";
    if ($show_back_link){
    $patListPage = isset($_GET["patients_prev"]) ? $_GET["patients_prev"] : "index.php?p=patients_list&m=patient_care";
        echo "<div style=\"margin-top: 10px;\">
                  <a href='$patListPage'>&laquo; Back to List of Patients</a>
              </div>";
    }
?>