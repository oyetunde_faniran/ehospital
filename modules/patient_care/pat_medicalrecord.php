<?php
//    die ('<pre>' . print_r($_SESSION, true));
    $treat_quest = new TreatmentQuestions();
    function getCheckBoxes($field_code, $parent){
        //DIFFERENTIAL_DIAGNOSIS, DIAGNOSIS
        global $treat_quest;
        $questions = $treat_quest->getTreatmentQuestions($field_code, $parent);
        $cboxes = '';
        foreach($questions as $quest){
            $cbox_id = "treatment-question-" . $quest['quest_id'];
            $label_id = "treatment-question-label-" . $quest['quest_id'];
            $container_id = "treatment-question-container-" . $quest['quest_id'];
            $cboxes .= '<div id="' . $container_id . '">
                            <input id="' . $cbox_id . '" type="checkbox" value="' . addslashes($quest['quest_question']) . '" name="treatment_question[' . $quest['treatfield_id'] . '][' . $quest['quest_id'] . ']" onclick="saveCheckBoxInForm(\'' . $cbox_id . '\', \'treatment_question[' . $quest['treatfield_id'] . '][' . $quest['quest_id'] . ']\', \'' . addslashes($quest['quest_question']) . '\');" />
                            <label id="' . $label_id . '" for="' . $cbox_id . '">' . $quest['quest_question'] . '</label>
                            <!--<span style="margin-left: 10px;"><img style="cursor: pointer;" src="images/edit.png" alt="Edit" title="Edit" onclick="editCheckBoxEntry(' . $quest['quest_id'] . ');" /></span>-->
                            <span style="margin-left: 10px;"><img style="cursor: pointer;" src="images/delete.png" alt="Delete" title="Delete" onclick="deleteCheckBoxEntry(' . $quest['quest_id'] . ');" /></span>
                       </div>';
        }
        return !empty($cboxes) ? $cboxes : 'No standard questions here. Use typing instead.';
    }   //END getCheckBoxes()

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');

	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;

} // End of defined() IF.
?>
<style type="text/css">
	.suggestionsBox {
		position: relative;
		left: 30px;
		margin: 0px 0px 0px 0px;
		width: 200px;
		background-color: #212427;
		-moz-border-radius: 7px;
		-webkit-border-radius: 7px;
		border: 2px solid #000;
		color: #fff;
	}

	.suggestionList {
		margin: 0px;
		padding: 0px;
	}

	.suggestionList li {

		margin: 0px 0px 3px 0px;
		padding: 3px;
		cursor: pointer;
	}

	.suggestionList li:hover {
		background-color: #659CD8;
	}
 #multiTransferExample a {
  display: block;
  border: 1px solid #aaa;
  text-decoration: none;
  background-color: #fafafa;
  color: #123456;
  margin: 2px;
  clear: both;
 }
 #multiTransferExample div {
  float:left;
  text-align: center;
  margin: 10px;
 }
 #multiTransferExample select {
  width: 100px;
  height: 80px;
 }

</style>

<script type="text/javascript" src="library/auto_complete/jquery-1.2.1.pack.js"></script>
<script type="text/javascript">
	function lookup(inputString) {
		if(inputString.length == 0) {
			// Hide the suggestion box.
			$('#suggestions').hide();
		} else {
			$.post("classes/patRPC.php", {queryString: ""+inputString+""}, function(data){
				if(data.length >0) {
					$('#suggestions').show();
					$('#autoSuggestionsList').html(data);
				}
			});
		}
	} // lookup

	function fill(thisValue) {
		$('#inputString').val(thisValue);
		setTimeout("$('#suggestions').hide();", 200);
	}



		function AddSub(dir) {
		var dd ;
			var selsub=document.getElementById("inputString");
			var selsub2=document.getElementById("inputString2");
			if (selsub.value.length<1){
				alert("Enter a drugname please") }
				else if (selsub2.value.length<1){
				alert("Enter drug dosage please") }
				else{
				if(dir==1) {

						if (document.casenote.sub1.value.length>0) {
						 dd=document.casenote.sub1.value;
							 }else{
							 dd="";
							 }
						var dd1=selsub.value+" ["+selsub2.value+"]";
						document.casenote.sub1.value=dd+dd1+"\n";
						document.casenote.inputString.value="";
						document.casenote.inputString2.value="";
						 }

			}
		}

		function ClearSub(){
		  document.casenote.sub1.value="";
		}

		function setValue(whichDoc){
			t = document.getElementById("docsname" + whichDoc);
			if (t.value == "Type Document Description Here"){
				t.value = "";
			}
		}

		nextDocument = 2;
		function addMoreDocuments(){
			try {
				cont = document.getElementById("docscontainer");
				newDoc = "<div style=\"padding-bottom: 10px;\">";
				newDoc = newDoc + " <input type=\"text\" name=\"docsname[]\" id=\"docsname" + nextDocument + "\" size=\"40\" style=\"color: #666;\" value=\"Type Document Description Here\" onclick=\"setValue(" + nextDocument + ");\" />";
				newDoc = newDoc + " <input type=\"file\" name=\"docs[]\" id=\"docs" + nextDocument + "\"  size=\"20\" />";
				newDoc = newDoc + " <input type=\"button\" value=\"Remove Document\" class=\"btn\" onclick=\"removeDocument(" + nextDocument + ");\" />";
				newDoc = newDoc + "</div>";

				newDiv = document.createElement("div");
				newDiv.id = "uploaddiv" + nextDocument;
				newDiv.className = "uploadDivs";
				newDiv.innerHTML = newDoc;
				cont.appendChild(newDiv);

				nextDocument++;
			} catch (e) {
				alert ("An error occurred while trying to add a new document field.");
			}
		}

		function removeDocument(whichDoc){
			c = "uploaddiv" + whichDoc;
			//p = "docscontainer";
			try {
				c = document.getElementById(c);
				//p = document.getElementById(p);
				c.parentNode.removeChild(c);
			} catch (e) {
				alert ("An error occurred while trying to add a new document field.");
			}
		}

</script>
<?php
	$newAppLink = $admitLink = $transferLink = "";
	$patuser = new patUsers;
	$select = new patSelectOptions;

	//Get the staff type (clinical / non-clinical)
	$stafftype = $patuser->getStaffType($myUserid);

	//Get the appointment id
	$app_id = isset($_GET['aa']) ? $_GET['aa'] : 0;

	$treat = new patTreatment();

	//if doctor searched for patient by hospital number, get the id of any unattended appointment for the patient
	if(isset($_POST['findpatient'])){
		$app_id = $treat->getAppID($_POST['findpatient']);
		if ($app_id == "")
			echo "<font color='#FF0000' style='font-weight:bold'>" . $patient_not_on_appointment_list . "</font>";
	}

	//Get consultant details

	//$consult_details = $patuser->isconsultant($myStaffid); //die ("<pre>" . print_r ($consult_details, true) . "</pre>");



	//Get the staff ID of the consultant to which this doctor is assigned (if any)
	$thisDoc = new Doctor($curLangField, $_SESSION[session_id() . "staffID"]);
	$consultantEmpIDAssignedTo = $thisDoc->getConsultantEmpIDAttachedTo();
	$myStaffid = !empty($consultantEmpIDAssignedTo) ? $consultantEmpIDAssignedTo : $myStaffid;



	$consult_details = $patuser->isconsultant($myStaffid);
	if ($stafftype == '1'){
		echo $error_msg_3;
	} elseif (is_array($consult_details) == FALSE){
		$msg = "error_msg_" . $consult_details;
		echo $$msg;
	} else {
		extract($consult_details);
?>
<!--<p><strong><?php //echo $consultant_label ?>: <?php //echo $consult_name ?></strong><br />
<strong><?php //echo $clinic_label ?></strong>: <?php /*$clinic_detail = $select->selectClinic($myClinicid, $curLangField);
if(is_array($clinic_detail)){
extract($clinic_detail);
echo $clinic_name;
}*/?></p>-->


<?php
//die ((string)$app_id . "--");
	if (!empty($app_id)){
		$patdetails = $treat->getPatientDetails($app_id, $consult_id);
		//die ("<pre>" . print_r($patdetails, true) . "</pre>");
		if($patdetails == false){
			echo $patient_not_on_appointment_list;
		} elseif (is_array($patdetails)){
			extract($patdetails);
/*			$subLinks = "<!--<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=med01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow1\", \"scrollbars=yes, status=no, toolbar =no, menubar = no, fullscreen=yes \")'>".$view_medical_history_label."</a>-->
							<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=app01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow2\", \"scrollbars=no, width=500, status=no, toolbar =no, menubar = no, height=400, resizeable=no\")'>".$schedule_appointment_label."</a>
							<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=admit01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow3\", \"scrollbars=no, status=no, toolbar =no, menubar = no, width=600, height=300\")'>".$admit_label."</a>
							<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=transfer01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow4\", \"scrollbars=no, toolbar =no, menubar = no, status=no, width=600, height=300\")'>".$transfer_label."</a>";*/
			$moreParams = "&a=" . $patadmid."&b=" . $hospital_no . "&c=" . $consult_id . "&d=" . $consult_clinic;
			$newAppLink = "patnewwindow_index.php?q=app01" . $moreParams;
			$admitLink = "patnewwindow_index.php?q=admit01" . $moreParams;
			$transferLink = "patnewwindow_index.php?q=transfer01" . $moreParams;
			$subLinks_notNeeded = "<a target=\"_blank\" href=\"$newAppLink\" >" . $schedule_appointment_label . "</a>
									<a target=\"_blank\" href=\"$admitLink\" >" . $admit_label . "</a>
									<!--<a target=\"_blank\" href=\"$transferLink\" >" . $transfer_label . "</a>-->";
?>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="16%" rowspan="2" valign="top">
<?php
		if ($passport != "")
			echo "<img src=\"modules/$m/passport/$passport\" border=\"0\" />";
		else echo "<img src=\"modules/$m/passport/default.jpg\" border=\"0\" />";
?>
	</td>
    <td width="84%" align="left" valign="top">
        <table cellspacing="3" cellpadding="3">
          <tr>
            <td style="border-right: 1px solid #999; border-left: 1px solid #999; padding: 0px 20px 0px 20px; vertical-align: top;">
<!-- BEGIN Patient Details -->
				<h3><u><strong>PATIENT'S PERSONAL DETAILS</strong></u></h3>
                <table cellspacing="3" cellpadding="3">
                  <tr class="tr-row">
                    <th align="left" scope="row"><?php echo $name_label ?>:</th>
                    <td align="left"><?php echo $name ?></td>
                  </tr>
                  <tr class="tr-row2">
                    <th align="left" scope="row"><?php echo $hospital_no_label ?>:</th>
                    <td align="left"><?php echo $hospital_no; ?></td>
                  </tr>
                  <tr class="tr-row">
                    <th align="left" scope="row"><?php echo $report_appointment_age ?>:</th>
                    <td align="left"><?php echo $age ?></td>
                  </tr>
                  <tr class="tr-row2">
                    <th align="left" scope="row"><?php echo $gender_label ?>:</th>
                    <td align="left"><?php if($sex == 0) echo $gender_0_label; ?><?php if($sex == 1) echo $gender_1_label; ?></td>
                  </tr>
                  <tr class="tr-row">
                    <th align="left" scope="row">Phone:</th>
                    <td align="left"><?php echo $patient_phone; ?></td>
                  </tr>
                  <tr class="tr-row2">
                    <th align="left" scope="row"><?php echo $report_appointment_date ?>:</th>
                    <td align="left"><?php echo $appdate ?></td>
                  </tr>
                  <tr class="tr-row">
                    <th align="left" scope="row"><?php echo $report_appointment_starttime ?>:</th>
                    <td align="left"><?php echo $starttime ?></td>
                  </tr>
                  <tr class="tr-row2">
                    <th align="left" scope="row"><?php echo $report_appointment_endtime ?>:</th>
                    <td align="left"><?php echo $endtime; ?></td>
                  </tr>
                </table>
<!-- END Patient Details -->
            </td>


            <td style="padding-left: 20px; vertical-align: top;">
<!-- BEGIN Patient Vital Signs -->
				<h3><u><strong>PATIENT'S CURRENT VITAL SIGNS</strong></u></h3>
<?php
	$vitalSigns = new patVitalSigns();
	$vSigns = $vitalSigns->getVitalSignsDeets($patadmid);
	$vitalSignsID = 0;
	//echo "<pre>" . print_r ($vSigns, true) . "</pre>";
	if (is_array ($vSigns)){
        //Get the row ID of the vital signs measurement so that it can be updated with the casenote_id such that the vital signs can be loaded with the medical history of the patient
		$vitalSignsID = $vSigns["patvital_id"];

        //Get custom fields if any
        $fBuilder = new FormBuilder();
        $entries = $fBuilder->getFormEntries($vitalSignsID, 1);
        $custom_entries = '';
//        die('<pre>' . print_r($entries, true));
        if (!empty($entries)){
            $row_style1 = 'tr-row';
            $row_style2 = 'tr-row2';
            $change_style = true;
            foreach ($entries as $entry){
                $row_style = $change_style ? $row_style1 : $row_style2;
                $change_style = !$change_style;
                $custom_entries .= "<tr class=\"$row_style\">
                                        <td><strong>" . strtoupper($entry['customfield_name']) . "</strong>:</td>
                                        <td>" . nl2br($entry['customentry_value']) . "</td>
                                    </tr>";
            }
        }

		$finalVSigns = "<table cellspacing=\"3\" cellpadding=\"3\" >
							<tr class=\"tr-row\">
								<td><strong>$vitalsigns_temperature</strong>:</td>
								<td>{$vSigns['patvital_temp']}</td>
							</tr>

							<tr class=\"tr-row2\">
								<td><strong>$vitalsigns_pulse</strong>:</td>
								<td>{$vSigns['patvital_pulse']}</td>
							</tr>

							<tr class=\"tr-row\">
								<td><strong>$vitalsigns_respiration</strong>:</td>
								<td>{$vSigns['patvital_resp']}</td>
							</tr>

							<tr class=\"tr-row2\">
								<td><strong>$vitalsigns_bloodpressure</strong>:</td>
								<td>{$vSigns['patvital_bp']}</td>
							</tr>

							<tr class=\"tr-row\">
								<td><strong>$vitalsigns_weight</strong>:</td>
								<td>{$vSigns['patvital_weight']}</td>
							</tr>

							<tr class=\"tr-row2\">
								<td><strong>$vitalsigns_others</strong>:</td>
								<td>{$vSigns['patvital_others']}</td>
							</tr>
							$custom_entries
						</table>";


		//If the state of the vital signs measurement instance is still such that a nurse can edit it, set the vital signs to used so that no nurse can edit it again
		$vitalSigns->setToUsed($vitalSignsID);

	} else {
			$finalVSigns = "";
			echo "<span class=\"redText\" style=\"padding: 20px 0px 20px 0px;\"><strong>Not available.</strong></span>";
			include_once ("patcare_vitalsigns_form4doctors.php");
	}
	echo $finalVSigns;
?>
<!-- END Patient Details -->
            </td>

          </tr>
        </table>

      <span id="status">&nbsp;</span>
    </td>
  </tr>
</table>

<?php
	if($appdate != $presdate){
		if($appdate > $presdate)
			echo $future_appointment_label;
		if($appdate < $presdate)
			echo $missed_appointment_label;
?>
            <p>&nbsp;</p>
            <a href="" onclick='JAVASCRIPT: window.open ("patnewwindow_index.php?q=app01&a=<?php echo $patadmid ?>&b=<?php echo $hospital_no ?>&c=<?php echo $consult_id ?>&d=<?php echo $consult_clinic ?>&g=<?php echo $app_id ?>", "newwindow5", "scrollbars=no, width=500, status=no, toolbar =no, menubar = no, height=400, resizeable=no")'><?php echo $click_here_reschedule_appointment_label ?></a>
<?php
		} elseif ($paystatus != '1'){
				echo $pat_yet_to_pay;
			} else {
?>
				<form action="index.php" method="post" name="casenote" id="casenote_treatment_form" enctype="multipart/form-data">
	  				<input type="hidden" name="m" value="patient_care" />
                    <input type="hidden" name="p" value="pat_treatment" />
                    <input type="hidden" name="vital_signs" id="vital-signs-id-container" value="<?php echo $vitalSignsID; ?>">
            	    <input type="hidden" name="treat[patadm_id]" value="<?php echo $patadmid; ?>">
				    <input type="hidden" name="treat[user_id]" value="<?php echo $myUserid; ?>">
                    <input type="hidden" name="treat[app_id]" value="<?php echo $app_id; ?>">


                    <input type="hidden" id="hist[complaint]" name="hist[complaint]" value="">
                    <input type="hidden" id="hist[complainthist]" name="hist[complainthist]" value="">
                    <input type="hidden" id="hist[medhist]" name="hist[medhist]" value="">
                    <input type="hidden" id="hist[sochist]" name="hist[sochist]" value="">
                    <input type="hidden" id="hist[drughist]" name="hist[drughist]" value="">
                    <input type="hidden" id="hist[sysreview]" name="hist[sysreview]" value="">
                    <input type="hidden" id="hist[others]" name="hist[others]" value="">
                    <input type="hidden" id="treat[genexam]" name="treat[genexam]" value="">
                    <input type="hidden" id="treat[diagnosis]" name="treat[diagnosis]" value="">
                    <input type="hidden" id="treat[sysexam]" name="treat[sysexam]" value="">
                    <input type="hidden" id="treat[diffdiagnosis]" name="treat[diffdiagnosis]" value="">
                    <input type="hidden" id="lab[investigation]" name="lab[investigation]" value="">
                    <input type="hidden" id="treatment[treatment]" name="treatment[treatment]" value="">

<!--<script type="text/javascript">
	/*function testMe(){
		alert ("LAB TESTS: " + document.getElementById("lab[investigation]").value);
		//alert (opener.document.getElementById("labrequests").value);
	}*/
</script>
<a href="#" onclick="testMe();">Test</a>-->

<div style="padding: 10px;">
	You can either fill in text from treatment sheet or simply scan and upload the treatment sheets. Please, click the option that you prefer below.
</div>

<div id="accordion">
	<h3 style="padding: 10px 10px 10px 25px;">Upload scanned treatment sheet</h3>
    <div>
		<div id="docscontainer">
              <div class="comments" style="padding-bottom: 10px;">
              		Please, note that the maximum file size allowed is <?php echo FILES_MAX_SIZE_TEXT; ?> (per document) and the accepted file formats are <?php echo FILES_ALLOWED_TYPES; ?>.<br />
              </div>
              <span class="rightsLink" onclick="addMoreDocuments();">Add a new document</span>
              <div>&nbsp;</div>
              <div id="uploaddiv1" class="uploadDivs"></div>
        </div>
    </div>






    <!-- BEGIN Treatment Sheet Text Entry -->
	<h3 style="padding: 10px 10px 10px 25px;">Enter text from treatment sheet</h3>
    <div>
        <div id="tabs" >
        <ul>
              <li><a href="#tabs-1"><?php echo $history_taking_label ?></a></li>
              <li><a href="#tabs-2"><?php echo $examination_label ?></a></li>
              <li><a href="#tabs-4">Clinic Specifics</a></li>
              <li><a href="#tabs-3"><?php echo $plan_label ?></a></li>
              <!--<li style="float:right">
                <input type="button" name="sButton" id="sButton" value="Save" class="btn" />
              </li>-->
              <!--<li style="float:right"><input type="button" name="Submit" onclick="showFormDeets(this.form);" value="<?php //echo $save_label ?>" /></li>-->
          </ul>
              <div class="tabContent" id="tabs-1">


        <table cellspacing="5" cellpadding="5">
<?php
    $field_array = array ('1' => array(
                                    'code' => 'PRESENTING_COMPLAINT',
                                    'label' => $complaint_label,
                                    'textbox_id' => 'hist[complaint]'
                                 ),
                            '2' => array(
                                        'code' => 'H_PRESENTING_COMPLAINT',
                                        'label' => $complaint_history_label,
                                        'textbox_id' => 'hist[complainthist]'
                                     ),
                            '3' => array(
                                        'code' => 'PAST_MEDICAL_HISTORY',
                                        'label' => $medical_history_label,
                                        'textbox_id' => 'hist[medhist]'
                                     ),
                            '4' => array(
                                        'code' => 'FAMILY_AND_SOCIAL_HISTORY',
                                        'label' => $social_history_label,
                                        'textbox_id' => 'hist[sochist]'
                                     ),
                            '5' => array(
                                        'code' => 'DRUG_HISTORY',
                                        'label' => $drug_history_label,
                                        'textbox_id' => 'hist[drughist]'
                                     ),
                            '6' => array(
                                        'code' => 'REVIEW_OF_SYSTEMS',
                                        'label' => $system_review_label,
                                        'textbox_id' => 'hist[sysreview]'
                                     ),
                            '7' => array(
                                        'code' => 'OTHERS',
                                        'label' => $others_label,
                                        'textbox_id' => 'hist[others]'
                                     ),
                    );
    $field_array2 = array ('8' => array(
                                        'code' => 'GENERAL_PHYSICAL_EXAMINATION',
                                        'label' => $general_exam_label,
                                        'textbox_id' => 'treat[genexam]'
                                 ),
                            '9' => array(
                                        'code' => 'SYSTEMIC_EXAMINATION',
                                        'label' => $system_examination_label,
                                        'textbox_id' => 'treat[sysexam]'
                                     ),
                            '10' => array(
                                        'code' => 'DIFFERENTIAL_DIAGNOSIS',
                                        'label' => $differential_diagnosis_label,
                                        'textbox_id' => 'treat[diffdiagnosis]'
                                     ),
                            '11' => array(
                                        'code' => 'DIAGNOSIS',
                                        'label' => $diagnosis_label,
                                        'textbox_id' => 'treat[diagnosis]'
                                     )
                    );

    $field_array3 = array ('13' => array(
                                    'code' => 'OTHER_TREATMENT',
                                    'label' => $other_treatment_label,
                                    'textbox_id' => 'treatment[treatment]'
                                 )
                    );

    $field_array_list = array('field_array', 'field_array2', 'field_array3');
    $fields_list = array();
    $fields = '';
    foreach ($field_array as $index => $field){
        $field_checkboxes = getCheckBoxes($field['code'], 0);
        $field_new_entry = '<span onclick="addNewCheckBoxEntry(' . $index . ');" class="new-checkbox-entry-link">Add a New Entry</span>';
        $fields .= <<<FIELDS
                    <tr>
                      <td>
                          <span id="igniter{$index}" class="treatmentIgniter">{$field['label']}</span>
                          <div id="treat_dialog{$index}" class="treatmentDialog" title="{$field['label']}">
                              <div style="margin-bottom: 10px;">
                                  <input type="button" value=" Switch to Typing " id="typing-button-{$index}" class="btn" onclick="changeInputType({$index}, 1);">
                                  <input type="button" value=" Switch to Checkboxes " id="checkboxes-button-{$index}" class="btn" onclick="changeInputType({$index}, 2);">
                              </div>
                              <div id="checkboxes-container-{$index}">
                                $field_new_entry
                                $field_checkboxes
                              </div>

                              <div id="typingbox-container-{$index}" style="display: none;">
                                  <textarea id="treat_dialog_cont{$index}" cols="80" rows="20" title="{$field['textbox_id']}"></textarea>
                              </div>
                          </div>
                      </td>
                      <td class="treatmentDiaglogStatus">
                          <span id="dataStatus{$index}" >[Click link to enter data]</span>
                      </td>
                    </tr>
FIELDS;
    }   //END foreach fields
    echo $fields;
?>

          <tr><td>&nbsp;</td></tr>
          <tr>
            <td>&nbsp;</td>
            <td>
                <input type="button" id="next-to-2" value="Next &raquo;" class="btn" />
            </td>
          </tr>
        </table>

              </div>
              <div class="tabContent" id="tabs-2">


        <table cellspacing="5" cellpadding="5">
<?php
    $fields = '';
    foreach ($field_array2 as $index => $field){
        if ($index != 10 && $index != 11){
            $typing_button = '<input type="button" value=" Switch to Typing " id="typing-button-' . $index . '" class="btn" onclick="changeInputType(' . $index . ', 1);">';
            $cb_button_text = 'Switch to Checkboxes';
            $cb_button_onclick = 'changeInputType(' . $index . ', 2);';
            $field_checkboxes = getCheckBoxes($field['code'], 0);
            $field_new_entry = '<span onclick="addNewCheckBoxEntry(' . $index . ');" class="new-checkbox-entry-link">Add a New Entry</span>';
        } else {
            $typing_button = '';
            $cb_button_text = 'Search';
            $cb_button_onclick = 'showDiagnosisSearchDialog(' . $index . ');';
            $field_checkboxes = 'Please, click the \'Search\' button above to fetch Diagnosis.';
            $field_new_entry = '';
        }
        $fields .= <<<FIELDS
                    <tr>
                      <td>
                          <span id="igniter{$index}" class="treatmentIgniter">{$field['label']}</span>
                          <div id="treat_dialog{$index}" class="treatmentDialog" title="{$field['label']}">
                              <div style="margin-bottom: 10px;">
                                  $typing_button
                                  <input type="button" value=" $cb_button_text " id="checkboxes-button-{$index}" class="btn" onclick="$cb_button_onclick">
                              </div>
                              <div id="checkboxes-container-{$index}">
                                $field_new_entry
                                $field_checkboxes
                              </div>

                              <div id="typingbox-container-{$index}" style="display: none;">
                                  <textarea id="treat_dialog_cont{$index}" cols="80" rows="20" title="{$field['textbox_id']}"></textarea>
                              </div>
                          </div>
                      </td>
                      <td class="treatmentDiaglogStatus">
                          <span id="dataStatus{$index}" >[Click link to enter data]</span>
                      </td>
                    </tr>
FIELDS;
    }   //END foreach fields
    echo $fields;
?>

          <tr><td>&nbsp;</td></tr>
          <tr>
            <td>&nbsp;</td>
            <td>
                <input type="button" id="prev-to-1" value="&laquo; Previous" class="btn" />
                <input type="button" id="next-to-3" value="Next &raquo;" class="btn" />
            </td>
          </tr>

        </table>

              </div>
              <div  class="tabContent" id="tabs-3">
                <table border="0" cellspacing="1" cellpadding="5" width="100%">
                    <tr>
                      <td valign="top"><?php echo $drug_name_label; ?>:<br /></td>
                    <td valign="top"><input type="text" size="40" value="" id="inputString" onkeyup="lookup(this.value);" onblur="fill();" />
                         <div class="suggestionsBox" id="suggestions" style="display: none;">
                            <div class="suggestionList" id="autoSuggestionsList">&nbsp;</div>
                        </div>
                        <br /></td>
                    <td rowspan="2" valign="top" id="clear">&nbsp;</td>
                    <td rowspan="2" valign="top" id="clear"><textarea cols="50" rows="10" name="pres[desc]" id="sub1" readonly="readonly"></textarea>
                      <a href="#clear" onclick="ClearSub();return false"><?php echo $clear_label; ?></a></td>
                    </tr>

                    <tr>
                      <td valign="top"><?php echo $dosage_label ?>:</td>
                      <td valign="top"><input type="text" size="30" value="" id="inputString2" onkeyup="lookup(this.value);" onblur="fill();" />
                      &nbsp;&nbsp;
                      <input id="add1" onclick="AddSub(1)" type="button" value=" >> " /></td>
                      </tr>
                </table>



                    <table cellspacing="5" cellpadding="5">
                      <tr>
                        <td>
                            <span id="igniter12_old" class="treatmentIgniter"><a href="index.php?p=doc_tests&m=lab&hospitalno=<?php echo $hospital_no; ?>&pid=<?php echo $patadmid; ?>&cid=<?php echo $_SESSION[session_id() . "clinicID"]; ?>" target="_blank"><?php echo $investigation_label ?></a></span>
                            <div id="treat_dialog12" class="treatmentDialog" title="<?php echo $investigation_label ?>"><textarea id="treat_dialog_cont12" cols="80" rows="20" title="lab[investigation]"></textarea></div>
                        </td>
                        <td id="dataStatus12" class="treatmentDiaglogStatus"><!-- [Click link to enter data] --></td>
                      </tr>

<?php
    $fields = '';
    foreach ($field_array3 as $index => $field){
        $field_checkboxes = getCheckBoxes($field['code'], 0);
        $field_new_entry = '<span onclick="addNewCheckBoxEntry(' . $index . ');" class="new-checkbox-entry-link">Add a New Entry</span>';
        $fields .= <<<FIELDS
                    <tr>
                      <td>
                          <span id="igniter{$index}" class="treatmentIgniter">{$field['label']}</span>
                          <div id="treat_dialog{$index}" class="treatmentDialog" title="{$field['label']}">
                              <div style="margin-bottom: 10px;">
                                  <input type="button" value=" Switch to Typing " id="typing-button-{$index}" class="btn" onclick="changeInputType({$index}, 1);">
                                  <input type="button" value=" Switch to Checkboxes " id="checkboxes-button-{$index}" class="btn" onclick="changeInputType({$index}, 2);">
                              </div>
                              <div id="checkboxes-container-{$index}">
                                $field_new_entry
                                $field_checkboxes
                              </div>

                              <div id="typingbox-container-{$index}" style="display: none;">
                                  <textarea id="treat_dialog_cont{$index}" cols="80" rows="20" title="{$field['textbox_id']}"></textarea>
                              </div>
                          </div>
                      </td>
                      <td class="treatmentDiaglogStatus">
                          <span id="dataStatus{$index}" >[Click link to enter data]</span>
                      </td>
                    </tr>
FIELDS;
    }   //END foreach fields
    echo $fields;
?>

                      <tr><td>&nbsp;</td></tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="button" id="prev-to-2" value="&laquo; Previous" class="btn" />
                            <input type="button" name="sButton" id="sButton" value="Save Treatment" class="btn" />
                        </td>
                      </tr>
                    </table>

              </div>
            <div class="tabContent" id="tabs-4">
<?php
    //include_once('pat_medicalrecord_customfields.php');
    $fBuilder = new FormBuilder();
    $custom_form = $fBuilder->getForm($_SESSION[session_id() . "clinicID"], 2);
    echo $custom_form;
?>
            </div>
        </div>
    </div>
    <!-- END Treatment Sheet Text Entry -->


</div>



</form>
  <?php
}
?>
<div>&nbsp;</div>
<hr />
<div>&nbsp;</div>
<h2 style="text-decoration: underline;"><strong><?php echo $previous_appointment_label ?></strong></h2>

<?php
	$visits = $treat->getOtherAppointment($hospital_no, $consult_id);
	if($visits != ""){
?>
<table width="100%" border="1" cellpadding="5" cellspacing="0" style="border: 1px solid #AAA; border-collapse: collapse;">
  <tr>
    <td><strong><?php echo $date_label ?></strong></td>
    <td><strong><?php echo $diagnosis_label ?></strong></td>
    <td><strong><?php echo $prescription_label ?></strong></td>
    <td><strong><?php echo $investigation_label ?></strong></td>
     <td><strong><?php echo $treatment_label ?></strong></td>
   <td>&nbsp;</td>
  </tr>
  <?php
	echo $visits;
?>
</table>
<?php

}else{
 echo $no_previous_visits;
}
}
}else{
?>
<form action="index.php?<?php echo $_SERVER['QUERY_STRING']; ?>" method="post" name="search_patient" enctype="multipart/form-data">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td width="24%" align="left" valign="top"><?php echo $hospital_no_label; ?></td>
              <td width="76%" align="left"><input type="text" name="findpatient" id="findpatient" />
              &nbsp;&nbsp;
              <label>
                    <input type="submit" name="Submit2" value="<?php echo $submit_label ?>" />
              </label></td>
            </tr>
            <tr>
              <td colspan="2" align="left" valign="top"><?php echo $hospital_no_msg_label; ?></td>
            </tr>
            <tr>
              <td colspan="2" align="left" valign="top">&nbsp;</td>
            </tr>
    </table>
</form>
<?php
}
?>

<?php
}
?>


<div id="dialog" title="More Actions" style="display:none; text-align: center;">
	<p>Before, the details of the treatment is finally saved, please, click a button below to carry out the associated action if need be or click the "Save Treatment" button to save the treatment details of the patient.</p>

	<table cellpadding="3" cellspacing="3" width="200" align="center">
    	<tr>
        	<td align="right"><input type="button" id="new-app-button" class="btn" value="New Appointment"  /></td><!--onclick="window.open('<?php //echo $newAppLink; ?>', 'newAppointment', '');"-->
            <td align="left"><input type="button" id="admit-button" class="btn" value="Admit Patient"  /></td><!--onclick="window.open('<?php //echo $admitLink; ?>', 'newAppointment', '');"-->
        </tr>
        <tr>
                    <td align="left"><input type="button" id="refer-button" class="btn" value="Refer Patient" /></td>
                    <td align="left"><input type="button" id="transfer-button" class="btn" value="Transfer Patient" /></td><!--onclick="window.open('<?php //echo $transferLink; ?>', 'newAppointment', '');"-->
        </tr>
        <tr>
        	<td colspan="2">Please, click the &quot;Save Treatment&quot; button below when done with the tasks above.</td>
        </tr>


    </table>
</div>


<?php
	include_once ("pat_medicalrecord_dialogs.php");
	include_once ("pat_medicalrecord_jquery_init.php");
?>