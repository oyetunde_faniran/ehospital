<?php
    $searchArray = isset($_GET) ? $_GET : array();
    $hospitalnumber = isset($searchArray["hospitalnumber"]) ? trim($searchArray["hospitalnumber"]) : "";
    $surname = isset($searchArray["surname"]) ? trim($searchArray["surname"]) : "";
    $othernames = isset($searchArray["othernames"]) ? trim($searchArray["othernames"]) : "";
    $dobfrom = isset($searchArray["dobfrom"]) ? trim($searchArray["dobfrom"]) : "";
    $dobto = isset($searchArray["dobto"]) ? trim($searchArray["dobto"]) : "";
    $gender = isset($searchArray["gender"]) ? trim($searchArray["gender"]) : "";
    $phone = isset($searchArray["phone"]) ? trim($searchArray["phone"]) : "";
    $email = isset($searchArray["email"]) ? trim($searchArray["email"]) : "";
    $address = isset($searchArray["address"]) ? trim($searchArray["address"]) : "";
    $nationality = isset($searchArray["nationality"]) ? trim($searchArray["nationality"]) : "";
    $selectedState = isset($searchArray["state"]) ? trim($searchArray["state"]) : "";
    $civilstate = isset($searchArray["civilstate"]) ? trim($searchArray["civilstate"]) : "";
    $religion = isset($searchArray["religion"]) ? trim($searchArray["religion"]) : "";
    $occupation = isset($searchArray["occupation"]) ? trim($searchArray["occupation"]) : "";
    $placeofwork = isset($searchArray["placeofwork"]) ? trim($searchArray["placeofwork"]) : "";
    $nextofkin = isset($searchArray["nextofkin"]) ? trim($searchArray["nextofkin"]) : "";
    $nokrelationship = isset($searchArray["nokrelationship"]) ? trim($searchArray["nokrelationship"]) : "";
    $nokphone = isset($searchArray["nokphone"]) ? trim($searchArray["nokphone"]) : "";
    $nokemail = isset($searchArray["nokemail"]) ? trim($searchArray["nokemail"]) : "";
    $nokaddress = isset($searchArray["nokaddress"]) ? trim($searchArray["nokaddress"]) : "";
?>
        <div id="accordion">
            <h3><a href="#">Filter / Search</a></h3>
            <div>
                <form action="" method="get">
                    <div id="tabs">
                        <ul>
                            <li><a href="#tabs-1"><?php echo $personal_detail_label ?> I</a></li>
                            <li><a href="#tabs-2"><?php echo $personal_detail_label ?> II</a></li>
                            <li><a href="#tabs-3"><?php echo $next_of_kin_label ?></a></li>
                            <li style="float:right">
                                <input type="hidden" name="p" value="<?php echo $p; ?>" />
                                <input type="hidden" name="m" value="<?php echo $m; ?>" />
                                <input type="submit" name="search" value=" Search " class="btn" />
                            </li>
                        </ul>
                        <div id="tabs-1">
                            <table cellpadding="2" cellspacing="0">
                                <tr>
                                    <td width="15%" align="left" valign="top"><strong>Hospital No:</strong></td>
                                    <td width="35%" align="left" valign="top">
                                        <input type="text" name="hospitalnumber" value="<?php echo $hospitalnumber; ?>" />
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td width="35%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong><?php echo $surname_label ?>:</strong></td>
                                    <td align="left" valign="top">
                                        <input type="text" name="surname" value="<?php echo $surname; ?>" />
                                    </td>
                                    <td align="left" valign="top"><strong><?php echo $othernames_label ?>:</strong></td>
                                    <td align="left" valign="top"><input type="text" name="othernames" value="<?php echo $othernames; ?>" /></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong><?php echo $dob_label ?>:</strong></td>
                                    <td align="left" valign="top">
                                        <em>(From)</em><input type="text" name="dobfrom" id="dob-from" value="<?php echo $dobfrom; ?>" /><br />
                                        <em>(To)</em><input type="text" name="dobto" id="dob-to" value="<?php echo $dobto; ?>" />
                                    </td>
                                    <td align="left" valign="top"><strong><?php echo $gender_label ?>:</strong></td>
                                    <td align="left" valign="top">
                                        <select name="gender">
                                            <option value=""><?php echo $select_label ?> <?php echo $gender_label ?></option>
                                            <option value="0" <?php echo $gender === "0" ? "selected=\"selected\"" : ""; ?> > <?php echo $gender_0_label ?></option>
                                            <option value="1" <?php echo $gender === "1" ? "selected=\"selected\"" : ""; ?>><?php echo $gender_1_label ?></option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong><?php echo $phone_label ?>:</strong></td>
                                    <td align="left" valign="top"><input type="text" name="phone" value="<?php echo $phone; ?>" /></td>
                                    <td align="left" valign="top"><strong><?php echo $email_label ?>:</strong></td>
                                    <td align="left" valign="top"><input type="text" name="email" value="<?php echo $email; ?>" /></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong><?php echo $address_label ?>:</strong></td>
                                    <td colspan="3" align="left" valign="top"><textarea name="address" cols="20" rows="5"><?php echo $address; ?></textarea></td>
                                </tr>
                            </table>
                        </div>



                        <div id="tabs-2">
                             <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
                                <tr>
                                      <td width="20%" align="left" valign="top"><strong><?php echo $nationality_label ?>:</strong></td>
                                      <td width="80%" align="left" valign="top">
                                          <select name="nationality">
                                            <option value="" >-------</option>
<?php
    $allCountries = admin_Tools::getCountries();
    $countryDD = "";
    foreach ($allCountries as $country){
        $selected = $country["nationality_id"] == $nationality ? " selected=\"selected\" " : "";
        $countryDD .= "<option $selected value=\"" . $country["nationality_id"] . "\">" . stripslashes($country["nationality_name"]) . "</option>";
    }   //END foreach()
    echo $countryDD;
?>
                                          </select>
                                      </td>
                                </tr>

                                <tr>
                                  <td width="20%" align="left" valign="top"><strong><?php echo $state_label ?>:</strong></td>
                                  <td width="80%" align="left" valign="top">
                                      <select name="state" id="second">
                                          <option value="" >-------</option>
<?php
    $allStates = admin_Tools::getAllStates();
    $stateDD = "<optgroup label=\"Nigerian States\">";
    foreach ($allStates as $state){
        $selected = $state["state_id"] == $selectedState ? " selected=\"selected\" " : "";
        $stateDD .= "<option $selected value=\"" . $state["state_id"] . "\">" . stripslashes($state["state_name"]) . "</option>";
    }   //END foreach()
    $stateDD .= "</optgroup>
                 <optgroup label=\"Other Countries\">
                    <option value=\"38\">Outside Nigeria</option>
                 </optgroup>";
    echo $stateDD;
?>
                                      </select>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><strong><?php echo $civilstate_label ?>:</strong></td>
                                  <td align="left" valign="top">
                                      <select name="civilstate">
                                          <option value=" " selected="selected"><?php echo $select_label ?> <?php echo $civilstate_label ?></option>
                                          <option value="0" <?php echo $civilstate === "0" ? "selected=\"selected\"" : ""; ?> ><?php echo $single_label ?></option>
                                          <option value="1" <?php echo $civilstate === "1" ? "selected=\"selected\"" : ""; ?> ><?php echo $married_label ?></option>
                                          <option value="2" <?php echo $civilstate === "2" ? "selected=\"selected\"" : ""; ?> ><?php echo $divorced_label ?></option>
                                          <option value="3" <?php echo $civilstate === "3" ? "selected=\"selected\"" : ""; ?> ><?php echo $widowed_label ?></option>
                                      </select>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><strong><?php echo $religion_label ?>:</strong></td>
                                  <td align="left" valign="top">
                                        <select name="religion">
                                            <option value=""><?php echo $selectReligion_label; ?></option>
                                            <option value="<?php echo $xtian_label; ?>" <?php echo $religion === $xtian_label ? "selected=\"selected\"" : ""; ?> ><?php echo $xtian_label; ?></option>
                                            <option value="<?php echo $islam_label; ?>" <?php echo $religion === $islam_label ? "selected=\"selected\"" : ""; ?> ><?php echo $islam_label; ?></option>
                                            <option value="<?php echo $others_label; ?>" <?php echo $religion === $others_label ? "selected=\"selected\"" : ""; ?> ><?php echo $others_label; ?></option>
                                        </select>
                                  </td>
                                </tr>

                                <tr>
                                  <td align="left" valign="top"><strong><?php echo $occupation_label ?>:</strong></td>
                                  <td align="left" valign="top"><input type="text" name="occupation" value="<?php echo $occupation; ?>" /></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><strong><?php echo $placeofwork_label ?>:</strong></td>
                                  <td align="left" valign="top"><textarea name="placeofwork" cols="20" rows="5"><?php echo $placeofwork; ?></textarea></td>
                                </tr>
                            </table>
                        </div>


                         <div id="tabs-3">
                             <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
                                <tr>
                                  <td width="20%" align="left" valign="top"><strong><?php echo $name_label ?>:</strong></td>
                                  <td width="80%" align="left" valign="top"><input type="text" name="nextofkin" value="<?php echo $nextofkin; ?>" /></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><strong><?php echo $nokrelationship_label ?>:</strong></td>
                                  <td align="left" valign="top"><input type="text" name="nokrelationship" value="<?php echo $nokrelationship; ?>" /></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><strong><?php echo $phone_label ?>:</strong></td>
                                  <td align="left" valign="top"><input type="text" name="nokphone" value="<?php echo $nokphone; ?>" /></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><strong><?php echo $email_label ?>:</strong></td>
                                  <td align="left" valign="top"><input type="text" name="nokemail" value="<?php echo $nokemail; ?>" /></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><strong><?php echo $address_label ?>:</strong></td>
                                  <td align="left" valign="top"><textarea name="nokaddress" cols="20" rows="5"><?php echo $nokaddress; ?></textarea></td>
                                </tr>
                            </table>
                        </div>

<?php
    //Add needed existing $_GET variables
//    Array
//(
//    [p] => patients_list
//    [m] => patient_care
//    [sort] => 2
//    [d] => 1
//    [recpage] => 2
//    [pagerecs] => 10
//)
    $xGET = "";
    $xGET .= isset($_GET["p"]) ? "<input type=\"hidden\" name=\"p\" value=\"" . $_GET["p"] . "\" />" : "";
    $xGET .= isset($_GET["m"]) ? "<input type=\"hidden\" name=\"m\" value=\"" . $_GET["m"] . "\" />" : "";
    $xGET .= isset($_GET["sort"]) ? "<input type=\"hidden\" name=\"sort\" value=\"" . $_GET["sort"] . "\" />" : "";
    $xGET .= isset($_GET["d"]) ? "<input type=\"hidden\" name=\"d\" value=\"" . $_GET["d"] . "\" />" : "";
    $xGET .= isset($_GET["recpage"]) ? "<input type=\"hidden\" name=\"recpage\" value=\"" . $_GET["recpage"] . "\" />" : "";
    $xGET .= isset($_GET["pagerecs"]) ? "<input type=\"hidden\" name=\"pagerecs\" value=\"" . $_GET["pagerecs"] . "\" />" : "";
    echo $xGET;
?>

                    </div>
                </form>
            </div>
        </div>










        <link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
        <script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/ui.sortable.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/ui.tabs.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
        <script type="text/javascript">
            $(function() {
                $('#fromdate').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd'
                });
            });

            $(function() {
                $('#todate').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd'
                });
            });

            $(function() {
                $('#dob-from').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd'
                });
            });

            $(function() {
                $('#dob-to').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd'
                });
            });

            $(function() {
                $("#accordion").accordion({
                                              collapsible: true,
                                              active: false,
                                              clearStyle: true
                                          });
            });

            $(function() {
                $("#tabs").tabs().find(".ui-tabs-nav");
            });
        </script>