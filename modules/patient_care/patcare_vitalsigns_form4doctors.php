				<form action="patcare_vitalsigns_ajax_entry.php" target="_blank" method="post" name="vitalsignsentryform" id="vitalsignsentryform">
                    <table cellspacing="5" cellpadding="5">
                      <tr>
                        <td><strong><?php echo $vitalsigns_temperature; ?>:</strong></td>
                        <td><input type="text" name="temp" id="temp" maxlength="6" /> degrees
                         <select name="temp_unit" id="temp_unit">
                            <option value="C">C</option>
                            <option value="F">F</option>
                         </select>
                         </td>
                      </tr>

                      <tr>
                        <td><strong><?php echo $vitalsigns_pulse; ?>:</strong></td>
                        <td><input type="text" name="pulse" id="pulse" maxlength="20" /></td>
                      </tr>

                      <tr>
                        <td><strong><?php echo $vitalsigns_respiration; ?>:</strong></td>
                        <td><input type="text" name="resp" id="resp" maxlength="50" /></td>
                      </tr>

                      <tr>
                        <td><strong><?php echo $vitalsigns_bloodpressure; ?>:</strong></td>
                        <td><input type="text" name="bp" id="bp" maxlength="20" /></td>
                      </tr>

                      <tr>
                        <td><strong><?php echo $vitalsigns_weight; ?>:</strong></td>
                        <td><input type="text" name="weight" id="weight" maxlength="3" size="5" /> <strong>KG</strong></td>
                      </tr>

                      <tr>
                        <td><strong><?php echo $vitalsigns_others; ?>:</strong></td>
                        <td>
                            <textarea name="others" id="others" rows="5" cols="20"></textarea>
                        </td>
                      </tr>
<?php
    $fBuilder = new FormBuilder();
    $clinicID = $_SESSION[session_id() . "clinicID"];
    $custom_form = $fBuilder->getForm($clinicID, 1, true, false);
    echo ($custom_form != 'No custom form elements added yet.' ? $custom_form : '');
?>
                      <tr>
                        <td>&nbsp;</td>
                        <td>
                            <span id="vitals-button-container"><input type="submit" class="btn" id="vitals-btn" name="sButton" value="<?php echo $save_label; ?>" /></span>
                            <input type="hidden" name="pid" value="<?php echo $patadmid; ?>" />
                            <input type="hidden" name="ed" value="<?php echo "0";	//Editing is being carried out ?>" />
                            <input type="hidden" name="vsi" value="<?php echo "0";	//The id of the entered vital signs in the vital signs table ?>" />
                            <input type="hidden" name="aid" value="<?php echo $app_id; //Appointment ID?>" />
                            <input type="hidden" name="cid" value="<?php echo $_SESSION[session_id() . "clinicID"]; //Clinic ID?>" />
                        </td>
                      </tr>
                    </table>
                </form>
                <script>
					$('#vitals-btn').click(function() {
						$('#vitals-btn').fadeOut();
						vitalsButtonContainer = document.getElementById("vitals-button-container");
						vitalsButtonContainer.innerHTML = "<img src='images/_loader.gif' />";
						$.ajax({
						  type: 'POST',
						  url: "patcare_vitalsigns_ajax_entry.php",
						  data: $("#vitalsignsentryform").serialize(),
						  success: function(data){
										if (data != "0"){
											vitalsButtonContainer.innerHTML = "Vitals Signs Saved Successfully";
                                            document.getElementById('vital-signs-id-container').value = data;
                                        } else {
                                            vitalsButtonContainer.innerHTML = "Vitals Signs Saving Failed";
                                        }
                                   },
						  dataType: "html"
						});
					});
				</script>