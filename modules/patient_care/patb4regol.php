<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>
		<script type="text/javascript" src="library/doubleSelect.1.2/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="library/doubleSelect.1.2/jquery.doubleSelect.min.js"></script>
		<script type="text/JavaScript">
 $(document).ready(function()
 {
	    var selectoptions = {
    		<?php 
			
			$select->selectJqueryDepartAndClinic($curLangField);
			
              ?>
    };

<?php
 if(isset($myDeptid)&& $myDeptid!= "" && isset($myClinicid)&& $myClinicid !=""){?>
    var options = {
                    preselectFirst : <?php echo $myDeptid  ?>, 
                    preselectSecond : <?php echo $myClinicid ?>, 
                   };
    $('#first').doubleSelect('second', selectoptions, options);      
<?php  }else{ ?>                 
	    $('#first').doubleSelect('second', selectoptions);  
		<?
		}
		?>    
 });
   </script>
 <form action="index.php" method="post" name="search_patient" enctype="multipart/form-data">
      <table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" >
      	<tr>
          <td align="left" valign="top"><?php echo $search_label ?>:&nbsp;&nbsp;
            <input name="patsearch" type="text" id="patsearch" size="25" />&nbsp;
           <input name="search1" id="search1" type="submit" value="<?php echo $submit_label ?>" />
            <a href="index.php?p=advsearch&m=patient_care" ><?php echo $advancedsearch_label ?></a><br />
            <input type="hidden" name="m" value="patient_care" />
            <input type="hidden" name="p" value="register_patient" />
 		  </td>
        </tr>
 		<tr>
          <td style="color:#FF0000">
          (<?php echo $search_text ?>)
          </td>
        </tr>
 		<tr>
          <td>&nbsp;
          
          </td>
        </tr>
 		<tr>
          <td>&nbsp;
          
          </td>
        </tr>
	  </table>
 </form>
 <p> </p>
<form action="index.php" method="post" name="register_patient" enctype="multipart/form-data">
	<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" > 
    	<tr>
        	<td width="31%"  align="left" valign="top"><?php echo $select_label ?> <?php echo $department_label ?></td>
          	<td width="69%"  align="left" valign="top">
            	<select id="first" name="reg_dept">
  					<option value="" >-------</option>
				</select>
    			<input type="hidden" name="m" value="patient_care" />
     			<input type="hidden" name="p" value="reg_form" />            </td>
		</tr>
        
        <tr>
        	<td  align="left" valign="top"><?php echo $select_label ?> <?php echo $clinic_label ?></td>
            <td  align="left" valign="top">
            	<select id="second" name="reg_clinic">
                	<option value="">-------</option>
             	</select>        	</td>
    	</tr>
        
        <tr>
          <td  align="left" valign="top">Patient Type</td>
          <td  align="left" valign="top"><select name="patype" id="patype">
            <option value="0">New</option>
            <option value="1">Old</option>
          </select>          </td>
        </tr>
        
        <tr>
          <td  align="left" valign="top">&nbsp;</td>
          <td  align="left" valign="top"><input type="submit" name="button" id="button" value="<?php echo $submit_label ?>" /></td>
        </tr>
	</table>
</form>

<?php 

if(isset($_POST['patsearch'])){
$db2 = new DBConf();

?>
<div style="background-color:#CCCCCC"><hr color="#333333" width="100%"><?php echo $search_result_label ?><hr color="#333333" width="100%"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><strong>Hospital No.</strong></td>
    <td><strong>Old Hospital No.</strong></td>
    <td><strong>Name</strong></td>
    <td><strong>Gender</strong></td>
    <td>&nbsp;</td>
  </tr>

<?php
$search = strtoupper($_POST['patsearch']);
$patsearch = explode (" ", $search);

/* $sql = "SELECT reg_hospital_no, reg_hospital_oldno, reg_surname, reg_othernames, reg_gender FROM registry 
				WHERE reg_hospital_no LIKE '%".$search."%' 
				   OR reg_surname LIKE '%".$search."%' 
				   OR reg_othernames LIKE '%".$search."%' 
				   OR reg_hospital_oldno LIKE '%".$search."%'";
		$res = $db2->execute($sql);
		$num=mysql_num_rows($res);
		
		
		if($num>0){
		for ($i=0; $i<$num; $i++){
			$row = mysql_fetch_array($res);
			$hospital_no = $row['reg_hospital_no'];
			$oldhospital_no = $row['reg_hospital_oldno'];
			$reg_surname = $row['reg_surname'];
			$reg_othernames = $row['reg_othernames'];
			$reg_gender = $row['reg_gender'];
		}
		
		}
 */

foreach($patsearch as $key => $value){
		$sql="SELECT DISTINCT reg_id, reg_hospital_no, reg_hospital_oldno, reg_surname, reg_othernames, reg_gender FROM registry 
				WHERE reg_hospital_no LIKE '%".$value."%' 
				   OR reg_surname LIKE '%".$value."%' 
				   OR reg_othernames LIKE '%".$value."%' 
				   OR reg_hospital_oldno LIKE '%".$value."%'";
				
		$res = $db2->execute($sql);
		$num = mysql_num_rows($res);
		
		
		if($num>0){
		for ($i=0; $i<$num; $i++){
		$row = mysql_fetch_array($res);
		$hospital_no[$key] = $row['reg_hospital_no'];
		$oldhospital_no = $row['reg_hospital_oldno'];
		$reg_surname = $row['reg_surname'];
		$reg_othernames = $row['reg_othernames'];
		$reg_gender = $row['reg_gender'];
		$reg_id = $row['reg_id'];
		
		
		if($reg_gender =='0') $reg_gender = $gender_0_label;
		if($reg_gender =='1') $reg_gender = $gender_1_label;
		
		$reg_name = $reg_surname.' '.$reg_othernames;
		$reg_name = strtoupper($reg_name);
		$reg_name2 = "<font color='#D80000'>".$reg_name."</font>";
		
		$value2 = "<font color='#D80000'>".$value."</font>";
		if($key==0){
		?>
          <tr>
            <td><?php print str_ireplace ("$value", "$value2", $hospital_no[$key]); ?></td>
            <td><?php print str_ireplace ("$value", "$value2", $oldhospital_no); ?></td>
            <td><?php if($reg_name == $search){
					print str_ireplace ("$reg_name", "$reg_name2", $reg_name) ;
				}else{
				print str_ireplace ("$value", "$value2", $reg_name) ; 
				}
				?>
        </td>
            <td><?php echo $reg_gender; ?></td>
    		<td>&nbsp;</td>
          </tr>
		
		<?php
		}else{
				for($j=0; $j<$i; $j++){
				if($hospital_no[$key] == $hospital_no[$j]){
				}else{
		?>
          <tr>
            <td><?php print str_ireplace ("$value", "$value2", $hospital_no[$key]); ?></td>
            <td><?php print str_ireplace ("$value", "$value2", $oldhospital_no[$i]); ?></td>
            <td><?php print str_ireplace ("$value", "$value2", $reg_name) ; ?></td>
            <td><?php echo $reg_gender; ?></td>
     		<td>&nbsp;</td>
         </tr>
		
		<?php
				}
				}
		}
		}
			}else{
			echo "<tr>
    				<td colspan='4'><strong>No Result Found</strong></td>
				  </tr>";
			
			}
	  }
?>
</table>
<?php
}
?>