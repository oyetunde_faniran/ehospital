 <div  class="tabContent" id="tabs-2">        
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" >
<tr>
          <td width="20%" align="left" valign="top"><strong><?php echo $nationality_label ?>:</strong></td>
          <td width="80%" align="left" valign="top"><select name="reg[nationality]" id="first">
          <option value="" >-------</option>
          </select>          </td>
        </tr>
        
        <tr>
          <td align="left" valign="top"><strong><?php echo $state_label ?>:</strong></td>
          <td align="left" valign="top"><select name="reg[state]" id="second">
              <option value="" >-------</option>
          </select></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $civilstate_label ?>:</strong></td>
          <td align="left" valign="top"><select name="reg[civilstate]">
              <option value=" "><?php echo $select_label ?> <?php echo $civilstate_label ?></option>
              <option value="0" <?php if($reg->getReg_civilstate() =='0') echo "selected"; ?>><?php echo $single_label ?></option>
              <option value="1" <?php if($reg->getReg_civilstate() =='1') echo "selected"; ?>><?php echo $married_label ?></option>
              <option value="2" <?php if($reg->getReg_civilstate() =='2') echo "selected"; ?>><?php echo $divorced_label ?></option>
              <option value="3" <?php if($reg->getReg_civilstate() =='3') echo "selected"; ?>><?php echo $widowed_label ?></option>
          </select></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $religion_label ?>:</strong></td>
          <td align="left" valign="top"><input type="text" name="reg[religion]" value="<?php echo $reg->getReg_religion() ?>" /></td>
        </tr>
        
        <tr>
          <td align="left" valign="top"><strong><?php echo $occupation_label ?>:</strong></td>
          <td align="left" valign="top"><input type="text" name="reg[occupation]" value="<?php echo $reg->getReg_occupation() ?>" /></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $placeofwork_label ?>:</strong></td>
          <td align="left" valign="top"><textarea name="reg[placeofwork]" cols="20" rows="5"><?php echo $reg->getReg_placeofwork() ?></textarea></td>
        </tr>
</table>
</div>


<div id="tabs-3">   
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2">
        <tr>
          <td width="20%" align="left" valign="top"><strong><?php echo $name_label ?>:</strong></td>
          <td width="80%" align="left" valign="top"><input type="text" name="reg[nextofkin]" value="<?php echo $reg->getReg_nextofkin() ?>" /></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $nokrelationship_label ?>:</strong></td>
          <td align="left" valign="top"><input type="text" name="reg[nokrelationship]" value="<?php echo $reg->getReg_nokrelationship() ?>" /></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $phone_label ?>:</strong></td>
          <td align="left" valign="top"><input type="text" name="reg[nokphone]" value="<?php echo $reg->getReg_nokphone() ?>" /></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $email_label ?>:</strong></td>
          <td align="left" valign="top"><input type="text" name="reg[nokemail]" value="<?php echo $reg->getReg_nokemail() ?>" /></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $address_label ?>:</strong></td>
          <td align="left" valign="top"><textarea name="reg[nokaddress]" cols="20" rows="5"><?php echo $reg->getReg_nokaddress() ?></textarea></td>
        </tr>
</table>
</div>

<div id="tabs-4">


<!-- BEGIN ACCORDION -->
<div id="accordion">

<!-- BEGIN RETAINERSHIP -->
	<h3 style="padding: 5px 0px 5px 40px;"><?php echo $retainership_label ?></h3>
    <div>
        <table width="100%" border="0" cellpadding="2" cellspacing="0" id="retainerTable">
              <tr>
                 <td width="40%" align="left" valign="top"><strong><?php echo $company_label ?></strong></td>
                 <td width="60%" align="left" valign="top">
                         <select name="reg[retainership_id]" id="retcompanies" onchange="processPlans4Retainers('retcompanies', 'retplans');">
                            <option value="0" selected="selected"><?php echo $select_company_label ?></option>
<?php
	//Retainer / Company
	$curCompany = array_key_exists ("retainership_id", $reg->retDeets) ? $reg->retDeets["retainership_id"] : "0";
	$select->selectRetainer(1, $curCompany);
?>
                		</select>
                  </td>
              </tr>

					<tr>
                      <td width="40%" align="left" valign="top"><strong><?php echo $plan_label ?>:</strong></td>
                      <td width="60%" align="left" valign="top">
                          <select name="reg[ret_plan]" id="retplans">
                          		<option selected="selected" value=""><?php echo $select_plan_label ?></option>
<?php
	//Retainer Plan
	$curRPlan = array_key_exists ("retplan_id", $reg->retDeets) ? $reg->retDeets["retplan_id"] : "0";
	echo $select->selectRetainerPlans("retainer", $curCompany, $curRPlan);
?>
                           </select>
                           <span id="retplan_loader" style="display: none; padding-left: 10px; color: #999;"><?php echo $loading_label; ?></span>
                       </td>
                    </tr>

                    <tr>
                      <td align="left" valign="top"><strong><?php echo $dependant_label ?></strong></td>
                      <td align="left" valign="top">
<?php
	$curDependant = array_key_exists ("regret_isdependant", $reg->retDeets) ? $reg->retDeets["regret_isdependant"] : -1;
?>
                          <select name="reg[isdependant]">
                                <option value="" <?php if ($curDependant == -1) echo "selected=\"selected\"" ?>><?php echo $select_option_label ?></option>
                                <option value="0" <?php if ($curDependant == "0") echo "selected=\"selected\"" ?>><?php echo $no_label ?></option>
                                <option value="1" <?php if ($curDependant == "1") echo "selected=\"selected\"" ?>><?php echo $yes_label ?></option>
                          </select>
                      </td>
                    </tr>

                    <tr>
                      <td align="left" valign="top"><strong><?php echo $employee_name_label ?></strong></td>
                      <td align="left" valign="top">          
                          <input type="text" name="reg[employee]" id="reg[employee]" 
<?php
	$curDependant = array_key_exists ("regret_employee_name", $reg->retDeets) ? $reg->retDeets["regret_employee_name"] : "";
	echo "value=\"$curDependant\""
?>
                          />
                      </td>
                    </tr>

                    <tr>
                      <td align="left" valign="top"><strong><?php echo $identity_card_no_label ?></strong></td>
                      <td align="left" valign="top">
                      		<input type="text" name="reg[idcardno]" 
<?php
	$curID = array_key_exists ("regret_idcardno", $reg->retDeets) ? $reg->retDeets["regret_idcardno"] : "";
	echo "value=\"$curID\""
?>
                            />
					  </td>
                    </tr>
        </table>
	</div>
<!-- END RETAINERSHIP -->

<!-- BEGIN NHIS -->
	<h3 style="padding: 5px 0px 5px 40px;"><?php echo $nhis_label ?></h3>
    <div>
        <table width="100%" border="0"  cellpadding="2" cellspacing="0" id="nhisTable">
             <tr>
                <td width="40%" align="left" valign="top"><strong><?php echo $hmo_label ?></strong></td>
                <td width="60%" align="left" valign="top">
                	<select name="reg[nhis_id]">
						<option value=" " selected="selected"> <?php echo $select_hmo_label ?> </option>
<?php
	$curHMO = array_key_exists ("retainership_id", $reg->nhisDeets) ? $reg->nhisDeets["retainership_id"] : "";
	$select->selectRetainer(0, $curHMO);
?>
                	</select>
                </td>
            </tr>

           <tr>
              <td width="40%" align="left" valign="top"><strong><?php echo $plan_label ?>:</strong></td>
              <td width="60%" align="left" valign="top">
                  <select name="reg[nhis_plan]">
                        <option selected="selected" value=""><?php echo $select_plan_label ?></option>
<?php
	$curNHISPlan = array_key_exists ("nhisplan_id", $reg->nhisDeets) ? $reg->nhisDeets["nhisplan_id"] : "";
	echo $select->selectRetainerPlans("nhis", 0, $curNHISPlan);
?>
                  </select>
               </td>
            </tr>
              <tr>
                <td align="left" valign="top"><strong><?php echo $card_no_label ?></strong></td>
                <td align="left" valign="top">
                      		<input type="text" name="reg[cardno]" 
<?php
	$curID = array_key_exists ("regnhis_cardno", $reg->nhisDeets) ? $reg->nhisDeets["regnhis_cardno"] : "";
	echo "value=\"$curID\""
?>
                            />
                </td>
              </tr>
        </table>
    </div>
<!-- END NHIS -->

</div>
<!-- END ACCORDION -->

<!-- <div id="bookingDiv" class="reglinks">
	<p onclick="collapseDiv('bookingTable','bookingDiv','<?php //echo $bookingcategory_label ?>')">
       	<img src="images/collapse.jpg" /> <?php //echo $bookingcategory_label ?></p></div>
 <table width="100%" border="0" cellpadding="2" cellspacing="0" id="bookingTable">
		<tr>
            <td width="40%" align="left" valign="top"><?php //echo $bookingcategory_label ?></td>
            <td width="60%" align="left" valign="top">
            <input type="text" name="reg[matbookingcat]" value="<?php //echo $reg->getReg_matbookingcat() ?>"/></td>
        </tr> 
  </table>-->

 	
<!--<script type="text/javascript">
function expandDiv(id1,id2,cont){
	document.getElementById(id1).style.display = 'block';
  	p = document.getElementById(id2);
	p.innerHTML = "<p onclick=\"collapseDiv('"+id1+"','"+id2+"','"+cont+"')\"><img src='images/collapse.jpg' /> "+cont+"</p>";
}
function collapseDiv(id1,id2,cont){
	document.getElementById(id1).style.display = 'none';
  	p = document.getElementById(id2);
	p.innerHTML = "<p onclick=\"expandDiv('"+id1+"','"+id2+"','"+cont+"')\"><img src='images/expand.jpg' /> "+cont+"</p>";
}
</script>-->

</div>

<script type="text/javascript" language="javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/patreg.js"></script>