<script   type="text/javascript" src="myajax4.js"></script>
<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>

<?php  
//constructors
$patinp= new patinp_account();
$hospital= new patient_admission();
$inp = new inpatient_admission();
$wards= new wards();
$invoice= new invoice();

if (isset($_GET['patadm_id'])) {
		$med_id = $_GET['patadm_id'];
	} elseif (isset($_POST['patadm_id'])) { // Forms
		$med_id = $_POST['patadm_id'];
	} else {
		$med_id = NULL;
	}
if(!empty($med_id))
$hosp_id=$hospital->gethospital_name($med_id);
 
 //save new record
// if (isset($_POST['addpatinp']) || !empty($_POST['addpatinp'])){
// 	 	$i=0;
//		foreach ($_POST as $key=>$value) {	 
//				if($key=='p' or  $key=='addpatinp'  or $key=='patinpacc_id' or $key=='langid'){ //don't include these fields 
//				
//					}else{ 
//							 $patinp->patinp[$i]=$key;
//		 					$patinp->patinp2[$i]=$value;
//		
//							$i++;
//		  			}
//		 
//		 }
//	
//$message=$patinp->Save_Active_Row_as_New('patinp_account',$lang_curDB,$lang_curTB);
//if ($message){
//	//	header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=Success");
//		$c = "Patient Deposit Successfully Saved";
//		//exit();
//		}
//			
//		else $c = "Patient Not Successfully Saved";
//
// }
//edit existing record
if(isset($_POST['edit'])|| !empty($_POST['edit'])){
$i=0;
 foreach ($_POST as $key=>$value) {	if($key=='p' or $key=='editpatinp' or $key=='addpatinp' or  $key=='edit' or $key=='reset'  or $key=='patinpacc_id' or $key=='patadm_id' ){
				
		}else{ 
		  $patinp->patinp[$i]=$key;
		 $patinp->patinp2[$i]=$value; 
			$i++;
		  }
	   }
		   
    $message=$patinp->Save_Active_Row($_POST['patinpacc_id'],'patinp_account',$lang_curDB,$lang_curTB);
if ($message){
	//	header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=Success");
		$c = "Patient Deposit Successfully Updated";
		//exit();
		}
			
		else $c = "Patient Not Successfully Updated";
}
//if(isset($med_id))
	$hasRow=$patinp->Load_from_key($med_id);
	
//}
?>


<script language="javascript" type="text/javascript">
function display(id1){  //show only 1st
		if (document.form1.openclose.value == 'open') {
		    document.getElementById(id1).style.display = 'none';
			document.form1.openclose.value = 'close';
			document.getElementById('imageopenclose').src = './images/expand.jpg';
			document.getElementById('textopenclose').innerHTML = 'Pending Admission';
			document.getElementById('mesgopenclose').innerHTML = '';
			document.getElementById('saveopenclose').innerHTML = '';
		}else{
			document.getElementById(id1).style.display = 'block';
			document.form1.openclose.value = 'open';
			document.getElementById('imageopenclose').src = './images/collapse.jpg';
            document.getElementById('textopenclose').innerHTML = 'Close Pending List';
		}		
}

</script>
<?php require ("library/library_datepicker/library_ahmed.txt");?>
<script type="text/javascript">
	$(function() {
		$('#patinpacc_date').datepicker({
			changeDay: true,
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});
	});
</script>
<script type="text/JavaScript">

function validate(form){
		  if (document.patdeposit.patadm_purpose.value == "") {
			alert( "Please Select Admission Purpose " );
			document.patdeposit.patadm_purpose.focus();
			return false ;
		  }
		   if (document.patdeposit.patinpacc_date.value == "") {
			alert( "Please enter initiation Date" );
			document.patdeposit.patinpacc_date.focus();
			return false ;
		  }
    }


<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>

<!--<input type="hidden"  id="addWard" name="capMode" value="addWard" />-->
	<form name="form1" id="form1">
	 <input type="hidden"  id="openclose" name="openclose" value="close" />


			<div  ><img 
						id="imageopenclose"
						title="Collapse" 
						alt="Collapse" 
						src="./images/collapse.jpg" 
						style="border: none"
						onClick="display('addform');" 
						 
						 />&nbsp;&nbsp; <label   id="textopenclose" onClick="display('addform');">Close Pending List</label>	 
																</div>
</form>	


	<table  id="addform" style="display:block" class="tblborder" border="0"> 
	<?php 
$mesg=$hospital->getpendingAdmissionlist_id($lang_curDB,$lang_curTB);

if($mesg[1]==true){ 
?>
<?php 
									if($invoice->haspaid($med_id)){
	?>
	<tr><td colspan=3 class="bold"><strong>PAYMENT CONFIRMATION:</strong> Successful Payment<BR /> <a href="./index.php?p=inp&patadm_id=<?php echo $med_id ?>" style=" font-size:14px; color: #0000CC; font-weight:bold;">Click here to go to ADMISSION PAGE...</a></td></tr>
																		
									<?php }?>
	<tr><td width="334"><form action="./index.php" method="post" name="patdeposit"  id="patdeposit" onsubmit="MM_validateForm('patinpacc_date','','R','patadm_purpose','','R');return document.MM_returnValue">
	  <table class="tblborder2" > 
  <input type="hidden"  id="patadm_id" name="patadm_id" value="<?php echo $med_id ?>" />
   <input type="hidden"  id="p" name="p" value="patinp" />
 <input type="hidden"  id="patinpacc_type" name="patinpacc_type" value="1" />
 
 <?php if($hasRow==1){?>
 <input type="hidden"  id="patinpacc_id" name="patinpacc_id" value="<?php if(!empty($med_id))echo $patinp->patinpacc_id ?>" />
 <input type="hidden"  id="edit" name="edit" value="editpatinp" />
 <?php }?>
 <input type="hidden"  id="patadm_id" name="patadm_id" value="<?php if(!empty($med_id))echo $med_id ?>" />
 <tr>
<td align=right width="160"  colspan="3" bgcolor="#f1f1f1"><?php 
if(isset($_GET['c'])){ 
echo "<p id='saveopenclose' class='comments'>".$_GET['c']."</p>"; 
}else{
    if(isset($_GET['c']))
 echo "<p id='saveopenclose' class='comments'>$c</p>" ;
 
 } ?></td>

</tr>
<?php if(!empty($med_id)){ ?>
<tr>
<td width="160" align=right class="bold"><?php echo $lang_inpatname ?>:</td>
<td width="0"></td>
<td align=left width="144" class="bold"><?php if(!empty($med_id)) echo $hospital->getPatient_name($med_id); ?></td></tr>
<tr>
<tr>
<td align=right width="160" ><?php echo $lang_inpatdept ?>:</td>
<td width="0"></td>
<td width="144" align=left class="bold"><?php echo  $a1; ?></td>
</tr>

<tr>
<td align=right width="160" ><?php echo $lang_inpatclinic ?>:</td>
<td width="0"></td>
<td width="144" align=left class="bold"><?php if(!empty($med_id))echo $hospital->getClinic_name2($hosp_id,$lang_curDB,$lang_curTB); ?></td>
</tr>
<tr>
<td align=right width="160" ><?php echo $lang_inpatconsultant ?>:</td>
<td width="0"></td>
<td width="144" align=left class="bold" ><?php if(!empty($med_id)) echo $hospital->getConsultant_name2($hosp_id,$lang_curDB,$lang_curTB); ?></td>
</tr>
<?php /*?><tr>
<td align=right width="160" class=""><?php echo $lang_inpatadm_id ?>:</td>
<td width="0"></td>
<td align=left width="144" class="bold"><?php if(!empty($med_id)) echo $hospital->gethospital_name($med_id)?></td></tr>
<?php */?>

<tr>
<td width="160" align=right class=""><?php echo $lang_inpataddr ?>:</td>
<td width="0"></td>
<td align=left width="144" class="bold"><?php if(!empty($med_id)) echo $hospital->getPatient_address($med_id); ?></td></tr>
<tr>
<td width="160" align=right class=""><?php if(!empty($med_id))echo $lang_inpatsex ?>:</td>
<td width="0"></td>
<td align=left width="144" class="bold"><?php if(!empty($med_id)){  if($hospital->getPatient_gender($med_id)==0) echo $lang_inpatsexmale ; else echo $lang_inpatsexfemale ; }?></td></tr>
<?php /*?><tr>
<td width="160" align=right class=""><?php if(!empty($med_id))echo $lang_inpatage ?>:</td>
<td width="0"></td>
<td align=left width="144" class="bold"><?php if(!empty($med_id)) echo  $hospital->getPatient_age($med_id) ?></td></tr><?php */?>
<tr>

<tr>
		<td align=right width="160" class=""><?php echo $lang_adm_purpose ?> :</td>
		<td width="0"></td>
		<td align=left width="144" class="bold"><select   id = "patadm_purpose" name="patadm_purpose" size="" >
		<option value="">--Choose Admission Purpose</option>
		<option value="1" <?php if (isset($patinp->patadm_purpose) && $patinp->patadm_purpose==1) echo 'selected' ; ?>><?php echo $lang_adm_regular ?></option>
		<option value="2" <?php if (isset($patinp->patadm_purpose) && $patinp->patadm_purpose==2) echo 'selected' ; ?>><?php echo $lang_adm_shortstay ?></option>
		<option value="3" <?php if (isset($patinp->patadm_purpose) && $patinp->patadm_purpose==3) echo 'selected' ; ?>><?php echo $lang_adm_sideward ?></option>
		<?php /*?><option value="4" <?php if (isset($patinp->patadm_purpose) && $patinp->patadm_purpose==4) echo 'selected' ; ?>><?php echo $lang_adm_icuward ?></option>
		<option value="5" <?php if (isset($patinp->patadm_purpose) && $patinp->patadm_purpose==5) echo 'selected' ; ?>><?php echo $lang_adm_icutheatre ?></option>
		<option value="6" <?php if (isset($patinp->patadm_purpose) && $patinp->patadm_purpose==6) echo 'selected' ; ?>><?php echo $lang_adm_burns ?></option>
		<option value="7" <?php if ( isset($patinp->patadm_purpose) && $patinp->patadm_purpose==7) echo 'selected' ; ?>><?php echo $lang_adm_tetanus ?></option><?php */?>
		</select></td>
		<?php /*?><td align=left width="144" class="bold"><select   id = "patadm_purpose" name="patadm_purpose" size="" onChange="showBill(<?php if(!empty($med_id)) echo  $med_id  ?>,this.value)">
		<option value="1" <?php if (isset($patinp->patadm_purpose) && $patinp->patadm_purpose==1) echo 'selected' ; ?>><?php echo $lang_adm_regular ?></option>
		<option value="2" <?php if (isset($patinp->patadm_purpose) && $patinp->patadm_purpose==2) echo 'selected' ; ?>><?php echo $lang_adm_shortstay ?></option>
		<option value="3" <?php if (isset($patinp->patadm_purpose) && $patinp->patadm_purpose==3) echo 'selected' ; ?>><?php echo $lang_adm_sideward ?></option>
		<option value="4" <?php if (isset($patinp->patadm_purpose) && $patinp->patadm_purpose==4) echo 'selected' ; ?>><?php echo $lang_adm_icuward ?></option>
		<option value="5" <?php if (isset($patinp->patadm_purpose) && $patinp->patadm_purpose==5) echo 'selected' ; ?>><?php echo $lang_adm_icutheatre ?></option>
		<option value="6" <?php if (isset($patinp->patadm_purpose) && $patinp->patadm_purpose==6) echo 'selected' ; ?>><?php echo $lang_adm_burns ?></option>
		<option value="7" <?php if ( isset($patinp->patadm_purpose) && $patinp->patadm_purpose==7) echo 'selected' ; ?>><?php echo $lang_adm_tetanus ?></option>
		</select></td><?php */?>
</tr>
<tr>
    <td colspan="3" align="center"><div id='admbill'>
			 
        	</div></td>
  </tr>
<tr>
<td align=right width="160" class=""><?php echo $lang_patinp_datedeposit ?></td>
<td width="0"></td>
<td align=left width="144" class="bold"><input type="text"  id="patinpacc_date" name="patinpacc_date" value="<?php if(!empty($patinp->patinpacc_date)) echo  $patinp->patinpacc_date ?>"></input></td> 
</tr>

<?php /*?><tr>
<td align=right width="160" class=""><?php echo $lang_account_type ?>:</td>
<td width="0"></td>
<td align=left width="144" class="bold"><select  id="patinpacc_type" name="patinpacc_type" >
<option value="1" <?php if (isset($patinp->patinpacc_type) && $patinp->patinpacc_type==1) echo 'selected' ; ?>><?php echo $lang_deposit ?></option>
<option value="0" <?php if (isset($patinp->patinpacc_type) && $patinp->patinpacc_type==0) echo 'selected' ; ?>><?php echo $lang_withdrwal ?></option>
</select></td>
</tr>
<?php */?>

<tr>
<td width="160" align=right></td>
<td width="0"></td>
<td width="144"></td>
</tr>
<tr>
<td width="160" align=right>
 <?php if($hasRow==1 ){
 			if(!$invoice->haspaid($med_id)){
 ?>
<input name="editpatinp"  type="submit"  id="editpatinp"  class="btn" value="Update Deposit Settings"  />
<?php 
			}
}else{?>
<input name="addpatinp"  type="submit"  id="addpatinp" class="btn" value="Save Deposit Settings"   onClick="return  validate(this)"/>
<?php }?> </td><td width="0"></td>
<td width="144"></td>
</tr>
<tr><td colspan=3></td></tr>
<?php }else{?>
<tr><td colspan=3>Please Select Patient From the List</td></tr>
<tr><td colspan=3 class="bold"></td></tr>
<?php }?>
</table></form>
		</td>
	<td width="150"  valign="top">
	<table  height="46"  border="1" >
  <tr>
    <td width="163"><strong>Pending Admissions </strong></td>
  </tr>
   
   <?php
    $mesg1=$hospital->getpendingAdmissionlist_id($lang_curDB,$lang_curTB);
    echo $mesg1[0] ; ?>
  
</table>
	</td>
	</tr>
	<?php }else{?> 
<tr><td colspan=2 style="background-color:#CCCCCC;">No Pending Admission</td></tr>
<?php }?>
</table>		
</center>


<form name="standardView" method="post" action="./index.php?p=inp">

			<BR />
	<?php /*?><h2><?php echo $lang_title_key ?> :  <?php echo $lang_title_value_inpatient ?></h2><?php */?>
		
			<?php /*?><img 
						title="Add" 
						alt="Add" 
						src="./images/btn_add.gif" 
						style="border: none"
						onClick="display('addform','addform1','atminter');" 
						onMouseOut="this.src='./images/btn_add.gif" ;" 
						onMouseOver="this.src='./images/btn_add_02.gif';" /><img 
							title="Delete" 
							alt="Delete" 
							style="border: none;" 
							src="./images/btn_delete.gif"
							onclick="returnDelete();" 
							onmouseout="this.src='./images/btn_delete.gif';" 
							onmouseover="this.src='./images/btn_delete_02.gif';" />	<?php */?>											
							
		
		
					<table  cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="22%" style="white-space: nowrap;"><h2><?php echo $lang_title_value_inpatient ?></h2></td>
				<td width='78%' align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
  </table>
	
	<hr />
			        <table width="" border="0" cellpadding="2" cellspacing="0" id="addform1" >
			<tr style="white-space: nowrap">
				                <!-- <td   style="white-space: nowrap;">
									<input type="checkbox" class="checkbox" name="allCheck" value="" onclick="doHandleAll();" />								                                    </td>-->
								    <td   ><strong><?php echo $lang_sno	 ?></strong></td>
									<td   ><strong><?php echo "Hospital No."	 ?></strong></td>
									<td  colspan="2"  ><strong><?php echo $lang_inp_fullname	 ?></strong></td>
									<td   ><strong><?php echo $lang_inp_dateattended	 ?></strong></td>
				      				<td  ><strong><?php echo $lang_ward  ?></strong></td>
									<td>&nbsp;</td>
			
			</tr>
			
			<?php
			if(empty($search_field)||empty($search_value))
			//list all
			$inp->allrows($lang_curDB,$lang_curTB);
			else
			//return search
			$inp->rowSearch($search_field,$search_value) ;
			?>
  </table>

</form>
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() }
      });
      cal.manageFields("f_date", "f_date", "%Y-%m-%d");
    //]]></script>

</body>
</html>
