<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>

<?php  
//constructor
$hospital= new patient_admission();
$inp = new inpatient_admission();
$wards= new wards();
//delete row with ID
if (isset($id))
 $inp->Delete_row_from_key($_REQUEST['id']);
 
 //save new record
 if (isset($addinp) || !empty($addinp)){
    foreach ($_POST as $key=>$value) {	 $inp->inp[$key]=$value; }
    $inp->Save_Active_Row_as_New();
}

//edit existing record
if(isset($edit)|| !empty($edit)){

    foreach ($_POST as $key=>$value) {	 $inp->inp[$key]=$value; }
    $message=$inp->Save_Active_Row($inp_id);
}

?>


<script language="javascript" type="text/javascript">
function display(id1,id2,id3){  //show only 1st
		document.getElementById(id1).style.display = 'block';
		document.getElementById(id2).style.display = 'block';
		document.getElementById(id3).style.display = 'none';		
}

</script>
<script language="javascript" type="text/javascript">
<?php /*?>function validate(form){
var  msg='';
  if (document.form.ward_id.value=="") msg="You Need to select the Clinic.";
 if (document.form.clinic_id.value=="") msg="You Need to select the Clinic.";
  if (document.form.ward_name.value=="") msg="You Need to type the Ward name.";
   if (document.form.ward_bedspaces.value=="") msg="You Need to type the Bed Spaces.";
    if (document.form.siderooms.value=="") msg="You Need to type the Side Rooms.";
 if (!msg=''){
  alert(msg);
  return false;
 }
  alert(msg);

 document.frmWard.submit();
 return;
}<?php */?>
function validate(form){
  if (document.form.clinic_id.value == "") {
    alert( "Please select Clinic " );
    document.form.clinic_id.focus();
    return false ;
  }
   if (document.form.ward_name.value == "") {
    alert( "Please enter ward description" );
    document.form.ward_name.focus();
    return false ;
  }
  }
function doHandleAll() {
		with (document.standardView) {
			if(elements['allCheck'].checked == false){
				doUnCheckAll();
			}
			else if(elements['allCheck'].checked == true){
				doCheckAll();
			}
		}
	}

	function doCheckAll() {
		with (document.standardView) {
			for (var i=0; i < elements.length; i++) {
				if (elements[i].type == 'checkbox') {
					elements[i].checked = true;
				}
			}
		}
	}

	function doUnCheckAll() {
		with (document.standardView) {
			for (var i=0; i < elements.length; i++) {
				if (elements[i].type == 'checkbox') {
					elements[i].checked = false;
				}
			}
		}
	}

	function clear_form() {
		document.standardView.ward_name.value='';
		document.standardView.loc_code.options[0].selected=true;
	}
	function returnSearch() {

		if (document.standardView.ward_field.value == -1) {
			alert("Select the field to search!");
			document.standardView.ward_field.Focus();
			return false;
		};
		<?php /*?>document.standardView.capMode.value = 'SearchMode';
		document.standardView.submit();<?php */?>
		
	}
	
	function returnDelete() {
		$check = 0;
		with (document.standardView) {
			for (var i=0; i < elements.length; i++) {
				if ((elements[i].type == 'checkbox') && (elements[i].checked == true) && (elements[i].name == 'chkLocID[]')){
					$check = 1;
				}
			}
		}

		if ($check == 1){

			var res = confirm("Deletion might affect employee information, Job Titles. Do you want to delete ?");

			if(!res) return;

			document.standardView.delState.value = 'DeleteMode';
			document.standardView.pageNO.value=1;
			document.standardView.submit();
		}else{
			alert("Select at least one record to delete");
		}
	}
	
</script>
<script type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>

<BR />

<div class="innerPageborder">

	<!--<input type="hidden"  id="addWard" name="capMode" value="addWard" />-->
		
	<?php /*?><table   width="500"> 
	<tr><td width="318"><table  id="addform" style="display:none" > 
<form action="./index.php" method="post" name="form"  id="form" onsubmit="MM_validateForm('inp_dateattended','','R','inp_datedischarged','','R','inp_dischargedto','','R','inp_conddischarged','','R','inp_refferer','','R');return document.MM_returnValue"  > 
 <input type="hidden"  id="p" name="p" value="inp" />


<!--<tr>
<td align=right width="200" class="formdetails">Row ID:</td><td width="10"></td><td align=left width="200" class="formvalue"><input type="text" name="inp_id" value="" size="" maxlength="" class="forminput"></input></td>
</tr>-->
<tr>
<td align=right width="160" class="formdetails"><?php echo $lang_inpatadm_id ?>:</td>
<td width="0"></td>
<td align=left width="144" class="formvalue"><select  name="patadm_id" size=""  class=""><?php $hospital->get_admNodropdown()?></select></td>

</tr>
<tr>
<td align=right width="160" class="formdetails"><?php echo $lang_inp_dateattended ?></td>
<td width="0"></td>
<td align=left width="144" class="formvalue"><input type="text" name="inp_dateattended" value="" size="" maxlength="" class="forminput"></input></td>
</tr>
<tr>
<td align=right width="160" class="formdetails"><?php echo $lang_inp_datedischarged ?>:</td>
<td width="0"></td>
<td align=left width="144" class="formvalue"><input type="text" name="inp_datedischarged" value="" size="" maxlength="" class="forminput"></input></td>
</tr>
<tr>
<td align=right width="160" class="formdetails"><?php echo $lang_inp_dischargedto ?>:</td>
<td width="0"></td>
<td align=left width="144" class="formvalue"><input type="text" name="inp_dischargedto" value="" size="" maxlength="" class="forminput"></input></td>
</tr>
<tr>
<td align=right width="160" class="formdetails"><?php echo $lang_inp_conddischarged ?>:</td>
<td width="0"></td>
<td align=left width="144" class="formvalue"><input type="text" name="inp_conddischarged" value="" size="" maxlength="" class="forminput"></input></td>
</tr>

<tr>
<td align=right width="160" class="formdetails"><?php echo $lang_inp_refferer ?>:</td>
<td width="0"></td>
<td align=left width="144" class="formvalue"><input type="text" name="inp_refferer" value="" size="" maxlength="" class="forminput"></input></td>
</tr>
<tr>
<td align=right width="160" class="formdetails"><?php echo $lang_ward ?> :</td>
<td width="0"></td>
<td align=left width="144" class="formvalue"><select name="ward_id" value="" size="" maxlength="" class=""><?php $wards->getWard_id() ?></select></td>
</tr>
<tr>
<td align=right width="160" class="formdetails"><?php echo $lang_ct_id ?>:</td>
<td width="0"></td>
<td align=left width="144" class="formvalue"><select type="text" name="ct_id" size="" maxlength="" class="forminput"><?php echo $inp->getCondtype_id() ?></select></td>
</tr>
<tr>
<td width="160" align=right></td>
<td width="0"></td>
<td width="144"></td>
</tr>
<tr>
<td width="160" align=right><input type=reset value="Reset"><input name="addinp"  type="submit"  id="addinp"value="Save"  onClick="return  validate(this)"/></td><td width="0"></td>
<td width="144"></td>
</tr>
<tr><td colspan=3></td></tr></form></table>
		</td>
	<td width="170" valign="top">
	<table  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="168"><a href="#">treatment</a></td>
  </tr>
  <tr>
    <td height="30"><a href="#">treatment prescription</a></td>
  </tr>
  <tr>
    <td><a href="#">Diagnosis</a></td>
  </tr>
  <tr>
    <td><a href="#">laboratory</a></td>
  </tr>
</table>

	
	
	</td>
	</tr>
</table>		<?php */?>
</center>

<form name="standardView" method="post" action="index.php?p=inp">

			<BR /><BR />
	<div class="innerPageborder"><h2><?php echo $lang_title_key ?> :  <?php echo $lang_title_value_inpatient ?></h2></div>
		
			<div  ><?php /*?><img 
						title="Add" 
						alt="Add" 
						src="./images/btn_add.gif" 
						style="border: none"
						onClick="display('addform','addform1','atminter');" 
						onMouseOut="this.src='./images/btn_add.gif" ;" 
						onMouseOver="this.src='./images/btn_add_02.gif';" /><img 
							title="Delete" 
							alt="Delete" 
							style="border: none;" 
							src="./images/btn_delete.gif"
							onclick="returnDelete();" 
							onmouseout="this.src='./images/btn_delete.gif';" 
							onmouseover="this.src='./images/btn_delete_02.gif';" />	<?php */?>											</div>
		
		<div style="width: 98%;">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="22%" style="white-space: nowrap;"><h3><?php echo $lang_PageTitle_inpatient ?></h3></td>
				<td width='78%' align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			</table>
		</div>
	
			<table  border="0" cellpadding="5" cellspacing="0" class="">
				<tr>
					<td width="200" >
						<label for="loc_code" style="float: left; padding-right: 10px;">Search By:</label><br>
						<select  name="search_field"><?php echo $inp->GetSearchFields()	 ?></select>
					</td>
					<td  class="dataLabel" style="white-space: nowrap;">
						<label for="ward_value" style="float: left; padding-right: 10px;">Search For:</label><br>
						<input type=text size="20" name="search_value"  value="" />					</td>
					<td align="right" width="180" >
<?php /*?>						<img 
							title="Search" 
							alt="Search" 
							src="./images/btn_search.gif" 
							onclick="returnSearch();" 
							onmouseout="this.src='./images/btn_search.gif';" 
							onmouseover="this.src='./images/btn_search_02.gif';" /><?php */?>&nbsp;&nbsp;
						<img 
							title="Clear" 
							alt="Clear"
							style="border: none"
							src="images/btn_clear.gif"
							onclick="clear_form();" 
							onmouseout="this.src='./images/btn_clear.gif';" 
							onmouseover="this.src='./images/btn_clear_02.gif';" /><input name="search" type="submit" value="<?php echo $lang_search_button_label ?>" onclick="returnSearch();"/>					</td>
				</tr>
			</table>
	
	
		<table width="100%" border="0" cellpadding="5" cellspacing="0" id="addform1" style="display:block">
			<tr style="white-space: nowrap">
				<td   style="white-space: nowrap;">
									<input type="checkbox" class="checkbox" name="allCheck" value="" onclick="doHandleAll();" />								<?php //echo $lang_sno; ?></td>
									<td   ><?php echo $lang_inp_id  ?>
									</td>
									<td   >
						<?php echo $lang_inpatadm_id	 ?>			</td>
									<td   >
						<?php echo $lang_inp_dateattended	 ?>			</td>
				      			<td  ><?php echo $lang_inp_datedischarged ?></td>
								<td  ><?php echo $lang_inp_dischargedto  ?></td>
    							<td  ><?php echo $lang_inp_conddischarged  ?></td>
								<td  ><?php echo $lang_inp_refferer  ?></td>
								<td  ><?php echo $lang_ward  ?></td>
			
			</tr>
			
			<?php
			if(empty($search_field)||empty($search_value))
			//list all
			$inp->allrows();
			else
			//return search
			$inp->rowSearch($search_field,$search_value) ;
			?>
  </table>

</form>

</body>
</html>
