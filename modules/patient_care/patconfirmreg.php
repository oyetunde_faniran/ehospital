<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
} // End of defined() IF.
			$regid = isset($_GET["regid"]) ? (int)$_GET["regid"] : 0;
			$reg_clinic = isset($_GET["regclinic"]) ? (int)$_GET["regclinic"] : 0;
			$reg_dept = isset($_GET["regdept"]) ? (int)$_GET["regdept"] : 0;
			
			$reg = new patRegistration;
			$select = new patSelectOptions;
			$reg->Load_from_key($regid);			
?>
<h3><?php echo $department_label ?>: <?php $dept_detail = $select->selectDepartment($reg_dept, $curLangField);
if(is_array($dept_detail)){
extract($dept_detail);
echo $dept_name;
}
 ?></h3>
<h4><?php echo $clinic_label ?>: <?php $clinic_detail = $select->selectClinic($reg_clinic, $curLangField);
if(is_array($clinic_detail)){
extract($clinic_detail);
echo $clinic_name;
}
 ?></h4>
<div id="tabs">
			<ul>
				<li><a href="#tabs-1"><?php echo $personal_detail_label ?></a></li>
				<li><a href="#tabs-2"><?php echo $next_of_kin_label ?></a></li>
				<li><a href="#tabs-3"><?php echo $others_label ?></a></li>
         		<li style="float:right"><input type="button" name="update" id="update" value=" <?php echo $continue_label ?> " onclick="window.location.href='index.php?regid=<?php echo $regid ?>&m=patient_care&p=referer&regdept=<?php echo $reg_dept ?>&regclinic=<?php echo $reg_clinic ?>'" /></li>
                <li style="float:right"><input type="button" name="edit" id="edit" value=" <?php echo $edit_label ?> " onclick="window.location.href='index.php?regid=<?php echo $regid ?>&m=patient_care&p=edit_reg&regdept=<?php echo $reg_dept ?>&regclinic=<?php echo $reg_clinic ?>'"/></li>
			</ul>   
 <div id="tabs-1">        
   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="10" >
      <?php if($reg->getReg_passport() != ""){?>  <tr>
          <td colspan="4" align="left" valign="top">
          <img class="img-rounded img-thumbnail img-responsive" style="height: 120px" src="./modules/patient_care/passport/<?php echo $reg->getReg_passport() ?>"/>&nbsp; </td>
        </tr><?php }?>
        <tr>
          <td colspan="4" align="left" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top">
          <strong><?php echo $hospital_no_label ?>: </strong>          </td>
          <td align="left" valign="top"><strong><?php echo $reg->getReg_hospital_no(); ?></strong></td>
          <td align="left" valign="top">
            <strong><?php echo $old_hospital_no_label ?>:           </strong></td>
          <td align="left" valign="top"><?php echo $reg->getReg_hospital_oldno(); ?></td>
        </tr>
        <tr>
            <td width="20%" align="left" valign="top"><strong><?php echo $surname_label ?>:</strong></td>
            <td width="30%" align="left" valign = "top">
              <?php echo $reg->getReg_surname() ?>             </td>
          <td width="20%" align="left" valign="top"><strong><?php echo $othernames_label ?>:</strong></td>
          <td width="30%" align="left" valign="top"><?php echo $reg->getReg_othernames() ?></td>
      </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $dob_label ?>:</strong></td>
          <td align="left" valign="top"><?php echo $reg->getReg_dob() ?></td>
          <td align="left" valign="top"><strong><?php echo $gender_label ?>:</strong></td>
          <td align="left" valign="top">
              <?php if($reg->getReg_gender()==0) echo $gender_0_label; ?>
           <?php if($reg->getReg_gender()==1) echo $gender_1_label; ?>          </td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $phone_label ?>:</strong></td>
          <td align="left" valign="top"><?php echo $reg->getReg_phone() ?></td>
          <td align="left" valign="top"><strong><?php echo $email_label ?>:</strong></td>
          <td align="left" valign="top"><?php echo $reg->getReg_email() ?></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $address_label ?>:</strong></td>
          <td align="left" valign="top"><?php echo $reg->getReg_address() ?></td>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>

        <tr>
                  <td width="19%" align="left" valign="top"><strong><?php echo $nationality_label ?>:</strong></td>
                  <td width="34%" align="left" valign="top">  <?php
                        $select->selectNationalityName($reg->getNationality_id());
        
                  ?>    </td>
                  <td width="15%" align="left" valign="top"><strong><?php echo $state_label ?>:</strong></td>
                  <td width="32%" align="left" valign="top">  <?php
                        $select->selectStateName($reg->getState_id());
        
                  ?>       </td>
                </tr>
                
                <tr>
                  <td align="left" valign="top"><strong><?php echo $religion_label ?>:</strong></td>
                  <td align="left" valign="top"><?php echo $reg->getReg_religion() ?></td>
                  <td align="left" valign="top"><strong><?php echo $civilstate_label ?>:</strong></td>
                  <td align="left" valign="top"><?php if($reg->getReg_civilstate()=='0') echo $single_label; ?>
                                                <?php if($reg->getReg_civilstate()=='1') echo $married_label; ?>                 
                                                <?php if($reg->getReg_civilstate()=='2') echo $divorced_label; ?>
                                                <?php if($reg->getReg_civilstate()=='3') echo $married_label; ?></td>
                </tr>
                
                <tr>
                  <td align="left" valign="top"><strong><?php echo $occupation_label ?>:</strong></td>
                  <td align="left" valign="top"><?php echo $reg->getReg_occupation() ?></td>
                  <td align="left" valign="top"><strong><?php echo $placeofwork_label ?>:</strong></td>
                  <td align="left" valign="top"><?php echo $reg->getReg_placeofwork() ?></td>
                </tr>
        </table>
   </div>

<?php		 
	include_once("modules/$m/patreg_confirm_others_inc.php"); 
?>
<!--        <tr>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
</table>-->
</div>
<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.tabs.js"></script>
<script type="text/javascript">
$(function() {
    $("#tabs").tabs().find(".ui-tabs-nav");
});
</script>