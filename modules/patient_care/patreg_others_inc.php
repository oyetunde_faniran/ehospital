<div id="tabs-2">   
 <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
<tr>
          <td width="20%" align="left" valign="top"><strong><?php echo $nationality_label ?>:</strong></td>
          <td width="80%" align="left" valign="top"><select required name="reg[nationality]" id="first">
          <option value="" >-------</option>
          </select>          </td>
    </tr>
        
        <tr>
          <td width="20%" align="left" valign="top"><strong><?php echo $state_label ?>:</strong></td>
          <td width="80%" align="left" valign="top"><select required name="reg[state]" id="second">
              <option value="" >-------</option>
          </select></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $civilstate_label ?>:</strong></td>
          <td align="left" valign="top"><select required name="reg[civilstate]">
              <option value=" " selected="selected"><?php echo $select_label ?> <?php echo $civilstate_label ?></option>
              <option value="0"><?php echo $single_label ?></option>
              <option value="1"><?php echo $married_label ?></option>
              <option value="2"><?php echo $divorced_label ?></option>
              <option value="3"><?php echo $widowed_label ?></option>
          </select></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $religion_label ?>:</strong></td>
          <td align="left" valign="top">
          		<!--<input type="text" name="reg[religion]" />-->
                <select name="reg[religion]">
                	<option value=""><?php echo $selectReligion_label; ?></option>
                	<option value="<?php echo $xtian_label; ?>"><?php echo $xtian_label; ?></option>
                	<option value="<?php echo $islam_label; ?>"><?php echo $islam_label; ?></option>
                	<option value="<?php echo $others_label; ?>"><?php echo $others_label; ?></option>
                </select>
          </td>
        </tr>
        
        <tr>
          <td align="left" valign="top"><strong><?php echo $occupation_label ?>:</strong></td>
          <td align="left" valign="top"><input type="text" name="reg[occupation]" /></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $placeofwork_label ?>:</strong></td>
          <td align="left" valign="top"><textarea name="reg[placeofwork]" cols="20" rows="5"></textarea></td>
        </tr>
</table>
</div>


<div id="tabs-3">   
 <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
        <tr>
          <td width="20%" align="left" valign="top"><strong><?php echo $name_label ?>:</strong></td>
          <td width="80%" align="left" valign="top"><input type="text" name="reg[nextofkin]" /></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $nokrelationship_label ?>:</strong></td>
          <td align="left" valign="top"><input type="text" name="reg[nokrelationship]" /></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $phone_label ?>:</strong></td>
          <td align="left" valign="top"><input type="text" name="reg[nokphone]" /></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $email_label ?>:</strong></td>
          <td align="left" valign="top"><input type="text" name="reg[nokemail]" /></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong><?php echo $address_label ?>:</strong></td>
          <td align="left" valign="top"><textarea name="reg[nokaddress]" cols="20" rows="5"></textarea></td>
        </tr>
</table>
</div>


<!--BEGIN Others Tab-->
<div id="tabs-4">
		<!--BEGIN Accordion-->
        <div id="accordion">
        
        <!--BEGIN BOOKING CAT-->
            <!--<h3 style="padding: 5px 0px 5px 40px;"><?php //echo $bookingcategory_label ?></h3>
            <div>
                <table border="0" align="left" cellpadding="3" cellspacing="3" id="bookingTable">
                    <tr>
                        <td align="left" valign="top"><strong><?php //echo $bookingcategory_label ?>:</strong></td>
                        <td align="left" valign="top"><input type="text" name="reg[matbookingcat]" /></td>
                    </tr>
                </table>
            </div>-->
        <!--END BOOKING CAT-->
        
        
        <!--BEGIN RETAINERS-->
            <h3 style="padding: 5px 0px 5px 40px;"><?php echo $retainership_label ?></h3>
            <div>
                   <table border="0" align="left" cellpadding="3" cellspacing="3" id="retainerTable">
                      <tr>
                         <td width="40%" align="left" valign="top"><strong><?php echo $company_label ?>:</strong></td>
                         <td width="60%" align="left" valign="top">
                         <select name="reg[retainership_id]" id="retcompanies" onchange="processPlans4Retainers('retcompanies', 'retplans');">
                            <option value="" selected="selected"> <?php echo $select_company_label ?> </option>
                                    <?php
                                        $select->selectRetainer('1');
                                    ?>
                         </select>		 
                         </td>
                      </tr>

					<tr>
                      <td width="40%" align="left" valign="top"><strong><?php echo $plan_label ?>:</strong></td>
                      <td width="60%" align="left" valign="top">
                          <select name="reg[ret_plan]" id="retplans">
                          		<option selected="selected" value=""><?php echo $select_plan_label ?></option>
                                <?php
                                   // echo $select->selectRetainerPlans("retainer", 1);
                                ?>
                           </select>
                           <span id="retplan_loader" style="display: none; padding-left: 10px; color: #999;"><?php echo $loading_label; ?></span>
                       </td>
                    </tr>

                      <tr>
                        <td align="left" valign="top"><strong><?php echo $dependant_label ?>:</strong></td>
                        <td align="left" valign="top">
                        <select name="reg[isdependant]">
                            <option value=" " selected="selected"><?php echo $select_option_label ?></option>
                            <option value="0"><?php echo $no_label ?></option>
                            <option value="1"><?php echo $yes_label ?></option>
                        </select></td>
                        </tr>
                            <tr>
                              <td align="left" valign="top"><strong><?php echo $employee_name_label ?>:</strong></td>
                              <td align="left" valign="top">          
                                  <input type="text" name="reg[employee]" id="reg[employee]" /></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top"><strong><?php echo $identity_card_no_label ?>:</strong></td>
                              <td align="left" valign="top"><input type="text" name="reg[idcardno]" /></td>
                            </tr>
                    </table>
            </div>
        <!--END RETAINERS-->
        
        
        <!--BEGIN NHIS-->
            <h3 style="padding: 5px 0px 5px 40px;"><?php echo $nhis_label ?></h3>
            <div>
                <table border="0" cellpadding="3" cellspacing="3" align="left" id="nhisTable">
                   <tr>
                      <td width="40%" align="left" valign="top"><strong><?php echo $hmo_label ?>:</strong></td>
                      <td width="60%" align="left" valign="top">
                          <select name="reg[nhis_id]">
                          		<option selected="selected" value=""><?php echo $select_hmo_label ?></option>
                                <?php
                                    $select->selectRetainer('0');
                                ?>
                           </select>
                       </td>
                    </tr>

                   <tr>
                      <td width="40%" align="left" valign="top"><strong><?php echo $plan_label ?>:</strong></td>
                      <td width="60%" align="left" valign="top">
                          <select name="reg[nhis_plan]">
                          		<option selected="selected" value=""><?php echo $select_plan_label ?></option>
                                <?php
                                    echo $select->selectRetainerPlans("nhis");
                                ?>
                          </select>
                       </td>
                    </tr>

                     <tr>
                        <td align="left" valign="top"><strong><?php echo $card_no_label ?>:</strong></td>
                        <td align="left" valign="top"><input type="text" name="reg[cardno]" />
                        <input name="regextra" type="hidden" id="regextra" value="1" /></td>
                    </tr>
                </table>
            </div>
        <!--END NHIS-->
        </div>
        <!--BEGIN Accordion-->
</div>
<!--END Others Tab-->

<script type="text/javascript" language="javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/patreg.js"></script>

<script type="text/javascript">
function expandDiv(id1,id2,cont){
	document.getElementById(id1).style.display = 'block';
  	p = document.getElementById(id2);
	p.innerHTML = "<p onclick=\"collapseDiv('"+id1+"','"+id2+"','"+cont+"')\"><img src='images/collapse.jpg' /> "+cont+"</p>";
}
function collapseDiv(id1,id2,cont){
	document.getElementById(id1).style.display = 'none';
  	p = document.getElementById(id2);
	p.innerHTML = "<p onclick=\"expandDiv('"+id1+"','"+id2+"','"+cont+"')\"><img src='images/expand.jpg' /> "+cont+"</p>";
}
</script>