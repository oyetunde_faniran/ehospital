<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.

	$dept_id = isset($_GET['b']) ? $_GET['b'] : 0;
	$clinic_id = isset($_GET['c']) ? $_GET['c'] : 0;
	$hospital_no = isset($_GET['d']) ? $_GET['d'] : 0;
	$consultant_id = isset($_GET['a']) ? $_GET['a'] : 0;
	$authorizingConsultant = isset($_GET['e']) ? $_GET['e'] : 0;
	$select = new patSelectOptions();
	$reg = new patRegistration();
	$reg->loadPatientDetails($hospital_no);
	$patuser = new patUsers;
	$consult_details = $patuser->isConsultant4rmConsultantID($consultant_id);	//The consultant the patient was assigned to
	$authorizingConsultant = $patuser->isConsultant4rmConsultantID($authorizingConsultant);		//The consultant that authorized the patient's registration
	$authorizingConsultantName = isset($authorizingConsultant["consult_name"]) ? $authorizingConsultant["consult_name"] : "";
	if(is_array($consult_details)){
		extract($consult_details);
	}
?>
<table width="100%" border="0">
   <tr>
    <td width="35%" align="left"><strong><?php echo $department_label; ?>:</strong> </td>
    <td align="left">
<?php
    $dept_detail = $select->selectDepartment($dept_id, $curLangField);
    if(is_array($dept_detail)){
        extract($dept_detail);
        echo $dept_name;
    }
?>
    </td>
  </tr>
  <tr>
    <td><strong><?php echo $clinic_label; ?>:</strong></td>
    <td><?php $clinic_detail = $select->selectClinic($clinic_id, $curLangField);
				if(is_array($clinic_detail)){
				extract($clinic_detail);
				echo $clinic_name; 
				}
		?></td>
  </tr>
 <tr>
    <td><strong><?php echo $hospital_no_label; ?>:</strong></td>
    <td><?php echo $hospital_no ?></td>
  </tr>

 <tr>
    <td><strong><?php echo $old_hospital_no_label; ?>.:</strong></td>
    <td>
<?php 
	$query = "SELECT reg_hospital_oldno FROM registry
				WHERE reg_hospital_no = '$hospital_no'";
	$conn = new DBConf();
	$result = $conn->execute($query);
	if ($conn->hasRows($result, 1)){
		$row = mysql_fetch_array ($result, MYSQL_ASSOC);
		$oldHospitalNo = $row["reg_hospital_oldno"];
	} else $oldHospitalNo = "--";
	echo $oldHospitalNo;
?>
	</td>
  </tr>
  
  <tr>
    <td><strong><?php echo $name_label; ?>:</strong></td>
    <td><?php echo strtoupper($reg->getReg_surname())." ".$reg->getReg_othernames(); ?></td>
  </tr>

  <tr>
    <td><strong><?php echo $authorizedBy_label; ?>:</strong></td>
    <td><?php echo $authorizingConsultant["consult_name"]; ?></td>
  </tr>

  <tr>
    <td><strong><?php echo $assigned_consultant_label; ?>:</strong></td>
    <td><?php echo $consult_name; ?></td>
  </tr>
  <tr>
    <td colspan="2">
        <div style="width:50%; padding:0; float:left"><a href="index.php?p=register_patient&m=patient_care">&laquo; <?php echo $patreg_backbtn ?></a></div>
        <!--<div style="width:50%; padding: 0; float:left; text-align:right"><a href="#" onclick="JAVASCRIPT: window.open ('patnewwindow_index.php?q=reg01&a=<?php echo $hospital_no ?>&b=<?php echo $clinic_id ?>&c=<?php echo $dept_id ?>&d=<?php echo $consultant_id ?>', 'newwindow2', 'scrollbars=no, width=500, status=no, toolbar =no, menubar = no, height=400, resizeable=no'); return false"><?php echo $patreg_printnow_label; ?></a></div>-->
    </td>
  </tr>
</table>