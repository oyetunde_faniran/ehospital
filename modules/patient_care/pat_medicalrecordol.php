<?php # Script 2.6 - search.inc.php

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>
<style type="text/css">
	.suggestionsBox {
		position: relative;
		left: 30px;
		margin: 0px 0px 0px 0px;
		width: 200px;
		background-color: #212427;
		-moz-border-radius: 7px;
		-webkit-border-radius: 7px;
		border: 2px solid #000;	
		color: #fff;
	}
	
	.suggestionList {
		margin: 0px;
		padding: 0px;
	}
	
	.suggestionList li {
		
		margin: 0px 0px 3px 0px;
		padding: 3px;
		cursor: pointer;
	}
	
	.suggestionList li:hover {
		background-color: #659CD8;
	}
 #multiTransferExample a {
  display: block;
  border: 1px solid #aaa;
  text-decoration: none;
  background-color: #fafafa;
  color: #123456;
  margin: 2px;
  clear: both;
 }
 #multiTransferExample div {
  float:left;
  text-align: center;
  margin: 10px;
 }
 #multiTransferExample select {
  width: 100px;
  height: 80px;
 }

</style>
 
<script type="text/javascript" src="library/auto_complete/jquery-1.2.1.pack.js"></script>
<script type="text/javascript">
	function lookup(inputString) {
		if(inputString.length == 0) {
			// Hide the suggestion box.
			$('#suggestions').hide();
		} else {
			$.post("classes/patRPC.php", {queryString: ""+inputString+""}, function(data){
				if(data.length >0) {
					$('#suggestions').show();
					$('#autoSuggestionsList').html(data);
				}
			});
		}
	} // lookup
	
	function fill(thisValue) {
		$('#inputString').val(thisValue);
		setTimeout("$('#suggestions').hide();", 200);
	}
	
	
	
		function AddSub(dir) {
		var dd ;
			var selsub=document.getElementById("inputString");
			var selsub2=document.getElementById("inputString2");
			if (selsub.value.length<1){
				alert("Enter a drugname please") }
				else if (selsub2.value.length<1){
				alert("Enter drug dosage please") }
				else{
				if(dir==1) { 
				
						if (document.casenote.sub1.value.length>0) {
						 dd=document.casenote.sub1.value;	
							 }else{
							 dd="";
							 }
						var dd1=selsub.value+" ["+selsub2.value+"]"; 
						document.casenote.sub1.value=dd+dd1+"\n";
						document.casenote.inputString.value="";
						document.casenote.inputString2.value="";
						 }
					
			}
		}
		
		function ClearSub(){
		  document.casenote.sub1.value="";
		}

</script>
<?php
$app_id = isset($_GET['aa']) ? $_GET['aa'] : 0;
 ?>
<?php 
$treat = new patTreatment;
if(isset($_POST['findpatient']) && isset($_POST['findpatient'])){
$app_id = $treat->getAppID($_POST['findpatient']);
if ($app_id == "")
echo "<font color='#FF0000' style='font-weight:bold'>".$patient_not_on_appointment_list."</font>";
}
$consult_details = $patuser->isconsultant($myStaffid);
if ($stafftype == '1'){
echo $error_msg_3;
}elseif(is_array($consult_details) == FALSE){
$msg = "error_msg_".$consult_details;
echo $$msg;
}else{
extract($consult_details);
?>
<p><strong><?php echo $consultant_label ?>: <?php echo $consult_name ?></strong><br />
<strong><?php echo $clinic_label ?></strong>: <?php $clinic_detail = $select->selectClinic($myClinicid, $curLangField);
if(is_array($clinic_detail)){
extract($clinic_detail);
echo $clinic_name; 
}?></p>
 

<?php 
//die ((string)$app_id);
	if (!empty($app_id)){
		$patdetails = $treat->getPatientDetails($app_id, $consult_id);
		//die ("<pre>" . print_r($patdetails, true) . "</pre>");
		if($patdetails == false){
			echo $patient_not_on_appointment_list;
		} elseif (is_array($patdetails)){
		extract($patdetails);
$subLinks = "<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=med01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow1\", \"scrollbars=yes, status=no, toolbar =no, menubar = no, fullscreen=yes \")'>".$view_medical_history_label."</a>
<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=app01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow2\", \"scrollbars=no, width=500, status=no, toolbar =no, menubar = no, height=400, resizeable=no\")'>".$schedule_appointment_label."</a>
<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=admit01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow3\", \"scrollbars=no, status=no, toolbar =no, menubar = no, width=600, height=300\")'>".$admit_label."</a>
<a href='#' onclick='JAVASCRIPT: window.open (\"patnewwindow_index.php?q=transfer01&a=".$patadmid."&b=".$hospital_no."&c=".$consult_id."&d=".$consult_clinic."\", \"newwindow4\", \"scrollbars=no, toolbar =no, menubar = no, status=no, width=600, height=300\")'>".$transfer_label."</a>";
?> 
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="16%" rowspan="2"><?php if ($passport !="") {?><img src="modules/<?php echo $m ?>/passport/<?php echo $passport ?>" border="0" />
    	<?php }else{  ?>
        <img src="modules/<?php echo $m ?>/passport/default.jpg" border="0" />
        <?php 
			}
		?>    </td>
    <td width="84%" align="left" valign="top"><strong><?php echo $name_label ?>: </strong> <?php echo $name ?>&nbsp;&nbsp;
	<strong><?php echo $hospital_no_label ?>: </strong> <?php echo $hospital_no; ?><br />      
    <strong><?php echo $report_appointment_age ?>: </strong><?php echo $age ?>&nbsp;&nbsp;<strong><?php echo $gender_label ?>: </strong>
      <?php if($sex == 0) echo $gender_0_label; ?><?php if($sex == 1) echo $gender_1_label; ?><br />
      <strong><?php echo $report_appointment_date ?>:</strong> <?php echo $appdate ?>&nbsp;&nbsp;
	  <strong><?php echo $report_appointment_starttime ?>: </strong><?php echo $starttime ?>&nbsp;&nbsp;
	  <strong><?php echo $report_appointment_endtime ?>:</strong> <?php echo $endtime; ?>      
      <span id="status">&nbsp;</span>
    </td>
  </tr>
</table>

<?php 
if($appdate < $presdate){
if($appdate > $presdate) echo $future_appointment_label;
if($appdate < $presdate) echo $missed_appointment_label;
?>
<p>&nbsp; </p>
<a href=""><?php echo $click_here_reschedule_appointment_label ?></a>
<?php
}else{
?>
<form action="index.php" method="post" name="casenote">
	  <div align="right">
          <input type="submit" name="Submit" value="<?php echo $save_label ?>" />
      </div>
	  				<input type="hidden" name="m" value="patient_care" />
                    <input type="hidden" name="p" value="pat_treatment" />
            	    <input type="hidden" name="treat[patadm_id]" value="<?php echo $patadmid; ?>">
				    <input type="hidden" name="treat[user_id]" value="<?php echo $myUserid; ?>"> 
				    <input type="hidden" name="treat[app_id]" value="<?php echo $app_id; ?>"> 
<ul id="tabs">
      <li><a href="#tabs-1"><?php echo $history_taking_label ?></a></li>
      <li><a href="#tabs-2"><?php echo $examination_label ?></a></li>
      <li><a href="#tabs-3"><?php echo $plan_label ?></a></li>
  </ul>   
      <div  class="tabContent" id="tabs-1">           
        <table border="0" cellspacing="1" cellpadding="5" width="100%">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td ><?php echo $complaint_label ?></td>
                <td><textarea cols="25" rows="4" name="hist[complaint]"></textarea></td>
                <td><?php echo $complaint_history_label ?></td>
                <td><textarea cols="25" rows="4" name="hist[complainthist]"></textarea></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td ><?php echo $medical_history_label ?></td>
                <td><textarea cols="25" rows="4" name="hist[medhist]"></textarea></td>
                <td><?php echo $social_history_label ?></td>
                <td><textarea cols="25" rows="4" name="hist[sochist]"></textarea></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>

            </tr>
            <tr>
                <td><?php echo $drug_history_label ?></td>
                <td><textarea cols="25" rows="4" name="hist[drughist]"></textarea></td>
                <td><?php echo $system_review_label ?></td>
                <td><textarea cols="25" rows="4" name="hist[sysreview]"></textarea></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><?php echo $others_label ?></td>
                <td><textarea cols="25" rows="4" name="hist[others]"></textarea></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
      </div>
      <div  class="tabContent" id="tabs-2">        
        <table border="0" cellspacing="1" cellpadding="5" width="100%">
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
                <td><?php echo $general_exam_label ?></td>
                <td><textarea cols="25" rows="4" name="treat[genexam]"></textarea></td>
                <td><?php echo $diagnosis_label ?></td>
                <td><textarea cols="25" rows="4" name="treat[diagnosis]"></textarea></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
                <td><?php echo $system_examination_label ?></td>
                <td><textarea cols="25" rows="4" name="treat[sysexam]"></textarea></td>
                <td><?php echo $differential_diagnosis_label ?></td>
                <td><textarea cols="25" rows="4" name="treat[diffdiagnosis]"></textarea></td>
            </tr>
        </table>
      </div>
      <div  class="tabContent" id="tabs-3">
        <table border="0" cellspacing="1" cellpadding="5" width="100%">
        <tr>
          <td valign="top">&nbsp;</td>
          <td valign="top">&nbsp;</td>
          <td colspan="2" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td valign="top"><?php echo $drug_name_label; ?>:<br /></td>
        <td valign="top"><input type="text" value="" id="inputString" onkeyup="lookup(this.value);" onblur="fill();" />
			 <div class="suggestionsBox" id="suggestions" style="display: none;">
				<div class="suggestionList" id="autoSuggestionsList">
				
&nbsp;				</div>
			</div>   
		    <br /></td>
        <td rowspan="2" valign="top" id="clear">&nbsp;</td>
        <td rowspan="2" valign="top" id="clear"><textarea cols="25" rows="5" name="pres[desc]" id="sub1" readonly="readonly"></textarea>
          <a href="#clear" onclick="ClearSub();return false"><?php echo $clear_label; ?></a></td>
        </tr>
        
        <tr>
          <td valign="top">Dosage:</td>
          <td valign="top"><input type="text" value="" id="inputString2" onkeyup="lookup(this.value);" onblur="fill();" />
          &nbsp;&nbsp;
          <input id="add1" onclick="AddSub(1)" type="button" value=" >> " /></td>
          </tr>
        <tr>
          <td valign="top">&nbsp;</td>
          <td valign="top">&nbsp;</td>
          <td colspan="2" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td valign="top"><?php echo $investigation_label ?>:</td>
          <td valign="top"><textarea cols="25" rows="5" name="lab[investigation]" id="plan[investigation]"></textarea></td>
          <td valign="top"><?php echo $other_treatment_label ?>:</td>
          <td valign="top"><textarea cols="25" rows="5" name="treatment[treatment]" id="plan[desc]"></textarea></td>
        </tr>
        </table>
      </div>        
	  
</form>
<p>
  <?php 
}
?>
</p>
<p>&nbsp; </p>

<div style="background-color:#CCCCCC"><hr color="#333333" width="100%"><?php echo $previous_appointment_label ?><hr color="#333333" width="100%"></div>
  
<?php 
	$visits = $treat->getOtherAppointment($hospital_no, $consult_id);
	if($visits != ""){
?>
<table width="100%" border="0" cellpadding="5" cellspacing="0">
  <tr>
    <td><strong><?php echo $date_label ?></strong></td>
    <td><strong><?php echo $diagnosis_label ?></strong></td>
    <td><strong><?php echo $prescription_label ?></strong></td>
    <td><strong><?php echo $investigation_label ?></strong></td>
     <td><strong><?php echo $treatment_label ?></strong></td>
   <td>&nbsp;</td>
  </tr>
  <?php
	echo $visits;
?>
</table>
<?php
	
}else{
 echo $no_previous_visits;
}
}
}else{
?>
<form action="index.php?<?php echo $_SERVER['QUERY_STRING']; ?>" method="post" name="search_patient" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td width="24%" align="left" valign="top"><?php echo $hospital_no_label; ?></td>
              <td width="76%" align="left"><input type="text" name="findpatient" id="findpatient" />
              &nbsp;&nbsp;
              <label>
              <input type="submit" name="Submit2" value="<?php echo $submit_label ?>" />
              </label></td>
            </tr>
            <tr>
              <td colspan="2" align="left" valign="top"><?php echo $hospital_no_msg_label; ?></td>
            </tr>
            <tr>
              <td colspan="2" align="left" valign="top">&nbsp;</td>
            </tr>
  </table>
</form>
<?php
}
?>

<?php
}
?>
