<script type="text/javascript"  src="./library/timepicker/jquery.min.js"> </script>
<script type="text/javascript"  src="./library/timepicker/jquery.timepicker.js"></script>
<script type="text/javascript" src="./library/timepicker/timepicker.js">  </script>
  <style>
  div.time-picker {
  position: absolute;
  height: 200px;
  width:4em; /* needed for IE */
  overflow: auto;
  background: #fff;
  border: 1px solid #000;
  z-index: 99;
}
div.time-picker-12hours {
  width:6em; /* needed for IE */
}

div.time-picker ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}
div.time-picker li {
  padding: 1px;
  cursor: pointer;
}
div.time-picker li.selected {
  background: #316AC5;
  color: #fff;
}
div.time-picker {
    margin-top: 1.6em;
	width: 4.7em;
  }

</style>
<form name="appointment" action="patnewwindow_index.php?q=<?php echo $vv; ?>&b=<?php echo $bb; ?>" method="post">
<table border="0" cellspacing="5" cellpadding="5">
<!--  <tr>
    <td colspan="4" align="center"><?php //echo $schedule_appointment_label ?> </td>
  </tr>-->
  <tr>
    <td width="20%"><strong><?php echo strtoupper($hospital_no_label); ?></strong>:</td>
    <td width="28%"><?php echo $bb ?><input type="hidden" name="hospital_no" id="hospital_no" value="<?php echo $bb ?>" />
    				<input type="hidden" name="appoint[consultant_id]" value="<?php echo $cc ?>" />
    				<input type="hidden" name="appoint[patadm_id]" value="<?php echo $aa ?>" />
    				<input type="hidden" name="appoint[dept_id]" value="<?php echo $dept ?>" />
     				<input type="hidden" name="appoint[clinic_id]" value="<?php echo $dd ?>" />
                    <input type="hidden" name="appoint[user_id]" value="<?php echo $myUserid ?>" />
                    <?php if (isset($gg)){ ?>
                    	<input type="hidden" name="app_id" value="<?php echo $gg ?>" />
                    <?php
					}
                    ?></td>
    <td width="11%"><strong><?php echo $report_appointment_date ?></strong>:</td>
    <td width="41%"><input type="text" name="appoint[date]" maxlength="10" value="<?php echo $appdate ?>" id="f_date" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><?php echo $report_appointment_starttime  ?></strong>:</td>
    <td><input name="appoint[startime]" type="text" id="time3" value="<?php echo $appstart ?>" size="5" maxlength="5" /></td>
    <td><strong><?php echo $report_appointment_endtime ?></strong>:</td>
    <td><input name="appoint[endtime]" type="text" id="time4" value="<?php echo $append ?>" size="5" maxlength="5" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><?php echo $report_appointment_apptype ?></strong>:</td>
    <td>
    	<?php echo $pat_appointment_followup ?>
    	<input type="hidden" name="appoint[app_type]" id="app_type" value="2" />
        <!--<select name="appoint[app_type]" id="app_type">
          <option value="1" <?php //if ($apptype==1) echo "selected" ?>><?php //echo $pat_appointment_consultation ?></option>
          <option value="2" <?php //if ($apptype==2) echo "selected" ?>><?php //echo $pat_appointment_followup ?></option>
        </select>-->
    </td>
    <td><input type="submit" class="btn" name="button" id="button" value="Schedule" /></td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
<!--    <script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() }
      });
      cal.manageFields("f_date", "f_date", "%Y-%m-%d");
    //]]></script>-->
<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$('#f_date').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			minDate: +1
		});
	});
</script>