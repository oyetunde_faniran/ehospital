<script   type="text/javascript" src="myajax4.js"></script>


<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>

<?php  
//constructors

$patinp= new patinp_account();
$hospital= new patient_admission();
$inp = new inpatient_admission();
$invoice = new invoice();
$wards= new wards();
if(!empty($_POST['hosp_id'])){
$hosp_id=$_POST['hosp_id'];

$med_id=$hospital->getCurrentmedical_trans_id($hosp_id);
$deposit_purpose_id=$hospital->getDeposit_purpose($med_id);
$clinic_id=$hospital->getClinic_id2($hosp_id,$lang_curDB,$lang_curTB);
$transno=$hospital->get_tran_id();

if(isset($_POST['saveInvoice'])){
$subcount=$_POST['subcount'];
	for ($no = 1; $no <= $subcount; $no++) {
		
			
			$thepatitem_paymentsource[$no] =$_POST["patitem_paymentsource$no"]; 
			 // Begin
					 //NHIS PATIENT ; this variable $sumoftheamount1 STORES actual price minus amount paid by the NHIS
					 	// patitem_paymentsource equal 1 for patient under NHIS
			 if($_POST["patitem_paymentsource$no"]=='1')
				  	$theitemamounts1[$no] =$_POST["itemamount$no"]* 0.1; 
			else
					$theitemamounts[$no] =$_POST["itemamount$no"]; 
		   	//ends
			$theitemids[$no] =$_POST["itemid$no"];
		    $theitemnames[$no] =$_POST["itemname$no"]; 
		
}


$inserttrue=$invoice->insertInvoice($_POST,$thepatitem_paymentsource,$subcount,$theitemamounts1,$theitemamounts,$theitemids,$theitemnames,$lang_curDB,$lang_curTB);
   if(!empty($inserttrue[2])) $pattotal_id=$inserttrue[2];
	if($inserttrue[1]){
		header ("Location: {$_SERVER['PHP_SELF']}?p=viewinvoice&c=Success&hosp_id=$hosp_id&pattotal_id=$pattotal_id");
	}else{
	$c="Insertion failed";
	//echo $c ;
	}
}
}

$Dbconnect = new DBConf();
$sql_nhis="SELECT rn.nhisplan_id FROM  registry r 
	INNER JOIN registry_nhis rn ON  r.reg_id=rn.reg_id 
	WHERE  r.reg_hospital_no='$hosp_id'
	";
		$result_nhis= $Dbconnect->execute($sql_nhis);
if ($Dbconnect->hasRows($result_nhis, 1)){

	$row_plan_id= mysql_fetch_array ($result_nhis);
	$nhisplan_id=$row_plan_id['nhisplan_id'];
	
}	
?>
<script language="javascript" type="text/javascript">
function display(id1,id2,id3){  //show only 1st
		document.getElementById(id1).style.display = 'block';
		document.getElementById(id3).st		document.getElementById(id2).style.display = 'block';
yle.display = 'none';		
}
</script>
<form action="./index.php?p=invoice" method="post" name="form"  id="form"> 
<input type="hidden"  id="clinic_id" name="clinic_id" value="<?php if(!empty($med_id))echo $clinic_id ?>" />
<input type="hidden"  id="deptid" name="deptid" value="<?php if(!empty($med_id))echo $deptid ?>" />
<input type="hidden"  id="hosp_id" name="hosp_id" value="<?php if(!empty($med_id))echo $hosp_id ?>" />
<input type="hidden"  id="user_id" name="user_id" value="<?php if(!empty($med_id))echo $a2 ?>" />
<input type="hidden"  id="transno" name="transno" value="<?php if(!empty($med_id))echo $transno ?>" />
<input type="hidden"  id="patadm_id" name="patadm_id" value="<?php if(!empty($med_id))echo $med_id ?>" />
<input type="hidden"  id="paystatus" name="paystatus" value="0" />
<input type="hidden"  id="patservice_type" name="patservice_type" value="1" />
<input type="hidden"  id="patservice_type" name="pattotal_invoice_type" value="1" />
<input type="hidden"  id="pattotal_paymthd" name="pattotal_paymthd" value="" />
<table width="700" border="0" cellspacing="4" cellpadding="5">
  <tr>
    <td><strong><?php echo $lang_invoice_no ?></strong>:<br /><?php if(!empty($med_id)) echo $transno ?></td> 
	<td><strong>Date:</strong><br /><input type="hidden"  id="pattotal_date" name="pattotal_date" value="<?php echo date("d-m-Y"); ?>" ></input><?php echo date("d-m-Y"); ?></td>
  </tr>
  <tr>
      <td><strong><?php echo $lang_inpatname ?>:</strong><br /><?php if(!empty($med_id)) echo $hospital->getPatient_name($med_id); ?></td>
	   <td><strong><?php echo $lang_inpatclinic ?>:</strong><br /><?php echo $hospital->getClinic_name2($hosp_id,$lang_curDB,$lang_curTB); ?></td>
  </tr>
  <tr>
      <td><strong><?php echo $lang_inpataddr ?>:</strong><br /><?php echo $hospital->getPatient_address($med_id,$lang_curDB,$lang_curTB); ?></td> <td><strong>Invoice Type:</strong><?php $lang_admcharges_invoicetype?>:<br /><?php 
	    if($_POST['invoicetype']==1)
			$type="Admission Advice";
		 if($_POST['invoicetype']==2)
			$type="Service Advice";
		if($_POST['invoicetype']==3)
			$type="Drugs Advice";
			echo $type;
	  ?></td>
  </tr>
  <tr>
      <td colspan="2"><table width="85%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td class="bold"><?php echo $lang_sno ?></td>
    <td class="bold"><?php echo $lang_itemdesc ?></td>
    
    <td class="bold"><?php echo $lang_price ?></td>
    <td class="bold"><?php echo $lang_Amount ?></td>
	<?php if (!empty($nhisplan_id)){?>
	<td class="bold">Payable By Patient</td>
	<td class="bold">NHIS Status</td>
	
		<?php }?>
   <?php /*?> <td class="bold"><?php echo $lang_discounttype ?></td>
    <td class="bold"><?php echo $lang_discountamt ?></td><?php */?>
  </tr>
  <?php  if (!empty($nhisplan_id)){
			  $nhis_status="NHIS Covered";
			  $patitem_paymentsource='1';
		  }
		  else{
			  $patitem_paymentsource='3';
			  $nhis_status="";	
		  }
		  
?>
 
    <? $hospital->getAdmission_Charges4invoice2($hosp_id,$deposit_purpose_id,$deptid,$patitem_paymentsource,$nhis_status,$lang_curDB,$lang_curTB); 
																									?>
    
   
  
 <!-- <tr>
    <td colspan="5">Discount</td>
    
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5">Net Total</td>
    <td>&nbsp;</td>
  </tr>-->
   <tr>
    <td colspan="4" align="right"><input name="saveInvoice" type="submit" value="Save Invoice"></td>
    
  </tr>
  <tr>
    <td colspan="5">Raised By: <?php echo $_SESSION[session_id() . "staffName"]; ?></td>
    
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
    
  </tr>
 
</table>
</td>
  </tr>
</table>	</form>

<!--<SCRIPT type="text/javascript" src="./includes/js/jquery-1.3.2.min.js"></SCRIPT>
        <script type="text/javascript">
            $(function(){
                $('#editbtn').click(function(){
                    var da = $('#ward_id').val();
					var da2 = $('#p').val();
					var da3 = $('#clinic_id').val();
					var da4 = $('#ward_name').val();
					var da5 = $('#ward_bedspaces').val();
					var da6 = $('#ward_siderooms').val();
                    $('#mytable').load('ajax.php',{dateval:da,dateval2:da2,dateval3:da3,dateval4:da4,dateval5:da5,dateval6:da6});});
            });

 </script>
-->
<?php ob_end_flush();?>