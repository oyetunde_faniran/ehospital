<script   type="text/javascript" src="myajax4.js"></script>
<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>
<script language="JavaScript" type="text/JavaScript">
<!--
/**
 * Displays an confirmation box before to submit a "DROP DATABASE" query.
 * This function is called while clicking links
 *
 * @param   object   the link
 * @param   object   the sql query to submit
 *
 * @return  boolean  whether to run the query or not
 */
function confirmLinkDropDB(theLink, theSqlQuery)
{
    // Confirmation is not required in the configuration file
    // or browser is Opera (crappy js implementation)
    if (confirmMsg == '' || typeof(window.opera) != 'undefined') {
        return true;
    }

    var is_confirmed = confirm(confirmMsgDropDB + '\n' + confirmMsg + ' :\n' + theSqlQuery);
    if (is_confirmed) {
        theLink.href += '&is_js_confirmed=1';
    }

    return is_confirmed;
} // end of the 'confirmLink()' function

/**
 * Displays an confirmation box beforme to submit a "DROP/DELETE/ALTER" query.
 * This function is called while clicking links
 *
 * @param   object   the link
 * @param   object   the sql query to submit
 *
 * @return  boolean  whether to run the query or not
 */
function confirmLink(theLink, theSqlQuery)
{
    // Confirmation is not required in the configuration file
    // or browser is Opera (crappy js implementation)
    if (confirmMsg == '' || typeof(window.opera) != 'undefined') {
        return true;
    }

    var is_confirmed = confirm(confirmMsg + theSqlQuery);
    if (is_confirmed) {
        theLink.href += '&is_js_confirmed=1';
    }

    return is_confirmed;
} // end of the 'confirmLink()' function

    // js form validation stuff
    var confirmMsg  = 'Do you really want to ';

//-->


</script>

<?php  
//constructors
$patinp= new patinp_account();
$hospital= new patient_admission();
$inp = new inpatient_admission();
$invoice = new invoice();
$wards= new wards();
if(!empty($_POST['hosp_id'])){
    $hosp_id=$_POST['hosp_id'];

    $med_id=$hospital->getCurrentmedical_trans_id($hosp_id);
    $deposit_purpose_id=$hospital->getDeposit_purpose($med_id);
    $clinic_id=$hospital->getClinic_id2($hosp_id,$lang_curDB,$lang_curTB);
    $transno=$hospital->get_tran_id();

     //Save invoice and redirect to view invoice interface
    if(isset($_POST['saveInvoice']) ){


        if (is_array($_POST["check"])) {
           // echo "enter";
            $allcheck = array_values ($_POST["check"]);
                foreach ($allcheck as $value) {
                    $check=$value;
                    $no=$check;
                    $theitemamounts[$no] =$_POST["itemamount$no"];

                    $cnote_id=$_POST["cnote_id"];
                   //
                    $theitemids[$no] =$_POST["itemid$no"];
                    $theitemnames[$no] =$_POST["itemname$no"];
                    $thepatitem_paymentsource[$no] = $_POST["patitem_paymentsource$no"];
					 // Begin
					 	//NHIS PATIENT ; this variable $sumoftheamount1 STORES actual price minus amount paid by the NHIS
					 		// patitem_paymentsource equal 1 for patient under NHIS
                         if($_POST["patitem_paymentsource$no"]=='1'){
						 
                                $sumoftheamount1=$sumoftheamount1+($theitemamounts[$no]*.1);
                                $theserv_price1[$no] =$theitemamounts[$no]*.1;
                        }else{
                                $sumoftheamount1=$sumoftheamount1+($theitemamounts[$no]);
                        }
						//ends
                        $sumoftheamount=$sumoftheamount+$theitemamounts[$no]; // this variable $sumoftheamount stores actual amounts
                }
        }
            //exit();
   //         print_r ($_POST);
    //exit();
 //die("$cnote_id");
    $inserttrue=$invoice->insertInvoice4service($_POST, $cnote_id,$theitemamounts,$thepatitem_paymentsource,$theitemids,$theitemnames,$theserv_price1,$sumoftheamount1,$sumoftheamount,$lang_curDB,$lang_curTB);
	
	
       if(!empty($inserttrue[2])) $pattotal_id=$inserttrue[2];
        if($inserttrue[1]){
           
            header ("Location: {$_SERVER['PHP_SELF']}?p=viewinvoice4service&c=Success&hosp_id=$hosp_id&pattotal_id=$pattotal_id");
        }else{
        $c="Insertion failed";
        //echo $c ;
        }
    }
}
?>


<?php 
  //NHIS SCHEME Confirming patient involvement
$sql_nhis="SELECT rn.nhisplan_id FROM  registry r 
	INNER JOIN registry_nhis rn ON  r.reg_id=rn.reg_id 
	WHERE  r.reg_hospital_no='$hosp_id'
	";

$sql_retainer="SELECT nhisplan_id FROM  registry r 
	INNER JOIN registry_retainership rr ON  r.reg_id=rr.reg_id 
	WHERE  r.reg_hospital_no='$hosp_id'
	";
//echo $sql_nhis;
//exit();
$Dbcn = new DBConf();
$result_nhis=$Dbcn->execute($sql_nhis);


$result_retainer=$Dbcn->execute($sql_retainer);

if (isset($_POST["select30"])){
 $_POST["select30"]=array_unique($_POST["select30"]); 
$productID_comma_separated=implode(",",$_POST["select30"]);

if ($Dbcn->hasRows($result_retainer)){
	$sql_scheme="SELECT *  FROM  nhis_plan np INNER JOIN  nhisplan_services ns 
						ON np.nhisplan_id=ns.nhisplanser_plan_id
					WHERE  np.nhisplan_id='$nhisplan_id' AND ns.nhisplanserv_servdrug_id
					IN ($productID_comma_separated)
					";
}

if ($result_nhis){
	$row_plan_id= mysql_fetch_array ($result_nhis);
	$nhisplan_id=$row_plan_id['nhisplan_id'];
		$sql_scheme="SELECT ns.nhisplanserv_servdrug_id  FROM  nhis_plan np INNER JOIN  nhis_plan_services ns 
						ON np.nhisplan_id=ns.nhisplanserv_plan_id
					WHERE  np.nhisplan_id='$nhisplan_id' AND ns.nhisplanserv_servdrug_id
					IN ($productID_comma_separated)
					";
			//die("$sql_scheme");
		$result_scheme=$Dbcn->execute($sql_scheme);
		$sn2=1;
		while($row_scheme = mysql_fetch_array ($result_scheme)){
			$rows_nhis_serv_id[$sn2] =$row_scheme['nhisplanserv_servdrug_id'] ;
	   	$sn2++;	
	   }
	   //print_r($rows_nhis_drug_id);
	   //exit();
	   
}

}	
?>


<script language="javascript" type="text/javascript">
function display(id1,id2,id3){  //show only 1st
		document.getElementById(id1).style.display = 'block';
		document.getElementById(id3).style.display = 'none';	
		document.getElementById(id2).style.display = 'block';	
}
</script>
<form action="./index.php" method="post" name="form"  id="form"> 
<input type="hidden"  id="clinic_id" name="clinic_id" value="<?php if(!empty($med_id))echo $clinic_id ?>" />
<input type="hidden"  id="deptid" name="deptid" value="<?php if(!empty($med_id))echo $deptid ?>" />
<input type="hidden"  id="hosp_id" name="hosp_id" value="<?php if(!empty($med_id))echo $hosp_id ?>" />
<input type="hidden"  id="user_id" name="user_id" value="<?php if(!empty($med_id))echo $a2 ?>" />
<input type="hidden"  id="transno" name="transno" value="<?php if(!empty($med_id))echo $transno ?>" />
<input type="hidden"  id="patadm_id" name="patadm_id" value="<?php if(!empty($med_id))echo $med_id ?>" />
<input type="hidden"  id="paystatus" name="paystatus" value="0" />
<input type="hidden"  id="cnote_id" name="cnote_id" value="<?php if(!empty($med_id))echo implode(",",$_POST["cnote_id"]); ?>" />
<input type="hidden"  id="p" name="p" value="invoice4service" />
<input type="hidden"  id="patservice_type" name="patservice_type" value="1" />
<input type="hidden"  id="pattotal_invoice_type" name="pattotal_invoice_type" value="4" />
<input type="hidden"  id="pattotal_paymthd" name="pattotal_paymthd" value="" />
<table width="700" border="0" cellspacing="4" cellpadding="5">
  <tr>
    <td><strong><?php echo $lang_invoice_no ?></strong>:<br /><?php if(!empty($med_id)) echo $transno ?></td> 
	<td><strong>Date:</strong><br /><input type="hidden"  id="pattotal_date" name="pattotal_date" value="<?php echo date("d-m-Y"); ?>" ></input><?php echo  date("M j, Y");  ?></td>
  </tr>
  <tr>
      <td><strong><?php echo $lang_inpat ?>:</strong><br /><?php //echo $lang_inpatname ?><?php if(!empty($med_id)) echo $hospital->getPatient_name($med_id); ?><br /><?php //echo $lang_inpataddr ?><?php if(!empty($med_id)) echo $hospital->getPatient_address($med_id,$lang_curDB,$lang_curTB); ?></td>
	   <td valign="top"><strong><?php echo $lang_inpatclinic ?>:</strong><br /><?php if(!empty($med_id))echo $hospital->getClinic_name2($hosp_id,$lang_curDB,$lang_curTB); ?><br /><br /><strong>Invoice Type:</strong><?php $lang_admcharges_invoicetype?>:<br /><?php 
	   
	    if(!empty($_POST['invoicetype']) && $_POST['invoicetype']==1)
			$type="Admission Advice";
		 if(!empty($_POST['invoicetype']) && $_POST['invoicetype']==2)
			$type="Drug Advice";

		if(!empty($_POST['invoicetype']) && $_POST['invoicetype']==3)
			$type="Registration";
		if(!empty($_POST['invoicetype']) && $_POST['invoicetype']==4)
			$type="Laboratory Service";
			if(isset($type))
			echo $type;
	  ?></td>
  </tr>
  <tr>
      <td><?php //echo $lang_inpataddr ?><?php //if(!empty($med_id)) echo $hospital->getPatient_address($med_id,$lang_curDB,$lang_curTB); ?></td> <td valign="top"></td>
  </tr>  <tr>
      <td colspan="2"><table width="70%" border="0" cellspacing="4" cellpadding="5">
  <tr>
    <td class="bold" align="center"><?php echo $lang_sno ?></td>
    <td class="bold"><?php // echo $lang_tick ?></td>
    <td class="bold"><?php echo $lang_itemdesc ?></td>
    
    <td class="bold"><?php echo $lang_price ?>(&#8358;)</td>
    <td class="bold" align="center"><?php echo $lang_Amount ?>(&#8358;)</td>
    <?php
	$show_nhis_label = "no";
    if (!empty($nhisplan_id))
    {   $show_nhis_label="yes";      ?>


    <td class="bold">NHIS Status</td>
	<?php }?>
   <?php /*?> <td class="bold"><?php echo $lang_discounttype ?></td>
    <td class="bold"><?php echo $lang_discountamt ?></td><?php */?>
  </tr>
     
    <?
	if ( isset($_POST["select30"]) && is_array($_POST["select30"])){
	$_POST["select30"]=array_unique($_POST["select30"]);  
	//$servId_array=implode(",",$rows_nhis_serv_id);
    $servId_array = !empty($rows_nhis_serv_id) ? $rows_nhis_serv_id : array();
    //print_r($servId_array);
   // die();
	 $hospital->getService_list4invoice1($_POST,$servId_array,$show_nhis_label,$hosp_id,$deptid,$lang_curDB,$lang_curTB);
																							
	}																						
	?>
    
   
  
 <!-- <tr>
    <td colspan="5">Discount</td>
    
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5">Net Total</td>
    <td>&nbsp;</td>
  </tr>-->
   <tr>
    <td colspan="6" align="right"><input name="saveInvoice" type="submit" value="Save Invoice" onClick="return confirmLink(this, 'invoice these items? \n\n N.B: You may not be allowed to edit the invoice again.')"></td>
    
  </tr>
  <tr>
    <td colspan="6">Raised By: <?php echo $a3 ?></td>
    
  </tr>
  <tr>
    <td colspan="6">&nbsp;</td>
    
  </tr>
 
</table>
</td>
  </tr>
</table>	</form>

<!--<SCRIPT type="text/javascript" src="./includes/js/jquery-1.3.2.min.js"></SCRIPT>
        <script type="text/javascript">
            $(function(){
                $('#editbtn').click(function(){
                    var da = $('#ward_id').val();
					var da2 = $('#p').val();
					var da3 = $('#clinic_id').val();
					var da4 = $('#ward_name').val();
					var da5 = $('#ward_bedspaces').val();
					var da6 = $('#ward_siderooms').val();
                    $('#mytable').load('ajax.php',{dateval:da,dateval2:da2,dateval3:da3,dateval4:da4,dateval5:da5,dateval6:da6});});
            });

 </script>
-->
