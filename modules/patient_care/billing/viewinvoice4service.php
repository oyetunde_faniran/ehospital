
<script   type="text/javascript" src="myajax4.js"></script>
<?php
# Script 2.5 - main.inc.php

/*
 * 	This is the main content module.
 * 	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

    // Need the BASE_URL, defined in the config file:
    require_once ('./includes/config.inc.php');

    // Redirect to the index page:
    $url = BASE_URL . 'index.php';
    header("Location: $url");
    exit;
} // End of defined() IF.
?>

<?php
//constructors

$patinp = new patinp_account();
$hospital = new patient_admission();
$inp = new inpatient_admission();
$invoice = new invoice();
$invoice2 = new invoice();
$invoice3 = new invoice();
$transno = $hospital->get_tran_id();
$wards = new wards();

//$Numword= new Numword();

$servicename = new service();

if (isset($_POST['savereceipt'])) {
    $updateInvoice = $invoice2->updatePattotal4receipt($_POST);

    if (!empty($updateInvoice[2])) {
        $pattotal_id = $updateInvoice[2];
        $hosp_id = $updateInvoice[3];
    }
    if ($updateInvoice[1]) {
        header("Location: {$_SERVER['PHP_SELF']}?p=viewreceipt&c=Success&hosp_id=$hosp_id&pattotal_id=$pattotal_id");
    } else {
        $c = "Insertion failed";
        echo $c;
    }
}

if (isset($_GET['pattotal_id']))
    $pattotal_id = $_GET['pattotal_id'];
if (isset($_GET['checkinvoice']))
    $checkinvoice = $_GET['checkinvoice'];
$invoice = $invoice->getTotalpay($pattotal_id);

$Dbconnect = new DBConf();

$sql = "Select * from   pat_serviceitem pservice inner join
 						 pat_transitem pitem inner join 
                         pat_transtotal ptotal
						 ON 
						 pitem.patservice_id=pservice.patservice_id  AND
						 pitem.pattotal_id=ptotal.pattotal_id 
		   where pitem.pattotal_id='$pattotal_id' AND ptotal.pattotal_invoice_type=4";
$res20 = $Dbconnect->execute($sql);


$sql_nhis = "SELECT rn.nhisplan_id FROM  registry r
	INNER JOIN registry_nhis rn ON  r.reg_id=rn.reg_id
	WHERE  r.reg_hospital_no='$hosp_id'
	";
$result_nhis = $Dbconnect->execute($sql_nhis);
if ($Dbconnect->hasRows($result_nhis, 1)) {

    $row_plan_id = mysql_fetch_array($result_nhis);
    $nhisplan_id = $row_plan_id['nhisplan_id'];
}
$hosp_id = $_GET['hosp_id'];

$med_id = $hospital->getCurrentmedical_trans_id($hosp_id);
//$deposit_purpose_id=$hospital->getDeposit_purpose($med_id);
$clinic_id = $hospital->getClinic_id2($hosp_id, $lang_curDB, $lang_curTB);
//$transno=$hospital->get_tran_id();
?>


<script language="javascript" type="text/javascript">
    function display(id1,id2,id3){  //show only 1st
        document.getElementById(id1).style.display = 'block';
        document.getElementById(id3).style.display = 'none';
        document.getElementById(id2).style.display = 'block';
    }

</script>



<!--<form id="form" name="form" method="post" action="http://www.branchcollect.com/branchcollect_test/payment/payment.php">-->
<form id="form" name="form" method="post" action="">
    <table width="750" border="0" cellspacing="0" cellpadding="0" class="tableborder">
        <tr>
            <td><strong><?php echo $lang_invoice_no ?></strong>:<br /><?php if (!empty($med_id))
    echo $invoice['pattotal_transno'] ?></td>
                <td><strong>Date:</strong><br /><?php if (!empty($med_id)) //echo  date("M j, Y", mktime(0, 0, 0, $invoice['TRANSACTION MONTH'], $invoice['TRANSACTION DAY'],$invoice['TRANSACTION YEAR']))
        echo $invoice['TRANSACTION DATE']; ?> </td>
            </tr>
            <tr>
                <td><strong><?php echo $lang_inpat ?>:</strong><br /><?php //echo $lang_inpatname  ?><?php if (!empty($med_id))
        echo $hospital->getPatient_name($med_id); ?><br /><?php //echo $lang_inpataddr  ?><?php if (!empty($med_id))
        echo $hospital->getPatient_address($med_id, $lang_curDB, $lang_curTB); ?></td>
            <td valign="top"><strong><?php echo $lang_inpatclinic ?>:</strong><br /><?php if (!empty($med_id)
                    )echo $hospital->getClinic_name2($hosp_id, $lang_curDB, $lang_curTB); ?><br /><br /><strong>Invoice Type:</strong><?php $lang_admcharges_invoicetype ?>:<br /><?php
                if ($invoice['pattotal_invoice_type'] == 1)
                    $type = "Admission Advice";
                if ($invoice['pattotal_invoice_type'] == 2)
                    $type = "Drug Prescriptions";
                if ($invoice['pattotal_invoice_type'] == 3)
                    $type = "Registration";
                if ($invoice['pattotal_invoice_type'] == 4)
                    $type = "Lab Test";
                if (isset($type))
                    echo $type;
?></td>
        </tr>
        <tr>
            <td><?php //echo $lang_inpataddr  ?><?php //if(!empty($med_id)) echo $hospital->getPatient_address($med_id,$lang_curDB,$lang_curTB);  ?></td> <td valign="top"></td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" class="border">
                    <tr class="border">
                        <td class="bold"><?php echo $lang_sno ?></td>
                        <td class="bold"><?php echo $lang_itemdesc ?></td>

                        <td class="bold" style="text-align: right;"><?php echo $lang_price ?>(&#8358;)</td>
                        <td class="bold" style="text-align: right;"><?php echo $lang_Amount ?>(&#8358;)</td>
<?php if (!empty($nhisplan_id)) { ?>
                            <td class="bold">Payable by NHIS<?php //echo $lang_Amount  ?>(&#8358;)</td>
                            <td class="bold">NHIS Status</td>
                            <td class="bold">Payable by patient  <?php //echo $lang_Amount ?>(&#8358;)</td>

<?php } ?>
                    </tr>

                    <?php
                        $cnt2 = 1;
                        $discount = $thisamount = 0;
                        while ($rowservicename = mysql_fetch_array($res20)) {
                            $amount = $rowservicename['patitem_amount'];
                            $amount_nhis = $rowservicename['patitem_amount_nhis'];
                            $serice_item = $rowservicename['patservice_name'];
                    ?>
                            <tr>
                                <td ><?php echo $cnt2 ?></td>
                                <td ><?php echo $rowservicename['patservice_name'] ?></td>
                                <td class="value" ><?php echo number_format($rowservicename['patitem_amount'], 2) ?></td>
                            <td  class="value"><?php echo number_format($rowservicename['patitem_amount'], 2) ?></td>
                        <?php if (!empty($nhisplan_id)) {
 ?>

<?php if ($rowservicename['patitem_paymentsource'] == '1') { ?>
                                    <td class="value"><?php echo number_format($amount - $amount_nhis, 2) ?></td>
                        <? } else {
 ?>
                                    <td class="value">-</td>
<? } ?>
<?php if ($rowservicename['patitem_paymentsource'] == '1') { // NHIS COVERED ITEMS ?>

                                    <td class="value">Covered</td>
                                    <td class="value"><?php echo number_format($amount_nhis, 2) ?>
                                        <input name="btrans_item[<? echo $cnt2 ?>]" type="hidden" id="btrans_item<? echo $cnt2 ?>" value="<? echo $serice_item ?>">
                                        <input name="btrans_amount[<? echo $cnt2 ?>]" type="hidden" id="btrans_amount<? echo $cnt2 ?>" value="<? echo $amount_nhis ?>">

                                    </td>

<?php } else { // NORMAL PAYMENT
                                    // btrans_item identifiers REPRESENTS items goining to branchcollect  ?>

                                    <td class="value">-<?php // echo number_format($amount,2)  ?></td>
                                    <td class="value"><?php echo number_format($amount, 2) ?>
                                        <input name="btrans_item[<? echo $cnt2 ?>]" type="hidden" id="btrans_item<? echo $cnt2 ?>" value="<? echo $serice_item ?>">
                                        <input name="btrans_amount[<? echo $cnt2 ?>]" type="hidden" id="btrans_amount<? echo $cnt2 ?>" value="<? echo $amount ?>">
                                    </td>

<?php } ?>

<?php } else { ?>


                            <input name="btrans_item[<? echo $cnt2 ?>]" type="hidden" id="btrans_item<? echo $cnt2 ?>" value="<? echo $serice_item ?>">

                                <input name="btrans_amount[<? echo $cnt2 ?>]" type="hidden" id="btrans_amount<? echo $cnt2 ?>" value="<? echo $amount ?>">
                    <?php } ?>

                    <?php
                            $discount = 0;
                            $discount = $discount + $rowservicename['patitem_discount'];
                            if ($rowservicename['patitem_paymentsource'] == '1')
                                $thisamount = $thisamount + $amount_nhis;
                            else
                                $thisamount=$thisamount + $amount;
                            $cnt2 = $cnt2 + 1;
                    ?>
                            </tr>
                    <?
                            $myamount = number_format($thisamount);
                            $total_amount = $myamount;
                            $mytotal_amount = $total_amount;
                            $mytotal_amountkobo = $total_amount * 100;
                        }
                    ?>

                        <tr class="border1">
                            <td colspan="3">Gross Amount</td>

                            <td class="value">&#8358;<?php echo number_format($thisamount, 2) ?></td>
                        </tr>
                        <tr>
                            <td colspan="3">Less: Discount</td>

                            <td class="value">&#8358;<?php echo '(' . number_format($discount, 2) . ')'; ?></td>
                        </tr>
                        <tr>
                            <td colspan="3">Net Total</td>
                            <td class="value">&#8358;<?php echo number_format($thisamount - $discount, 2); ?></td>
                        </tr>
                        <tr>
                            <td  colspan="5"><br /><?php // echo /*(int)$invoice['pattotal_totalamt'];*/ucfirst(Numword::currency(1500)) ;  ?></td>
                        </tr>

                    <?php //header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=Success");?>
                    <?php /* ?><tr>
                          <td colspan="2" align="right"> </td ><td><form action="./viewinvoice1.php?<?php echo $_SERVER['QUERY_STRING']?>" method="post" name="form"  id="form">
                          <input name="printpreview" type="button" value="" disabled="disabled"></td><td><a href="./index.php?p=viewinvoice4service&pattotal_id=<?php echo $pattotal_id ?>&hosp_id=<?php echo $hosp_id ?>" title="Generate Receipt" style="background-color:#FFFFCC; border:double" > Process Payment</a></form></td>

                          </tr><?php */ ?>

                    </table>
                </td>
            </tr>
            <tr>
                <td ><!--Raised By: <?php echo $a3 ?>-->&nbsp;</td>

            <?php
                        if (isset($_GET['checkinvoice']))
                            if ($checkinvoice == 'show') {
                                //echo "enter here";
            ?>

                                <td ><?php if ($invoice['pattotal_status'] == 0) { ?>

                                        <input name="btrans_no" type="hidden" id="btrans_no" value="<?php if (!empty($pattotal_id)
                                        )echo $invoice3->getTrans_no($pattotal_id); ?>">

                                    <!--<input name="btrans_semester" type="hidden" id="btrans_semester" value="<? echo $btrans_semester ?>">

                                    <input name="btrans_session" type="hidden" id="btrans_session" value="<? echo $trans_year ?>">
                                        -->
                                        <input name="std_idno" type="hidden" id="std_idno" value="<?php if (!empty($hosp_id)
                                        )echo $med_id ?>">

                                            <input name="std_matric" type="hidden" id="std_matric" value="<?php if (!empty($hosp_id)
                                            )echo $hosp_id ?>">

                                                <input name="std_surname" type="hidden" id="std_surname" value="<?php if (!empty($hosp_id)) echo $hospital->getPatient_surname($hosp_id); ?>">

                                                <input name="customer_no" type="hidden" id="customer_no" value="<?php if (!empty($hosp_id)) echo $hosp_id; ?>">

                                                <input name="std_firstname" type="hidden" id="std_firstname" value="<?php if (!empty($hosp_id)) echo $hospital->getPatient_othernames($hosp_id); ?>">

                                                <input name="std_othernames" type="hidden" id="std_othernames" value="<? //echo $othernames  ?>">

                                                <input name="std_programme" type="hidden" id="std_programme" value="<? echo $pr ?>">

                                                <input name="std_faculty" type="hidden" id="std_faculty" value="<? echo $fa ?>">

                                                <input name="std_department" type="hidden" id="std_department" value="<? echo $de ?>">

                                                <input name="std_degree" type="hidden" id="std_degree" value="">

                                                <input name="std_level" type="hidden" id="std_level" value="<? //echo GetCurrentSessionLevel($std_id)  ?>">

                                                <input name="std_email" type="hidden" id="std_email" value="<? echo $student_email ?>">

                                                <input name="std_gsm" type="hidden" id="std_gsm" value="<? echo $student_mobiletel ?>">

                                                <input name="redirect_url" type="hidden" id="redirect_url" value="<?php echo "http://" . $_SERVER['HTTP_HOST'] . "/ehospital/transact_bankpayment.php"; ?>">

                                                <input name="update_url" type="hidden" id="update_url" value="<?php echo "http://" . $_SERVER['HTTP_HOST'] . "/ehospital/BranchCollectUpdate_URL.php"; ?>">

                                                <input name="client_id" type="hidden" id="client_id" value="29">
                                                <input name="client_code" type="hidden" id="client_code" value="LUTH">
                                                <input name="bank_id" type="hidden" id="bank_id" value="3">
                                                <input name="acquiring_bankid[1]" type="hidden" id="acquiring_bankid1" value="3">
                                                <input name="client_account_name[1]" type="hidden" id="client_account_name1" value="LUTH">
                                                <input name="client_account_no[1]" type="hidden" id="client_account_no1" value="MEDICAL FEES">
                                                <input name="client_account_amount[1]" type="hidden" id="client_account_amount1" value="<? echo ($thisamount - ($thisamount * 0.02)); ?>">
                                                <input name="client_account_name[2]" type="hidden" id="client_account_name2" value="Upperlink">
                                                <input name="client_account_no[2]" type="hidden" id="client_account_no2" value="TECHNOLOGY FEES">
                                                <input name="client_account_amount[2]" type="hidden" id="client_account_amount2" value="<?php echo $thisamount * 0.01; ?>">
                                                <input name="client_account_name[3]" type="hidden" id="client_account_name3" value="Bank">
                                                <input name="client_account_no[3]" type="hidden" id="client_account_no3" value="PARTICIPATING BANK">
                                                <input name="client_account_amount[3]" type="hidden" id="client_account_amount3" value="<?php echo $thisamount * 0.01; ?>">



                                                <input type="hidden"  id="pattotal_id" name="pattotal_id" value="<?php if (!empty($pattotal_id)
                                                )echo $pattotal_id ?>" />
                                                <input type="hidden"  id="pattotal_paymthd" name="pattotal_paymthd" value="Branch Collect" />
                                                <input type="hidden"  id="transno" name="transno" value="<?php if (!empty($hosp_id)
                                                    )echo $transno ?>" />
                                                        <input type="hidden"  id="hosp_id" name="hosp_id" value="<?php if (!empty($hosp_id)
                                                        )echo $hosp_id ?>" />
                                                            <input type="hidden"  id="checkinvoice" name="checkinvoice" value="show" />
                                                            <!--<input name="savereceipt" type="submit" value="Process Payment via Branch Collect" id="btn" class="btn" onClick="return confirmLink(this, 'process this invoice and generate receipt? you may not be allowed to reverse the receipt again.')">-->
													<?php
														include_once ("trans-payment-processor-snippet.php");
	 												} else {
                                                        echo "Payment Status: Paid";
                                                    } ?>
                                                    </td>

<?php
                                                    //$checkinvoice="" ;
                                                } else {
?>
                                                    <td >&nbsp;</td><?php } ?>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>

        </tr>

    </table>
</form>
<!--<SCRIPT type="text/javascript" src="./includes/js/jquery-1.3.2.min.js"></SCRIPT>
        <script type="text/javascript">
            $(function(){
                $('#editbtn').click(function(){
                    var da = $('#ward_id').val();
					var da2 = $('#p').val();
					var da3 = $('#clinic_id').val();
					var da4 = $('#ward_name').val();
					var da5 = $('#ward_bedspaces').val();
					var da6 = $('#ward_siderooms').val();
                    $('#mytable').load('ajax.php',{dateval:da,dateval2:da2,dateval3:da3,dateval4:da4,dateval5:da5,dateval6:da6});});
            });

 </script>
-->
