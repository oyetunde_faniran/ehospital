<script   type="text/javascript" src="myajax3.js"></script>

<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	 
} // End of defined() IF.
?>

<?php 

$hospital= new patient_admission();

$inp = new inpatient_admission();
$refer= new patient_referrer();
$invoice= new invoice();
$wards= new wards();

if(isset($_GET['q'])){ 
$q= $_GET['q'];

$med_id=$hospital->getCurrentmedical_trans_id($q);
//echo "initial med ID ".$med_id;
}
?>

<script language="javascript" type="text/javascript">
function display(id1,id2,id3){  //show only 1st
		document.getElementById(id1).style.display = 'block';
		document.getElementById(id2).style.display = 'block';
		document.getElementById(id3).style.display = 'none';		
}

</script>


<BR />



	<!--<input type="hidden"  id="addWard" name="capMode" value="addWard" />-->
		
	<table border="0" bordercolor="#FFCCFF" width='100%'> 
		<tr><td align="center" width="500">
		   			 <?php
					
						if(!isset($_GET['fw2'])){
						?>
						<h4>
                        	Welcome!&nbsp;&nbsp; Please Supply the Patient's Hospital Number in the Search Box
<?php
	$paymentTracker = new Payment($_SESSION[session_id() . "userID"]);
	$amountCollected = $paymentTracker->getTotalDayPayment();
	$display = "<p>
					<strong>TOTAL AMOUNT COLLECTED SO FAR TODAY: <span style=\"color: #900; font-weight: bold;\">&#8358; $amountCollected</span></strong>
			    </p>";
	echo $display;
?>
                        </h4>
						<?
						} else {
						?>
                
						<table class="tblborder2"  border="1" width="70%" >
									<tr>
										<td colspan=3 class="bold">
										
										<?php 
										echo $hospital->getPatient_name($med_id);?>
									
										</td>
										
									</tr>
										<tr>
										<td colspan=2 class="bold">Consultant:
										 
										 </td>
										 <td><? echo $hospital->getConsultant_name2($q,$lang_curDB,$lang_curTB);?></td>
									
									  </tr>
									<tr>
										<td colspan=2 class="bold">Patient Type:
										
										 </td>
									   <td><? echo $hospital->getPatient_typename($q,$lang_curDB,$lang_curTB); ?></td>
									</tr>
									<tr>
										<td colspan=2 class="bold">Clinic: 
										 </td>
											<td><? echo $hospital->getClinic_name2($q,$lang_curDB,$lang_curTB); ?></td>
									</tr>
									<tr>
										<td colspan=3 class="bold">Registration/Consultation Charges</td>
									</tr>
									
										<tr>
										<td colspan=3 ><a href="./index.php?p=viewinvoices&hosp_id=<?php echo $q ?>&invoice_type=3" target="_blank">View Invoices</a></td>
									</tr>							
									<tr>
										<td colspan=3 class="bold">Admission Charges  </td>
									</tr>
									<?php 
									//Admission Charges starts
									 if($invoice->hasinvoiced($med_id)){ 
											?>
									<tr>
										<td colspan="3"><?  											
											
   											 ?><a href="./index.php?p=viewadm_charges&hospital_id=<?php echo $q ?>">View Admission Charges</a></td>
									</tr>
									<?php }?>
									<?php 
									
									 if(!$invoice->hasinvoiced($med_id)){
									?>
									<tr>
										<td colspan=3 ><a href="./index.php?p=viewinvoices&hosp_id=<?php echo $q ?>&invoice_type=1" target="_blank">View Admission Charges</a></td>
									</tr>
									<?php }
									//Admission ends
									 ?>
									
									<tr>
										<td colspan=3 class="bold">Laboratory Invoices</td>
									</tr>
																								
									<tr>
										<td colspan=3 ><a href="./index.php?p=viewinvoices&hosp_id=<?php echo $q ?>&invoice_type=4" target="_blank">View Invoices</a></td>
									</tr>
									<?php //} ?>
									
									<tr>
										<td colspan=3 class="bold">Products Invoices</td>
									</tr>
									
									<tr>
										<td colspan="3"><a href="./index.php?p=viewinvoices&hosp_id=<?php echo $q ?>&invoice_type=2" target="_blank">View Invoices</a></td>
									</tr>
        				</table> <?php }?>
				</td>
                <td valign="top" class="search-box-new" style="position: relative; top: -10px;">
                    <div>
                        <input type="radio" name="number_type" id="old-no" value="1" checked="checked" onclick="setNumberType(1);" />
                        <label for="old-no">New Hospital Number</label>
                        <input type="radio" name="number_type" id="new-no" value="2" onclick="setNumberType(2);" />
                        <label for="new-no">Old Hospital Number</label>
                    </div>
                    <div style="position: relative; margin-bottom: 20px;">
                        <input type="text" name="hosp" id="search-box" onkeyup="showMesg(this.value)" />
                    </div>
                    <div id="message"></div>
                </td>
	</tr>
</table>		


</body>
</html>