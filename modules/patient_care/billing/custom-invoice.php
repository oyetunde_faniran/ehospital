<?php
    //echo 'Custom Invoice goes here';
    $hospital_no = isset($_GET['h']) ? $_GET['h'] : '';
    $pat = new Patient();
    $pat_info = $pat->getFullPatientInfo($hospital_no);
    try {
        if (empty($pat_info)){
            throw new Exception('Patient details not found!');
        }
//        die ('<pre>' . print_r($pat_info, true));

        $display = '<h1 style="margin: 15px 0px;">PATIENT DETAILS</h1>
                    <table>
                        <tr>
                            <td><strong>PATIENT\'S SURNAME:</strong></td>
                            <td>' . stripslashes($pat_info['reg_surname']) . '</td>
                        </tr>
                        <tr>
                            <td><strong>PATIENT\'S OTHER NAMES:</strong></td>
                            <td>' . stripslashes($pat_info['reg_othernames']) . '</td>
                        </tr>
                        <tr>
                            <td><strong>PATIENT\'S HOSPITAL NUMBER:</strong></td>
                            <td>' . stripslashes($pat_info['reg_hospital_no']) . '</td>
                        </tr>
                        <tr>
                            <td><strong>GENDER:</strong></td>
                            <td>' . stripslashes($pat_info['sex']) . '</td>
                        </tr>
                    </table>
                    <h1 style="margin: 15px 0px;">INVOICE DETAILS</h1>';
        echo $display;
?>
<!--<form>
    <table>
        <tr>
            <td><strong>INVOICE TYPE:</strong></td>
            <td>
                <select>
                    <option value="0">--Select Invoice Type--</option>
                    <option value="1">Admission</option>
                    <option value="2">Drugs</option>
                    <option value="3">Laboratory Services</option>
                    <option value="4">Registration/Consultation</option>
                </select>
            </td>
        </tr>
    </table>
</form>-->
<form action="" method="post" enctype="multipart/form-data" onsubmit="confirm("Are you sure you want to raise a custom invoice for the entered items?");">
    <table>
        <tr>
            <td><strong>INVOICE TYPE:</strong></td>
            <td>
                <select name="invoice_type" id="invoice_type" required>
                    <option value="">--Select Invoice Type--</option>
                    <option value="1">Admission</option>
                    <option value="2">Products</option>
                    <option value="4">Laboratory Services</option>
                    <option value="3">Registration/Consultation</option>
                </select>
            </td>
        </tr>
    </table>
    <div id="docscontainer">
        <span class="rightsLink" onclick="addMoreDocuments();">Add a new Item</span>
        <div>&nbsp;</div>
        <div id="uploaddiv1" class="uploadDivs"></div>
    </div>
    <div id="button-container" style="padding-left: 10px; display: none;"><input type="submit" value="Save Invoice" class="btn" /></div>
</form>
<script type="text/javascript">
    nextDocument = 2;
    allDocuments = 0;
    function addMoreDocuments(){
        try {
            cont = document.getElementById("docscontainer");
            newDoc = "<div style=\"padding-bottom: 10px;\">";
            newDoc = newDoc + " <input type=\"text\" name=\"description[]\" id=\"docsname" + nextDocument + "\" size=\"40\" placeholder=\"Item Description\" required/>";
            newDoc = newDoc + " <input type=\"text\" name=\"amount[]\" id=\"docs" + nextDocument + "\"  size=\"20\" placeholder=\"Amount (&#8358;)\" required/>";
            newDoc = newDoc + " <input type=\"button\" value=\"Remove Item\" class=\"btn\" onclick=\"removeDocument(" + nextDocument + ");\" />";
            newDoc = newDoc + "</div>";

            newDiv = document.createElement("div");
            newDiv.id = "uploaddiv" + nextDocument;
            newDiv.className = "uploadDivs";
            newDiv.innerHTML = newDoc;
            cont.appendChild(newDiv);

            nextDocument++;
            allDocuments++;
            checkSubmitButton();
        } catch (e) {
            //alert(e);
            alert ("An error occurred while trying to add a new document field.");
        }
    }

    function removeDocument(whichDoc){
        c = "uploaddiv" + whichDoc;
        //p = "docscontainer";
        try {
            c = document.getElementById(c);
            //p = document.getElementById(p);
            c.parentNode.removeChild(c);
            allDocuments--;
            checkSubmitButton();
        } catch (e) {
            alert ("An error occurred while trying to add a new document field.");
        }
    }

    function checkSubmitButton(){
        bContainer = document.getElementById("button-container");
        if (allDocuments > 0){
            bContainer.style.display = "inline";
        } else {
            bContainer.style.display = "none";
        }
    }


    function confirmSubmit(disForm){
        if (confirm("Are you sure you want to raise a custom invoice for the entered items?")){
            disForm.submit();
        }
    }

</script>
<?php
    } catch (Exception $e) {
        $display = $e->getMessage();
        echo $display;
    }