<?php
    //Get the trans ID
    $no = isset($_GET['no']) ? $_GET['no'] : 0;
    $db = new DBConf();
    
    //Fetch details from the main transaction table
    /*$query = "SELECT pt.*,
                    DATE_FORMAT(pt.pattotal_date, '%M %d, %Y') 'trans_date',
                    DATE_FORMAT(pt.pattotal_receiptdate, '%M %d, %Y') 'paid_date',
                    CONCAT_WS(' ', r.reg_othernames, r.reg_surname) 'fullname'
                FROM pat_transtotal pt INNER JOIN registry r
                ON pt.reg_hospital_no = r.reg_hospital_no
                WHERE pattotal_id = '$no'";*/
    $query = "SELECT pt.*, lg1.lang1 'dept', lg2.lang1 'clinic',
                    DATE_FORMAT(pt.pattotal_date, '%M %d, %Y') 'trans_date',
                    DATE_FORMAT(pt.pattotal_receiptdate, '%M %d, %Y') 'paid_date',
                    CONCAT_WS(' ', r.reg_othernames, r.reg_surname) 'fullname'
                FROM pat_transtotal pt
                    INNER JOIN registry r ON pt.reg_hospital_no = r.reg_hospital_no
                    INNER JOIN department d ON pt.dept_id = d.dept_id
                    INNER JOIN clinic c ON pt.clinic_id = c.clinic_id
                    INNER JOIN language_content lg1 ON d.langcont_id = lg1.langcont_id
                    INNER JOIN language_content lg2 ON c.langcont_id = lg2.langcont_id
                WHERE pattotal_id = '$no'";
    //die('<pre>' . $query);
    $trans_total_result = $db->run($query);
    $display = '';
    if ($db->hasRows($trans_total_result)){
        $trans_total_row = mysql_fetch_assoc($trans_total_result);
        //die ('<pre>' . print_r ($trans_total_row, TRUE));
        
        //Confirm that the transaction has been paid for before displaying receipt
        if ($trans_total_row['pattotal_status'] != '1'){
            die ('Payment has not been made for this transaction yet. So, a receipt cannot be printed.');
        }
        
        if (!empty($trans_total_row['patinv_id'])){
            $pos_tid_display = '<tr>
                                    <td><strong>POS TRANS. ID:</strong></td>
                                    <td>' . stripslashes($trans_total_row['patinv_id']) . '</td>
                                </tr>';
        } else $pos_tid_display = '';
        $display = '<h3 style="border-bottom: 1px solid #CCC; text-align: center;">' . ORGANISATION_NAME . '<div style="font-size: 12px; font-weight: bold;">(Receipt)</div></h3>
                    <table cellpadding="5" cellspacing="5" width="300">
                        <tr>
                            <td><strong>NAME:</strong></td>
                            <td>' . stripslashes($trans_total_row['fullname']) . '</td>
                        </tr>
                        <tr>
                            <td><strong>DEPARTMENT:</strong></td>
                            <td>' . stripslashes($trans_total_row['dept']) . '</td>
                        </tr>
                        <tr>
                            <td><strong>CLINIC:</strong></td>
                            <td>' . stripslashes($trans_total_row['clinic']) . '</td>
                        </tr>
                        <tr>
                            <td><strong>HOSPITAL NO:</strong></td>
                            <td>' . stripslashes($trans_total_row['reg_hospital_no']) . '</td>
                        </tr>
                        <tr>
                            <td><strong>TRANSACTION ID:</strong></td>
                            <td>' . stripslashes($trans_total_row['pattotal_transno']) . '</td>
                        </tr>
                        ' . $pos_tid_display . '
                        <tr>
                            <td><strong>INVOICE DATE:</strong></td>
                            <td>' . stripslashes($trans_total_row['trans_date']) . '</td>
                        </tr>
                        <tr>
                            <td><strong>PAYMENT DATE:</strong></td>
                            <td>' . stripslashes($trans_total_row['paid_date']) . '</td>
                        </tr>
                    </table>
                    <table cellpadding="5" border="1" width="300">
                        <thead>
                            <tr>
                                <th>S/NO</th>
                                <th>DESCRIPTION</th>
                                <th>AMOUNT (&#8358;)</th>
                            </tr>
                        </thead>
                        <tbody>';
        
        //Fetch details from the transaction items table
        $query = "SELECT pti.*, psi.patservice_name
                    FROM pat_transitem pti INNER JOIN pat_serviceitem psi
                    ON pti.patservice_id = psi.patservice_id
                    WHERE pti.pattotal_id = '$no'";
        $trans_item_result = $db->run($query);
        $total = $sno = 0;
        if ($db->hasRows($trans_item_result)){
            while ($trans_item_row = mysql_fetch_assoc($trans_item_result)){
                $dis_amount = $trans_item_row['patitem_amount'] * $trans_item_row['patitem_totalqty'];
                $display .= '<tr>
                                <td>' . (++$sno) . '.</td>
                                <td>' . stripslashes($trans_item_row['patservice_name']) . '<!-- / ' . $trans_item_row['patitem_totalqty'] . '--></td>
                                <td style="text-align: right;">' . number_format($dis_amount, 2) . '</td>
                             </tr>';
            }
            $display .= '<tr>
                            <td colspan="2"><strong>TOTAL:</strong></td>
                            <td style="text-align: right;"><strong style="text-align: right;">' . number_format($trans_total_row['pattotal_totalamt'], 2) . '</strong></td>
                         </tr>';
//            $total += $trans_item_row['patitem_amount'];
        }
        $display .= '   </tbody>
                     </table>';
        $print_script = '<script type="text/javascript">
                            window.print();
                         </script>';
            
        
    } else {
        $display = "Sorry! The details of the specified transaction could not be fetched.";
        $print_script = '';
    }
?>

<html>
    <head>
        <title>Transaction Receipt <?php echo (!empty($trans_total_row['pattotal_transno']) ? "({$trans_total_row['pattotal_transno']})" : ''); ?></title>
        <style type="text/css">
            body        { font-family: Tahoma, Helvetica, 'sans serif'; width: 350px; }
            table       { border-collapse: collapse; font-size: 12px; }
        </style>
    </head>
    <body>
<?php
    echo $display . $print_script;
?>
    </body>
</html>