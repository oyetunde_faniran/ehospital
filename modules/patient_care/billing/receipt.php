
<script   type="text/javascript" src="myajax4.js"></script>
<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('./includes/config.inc.php');
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>
<script language="javascript" type="text/javascript">
function display(id1,id2,id3){  //show only 1st
		document.getElementById(id1).style.display = 'block';
		document.getElementById(id2).style.display = 'none';
			
}

</script>
<?php  
//constructors

$patinp= new patinp_account();
$hospital= new patient_admission();
$inp = new inpatient_admission();
$invoice2 = new invoice();
$wards= new wards();
$transno=$hospital->get_tran_id();
if(isset($_POST['pattotal_id']))
$pattotal_id=$_POST['pattotal_id'];

if(isset($_POST['savereceipt'])){
	$updateInvoice=$invoice2->updatePattotal($_POST);
	
	if(!empty($updateInvoice[2])){ $pattotal_id=$updateInvoice[2];$hosp_id=$updateInvoice[3];  }
	if($updateInvoice[1]){
		header ("Location: {$_SERVER['PHP_SELF']}?p=viewreceipt&c=Success&hosp_id=$hosp_id&pattotal_id=$pattotal_id");
	}else{
	$c="Insertion failed";
	echo $c ;
	}
}	
//$Numword= new Numword();
$servicename= new service();
if(isset($_GET['pattotal_id']))
$pattotal_id=$_GET['pattotal_id'];
$invoice=$invoice2->getTotalpay($pattotal_id);

$Dbconnect=new DBConf();

 $sql = "Select * from   pat_transitem pitem inner join pat_serviceitem pservice 
		   on
		 
       	   pitem.patservice_id=pservice.patservice_id  
		   where pitem.pattotal_id='$pattotal_id'";
            $res20 = $Dbconnect->execute($sql);
	//echo $sql;		
$hosp_id=$_GET['hosp_id'];

$med_id=$hospital->getCurrentmedical_trans_id($hosp_id);
$deposit_purpose_id=$hospital->getDeposit_purpose($med_id);
$clinic_id=$hospital->getClinic_id2($hosp_id,$lang_curDB,$lang_curTB);
//$transno=$hospital->get_tran_id();

//$discount=$invoice2->getDiscount($pattotal_id);
//	$netamt=$invoice['pattotal_totalamt']-$discount; 

?>
<style type="text/css">
<!--
.style1 {
	font-size: 14px;
	font-weight: bold;
}
-->
</style>

<form action="./index.php?p=receipt" method="post" name="form"  id="form"> 
<input type="hidden"  id="transno" name="transno" value="<?php if(!empty($med_id))echo $transno ?>" />
<input type="hidden"  id="pattotal_id" name="pattotal_id" value="<?php if(!empty($med_id))echo $pattotal_id ?>" />
<input type="hidden"  id="hosp_id" name="hosp_id" value="<?php if(!empty($med_id))echo $hosp_id ?>" />
<input type="hidden"  id="patadm_id" name="patadm_id" value="<?php if(!empty($med_id))echo $med_id ?>" />
<input type="hidden"  id="pay_status" name="pay_status" value="1" />

<table width="500" border="0"  align="center"cellspacing="8" cellpadding="5" summary="Payment Receipt">
  <caption align="top">
    <h4>Lagos University Teaching Hospital <br />
  Billing Center  </h4>
    <strong>PAYMENT SLIP</strong>
  
  </caption>
  <tr>
    <td scope="row" nowrap="nowrap"><strong>Receipt No</strong>:&nbsp;&nbsp;<?php if(!empty($med_id)) echo $transno ?></td>
    <td ><div align="center"><strong>Date:</strong>
        <input type="hidden"  id="pattotal_receiptdate" name="pattotal_receiptdate" value="<?php echo date("d-m-Y"); ?>" ></input><?php echo  date("M j, Y");  ?>
    </div></td>
  </tr>
  <tr>
    <td colspan="2" ><span class="style1">Received from</span>:&nbsp;&nbsp; <?php echo $hospital->getPatient_nameonly($med_id) ?></td>
  </tr>
  <tr>
    <td colspan="2"><span class="style1">The Sum of :</span>&nbsp;&nbsp;<?php echo Numword::currency($invoice['pattotal_totalamt']) ?> </td>  
  </tr>
    <tr>
    <td colspan="2"><span class="style1">Being Payment for:</span>&nbsp;&nbsp;<?php 

        $type=NULL;
	    if($invoice['pattotal_invoice_type']==1)
			$type="Admission Advice";
		 if($invoice['pattotal_invoice_type']==2)
			$type="Drug Prescriptions";
		if($invoice['pattotal_invoice_type']==3)
			$type="Registration";
		 if($invoice['pattotal_invoice_type']==4)
			$type="Lab Test";
			echo $type;
	  ?> </td>
  </tr>
  </tr>
  <tr>
    <td width="20%" ><strong>Payment Mode:</strong></td>
	<td  width="80%"><input name="paymode"  type="radio" value="radiobutton"  onClick="display('rowcash','rowchq');" checked="checked"/>Cash&nbsp;&nbsp;&nbsp;<input name="paymode" type="radio" value="radiobutton" onClick="display('rowchq','rowcash');" />Cheque</td>
  </tr>
  <tr id="rowchq" style="display:none">
    <td >Teller No.<input name="pattotal_tellerno" type="text"  size="10"/></td>
	<td >Account No.<input name="pattotal_acctno" type="text" size="10" /></td>
	
  </tr>
  <tr id="rowcash">
    <td  width="100%" colspan="2">Receipt No.<input name="pattotal_receiptmanual" type="text"  size="10"/>&nbsp;&nbsp; If Manual receipt issued previously</td>

  </tr>
  <tr>
    <td colspan="2"><strong>Amount in Figure</strong>:&nbsp;&nbsp;&nbsp;&nbsp;&#8358;<?php 
	$netamt=0;
	echo number_format($invoice['pattotal_totalamt'],2);
	?></td>
  </tr>
  <tr>
    <td colspan="2"><strong>Cashier  :&nbsp;&nbsp;&nbsp;&nbsp;</strong><?php echo $a3 ?> </td>
  </tr>
  <tr>
    <td > </td> <td ><input name="savereceipt" type="submit" value="Save Receipt"></td>
  </tr>
</table>

<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() }
      });
      cal.manageFields("f_date", "f_date", "%Y-%m-%d");
    //]]></script>
 


<!--<SCRIPT type="text/javascript" src="./includes/js/jquery-1.3.2.min.js"></SCRIPT>
        <script type="text/javascript">
            $(function(){
                $('#editbtn').click(function(){
                    var da = $('#ward_id').val();
					var da2 = $('#p').val();
					var da3 = $('#clinic_id').val();
					var da4 = $('#ward_name').val();
					var da5 = $('#ward_bedspaces').val();
					var da6 = $('#ward_siderooms').val();
                    $('#mytable').load('ajax.php',{dateval:da,dateval2:da2,dateval3:da3,dateval4:da4,dateval5:da5,dateval6:da6});});
            });

 </script>
-->
