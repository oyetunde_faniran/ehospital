<script   type="text/javascript" src="myajax3.js"></script>

<?php # Script 2.5 - main.inc.php

/*
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('./includes/config.inc.php');

	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;

} // End of defined() IF.
?>

<?php
$hospital= new patient_admission();

$inp = new inpatient_admission();
$refer= new patient_referrer();
$invoice= new invoice();
$wards= new wards();

if(isset($_GET['q'])){
$q= $_GET['q'];

$med_id=$hospital->getCurrentmedical_trans_id($q);
//echo "initial med ID ".$med_id;
}
?>

<script language="javascript" type="text/javascript">
function display(id1,id2,id3){  //show only 1st
		document.getElementById(id1).style.display = 'block';
		document.getElementById(id2).style.display = 'block';
		document.getElementById(id3).style.display = 'none';
}

</script>


<br />



	<!--<input type="hidden"  id="addWard" name="capMode" value="addWard" />-->

	<table border="0" bordercolor="#FFCCFF" width='100%'>
		<tr><td align="center" width="500">
<?php
    if(!isset($_GET['fw2'])){
?>
						<h4>
                        	Welcome!&nbsp;&nbsp; Please Supply the Patient's Hospital Number in the Search Box
<?php
	$paymentTracker = new Payment($_SESSION[session_id() . "userID"]);
	$amountCollected = $paymentTracker->getTotalDayPayment();
	$display = "<p>
					<strong>TOTAL AMOUNT COLLECTED SO FAR TODAY: <span style=\"color: #900; font-weight: bold;\">&#8358; $amountCollected</span></strong>
			    </p>";
	echo $display;
?>
                        </h4>
<?php
    } else {
        //Fetch the details of the patient
        $pat = new Patient();
        $pat_deets = $pat->getFullPatientInfo($q);

        if (!empty($pat_deets)){

            if (empty($pat_deets['reg_surname']) || empty($pat_deets['reg_othernames'])){
                $name_form = '<div>
                                <form action="" method="post">
                                    <div><label for="first-name">FIRST NAME:</label></div>
                                    <div><input type="text" name="first_name" id="first-name" maxlength="100" /></div>
                                    <div><label for="last-name">SURNAME:</label></div>
                                    <div><input type="text" name="last_name" id="last-name" maxlength="100" /></div>
                                    <div><input type="submit" name="submit_button" id="submit-button" maxlength="100" value="Save Changes" /></div>
                                </form>
                              </div>';
                echo $name_form;
            }
?>
						<table class="tblborder2"  border="1" width="70%" >
                            <tr>
                                <td colspan=3 class="bold">
                                    <a target="_blank" href="index.php?p=custom-invoice&h=<?php echo $_GET['q']; ?>" style="font-size: 18px;">Create Custom Invoice</a>
                                </td>
                            </tr>
									<tr>
										<td colspan=3 class="bold">
<?php
        echo $hospital->getPatient_name($med_id);
?>
										</td>

									</tr>
										<tr>
										<td colspan=2 class="bold">Consultant:

										 </td>
										 <td><? echo $hospital->getConsultant_name2($q,$lang_curDB,$lang_curTB);?></td>

									  </tr>
									<tr>
										<td colspan=2 class="bold">Patient Type:

										 </td>
									   <td><? echo $hospital->getPatient_typename($q,$lang_curDB,$lang_curTB); ?></td>
									</tr>
									<tr>
										<td colspan=2 class="bold">Clinic:
										 </td>
											<td><? echo $hospital->getClinic_name2($q,$lang_curDB,$lang_curTB); ?></td>
									</tr>
<!--									<tr>-->
<!--										<td colspan=3 class="bold">Registration/Consultation Charges</td>-->
<!--									</tr>-->
<!---->
<!--										<tr>-->
<!--										<td colspan=3 ><a href="./index.php?p=viewinvoices&hosp_id=--><?php //echo $q ?><!--&invoice_type=3" target="_blank">View Invoices</a></td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td colspan=3 class="bold">Admission Charges  </td>-->
<!--									</tr>-->
									<?php
									//Admission Charges starts
									 if($invoice->hasinvoiced($med_id)){
											?>
									<tr>
										<td colspan="3"><?

   											 ?><a href="./index.php?p=viewadm_charges&hospital_id=<?php echo $q ?>">View Admission Charges</a></td>
									</tr>
									<?php }?>
									<?php

									 if(!$invoice->hasinvoiced($med_id)){
									?>
									<tr>
										<td colspan=3 ><a href="./index.php?p=viewinvoices&hosp_id=<?php echo $q ?>&invoice_type=1" target="_blank">View Admission Charges</a></td>
									</tr>
									<?php }
									//Admission ends
									 ?>
<!---->
<!--									<tr>-->
<!--										<td colspan=3 class="bold">Laboratory Invoices</td>-->
<!--									</tr>-->
<!---->
<!--									<tr>-->
<!--										<td colspan=3 ><a href="./index.php?p=viewinvoices&hosp_id=--><?php //echo $q ?><!--&invoice_type=4" target="_blank">View Invoices</a></td>-->
<!--									</tr>-->
<!--									--><?php ////} ?>
<!---->
<!--									<tr>-->
<!--										<td colspan=3 class="bold">Products Invoices</td>-->
<!--									</tr>-->
<!---->
<!--									<tr>-->
<!--										<td colspan="3"><a href="./index.php?p=viewinvoices&hosp_id=--><?php //echo $q ?><!--&invoice_type=2" target="_blank">View Invoices</a></td>-->
<!--									</tr>-->
        				</table><br>
<?php
        } else {    //END patients details array is empty
            echo 'Hospital Number was not found.';
        }
    }
?>
				</td>
                <td valign="top" class="search-box-new" style="position: relative; top: -10px;">
                    <div>
                        <input type="radio" name="number_type" id="old-no" value="1" checked="checked" onclick="setNumberType(1);" />
                        <label for="old-no">New Hospital Number</label>
                        <input type="radio" name="number_type" id="new-no" value="2" onclick="setNumberType(2);" />
                        <label for="new-no">Old Hospital Number</label>
                    </div>
                    <div style="position: relative; margin-bottom: 20px;">
                        <input type="text" name="hosp" id="search-box" onkeyup="showMesg(this.value)" />
                    </div>
					<br/>
                    <div id="message"></div>
                </td>
	</tr>
</table>

<!--General Invoice Table Testing Starts-->
<?php
if(isset($q)) {
?>
<?php
//constructors

$patinp= new patinp_account();
$servicename= new service();
$hosp_id = $q;


$Dbconnect=new DBConf();
$sql_nhis="SELECT rn.nhisplan_id FROM  registry r 
	INNER JOIN registry_nhis rn ON  r.reg_id=rn.reg_id 
	WHERE  r.reg_hospital_no='$hosp_id'
	";
$result_nhis= $Dbconnect->execute($sql_nhis);
if ($Dbconnect->hasRows($result_nhis, 1)){

    $row_plan_id= mysql_fetch_array ($result_nhis);
    $nhisplan_id=$row_plan_id['nhisplan_id'];
}
$med_id=$hospital->getCurrentmedical_trans_id($hosp_id);
$sql = "Select *, MONTH(pattotal_date) 'TRANSACTION MONTH', DAY(pattotal_date) 'TRANSACTION DAY', YEAR(pattotal_date) 'TRANSACTION YEAR', pattotal_invoice_type from pat_transtotal where patadm_id='$med_id' ORDER BY pattotal_date DESC";
$res20 = $Dbconnect->execute($sql);
$clinic_id=$hospital->getClinic_id2($hosp_id,$lang_curDB,$lang_curTB);
?>
<table width="800" border="0" cellspacing="4" cellpadding="5">
<!--    <tr>-->
<!--        <td colspan="2">-->
<!--            <table width="90%" border="0" cellspacing="4" cellpadding="5" class="border">-->
                <tr class="title-row" align="center">
                    <td ><?php echo $lang_sno ?></td>
                    <td >Invoice No<?php //echo $lang_price ?></td>
                    <td >Invoice Date<?php //echo $lang_itemdesc ?></td>

                    <td >Invoice Amount(&#8358;)<?php //echo $lang_price ?></td>
                    <td >Pay Status<?php //echo $lang_qty ?></td>
                    <td >Payment Method<?php //echo $lang_Amount ?></td>
                    <td >Invoice Type</td>
                </tr>
                <?php
                $cnt2=1;
                while ($rowservicename = mysql_fetch_array($res20)) {
                    //$amount=$rowservicename['patitem_totalqty']*$rowservicename['patitem_amount'];
                    if ($cnt2%2 ==0) {$bgcolor = "tr-row";} else {$bgcolor = "tr-row2";}
                    ?>
                    <tr class="<?php echo $bgcolor ?>">
                        <td ><?php echo $cnt2 ?></td>
                        <?php if($rowservicename['pattotal_invoice_type']=='1'){
                            $pattotal_id=$rowservicename['pattotal_id'];
                            ?>
                            <td  align="center"><a href="./index.php?p=viewinvoice&pattotal_id=<?php echo $pattotal_id  ?>&hosp_id=<?php echo  $hosp_id ?>&checkinvoice=show" title="Open Invoice" accesskey="o" target="_self"><?php echo $rowservicename['pattotal_transno'] ?></a></td>
                        <?php }?>
                        <?php if($rowservicename['pattotal_invoice_type']=='2'){
                            $pattotal_id=$rowservicename['pattotal_id'];
                            ?>
                            <td align="center"><a href="./index.php?p=viewinvoice4drug&pattotal_id=<?php echo $pattotal_id  ?>&hosp_id=<?php echo  $hosp_id ?>&checkinvoice=show" title="Open Invoice" accesskey="o" target="_self"><?php echo $rowservicename['pattotal_transno'] ?></a></td>
                        <?php }?>
                        <?php if($rowservicename['pattotal_invoice_type']=='3'){
                            $pattotal_id=$rowservicename['pattotal_id'];
                            ?>
                            <td align="center"><a href="./index.php?p=viewinvoice4reg&pattotal_id=<?php echo $pattotal_id  ?>&hosp_id=<?php echo  $hosp_id ?>&checkinvoice=show" title="Open Invoice" accesskey="o" target="_self"><?php echo $rowservicename['pattotal_transno'] ?></a></td>
                        <?php }?>
                        <?php if($rowservicename['pattotal_invoice_type']=='4'){
                            $pattotal_id=$rowservicename['pattotal_id'];
                            ?>
                            <td align="center"><a href="./index.php?p=viewinvoice4service&pattotal_id=<?php echo $pattotal_id  ?>&hosp_id=<?php echo  $hosp_id ?>&checkinvoice=show" title="Open Invoice" accesskey="o" target="_self"><?php echo $rowservicename['pattotal_transno'] ?></a></td>
                        <?php }?>


                        <td ><?php echo date("M j, Y", mktime(0, 0, 0, $rowservicename['TRANSACTION MONTH'], $rowservicename['TRANSACTION DAY'],$rowservicename['TRANSACTION YEAR'])) //echo $rowservicename['pattotal_date'] ?></td>

                        <td   align="center">
                            <?php
                            //$discount=$invoice2->getDiscount($pattotal_id);
                            //	$netamt=$invoice['pattotal_totalamt']-$discount;
                            if ($rowservicename['pattotal_totalamt_nhis']> 1){
                                echo number_format($rowservicename['pattotal_totalamt_nhis'],2);
                            }else{
                                echo  number_format($rowservicename['pattotal_totalamt'],2);
                            }
                            ?>
                            <?php //echo number_format($rowservicename['pattotal_totalamt'],0) ?>

                        </td>
                        <td  align="center"><?php if($rowservicename['pattotal_status']==1)echo "Paid" ; else echo "Not Paid"; ?></td>
                        <td  align="center"><?php if($rowservicename['pattotal_paymthd']=="")echo "N/A" ; else echo $rowservicename['pattotal_paymthd']; ?></td>

                        <?php if($rowservicename['pattotal_invoice_type']=='1'){
                            ?>
                            <td  align="center">Admission</td>
                        <?php }?>
                        <?php if($rowservicename['pattotal_invoice_type']=='2'){
                            ?>
                            <td  align="center">Products</td>
                        <?php }?>
                        <?php if($rowservicename['pattotal_invoice_type']=='3'){
                            ?>
                            <td  align="center">Registration/Consultation</td>
                        <?php }?>
                        <?php if($rowservicename['pattotal_invoice_type']=='4'){
                            ?>
                            <td  align="center">Laboratory Services</td>
                        <?php }?>

                    </tr>

                    <?php  $cnt2++ ; } ?>

<!--            </table>-->
        </td>
    </tr>
</table>
<?php } ?>
<!--General Invoice Table Testing Ends-->
</body>
</html>