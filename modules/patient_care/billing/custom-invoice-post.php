<?php
//    die ('<pre>' . print_r($_POST, true));
    $conn = new DBConf();
//    $_POST = admin_Tools::doEscape($_POST, $conn);

    $hospital_no = isset($_GET['h']) ? admin_Tools::doEscape($_GET['h'], $conn) : '';

    //Get the latest patadm_id
    $query = "SELECT patadm_id FROM patient_admission WHERE reg_hospital_no = '$hospital_no'
                ORDER BY patadm_id DESC
                LIMIT 0,1";
    $result = $conn->execute($query);
    if ($conn->hasRows($result)){
        $row = mysql_fetch_assoc($result);
        $patadm_id = $row['patadm_id'];
    } else {
        $patadm_id = 0;
    }

    //Get the dept ID and clinic ID from appointment table
    $query2 = "SELECT dept_id, clinic_id FROM appointment WHERE patadm_id = '$patadm_id'
                ORDER BY app_id DESC
                LIMIT 0,1";
    $result2 = $conn->execute($query2);
    if ($conn->hasRows($result2)){
        $row = mysql_fetch_assoc($result2);
        $dept_id = $row['dept_id'];
        $clinic_id = $row['clinic_id'];
    } else {
        $dept_id = $clinic_id = 0;
    }

    //Get the POST variables
    $items = isset($_POST['description']) ? $_POST['description'] : array();
    $prices = isset($_POST['amount']) ? $_POST['amount'] : array();
    $invoice_type = isset($_POST['invoice_type']) ? $_POST['invoice_type'] : array();

    //Get the total amount
    $total_amount = 0;
    foreach ($prices as $amount){
        $total_amount += (float)$amount;
    }

    //Insert into the "transaction total" table
    $query_total = "INSERT INTO pat_transtotal
                SET pattotal_transno = '" . admin_Tools::getRandNumber(rand(6, 16)) . "',
                    pattotal_totalamt = '$total_amount',
                    pattotal_totalamt_nhis = '$total_amount',
                    patadm_id = '$patadm_id',
                    reg_hospital_no = '$hospital_no',
                    dept_id = '$dept_id',
                    clinic_id = '$clinic_id',
                    user_id = '" . $_SESSION[session_id() . 'userID'] . "',
                    pattotal_invoice_type = '$invoice_type'";
    $query_total .= $invoice_type == 3 ? ", pattotal_newstatus = '0'" : ''; //If the invoicetype is "REGISTRATION", then set pattotal_newstatus
    $result_total = $conn->execute($query_total);
    $transtotal_id = mysql_insert_id();

    /******************************
     * TO INSERT INTO THE pat_transitem TABLE, THE FOLLOWING TABLES HAS TO BE INSERTED INTO THE FOLLOWING ORDER
     * language_content
     * service_name
     * service
     * pat_serviceitem
     * pat_transitem
     *******************************/

    //Insert the items into the "transaction items" table
    foreach ($items as $key => $item){
        $amount = isset($prices[$key]) ? (float)$prices[$key] : 0;

        //Insert into the item into the language table
        $query_lang = "INSERT INTO language_content
                        SET lang1 = '$item'";
        $result_lang = $conn->execute($query_lang);
        $lang_id = mysql_insert_id();


        //Insert into "service_name"
        $query_servicename = "INSERT INTO service_name
                                SET langcont_id = '$lang_id'";
        $result_servicename = $conn->execute($query_servicename);
        $servicename_id = mysql_insert_id();


        //Insert into "service"
        $query_service = "INSERT INTO service
                                SET dusc_id = '$servicename_id',
                                    service_type = 'Servicename'";
        $result_service = $conn->execute($query_service);
        $service_id = mysql_insert_id();


        //Insert into "pat_serviceitem"
        $query_serviceitem = "INSERT INTO pat_serviceitem
                                SET patservice_type = '1',
                                    patservice_itemid = '$service_id',
                                    patservice_name = '$item'";
        $result_serviceitem = $conn->execute($query_serviceitem);
        $serviceitem_id = mysql_insert_id();


        //Insert into "pat_transitem"
        $query_item = "INSERT INTO pat_transitem
                        SET pattotal_id = '$transtotal_id',
                            patitem_amount = '$amount',
                            patitem_amount_nhis = '$amount',
                            patservice_id = '$serviceitem_id'";
        $result_item = $conn->execute($query_item);
    }   //END foreach item

//Customize the redirect page based on the invoice type
switch ($invoice_type){
    //1 for Admission , 2 for Drug, 3 for Reg, 4 for Lab
    //
    case 1:     $redirect_type = 'viewinvoice'; break;
    case 2:     $redirect_type = 'viewinvoice4drug'; break;
    case 3:     $redirect_type = 'viewinvoice4reg'; break;
    case 4:     $redirect_type = 'viewinvoice4service'; break;
}
$redirect_page = "index.php?p=$redirect_type&pattotal_id=$transtotal_id&hosp_id=$hospital_no&checkinvoice=show";
//die($redirect_page);
header ("Location: $redirect_page");