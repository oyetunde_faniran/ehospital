<?php
	$doctor = $_SESSION[session_id() . "consultantID"];
	$form = '<p>' . $appointment_text_label . '</p>
			<table width="200" border="0" cellspacing="5" cellpadding="5">
				<tr>
				  <td><strong>' . $pat_appointment_form_fromdatefield . '</strong></td>
				  <td><strong>' . $pat_appointment_form_todatefield . '</strong></td>
				  <td>&nbsp;</td>
				</tr>
				<tr>
				  <td>
					  <input name="dept" type="hidden" value="All">
					  <input name="doctor" type="hidden" value="' . $doctor . '">
					  <input type="text" name="fromdate" id="fromdate" />
				  </td>
				  <td>
					  <input type="text" name="todate" id="todate" />
				  </td>
				  <td><input type="submit" class="btn" value="Search" name="Button" /></td>
				</tr>
			  </table>';
?>

<form action="" method="post" name="filterform" id="filterform">
<?php
	echo $form;
?>
</form>





<?php
	//***BEGIN Get the consultant details
	$query = "SELECT s.staff_employee_id id, 
					 CONCAT_WS(' ', s.staff_title, UPPER(s.staff_surname), s.staff_othernames) doctor, 
					 lc1.$curLangField department, lc2.$curLangField unit				
				FROM consultant cons 
					INNER JOIN language_content lc1
					INNER JOIN language_content lc2 
					LEFT JOIN department d ON d.langcont_id = lc1.langcont_id 
					LEFT JOIN clinic cl ON cl.langcont_id = lc2.langcont_id 
					INNER JOIN staff s 
					INNER JOIN appointment app 
				ON app.consultant_id = cons.consultant_id 
					AND cons.staff_employee_id = s.staff_employee_id
					AND cons.clinic_id = cl.clinic_id 
					AND cl.dept_id = d.dept_id
				#WHERE app.consultant_id = '$doctor'"; //die ("<pre>$query</pre>");
	$appoint = new patAppointment();
	$consultantDetails = $appoint->getDoctorDeets($query);
	//***END Get the consultant details




//***BEGIN Get the appointment list
	//Query to retrieve the appointment list
	$status_key =  "consultant";
	$query = "SELECT DATE(app.app_starttime) AS 'date',
						reg.reg_hospital_no AS hospital_no, app_status,
						CONCAT_WS(' ', UPPER(reg.reg_surname), LOWER(reg.reg_othernames)) AS name,
						(
							CASE reg.reg_gender
								WHEN '0' THEN '$gender_0_label'
								WHEN '1' THEN '$gender_1_label'
							END
						) AS sex,
						YEAR(CURDATE()) - YEAR(reg.reg_dob) AS age,
						TIME_FORMAT(TIME(app.app_starttime),'%r') AS starttime,
						TIME_FORMAT(TIME(app.app_endtime),'%r') AS endtime, 
						(
							CASE app.app_type
								WHEN '0' THEN '$pat_appointment_consultation'
								WHEN '1' THEN '$pat_appointment_consultation'
								WHEN '2' THEN '$pat_appointment_followup'
							END
						) AS appoint_type,
						trans.pattotal_status AS '$status_key', 
						app.app_id AS app_id,
						trans.pattotal_status 'paid'	# = 1 if patient has paid for this consultation, 0 if not
				FROM appointment app 
				LEFT JOIN pat_transtotal trans ON (trans.patadm_id = app.patadm_id AND trans.pattotal_servicerendered='0' AND trans.pattotal_newstatus IN (0,1,2))
					INNER JOIN registry reg 
					INNER JOIN department dept 
					INNER JOIN language_content lc2 
					INNER JOIN patient_admission pat
				 ON app.patadm_id = pat.patadm_id 
					AND pat.reg_hospital_no = reg.reg_hospital_no
					AND app.dept_id = dept.dept_id
					AND dept.langcont_id = lc2.langcont_id
				#WHERE app.consultant_id = '$doctor' \n";

	//Generate the appropriate date filters
	if (!$_POST){
		$fromdate = " DATE_FORMAT(NOW(), '%y-%m-%d 00:00:00') ";
		$todate = " DATE_FORMAT(NOW(), '%y-%m-%d 23:59:59') ";
	} else {
			//Clean-up
			$fromdate = "'" . admin_Tools::doEscape($_POST["fromdate"], $appoint->conn) . " 00:00:00 '";
			$todate = "'" . admin_Tools::doEscape($_POST["todate"], $appoint->conn) . " 23:59:59 '";
	}

	//Add the date filters to the query
	$query .= " AND (app.app_starttime BETWEEN $fromdate AND $todate OR app.app_endtime BETWEEN $fromdate AND $todate) ";

	//Add the ordering clause
	$query .= " ORDER BY `date` DESC "; //die ("<pre>$query</pre>");

	//die ("<pre>$query</pre>");

	//Get the actual appointment list
	$result = $appoint->getResults4DataEntry($query);
//***END Get the appointment list


	if (!empty($result)){
		echo "<div><hr /></div>
				<table border=\"1\"  width=\"100%\">
					<tr><td colspan=\"10\" align=\"center\"><strong>$consultantDetails</strong></td></tr>
					<tr class=\"title-row\">
						<td align=\"center\"><strong>$pat_appointment_sno</strong></td>
						<td align=\"center\"><strong>$hospital_no_label</strong></td>
						<td align=\"center\"><strong>$pat_appointment_patientname</strong></td>
						<td align=\"center\"><strong>$pat_appointment_sex</strong></td>
						<td align=\"center\"><strong>$pat_appointment_age</strong></td>
						<td align=\"center\"><strong>$pat_appointment_date</strong></td>
						<td align=\"center\"><strong>$pat_appointment_starttime</strong></td>
						<td align=\"center\"><strong>$pat_appointment_endtime</strong></td>
						<td align=\"center\"><strong>$pat_appointment_apptype</strong></td>
						<td align=\"center\"><strong>$pat_appointment_appstatus</strong></td>
					 </tr>"
					 . $result . 
			 "</table>";
	} elseif ($_POST) {	//Form was submitted .i.e.Search was conducted
		echo "<div style=\"padding: 10px 0px 10px 0px;\" class=\"comments\">No appointments were found for the selected date range.</div>";
	} else {	//No search was conducted, but an attempt was made to retrieve the list of appointments for TODAY for the logged-in consultant
			echo "<div style=\"padding-top: 10px 0px 10px 0px;\" class=\"comments\">No appointment has been fixed for today.</div>";
	}
?>

<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$('#fromdate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});
	});

	$(function() {
		$('#todate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});
	});
</script>