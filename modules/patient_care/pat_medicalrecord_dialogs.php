<div id="saving-div" class="ajax-in-progress" style="display: none;">
    <h3>Action Execution In Progress...</h3>
    <img src="images/_loader.gif" />
</div>

<div id="completed-div" class="ajax-in-progress" style="display: none;">
    <h3 id="status-report"></h3>
    <input type="button" id="saved_edit" value="Close" class="all-buttons" />
</div>






<div id="new-app-dialog" title="Schedule New Appointment" style="display:hidden;">
	<div id="app-form-container">
        <form name="appointment" id="new-appointment-form" action="" method="post">
            <table cellpadding="3" cellspacing="3" align="center">
                <tr>
                    <td>
                        Please, select the next appointment date for this patient in the Calendar provided below.
                    </td>
                </tr>
    
                <tr>
                    <td><strong>NEXT APPOINTMENT DATE:</strong></td>
                </tr>
                <tr>
                    <td>
                        <input type="hidden" name="button" value="Schedule" />
                        <input type="hidden" name="hospital_no" value="<?php echo $hospital_no; ?>" />
    
                        <input type="hidden" name="appoint[date]" id="f_date" value="" />
                        <input type="hidden" name="appoint[consultant_id]" value="<?php echo $consult_details["consult_id"]; ?>" />
                        <input type="hidden" name="appoint[patadm_id]" value="<?php echo $patadmid; ?>" />
                        <input type="hidden" name="appoint[dept_id]" value="<?php echo $_SESSION[session_id() . "deptID"]; ?>" />
                        <input type="hidden" name="appoint[clinic_id]" value="<?php echo $consult_details["consult_clinic"]; ?>" />
                        <input type="hidden" name="appoint[user_id]" value="<?php echo $_SESSION[session_id() . "userID"]; ?>" />
                        <input type="hidden" name="appoint[startime]" value="00:00" />
                        <input type="hidden" name="appoint[endtime]" value="23:59" />
                        <input type="hidden" name="appoint[app_type]" value="2" />
                        <div id="date_ui"></div>
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <div id="app-saving-div" class="ajax-in-progress" style="display: none;">
        <h3>Action Execution In Progress...</h3>
        <img src="images/_loader.gif" />
    </div>
    
    <div id="app-completed-div" class="ajax-in-progress" style="display: none;">
        <h3 id="app-status-report"></h3>
        <input type="button" id="app-completed-button" value="Close" class="btn" /> 
        <span id="try-again-container" style="display: none; padding-left: 10px;"><input type="button" id="try-again-button" value="Try Again" class="btn" /></span>
    </div>

</div>





<div id="admit-dialog" title="Admit Patient Into A Ward" style="display:none;">
    <div>&nbsp;</div>
    <div id="admit-form-container">
    	Please, select a ward into which the patient would be admitted.
        <form name="admission" id="admission-form" action="" method="post">
            <table cellpadding="3" cellspacing="3" align="center">
                <tr>
                    <td><strong>WARD:</strong></td>
                    <td>
                        <select name="treat[ward_id]" id="first">
                            <option value="">--Select Ward--</option>
<?php
	$treat = new patTreatment();
	echo $treat->getWards4DropDown($_SESSION[session_id() . "clinicID"]);
?>
                        </select>
                        <input type="hidden" name="submit" value="Submit" />
                        <input type="hidden" name="hospital_no" value="<?php echo $hospital_no; ?>" />
                        <input type="hidden" name="treat[consultant_id]" value="<?php echo $consult_details["consult_id"]; ?>" />
                        <input type="hidden" name="treat[patadm_id]" value="<?php echo $patadmid; ?>" />
                    </td>
                </tr>
            </table>
        </form>
    </div>
    
    <div id="admit-saving-div" class="ajax-in-progress" style="display: none;">
        <h3>Action Execution In Progress...</h3>
        <img src="images/_loader.gif" />
    </div>
    
    <div id="admit-completed-div" class="ajax-in-progress" style="display: none;">
        <h3 id="admit-status-report"></h3>
        <input type="button" id="admit-completed-button" value="Close" class="btn" />
    </div>
</div>


<div id="refer-dialog" title="Refer Patient" style="display:none;">

</div>


<div id="transfer-dialog" title="Transfer Patient" style="display:none;">
    <div id="admit-form-container">
        <form name="appointment" id="new-appointment-form" action="" method="post">
            <table cellpadding="3" cellspacing="3" align="center">
                <tr>
                    <td colspan="2">
                        Please, select the consultant you're transfering the patient to.
                    </td>
                </tr>
                <tr>
                    <td><strong>DEPARTMENT:</strong></td>
                    <td>
                        <select name="appoint[dept_id]" id="staffdept" onchange="processClinics4Depts('staffdept', 'staffclinic');">
                            <option value="0">--Select Department--</option>
                            <?php
                            echo admin_Tools::getStaffDepts4DropDown($curLangField);
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><strong>CLINIC:</strong></td>
                    <td>
                        <select name="appoint[clinic_id]" id="staffclinic">
                            <option value="0">--Select Clinic--</option>
                        </select>
                        <span id="retplan_loader" style="display: none; padding-left: 10px; color: #999;"><?php echo $loading_label; ?></span>
                    </td>
                </tr>
                <tr>
                    <td><strong>CONSULTANT:</strong></td>
                    <td>
                        <select name="appoint[consultant_id]" id="consultant">
                            <option value="0">--Select Consultant--</option>
                        </select>
                        <span id="consultant_loader" style="display: none; padding-left: 10px; color: #999;"><?php echo $loading_label; ?></span>
                    </td>
                </tr>
                <tr>
                    <td><strong>NEXT APPOINTMENT DATE:</strong></td>
                    <td>
                        <input type="hidden" name="button" value="Schedule" />
                        <input type="hidden" name="appoint[date]" id="f_date" value="" />
                        <!--                            <input type="hidden" name="appoint[patadm_id]" value="<?php echo $patadmid; ?>" />-->
                        <input type="hidden" name="appoint[user_id]" value="<?php echo $_SESSION[session_id() . "userID"]; ?>" />
                        <input type="hidden" name="appoint[startime]" value="00:00" />
                        <input type="hidden" name="appoint[endtime]" value="23:59" />
                        <input type="hidden" name="appoint[app_type]" value="2" />
                        <div id="date_ui2"></div>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script type="text/javascript" language="javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/staffreg.js"></script>

<script type="text/javascript">
    //DATE PICKER for transfer appointment//show-date
    $(function() {
        $('#date_ui2').datepicker({
            changeMonth:	true,
            changeYear:		true,
            dateFormat:		'yy-mm-dd',
            minDate:		0,
            altField:		'#f_date',
            altFormat:		'yy-mm-dd'
        });
    });
//    $('#new-appointment-button').click(function(){
//        $('#new-app-dialog').dialog('open');
//    });
//    $("#new-app-dialog").dialog({
//        bgiframe: true,
//        autoOpen: false,
//        height: 500,
//        width: 500,
//        modal: true,
//        hide: 'explode',
//        buttons: {
//            'Schedule Appointment': function() {
//                $('#app-saving-div').fadeIn();
//                $.ajax({
//                    type: 'POST',
//                    url: 'admin_ajaxTOC.php?t=schedule-new-appointment',
//                    data: $("#new-appointment-form").serialize(),
//                    success: function(data){
//                        $('#app-saving-div').fadeOut();
//                        document.getElementById('app-completed-div').innerHTML = data;
//                        $('#app-completed-div').fadeIn();
//                    },
//                    dataType: "html"
//                });
//                //$(this).dialog('close');
//            },
//            'Cancel': function() {
//                $(this).dialog('close');
//            }
//        },
//
//        close: function() {}
//    });
    $('#staffclinic').change(function(){
        dis_url = 'admin_ajaxTOC.php?t=get-consultants-in-clinic&id=' + document.getElementById('staffclinic').value;
        $('#consultant_loader').fadeIn();
        $.ajax({
            type: 'GET',
            url: dis_url,
            success: function(data){
                try{
                    document.getElementById('consultant').innerHTML = data;
                    $('#consultant_loader').fadeOut();
                } catch (e) {}
            },
            dataType: "html"
        });
    });
</script>

<div id="hand-writing-presenting-complaint" title="Presenting Complaint [Hand Writing]" style="display: none;">
    <canvas id="colors_sketch1" width="600" height="500" style="border: 1px solid #900;"></canvas>
</div>
<div id="hand-writing-history-presenting-complaint" title="History of Presenting Complaint [Hand Writing]" style="display: none;">
    <canvas id="colors_sketch2" width="600" height="500" style="border: 1px solid #900;"></canvas>
</div>


<div id="diagnosis-search-dialog" title="Diagnosis Search" style="display:none;">
    <div style="border-bottom: 1px solid #CCC; padding: 10px 0px; margin-bottom: 10px;">
        <strong>DIAGNOSIS: </strong>
        <input type="input" type="text" id="diagnosis-search-box" size="20" onkeyup="clearTimeout(lastTimeOut); lastTimeOut = setTimeout(getMatchingDiagnosis, 500);" />
        <span id="diagnosis-search-loader-image" style="display: none;">
            <img src="images/_loader.gif" />
        </span>
    </div>
    <div id="diagnosis-search-result"></div>
</div>



<div id="new-checkbox-entry-dialog" title="Add New Check Box Entry" style="display: none;">
    <div id="new-checkbox-entry-add-server-response" style="display: none; color: #F00; font-weight: bold; font-size: 14px; text-align: center;"></div>
	<div id="new-checkbox-entry-actual">
        <form name="checkbox_add" id="new-checkbox-entry-form" action="" method="post">
            <table cellpadding="3" cellspacing="3" align="center">
                <tr>
                    <td>
                        <label><strong>ENTRY:</strong></label>
                    </td>
                    <td>
                        <input type="text" size="60" name="entry" id="new-checkbox-entry-text-entry" />
                        <input type="hidden" name="field" value="0" id="new-checkbox-entry-field" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="radio" checked="checked" name="clinic" value="<?php echo $_SESSION[session_id() . 'clinicID']; ?>" id="new-checkbox-entry-clinic-own" />
                        <label for="new-checkbox-entry-clinic-own"><?php echo 'For my clinic only (' . $_SESSION[session_id() . 'clinicName'] . ')'; ?></label>
                        
                        <input type="radio" name="clinic" value="0" id="new-checkbox-entry-clinic-all" />
                        <label for="new-checkbox-entry-clinic-all">For all clinics</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="button" class="btn" value="Add Entry" id="new-checkbox-entry-button-add" />
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <div id="new-checkbox-entry-progress-image" style="display: none;">
        <img src="images/_loader.gif" />
    </div>
</div>




<div id="delete-checkbox-entry-dialog" title="Delete Check Box Entry" style="display: none;">
    <div id="delete-checkbox-entry-server-response" style="display: none; color: #F00; font-weight: bold; font-size: 14px; text-align: center;"></div>
	<div id="delete-checkbox-entry-actual">
        <form name="checkbox_delete" id="delete-checkbox-entry-form" action="" method="post">
            <table cellpadding="3" cellspacing="3" align="center">
                <tr>
                    <td colspan="2">
                        <div>
                            Are you sure you want to delete the selected question/item?
                            <span id="delete-checkbox-entry-confirm-question"></span>
                        </div>
                        <div style="margin-top: 10px; text-align: right;">
                            <input type="hidden" value="0" id="delete-checkbox-entry-question-id" name="id" />
                            <input type="button" class="btn" value="Yes, Delete" id="delete-checkbox-entry-button-yes" />
                            <input type="button" class="btn" value="No" id="delete-checkbox-entry-button-no" />
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <div id="delete-checkbox-entry-progress-image" style="display: none;">
        <img src="images/_loader.gif" />
    </div>
</div>