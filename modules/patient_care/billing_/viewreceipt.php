
<script   type="text/javascript" src="myajax4.js"></script>
<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>
<script language="javascript" type="text/javascript">
function display(id1,id2,id3){  //show only 1st
		document.getElementById(id1).style.display = 'block';
		document.getElementById(id2).style.display = 'none';
			
}

</script>
<?php 
//require_once ('./includes/config.inc.php'); 
//constructors

$patinp= new patinp_account();
$hospital= new patient_admission();
$inp = new inpatient_admission();
$invoice2 = new invoice();
$wards= new wards();
//$transno=$hospital->get_tran_id();
//if(isset($_GET['pattotal_id']))
$pattotal_id=$_GET['pattotal_id'];

//$Numword= new Numword();
$servicename= new service();
if(isset($_GET['pattotal_id']))
$invoice=$invoice2->getTotalpay($pattotal_id);

$Dbconnect=new DBConf();

/* $sql = "Select * from   pat_transitem pitem inner join pat_serviceitem pservice 
		   on
		 
       	   pitem.patservice_id=pservice.patservice_id  
		   where pitem.pattotal_id='$pattotal_id'";
		  // 	echo $sql;	
            $res20 = $Dbconnect->execute($sql);
*/	
$hosp_id=$_GET['hosp_id'];

$med_id=$hospital->getCurrentmedical_trans_id($hosp_id);
$deposit_purpose_id=$hospital->getDeposit_purpose($med_id);
$clinic_id=$hospital->getClinic_id2($hosp_id,$lang_curDB,$lang_curTB);
//$transno=$hospital->get_tran_id();

//$discount=$invoice2->getDiscount($pattotal_id);
	//$netamt=$invoice['pattotal_totalamt']-$discount; 

?>
<style type="text/css">
<!--
.style1 {
	font-size: 14px;
	font-weight: bold;
}
-->
</style>



<table width="700" border="0"  cellspacing="0" cellpadding="5" summary="Payment Receipt">
  <caption align="top">
    <h4>Lagos University Teaching Hospital <br />
  Billing Center  </h4>
    <strong>RECEIPT</strong>
  
  </caption>
  <tr>
    <td width="394"><span class="style1">Receipt No</span></strong>:&nbsp;&nbsp;<strong>
      <?php if(!empty($med_id)) echo $invoice['pattotal_receiptno'] ;?>
    </strong></td>
    <td width="286"><strong>Date</strong>:
        <?php if(!empty($med_id)) echo  date("d-m-Y") ;?></td>
  </tr>
  <tr>
    <td colspan="2" scope="row"><span class="style1">Received from</span>:&nbsp;&nbsp; <?php echo $hospital->getPatient_nameonly($med_id); echo " (".$hospital->gethospital_name($med_id).")" ?></td>
  </tr>
  <tr>
    <td colspan="2"><span class="style1">The Sum of :</span>&nbsp;&nbsp;<?php echo Numword::currency($invoice['pattotal_totalamt']) ?> </td>
  </tr>
  <tr>
    <td colspan="2"><span class="style1">Being Payment for:</span>&nbsp;&nbsp;
        <?php 
	    if($invoice['pattotal_invoice_type']==1)
			$type="Admission Advice";
		 if($invoice['pattotal_invoice_type']==2)
			$type="Drug Prescriptions";
		if($invoice['pattotal_invoice_type']==3)
			$type="Registration";
		 if($invoice['pattotal_invoice_type']==4)
			$type="Lab Test";
			if(isset($type))
			echo $type;
	  ?>
    </td>
  </tr>
  <?php /*?><tr>
    <td ><strong>Payment Mode:</strong></td>
    <td ><?php if($invoice['pattotal_tellerno']==""){
	echo "Cash";
	echo "<br/>";
	echo "Receipt Number";
	echo "<br/>";
	
	echo  $invoice['pattotal_receiptmanual'];
	}else echo "Cheque";?>
    </td>
  </tr><?php */
  $sql_nhis="SELECT rn.nhisplan_id FROM  registry r 
	INNER JOIN registry_nhis rn ON  r.reg_id=rn.reg_id 
	WHERE  r.reg_hospital_no='$hosp_id'
	";
		$result_nhis= $Dbconnect->execute($sql_nhis);
if ($Dbconnect->hasRows($result_nhis, 1)){

	$row_plan_id= mysql_fetch_array ($result_nhis);
	$nhisplan_id=$row_plan_id['nhisplan_id'];
	
}
  ?>
  
  <tr>
    <td colspan="2"><span class="style1">Amount in Figure:
        </span>
      <?php 
	//$discount=$invoice2->getDiscount($pattotal_id);
//	$netamt=$invoice['pattotal_totalamt']-$discount; 
	 if (!empty($nhisplan_id)){
	echo "   ".number_format($invoice['pattotal_totalamt_nhis'],2);
	}else{
	echo "   ".number_format($invoice['pattotal_totalamt'],2);
	}
	?>    </td>
  </tr>
  <tr>
    <td colspan="2"><span class="style1">Cashier  :</span><?php echo "    ".$a3 ?> </td>
  </tr>
  <tr>
    <td colspan="2" align="right"><form action="./viewreceipt1.php?<?php echo $_SERVER['QUERY_STRING']?>" method="post" name="form"  id="form">
      <!--<input name="printpreview" type="submit" value="Print Preview">-->
    </form></td>
  </tr>
</table>
<!--<SCRIPT type="text/javascript" src="./includes/js/jquery-1.3.2.min.js"></SCRIPT>
        <script type="text/javascript">
            $(function(){
                $('#editbtn').click(function(){
                    var da = $('#ward_id').val();
					var da2 = $('#p').val();
					var da3 = $('#clinic_id').val();
					var da4 = $('#ward_name').val();
					var da5 = $('#ward_bedspaces').val();
					var da6 = $('#ward_siderooms').val();
                    $('#mytable').load('ajax.php',{dateval:da,dateval2:da2,dateval3:da3,dateval4:da4,dateval5:da5,dateval6:da6});});
            });

 </script>
-->
