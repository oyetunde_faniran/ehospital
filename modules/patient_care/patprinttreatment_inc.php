<?php
$patuser = new patUsers;
$stafftype = $patuser->getStaffType($myUserid);
$consult_details = $patuser->isconsultant($myStaffid);
if ($stafftype == '1'){
	echo $error_msg_3;
}elseif(is_array($consult_details) == FALSE){
	$msg = "error_msg_".$consult_details;
	echo $$msg;
}else{
	extract($consult_details);
	$patdetails = $treat->getPatientDetails($bb, $cc);
	if($patdetails === FALSE){

	}
	else{
			extract($patdetails);
					
?>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
	<td colspan="2" align="center" valign="top"><H2><strong><?php echo $appTitle ?></strong></H2>
    	<h3>
			<strong><?php echo $department_label ?></strong>:
<?php
	$department_detail = $select->selectDepartment($dept, $curLangField);
	if(is_array($department_detail)){
		extract($department_detail);
		echo $dept_name; 
	}
?>

<br />

		<strong><?php echo $clinic_label ?></strong>: 
<?php
	$clinic_detail = $select->selectClinic($myClinicid, $curLangField);
	if(is_array($clinic_detail)){
		extract($clinic_detail);
		echo $clinic_name; 
	}
?>
	</h3>
	</td>
			  </tr>
  <tr>
	<td align="left" valign="top"><strong><?php echo $name_label ?>: </strong> <?php echo $name ?><br />      
    <strong><?php echo $hospital_no_label ?>: </strong><?php echo $hospital_no ?></td>
				<td align="right" valign="top"><?php echo date('F d, Y'); ?></td>
  </tr>
</table>
			
			<!--- Show prescription if any -->
		<?php 
		$pres_row = $treat->selectPrescription($casenote_id);
		if ($pres_row !=""){ ?>
			<table width="100%" border="0" cellspacing="5" cellpadding="5">
			  <tr>
				<td colspan="3" align="left" valign="top"><strong>PRESCRIPTION</strong></td>
			  </tr>
			  <tr>
				<td align="left" valign="top"><strong>S/N</strong></td>
				<td align="left" valign="top"><strong>Drug</strong></td>
				<td align="left" valign="top"><strong>Dosage</strong></td>
			  </tr>
				<?php echo 	$pres_row;?>	
			</table>

			<?php 
			}//end prescription
			?>
			
			<!--- Show lab  test if any -->
			<?php  
			$labtest = $treat->selectLabtest($casenote_id);
			if ($labtest !==0){ ?>
				<table width="100%" border="0" cellspacing="5" cellpadding="5">
				  <tr>
					<td align="left"><strong>LAB TEST</strong></td>
				  </tr>
				  <tr>
					<td align="left"><?php echo $labtest ?></td>
				  </tr>
				</table>
			<?php
			}//end lab test
			?>
			
			<!--- Show other treatment if any -->
			<?php  
			$othertreat = $treat->selectTreatment($casenote_id);
			if ($othertreat !==0){ ?>
				<table width="100%" border="0" cellspacing="5" cellpadding="5">
				  <tr>
					<td align="left"><strong>OTHER TREATMENT</strong></td>
				  </tr>
				  <tr>
					<td align="left"><?php echo $othertreat; ?></td>
				  </tr>
				</table>
			<?php
			}//end other treat
			?>
			
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
			  <tr>
			    <td>&nbsp;</td>
			    <td align="right">&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td align="right">&nbsp;</td>
		      </tr>
			  <tr>
				<td width="57%"><?php echo $consultant_label ?>: <?php echo $consult_name ?></td>
				<td width="43%" align="right">Signature:.....................................................</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
		</table>
<p><a href="#" onclick="window.print(); return false"><?php echo $patreg_printnow_label; ?></p>
<?php
	}
}
?>
