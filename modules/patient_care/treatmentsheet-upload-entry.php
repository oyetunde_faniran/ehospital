<?php
    if (!empty($_GET["comments"])){
        echo "<div class=\"comments\" style=\"padding-bottom: 20px;\">" . $_GET["comments"] . "</div>";
    }

    $appID = isset($_GET["aa"]) ? (int)$_GET["aa"] : 0;
    //Get the appointment & patient details
    $app = new patAppointment();
    $app_deets = $app->getAppointment($appID);
    if (!empty($app_deets)){
        $pat_app_details = "<table cellspacing=\"3\" cellpadding=\"3\" style=\"margin-bottom: 20px;\">
                                <tr>
                                    <td><h3><strong>PATIENT'S NAME:</strong></h3></td>
                                    <td>{$app_deets['patName']}</td>
                                </tr>
                                <tr>
                                    <td><h3><strong>PATIENT'S HOSPITAL NUMBER:</strong></h3></td>
                                    <td>{$app_deets['reg_hospital_no']}</td>
                                </tr>
                                <tr>
                                    <td><h3><strong>APPOINTMENT DATE:</strong></h3></td>
                                    <td>{$app_deets['appDate']}</td>
                                </tr>
                            </table>";
        echo $pat_app_details;
    }   //END if appointment details were found
?>

<div id="accordion">
        <h3 style="padding: 10px 10px 10px 25px;">Upload Scanned Treatment Sheet</h3>
        <div>
            <form action="" method="post" enctype="multipart/form-data">
                <div id="docscontainer">
                    To add documents, click the <strong>'Add More Documents'</strong> link. Then select a scanned document to upload and enter a description (if available).<br /><br />
                    <div class="comments" style="padding-bottom: 10px;">
                        Please, note that the maximum file size allowed is <?php echo FILES_MAX_SIZE_TEXT; ?> (per document) and the accepted file formats are <?php echo FILES_ALLOWED_TYPES; ?>.<br />
                    </div>
                    <span class="rightsLink" onclick="addMoreDocuments();">Add a new document</span>
                    <span id="button-container" style="padding-left: 10px; display: none;"><input type="button" value="Save Documents" class="btn" onclick="confirmSubmit(this.form);" /></span>
                    <div>&nbsp;</div>
                    <div id="uploaddiv1" class="uploadDivs"></div>
                </div>
            </form>
        </div>




    <h3 style="padding: 10px 10px 10px 25px;">View Uploaded Treatment Sheets</h3>
    <div>
<?php
    $uploader = new FileUploader();
    echo $uploader->getUploadedDocuments($appID);
?>
    </div>
</div>

<?php
    //Show the "Back to Appointment List" link if it is a treatment sheet entry officer that is accessing this page
    if (empty($entry)){
        echo '<div style="padding-top: 20px">
                  <a href="index.php?p=treatmentsheet&m=patient_care">&laquo; Back to Appointment List</a>
              </div>';
    }
?>





<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/smoothness/ui.core.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/smoothness/ui.base.css" rel="stylesheet" />
<script type="text/javascript" src="library/doubleSelect.1.2/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
<script type="text/javascript">

    //Show the different treatment sheet entry options in an accordion
	$(function() {
		$("#accordion").accordion({
			collapsible: true,
			active: false,
			clearStyle: true
		});
	});
    nextDocument = 2;
    allDocuments = 0;


    function setValue(whichDoc){
        t = document.getElementById("docsname" + whichDoc);
        if (t.value == "Type Document Description Here"){
            t.value = "";
        }
    }


    function addMoreDocuments(){
        try {
            cont = document.getElementById("docscontainer");
            newDoc = "<div style=\"padding-bottom: 10px;\">";
            newDoc = newDoc + " <input type=\"text\" name=\"docsname[]\" id=\"docsname" + nextDocument + "\" size=\"40\" style=\"color: #666;\" value=\"Type Document Description Here\" onclick=\"setValue(" + nextDocument + ");\" />";
            newDoc = newDoc + " <input type=\"file\" name=\"docs[]\" id=\"docs" + nextDocument + "\"  size=\"20\" />";
            newDoc = newDoc + " <input type=\"button\" value=\"Remove Document\" class=\"btn\" onclick=\"removeDocument(" + nextDocument + ");\" />";
            newDoc = newDoc + "</div>";

            newDiv = document.createElement("div");
            newDiv.id = "uploaddiv" + nextDocument;
            newDiv.className = "uploadDivs";
            newDiv.innerHTML = newDoc;
            cont.appendChild(newDiv);

            nextDocument++;
            allDocuments++;
            checkSubmitButton();
        } catch (e) {
            alert ("An error occurred while trying to add a new document field.");
        }
    }

    function removeDocument(whichDoc){
        c = "uploaddiv" + whichDoc;
        //p = "docscontainer";
        try {
            c = document.getElementById(c);
            //p = document.getElementById(p);
            c.parentNode.removeChild(c);
            allDocuments--;
            checkSubmitButton();
        } catch (e) {
            alert ("An error occurred while trying to add a new document field.");
        }
    }

    function checkSubmitButton(){
        bContainer = document.getElementById("button-container");
        if (allDocuments > 0){
            bContainer.style.display = "inline";
        } else {
            bContainer.style.display = "none";
        }
    }

    function confirmSubmit(disForm){
        if (confirm("Are you sure you want to upload the selected documents as the treatment sheets of this patient for this appointment?")){
            disForm.submit();
        }
    }

</script>