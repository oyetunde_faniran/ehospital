<?php # Script 2.6 - search.inc.php

/*
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');

	// Redirect to the index page:
	$url = BASE_URL . 'index.php';

	// Pass along search terms?

} // End of defined() IF.


	$app = new patAppointment();
	$appID = isset($_GET["id"]) ? (int)$_GET["id"] : 0;
    $clinicID = isset($_GET["dc"]) ? (int)$_GET["dc"] : 0;

?>


<script type="text/javascript">
	function doCloseCleanUp(){
		edit1 = opener.document.getElementById("showEdit" + <?php echo $appID; ?> + "_1");
		edit2 = opener.document.getElementById("showEdit" + <?php echo $appID; ?> + "_2");
		vStatus = opener.document.getElementById("appstatus" + <?php echo $appID; ?>);
		edit1.innerHTML = " [Edit] ";
		edit2.innerHTML = " [Edit] ";
		vStatus.innerHTML = "Already Taken";
		window.close();//
	}
</script>



<?php
	//Get the details of the patient that owns this appointment and also confirm that today's the appointment due date and also in the clinic of this nurse
	//$patDeets = $app->getPatAppointmentDeets($appID, $_SESSION[session_id() . "clinicID"]);
    $patDeets = $app->getPatAppointmentDeets($appID, $clinicID);
    $vitalSigns = new patVitalSigns();

	if (isset($_GET["c"]))
		echo "<p class=\"comments\">{$_GET['c']}</p>";

	if (!isset($_GET["ct"])){       //Vital Signs was just saved. So, show comments about the operation to the user
		if ($patDeets !== false){   //Are the patients credentials / details OK?
				//To be shown when a nurse is only able to view, but not edit vital signs (i.e. The vital signs has been used by a doctor)
				$topPatientDeets = "<h3><strong>" . $patDeets["patDeets"] . "</strong></h3><hr />";

				//To be shown when a nurse can view & edit vital signs (i.e. The vital signs has not been used by any doctor)
				$topDeets = "<div>$vitalsigns_entrypage_story</div> <div>&nbsp;</div>" . $topPatientDeets;
                $treat = new patTreatment();
                $vitalSignsDeets = $vitalSigns->getVitalSignsDeets($patDeets["patAdmID"]);
                $editing = is_array ($vitalSignsDeets) ? true : false;


			//Show the form to edit
			if (!$vitalSigns->usedByDoctor($patDeets["patAdmID"])){
				echo  $topDeets;
?>
                <form action="" method="post" name="vitalsignsentryform">
                    <table cellspacing="5" cellpadding="5">
                      <tr>
                        <td><strong><?php echo $vitalsigns_temperature; ?>:</strong></td>
                        <td><input type="text" name="temp" id="temp"
<?php
	echo $editing ? " value=\"{$vitalSignsDeets['patvital_temp']}\" " : "";
?>
                         maxlength="6" /> degrees
                         <select name="temp_unit" id="temp_unit">
                            <option value="C" <?php echo $vitalSignsDeets["patvital_temp_unit"] == "C" ? " selected=\"selected\" " : ""; ?>>C</option>
                            <option value="F" <?php echo $vitalSignsDeets["patvital_temp_unit"] == "F" ? " selected=\"selected\" " : ""; ?>>F</option>
                         </select>
                         </td>
                      </tr>

                      <tr>
                        <td><strong><?php echo $vitalsigns_pulse; ?>:</strong></td>
                        <td><input type="text" name="pulse" id="pulse"
<?php
	echo $editing ? " value=\"{$vitalSignsDeets['patvital_pulse']}\" " : "";
?>
                         maxlength="20" /></td>
                      </tr>

                      <tr>
                        <td><strong><?php echo $vitalsigns_respiration; ?>:</strong></td>
                        <td><input type="text" name="resp" id="resp"
<?php
	echo $editing ? " value=\"{$vitalSignsDeets['patvital_resp']}\" " : "";
?>
                         maxlength="50" /></td>
                      </tr>

                      <tr>
                        <td><strong><?php echo $vitalsigns_bloodpressure; ?>:</strong></td>
                        <td><input type="text" name="bp" id="bp"
<?php
	echo $editing ? " value=\"{$vitalSignsDeets['patvital_bp']}\" " : "";
?>
                         maxlength="20" /></td>
                      </tr>

                      <tr>
                        <td><strong><?php echo $vitalsigns_weight; ?>:</strong></td>
                        <td><input type="text" name="weight"
<?php
	echo $editing ? " value=\"{$vitalSignsDeets['patvital_weight']}\" " : "";
?>
                         id="weight" maxlength="3" size="5" /> <strong>KG</strong></td>
                      </tr>

                      <tr>
                        <td><strong><?php echo $vitalsigns_others; ?>:</strong></td>
                        <td>
                            <textarea name="others" id="others" rows="5" cols="20"><?php echo $editing ? $vitalSignsDeets['patvital_others'] : "";?></textarea>
                        </td>
                      </tr>

<?php
    $fBuilder = new FormBuilder();
    $custom_form = $fBuilder->getForm($clinicID, 1, true, false);
    echo $custom_form;
?>

                      <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="btn" name="sButton" value="<?php echo $save_label; ?>" />
                            <input type="hidden" name="pid" value="<?php echo $patDeets["patAdmID"]; ?>" />
                            <input type="hidden" name="ed" value="<?php echo $editing ? "1" : "0";	//Editing is being carried out ?>" />
                            <input type="hidden" name="vsi" value="<?php echo $editing ? $vitalSignsDeets["patvital_id"] : "0";	//The id of the entered vital signs in the vital signs table ?>" />
                            <input type="hidden" name="aid" value="<?php echo $appID; ?>" />
                        </td>
                      </tr>
                    </table>
                </form>
<?php
            } else {
					$display = "<p>$vitalsigns_usedByDoctor</p>
								$topPatientDeets
								<table>
										<tr>
											<td><strong>$vitalsigns_temperature:</strong></td>
											<td>{$vitalSignsDeets['patvital_temp']}&deg;{$vitalSignsDeets['patvital_temp_unit']}</td>
										</tr>

										<tr>
											<td><strong>$vitalsigns_pulse:</strong></td>
											<td>{$vitalSignsDeets['patvital_pulse']}</td>
										</tr>

										<tr>
											<td><strong>$vitalsigns_respiration:</strong></td>
											<td>{$vitalSignsDeets['patvital_resp']}</td>
										</tr>

										<tr>
											<td><strong>$vitalsigns_bloodpressure:</strong></td>
											<td>{$vitalSignsDeets['patvital_bp']}</td>
										</tr>

										<tr>
											<td><strong>$vitalsigns_weight:</strong></td>
											<td>{$vitalSignsDeets['patvital_weight']} KG</td>
										</tr>

										<tr>
											<td><strong>$vitalsigns_others:</strong></td>
											<td>{$vitalSignsDeets['patvital_others']}</td>
										</tr>
									</table>";
					echo $display;
			}	//END else part of if vital signs used by a doctor
		} else echo $vitalsigns_invalidPatient;
	} else echo "<h3><strong>" . $patDeets["patDeets"] . "</strong></h3><div>&nbsp;</div>
				<div>
					<button onclick=\"window.history.back();\" class=\"btn\">Edit Vital Signs</button>
					<button onclick=\"doCloseCleanUp();\" class=\"btn\">Close Window</button>
				</div>";
?>