<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<?php 
	$form = 0;
	$status = $pat_appointment_status;
	$status1 = $pat_appointment_paid;
	$status2 = $pat_appointment_unpaid;
	//die ("$status -- $status1 -- $status2");
	
	//Please do not change
	$status_key = "others";

/*
The next executed line returns somethhing like
Array
(
    [patuser_name] => Mr. Akpan Aliu
    [patuser_empid] => 2
    [patuser_stafftype] => 0
)
*/
	$patuser_detail = $patuser->getPatUser($myUserid); //die ("<pre>" . print_r ($patuser_detail, true) . "</pre>");
	extract($patuser_detail);
	$appoint = new patAppointment; 
	$consult_details = $patuser->isconsultant($patuser_empid);
	$query = "SELECT dept_id, lc.$curLangField dept_name 
				FROM department dept INNER JOIN language_content lc
				ON dept.langcont_id = lc.langcont_id WHERE dept.dept_isclinical='1'";

if ($patuser_stafftype == '1'){
	echo $error_msg_3;
}else{
	if(is_array($consult_details) == TRUE){
		extract($consult_details); 
		$status = $pat_appointment_action;
		$status1 = $pat_appointment_start;
		$status2 = $pat_appointment_nil;
		
		//Please do not change
		$status_key =  "consultant";
		
		$form = '
		<p>'.$appointment_text_label.'</p>
		<table width="200" border="0" cellspacing="5" cellpadding="5">
					  <tr>
						<td><strong>'.$pat_appointment_form_fromdatefield.'</strong></td>
						<td><strong>'.$pat_appointment_form_todatefield.'</strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>
							<input name="dept" type="hidden" value="All">
							<input name="doctor" type="hidden" value="'.$consult_id.'">
							<input type="text" name="fromdate" id="fromdate" />
						</td>
						<td>
							<input type="text" name="todate" id="todate" />
						</td>
						<td><input type="submit" class="btn" value="Search" name="Button" /></td>
					  </tr>
					</table>';
		?>
		<!--<p><strong><?php //echo $consultant_label ?>: <?php //echo $consult_name; ?></strong><br />
		<strong><?php //echo $clinic_label ?></strong>: <?php /*$clinic_detail = $select->selectClinic($consult_clinic, $curLangField); 
		if(is_array($clinic_detail)){
		extract($clinic_detail);
		echo $clinic_name;
		}*/
?></p>
--><?php
	}	//END if inside the else block
 ?>

<div>
        <form action="index.php?<?php echo $_SERVER['QUERY_STRING']; ?>" method="post" name="filterform" id="filterform">
            <?php 
        	
		if ($form === 0){ echo "<p> $appointment_text_label2 </p>";
		?>
            <table width="200" border="0" cellspacing="5" cellpadding="5">
              <tr>
                <td><strong><?php echo $pat_appointment_form_deptfield; ?></strong></td>
                <td><strong><?php echo $pat_appointment_form_doctorfield; ?></strong></td>
                <td><strong><?php echo $pat_appointment_form_fromdatefield; ?></strong></td>
                <td><strong><?php echo $pat_appointment_form_todatefield; ?></strong></td>
                <td>&nbsp;</td>
              </tr>


              <tr>
                <td>
                    <select name="dept" onchange="this.form.w.value = this.options[this.selectedIndex].text;">
<?php
	echo $appoint->getDepts($query);
?>
                    </select>
                    <input type="hidden" name="w" value="" />
                    <script language="javascript" type="text/javascript">
						disForm = document.getElementById("filterform");
						if (disForm){
							disForm.w.value = disForm.dept.options[disForm.dept.selectedIndex].text;
						}
					</script>
                </td>
                <td>
                    <select name="doctor" onchange="this.form.w.value = this.options[this.selectedIndex].text;">
<?php
	echo $appoint->getDoctors4DropDown();
?>
                    </select>
                </td>
                <td>
                    <input type="text" name="fromdate" id="fromdate" />
                </td>
                <td>
                    <input type="text" name="todate" id="todate" />
                </td>
                <td><input type="submit" class="btn" value="Search" name="Button" /></td>
              </tr>
            </table>
<?php 
				$doctor = "All";
			} else {
					echo $form;
					$doctor = $consult_id;
			}
?>
        </form>
</div>
	<div>&nbsp;</div>
        <div>
<?php
$thedate = isset($_POST['fromdate']) ? $_POST["fromdate"] : "Today";
$thedate2 = isset($_POST['todate']) ? $_POST["todate"] : "Today";
//Form the query
		echo "<hr /><div>&nbsp;</div>";
		//Query to retrieve appointment list
		$query = "SELECT DATE(app.app_starttime) AS date,
							reg.reg_hospital_no AS hospital_no, app_status,
							CONCAT_WS(' ', UPPER(reg.reg_surname), LOWER(reg.reg_othernames)) AS name,
							(
								CASE reg.reg_gender
									WHEN '0' THEN '$gender_0_label'
									WHEN '1' THEN '$gender_1_label'
								END
							) AS sex,
							YEAR(CURDATE()) - YEAR(reg.reg_dob) AS age,
							TIME_FORMAT(TIME(app.app_starttime),'%r') AS starttime,
							TIME_FORMAT(TIME(app.app_endtime),'%r') AS endtime, 
							(
								CASE app.app_type
									WHEN '0' THEN '$pat_appointment_consultation'
									WHEN '1' THEN '$pat_appointment_consultation'
									WHEN '2' THEN '$pat_appointment_followup'
								END
							) AS appoint_type,
							trans.pattotal_status AS '$status_key', 
							app.app_id AS app_id
					FROM appointment app 
					LEFT JOIN pat_transtotal trans ON (trans.patadm_id = app.patadm_id AND trans.pattotal_servicerendered='0' AND trans.pattotal_newstatus != '')
						INNER JOIN registry reg 
						INNER JOIN department dept 
						INNER JOIN language_content lc2 
						INNER JOIN patient_admission pat
					 ON app.patadm_id = pat.patadm_id 
						AND pat.reg_hospital_no = reg.reg_hospital_no
						AND app.dept_id = dept.dept_id
						AND dept.langcont_id = lc2.langcont_id
					WHERE true";	//Apply filters (if any)

		$dept = isset($_POST["dept"]) ? $_POST["dept"] : 0;
		$doctor = isset($_POST["doctor"]) ? $_POST["doctor"] : 0;
		$conn = new DBConf();

 		if (!empty($_POST["fromdate"]) && !empty($_POST["todate"])){
			//Clean-up
			$fromdate = mysql_real_escape_string($_POST["fromdate"], $conn->getConnectionID());
			$todate = mysql_real_escape_string($_POST["todate"], $conn->getConnectionID());
			
			//Make it a valid DateTime value
			$fromdate = ($fromdate == "Today") ? " DATE_FORMAT(NOW(), '%y-%m-%d 00:00:00') " : "'$fromdate 00:00:00'";
			$todate = ($todate == "Today") ? " DATE_FORMAT(NOW(), '%y-%m-%d 23:59:59') " : "'$todate 23:59:59'";

			//Add to query
			$query .= " AND (app.app_starttime BETWEEN $fromdate AND $todate OR app.app_endtime BETWEEN $fromdate AND $todate) ";
		}
	
		if ($dept != "All"){
			$dept = is_numeric($dept) ? $dept : 0;
			$query .= " AND app.dept_id = '$dept'";
		}

		if (!isset($_POST["doctor"])){
			$query .= " AND DATE(app.app_starttime) = CURDATE() ";
		}

		$resultFound = false;
		//Single Doctor
		if ($doctor != "All"){
			//die ("-->$doctor");
			$doctor = is_numeric($doctor) ? $doctor : 0;
			$query .= " AND app.consultant_id = '$doctor'";
			$query .= " ORDER BY app.app_starttime, reg.reg_surname, reg.reg_othernames";
			//die ("<pre>$query</pre>");
			$s = $appoint->getResults($query);	//Get the real appointment list

			if (!$s === false){
?>
<div class="headerText"><strong><?php echo strtoupper($pat_appointment_list); ?></strong>&nbsp; 
(<strong><?php echo strtolower($pat_appointment_form_fromdatefield) ?>:</strong> 
<?php echo "$thedate" ?> <strong><?php echo strtolower($pat_appointment_form_todatefield) ?>:</strong>
<?php echo  "$thedate2"; ?>)</div>
<?php
				//Query to retrieve doctor details
				$query = "SELECT s.staff_employee_id id, 
								 CONCAT_WS(' ', s.staff_title, UPPER(s.staff_surname), s.staff_othernames) doctor, 
								 lc1.$curLangField department, lc2.$curLangField unit				
							FROM consultant cons 
								INNER JOIN language_content lc1
								INNER JOIN language_content lc2 
								LEFT JOIN department d ON d.langcont_id = lc1.langcont_id 
								LEFT JOIN clinic cl ON cl.langcont_id = lc2.langcont_id 
								INNER JOIN staff s 
								INNER JOIN appointment app 
							ON app.consultant_id = cons.consultant_id 
								AND cons.staff_employee_id = s.staff_employee_id
								AND cons.clinic_id = cl.clinic_id 
								AND cl.dept_id = d.dept_id
							WHERE app.consultant_id = '$doctor'"; //die ("<pre>$query</pre>");
				$d = $appoint->getDoctorDeets($query);
				if ($d !== false){
/*					echo "<table border=\"1\"  width=\"100%\">
								<tr><td colspan=\"10\" align=\"center\"><strong>$d</strong></td></tr>
											<tr><td align=\"center\"><strong>$pat_appointment_sno</strong></td>
												<td align=\"center\"><strong>$pat_appointment_date</strong></td>
												<td align=\"center\"><strong>$hospital_no_label</strong></td>
												<td align=\"center\"><strong>$pat_appointment_patientname</strong></td>
												<td align=\"center\"><strong>$pat_appointment_sex</strong></td>
												<td align=\"center\"><strong>$pat_appointment_age</strong></td>
												<td align=\"center\"><strong>$pat_appointment_starttime</strong></td>
												<td align=\"center\"><strong>$pat_appointment_endtime</strong></td>
												<td align=\"center\"><strong>$pat_appointment_apptype</strong></td>
												<td align=\"center\"><strong>$status</strong></td>
											</tr>".$s."</table>";*/
					//Display for consultants
					echo "<table border=\"1\"  width=\"100%\">
								<tr><td colspan=\"10\" align=\"center\"><strong>$d</strong></td></tr>
								<tr class=\"title-row\">
									<td align=\"center\"><strong>$pat_appointment_sno</strong></td>
									<td align=\"center\"><strong>$hospital_no_label</strong></td>
									<td align=\"center\"><strong>$pat_appointment_patientname</strong></td>
									<td align=\"center\"><strong>$pat_appointment_sex</strong></td>
									<td align=\"center\"><strong>$pat_appointment_age</strong></td>
									<td align=\"center\"><strong>$pat_appointment_date</strong></td>
									<td align=\"center\"><strong>$pat_appointment_starttime</strong></td>
									<td align=\"center\"><strong>$pat_appointment_endtime</strong></td>
									<td align=\"center\"><strong>$pat_appointment_apptype</strong></td>
									<td align=\"center\"><strong>$pat_appointment_appstatus</strong></td>
									<!--<td align=\"center\"><strong>$status</strong></td>-->
								 </tr>"
								 . $s . 
						 "</table>";
				} else echo "<div>$pat_appointment_error_unknown_doctor</div>";
				$resultFound = true;
			}
		} else {	//All Doctors
				//Get the consultant_id of all the doctors
				$ids = $appoint->getDoctors();
				if ($ids !== false){
					//Fetch the list of appointment for each doctor
					foreach ($ids as $v){
						$tempQuery = $query . " AND app.consultant_id = '$v'  ORDER BY lc2.$curLangField, app.app_starttime, reg.reg_surname, reg.reg_othernames";
		//die($tempQuery);
						$s = $appoint->getResults($tempQuery);
						if (!$s === false){
							$docQuery = "SELECT s.staff_employee_id id, 
								 CONCAT_WS(' ', s.staff_title, UPPER(s.staff_surname), 
								s.staff_othernames) doctor, lc1.$curLangField department, lc2.$curLangField unit				
										FROM consultant cons 
										INNER JOIN language_content lc1
										INNER JOIN language_content lc2 
										LEFT JOIN department d ON d.langcont_id = lc1.langcont_id 
										LEFT JOIN clinic cl ON cl.langcont_id = lc2.langcont_id 
										INNER JOIN staff s 
										INNER JOIN appointment app 
										ON app.consultant_id = cons.consultant_id 
											AND cons.staff_employee_id = s.staff_employee_id
											AND cons.clinic_id = cl.clinic_id 
											AND cl.dept_id = d.dept_id
										WHERE app.consultant_id = '$v'";
							$d = $appoint->getDoctorDeets($docQuery);

							if ($d !== false){
								//Display for Health Records Officials
								echo "<table width=\"100%\" border=\"1\">
											<tr><td colspan=\"10\" align=\"center\"><strong>$d</strong></td></tr>
											<tr class=\"title-row\">
												<td align=\"center\"><strong>$pat_appointment_sno</strong></td>
												<td align=\"center\"><strong>$hospital_no_label</strong></td>
												<td align=\"center\"><strong>$pat_appointment_patientname</strong></td>
												<td align=\"center\"><strong>$pat_appointment_sex</strong></td>
												<td align=\"center\"><strong>$pat_appointment_age</strong></td>
												<td align=\"center\"><strong>$pat_appointment_date</strong></td>
												<td align=\"center\"><strong>$pat_appointment_starttime</strong></td>
												<td align=\"center\"><strong>$pat_appointment_endtime</strong></td>
												<td align=\"center\"><strong>$pat_appointment_apptype</strong></td>
												<td align=\"center\"><strong>$pat_appointment_appstatus</strong></td>
												<!--<td align=\"center\"><strong>$status</strong></td>-->
											</tr>".$s."</table><p>&nbsp;</p>";
							} else echo "<div>$pat_appointment_error_unknown_doctor</div>";
							$resultFound = true;
						}
					}	//END foreach($ids)
				} else  echo $pat_appointment_error_nodoctor;
			}	//END else part of if (doctor != all)

		if (!$resultFound)
			echo "<div class='redText'>".$pat_appointment_error_noresult."</div>";
	//}	//END if ($_POST)
?>
    </div>
<?php 
}
?>
<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$('#fromdate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});
	});

	$(function() {
		$('#todate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});
	});
</script>

