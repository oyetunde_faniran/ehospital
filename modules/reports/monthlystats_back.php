<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<h3><?php echo $report_monthlystats_header; ?></h3>
<div><?php echo $report_monthlystats_story; ?></div>
<div id="navlist">
	<div>&nbsp;</div>
    <form action="" method="post" name="monthlystatsform" target="_blank">
        <table>
            <tr>
                <td align="left">
                	<span style="padding-right: 20px;">
                        <label for="month" style="padding-right: 10px;">MONTH: </label>
                        <select name="month" id="month">
<?php
	$month = date("m");
	$year = date("Y");
	$month = (int)$month;
	$common = new report_common();
	echo $common->getMonthsDropDown($month);
	$years = $common->getYearsDropDown($year);
?>
                        </select>
                    </span>
                    <span>
                        <label for="year" style="padding-right: 10px;">YEAR: </label>
                        <select name="year" id="year">
<?php
	echo $years;
?>
                        </select>
                    </span>
                </td>
            </tr>
            <tr>
<script language="javascript" type="text/javascript">
	function viewReport(d){
		month = document.getElementById("month").value;
		year = document.getElementById("year").value;
		disLink = "index.php?p=" + d + "&m=reports" + "&month=" + month + "&year=" + year;
		location.href = disLink;
	}
</script>
                <td><a href="javascript: viewReport('inpatsummary');"><?php echo $report_monthlystats_inpsummary_header; ?></a></td>
            </tr>
            <tr><td>Monthly Department Activity Analysis</td></tr>
            <tr><td><a href="javascript: viewReport('outpatattendance');"><?php echo $report_monthlystats_outpatattendance; ?></a></td></tr>
            <tr><td><a href="javascript: viewReport('outpatunitbyunit');"><?php echo $report_monthlystats_outpatunits; ?></a></td></tr>
            <tr><td>Out-Patient Unit-by-Unit Analysis (Surgery)</td></tr>
            <tr><td>Out-Patient Unit-by-Unit Analysis (Paediatrics)</td></tr>
            <tr><td>Out-Patient Unit-by-Unit Analysis (Dental)</td></tr>
            <tr><td>Out-Patient Attendance by Age Group</td></tr>
            <tr><td>Out-Patient Unit HIV Paediatrics</td></tr>
            <tr><td>Out-Patient Unit HIV Adult</td></tr>
            <tr><td>Dental Division</td></tr>
            <tr><td>Source of New Patients</td></tr>
            <tr><td>Live Births, Still Births, NeoNatal and Maternal Deaths by Sex</td></tr>
            <tr><td>Deliveries and NeoNatal Mortality Data</td></tr>
            <tr><td>In-Patient Analysis by Sex</td></tr>
            <tr><td>In-Patient Admission By Age Group</td></tr>
            <tr><td>In-Patient Unit-by-Unit (Medicine)</td></tr>
            <tr><td>In-Patient Unit-by-Unit (Surgery)</td></tr>
            <tr><td>In-Patient Unit-by-Unit (Paediatrics)</td></tr>
            <tr><td>In-Patient (Dental Division)</td></tr>
            <tr><td>In-Patient (H.I.V. Adult)</td></tr>
            <tr><td>In-Patient (H.I.V. Children)</td></tr>
            <tr><td>A & E In-Patient Records</td></tr>
            <tr><td>Road Traffic Accident</td></tr>
            <tr><td>Other Clinics</td></tr>
            <tr><td>RadioDiagnosis</td></tr>
            <tr><td>Physiotherapy</td></tr>
            <tr><td>Dialysis Centre</td></tr>
            <tr><td>Radiotherapy Department</td></tr>
            <tr><td>Medical Social Services</td></tr>
            <tr><td>Occupational Therapy</td></tr>
            <tr><td>Diagnosis Centre</td></tr>
            <tr><td>Laboratory Services</td></tr>
            <tr><td>Clinical Pathology</td></tr>
            <tr><td>Modular Theatre</td></tr>
            <tr><td>K-Block Theatre</td></tr>
            <tr><td>Accident & Emergency Theatre</td></tr>
            <tr><td>Guiness Eye Centre Theatre</td></tr>
        </table>
	</form>
</div>