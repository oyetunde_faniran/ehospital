<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">
<html lang=\"en\"><head>

    
        <title>Page Title here</title>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
        <meta name=\"author\" content=\"Dmitry Baranovskiy\">
        <meta name=\"description\" content=\"Charting JavaScript Library\">
        <link rel=\"stylesheet\" href=\"library/raphaelchart/demo.css\" type=\"text/css\" media=\"screen\" charset=\"utf-8\">
        <link rel=\"stylesheet\" href=\"library/raphaelchart/demo-print.css\" type=\"text/css\" media=\"print\" charset=\"utf-8\">
        <script src=\"library/raphaelchart/raphael.js\" type=\"text/javascript\" charset=\"utf-8\"></script>

        <script src=\"library/raphaelchart/g_002.js\" type=\"text/javascript\" charset=\"utf-8\"></script>
        <script src=\"library/raphaelchart/g.js\" type=\"text/javascript\" charset=\"utf-8\"></script>
        <script type=\"text/javascript\" charset=\"utf-8\">
            window.onload = function () {
                var r = Raphael(\"holder\");
                r.g.txtattr.font = \"12px 'Fontin Sans', Fontin-Sans, sans-serif\";
                
                r.g.text(320, 50, \"<Title here>\").attr({\"font-size\": 14});
                
                var pie = r.g.piechart(320, 240, 100, [100, 50, 80, 20, 20, 20, 20, 20, 10, 5], {legend: [\"Enterprise Users(100)\", \"IE Users(50)\", \"Firefox\", \"Opera\"], legendpos: \"west\"});
                pie.hover(function () {
                    this.sector.stop();
                    this.sector.scale(1.1, 1.1, this.cx, this.cy);
                    if (this.label) {
                        this.label[0].stop();
                        this.label[0].scale(1.5);
                        this.label[1].attr({\"font-weight\": 800});
                    }
                }, function () {
                    this.sector.animate({scale: [1, 1, this.cx, this.cy]}, 500, \"bounce\");
                    if (this.label) {
                        this.label[0].animate({scale: 1}, 500, \"bounce\");
                        this.label[1].attr({\"font-weight\": 400});
                    }
                });
                
            };
        </script>
    </head>
<body class=\"raphael\" id=\"g.raphael.dmitry.baranovskiy.com\">
	<div id=\"holder\"></div>
</body>
</html>