<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
} // End of defined() IF.


//Get needed vars for search
include_once ("monthlystats_dateinit.php");

$common  = new report_common();

	if ($showReport){
		echo "<div>&nbsp;</div>
				<div><hr /></div>
				<div>&nbsp;</div>";
		$title = "<h2>$report_monthlystats_inpsummary_header $report_monthlystats_inpsummary_for <em>$displayDate</em>.</h2>";
		$result = $title;// . "<div>&nbsp;</div>";

		$dis = new report_monthlystats_inpatsummary($month, $year, $fromDate, $toDate);
		include_once ("printview.php");
?>

<script type="text/javascript" src="library/table2Excel.js"></script>

<?php
		//Get all needed vars
		$avgDailyBeds = $dis->getAvgDailyBedsAvailable();
		$occupied = $dis->getAvgDailyBedsOccupied();
		$avgStayLength = round($dis->getAvgStayLength(), 2);
		$occuPerc = round($dis->getOccuPerc($occupied), 2) . "%";
		$monthAdms = $dis->getMonthAdmission();
		$monthDischarges = $dis->getMonthDischarges($curLangField);
		$monthDeaths = $dis->getMonthDeaths();
		$patsMonthBegin = $dis->getPatsMonthBegin();
		$patsMonthEnd = $dis->getPatsMonthEnd();
	
		$adms = $dis->getAdmissions();
		$deaths = $dis->getDeaths();
		$discharges = $dis->getDischarges();
	
	
		$result .= "<p>
					<table border=\"1\" style=\"border-color: #999999;\">
					<tr class=\"tr-row\">
						<td><strong>" . $report_monthlystats_avgdailybeds . ":</strong></td>
						<td width=\"20\" align=\"center\">" . $avgDailyBeds . "</td>
					</tr>
				
					<tr class=\"tr-row2\">
						<td><strong>" . $report_monthlystats_avgdailybedsoccupied . ":</strong></td>
						<td align=\"center\">" . $occupied .  "</td>
					</tr>
				
					<tr class=\"tr-row\">
						<td><strong>" . $report_monthlystats_avglengthofdays . ":</strong></td>
						<td align=\"center\">" . $avgStayLength . "</td>
					</tr>
				
					<tr class=\"tr-row2\">
						<td><strong>" . $report_monthlystats_occuperc . ":</strong></td>
						<td align=\"center\">" . $occuPerc . "</td>
					</tr>
				
					<tr class=\"tr-row\">
						<td><strong>" . $report_monthlystats_monthadmission . ":</strong></td>
						<td align=\"center\">" . $monthAdms . "</td>
					</tr>
				
					<tr class=\"tr-row2\">
						<td><strong>" . $report_monthlystats_monthdischarges . ":</strong></td>
						<td align=\"center\">" . $monthDischarges . "</td>
					</tr>
				
					<tr class=\"tr-row\">
						<td><strong>" . $report_monthlystats_monthdeaths . ":</strong></td>
						<td align=\"center\">" . $monthDeaths . "</td>
					</tr>
				
					<tr class=\"tr-row2\">
						<td><strong>" . $report_monthlystats_patsmonthbegin . ":</strong></td>
						<td align=\"center\">" . $patsMonthBegin . "</td>
					</tr>
				
					<tr class=\"tr-row\">
						<td><strong>" . $report_monthlystats_patsmonthend . ":</strong></td>
						<td align=\"center\">" . $patsMonthEnd . "</td>
					</tr>
				</table>
			</p>
	
			<p>
			<table border=\"1\" style=\"border-color:#999999;\">
			<tr class=\"title-row\">
				<td>&nbsp;</td>
				<th>" . $report_monthlystats_admissions . "</th>
				<th>" . $report_monthlystats_discharges . "</th>
				<th>" . $report_monthlystats_deaths . "</th>
			</tr>
	
			<tr class=\"tr-row\">
				<th>" . $report_monthlystats_male . "</th>
				<td>" . $adms["0"] . "</td>
				<td>" . $discharges["0"] . "</td>
				<td>" . $deaths["0"] . "</td>
			</tr>
	
			<tr class=\"tr-row2\">
				<th>" . $report_monthlystats_female . "</th>
				<td>" . $adms["1"] . "</td>
				<td>" . $discharges["1"] . "</td>
				<td>" . $deaths["1"] . "</td>
			</tr>
	
			<tr class=\"tr-row\">
				<th>" . $report_monthlystats_total . "</th>
				<td>" . $adms["total"] . "</td>
				<td>" . $discharges["total"] . "</td>
				<td>" . $deaths["total"] . "</td>
			</tr>
		</table>
		</p>";
	
		echo "<div id=\"resultContainer\">$result</div>";
	


/****** BEGIN EXPORT TO EXCEL STUFFS ******/

		//Form all the needed vars for all forms of export
		$contents = array (
						array ($report_monthlystats_avgdailybeds, $avgDailyBeds), 
						array ($report_monthlystats_avgdailybedsoccupied, $occupied), 
						array ($report_monthlystats_avglengthofdays, $avgStayLength), 
						array ($report_monthlystats_occuperc, $occuPerc), 
						array ($report_monthlystats_monthadmission, $monthAdms), 
						array ($report_monthlystats_monthdischarges, $monthDischarges), 
						array ($report_monthlystats_monthdeaths, $monthDeaths), 
						array ($report_monthlystats_patsmonthbegin, $patsMonthBegin), 
						array ($report_monthlystats_patsmonthend, $patsMonthEnd),
						array ("", ""),
						array ("", ""),
						array ("", $report_monthlystats_admissions, $report_monthlystats_discharges, $report_monthlystats_deaths),
						array ($report_monthlystats_male, $adms["0"], $discharges["0"], $deaths["0"]),
						array ($report_monthlystats_female, $adms["1"], $discharges["1"], $deaths["1"]),
						array ($report_monthlystats_total, $adms["total"], $discharges["total"], $deaths["total"]),
					);
		$options = array (
						"rowHeader" => true,
						"colHeader" => false,
					);
		$contentArray = array (
							"contents" => $contents,
							"options" => $options
						);
	//	$barChart = array();
	
		$finalArray = array (
							"title"		=> $title,			//Report title
							//"print"		=> $result,		//Content for print view
							"others"	=> $contentArray	//Array of values to be used to export to pdf & excel
							//"barChart"	=> $barChart	//Array of values for barcharts and pie charts
						);
		echo "<form method=\"post\" id=\"xpform\" name=\"xpform\" target=\"_blank\" action=\"export.php?{$_SERVER['QUERY_STRING']}&action=excel\">
					<input type=\"hidden\" name=\"xp\" value='" . base64_encode(serialize($finalArray)) . "' />
				</form>";

/****** END EXPORT TO EXCEL STUFFS ******/


	}		//END if $showReports
?>