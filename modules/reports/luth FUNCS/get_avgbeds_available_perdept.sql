DELIMITER $$

DROP FUNCTION IF EXISTS `mediluth_skye`.`get_avgbeds_available_perdept`$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_avgbeds_available_perdept`(`dmonth` VARCHAR(2), `dyear` VARCHAR(4), `ddept` INT) RETURNS float(10,2)
    SQL SECURITY INVOKER
    COMMENT 'Calculates the average number of vacant beds for a month per dep'
BEGIN
	DECLARE eachday INT;	                                
	DECLARE alldays INT;	                                          
	DECLARE totalbeds INT;	                                                                 
	DECLARE whichday INT;	                                     
	DECLARE whichdate VARCHAR(20);	                                                    
	DECLARE daysinmonth INT;	                                               
	DECLARE average FLOAT (10,2);	                           
	SET @alldays = 0;
	SET @whichday = 0;
	SET dmonth = LPAD(dmonth, 2, '0');

	                                                    
	SELECT DAY(LAST_DAY(CONCAT_WS('-', dyear, dmonth, '01'))) INTO @daysinmonth;


                                                                                                                                                             
	SELECT IFNULL(SUM(w.ward_bedspaces) + SUM(ward_siderooms), 0) INTO @totalbeds 
	FROM wards w INNER JOIN clinic c
	ON w.clinic_id = c.clinic_id
	WHERE c.dept_id = ddept;
	                                                                         
	REPEAT

		SET @whichday = @whichday + 1;
		SET @whichdate = CONCAT_WS('-', dyear, dmonth, LPAD(@whichday, 2, '0'));
		SET @whichdate = CONCAT(@whichdate, ' 23:59:59');

		SELECT COUNT(*) INTO @eachday 
		FROM inpatient_admission inp INNER JOIN wards w INNER JOIN clinic c
		ON inp.ward_id = w.ward_id AND w.clinic_id = c.clinic_id
		WHERE inp.inp_dateattended <= @whichdate 
		AND (inp.inp_datedischarged IS NULL OR inp.inp_datedischarged = '0000-00-00 00:00:00' OR inp.inp_datedischarged > @whichdate)
		AND c.dept_id = ddept;

		SET @alldays = @alldays + (@totalbeds - @eachday);

	UNTIL @whichday = @daysinmonth
	END REPEAT;

	SET @average = @alldays / @daysinmonth;

	RETURN @average;
END$$

DELIMITER ;