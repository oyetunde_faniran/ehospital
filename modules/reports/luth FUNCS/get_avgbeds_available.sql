DELIMITER $$

DROP FUNCTION IF EXISTS `mediluth_skye`.`get_avgbeds_available`$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_avgbeds_available`(dmonth VARCHAR(2), dyear VARCHAR(4)) RETURNS float(10,2)
    SQL SECURITY INVOKER
    COMMENT 'Calculates the average number of vacant beds for a month'
BEGIN
	DECLARE eachday INT;	                                
	DECLARE alldays INT;	                                          
	DECLARE totalbeds INT;	                                                                 
	DECLARE whichday INT;	                                     
	DECLARE whichdate VARCHAR(20);	                                                    
	DECLARE daysinmonth INT;	                                               
	DECLARE average FLOAT (10,2);	                           
	SET @alldays = 0;
	SET @whichday = 0;
	SET dmonth = LPAD(dmonth, 2, '0');

	                                                    
	SELECT DAY(LAST_DAY(CONCAT_WS('-', dyear, dmonth, '01'))) INTO @daysinmonth;

	                                                                                
	SELECT (SUM(ward_bedspaces) + SUM(ward_siderooms)) INTO @totalbeds FROM wards;

	                                                                         
	REPEAT

		SET @whichday = @whichday + 1;
		SET @whichdate = CONCAT_WS('-', dyear, dmonth, LPAD(@whichday, 2, '0'));
		SET @whichdate = CONCAT(@whichdate, ' 23:59:59');
		
		SELECT COUNT(*) INTO @eachday FROM inpatient_admission
		WHERE inp_dateattended <= @whichdate 
		AND (inp_datedischarged IS NULL OR inp_datedischarged = '0000-00-00 00:00:00' OR inp_datedischarged > @whichdate);

		                                                                                            
		SET @alldays = @alldays + (@totalbeds - @eachday);

	UNTIL @whichday = @daysinmonth
	END REPEAT;

	SET @average = @alldays / @daysinmonth;

	RETURN @average;
    END$$

DELIMITER ;