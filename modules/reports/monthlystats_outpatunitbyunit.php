<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

//Get needed vars for search
	$dept = isset($_POST["dept"]) ? (int)$_POST["dept"] : 0;
	if ($_POST){
		$ddate = isset($_POST["ddate"]) ? $_POST["ddate"] : "";
		if (preg_match("/^[0-9]{4,4}[-][0-9]{2,2}$/", $ddate)){
			$ddate = explode("-", $ddate);
			$year = $ddate[0];
			$month = $ddate[1];
		} else {
				$year = date("Y");
				$month = (int)date("m");
		}
		$fromDate = "$year-" . str_pad($month, 2, "0", STR_PAD_LEFT) . "-01 00:00:00";
		$toDate = "$year-" . str_pad($month, 2, "0", STR_PAD_LEFT) . "-31 23:59:59";
		if (!empty($dept))
			$showReport = true;
	} else {
			$showReport = false;
			$fromDate = "";
			$toDate = "";
			$month = "";
			$year = "";
			$dept = 0;
		}

	//Format the date to be displayed like this January, 2009
	$displayDate = date("F, Y", strtotime($fromDate));

	$common  = new report_common();
?>

<div>&nbsp;</div>


<?php
	echo "<div>$report_monthlystats_outpatunits_story</div><div>&nbsp;</div>";
?>


<form method="post" name="unitbyunitform">
    <table cellspacing="3" cellpadding="3">
      <tr>
        <td>
<?php
	echo "<strong>" . strtoupper($report_dept) . "</strong>: ";
?>
        </td>
        <td>
            <select name="dept" id="dept">
<?php
	$query = "SELECT dept.dept_id, lc.$curLangField dept
				FROM department dept INNER JOIN language_content lc
				ON dept.langcont_id = lc.langcont_id
				WHERE dept.dept_isclinical = '1'
				ORDER BY dept";
	echo $common->getAllDepts4DropDown($query, "dept");
?>
            </select>
        </td>
      </tr>
      <tr>
        <td>
<?php
	echo "<strong>" . strtoupper($report_date) . "</strong>: ";
?>
        </td>
        <td>
            <input type="text" name="ddate" 
<?php
	if (isset($_GET["ddate"]) && !isset($_POST["ddate"])){
		if (preg_match("/^[0-9]{4,4}[-][0-9]{2,2}$/", $_GET["ddate"]))
			echo " value=\"{$_GET['ddate']}\"";
	} elseif (isset($_POST["ddate"]))
			echo " value=\"{$_POST['ddate']}\"";
		else echo " value=\"" . date("Y-m") . "\"";
?>
             id="ddate" maxlength="7" size="7" />
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="submit" name="sButton" value="<?php echo $report_show; ?>" class="btn" /></td>
      </tr>
    </table>
</form>

<?php
if ($showReport){
	include_once ("printview.php");
	echo "<div>&nbsp;</div>
			<div><hr /></div>
			<div>&nbsp;</div>";
	$title = "<h2>$report_monthlystats_outpatunits (<span id=\"deptname\"></span>) $report_monthlystats_inpsummary_for <em>$displayDate</em>.</h2>";
?>

<div id="resultContainer">
<?php
	echo $title;
?>
	<div>&nbsp;</div>
    <script language="javascript" type="text/javascript">
		disDept = document.getElementById("dept");
		disSpan = document.getElementById("deptname");
		disSpan.innerHTML = disDept.options[disDept.selectedIndex].text;
	</script>
    <script type="text/javascript" src="library/table2Excel.js"></script>
    <table border="1" class="reportTables" id="reportTable">
    	<thead class="title-row">
        	<tr>
                <th><?php echo $report_monthlystats_sno; ?></th>
                <th><?php echo $report_monthlystats_unit; ?></th>
                <th colspan="2"><?php echo $report_monthlystats_newpatient; ?></th>
                <th colspan="2"><?php echo $report_monthlystats_oldpatient; ?></th>
                <th colspan="2"><?php echo $report_monthlystats_totalatt; ?></th>
                <th><?php echo $report_monthlystats_clinicsessions; ?></th>
            </tr>
        </thead>

		<tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>
            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>
            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>
            <td>&nbsp;</td>
        </tr>

<?php
	$clinicQuery = "SELECT c.clinic_id, lc.$curLangField clinics FROM clinic c
					INNER JOIN language_content lc 
					ON lc.langcont_id = c.langcont_id
					WHERE dept_id = '$dept'
					ORDER BY clinics";
	$dis = new report_monthlystats_outpatunitbyunit($month, $year);
	$result = $dis->getResults($clinicQuery, $dept, $report_total, $report_gTotal);
	if (!empty($result))
		echo $result;
	else echo "<tr><td colspan=\"9\">$report_monthlystats_unit_error</td></tr>";
?>
    </table>
</div>
<?php
		$exportForm =  "<form method=\"post\" id=\"xc\" name=\"xform\" target=\"_blank\" action=\"export.php?{$_SERVER['QUERY_STRING']}&action=excel\">
							<input type=\"hidden\" name=\"t\" value='$title' />
							<input type=\"hidden\" name=\"v\" value='1' />
							<input type=\"hidden\" name=\"h\" value='1' />
						</form>";
		echo $exportForm;
	}	//if $showReport
?>

<link type="text/css" href="library/admin_jquery/themes/base/ui.datepicker.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/base/ui.base.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/base/ui.theme.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$('#ddate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm',
			showButtonPanel: true,
			showOn: 'button',
			buttonImage: 'images/calendar.gif',
			buttonImageOnly: true,
			onChangeMonthYear: function(year, month, inst) {
									d = document.getElementById("ddate");
									m = month.toString();
									if (m.length == 1)
										m = "0" + m;
									d.value = year + "-" + m;
								}

		});
	});
</script>