<?php # Script 2.6 - search.inc.php

/*
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');

	// Redirect to the index page:
	$url = BASE_URL . 'index.php';

	// Pass along search terms?

} // End of defined() IF.


    $fromDate = isset($_GET["fromdate"]) ? $_GET["fromdate"] : date('Y-m-d');
    $toDate = isset($_GET["todate"]) ? $_GET["todate"] : date('Y-m-d');
    $cashier = isset($_GET["cashier"]) ? (int)$_GET["cashier"] : 0;
    $dept = isset($_GET["department"]) ? (int)$_GET["department"] : 0;
    $payMode = isset($_GET["paymentmode"]) ? (int)$_GET["paymentmode"] : 0;
    include_once ("printview.php");

?>

<script type="text/javascript" src="library/table2Excel.js"></script>
<div>
    <p>You can customize this report below or simply click the 'Show Report' button to view all transactions.</p>
    <form method="get" action="">
    	<fieldset>
        	<legend>Customize Report</legend>
            <table cellspacing="5" cellpadding="5">
                <tr>
                    <td><strong>FROM:</strong></td>
                    <td><input type="text" name="fromdate" id="fromdate" value="<?php echo !empty($fromDate) ? $fromDate : "Click to set date"; ?>" /></td>
                    <td><strong>TO:</strong></td>
                    <td><input type="text" name="todate" id="todate" value="<?php echo !empty($toDate) ? $toDate : "Click to set date"; ?>" /></td>
                </tr>
                <tr>
                    <td><strong>COLLECTED BY:</strong></td>
                    <td>
                        <select name="cashier" id="cashier">
                            <option value="">--All Cashiers--</option>
<?php
	//Get hospital staff working as Billing Officials
	$query = "SELECT s.user_id, '-' 'staffNo', CONCAT_WS(' ', s.teller_title, s.teller_surname, s.teller_othernames) 'staffName'
				FROM bank_tellers s INNER JOIN users u
				ON s.user_id = u.user_id
				WHERE u.group_id = '" . BILLING_GROUP_ID . "'# OR u.group_id = '" . SUPER_ADMIN_GROUP_ID . "'
				ORDER BY s.teller_title, s.teller_surname, s.teller_othernames";
	$display = "";
	$conn = new DBConf();
	$result = $conn->run($query);
	if ($conn->hasRows($result)){
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
            $selected = $cashier == $row["user_id"] ? " selected=\"selected\" " : "";
			$display .= "<option $selected value=\"" . $row["user_id"] . "\">" . stripslashes($row["staffName"]) /*. " [" . $row["staffNo"] . "]"*/ . "</option>";
		}
		//echo $display;
	}
	echo $display;
?>
                        </select>
                    </td>
                    <td><strong>PAYMENT MODE:</strong></td>
                    <td>
                        <select name="paymentmode" id="paymentmode">
                            <option value="0">--All Payment Modes--</option>
                            <option value="1" <?php echo $payMode == 1 ? " selected=\"selected\" " : ""; ?>>Cash</option>
                            <!--<option value="3" <?php echo $payMode == 3 ? " selected=\"selected\" " : ""; ?>>Mobile Money</option>-->
                            <option value="2" <?php echo $payMode == 2 ? " selected=\"selected\" " : ""; ?>>POS</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><strong>DEPARTMENT:</strong></td>
                    <td>
                        <select name="department" id="department">
                            <option value="0">--All Departments--</option>
<?php
	if (isset($_GET["department"]))
		echo admin_Tools::getStaffDepts4DropDown($curLangField, $_GET["department"]);
	else echo admin_Tools::getStaffDepts4DropDown($curLangField);
?>
                        </select>
                    </td>
                </tr>
                <tr>
                	<td>&nbsp;</td>
                    <td>
                        <input type="hidden" name="p" id="p" value="viewfinreports" />
                        <input type="hidden" name="m" id="m" value="reports" />
                        <input type="submit" name="sButton" id="sButton" value="<?php echo $report_showReport; ?>" class="btn"/>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>

<?php
	//Show report if the form was submitted
	if ($_GET){
        //Show reports iff dates were chosen
        if (!empty($fromDate) && !empty($toDate)){
            $conn = new DBConf();
            $fromDate = admin_Tools::doEscape($fromDate, $conn) . " 00:00:00";
			$toDate = admin_Tools::doEscape($toDate, $conn) . " 23:59:59";
            $addQuery = "";
            //$addQuery .= !empty($cashier) ? " AND s.user_id = '$cashier' " : "";
            $addQuery .= !empty($cashier) ? " AND s.user_id = '$cashier' " : "";
            $addQuery .= !empty($dept) ? " AND pt.dept_id = '$dept' " : "";
            //$addQuery .= !empty($payMode) ? ($payMode == 1 ? " AND pattotal_paymthd = 'Cash' " : " AND pattotal_paymthd = 'POS' ") : "";
            if (!empty($payMode)){
                $pay_mode_text = '';
                switch($payMode){
                    case 2: $pay_mode_text = 'POS'; break;
                    case 3: $pay_mode_text = 'Mobile Money'; break;
                    case 1:
                    default: $pay_mode_text = 'Cash'; break;
                }   //END switch
                $addQuery .= " AND pattotal_paymthd = '$pay_mode_text' ";
            }   //END if

            $query = "SELECT pt.*, CONCAT_WS(' ', reg_surname, reg_othernames) 'patName', reg.reg_hospital_no 'patNo',
                            CONCAT_WS(' ', s.teller_surname, s.teller_othernames) 'staffName',
							DATE_FORMAT (pattotal_receiptdate, '%M %d, %Y') 'transDate', lc.lang1 'dept',
                            IFNULL(pti.patitem_amount, 0) 'transCost'
                        FROM pat_transtotal pt
                            INNER JOIN registry reg
                            INNER JOIN bank_tellers s
                            INNER JOIN department d
                            INNER JOIN language_content lc
                        ON pt.reg_hospital_no = reg.reg_hospital_no
                            AND s.user_id = pt.user_id
                            AND pt.dept_id = d.dept_id
                            AND d.langcont_id = lc.langcont_id
                        LEFT JOIN pat_transitem pti
                     	ON pt.pattotal_id = pti.pattotal_id
                     		AND pti.patservice_id = '" . TRANSACTION_COST_SERVICE_ID . "'
                        WHERE pattotal_receiptdate BETWEEN '$fromDate' AND '$toDate'
                            AND pattotal_status = '1'
                            $addQuery
                        ORDER BY pattotal_receiptdate DESC, lc.lang1, staffName DESC";
            /*$query = "SELECT pt.*, CONCAT_WS(' ', reg_surname, reg_othernames) 'patName', reg.reg_hospital_no 'patNo',
                            CONCAT_WS(' ', b.teller_surname, b.teller_othernames) 'staffName',
							DATE_FORMAT (pattotal_receiptdate, '%M %d, %Y') 'transDate', lc.lang1 'dept',
                            IFNULL(pti.patitem_amount, 0) 'transCost'
                        FROM pat_transtotal pt
                            INNER JOIN registry reg
                            INNER JOIN bank_tellers b
                            INNER JOIN department d
                            INNER JOIN language_content lc
                        ON pt.reg_hospital_no = reg.reg_hospital_no
                            AND b.user_id = pt.user_id
                            AND pt.dept_id = d.dept_id
                            AND d.langcont_id = lc.langcont_id
                        LEFT JOIN pat_transitem pti
                     	ON pt.pattotal_id = pti.pattotal_id
                     		AND pti.patservice_id = '" . TRANSACTION_COST_SERVICE_ID . "'
                        WHERE pattotal_receiptdate BETWEEN '$fromDate' AND '$toDate'
                            AND pattotal_status = '1'
                            $addQuery
                        ORDER BY pattotal_receiptdate DESC, lc.lang1, staffName DESC";*/
            /*$query = "SELECT pt.*, CONCAT_WS(' ', reg_surname, reg_othernames) 'patName', reg.reg_hospital_no 'patNo',
                            CONCAT_WS(' ', s.staff_surname, s.staff_othernames) 'staffName',
							DATE_FORMAT (pattotal_receiptdate, '%M %d, %Y') 'transDate', lc.lang1 'dept',
                            IFNULL(pti.patitem_amount, 0) 'transCost'
                        FROM pat_transtotal pt
                            LEFT JOIN registry reg ON pt.reg_hospital_no = reg.reg_hospital_no
                            LEFT JOIN staff s ON s.user_id = pt.user_id
                            LEFT JOIN department d ON pt.dept_id = d.dept_id
                            LEFT JOIN language_content lc ON d.langcont_id = lc.langcont_id
                         	LEFT JOIN pat_transitem pti ON pt.pattotal_id = pti.pattotal_id AND pti.patservice_id = '" . TRANSACTION_COST_SERVICE_ID . "'
                        WHERE pattotal_receiptdate BETWEEN '$fromDate' AND '$toDate'
                            AND pattotal_status = '1'
                            $addQuery
                        ORDER BY pattotal_receiptdate DESC, lc.lang1, staffName DESC";*/
            //echo ("<pre>$query</pre>");
            $result = $conn->execute($query);
            if ($conn->hasRows($result)){
                $title = "TRANSACTION REPORTS FROM '$fromDate' TO '$toDate'";
                $display = "<div id=\"resultContainer\">
                            <h2>TRANSACTION REPORTS FROM <strong style=\"color: #999\">'$fromDate'</strong> TO <strong style=\"color: #999\">'$toDate'</strong></h2>
                            <div>&nbsp;</div>
                            <table cellspacing=\"3\" cellpadding=\"3\" style=\"border-collapse: collapse; border-color: #CCC;\" border=\"1\" id=\"reportTable\">
                                <tr class=\"title-row\">
                                    <td><strong>S/NO</strong></td>
                                    <td><strong>AMOUNT PAID (&#8358;)</strong></td>
                                    <td><strong>TRANSACTION COST (&#8358;)</strong></td>
                                    <td><strong>BALANCE (&#8358;)</strong></td>
                                    <td><strong>COLLECTED BY</strong></td>
                                    <td colspan=\"2\"><strong>COLLECTED FROM</strong></td>
                                    <td><strong>DEPARTMENT</strong></td>
                                    <td><strong>PAYMENT MODE</strong></td>
									<td><strong>DATE PAID</strong></td>
									<td><strong>RECEIPT</strong></td>
                                </tr>
                                <tr>
                                    <td colspan=\"5\">&nbsp;</td>
                                    <td><em>NAME</em></td>
									<td><em>HOSPITAL NO</em></td>
                                    <td colspan=\"4\">&nbsp;</td>

                                </tr>";
                $sNo = $total = $tCostTotal = $balTotal = 0;
                $juggleRows = true;
                $rowClass1 = "tr-row";
                $rowClass2 = "tr-row2";
                while ($row = mysql_fetch_array ($result, MYSQL_ASSOC)){
					//die('<pre>' . print_r($row, true));
                    //Amounts to be shown
                    $amount = $row["pattotal_totalamt"];
                    $tCost = $row["transCost"];
                    $balance = $amount - $tCost;

                    //Total of the amounts above
                    $total += $amount;
                    $tCostTotal += $tCost;
                    $balTotal += $balance;

                    //This row
                    $rowClass = $juggleRows ? $rowClass1 : $rowClass2;
                    $juggleRows = !$juggleRows;
					//window.open("index.php?p=print-invoice&no=175", "printInvoice", "status,menubar,height=400,width=350");
					$print_receipt_btn = '<button class="btn" type="button" onclick="showReceipt(' . $row['pattotal_id'] . ');">Print</button>';
					//$print_receipt_link = '<a href="index.php?p=print-invoice&no=' . $row['pattotal_id'] . '" target="_blank" >Print Receipt</a>';
                    $display .= "<tr class=\"$rowClass\">
                                    <td align=\"left\">" . (++$sNo) . ".</td>
                                    <td align=\"right\">" . number_format($amount, 2) . "</td>
                                    <td align=\"right\">" . number_format($tCost, 2) . "</td>
                                    <td align=\"right\">" . number_format($balance, 2) . "</td>
                                    <td align=\"left\">" . $row["staffName"] . "</td>
                                    <td align=\"left\">" . $row["patName"] . "</td>
									<td align=\"left\">" . $row["patNo"] . "</td>
                                    <td align=\"left\">" . $row["dept"] . "</td>
                                    <td align=\"left\">" . $row["pattotal_paymthd"] . "</td>
									<td align=\"left\">" . $row["transDate"] . "</td>
									<td>" . $print_receipt_btn . "</td>
                                 </tr>";
                }   //END while
                $display .= "   <tr class=\"title-row\">
                                    <td align=\"left\"><h2>TOTAL:</h2></td>
                                    <td align=\"right\"><h2>" . number_format($total, 2) . "</h2></td>
                                    <td align=\"right\"><h2>" . number_format($tCostTotal, 2) . "</h2></td>
                                    <td align=\"right\"><h2>" . number_format($balTotal, 2) . "</h2></td>
                                    <td colspan=\"7\">&nbsp;</td>
                                 </tr>
                             </table>
                             </div>";
                $display .=  "<form method=\"post\" id=\"xc\" name=\"xform\" target=\"_blank\" action=\"export.php?{$_SERVER['QUERY_STRING']}&action=excel\">
                                    <input type=\"hidden\" name=\"t\" value='$title' />
                                    <input type=\"hidden\" name=\"v\" value='1' />
                                    <input type=\"hidden\" name=\"h\" value='1' />
                                </form>";
            } else $display = "<div><strong>No matching results were found</strong></div>";
            echo $display;
        }
	}
?>
</div>
<style type="text/css">
    strong, em      { text-align:  center; }
</style>
<link type="text/css" href="library/admin_jquery/themes/base/ui.datepicker.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/base/ui.base.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/base/ui.theme.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript">

	//window.open("index.php?p=print-invoice&no=175", "printInvoice", "status,menubar,height=400,width=350");
	function showReceipt(id){
		window.open("index.php?p=print-invoice&no=" + id, "printInvoice" + id, "status,menubar,height=400,width=350");
	}


	$(function() {
		$('#fromdate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});

		$('#todate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});
	});
</script>