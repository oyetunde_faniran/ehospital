<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<h4><?php echo $report_bedstateward_header; ?></h4>
<div>&nbsp;</div><div>&nbsp;</div>
<div>
    <form action="index.php?<?php echo $_SERVER['QUERY_STRING']; ?>" method="post" name="filterform" id="filterform">
       	<script language="javascript" type="text/javascript" src="library/calendar/jcalendar.js"></script>
            <table border="0">
              <tr>
                <td><strong><?php echo $report_bedstateward_ward; ?></strong></td>
                <td><strong><?php echo $report_bedstateward_story1; ?></strong></td>
              </tr>
              <tr>
                <td>
                	<select name="ward" id="ward" onchange="this.form.w.value = this.options[this.selectedIndex].text;">
<?php
	$common = new report_common();
	$query = "SELECT lc_ward.$curLangField ward, lc_clinic.$curLangField clinic, w.ward_id 
				FROM language_content lc_ward INNER JOIN wards w INNER JOIN clinic c INNER JOIN language_content lc_clinic 
				ON w.langcont_id = lc_ward.langcont_id AND w.clinic_id = c.clinic_id 
					AND c.langcont_id = lc_clinic.langcont_id
				ORDER BY clinic, ward";
	echo $common->getAllWards4DropDown($query, "ward");
?>
                    </select>
					<input type="hidden" name="w" value="" />
                    <script language="javascript" type="text/javascript">
						disForm = document.getElementById("filterform");
						if (disForm && disForm.ward){
							disForm.w.value = disForm.ward.options[disForm.ward.selectedIndex].text;
						}
					</script>
                </td>
                <td>
                    <div>
<?php
//	$datePicker = new report_datepickerinit();
//	echo $datePicker->initField("date");
?>
                        <input name="date" maxlength="10" size="10" type="text" id="date" 
<?php
	if (!$_POST){
		echo "value=\"" . date("Y-m-d") . "\"";
	} else echo "value=\"{$_POST['date']}\"";
?> 
                        /> <?php echo $report_bedstateward_dateformat; ?> 
                        <a href="javascript: openCalendar('filterform', 'date', 'date');"><?php echo $report_bedstateward_insertdate; ?></a>
                    </div>
                </td>
              </tr>
              <tr>
                <td><input type="submit" name="sButton" value="<?php echo $report_show; ?>" /></td>
                <td>&nbsp;</td>
              </tr>
            </table>
    </form>

<?php
	if ($_POST){
		echo "<div>&nbsp;</div><hr /><div>&nbsp;</div>";
		echo sprintf("<p>$report_bedstateward_story2</p>", $_POST['date'], $_POST['w']);
		$s = "";
		$bedstate = new report_bedstateward();

		//Clean up G.P.C. variables
		$wardid = mysql_real_escape_string($_POST["ward"], $bedstate->conn->getConnectionID());
		$date = mysql_real_escape_string($_POST["date"], $bedstate->conn->getConnectionID());


		//Get the details of patients on ADMISSION
		$query = "SELECT reg.reg_hospital_no AS '$report_bedstateward_no', 
						reg.reg_surname AS '$report_bedstateward_sname', 
						reg.reg_othernames AS '$report_bedstateward_onames', 
						( 
							CASE reg.reg_gender 
								WHEN '0' THEN 'Male' 
								WHEN '1' THEN 'Female' 
							END 
						) AS '$report_bedstateward_sex', 
						get_age(DATE(reg.reg_dob)) AS '$report_bedstateward_age', 
						 CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) '$report_bedstateward_cons' 
					FROM inpatient_admission inp 
						INNER JOIN patient_admission pat 
						INNER JOIN registry reg 
						INNER JOIN consultant c 
						INNER JOIN staff s 
					ON inp.patadm_id = pat.patadm_id 
						AND pat.reg_hospital_no = reg.reg_hospital_no 
						AND inp.consultant_id = c.consultant_id 
						AND c.staff_employee_id = s.staff_employee_id 
					WHERE inp.ward_id = '$wardid' AND inp.inp_dateattended LIKE '$date%'";

		$result = $bedstate->getResult($query);
		$s .= "<p><strong>$report_bedstateward_adm</strong></p>";
		if ($result !== false)
			$s .= $result;
		else $s .= "<div>$report_bedstateward_error_noadmission</div>";


		//Get the ct_id from conditiontype table that indicates that a patient is dead
		$condQuery = "SELECT ct.ct_id 
						FROM conditiontype ct INNER JOIN language_content lc
						ON ct.langcont_id = lc.langcont_id
						WHERE LOWER(lc.lang1) = 'dead'";
		$result = $bedstate->conn->execute($condQuery);
		if ($result && mysql_affected_rows($bedstate->conn->getConnectionID()) == 1){
			$row = mysql_fetch_row ($result);
			$dead = $row[0];
		} else $dead = 0;


		//Get the details of patients that were DISCHARGED
		//This query is common to both DISCHARGES & DEATHS
		$query = "SELECT reg.reg_hospital_no no, 
						reg.reg_surname sname, 
						reg.reg_othernames onames,
						( 
							CASE reg.reg_gender 
								WHEN '0' THEN 'Male' 
								WHEN '1' THEN 'Female' 
							END 
						) sex, 
						get_age(DATE(reg.reg_dob)) age, 
						 CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) doctor, 
						 inp.patadm_id patid
					FROM inpatient_admission inp 
						INNER JOIN patient_admission pat 
						INNER JOIN registry reg 
						INNER JOIN consultant c 
						INNER JOIN staff s 
					ON inp.patadm_id = pat.patadm_id 
						AND pat.reg_hospital_no = reg.reg_hospital_no 
						AND inp.consultant_id = c.consultant_id 
						AND c.staff_employee_id = s.staff_employee_id";
		$disQuery = $query . " WHERE inp.ward_id = '$wardid' AND inp.inp_datedischarged LIKE '$date%' AND inp.ct_id <> '$dead'";
		$s .= "<p>&nbsp;</p><p><strong>$report_bedstateward_discharges</strong></p>";
		$result = $bedstate->getDischarges($disQuery);
		if ($result !== false){
			$s .= "<table border=\"1\">
					<thead>
						<th>$report_bedstateward_sno</th>
						<th>$report_bedstateward_no</th>
						<th>$report_bedstateward_sname</th>
						<th>$report_bedstateward_onames</th>
						<th>$report_bedstateward_sex</th>
						<th>$report_bedstateward_age</th>
						<th>$report_bedstateward_diagnosis</th>
						<th>$report_bedstateward_cons</th>
					</thead>" . $result . "</table>";
		} else $s .= "<div>$report_bedstateward_error_nodischarges</div>";


		//Get the list of patients that DIED on the chosen date
		$disQuery = $query . " WHERE inp.ward_id = '$wardid' AND inp.inp_datedischarged LIKE '$date%' AND inp.ct_id = '$dead'";
		$s .= "<p>&nbsp;</p><p><strong>$report_bedstateward_deaths</strong></p>";
		$result = $bedstate->getDischarges($disQuery);
		if ($result !== false){
			$s .= "<table border=\"1\">
					<thead>
						<th>$report_bedstateward_sno</th>
						<th>$report_bedstateward_no</th>
						<th>$report_bedstateward_sname</th>
						<th>$report_bedstateward_onames</th>
						<th>$report_bedstateward_sex</th>
						<th>$report_bedstateward_age</th>
						<th>$report_bedstateward_diagnosis</th>
						<th>$report_bedstateward_cons</th>
					</thead>" . $result . "</table>";
		} else $s .= "<div>$report_bedstateward_error_nodeath</div>";
		echo ($s);
	}
?>
</div>