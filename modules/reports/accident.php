<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<h4><?php echo $report_accident_header; ?></h4>

<div>
	<div>&nbsp;</div><div>&nbsp;</div>
        <form action="index.php?<?php echo $_SERVER['QUERY_STRING']; ?>" method="post" name="filterform" id="filterform">
        	<p><?php echo $report_accident_story1; ?></p>

			<table border="0" cellpadding="5">
              <tr>
                <th align="left"><?php echo $report_accident_form_patient; ?>:</th>
                <td align="left">

                    <table border="0" cellpadding="5">
                      <tr>
                        <td align="left">
                        	<label for="numbersearch">
                            	<input name="searchtype" id="numbersearch" type="radio" value="number" 
<?php
	if ($_POST && isset($_POST["searchtype"]) && $_POST["searchtype"] == "number")
		echo "checked=\"checked\"";
	else echo "checked=\"checked\"";
?>
                                 onclick="this.form.patno.disabled = false; this.form.patient.disabled = true;" /> 
                                <?php echo $report_accident_searchbynumber; ?>
                            </label>
                        </td>

                        <td align="left">
        					<input name="patno" id="patno" type="text" size="6" 
<?php
	if ($_POST && isset($_POST["patno"]))
		echo "value=\"{$_POST['patno']}\"";
?>
                             maxlength="10" />
                        </td>
                      </tr>

                      <tr>
                        <td align="left">
                        	<label for="namesearch">
                            	<input name="searchtype" id="namesearch" type="radio"  value="name" onclick="this.form.patient.disabled = false; this.form.patno.disabled = true;" /> 
                                <?php echo $report_accident_searchbyname; ?>
                            </label>
                        </td>

                        <td align="left">
        					<select name="patient" id="patient" disabled="disabled">
                                <?php
                                    $acc = new report_accident();
                                    echo $acc->getPatients();
                                ?>
                            </select>
                            <input type="hidden" name="w" value="" />
                            <script language="javascript" type="text/javascript">
                                disForm = document.getElementById("filterform");
                                if (disForm){
                                    disForm.w.value = disForm.patient.options[disForm.nurse.selectedIndex].text;
                                }
                            </script>
                        </td>
                      </tr>

                    </table>

                </td>
              </tr>

              <tr>
                <th align="left"><?php echo $report_accident_form_fromdatefield; ?>:</th>
                <td align="left">
                	<select name="fromdate" id="fromdate" onchange="if (this.options[this.selectedIndex].text == 'All') document.getElementById('todate').selectedIndex = this.selectedIndex;">
						<?php echo $acc->getDates("fromdate"); ?>
                    </select>
                </td>
              </tr>

			  <tr>
                <th align="left"><?php echo $report_accident_form_todatefield; ?>:</th>
                <td align="left">
                	<select name="todate" id="todate" onchange="if (this.options[this.selectedIndex].text == 'All') document.getElementById('fromdate').selectedIndex = this.selectedIndex;">
						<?php echo $acc->getDates("todate"); ?>
                    </select>
                </td>
              </tr>
              
              <tr>
              	<td>&nbsp;</td>
                <td align="left"><input type="submit" value="<?php echo $report_show; ?>" name="sButon" /></td>
              </tr>

            </table>
        </form>
</div>
    <div>
<?php
	if ($_POST){
		echo "<div>&nbsp;</div><hr /><div>&nbsp;</div>";

		//The reg number of the patient to search for
		switch ($_POST["searchtype"]){
			case "number":	$id = (int)$_POST["patno"];
							break;
			case "name":	$id = (int)$_POST["patient"];
							break;
			default:		$id = 0;
		}

		//Get the patient's bio
		$patient = $acc->getPatient($id);
		if ($patient !== false){
			echo "<table border=\"0\">
						<tr>
							<th align=\"left\">$report_accident_tableheader_surname</th>
							<td>{$patient['surname']}</td>
						</tr>\n
						<tr>
							<th align=\"left\">$report_accident_tableheader_othernames</th>
							<td>{$patient['othernames']}</td>
						</tr>\n
						<tr>
							<th align=\"left\">$report_accident_tableheader_sex</th>
							<td>{$patient['sex']}</td>
						</tr>\n
						<tr>
							<th align=\"left\">$report_accident_tableheader_age</th>
							<td>{$patient['age']}</td>
						</tr>\n
						<tr>
							<th align=\"left\">$report_accident_tableheader_address</th>
							<td>{$patient['address']}</td>
						</tr>\n</table><div>&nbsp;</div>";
		}

		//Get Emergency Deets
		$deets = $acc->getEmergencyDeets($id);
		if ($deets !== false){
			//Show report & title
			echo "<table border=\"1\">
						<tr>
							<th>$report_accident_tableheader_date</th>
							<th>$report_accident_tableheader_deets</th>
							<th>$report_accident_tableheader_treatment</th>
						</tr>\n" . $deets . "</table>";
		} elseif ($patient === false)
				echo $report_accident_error_noresult1;	//Error
			else echo $report_accident_error_noresult2;	//Bio found, but no emergency deets for the date interval
	}
?>
    </div>