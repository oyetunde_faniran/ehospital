<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<h4><?php echo $report_morning_header; ?></h4>

<div>
	<div>&nbsp;</div><div>&nbsp;</div>
        <form action="index.php?<?php echo $_SERVER['QUERY_STRING']; ?>" method="post" name="filterform" id="filterform">
        	<p><?php echo $report_morning_story1; ?></p>
            <table border="0" cellspacing="5" cellpadding="5">

<!--BEGIN Nurse-->
              <tr>
              	<td><strong><?php echo $report_evening_form_nursefield; ?>:</strong></td>
                <td>

                	<table width="100%" border="0" cellpadding="5">
                      <tr>
                        <td>
                        	<label for="numbersearch">
                            	<input name="searchtype" id="numbersearch" type="radio" value="number" 
<?php
	if ($_POST && isset($_POST["searchtype"]) && $_POST["searchtype"] == "number")
		echo "checked=\"checked\"";
	else echo "checked=\"checked\"";
?>
                                 onclick="this.form.staffno.disabled = false; this.form.nurse.disabled = true;" /> 
                                <?php echo $report_evening_searchbynumber; ?>
                            </label>
                        </td>
                        <td><input name="staffno" id="staffno" type="text" 
<?php
	if ($_POST && isset($_POST["staffno"]))
		echo "value=\"{$_POST['staffno']}\"";
?>
                         size="6" maxlength="10" /></td>
                      </tr>
                      <tr>
                        <td>
                        	<label for="namesearch">
                            	<input name="searchtype" id="namesearch" type="radio" value="name" onclick="this.form.staffno.disabled = true; this.form.nurse.disabled = false;" /> 
                                <?php echo $report_evening_searchbyname; ?>
                            </label>
                        </td>
                        <td>
                   		  <select name="nurse" onchange="this.form.w.value = this.options[this.selectedIndex].text;" disabled="disabled">
<?php
	$eve = new report_evenmorn();
	echo $eve->getNurses();
?>
                          </select>
                            <input type="hidden" name="w" value="" />
                          <script language="javascript" type="text/javascript">
                                disForm = document.getElementById("filterform");
                                if (disForm && disForm.nurse){
                                    disForm.w.value = disForm.nurse.options[disForm.nurse.selectedIndex].text;
                                }
                            </script>
                        </td>
                      </tr>
                    </table>

                </td>
              </tr>
<!--END Nurse-->

              <tr>
                <td><strong><?php echo $report_evening_form_fromdatefield; ?>:</strong></td>
                <td>
                    <select name="fromdate" id="fromdate" onchange="if (this.options[this.selectedIndex].text == 'All') document.getElementById('todate').selectedIndex = this.selectedIndex;">
						<?php echo $eve->getDates("fromdate"); ?>
                    </select>
                </td>
              </tr>

			  <tr>
              	<td><strong><?php echo $report_evening_form_todatefield; ?>:</strong></td>
                <td>
                    <select name="todate" id="todate" onchange="if (this.options[this.selectedIndex].text == 'All') document.getElementById('fromdate').selectedIndex = this.selectedIndex;">
						<?php echo $eve->getDates("todate"); ?>
                    </select>
                </td>
              </tr>

              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" value="<?php echo $report_show; ?>" name="sButon" /></td>
              </tr>
            </table>
        </form>
</div>
	
    <div>
<?php
	if ($_POST){
		echo "<div>&nbsp;</div><hr /><div>&nbsp;</div>";
		$s = $eve->getResults(0);
		if (!$s === false){

			//Form the report title
			//nurse
			$top = "<tr align=\"center\"><td colspan=\"9\"><strong>$report_evening_title";
			if (isset($_POST["nurse"])){
				if ($_POST["nurse"] != "All")
					$nurse = !empty($_POST["w"]) ? $_POST["w"] : $eve->getNurseName();
				else $nurse = $report_evening_allnurses;
			} else $nurse = $eve->getNurseName();
			$top .= !empty($nurse) ? $nurse : $report_evening_unknown;

			//date
			if ($_POST["fromdate"] == "All" || $_POST["todate"] == "All")
				$top .= " ($report_evening_daterange: $report_evening_alltime)";	//DATE RANGE: All Time
			else $top .= " ($report_evening_daterange: {$_POST['fromdate']} - {$_POST['todate']})";
			$top .= "</strong></td></tr>";

			//Show report & title
			echo "<table border=\"1\">$top
						<tr>
							<th>$report_evening_sno</th>
							<th>$report_evening_tableheader_name</th>
							<th>$report_evening_tableheader_age</th>
							<th>$report_evening_tableheader_bp</th>
							<th>$report_evening_tableheader_temp</th>
							<th>$report_evening_tableheader_pulse</th>
							<th>$report_evening_tableheader_resp</th>
							<th>$report_evening_tableheader_cond</th>
							<th>$report_evening_tableheader_diag</th>
						</tr>\n" . $s . "</table>";
		} else echo $report_evening_error_noresult;
	}
?>
    </div>
