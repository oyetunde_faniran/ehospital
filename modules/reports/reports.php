<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<h3><?php echo $report_general_header; ?></h3>
<div><?php echo $report_general_story; ?></div>
<div id="navlist">
	<div>&nbsp;</div>
	<div><a href="index.php?p=eveningreport&m=reports"><?php echo $report_evening_report; ?></a></div>
	<div><a href="index.php?p=morningreport&m=reports"><?php echo $report_morning_report; ?></a></div>
	<div><a href="index.php?p=admissionbook&m=reports"><?php echo $report_adm_book; ?></a></div>
	<!--<div><a href="index.php?p=accountbook&m=reports"><?php //echo $report_account_book; ?></a></div>-->
	<div><a href="index.php?p=bedstatewards&m=reports"><?php echo $report_bedstatewards; ?></a></div>
	<div><a href="index.php?p=bedstateward&m=reports"><?php echo $report_bedstate; ?></a></div>
	<div><a href="index.php?p=dailybed&m=reports"><?php echo $report_dailybed_statements; ?></a></div>
	<div><a href="index.php?p=appointment&m=reports"><?php echo $report_appointment_listing; ?></a></div>
	<div><a href="index.php?p=accident&m=reports"><?php echo $report_accident; ?></a></div>
	<!--<div><a href="index.php?p=patientsummaryward&m=reports"><?php //echo $report_patient_summary_wards; ?></a></div>-->
	<div><a href="index.php?p=outpatmonthlyatt&m=reports"><?php echo $report_monthly_attendances_outpatient; ?></a></div>
	<div><a href="index.php?p=monthlystats&m=reports"><?php echo $report_monthlystats; ?> &raquo;</a></div>
</div>