<?php
//Get needed vars for search
	if ($_POST){
		$showReport = true;
		$ddate = isset($_POST["ddate"]) ? $_POST["ddate"] : "";
		if (preg_match("/^[0-9]{4,4}[-][0-9]{2,2}$/", $ddate)){
			$ddate = explode("-", $ddate);
			$year = $ddate[0];
			$month = $ddate[1];
		} else {
				$year = date("Y");
				$month = (int)date("m");
		}
		$fromDate = "$year-" . str_pad($month, 2, "0", STR_PAD_LEFT) . "-01 00:00:00";
		$toDate = "$year-" . str_pad($month, 2, "0", STR_PAD_LEFT) . "-31 23:59:59";
	} elseif (isset($_GET["ddate"])) {
			$showReport = true;
			$ddate = $_GET["ddate"];
			if (preg_match("/^[0-9]{4,4}[-][0-9]{2,2}$/", $ddate)){
				$ddate = explode("-", $ddate);
				$year = $ddate[0];
				$month = $ddate[1];
			} else {
				$year = date("Y");
				$month = (int)date("m");
			}
			$fromDate = "$year-" . str_pad($month, 2, "0", STR_PAD_LEFT) . "-01 00:00:00";
			$toDate = "$year-" . str_pad($month, 2, "0", STR_PAD_LEFT) . "-31 23:59:59";
		} else {
				$showReport = false;
				$fromDate = "";
				$toDate = "";
				$month = "";
				$year = "";
			}

	//Format the date to be displayed like this January, 2009
	$displayDate = date("F, Y", strtotime($fromDate));
?>

<div>
<?php
	echo "<h4>$report_selectDate</h4>";
?>
	   <form action="" method="post" name="monthlystatsform">
        <table>
        	<tr>
            	<td><label for="ddate"><strong><?php echo $report_date; ?>:</strong></label></td>
				<td><input type="text" name="ddate" 
<?php
	if ($_POST){
		if (isset($_POST["ddate"]))
			echo " value=\"{$_POST['ddate']}\"";
	} elseif (isset($_GET["ddate"]))
				echo " value=\"{$_GET['ddate']}\"";
		else echo " value=\"" . date("Y-m") . "\"";
?>
                 id="ddate" maxlength="7" size="7" /></td>
                <td><input type="submit" name="sButton" id="sButton" value="<?php echo $report_showReport; ?>" class="btn"/></td>
            </tr>
         </table>
        </form>
</div>

<link type="text/css" href="library/admin_jquery/themes/base/ui.datepicker.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/base/ui.base.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/base/ui.theme.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$('#ddate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm',
			showButtonPanel: true,
			showOn: 'button',
			buttonImage: 'images/calendar.gif',
			buttonImageOnly: true,
			onChangeMonthYear: function(year, month, inst) {
									d = document.getElementById("ddate");
									m = month.toString();
									if (m.length == 1)
										m = "0" + m;
									d.value = year + "-" + m;
								}

		});
	});
</script>