<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

//Get needed vars for search
include_once ("monthlystats_dateinit.php");

	$common  = new report_common();

	if ($showReport){
		echo "<div>&nbsp;</div>
				<div><hr /></div>
				<div>&nbsp;</div>";
		$title = "<h2>$report_monthlystats_inpatattendance_age $report_monthlystats_inpsummary_for <em>$displayDate</em>.</h2>";
		include_once ("printview.php");
?>

<div id="resultContainer">
<?php
	echo $title;
?>
	<div>&nbsp;</div>
    <script type="text/javascript" src="library/table2Excel.js"></script>
    <table border="1" class="reportTables" id="reportTable">
    	<thead class="title-row">
        	<th><?php echo $report_monthlystats_sno; ?></th>
        	<th><?php echo $report_monthlystats_dept; ?></th>
            <th colspan="2"><?php echo $report_monthlystats_inpatattendance_age_col1; ?></th>
            <th colspan="2"><?php echo $report_monthlystats_inpatattendance_age_col2; ?></th>
            <th colspan="2"><?php echo $report_monthlystats_inpatattendance_age_col3; ?></th>
            <th colspan="2"><?php echo $report_monthlystats_inpatattendance_age_col4; ?></th>
            <th colspan="2"><?php echo $report_monthlystats_inpatattendance_age_col5; ?></th>
            <th colspan="2"><?php echo $report_monthlystats_inpatattendance_age_col6; ?></th>
            <th colspan="2"><?php echo $report_monthlystats_inpatattendance_age_total; ?></th>
            <th><?php echo $report_monthlystats_inpatattendance_age_csessions; ?></th>
        </thead>

		<tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            
            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>

            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>
            
            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>
            
            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>
            
            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>
            
            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>
            
            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>
            
<!--            <td><?php //echo strtoupper($sex_male); ?></td>
            <td><?php //echo strtoupper($sex_female); ?></td>-->
        </tr>

<?php
	$dis = new report_monthlystats_inpatattendance_age($month, $year);
	$deptQuery = "SELECT dept.dept_id, lc.$curLangField dept FROM department dept 
					INNER JOIN language_content lc 
					ON lc.langcont_id = dept.langcont_id
					ORDER BY dept";
	$result = $dis->getResults($deptQuery, $report_total);
	if (!empty($result))
		echo $result;
	else echo "<tr><td colspan=\"7\">$report_monthlystats_dept_error</td></tr>";
?>
    </table>
</div>
<?php
		$exportForm =  "<form method=\"post\" id=\"xc\" name=\"xform\" target=\"_blank\" action=\"export.php?{$_SERVER['QUERY_STRING']}&action=excel\">
							<input type=\"hidden\" name=\"t\" value='$title' />
							<input type=\"hidden\" name=\"v\" value='1' />
							<input type=\"hidden\" name=\"h\" value='1' />
						</form>";
		echo $exportForm;
	}
?>