<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

//Get needed vars for search
include_once ("monthlystats_dateinit.php");

	$common  = new report_common();

	if ($showReport){
		echo "<div>&nbsp;</div>
				<div><hr /></div>
				<div>&nbsp;</div>";
		$title = "<h2>$report_monthlystats_patientsource $report_monthlystats_inpsummary_for <em>$displayDate</em>.</h2>";
		include_once ("printview.php");
?>

<script type="text/javascript" src="library/table2Excel.js"></script>
<div id="resultContainer">
<?php
	echo $title;
?>
	<div>&nbsp;</div>
    <table border="1" class="reportTables" id="reportTable">
    	<thead class="title-row">
        	<tr>
                <th><?php echo $report_monthlystats_sno; ?></th>
                <th><?php echo $report_monthlystats_ps_source; ?></th>
                <th><?php echo $report_monthlystats_ps_new; ?></th>
                <th><?php echo $report_monthlystats_ps_allpats; ?></th>
                <th><?php echo $report_monthlystats_ps_ptotal; ?></th>
			</tr>
        </thead>

		<tbody>
<?php
	$sourceQuery = "SELECT lc.$curLangField source, ref.refferer_id sourceid
					FROM patient_referrer ref INNER JOIN language_content lc
					ON lc.langcont_id = ref.langcont_id
					ORDER BY source";
	$dis = new report_monthlystats_patientsource($month, $year);
	$result = $dis->getResults($sourceQuery, $report_total);
	echo $result;
?>
        </tbody>
	</table>
</div>
<?php
		$exportForm =  "<form method=\"post\" id=\"xc\" name=\"xform\" target=\"_blank\" action=\"export.php?{$_SERVER['QUERY_STRING']}&action=excel\">
							<input type=\"hidden\" name=\"t\" value='$title' />
							<input type=\"hidden\" name=\"v\" value='1' />
							<input type=\"hidden\" name=\"h\" value='1' />
						</form>";
		echo $exportForm;
	}
?>