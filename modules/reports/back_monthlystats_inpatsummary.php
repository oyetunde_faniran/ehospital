<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
} // End of defined() IF.


//Get needed vars for search
	if ($_POST){
		$showReport = true;
		$fromDate1 = isset($_POST["fromdate"]) ? $_POST["fromdate"] : "";
		$fromDate = admin_Tools::isValidDate($fromDate1) ? $fromDate1 . " 00:00:00" : date("Y-m-d") . " 00:00:00";
		$toDate1 = isset($_POST["todate"]) ? $_POST["todate"] : "";
		$toDate = admin_Tools::isValidDate($toDate1) ? $toDate1 . " 23:59:59" : date("Y-m-d") . " 23:59:59";
	} elseif (isset($_GET["from"], $_GET["to"])) {
			$showReport = true;
			$fromDate1 = isset($_GET["from"]) ? $_GET["from"] : "";
			$fromDate = admin_Tools::isValidDate($fromDate1) ? $fromDate1 . " 00:00:00" : date("Y-m-d") . " 00:00:00";
			$toDate1 = isset($_GET["to"]) ? $_GET["to"] : "";
			$toDate = admin_Tools::isValidDate($toDate1) ? $toDate1 . " 23:59:59" : date("Y-m-d") . " 23:59:59";
		} else {
				$showReport = false;
				$fromDate1 = "";
				$toDate1 = "";
			}

//Format the dates to be displayed like this January 12, 2009
$displayFromDate = admin_Tools::formatDate($fromDate1);
$displayToDate = admin_Tools::formatDate($toDate1);

$month = !empty($_GET["month"]) ? (int)$_GET["month"] : (int)date("m");
$year = !empty($_GET["year"]) ? (int)$_GET["year"] : date("Y");
$common  = new report_common();

//$title = "<h4>$report_monthlystats_inpsummary_header $report_monthlystats_inpsummary_for " . $common->getMonth($month) . ", $year</h4>";
?>

<div>
<?php
	echo "<h4>$report_selectDate</h4>";
?>
	   <form action="" method="post" name="monthlystatsform">
        <table>
        	<tr>
            	<td><label for="fromdate"><strong><?php echo $report_fromdate; ?>:</strong></label></td>
                <td><label for="todate"><strong><?php echo $report_todate; ?>:</strong></label></td>
            </tr>
            <tr>
            	<td><input type="text" name="fromdate" 
<?php
	if ($_POST){
		if (isset($_POST["fromdate"]))
			echo " value=\"{$_POST['fromdate']}\"";
	}
?>
                 id="fromdate" maxlength="10" size="12" /></td>
                <td><input type="text" name="todate" 
<?php
	if ($_POST){
		if (isset($_POST["todate"]))
			echo " value=\"{$_POST['todate']}\"";
	}
?>
                 id="todate" maxlength="10" size="12" /></td>
                <td><input type="submit" name="sButton" id="sButton" value="<?php echo $report_showReport; ?>" class="btn"/></td>
            </tr>
         </table>
        </form>
</div>

<div><hr /></div>

<?php
	if ($showReport){
		$title = "<h2>$report_monthlystats_inpsummary_header $report_monthlystats_inpsummary_for <em>$displayFromDate</em> $report_monthlystats_inpsummary_to $displayToDate</h2>";
		echo $title;

		$month = str_pad($month, 2, '0', STR_PAD_LEFT);
		$dis = new report_monthlystats_inpatsummary($month, $year, $fromDate, $toDate);
		include_once ("printview.php");
?>


<?php
	//Get all needed vars
	$avgDailyBeds = $dis->getAvgDailyBedsAvailable();
	$occupied = $dis->getAvgDailyBedsOccupied();
	$avgStayLength = round($dis->getAvgStayLength(), 2);
	$occuPerc = round($dis->getOccuPerc($occupied), 2) . "%";
	$monthAdms = $dis->getMonthAdmission();
	$monthDischarges = $dis->getMonthDischarges($curLangField);
	$monthDeaths = $dis->getMonthDeaths();
	$patsMonthBegin = $dis->getPatsMonthBegin();
	$patsMonthEnd = $dis->getPatsMonthEnd();

	$adms = $dis->getAdmissions();
	$deaths = $dis->getDeaths();
	$discharges = $dis->getDischarges();


	$result = "<p>
				<table border=\"1\" style=\"border-color: #999999;\">
				<tr class=\"tr-row\">
					<td><strong>" . $report_monthlystats_avgdailybeds . ":</strong></td>
					<td width=\"20\" align=\"center\">" . $avgDailyBeds . "</td>
				</tr>
			
				<tr class=\"tr-row2\">
					<td><strong>" . $report_monthlystats_avgdailybedsoccupied . ":</strong></td>
					<td align=\"center\">" . $occupied .  "</td>
				</tr>
			
				<tr class=\"tr-row\">
					<td><strong>" . $report_monthlystats_avglengthofdays . ":</strong></td>
					<td align=\"center\">" . $avgStayLength . "</td>
				</tr>
			
				<tr class=\"tr-row2\">
					<td><strong>" . $report_monthlystats_occuperc . ":</strong></td>
					<td align=\"center\">" . $occuPerc . "</td>
				</tr>
			
				<tr class=\"tr-row\">
					<td><strong>" . $report_monthlystats_monthadmission . ":</strong></td>
					<td align=\"center\">" . $monthAdms . "</td>
				</tr>
			
				<tr class=\"tr-row2\">
					<td><strong>" . $report_monthlystats_monthdischarges . ":</strong></td>
					<td align=\"center\">" . $monthDischarges . "</td>
				</tr>
			
				<tr class=\"tr-row\">
					<td><strong>" . $report_monthlystats_monthdeaths . ":</strong></td>
					<td align=\"center\">" . $monthDeaths . "</td>
				</tr>
			
				<tr class=\"tr-row2\">
					<td><strong>" . $report_monthlystats_patsmonthbegin . ":</strong></td>
					<td align=\"center\">" . $patsMonthBegin . "</td>
				</tr>
			
				<tr class=\"tr-row\">
					<td><strong>" . $report_monthlystats_patsmonthend . ":</strong></td>
					<td align=\"center\">" . $patsMonthEnd . "</td>
				</tr>
			</table>
		</p>

		<p>
		<table border=\"1\" style=\"border-color:#999999;\">
		<tr class=\"title-row\">
        	<td>&nbsp;</td>
            <th>" . $report_monthlystats_admissions . "</th>
            <th>" . $report_monthlystats_discharges . "</th>
            <th>" . $report_monthlystats_deaths . "</th>
        </tr>

		<tr class=\"tr-row\">
        	<th>" . $report_monthlystats_male . "</th>
            <td>" . $adms["0"] . "</td>
            <td>" . $discharges["0"] . "</td>
            <td>" . $deaths["0"] . "</td>
        </tr>

		<tr class=\"tr-row2\">
        	<th>" . $report_monthlystats_female . "</th>
            <td>" . $adms["1"] . "</td>
            <td>" . $discharges["1"] . "</td>
            <td>" . $deaths["1"] . "</td>
        </tr>

		<tr class=\"tr-row\">
        	<th>" . $report_monthlystats_total . "</th>
            <td>" . $adms["total"] . "</td>
            <td>" . $discharges["total"] . "</td>
            <td>" . $deaths["total"] . "</td>
        </tr>
    </table>
	</p>";

	//Form all the needed vars for all forms of export
	$contents = array (
					array ($report_monthlystats_avgdailybeds, $avgDailyBeds), 
					array ($report_monthlystats_avgdailybedsoccupied, $occupied), 
					array ($report_monthlystats_avglengthofdays, $avgStayLength), 
					array ($report_monthlystats_occuperc, $occuPerc), 
					array ($report_monthlystats_monthadmission, $monthAdms), 
					array ($report_monthlystats_monthdischarges, $monthDischarges), 
					array ($report_monthlystats_monthdeaths, $monthDeaths), 
					array ($report_monthlystats_patsmonthbegin, $patsMonthBegin), 
					array ($report_monthlystats_patsmonthend, $patsMonthEnd),
					array ("", ""),
					array ("", ""),
					array ("", $report_monthlystats_admissions, $report_monthlystats_discharges, $report_monthlystats_deaths),
					array ($report_monthlystats_male, $adms["0"], $discharges["0"], $deaths["0"]),
					array ($report_monthlystats_female, $adms["1"], $discharges["1"], $deaths["1"]),
					array ($report_monthlystats_total, $adms["total"], $discharges["total"], $deaths["total"]),
				);
	$options = array (
					"rowHeader" => true,
					"colHeader" => false,
				);
	$contentArray = array (
						"contents" => $contents,
						"options" => $options
					);
	$barChart = array();

	//Store in a session variable for accessibility on the export page
	$_SESSION[session_id() . "_export"] = array (
												"title"		=> $title,			//Report title
												"print"		=> $result,			//Content for print view
												"others"	=> $contentArray,	//Array of values to be used to export to pdf & excel
												"barChart"	=> $barChart		//Array of values for barcharts and pie charts
											);
	echo $result;
?>
<?php
	}
//	include_once ("modules/reports/back2stats.php");
?>
<link type="text/css" href="library/admin_jquery/themes/base/ui.datepicker.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/base/ui.base.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/base/ui.theme.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$('#fromdate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});
	});

	$(function() {
		$('#todate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});
	});
</script>