<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<h4><?php echo $report_appointment_header; ?></h4>

<div>
	<div>&nbsp;</div><div>&nbsp;</div>
        <form action="index.php?<?php echo $_SERVER['QUERY_STRING']; ?>" method="post" name="filterform" id="filterform">
        	<p><?php echo $report_appointment_story1; ?></p>
            <table width="200" border="0" cellspacing="5" cellpadding="5">
              <tr>
                <td><strong><?php echo $report_appointment_form_deptfield; ?></strong></td>
                <td><strong><?php echo $report_appointment_form_doctorfield; ?></strong></td>
                <td><strong><?php echo $report_appointment_form_fromdatefield; ?></strong></td>
                <td><strong><?php echo $report_appointment_form_todatefield; ?></strong></td>
                <td>&nbsp;</td>
              </tr>


              <tr>
                <td>
                    <select name="dept" onchange="this.form.w.value = this.options[this.selectedIndex].text;">
<?php
	$eve = new report_appointment();
	$query = "SELECT dept_id, lc.$curLangField dept_name 
				FROM department dept INNER JOIN language_content lc
				ON dept.langcont_id = lc.langcont_id";
	echo $eve->getDepts($query);
?>
                    </select>
                    <input type="hidden" name="w" value="" />
                    <script language="javascript" type="text/javascript">
						disForm = document.getElementById("filterform");
						if (disForm){
							disForm.w.value = disForm.dept.options[disForm.dept.selectedIndex].text;
						}
					</script>
                </td>
                <td>
                    <select name="doctor" onchange="this.form.w.value = this.options[this.selectedIndex].text;">
<?php
	echo $eve->getDoctors4DropDown();
?>
                    </select>
                </td>
                <td>
                    <select name="fromdate" id="fromdate" onchange="if (this.options[this.selectedIndex].text == 'All') document.getElementById('todate').selectedIndex = this.selectedIndex;">
						<?php echo $eve->getDates("fromdate"); ?>
                    </select>
                </td>
                <td>
                    <select name="todate" id="todate" onchange="if (this.options[this.selectedIndex].text == 'All') document.getElementById('fromdate').selectedIndex = this.selectedIndex;">
						<?php echo $eve->getDates("todate"); ?>
                    </select>
                </td>
                <td><input type="submit" value="<?php echo $report_show; ?>" name="sButon" /></td>
              </tr>
            </table>
        </form>
</div>
	<div>&nbsp;</div><div>&nbsp;</div>
        <div>
<?php
	if ($_POST && isset($_POST["dept"], $_POST["doctor"], $_POST["fromdate"], $_POST["todate"])){
//Form the query
		echo "<div>&nbsp;</div><hr /><div>&nbsp;</div>";
/*		$query = "SELECT DATE(app.app_starttime) AS '$report_appointment_date',
							reg.reg_hospital_no AS '$report_appointment_patientcode',
							CONCAT_WS(' ', UPPER(reg.reg_surname), LOWER(reg.reg_othernames)) AS '$report_appointment_patientname',
							(
								CASE reg.reg_gender
									WHEN '0' THEN '$sex_male'
									WHEN '1' THEN '$sex_female'
								END
							) AS '$report_appointment_sex',
							get_age (reg.reg_dob) AS '$report_appointment_age',
							app.app_transno AS '$report_appointment_transcode',
							lc1.$curLangField AS '$report_appointment_apptype',
							TIME(app.app_starttime) AS '$report_appointment_starttime',
							TIME(app.app_endtime) AS '$report_appointment_endtime',
							lc2.$curLangField AS '$report_appointment_dept'
					FROM appointment app 
						INNER JOIN registry reg 
						INNER JOIN service serv 
						INNER JOIN language_content lc1
						INNER JOIN department dept 
						INNER JOIN language_content lc2 
						INNER JOIN patient_admission pat
					ON app.patadm_id = pat.patadm_id 
						AND pat.reg_hospital_no = reg.reg_hospital_no
						AND app.service_id = serv.service_id
						AND app.dept_id = dept.dept_id
						AND serv.langcont_id = lc1.langcont_id
						AND dept.langcont_id = lc2.langcont_id
					WHERE true ";*/
		$query = "SELECT DATE(app.app_starttime) AS '$report_appointment_date',
							reg.reg_hospital_no AS '$report_appointment_patientcode',
							CONCAT_WS(' ', UPPER(reg.reg_surname), LOWER(reg.reg_othernames)) AS '$report_appointment_patientname',
							(
								CASE reg.reg_gender
									WHEN '0' THEN '$sex_male'
									WHEN '1' THEN '$sex_female'
								END
							) AS '$report_appointment_sex',
							YEAR(CURDATE()) - YEAR(reg.reg_dob) AS '$report_appointment_age',
							TIME(app.app_starttime) AS '$report_appointment_starttime',
							TIME(app.app_endtime) AS '$report_appointment_endtime',
							lc2.$curLangField AS '$report_appointment_dept'
					FROM appointment app 
						INNER JOIN registry reg 
						INNER JOIN department dept 
						INNER JOIN language_content lc2 
						INNER JOIN patient_admission pat
					ON app.patadm_id = pat.patadm_id 
						AND pat.reg_hospital_no = reg.reg_hospital_no
						AND app.dept_id = dept.dept_id
						AND dept.langcont_id = lc2.langcont_id
					WHERE true ";

	//Apply filters (if any)
		//dept
		if ($_POST["dept"] != "All"){
			$dept = is_numeric($_POST["dept"]) ? $_POST["dept"] : 0;
			$query .= " AND app.dept_id = '$dept'";
		}

		//dates
		$conn = new DBConf();
		$fromdate = mysql_real_escape_string($_POST["fromdate"], $conn->getConnectionID());
		$todate = mysql_real_escape_string($_POST["todate"], $conn->getConnectionID());
		if ($_POST["fromdate"] != "All" && $_POST["todate"] != "All"){
			$fromdate = $_POST["fromdate"] == "Today" ? " DATE_FORMAT(NOW(), '%y-%m-%d 00:00:00') " : "'$fromdate'";
			$todate = $_POST["todate"] == "Today" ? " DATE_FORMAT(NOW(), '%y-%m-%d 23:59:59') " : "'$todate'";
			$query .= " AND (app.app_starttime BETWEEN $fromdate AND $todate OR app.app_endtime BETWEEN $fromdate AND $todate) ";
		}

		echo "<p><strong>$report_appointment_form_fromdatefield:</strong> {$_POST['fromdate']}</p>";
		echo "<p><strong>$report_appointment_form_todatefield:</strong> {$_POST['todate']}</p>";
		$resultFound = false;
		//Single Doctor
		if ($_POST["doctor"] != "All"){
			$doctor = is_numeric($_POST["doctor"]) ? $_POST["doctor"] : 0;
			$query .= " AND app.consultant_id = '$doctor'";
			$query .= " ORDER BY lc2.$curLangField, app.app_starttime, reg.reg_surname, reg.reg_othernames";

			$s = $eve->getResults($query);	//Get the real appointment list
			if (!$s === false){
				$query = "SELECT s.staff_employee_id id, CONCAT_WS(' ', s.staff_title, UPPER(s.staff_surname), s.staff_othernames) doctor, lc.$curLangField unit
							FROM consultant cons INNER JOIN staff s INNER JOIN appointment app INNER JOIN units u INNER JOIN language_content lc 
							ON app.consultant_id = cons.consultant_id 
								AND cons.staff_employee_id = s.staff_employee_id
								AND s.unit_id = u.unit_id 
								AND u.langcont_id = lc.langcont_id 
							WHERE app.consultant_id = '$doctor'";
				$d = $eve->getDoctorDeets($query);
				if ($d !== false){
					echo "<p><table border=\"1\"  width=\"100%\">
								<tr><td colspan=\"11\" align=\"center\"><strong>$d</strong></td></tr>" . 
								$s . 
								"</table></p>";
				} else echo "<div>$report_appointment_error_unknown_doctor</div>";
				$resultFound = true;
			}
		} else {	//All Doctors
				//Get the consultant_id of all the doctors
				$ids = $eve->getDoctors();
				if ($ids !== false){
					//Fetch the list of appointment for each doctor
					foreach ($ids as $v){
						$tempQuery = $query . " AND app.consultant_id = '$v'  ORDER BY lc2.$curLangField, app.app_starttime, reg.reg_surname, reg.reg_othernames";
						$s = $eve->getResults($tempQuery);
						if (!$s === false){
							$docQuery = "SELECT s.staff_employee_id id, CONCAT_WS(' ', s.staff_title, UPPER(s.staff_surname), s.staff_othernames) doctor, lc.$curLangField unit
										FROM consultant cons INNER JOIN staff s INNER JOIN appointment app INNER JOIN units u INNER JOIN language_content lc 
										ON app.consultant_id = cons.consultant_id 
											AND cons.staff_employee_id = s.staff_employee_id
											AND s.unit_id = u.unit_id 
											AND u.langcont_id = lc.langcont_id 
										WHERE app.consultant_id = '$v'";
							$d = $eve->getDoctorDeets($docQuery);

							if ($d !== false){
								echo "<p><table width=\"100%\" border=\"1\">
											<tr><td colspan=\"11\" align=\"center\"><strong>$d</strong></td></tr>" . 
											$s . 
											"</table></p>";
							} else echo "<div>$report_appointment_error_unknown_doctor</div>";
							$resultFound = true;
						}
					}	//END foreach($ids)
				} else  echo $report_appointment_error_nodoctor;
			}	//END else part of if (doctor != all)

		if (!$resultFound)
			echo $report_appointment_error_noresult;
	}	//END if ($_POST)
?>
    </div>