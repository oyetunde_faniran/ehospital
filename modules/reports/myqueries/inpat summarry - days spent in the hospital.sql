/*
CASE 3:

Admitted as at the 1st of the month, but not discharged yet .:. 
Number of days spent in the hospital = Date Discharged - Date Admitted

*/
SELECT SUM(DATEDIFF(NOW(), '2010-01-01 00:00:00')) FROM inpatient_admission
WHERE inp_datedischarged IS NULL
	OR inp_datedischarged = '0000-00-00 00:00:00'
	AND inp_dateattended < '2010-01-01 00:00:00'



/*
CASE 1:

Admitted and discharged in the month .:. 
Number of days spent in the hospital = Date Discharged - Date Admitted

*/
#SELECT SUM(DATEDIFF(inp_datedischarged, inp_dateattended)) FROM inpatient_admission
#WHERE inp_datedischarged BETWEEN '2010-01-01 00:00:00' AND '2010-01-31 23:59:59'
#	AND inp_dateattended >= '2010-01-01 00:00:00'


/*
CASE 2:

Not Admitted in the month, but discharged in the month .:. 
Number of days spent in the hospital = Date Discharged - 1st day in the month

*/
#SELECT SUM(DATEDIFF(inp_datedischarged, '2010-01-01 00:00:00')) FROM inpatient_admission
#WHERE inp_datedischarged BETWEEN '2010-01-01 00:00:00' AND '2010-01-31 23:59:59'
#	AND inp_dateattended < '2010-01-01 00:00:00'
