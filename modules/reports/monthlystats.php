<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>

<script language="javascript" type="text/javascript">
	function viewReport_old(d){
		month = document.getElementById("month").value;
		year = document.getElementById("year").value;
		disLink = "index.php?p=" + d + "&m=reports" + "&month=" + month + "&year=" + year;
		//alert (disLink);
		location.href = disLink;
	}
	
	function viewReport(d){
		try{
			fromdate = new String(document.getElementById("ddate").value);
			if (fromdate.search(/^[0-9]{4,4}[-][0-9]{2,2}$/) == -1)
				throw new Exception;
			disLink = "index.php?p=" + d + "&m=reports" + "&ddate=" + fromdate;
			//alert (disLink);
			//location.href = disLink;
			dis = window.open(disLink, "report", "", true);
		} catch (e) {
				//alert (e.toString());
				alert ("Please, fill the 'Date' field using the format 'YYYY-MM' before clicking any report.");
			}
	}
</script>
<div><?php echo $report_monthlystats_story; ?></div>
<div id="navlist">
    <form action="" method="post" name="monthlystatsform" target="_blank">
        <table class="reportContainer">
        	<!--<tr>
            	<td>
                	<label for="ddate"><strong><?php //echo $report_date; ?>:</strong></label>
                    <input type="text" name="ddate" id="ddate" maxlength="7" size="7" value="<?php //echo date("Y-m"); ?>" />
                </td>
            </tr>-->
           <!-- <tr>
                <td colspan="2" align="left">
                	<span style="padding-right: 20px;">
                        <label for="month" style="padding-right: 10px;">MONTH: </label>
                        <select name="month" id="month">
<?php
/*	$month = date("m");
	$year = date("Y");
	$month = (int)$month;
	$common = new report_common();
	echo $common->getMonthsDropDown($month);
	$years = $common->getYearsDropDown($year);*/
?>
                        </select>
                    </span>
                    <span>
                        <label for="year" style="padding-right: 10px;">YEAR: </label>
                        <select name="year" id="year">
<?php
	//echo $years;
?>
                        </select>
                    </span>
                </td>
            </tr>-->
            <tr><td>
            	<table cellpadding="5" cellspacing="5" border="0">
<?php
	$disDate = date("Y-m");
	$tocArray = array (
					"inpatsummary" 				=> $report_monthlystats_inpsummary_header,
					"deptact" 					=> $report_monthlystats_deptact,
					"outpatattendance"			=> $report_monthlystats_outpatattendance,
					"outpatattendanceage"		=> $report_monthlystats_outpatattendance_age,
					"outpatunitbyunit"			=> $report_monthlystats_outpatunits,
					"inpatattendanceage"		=> $report_monthlystats_inpatattendance_age,
					"inpatattendanceadm"		=> $report_monthlystats_inpat_adm,
					"inpatunitbyunit"			=> $report_monthlystats_inpatunits,
					"inpatunitbyunitadm"		=> $report_monthlystats_inpatunitsadm,
					"patientsource"				=> $report_monthlystats_patientsource,
					"inoutdepts"				=> $report_monthlystats_inoutdepts
				);


	$inpArray = array (
					"inpatsummary" 				=> $report_monthlystats_inpsummary_header,
					"inpatattendanceage"		=> $report_monthlystats_inpatattendance_age,
					"inpatattendanceadm"		=> $report_monthlystats_inpat_adm,
					"inpatunitbyunit"			=> $report_monthlystats_inpatunits,
					"inpatunitbyunitadm"		=> $report_monthlystats_inpatunitsadm,
				);

	$outpArray = array (
					"outpatattendance"			=> $report_monthlystats_outpatattendance,
					"outpatattendanceage"		=> $report_monthlystats_outpatattendance_age,
					"outpatunitbyunit"			=> $report_monthlystats_outpatunits,
				);

	$othersArray = array (
					"deptact" 					=> $report_monthlystats_deptact,
					"patientsource"				=> $report_monthlystats_patientsource,
					"inoutdepts"				=> $report_monthlystats_inoutdepts
				);

/*	$counter = 0;
	foreach ($tocArray as $key=>$value){
		$counter++;
		echo "<tr><td>$counter.</td><td><a target=\"_blank\" href=\"index.php?p={$key}&m=reports&ddate=$disDate\">$value</a></td></tr>";
	}*/
?>
				</table>
    <div id="accordion">
        <h3><a href="#"><?php echo $report_monthlystats_inp; ?></a></h3>
        <div>
<?php
	$inpArray = array (
					"inpatsummary" 				=> $report_monthlystats_inpsummary_header,
					"inpatattendanceage"		=> $report_monthlystats_inpatattendance_age,
					"inpatattendanceadm"		=> $report_monthlystats_inpat_adm,
					"inpatunitbyunit"			=> $report_monthlystats_inpatunits,
					"inpatunitbyunitadm"		=> $report_monthlystats_inpatunitsadm,
				);
	foreach ($inpArray as $key=>$value){
		echo "<div><a target=\"_blank\" href=\"index.php?p={$key}&m=reports&ddate=$disDate\">$value</a></div><div>&nbsp;</div>";
	}
?>
        </div>
        <h3><a href="#"><?php echo $report_monthlystats_outp; ?></a></h3>
        <div>
<?php
	$outpArray = array (
					"outpatattendance"			=> $report_monthlystats_outpatattendance,
					"outpatattendanceage"		=> $report_monthlystats_outpatattendance_age,
					"outpatunitbyunit"			=> $report_monthlystats_outpatunits,
				);
	foreach ($outpArray as $key=>$value){
		echo "<div><a target=\"_blank\" href=\"index.php?p={$key}&m=reports&ddate=$disDate\">$value</a></div><div>&nbsp;</div>";
	}
?>
        </div>
        <h3><a href="#"><?php echo $report_monthlystats_others; ?></a></h3>
        <div>
<?php
		$othersArray = array (
					"deptact" 					=> $report_monthlystats_deptact,
					"patientsource"				=> $report_monthlystats_patientsource,
					"inoutdepts"				=> $report_monthlystats_inoutdepts
				);
	foreach ($othersArray as $key=>$value){
		echo "<div><a target=\"_blank\" href=\"index.php?p={$key}&m=reports&ddate=$disDate\">$value</a></div><div>&nbsp;</div>";
	}
?>
        </div>
    </div>

			</td></tr>

<!--            <tr><td>Out-Patient Unit-by-Unit Analysis (Surgery)</td></tr>
            <tr><td>Out-Patient Unit-by-Unit Analysis (Paediatrics)</td></tr>
            <tr><td>Out-Patient Unit-by-Unit Analysis (Dental)</td></tr>
            <tr><td>Out-Patient Unit HIV Paediatrics</td></tr>
            <tr><td>Out-Patient Unit HIV Adult</td></tr>
            <tr><td>Dental Division</td></tr>
            <tr><td>Source of New Patients</td></tr>
            <tr><td>Live Births, Still Births, NeoNatal and Maternal Deaths by Sex</td></tr>
            <tr><td>Deliveries and NeoNatal Mortality Data</td></tr>
            <tr><td>In-Patient Analysis by Sex</td></tr>
            <tr><td>In-Patient Admission By Age Group</td></tr>
            <tr><td>In-Patient Unit-by-Unit (Medicine)</td></tr>
            <tr><td>In-Patient Unit-by-Unit (Surgery)</td></tr>
            <tr><td>In-Patient Unit-by-Unit (Paediatrics)</td></tr>
            <tr><td>In-Patient (Dental Division)</td></tr>
            <tr><td>In-Patient (H.I.V. Adult)</td></tr>
            <tr><td>In-Patient (H.I.V. Children)</td></tr>
            <tr><td>A & E In-Patient Records</td></tr>
            <tr><td>Road Traffic Accident</td></tr>
            <tr><td>Other Clinics</td></tr>
            <tr><td>RadioDiagnosis</td></tr>
            <tr><td>Physiotherapy</td></tr>
            <tr><td>Dialysis Centre</td></tr>
            <tr><td>Radiotherapy Department</td></tr>
            <tr><td>Medical Social Services</td></tr>
            <tr><td>Occupational Therapy</td></tr>
            <tr><td>Diagnosis Centre</td></tr>
            <tr><td>Laboratory Services</td></tr>
            <tr><td>Clinical Pathology</td></tr>
            <tr><td>Modular Theatre</td></tr>
            <tr><td>K-Block Theatre</td></tr>
            <tr><td>Accident & Emergency Theatre</td></tr>
            <tr><td>Guiness Eye Centre Theatre</td></tr>-->
        </table>
	</form>
</div>


<link type="text/css" href="library/admin_jquery/themes/base/ui.datepicker.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/base/ui.base.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/base/ui.theme.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>

<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
<link type="text/css" href="library/admin_jquery/themes/base/ui.accordion.css" rel="stylesheet" />

<script type="text/javascript">
	$(function() {
		$('#ddate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm',
			showButtonPanel: true,
			onChangeMonthYear: function(year, month, inst) {
									d = document.getElementById("ddate");
									m = month.toString();
									if (m.length == 1)
										m = "0" + m;
									d.value = year + "-" + m;
								}
		});
	});

	$(function() {
		$("#accordion").accordion({
									  collapsible: true,
									  active: false,
									  clearStyle: true
								  });
	});
</script>