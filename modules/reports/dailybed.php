<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<h4><?php echo $report_dailybed_header; ?></h4>
<div>&nbsp;</div><div>&nbsp;</div>
<div>
    <form action="index.php?<?php echo $_SERVER['QUERY_STRING']; ?>" method="post" name="filterform" id="filterform">
       	<script language="javascript" type="text/javascript" src="library/calendar/jcalendar.js"></script>
    	<div><strong><?php echo $report_dailybed_story1; ?></strong></div>
        <div>
        	<input name="date" maxlength="10" size="10" type="text" id="date" 
<?php
if (!$_POST){
	echo "value=\"" . date("Y-m-d") . "\"";
} else echo "value=\"{$_POST['date']}\"";
?> 
            /> <?php echo $report_dailybed_dateformat; ?> 
            <a href="javascript: openCalendar('filterform', 'date', 'date');"><?php echo $report_dailybed_insertdate; ?></a>
        </div>
        <div>&nbsp;</div>
        <div><input type="submit" name="sButton" value="<?php echo $report_show; ?>" /></div>
    </form>
<?php
	if ($_POST){
		echo "<div>&nbsp;</div><hr /><div>&nbsp;</div>";
		echo "<p>$report_dailybed_story2 {$_POST['date']}</p>";
		$bedstate = new report_dailybed();
		//Construct the query to get all the wards available in the hospital
		$wardQuery = "SELECT lc_clinic.$curLangField clinic_name, lc.$curLangField ward_name, w.ward_bedspaces, w.ward_id 
						FROM wards w INNER JOIN clinic c INNER JOIN language_content lc INNER JOIN language_content lc_clinic
						ON w.clinic_id = c.clinic_id AND w.langcont_id = lc.langcont_id AND c.langcont_id = lc_clinic.langcont_id 
						ORDER BY clinic_name, lc.$curLangField";
		$s = $bedstate->getResults($_POST['date'], $wardQuery);

		if ($s !== false){
			//Show report & title
			echo "<table border=\"1\">
						<tr>
							<th>$report_dailybed_tableheader_sno</th>
							<th>$report_dailybed_tableheader_spec</th>
							<th>$report_dailybed_tableheader_wards</th>
							<th>$report_dailybed_tableheader_alloc</th>
							<th>$report_dailybed_tableheader_occ</th>
							<th>$report_dailybed_tableheader_vac</th>
						</tr>\n" . $s . "</table>";
		} else echo $report_dailybed_error_noresult . $_POST['date'];
	}
?>
</div>