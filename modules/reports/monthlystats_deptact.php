<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

//Get needed vars for search
include_once ("monthlystats_dateinit.php");

	$common  = new report_common();

	if ($showReport){
		echo "<div>&nbsp;</div>
				<div><hr /></div>
				<div>&nbsp;</div>";
		$title = "<h2>$report_monthlystats_deptact $report_monthlystats_inpsummary_for <em>$displayDate</em>.</h2>";
		include_once ("printview.php");
?>

<script type="text/javascript" src="library/table2Excel.js"></script>
<div>&nbsp;</div>

<div id="resultContainer">
<?php
	echo $title;
?>

	<div>&nbsp;</div>

<?php
	$dis = new report_monthlystats_deptact($month, $year);
	$deptQuery = "SELECT dept.dept_id, lc.$curLangField dept FROM department dept 
					INNER JOIN language_content lc 
					ON lc.langcont_id = dept.langcont_id
					WHERE dept.dept_isclinical = '1'
					ORDER BY dept";
	$result = $dis->getResults($deptQuery, $report_total, $report_gTotal);

	if (!empty($result[0])){

		//Generate the vars to be used for exporting
		$finalResult = "<table border=\"1\" style=\"border-color: #999999;\" id=\"reportTable\">
    				<thead class=\"title-row\">
						<tr>
							<th>" . $report_monthlystats_sno . "</th>
							<th>" . ucfirst(strtolower($report_monthlystats_dept)) . "</th>
							<th>" . ucfirst($report_monthlystats_deptact_bedsavail) . "</th>
							<th>" . ucfirst($report_monthlystats_deptact_avgdoccu) . "</th>
							<th>" . ucfirst($report_monthlystats_deptact_bedsvac) . "</th>
							<th>" . ucfirst($report_monthlystats_deptact_poccu) . "</th>
							<th>" . ucfirst($report_monthlystats_admissions) . "</th>
							<th>" . ucfirst($report_monthlystats_discharges) . "</th>
							<th>" . ucfirst($report_monthlystats_deaths) . "</th>
							<th>" . ucfirst($report_monthlystats_deptact_pdoveradm) . "</th>
							<th>" . ucfirst($report_monthlystats_deptact_pdovertdeaths) . "</th>
							<th>" . ucfirst($report_monthlystats_deptact_tbeddays) . "</th>
							<th>" . ucfirst($report_monthlystats_deptact_tpatdays) . "</th>
							<th>" . ucfirst($report_monthlystats_deptact_avgstaylength) . "</th>
						</tr>
					</thead>";
		$finalResult .= $result[0] . "</table>";
		echo $finalResult;
		$options = array (
						"rowHeader" => false,
						"colHeader" => true,
					);
		//Construct the array that would be used to export to excel & PDF
		$rowsArray = array(
							array ($common->reportHeader($report_monthlystats_sno),
									$common->reportHeader(ucfirst(strtolower($report_monthlystats_dept))),
									$common->reportHeader(ucfirst($report_monthlystats_deptact_bedsavail)),
									$common->reportHeader(ucfirst($report_monthlystats_deptact_avgdoccu)),
									$common->reportHeader(ucfirst($report_monthlystats_deptact_bedsvac)),
									$common->reportHeader(ucfirst($report_monthlystats_deptact_poccu)),
									$common->reportHeader(ucfirst($report_monthlystats_admissions)),
									$common->reportHeader(ucfirst($report_monthlystats_discharges)),
									$common->reportHeader(ucfirst($report_monthlystats_deaths)),
									$common->reportHeader(ucfirst($report_monthlystats_deptact_pdoveradm)),
									$common->reportHeader(ucfirst($report_monthlystats_deptact_pdovertdeaths)),
									$common->reportHeader(ucfirst($report_monthlystats_deptact_tbeddays)),
									$common->reportHeader(ucfirst($report_monthlystats_deptact_tpatdays)),
									$common->reportHeader(ucfirst($report_monthlystats_deptact_avgstaylength))
							)
						);
		foreach ($result[1] as $value)
			array_push ($rowsArray, $value);
		$contentArray = array (
							"contents" => $rowsArray,
							"options" => $options
						);

		$finalArray = array (
							"title"		=> $title,			//Report title
							"others"	=> $contentArray	//Array of values to be used to export to pdf & excel
						);
		$exportForm =  "<form method=\"post\" id=\"xpform\" name=\"xpform\" target=\"_blank\" action=\"export.php?{$_SERVER['QUERY_STRING']}&action=excel\">
							<input type=\"hidden\" name=\"xp\" value='" . base64_encode(serialize($finalArray)) . "' />
						</form>";
		$exportForm =  "<form method=\"post\" id=\"xc\" name=\"xform\" target=\"_blank\" action=\"export.php?{$_SERVER['QUERY_STRING']}&action=excel\">
							<input type=\"hidden\" name=\"t\" value='$title' />
							<input type=\"hidden\" name=\"v\" value='1' />
							<input type=\"hidden\" name=\"h\" value='1' />
						</form>";
	} else {
			echo "<p>$report_noData</p>";
			$exportForm = "";
	}	//END if result is not empty

?>

</div>
<?php
		echo $exportForm;
	}
?>