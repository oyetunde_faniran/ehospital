<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\"
\"http://www.w3.org/TR/html4/strict.dtd\">
<html lang=\"en\">
    <head>
        <title>%s</title>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
        <meta name=\"author\" content=\"Upperlink Nigeria Ltd.\">
        <meta name=\"description\" content=\"Charts for Statistical Reports\">

        <script src=\"library/raphaelchart/raphael.js\" type=\"text/javascript\" charset=\"utf-8\"></script>
        <script src=\"library/raphaelchart/g.raphael.js\" type=\"text/javascript\" charset=\"utf-8\"></script>
        <script src=\"library/raphaelchart/g.bar.js\" type=\"text/javascript\" charset=\"utf-8\"></script>
        <script type=\"text/javascript\" charset=\"utf-8\">

            window.onload = function () {
                var r = Raphael(\"holder\", screen.availWidth, 500),
                    fin = function () {
                        this.flag = r.g.popup(this.bar.x, this.bar.y, this.bar.value || \"0\").insertBefore(this);
                    },
                    fout = function () {
                        this.flag.animate({opacity: 0}, 300, function () {this.remove();});
                    },
                    fin2 = function () {
                        var y = [], res = [];
                        for (var i = this.bars.length; i--;) {
                            y.push(this.bars[i].y);
                            res.push(this.bars[i].value || \"0\");
                        }
                        this.flag = r.g.popup(this.bars[0].x, Math.min.apply(Math, y), res.join(\", \")).insertBefore(this);
                    },
                    fout2 = function () {
                        this.flag.animate({opacity: 0}, 300, function () {this.remove();});
                    };
                r.g.txtattr.font = \"12px 'Fontin Sans', Fontin-Sans, sans-serif\";
                
                r.g.barchart(20, 40, 300, 220, [[55, 20, 13, 32, 5, 1, 2, 10]]).hover(fin, fout);

            };
        </script>
    </head>
    <body style=\"background-color:#CCCCCC\">
    	<p>
            %s
        </p>
        <div id=\"holder\"></div>
    </body>
</html>
