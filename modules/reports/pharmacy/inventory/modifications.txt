Modifications today - 16th, May 2012;
-------------------------------------

Tables:

Added a column (inventory_transfer_expirtdate) to inventory_transfer_details table 

Modified the following files 


Files:
Filename: 				Modification
----------------------------------------------------
new_transferForm.php			Modified the point of calling record TransferDetails method to include adding the expiry date as well

withdrawTransfer.php			created to show report of transfer withdrawal attempts

ajaxFunctions.js			added a confirm function (confirmTransferWithdrawal) to confirm when a withdrawal link is clicked


classes:

Classname				method					modification
---------------------------------------------------------------------------------------------
inventory_transfer_class		recordTransferDetails			including the recording of item expiry date for each transfered item in the query
					
					getTransferDetails			modified the query to fetch the proper expiry date as included in the transfer_details 											table


					withdrawTransfer			created to withdraw a given transfer restoring the quantity in batchdetails






inventory_item_class			showItem				modify the query to select the proper qty balance from inventory_batch_details table 										instead of the defunct inventory_item_qties_by_location 

					qtyInTransit				just added it to bring out the qty of each item in transit from transfer details table 										such that we can add it to the balance stock from batch details to take care of items 										in transit



Alagbin
--------
3 packets
9 packs

3 packets
11 packs

1 packet
56 pack




----------------------------------------------------------------------------------------------------------------------------------
Procurement agent, we also act as a Procurement Agent on behalf of a client to manage the acquisition of materials and products required for charity project or any other.

