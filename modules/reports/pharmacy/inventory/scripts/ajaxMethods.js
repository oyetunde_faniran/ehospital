/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var obj;

function ProcessXML(url) {
  // native  object

  if (window.XMLHttpRequest) {
    // obtain new object
    obj = new XMLHttpRequest();
    // set the callback function
    obj.onreadystatechange = processChange;
    // we will do a GET with the url; "true" for asynch
    obj.open("GET", url, true);
    // null for GET with native object
    obj.send(null);
  // IE/Windows ActiveX object
  } else if (window.ActiveXObject) {
    obj = new ActiveXObject("Microsoft.XMLHTTP");
    if (obj) {
      obj.onreadystatechange = processChange;
      obj.open("GET", url, true);
      // don't send null for ActiveX
      obj.send();
    }
  } else {
    alert("Your browser does not support AJAX");
  }
}