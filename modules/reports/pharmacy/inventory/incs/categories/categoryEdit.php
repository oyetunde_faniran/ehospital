<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



$pageClass= new inventory_category_class();

if(isset($_POST['update'])){
    $id=$_POST['category_id'];
    $name=trim($_POST['categoryName']);
    $desc=trim($_POST['categoryDescription']);
    $parent=$_POST['parentCategory'];
    $author=$_SESSION[session_id()."userID"];
    
    if($pageClass->updateCategory($id,$name,$desc,$author,$parent)){
        echo $inventory_category_successfully_updated;
    }else{
        echo $inventory_category_update_failure_message;
    }
}


$categoryProperties=$pageClass->getCategoryProperties($_GET['edit']);
if(is_string($categoryProperties)){
    echo $categoryProperties;
        exit;
        }
echo "<h2>".$inventory_editingCategory ."&nbsp'". $categoryProperties['inventory_category_name']. "'</h2>"; ?>
<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="POST" enctype="multipart/form-data">
    <table border="0">

        <tr><td><?php echo $inv_categoryForm_name_label; ?></td><td><input type="text" name="categoryName" value="<?php echo $categoryProperties['inventory_category_name']; ?>" /></td></tr>

        <tr><td valign="top"><?php echo $inv_categoryForm_description_label; ?></td><td><textarea rows="4" wrap="soft" title="<?php echo $inventory_category_describe_the_category;?> " name="categoryDescription"><?php echo $categoryProperties['inventory_category_description']; ?></textarea></td></tr>

        <tr><td><?php echo $inv_categoryForm_parent_label; ?></td>
            <td>
                <select name="parentCategory">
                    <option selected value="<?php echo $categoryProperties['inventory_category_parent']; ?>"><?php echo $pageClass->getCategoryName($categoryProperties['inventory_category_parent']);?></option><option value="0">None</option>
                    <?php
                    $cats=$pageClass->get_existing_items('inventory_categories','inventory_category_name','asc');
                    if(is_string($cats)){echo $cats;}else{
                        while($cat=$cats->fetch_assoc()){
                            if($cat['inventory_category_id']!==$categoryProperties['inventory_category_id']){
                            echo "<option value=\"".$cat['inventory_category_id']."\">".$cat['inventory_category_name']."</option>";
                            }
                        }
                    }
                    ?>
                </select></td>
        </tr>

        <tr><td>&nbsp;</td><td><input type="hidden" value="<?php echo $categoryProperties['inventory_category_id']; ?>" name="category_id" /><input type="submit" name="update" value="<?php echo $inventory_update; ?>" class="button" /></td></tr>
    </table>
</form>