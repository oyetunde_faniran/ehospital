<?php 
$form=new inventory_forms_class;
if(isset($_POST['setup'])){
    $name=trim($_POST['formName']);
    $desc=trim($_POST['formDescription']);

    $report = $form->createNewForm($name,$desc);
    echo $report;
    if ($report == $inventoryFormCreationSuccessNews) {
        unset($_POST);
    } 
}
?>

<h2><?php echo $inv_ItemForm_create_prompt;?></h2>

<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="POST" enctype="multipart/form-data">
    
    <table border="0">
        
        <tr><td><?php echo $inv_itemName_label;?></td><td><input type="text" name="formName" <?php if(isset($_POST['formName'])){echo "value=\"".$_POST['formName']."\"";}?> /></td></tr>
        
        <tr><td valign="top"><?php echo $inv_newFormDescription_label;?></td><td><textarea rows="4" name="formDescription"><?php  if(isset($_POST['formDescription'])){echo $_POST['formDescription'];} ?></textarea> </td></tr>
        
        <tr><td>&nbsp;</td><td><input type="submit" name="setup" value="<?php echo $inventory_setup;?>" class="button" /></td></tr>
    </table>
</form>