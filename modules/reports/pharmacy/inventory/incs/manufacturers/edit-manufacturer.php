<?php
    $id = isset($_GET["id"]) ? (int)$_GET["id"] : 0;
    $manObj = new inventory_manufacturer();
    $disMan = $manObj->getManufacturer($id);
    //die ("<pre>" . print_r ($disMan, true) . "</pre>");
    if (is_array($disMan)){
        echo isset($_GET["c"]) ? "<div class=\"comments\">" . $_GET["c"] . "</div>" : "";
?>
        <form action="" method="post">
            <table cellpadding="3" cellspacing="3">
                <tr>
                    <td>
                        <label for="manufacturer-name"><?php echo strtoupper($inv_manufacturerForm_name_label) . ":"; ?></label>
                    </td>
                    <td>
                        <input type="text" name="manufacturer_name" id="manufacturer-name" value="<?php echo isset($disMan["inventory_manufacturer_name"]) ? $disMan["inventory_manufacturer_name"] : ""; ?>" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="manufacturer-address"><?php echo strtoupper($inv_manufacturerForm_address_label) . ":"; ?></label>
                    </td>
                    <td>
                        <input type="text" name="manufacturer_address" id="manufacturer-address" value="<?php echo isset($disMan["inventory_manufacturer_address"]) ? $disMan["inventory_manufacturer_address"] : ""; ?>" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="manufacturer-rcno"><?php echo strtoupper($inv_vendorRCNO_label) . ":"; ?></label>
                    </td>
                    <td>
                        <input type="text" name="manufacturer_rcno" id="manufacturer-rcno" value="<?php echo isset($disMan["inventory_manufacturer_rcno"]) ? $disMan["inventory_manufacturer_rcno"] : ""; ?>" />
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" name="sButton" id="submit-button" value="<?php echo ucwords($admin_button_savechanges); ?>" class="btn" />
                    </td>
                </tr>
            </table>
        </form>
<?php
    } else echo "Unknown Manufacturer";
?>