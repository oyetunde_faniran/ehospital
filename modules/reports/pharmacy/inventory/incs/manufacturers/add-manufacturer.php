<?php
    echo isset($_GET["c"]) ? "<div class=\"comments\">" . $_GET["c"] . "</div>" : "";
?>
        <form action="" method="post">
            <table cellpadding="3" cellspacing="3">
                <tr>
                    <td>
                        <label for="manufacturer-name"><?php echo strtoupper($inv_manufacturerForm_name_label) . ":"; ?></label>
                    </td>
                    <td>
                        <input type="text" name="manufacturer_name" id="manufacturer-name" value="<?php echo isset($_POST["manufacturer_name"]) ? $_POST["manufacturer_name"] : ""; ?>" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="manufacturer-address"><?php echo strtoupper($inv_manufacturerForm_address_label) . ":"; ?></label>
                    </td>
                    <td>
                        <input type="text" name="manufacturer_address" id="manufacturer-address" value="<?php echo isset($_POST["manufacturer_address"]) ? $_POST["manufacturer_address"] : ""; ?>" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="manufacturer-rcno"><?php echo strtoupper($inv_vendorRCNO_label) . ":"; ?></label>
                    </td>
                    <td>
                        <input type="text" name="manufacturer_rcno" id="manufacturer-rcno" value="<?php echo isset($_POST["manufacturer_rcno"]) ? $_POST["manufacturer_rcno"] : ""; ?>" />
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" name="sButton" id="submit-button" value=" Add " class="btn" />
                    </td>
                </tr>
            </table>
        </form>