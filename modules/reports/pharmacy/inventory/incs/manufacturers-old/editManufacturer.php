<?php 
$pageClass=new inventory_manufacturer_class();

if(isset($_POST['update'])){
    $name=trim($_POST['manufacturerName']);
    $address=trim($_POST['manufacturerAddress']);
    $rcno=trim($_POST['manufacturerRcNo']);
    
    $updated=$pageClass->updateManufacturer($name,$address,$rcno);
    
    if($updated==$inventory_manufacturer_updated_successfully){
        unset($_POST);
    }
    echo $updated;
}

if(($_GET['edit'])&&(is_numeric($_GET['edit']))){
$manufacturerInfo=$pageClass->getManufacturerDetails($_GET['edit']);
?>
    
<h2><?php echo $inv_manufacturerForm_updateLabel_prompt."&nbsp;'".$manufacturerInfo['inventory_manufacturer_name']."'";?></h2>

<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="POST" enctype="multipart/form-data">
    
    <table border="0">
        
        <tr><td><?php echo $inv_manufacturerForm_name_label;?></td><td><input type="text" name="manufacturerName" <?php if(isset($_POST['manufacturerName'])){
            echo "value=\"".$_POST['manufacturerName']."\"";
            }else{
                echo "value=\"".stripslashes($manufacturerInfo['inventory_manufacturer_name'])."\"";
                }?> /></td></tr>
        <tr><td valign="top"><?php echo $inv_manufacturerForm_address_label;?></td><td><textarea rows="4" name="manufacturerAddress"><?php  if(isset($_POST['manufacturerAddress'])){
            echo $_POST['manufacturerAddress'];
            }else{
                echo stripslashes($manufacturerInfo['inventory_manufacturer_address']);
            } ?></textarea> </td></tr>
        
        <tr><td><?php echo $inv_vendorRCNO_label;?></td><td><input type="text" name="manufacturerRcNo" <?php if(isset($_POST['manufacturerRcNo'])){
            echo "value=\"".$_POST['manufacturerRcNo']."\"";
            }  else {
             echo "value=\"".stripslashes($manufacturerInfo['inventory_manufacturer_rcno'])."\"";
            }?> /></td></tr>
          <tr><td>&nbsp;</td><td><input type="submit" name="update" value="<?php echo $inventory_update;?>" class="button" /></td></tr>
    </table>
</form>
<?php } ?>