<?php
$createManufacturer = new inventory_manufacturer_class();
if (isset($_POST['setup'])) {
    $name = trim($_POST['manufacturerName']);
    $address = trim($_POST['manufacturerAddress']);
    $rcno = $_POST['manufacturerRcno'];

    $report = $createManufacturer->addNewManufacturer($name, $address, $rcno);
    echo $report;
    if ($report == $inventoryManufacturerSuccessfullyAdded) {
        unset($_POST);
    }
}
?>
<h2><?php echo $inv_manufacturerForm_createLabel_prompt; ?></h2>

<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST" enctype="multipart/form-data">

    <table border="0">

        <tr><td><?php echo $inv_manufacturerForm_name_label; ?></td><td><input type="text" name="manufacturerName" <?php if (isset($_POST['manufacturerName'])) {
    echo "value=\"" . $_POST['manufacturerName'] . "\"";
} ?> /></td></tr>

        <tr><td valign="top"><?php echo $inv_manufacturerForm_address_label; ?></td><td><textarea rows="4" wrap="soft" name="manufacturerAddress"><?php if (isset($_POST['manufacturerAddress'])) {
    echo $_POST['manufacturerAddress'];
} ?></textarea></td></tr>

        <tr><td><?php echo $inv_manufacturerForm_rcno_label;?></td><td><input type="text" name="manufacturerRcno" <?php if (isset($_POST['manufacturerRcno'])) {
    echo "value=\"" . $_POST['manufacturerRcno'] . "\"";
} ?> /></td></tr>

        <tr><td>&nbsp;</td><td><input type="submit" name="setup" value="<?php echo $inventory_setup; ?>" class="button" /></td></tr>
    </table>
</form>