<?php
$pageClass = new inventory_stock_class();
$dUnits = new inventory_units();
$locationID = isset($_GET["location"]) ? $_GET["location"] : 0;
$_GET['location'] = $locationID = $inventory_locationID;
if (isset($_GET["c"])) {
    echo "<div class=\"comments\">" . $_GET["c"] . "</div>";
}
include_once ("./includes/pagination-init-pharmacy.php");
$args["total"] = $pageClass->getAllStockItems($locationID, true);
$paginationText = Paginator::doPagination($args);
$paginationText = "<div class=\"pagination-container\">" . $paginationText . "</div>";
echo $paginationText;
?>
<form action="" method="post" >
    <table width="98%" border="0">
        <tr><td>Select vendor</td>
            <td>
                <select name="vendor">
                    <?php
                    $vendors = $pageClass->get_existing_items('inventory_vendors', 'inventory_vendor_name', 'asc');
                    if (is_string($vendors)) {
                        echo $vendors;
                    } else {
                        while ($vendor = $vendors->fetch_assoc()) {
                            echo "<option value=\"" . $vendor['inventory_vendor_id'] . "\">" . $vendor['inventory_vendor_name'] . "</option>";
                        }
                    }
                    ?>
                </select>
            </td>
        </tr>

        <?php
        if (isset($_GET['location']) && is_numeric($_GET['location'])) {
            echo "<tr class=\"title-row\">
                    <th align=\"left\">" . $inventory_item_commercial_name . "</th>
                    <th align=\"left\">" . $inventory_item_medical_name . "</th>
                    <th align=\"left\">" . $inventory_manufacturer . "</th>
                    <th align=\"left\">" . $inventory_presentation . "</th>
                    <th align=\"left\">" . $inventory_strength . "</th>
                    <th align=\"center\">" . $inventory_stock_bal . "</th>
                    <th>Cost Price (&#8358;)</th>
                    <th>Expiry Date</th>
                  </tr>";


            //$items = $pageClass->getAllStockItems($locationID);
            $items = $pageClass->getAllStockItems();
            $datePickerInit = "";
            if (is_string($items)) {
                echo $items;
            } else {
                $juggle = true;
                $rowStyle1 = " class=\"tr-row\" ";
                $rowStyle2 = " class=\"tr-row2\" ";
                while ($item = $items->fetch_assoc()) {
                    $rowStyle = $juggle ? $rowStyle1 : $rowStyle2;
                    $juggle = !$juggle;
                    $datePickerInit .= "$('#expirydate-" . $item['drug_id'] . "').datepicker({
                                            changeMonth:	true,
                                            changeYear:		true,
                                            dateFormat:		'yy-mm-dd'
                                        });";
                    echo "<tr $rowStyle>
                            <td align=\"center\">" . $item['drug_name'] . "</td>
                            <td align=\"left\">" . $pageClass->getMedicalName($item['drug_desc']) . "</td>
                            <td align=\"left\">" . $pageClass->getManufacturerName($item['drug_manufacturer']) . "</td>
                            <td align=\"left\">" . $pageClass->getPresentationName($item['drug_presentation']) . "</td>
                            <td align=\"left\">" . $item['drug_strength'] . "</td>
                            <td>";
                    $itemUnits = $dUnits->showInAllUnits($pageClass->getItemBalStockByLocation($item['drug_id'], $_GET['location']), $item['drug_id']);
                    $display = "<table cellpadding=\"3\" cellspacing=\"3\">
                                <tr>
                                    <th>Qty In Stock</th>
                                    <th>Unit</th>
                                    <th>Receiving Qty</th>
                                </tr>";
                    foreach ($itemUnits as $disUnit) {
                        $display .= "<tr>
                                        <td>" . $disUnit["quantity"] . "</td>
                                        <td>" . $disUnit["name"] . "</td>
                                        <td>
                                            <input type=\"text\" name=\"qty[" . $item['drug_id'] . "][" . $disUnit["id"] . "]\" value=\"0\" size=\"5\" />
                                        </td>
                                     </tr>";
                    }   //END foreach
                    $display .= "</table>";
                    echo $display;


                    echo "  </td>
                            <td><input type=\"text\" size=\"10\" maxlength=\"20\" name=\"costprice[" . $item['drug_id'] . "]\" id=\"costprice-" . $item['drug_id'] . "\" /></td>
                            <td><input type=\"text\" size=\"10\" maxlength=\"10\" name=\"expirydate[" . $item['drug_id'] . "]\" id=\"expirydate-" . $item['drug_id'] . "\" /></td>
                          </tr>
                          <tr><td colspan=\"8\"><hr /></td></tr>";
                }
            }

            echo "<tr><td align=\"right\" colspan=\"8\"><input type=\"submit\" name=\"receive\" value=\"Receive\" class=\"btn\" /></td></tr>";
        }
        ?>
    </table>
</form>

<?php
    echo $paginationText;
?>

<link type="text/css" href="./library/admin_jquery/themes/base/ui.theme.css" rel="stylesheet" />
<link type="text/css" href="./library/admin_jquery/themes/smoothness/ui.core.css" rel="stylesheet" />
<link type="text/css" href="./library/admin_jquery/themes/smoothness/ui.base.css" rel="stylesheet" />
<link type="text/css" href="./library/admin_jquery/themes/base/ui.datepicker.css" rel="stylesheet" />
<link type="text/css" href="./library/admin_jquery/themes/smoothness/ui.datepicker.css" rel="stylesheet" />


<script type="text/javascript" src="./library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="./library/admin_jquery/ui/ui.draggable.js"></script>
<script type="text/javascript" src="./library/admin_jquery/ui/ui.resizable.js"></script>
<script type="text/javascript" src="./library/admin_jquery/ui/effects.core.js"></script>
<script type="text/javascript" src="./library/admin_jquery/ui/effects.highlight.js"></script>
<script type="text/javascript" src="./library/admin_jquery/external/bgiframe/jquery.bgiframe.js"></script>
<script type="text/javascript" src="./library/admin_jquery/ui/ui.datepicker.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $(function() {
<?php
    echo $datePickerInit;
?>
        });
        
    });
</script>