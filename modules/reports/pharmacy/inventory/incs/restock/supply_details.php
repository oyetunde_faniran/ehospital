<?php

$pageClass = new inventory_stock_class();
if (isset($_GET['supplyId']) && is_numeric($_GET['supplyId'])) {
    $supplyDetails = $pageClass->getSupplyDetails((int) $_GET['supplyId']);
    if (is_string($supplyDetails)) {
        echo $supplyDetails;
    } else {
        echo "<table border=\"0\" width=\"98%\">
                <tr class=\"title-row\">
                    <th align=\"center\">#</th>
                    <th align=\"left\">" . $inventory_item_medical_name . "</th>
                    <th align=\"left\">" . $inventory_item_commercial_name . "</th>
                    <th align=\"left\">" . $inventory_qty . "</th>
                </tr>";
        $counter = 1;
        while ($details = $supplyDetails->fetch_assoc()) {
            $items = new inventory_item_class();
            $item = $items->showItem($details['drug_id']);
            echo "<tr>
                    <td align=\"center\">" . $counter . ".</td>
                    <td align=\"left\">" .$pageClass->getMedicalName($item['drug_desc']) . "</td>
                        <td align=\"left\">" . $pageClass->getCommercialName($details['drug_id']) . "</td>
                    
                    <td align=\"left\">";
            $dUnits = new inventory_units();
            $itemUnits = $dUnits->showInAllUnits($details['inventory_batchdetails_received_quantity'], $details['drug_id']);

            $display = "<table cellpadding=\"3\" cellspacing=\"3\">
                            <tr>
                                <th>Qty Received</th>
                                <th>Unit</th>
                            </tr>";
            foreach ($itemUnits as $disUnit) {
                $display .= "<tr>
                                <td>" . $disUnit["quantity"] . "</td>
                                <td>" . $disUnit["name"] . "</td>
                             </tr>";
            }   //END foreach
            $display .= "</table>";
            echo $display;

            echo "      </td>
                  </tr>
                  <tr><td colspan=\"4\"><hr /></td></tr>";
            $counter++;
        }
        echo "</table>";
    }
} else {
    echo $inventory_unrecognized_supply_id;
}
echo "<p><a href=\"index.php?p=inventory_list_received_stock&m=pharmacy\">&laquo; Back to Re-Stock History</p>";
?>