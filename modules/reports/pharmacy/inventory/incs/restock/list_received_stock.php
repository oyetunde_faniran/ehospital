<?php

$stockLists=new inventory_stock_class();

$lists=$stockLists->getAllSupplies($inventory_locationID);

if(is_string($lists)){
    echo $lists;
}else{
    echo "<table border=\"0\" width=\"98%\">
            <tr class=\"title-row\">
                <th align=\"left\">#</th>
                <th align=\"left\">".$inventory_location."</th>
                <th align=\"left\">".$inventory_vendor."</th>
                <th align=\"left\">".$inventory_receiver."</th>
                <th align=\"left\">".$inventory_supplyDate."</th>
                <th align=\"left\">NUMBER OF ITEMS</th>
                <th align=\"left\">$inventory_view</th>
            </tr>";
    $counter=1;
    $rowStyle1 = " class=\"tr-row\" ";
    $rowStyle2 = " class=\"tr-row2\" ";
    $juggle = true;
    while($list=$lists->fetch_assoc()){
        $rowStyle = $juggle ? $rowStyle1 : $rowStyle2;
        $juggle = !$juggle;
        echo "<tr $rowStyle>
                  <td align=\"left\">".$counter.".</td>
                  <td align=\"left\">".$stockLists->getLocationName($list['inventory_location_id'])."</td>
                  <td align=\"left\">".$stockLists->getVendorName($list['inventory_vendor_id'])."</td>
                  <td align=\"left\">".$stockLists->getUserName($list['user_id'])."</td>
                  <td align=\"left\">".$list['supplyDate']."</td>
                  <td align=\"left\">" . $list['itemCount'] . "</td>
                  <td align=\"left\"><a href=index.php?p=view_supply_details&m=pharmacy&supplyId=".$list['inventory_batch_id']." />".$inventory_view."</a></td>
              </tr>";
        $counter ++;
    }
    
    echo "</table>";
}

?>
