<?php
include 'modules/pharmacy/inventory/incs/javascriptOperations.php';

$pageClass = new inventory_item_class();

$itemID=strip_tags((int)$_GET['item']);
if (isset($_POST['update'])) {
    //die ("<pre>" . print_r ($_POST, true) . "</pre>");
    extract($_POST);
if(empty($cat)){
        $_GET['comments']=$inventory_noCategory_msg;
    }else{
        $itemUpdate=$pageClass->updateItemProperties($itemID, $itemName, $medicalName, $form, $strength, $presentation, $manufacturer, $nafdac, $sideeffects);
        $_GET['comments']=$itemUpdate;
        $categorized=$pageClass->updateCategories($itemID,$cat);
        $_GET['comments'].=$categorized;
        if(!empty($oldChildQty)&& is_array($oldChildQty)){
            $_GET['comments'].=$pageClass->updateQties($oldChildQty);
        }
    }
        
//        if(!empty($delete)&& is_array($delete)){
//            $_GET['comments'].="<br />".$pageClass->removeUnits($delete);
//        }
        
//         $invUnit = new inventory_units();
//        $error = $invUnit->saveItemUnits((int)($_GET['item']), $_POST["unit"], $_POST["unitquantity"], $_SESSION[session_id() . "userID"]);
        
//        $medicalName = $pageClass->getMedicalName($medicalName);
//        $presentation = $pageClass->getPresentationName($presentation);
//        $form = $pageClass->getFormName($form);
//        $manufacturer = $pageClass->getManufacturerName($manufacturer);
//
//        $report = $pageClass->updateItem($itemName, $medicalName, $reorderLevel, $cat, $form, $strength, $presentation, $manufacturer, $nafdac, $location/*, $sellingprice, $markup, $pricetouse*/, $sideeffects);
//        $invUnit = new inventory_units();
//        
//        $unitsUpdate=$invUnit->updateOldUnits($oldUnits);
//        
//        $error = $invUnit->saveItemUnits($pageClass->itemID, $_POST["unit"], $_POST["unitquantity"], $_SESSION[session_id() . "userID"]);
//        //echo $report;
//        if ($report == $inventoryItemCreationSuccessNews) {
//            //unset($_POST);
//            $page = $_SERVER["PHP_SELF"] . "?" . $_SERVER["QUERY_STRING"] . "&comments=" . urlencode($report);
//            header("Location: " . $page);
//        }
    
}

$catArray=$pageClass->findItemCategories($_GET['item']);
//die (print_r ($catArray, true) . "-----");
if (!empty($_GET["comments"])) {
    echo "<div class=\"comments\">" . $_GET["comments"] . "</div>";
}
$itemId=((int)$_GET['item']);
$property=$pageClass->getItemForEdit($itemId);
if(!is_array($property)){
    echo $property;
}else{
?>

<form action="" method="POST" enctype="multipart/form-data">

    <div id="item-tabs">
        <ul>
            <li><a href="#tabs-1"><?php echo $inventory_item_info_label; ?></a></li>
            <li><a href="#tabs-2"><?php echo $inventory_item_category_label; ?></a></li>
            <li><a href="#tabs-3"><?php echo $inventory_item_unit_label; ?></a></li>
            <!--<li><a href="#tabs-4"><?php echo "Item Pricing"; ?></a></li>-->
        </ul>




        <div id="tabs-1">
            <table border="0" width="100%">

<tr><td><?php echo $inventory_item_medical_name; ?></td><td>
<select name="medicalName">

    <?php $medicalNames = $pageClass->findMedicalNames();
    if (is_string($medicalNames)) { ?>
        <option value=""><?php echo $medicalNames; ?></option>
    <?php } else {
        while ($medicalName = $medicalNames->fetch_assoc()) { ?>
            <option value="<?php echo $medicalName['inventory_medical_name_id']; ?>"<?php if($medicalName['inventory_medical_name_id']==$property['drug_desc']){echo "selected";}?>><?php echo $medicalName['inventory_medical_name']; ?></option>
        <?php
        }
    }
    ?>
</select></td></tr>
        <tr><td width="20%"><?php echo $inventory_item_commercial_name; ?></td><td><input type="text" name="itemName" <?php if (isset($_POST['itemName'])) {
    echo "value=\"" . $_POST['itemName'] . "\"";
}else{echo "value=\"" . $property['drug_name'] . "\"";} ?> /></td></tr>
                <tr><td><?php echo $inventory_manufacturer; ?></td><td>
                        <select name="manufacturer">
                            <option value="">&nbsp;</option>
                            <?php $forms = $pageClass->findItemManufacturers();
                            if (is_string($forms)) { ?>
                                <option value=""><?php echo $forms; ?></option>
                            <?php } else {
                                while ($form = $forms->fetch_assoc()) { ?>
                                    <option value="<?php echo $form['inventory_manufacturer_id'];?>" <?php if($form['inventory_manufacturer_id']==$property['drug_manufacturer']){echo "selected";}?>><?php echo $form['inventory_manufacturer_name']; ?></option>
                                <?php
                                }
                            }
                            ?>
                        </select></td></tr>

                <tr><td><?php echo $inventory_form; ?><td>
                        <select name="form">
                            <option value="">&nbsp;</option>
                            <?php $forms = $pageClass->findItemForms();
                            if (is_string($forms)) { ?>
                                <option value=""><?php echo $forms; ?></option>
                            <?php } else {
                                while ($form = $forms->fetch_assoc()) { ?>
                                    <option value="<?php echo $form['inventory_form_id'];?>"<?php if($form['inventory_form_id']==$property['drug_dosageform']){echo "selected";}?>><?php echo $form['inventory_form_name']; ?></option>
    <?php
    }
}
?>
                        </select></td></tr>

                <tr><td><?php echo $inventory_presentation; ?></td><td>
                        <select name="presentation"><option value="">&nbsp;</option>
                            <?php $forms = $pageClass->findItemPresentations();
                            if (is_string($forms)) { ?>
                                <option value=""><?php echo $forms; ?></option>
                            <?php } else {
                                while ($form = $forms->fetch_assoc()) { ?>
                                    <option value="<?php echo $form['inventory_presentation_id']; ?>"<?php if($form['inventory_presentation_id']==$property['drug_presentation']){echo "selected";} ?>><?php echo $form['inventory_presentation_name']; ?></option>
    <?php
    }
}
?>
                        </select></td></tr>

                <tr><td><?php echo $inventory_strength; ?></td><td>
                        <input type="text" name="strength" value="<?php echo $property['drug_strength'];?>" /></td></tr>

                <tr>
                    <td>NAFDAC No.</td>
                    <td><input type="text" name="nafdac"  value="<?php echo $property['inventory_item_nafdac']; ?>" /></td>
                </tr>
                
                
                <tr>
                    <td>Side Effects</td>
                    <td>
                        <textarea rows="10" cols="50" name="sideeffects" id="side-effects">
                            <?php echo $property['inventory_item_side_effects'];?>
                        </textarea>
                    </td>
                </tr>
                

                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="hidden" name="reorderLevel" value="" />
                        <?php
                        $locations = $pageClass->get_existing_items('inventory_locations', 'inventory_location_name', 'asc');
                        if (!$locations) {
                            echo $inventory_location_dbconnnect_error;
                        } elseif (is_string($locations)) {
                            echo $locations;
                        } else {
                            while ($location = $locations->fetch_array()) {
                                echo "<input type=\"hidden\" value=\"0\" name=\"location[" . $location['inventory_location_id'] . "]\" />";
                            }
                        }
                        ?>
                        <input type="button" id="next-to-tab-2" value=" <?php echo $inventory_item_next_label; ?> &raquo; " class="btn" />
                    </td>
                </tr>


            </table>
        </div>









        <div id="tabs-2">
            <?php
            include_once ("./modules/pharmacy/inventory/recursive-hierarchical-function.php");
            $invClass = new inventory_category_class();
            $display = "<table cellpadding=\"3\" cellspacing=\"3\" style=\"border: 5px solid #CCC\">
                    <tr>
                        <td><strong>S/NO</strong></td>
                        <td><strong>PRODUCT CATEGORY</strong></td>
                        <td>&nbsp;</td>
                    </tr>";
            $display .= getChildren(0, "", "", " class=\"tr-row\" ");
            $display .= "</table>";
            echo $display;
            ?>
            <div style="margin-top: 10px;">
                <input type="button" id="prev-to-tab-1" value=" &laquo; <?php echo $inventory_item_prev_label; ?> " class="btn" />
                <input type="button" id="next-to-tab-3" value=" <?php echo $inventory_item_next_label; ?> &raquo; " class="btn" />
            </div>
        </div>

        <div id="tabs-3">
            <table width="98%" border="0">
            <?php

$unitsClass=new inventory_units();
$newUnits=$unitsClass->getItemUnits($property['drug_id']);
if(is_array($newUnits)){
    foreach($newUnits as $newUnit){
        echo "<tr><td align=\"left\"><input type=\"text\" size=\"5\" maxlength=\"10\" value=\"".$newUnit['inventory_units_items_child_quantity']."\" name=\"oldChildQty[".$newUnit['inventory_unit_items_id']."]\" />&nbsp;&nbsp;</td><td align=\"left\">".$unitsClass->getUnitName($newUnit['inventory_unit_id'])." will make one (1) of the next unit</td><td align=\"center\"></td></tr>";
//            <input type=\"checkbox\" name=\"delete[]\" value=\"".$newUnit['inventory_unit_items_id']."\" /> Remove
                
    }
}
//echo "<tr><td align =\"left\" colspan=\"3\">";
//include_once ("./modules/pharmacy/inventory/incs/items/update-item-units-tab.php");
//echo "</td></tr>";
//die (print_r ($units, true) . "-----");
?>
            </table>
            <div style="margin-top: 10px;">
                <input type="button" id="prev-to-tab-2" value=" &laquo; <?php echo $inventory_item_prev_label; ?> " class="btn" />
                <input type="submit" name="update" value=" <?php echo $inventory_update; ?> " class="btn" />
                <!--<input type="button" id="next-to-tab-4" value=" <?php echo $inventory_item_next_label; ?> &raquo; " class="btn" />-->
            </div>
        </div>
        
        
        <!--<div id="tabs-4">
            <table>
                <tr>
                    <td>
                        <input type="radio" name="pricetouse" id="pricetouse-1" checked="checked" value="1" /> <label for="pricetouse-1">Use Selling Price (&#8358;):</label>
                    </td>
                    <td>
                        <input type="text" name="sellingprice" size="10" maxlength="20" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="radio" name="pricetouse" id="pricetouse-2" value="2" /> <label for="pricetouse-2">Use Mark-Up (&#8358;):</label>
                    </td>
                    <td>
                        <input type="text" name="markup" size="10" maxlength="20" />
                    </td>
                </tr>
            </table>
            <div style="margin-top: 10px;">
                <input type="button" id="prev-to-tab-3" value=" &laquo; <?php echo $inventory_item_prev_label; ?> " class="btn" />
                <input type="submit" name="setup" value=" <?php echo $inventory_setup; ?> " class="btn" />
            </div>
        </div>-->
        
        
    </div>
</form>



<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.sortable.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.tabs.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
<script type="text/javascript">
    $(function() {
        $("#item-tabs").tabs().find(".ui-tabs-nav").sortable({axis:'x'});
    });

    $('#next-to-tab-2').click(function(){
        $("#item-tabs").tabs("select", 1);
    });
    $('#next-to-tab-3').click(function(){
        $("#item-tabs").tabs("select", 2);
    });
    $('#prev-to-tab-1').click(function(){
        $("#item-tabs").tabs("select", 0);
    });
    $('#prev-to-tab-2').click(function(){
        $("#item-tabs").tabs("select", 1);
    });
    $('#next-to-tab-4').click(function(){
        $("#item-tabs").tabs("select", 3);
    });
    $('#prev-to-tab-3').click(function(){
        $("#item-tabs").tabs("select", 2);
    });
    
    function confirmSubmit(disForm){
        if (confirm("Are you sure you are ready to add this item?"))
            disForm.submit();
    }
   
</script>
<?php }?>