<?php
include 'modules/pharmacy/inventory/incs/javascriptOperations.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$pageClass = new inventory_stock_class();

$dUnits = new inventory_units();

if(isset($_POST['submit'])){

    $unitArray=$_POST['qty'];
    
    $totalQuantity = 0;
    
    foreach ($unitArray as $unitID => $quantity){
    
        $disQuantity = $dUnits->convertToLowestUnit($unitID, $_GET['item'], $quantity);
        
        $totalQuantity += $disQuantity;
                }

                $updated=$pageClass->updateReorderLevel($_GET['item'],$totalQuantity,$_SESSION[session_id() . "inventory_locationID"]);
                
                $_GET['comments']=$updated;
}
if(!empty($_GET['comments'])){
    echo "<div class=\"comments\">".$_GET['comments']."</div>";
}

echo $inventory_reorder_level_action_prompt;
?>
<form action="" method="POST">
    <table border="0" width="58%">
        <tr><td align="left" valign="top"><label>Reorder Level</label></td><td align="left">
                <?php
            $itemUnits = $dUnits->showInAllUnits($pageClass->getItemBalStockByLocation($_GET['item'], $_SESSION[session_id() . "inventory_locationName"]), $_GET['item']);
                    $display = "<table cellpadding=\"3\" cellspacing=\"3\">";
                                
                    foreach ($itemUnits as $disUnit) {
                        $display .= "<tr>
                                        <td>" . $disUnit["name"] . "</td>
                                        <td>
                                            <input type=\"text\" name=\"qty[" . $disUnit["id"] . "]\" value=\"0\" size=\"5\" />
                                        </td>
                                     </tr>";
                    }   //END foreach
                    $display .= "</table>";
                    echo $display;
    
            ?>
            </td></tr>
        <tr><td>&nbsp;</td><td align="left"><input type="submit" name="submit" value="<?php echo $inventory_update;?>" class="btn" /></td></tr>
    </table>
</form> 
