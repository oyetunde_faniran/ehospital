<?php
    $good2Go = true;
    $hospitalNumber = isset($_GET["h"]) ? $_GET["h"] : "";
    $transNo = isset($_GET["t"]) ? $_GET["t"] : "";
    $extraInfo = "";
    
    //Check the settings table to know if payment must be made before dispensing can be done
    $set = new admin_Settings();
    $transNeeded = $set->getSettings("DISPENSE_WITH_TRANSACTIONS_ONLY");
    
    //Get info about the transaction if available
    $invoicer = new invoice();
    $disTransaction = $invoicer->getTransaction($transNo);
    if (is_array($disTransaction)){
        $c = new Clinic_New();
        $d = new Department();
        $doc = new Doctor();
        $hospitalNumber = $disTransaction["reg_hospital_no"];
        
        //Get the items attached to this transaction
        $disTransDeets = $invoicer->getTransactionDetails($transNo);
        //die ("<pre>" . print_r ($disTransDeets, true) . "</pre>");
        $transItems = "";
        if (is_array($disTransDeets)){
            $sNo = 0;
            foreach ($disTransDeets as $item){
                $transItems .= "<div style=\"margin-bottom: 5px;\">" . (++$sNo) . ". " . $item["item"] . " / " . "QUANTITY: " . ((int)$item["patitem_totalqty"]) . "</div>";
            }
        }
        $deptName = $d->getDeptName($disTransaction["dept_id"]);
        $clinicName = $c->getClinicName($disTransaction["clinic_id"]);
        $doctor = $doc->getNameOfDoctorForAppointment(0, $disTransaction["patadm_id"], $disTransaction["dept_id"], $disTransaction["clinic_id"]);
        $extraInfo = "<tr class=\"tr-row2\">
                          <td><label>DEPARTMENT / CLINIC:</label></td>
                          <td>$deptName / $clinicName</td>
                      </tr>
                      <tr class=\"tr-row\">
                          <td><label>CONSULTANT:</label></td>
                          <td>" . $doctor . "</td>
                      </tr>
                      <tr class=\"tr-row2\">
                          <td><label>ACTUAL PRESCRIPTION:</label></td>
                          <td><pre>" . $disTransaction["pattotalextra_doctorprescription"] . "</pre></td>
                      </tr>
                      <tr class=\"tr-row\">
                          <td><label>ITEMS ON INVOICE:</label></td>
                          <td>" . $transItems . "</td>
                      </tr>";
    }
    
    //Get the patient info
    $pat = new Patient();
    $patInfo = $pat->getPatientInfo($hospitalNumber);
    
    if ($transNeeded == 1){
        if (!is_array($disTransaction)){
            $_GET["c"] = "Invalid transaction number specified!";
            $good2Go = false;
        } else {
            $isDrugInvoice = $disTransaction["pattotal_invoice_type"] == 2;
            $paid = $disTransaction["pattotal_status"] == 1;
            $serviceRendered = $disTransaction["pattotal_servicerendered"] == 1;
            
            //Confirm that the transaction is for drugs and has been paid for
            if (!$isDrugInvoice || !$paid){
                $_GET["c"] = "Please, advise the patient to make payment for this transaction first.";
                $good2Go = false;
            }
            
            //Confirm that service has not been rendered for this payment / transaction
            if ($serviceRendered){
                $_GET["c"] = "Drugs have been dispensed for this transaction already.";
                $good2Go = false;
            }
            
        }
    } else {
        //If the hospital number is invalid, then go to the hospital number entry page for dispensing
        if (!is_array($patInfo)){
            header ("Location: index.php?p=inventory_dispense_item&m=pharmacy&t=$transNo");
        }
    }
    
    
    if (isset($_GET["c"])){
        echo "<div class=\"comments\">" . $_GET["c"] . "</div>";
    }
    
    if ($good2Go){
    
    //Display the patient info
    $patInfoDisplay = "<fieldset>
                            <legend>Patient Information</legend>
                            <table cellpadding=\"3\" cellspacing=\"3\">
                                <tr class=\"tr-row2\">
                                    <td><label>PATIENT:</label></td>
                                    <td>" . $patInfo["reg_othernames"] . " " . $patInfo["reg_surname"] . " [$hospitalNumber]</td>
                                </tr>
                                <tr class=\"tr-row\">
                                    <td><label>GENDER:</label></td>
                                    <td>" . ($patInfo["reg_gender"] == 1 ? "Female" : "Male") . "</td>
                                </tr>
                                <!--<tr class=\"tr-row\">
                                    <td><label>DATE OF BIRTH:</label></td>
                                    <td>" . $patInfo["reg_dob"] . "</td>
                                </tr>-->
                                $extraInfo
                            </table>
                        </fieldset>";
    echo $patInfoDisplay;
    
?>

    <fieldset>
        <legend>Drugs selected for dispensing</legend>
        <div id="drugs-dispensing-temporary-display">
            No drugs selected for dispensing yet.
        </div>
        <form action="" method="post">
            <div id="drugs-dispensing-loading-image-container" style="display: none;">
                <img src="images/ajax-loader.gif" />
            </div>
            <table id="drugs-dispensing-table" cellpadding="3" cellspacing="3" style="display: none;">
                <thead>
                    <tr class="title-row">
                        <td><?php echo strtoupper($inventory_item_medical_name); ?></td>
                        <td><?php echo strtoupper($inventory_item_commercial_name); ?></td>
                        <td>FORM</td>
                        <td>PRESENTATION</td>
                        <td>STRENGTH</td>
                        <td>MANUFACTURER</td>
                        <td>QUANTITY TO DISPENSE</td>
                        <td>LABEL</td>
                        <td><em>Remove</em></td>
                    </tr>
                </thead>
                <tbody id="drugs-dispensing-container"></tbody>
            </table>
            <div style="margin-top: 10px; display: none;" id="drugs-dispensing-submit-button">
                <input type="button" class="btn" value=" Dispense " onclick="submitDispense(this.form);" />
            </div>
        </form>
    </fieldset>

    <fieldset>
        <legend>Drug Search</legend>
        <table cellpadding="3" cellspacing="3">
            <tr>
                <td><label>Search By:</label></td>
                <td>
                    <input type="radio" checked="checked" name="which" id="medicalname" />
                    <label for="medicalname"><?php echo ucwords($inventory_item_medical_name); ?></label>
                    <input type="radio" name="which" id="commercialname" style="margin-left: 20px;" />
                    <label for="commercialname"><?php echo ucwords($inventory_item_commercial_name); ?></label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="text" name="searchq" id="searchq" size="50" maxlength="50" />
                    <input type="button" name="searchButton" id="search-button" value=" Search " class="btn" />
                    <span id="loading-image-container" style="display: none;">
                        <img src="images/ajax-loader.gif" />
                    </span>
                </td>
            </tr>
        </table>
        
        <div style="margin-top: 10px;">
            <div id="drugs-error-div" style="display: none;">No drug matching your search criteria was found.</div>
            <table id="drugs-info-table" cellpadding="3" cellspacing="3" style="display: none;">
                <thead>
                    <tr class="title-row">
                        <td>S/NO</td>
                        <td><?php echo strtoupper($inventory_item_medical_name); ?></td>
                        <td><?php echo strtoupper($inventory_item_commercial_name); ?></td>
                        <td>FORM</td>
                        <td>PRESENTATION</td>
                        <td>STRENGTH</td>
                        <td>MANUFACTURER</td>
                        <td>QUANTITY IN STOCK</td>
                        <td>BATCH / EXPIRY DATE</td>
                        <td><em>Dispense</em></td>
                    </tr>
                </thead>
                <tbody id="drugs-info-container"></tbody>
            </table>
        </div>
    </fieldset>

<script type="text/javascript">
    dispensedDrugs = 0;
    dispenseList = new Array();
    $('#search-button').click(function() {
        q = document.getElementById('searchq').value;
        if (q != ""){
            $("#loading-image-container").fadeIn();
            which = document.getElementById("medicalname").checked ? 1 : 0;
            //alert ("index.php?p=ajax&m=sysadmin&t=drugs-dispense-search&d=" + which + "&q=" + q);
            $.ajax({
              type: 'GET',
              url: "index.php?p=ajax&m=sysadmin&t=drugs-dispense-search&d=" + which + "&q=" + q,
              success: function(data){
                            $('#loading-image-container').fadeOut();
                            if (data != ""){
                                $('#drugs-error-div').fadeOut();
                                $('#drugs-info-table').fadeIn();
                                drugsContainer = document.getElementById("drugs-info-container");
                                drugsContainer.innerHTML = data;
                            } else {
                                $('#drugs-info-table').fadeOut();
                                $('#drugs-error-div').fadeIn();
                            }
                        },
              dataType: "html"
            });
        }
    });
    
    function doDispense(id){
        if (!onDispenseList(id)){
            batchDD = document.getElementById("batch-expiry-date-view-" + id);
            batchID = batchDD.options[batchDD.options.selectedIndex].value;
            if (batchID != 0){
                addToDispenseList(id)
                $('#drugs-dispensing-temporary-display').fadeOut();
                $('#drugs-dispensing-loading-image-container').fadeIn();
                //alert ("index.php?p=ajax&m=sysadmin&t=get-item-for-dispense&d=" + id + "&bid=" + batchID);
                $.ajax({
                      type: 'GET',
                      url: "index.php?p=ajax&m=sysadmin&t=get-item-for-dispense&d=" + id + "&bid=" + batchID,
                      success: function(data){
                                    $('#drugs-dispensing-loading-image-container').fadeOut();
                                    if (data != ""){
                                        $('#drugs-dispensing-table').fadeIn();
                                        $('#drugs-dispensing-submit-button').fadeIn();
                                        dispenseContainer = document.getElementById("drugs-dispensing-container");
                                        dispenseContainer.innerHTML += data;
                                    }
                                },
                      dataType: "html"
                });
                dispensedDrugs++;
            } else alert ("Please, select a batch to dispense from.");
        } else alert("Selected item is already on the list of items to dispense.");
    }   //END doDispense()
    
    
    
    
    function getBatchQuantity(disSelect, id){
        batchID = disSelect.options[disSelect.options.selectedIndex].value;
        //alert ("index.php?p=ajax&m=sysadmin&t=get-batch-quantites&d=" + id + "&bid=" + batchID);
        $('#loading-image-container').fadeIn();
        $.ajax({
              type: 'GET',
              url: "index.php?p=ajax&m=sysadmin&t=get-batch-quantites&d=" + id + "&bid=" + batchID,
              success: function(data){
                            $('#loading-image-container').fadeOut();
                            if (data != ""){
                                quantityContainer = document.getElementById("quantity-cell-" + id);
                                quantityContainer.innerHTML = data;
                            }
                        },
              dataType: "html"
        });
        dispensedDrugs++;
    }
    
    
    
    
    function undoDispense(id){
        if (confirm("Are you sure you want to remove the selected item from the list of items to be dispensed?")){
            c = "dispense-row-" + id;
            try {
                c = document.getElementById(c);
                c.parentNode.removeChild(c);
                removeFromDispenseList(id)
            } catch (e) {
                alert ("An error occurred while trying to remove the item to be dispensed.");
            }
            dispensedDrugs--;
            if (dispensedDrugs == 0){
                $('#drugs-dispensing-temporary-display').fadeIn();
                $('#drugs-dispensing-table').fadeOut();
                $('#drugs-dispensing-submit-button').fadeOut();
            }
        }
    }   //END undoDispense()
    
    
    function submitDispense(disForm){
        if (confirm("Are you sure you want to dispense the quantities entered for each item selected for dispensing?")){
            disForm.submit();
        }
    }   //END submitDispense()
    
    
    
    function onDispenseList(id){
        itemCount = dispenseList.length;
        retVal = false;
        for (elem = 0; elem < itemCount; elem++){
            if (dispenseList[elem] == id){
                retVal = true;
                break;
            }
        }
        return retVal;
    }
    
    function addToDispenseList(id){
        dispenseList.push(id);
    }
    
    function removeFromDispenseList(id){
        itemCount = dispenseList.length;
        for (elem = 0; elem < itemCount; elem++){
            if (dispenseList[elem] == id){
                dispenseList[elem] = 0;
                break;
            }
        }
    }
    
</script>
<?php
    }   //END if good 2 go
?>