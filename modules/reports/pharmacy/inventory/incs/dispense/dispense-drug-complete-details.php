<?php
    //echo "<pre>" . print_r ($_GET, true) . "</pre>";
    $dispenseID = isset($_GET["d"]) ? (int)$_GET["d"] : 0;
    $invDispense = new inventory_dispense();
    
    //Get the main details of this dispense instance
    $disDispense = $invDispense->getDispenseInstance($dispenseID, $inventory_locationID);
    
    //Get the details of the dispense instance
    $disDispenseDetails = $invDispense->getDispenseInstanceDetails($dispenseID, $inventory_locationID);
    
    if (is_array($disDispense) && count($disDispenseDetails) > 0){
        $display = "<table cellspacing=\"3\" cellpadding=\"3\">
                        <tr>
                            <td><label>NAME OF RECIPIENT: </label></td>
                            <td>" . $disDispense["patientName"] . "</td>
                        </tr>
                        <tr>
                            <td><label>HOSPITAL NUMBER OF RECIPIENT: </label></td>
                            <td>" . $disDispense["inventory_dispensed_to"] . "</td>
                        </tr>
                        <tr>
                            <td><label>DATE DISPENSED: </label></td>
                            <td>" . $disDispense["dispenseDate"] . "</td>
                        </tr>
                        <tr>
                            <td><label>TIME DISPENSED: </label></td>
                            <td>" . $disDispense["dispenseTime"] . "</td>
                        </tr>
                    </table>";
        
        
        $display .= "<fieldset>
                        <legend>Dispense Details</legend>";
        $display .= "   <table cellspacing=\"3\" cellpadding=\"3\">
                            <tr class=\"title-row\">
                                <td><label>S/NO</label></td>
                                <td><label>DRUG</label></td>
                                <td><label>QUANTITY</label></td>
                                <td><label>LABEL</label></td>
                                <td><label>SIDE EFFECTS</label></td>
                            </tr>";
        $sNo = 0;
        $juggle = true;
        $rowStyle1 = " class=\"tr-row\" ";
        $rowStyle2 = " class=\"tr-row2\" ";
        $invUnit = new inventory_units();
        foreach ($disDispenseDetails as $dispense){
            //Manage row style
            $rowStyle = $juggle ? $rowStyle1 : $rowStyle2;
            $juggle = !$juggle;
            
            //Get quantity in units
            $allUnits = $invUnit->showInAllUnits($dispense["inventory_dispense_deets_quantity"], $dispense["drug_id"]);
            $disDispenseQuantity = "<table cellspacing=\"2\" cellpadding=\"2\">";
            foreach ($allUnits as $unit){
                if ($unit["quantity"] > 0){
                    $disDispenseQuantity .= "<tr>
                                                <td>" . stripslashes($unit["name"]) . ":</td>
                                                <td>" . number_format($unit["quantity"], 0) . "</td>
                                             </tr>";
                }
            }
            $disDispenseQuantity .= "</table>";
            
            $display .= "<tr $rowStyle>
                            <td>" . (++$sNo) . ".</td>
                            <td>" . stripslashes($dispense["drug_name"]) . "</td>
                            <td>" . stripslashes($disDispenseQuantity) . "</td>
                            <td>" . stripslashes($dispense["inventory_dispense_deets_label"]) . "</td>
                            <td>" . stripslashes($dispense["inventory_item_side_effects"]) . "</td>
                         </tr>";
        }   //END foreach()
        $display .= "   </table>
                     </fieldset>";
    } else $display = "There seems to be an error in the link that brought you to this page.";
    echo $display;
?>
