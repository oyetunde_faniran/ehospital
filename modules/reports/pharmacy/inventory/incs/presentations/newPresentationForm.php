<?php 
    $presentation=new inventory_presentation_class();
    if(isset($_POST['setup'])){
        $report=$presentation->createNewPresentation($_POST['presentationName'],$_POST['presentationDesc']); 
        //echo $report;
        //unset($_POST);
        $page = $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING'] . "&comments=" . urlencode($report);
        header ("Location: $page");
    }
    
    if (!empty($_GET["comments"])){
        echo "<div class=\"comments\">" . $_GET["comments"] . "</div>";
    }
    
?>

<h2><?php echo $inv_presentationForm_create_prompt;?></h2>

<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="POST" enctype="multipart/form-data">
    
    <table border="0">
        
        <tr><td><?php echo $inv_presentationForm_name_label;?></td><td><input type="text" name="presentationName" <?php if(isset($_POST['presentationName'])){echo "value=\"".$_POST['presentationName']."\"";}?> /></td></tr>
        
        <tr><td valign="top"><?php echo $inv_presentation_description_label;?></td><td><textarea rows="4" name="presentationDesc"><?php  if(isset($_POST['presentationDesc'])){echo "value=\"".$_POST['presentationDesc']."\"";} ?></textarea> </td></tr>
        
        <tr><td>&nbsp;</td><td><input type="submit" name="setup" value="<?php echo $inventory_setup;?>" class="button" /></td></tr>
    </table>
</form>