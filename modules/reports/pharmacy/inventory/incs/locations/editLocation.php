<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



$pageClass= new inventory_location_class();

if(isset($_POST['update'])){
    $id=$_POST['location_id'];
    $name=trim($_POST['locationName']);
    $desc=trim($_POST['locationDescription']);
    $author=$_SESSION[session_id()."userID"];
    
    if($pageClass->updateLocation($id,$name,$desc,$author)){
        echo $inventory_location_successfully_updated;
    }else{
        echo $inventory_location_update_failure_message;
    }
}


$locationProperties=$pageClass->getLocationProperties($_GET['edit']);
if(is_string($locationProperties)){
    echo $locationProperties;
        exit;
        }
echo "<h2>".$inventoryEditingLocation ."
    &nbsp'".$locationProperties['inventory_location_name']. "'</h2>"; ?>
<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="POST" enctype="multipart/form-data">
    <table border="0">

        <tr><td><?php echo $inv_locationForm_name_label; ?></td><td><input type="text" name="locationName" value="<?php echo $locationProperties['inventory_location_name']; ?>" /></td></tr>

        <tr><td valign="top"><?php echo $inv_locationForm_description_label; ?></td><td><textarea rows="4" wrap="soft" title="<?php echo $inventory_location_describe_the_location;?> " name="locationDescription"><?php echo $locationProperties['inventory_location_description']; ?></textarea></td></tr>

        <tr><td>&nbsp;</td><td><input type="hidden" value="<?php echo $locationProperties['inventory_location_id']; ?>" name="location_id" /><input type="submit" name="update" value="<?php echo $inventory_update; ?>" class="button" /></td></tr>
    </table>
</form>