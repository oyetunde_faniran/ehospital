<?php
    //The IDs of all the departments that are pharmacies (Only users that belong to these departments can be managed from here)
    //$pharmDeptArray = array(13, 14);
    $userID = isset($_GET["id"]) ? (int)$_GET["id"] : 0;
    $userObj = new admin_user();
    
    if (!empty($_GET["c"])){
        echo "<div class=\"comments\">" . $_GET["c"] . "</div>";
    }
    
    if (empty($userID)){
        //A user should be selected first
        $allUsers = $userObj->getUsersByDept($_SESSION[session_id() . "deptID"]);
        
        if (count($allUsers) > 0){
            $usersDisplay = "";
            $sNo = 0;
            $rowStyle1 = " class=\"tr-row\" ";
            $rowStyle2 = " class=\"tr-row2\" ";
            $juggleRows = true;
            foreach ($allUsers as $user){
                $disLink = "index.php?p=inventory-manage-locations-access&m=pharmacy&id=" . $user["user_id"];
                $rowStyle = $juggleRows ? $rowStyle1 : $rowStyle2;
                $juggleRows = !$juggleRows;
                $usersDisplay .= "<tr $rowStyle>
                                        <td>" . (++$sNo) . ".</td>
                                        <td><a href=\"$disLink\">" . stripslashes($user["staff_title"] . " " . $user["staff_surname"] . " " . $user["staff_othernames"] . " ") . "</a></td>
                                        <td><a href=\"$disLink\">" . stripslashes($user["staff_employee_id"]) . "</a></td>
                                  </tr>";
            }
            $display = '<p>Please, select the user to be configured.</p>
                        <table cellpadding="3" cellspacing="3">
                            <tr class="title-row">
                                <th>&nbsp;</th>
                                <th>NAME</th>
                                <th>EMPLOYEE NUMBER</th>
                            </tr>
                             ' . $usersDisplay . '
                        </table>';
        } else $display = "There are no users in pharmacy department to configure.";
    } else {
        //Show the available locations, but get the details of the selected user and confirm that the currently logged-in user can configure locations for the currently selected user
        $disUser = $userObj->getUserForLocationConfig($userID, $_SESSION[session_id() . "deptID"]);
        $currentLocationsInfo = $userObj->getCurrentUserLocations($userID, $_SESSION[session_id() . "deptID"]);
        $currentLocations = array_keys($currentLocationsInfo);
        if (count($disUser) > 1){
            $invLoc = new inventory_location_class();
            $locations = $invLoc->getLocations();
            $locDisplay = '<table cellpadding="3" cellspacing="3">
                            <tr class="title-row">
                                <th>&nbsp;</th>
                                <th>Location</th>
                                <th><input type="checkbox" name="checkall" id="checkall" value="0" onclick="doCheck(this.checked, this.form);" /></th>
                            </tr>';
            $sNo = 0;
            $rowStyle1 = " class=\"tr-row\" ";
            $rowStyle2 = " class=\"tr-row2\" ";
            $juggleRows = true;
            foreach ($locations as $loc){
                $rowStyle = $juggleRows ? $rowStyle1 : $rowStyle2;
                $juggleRows = !$juggleRows;
                $checked = in_array($loc["inventory_location_id"], $currentLocations) ? " checked=\"checked\" " : "";
                $locDisplay .= "<tr $rowStyle>
                                    <td>" . (++$sNo) . ".</td>
                                    <td>" . stripslashes($loc["inventory_location_name"]) . "</td>
                                    <td><input $checked type=\"checkbox\" name=\"locations[]\" value=\"" . $loc["inventory_location_id"] . "\" /></td>
                                </tr>";
            }
            $locDisplay .= "    <tr>
                                    <td colspan=\"3\" align=\"center\">
                                        <input type=\"button\" value=\" Assign Locations \" class=\"btn\" name=\"sButton\" id=\"sButton\" onclick=\"doSubmit(this.form);\" />
                                    </td>
                                </tr>
                            </table>";
            $displayName = $disUser["staff_title"] . " " . $disUser["staff_surname"] . " " . $disUser["staff_othernames"];
            $display = '<p><em>Configuring locations for <strong>' . $displayName . '</strong></em></p>
                        <form action="" method="post" name=\"locationconfigureform\" id=\"locationconfigureform\">' 
                            . $locDisplay
                            . '<input type="hidden" value="1" name="l" />'
                            . '</form>';
            $display .= "<p><a href=\"index.php?p=inventory-manage-locations-access&m=pharmacy\">&laquo; Back to List of Users</a></p>";
        } else $display = "Please, <a href=\"index.php?p=inventory-manage-locations-access&m=pharmacy\">click here to select a user</a> whose location(s) would be configured.";
    }
    echo $display;
?>

<script type="text/javascript">
    function doCheck(checkAll, disForm){
        elemCount = disForm.length;
        for (k = 0; k < elemCount; k++){
            if (disForm.elements[k].type == "checkbox"){
                disForm.elements[k].checked = checkAll;
            }
        }
    }
    
    function doSubmit(disForm){
        if (confirm("Are you sure you want to assign the selected locations to the selected user?")){
            disForm.submit();
        }
    }
    
</script>