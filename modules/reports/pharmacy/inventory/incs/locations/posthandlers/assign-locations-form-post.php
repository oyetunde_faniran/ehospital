<?php
    //die ("<pre>" . print_r ($_POST, true) . "</pre>");
    
    $userID = isset($_GET["id"]) ? (int)$_GET["id"] : 0;
    $userObj = new admin_user();
    $disUser = $userObj->getUserForLocationConfig($userID, $_SESSION[session_id() . "deptID"]);
    $locations = isset($_POST["locations"]) ? $_POST["locations"] : array();
    if (count($disUser) > 1){
        $updated = $userObj->updateLocations($userID, $locations);
    }
    
    if ($updated){	//Operation successful
        $c = "The selected location(s) was/were successfully configured for the selected user.";
        $c = urlencode($c);
        $page = $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING'] . "&c=" . $c;
        header ("Location: $page");
    } else {
            $_GET["c"] = "Sorry! Operation failed. Please, try again.";
    }
?>