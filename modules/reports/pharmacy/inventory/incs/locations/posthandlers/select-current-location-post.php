<?php
    //Get the selected location
    $selectedLocation = isset($_POST["currentlocation"]) ? (int)$_POST["currentlocation"] : 0;
    
    //Confirm that the user has access to the location first, then set it to the current location if true
    $userObj = new admin_user();
    $assignedLocations = $userObj->getCurrentUserLocations($_SESSION[session_id() . "userID"], $_SESSION[session_id() . "deptID"]);
    $locationIDs = array_keys($assignedLocations);
    if (in_array($selectedLocation, $locationIDs)){
        //Save the location ID
        $_SESSION[session_id() . "inventory_locationID"] = $selectedLocation;
        
        //Save the location Name
        $locs = new inventory_location_class();
        $location = $locs->getLocationProperties($selectedLocation);
        if (is_array($location)){
            $_SESSION[session_id() . "inventory_locationName"] = $location["inventory_location_name"];
        } else {
            $_SESSION[session_id() . "inventory_locationName"] = "";
        }
        
        $c = urlencode("Location was selected successfully.");
        $page = $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING'] . "&c=" . $c;
        header ("Location: $page");
    }
?>