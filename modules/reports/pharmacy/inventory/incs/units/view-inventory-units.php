<?php
    $invUnit = new inventory_units();
    $allUnits = $invUnit->getAllUnits(false);
    if (count($allUnits) > 0){
        $display = "<table cellpadding=\"3\" cellspacing=\"3\">
                        <tr class=\"title-row\">
                            <td>" . $report_admissionbook_sno . "</td>
                            <td>" . $inventory_item_unit_name_label . "</td>
                            <td>" . $inventory_item_unit_desc_label . "</td>
                            <td>" . $inventory_item_unit_enabled_label . "</td>
                            <td><em>" . $inventory_item_unit_edit_label . "</em></td>
                        </tr>";
        $sNo = 0;
        $juggleRows = true;
        $rowStyle1 = " class=\"tr-row\" ";
        $rowStyle2 = " class=\"tr-row2\" ";
        foreach ($allUnits as $unit){
            $rowStyle = $juggleRows ? $rowStyle1 : $rowStyle2;
            $juggleRows = !$juggleRows;
            $display .= "<tr $rowStyle>
                            <td>" . (++$sNo) . ".</td>
                            <td>" . stripslashes($unit["inventory_unit_name"]) . "</td>
                            <td>" . stripslashes($unit["inventory_unit_desc"]) . "</td>
                            <td>" . ($unit["inventory_unit_enabled"] == 1 ? $yes_label : $no_label) . "</td>
                            <td><a href=\"index.php?p=inventory-edit-unit&m=pharmacy&id=" . $unit["inventory_unit_id"] . "\"><img border=\"0\" src=\"images/edit.png\" title=\"" . $inventory_item_unit_edit_label . "\" alt=\"" . $inventory_item_unit_edit_label . "\" /></a></td>
                         </tr>";
        }
        $display .= "</table>";
    } else $display = $inventory_item_unit_no_units;
    echo $display;
?>
