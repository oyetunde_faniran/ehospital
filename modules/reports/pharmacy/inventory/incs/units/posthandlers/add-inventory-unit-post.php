<?php
    //die ("<pre>" . print_r ($_POST, true) . "</pre>");
    
    $unitName = isset($_POST["itemunitname"]) ? $_POST["itemunitname"] : "";
    $unitDescription = isset($_POST["itemdescription"]) ? $_POST["itemdescription"] : "";
    
    $invUnit = new inventory_units();
    $error = $invUnit->createUnit($unitName, $unitDescription, 1, $_SESSION[session_id() . "userID"]);
    
    if (!$error){	//Operation successful
        $c = "Item unit was added successfully.";
        $c = urlencode($c);
        $page = $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING'] . "&c=" . $c;
        header ("Location: $page");
    } else {
            $_GET["c"] = !empty($invUnit->errorMsg) ? $invUnit->errorMsg : "Sorry! Operation failed. Please, try again.";
    }
?>