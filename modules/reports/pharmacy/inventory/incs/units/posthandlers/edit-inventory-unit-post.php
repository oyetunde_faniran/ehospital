<?php
    //die ("<pre>" . print_r ($_POST, true) . "</pre>");
    
    $unitName = isset($_POST["itemunitname"]) ? $_POST["itemunitname"] : "";
    $unitDescription = isset($_POST["itemdescription"]) ? $_POST["itemdescription"] : "";
    $enabled = isset($_POST["enabled"]) ? true : false;
    $unitID = isset($_GET["id"]) ? (int)$_GET["id"] : 0;
    
    $invUnit = new inventory_units();//$unitID, $unitName, $unitDescription, $enabled, $userID
    $error = $invUnit->updateUnit($unitID, $unitName, $unitDescription, $enabled, $_SESSION[session_id() . "userID"]);
    
    if (!$error){	//Operation successful
        $c = "Your changes were successfully saved.";
        $c = urlencode($c);
        $page = $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING'] . "&c=" . $c;
        header ("Location: $page");
    } else {
            $_GET["c"] = !empty($invUnit->errorMsg) ? $invUnit->errorMsg : "Sorry! Operation failed. Please, try again.";
    }
?>