<?php
    $disUnit = isset($_POST) ? $_POST : array();
    echo isset($_GET["c"]) ? "<div class=\"comments\">" . $_GET["c"] . "</div>" : "";
?>

<form action="" method="post">
    <table cellpadding="3" cellspacing="3">
        <tr>
            <td>
                <label for="item-unit-name"><?php echo strtoupper($inventory_item_unit_name) . ":"; ?></label>
            </td>
            <td>
                <input type="text" name="itemunitname" id="item-unit-name" value="<?php echo isset($disUnit["itemunitname"]) ? $disUnit["itemunitname"] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                <label for="item-unit-description"><?php echo strtoupper($inventory_item_unit_desc) . ":"; ?></label>
            </td>
            <td>
                <textarea name="itemdescription" id="item-unit-description" rows="5" cols="20"><?php echo isset($disUnit["itemdescription"]) ? $disUnit["itemdescription"] : ""; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <input type="submit" name="sButton" id="submit-button" value="<?php echo ucwords($inventory_item_unit_submit_button); ?>" class="btn" />
            </td>
        </tr>
    </table>
</form>