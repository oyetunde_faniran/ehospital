<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<h4><?php echo $report_outpatmonthlyatt_header; ?></h4>

<div>
	<div>&nbsp;</div><div>&nbsp;</div>
        <form action="index.php?<?php echo $_SERVER['QUERY_STRING']; ?>" method="post" name="filterform" id="filterform">
        	<p><?php echo $report_outpatmonthlyatt_story1; ?></p>
            <table width="200" border="0" cellspacing="5" cellpadding="5">
              <tr>
                <td><strong><?php echo $report_outpatmonthlyatt_form_month; ?></strong></td>
                <td><strong><?php echo $report_outpatmonthlyatt_form_year; ?></strong></td>
                <td>&nbsp;</td>
              </tr>


              <tr>
                <td>
                    <select name="month" id="month" onchange="this.form.w.value = this.options[this.selectedIndex].text;">
<?php
	$months = array ("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	$curMonth = (int)date ("m");
	foreach ($months as $k=>$m){
		$k++;
		if (!$_POST && $curMonth == $k)
			echo "<option value=\"$k\" selected=\"selected\">$m</option>\n";
		elseif ($_POST && $_POST["month"] == $k)
			 echo "<option value=\"$k\" selected=\"selected\">$m</option>\n";
			else echo "<option value=\"$k\">$m</option>\n";
	}
?>
                    </select>
                    <input type="hidden" name="w" value="" />
                    <script language="javascript" type="text/javascript">
						disForm = document.getElementById("filterform");
						if (disForm){
							disForm.w.value = disForm.dept.options[disForm.month.selectedIndex].text;
						}
					</script>
                </td>
                <td>
                    <select name="year" id="year" onchange="if (this.options[this.selectedIndex].text == 'All') document.getElementById('fromdate').selectedIndex = this.selectedIndex;">
						<?php
							$dis = new report_attendance();
							echo $dis->getYears(date("Y"));
                        ?>
                    </select>
                </td>
                <td><input type="submit" value="<?php echo $report_show; ?>" name="sButon" /></td>
              </tr>
            </table>
        </form>
</div>

<div>
<?php
	if ($_POST){
		echo "<div>&nbsp;</div><hr /><div>&nbsp;</div>";
		$result = $dis->getResults();
		if (!$result === false){
			echo "<p><strong>$report_outpatmonthlyatt_form_month: </strong>";
			switch($_POST["month"]){
				case 1:		echo $report_outpatmonthlyatt_jan; break;
				case 2:		echo $report_outpatmonthlyatt_feb; break;
				case 3:		echo $report_outpatmonthlyatt_mar; break;
				case 4:		echo $report_outpatmonthlyatt_apr; break;
				case 5:		echo $report_outpatmonthlyatt_may; break;
				case 6:		echo $report_outpatmonthlyatt_jun; break;
				case 7:		echo $report_outpatmonthlyatt_jul; break;
				case 8:		echo $report_outpatmonthlyatt_aug; break;
				case 9:		echo $report_outpatmonthlyatt_sep; break;
				case 10:	echo $report_outpatmonthlyatt_oct; break;
				case 11:	echo $report_outpatmonthlyatt_nov; break;
				case 12:	echo $report_outpatmonthlyatt_dec; break;
				default:	echo $report_outpatmonthlyatt_unknown = "Unknown";
			}
			echo "</p>";
			echo $result;
		} else echo $report_outpatmonthlyatt_noresult;
	}
?>
</div>