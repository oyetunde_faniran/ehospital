<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<h4><?php echo $report_admissionbook_header; ?></h4>
<div>
	<div>&nbsp;</div><div>&nbsp;</div>
        <form action="index.php?<?php echo $_SERVER['QUERY_STRING']; ?>" method="post" name="filterform" id="filterform">
            <table width="200" border="0" cellspacing="5" cellpadding="5">
              <tr>
                <td nowrap="nowrap"><strong><?php echo $report_admissionbook_ward; ?></strong></td>
                <td nowrap="nowrap"><strong><?php echo $report_admissionbook_form_fromdatefield; ?></strong></td>
                <td nowrap="nowrap"><strong><?php echo $report_admissionbook_form_todatefield; ?></strong></td>
                <td>&nbsp;</td>
              </tr>


              <tr>
                <td>
                    <select name="wards" id="wards">
						<?php
							$dis = new report_admissionbook();
							$query = "SELECT DISTINCT w.ward_id, lc.$curLangField ward 
										FROM language_content lc INNER JOIN wards w
										ON lc.langcont_id = w.langcont_id
										ORDER BY ward";
							echo $dis->getWards($query);
						?>
                    </select>
                </td>
                <td>
                    <select name="fromdate" id="fromdate" onchange="if (this.options[this.selectedIndex].text == 'All') document.getElementById('todate').selectedIndex = this.selectedIndex;">
						<?php
							echo $dis->getDates("fromdate");
						?>
                    </select>
                </td>
                <td>
                    <select name="todate" id="todate" onchange="if (this.options[this.selectedIndex].text == 'All') document.getElementById('fromdate').selectedIndex = this.selectedIndex;">
						<?php echo $dis->getDates("todate"); ?>
                    </select>
                </td>
                <td><input type="submit" value="<?php echo $report_show; ?>" name="sButon" /></td>
              </tr>
            </table>
        </form>
</div>
    <div>
<?php
	if ($_POST && isset($_POST["fromdate"], $_POST["todate"], $_POST["wards"])){
		echo "<div>&nbsp;</div><div>&nbsp;</div>";
		$common = new report_common();

		$query = "SELECT reg.reg_hospital_no id, reg.reg_surname sname, reg.reg_othernames oname, reg.reg_address addy, 
						reg.reg_gender sex, get_age(reg.reg_dob) age, reg.reg_civilstate cstate, 
						lc.$curLangField ward, 
						CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) doctor, 
						IFNULL(inp.inp_datedischarged, 'Not Discharged Yet') ddate, inp.inp_dateattended date, inp.patadm_id
					FROM inpatient_admission inp 
						INNER JOIN patient_admission pat 
						INNER JOIN registry reg 
						INNER JOIN wards w 
						INNER JOIN consultant c 
						INNER JOIN staff s 
						INNER JOIN language_content lc
					ON inp.patadm_id = pat.patadm_id AND pat.reg_hospital_no = reg.reg_hospital_no 
						AND inp.ward_id = w.ward_id 
						AND inp.consultant_id = c.consultant_id AND c.staff_employee_id = s.staff_employee_id 
						AND w.langcont_id = lc.langcont_id
					WHERE true ";


		//Apply date filter if need be
		$date = $common->formatDate ($_POST["fromdate"], $_POST["todate"]);
		if (!$date["noFilter"])
			$query .= " AND ((inp.inp_dateattended BETWEEN ({$date['fromdate']}) AND ({$date['todate']})) 
							OR (inp.inp_dateattended BETWEEN ({$date['todate']}) AND ({$date['fromdate']})))";

		//Apply ward filter if need be
		if ($_POST["wards"] != "All"){
			$ward = (int)$_POST["wards"];
			$query .= " AND w.ward_id = '$ward'";
		}


		$result = $dis->getResults($query);
		if ($result !== false){
			$final = "<table border=\"1\" >";
			if ($_POST["fromdate"] != "All" && $_POST["todate"] != "All"){
				if ($_POST["fromdate"] == $_POST["todate"])
					$final .= sprintf("<tr><td colspan=\"13\" align=\"center\"><strong>$report_admissionbook_daterange%s</strong></td></tr>", $_POST["todate"]);
				else $final .= sprintf("<tr><td colspan=\"13\" align=\"center\"><strong>$report_admissionbook_daterange%s - %s</strong></td></tr>", $_POST["fromdate"], $_POST["todate"]);
			} else $final .= "<tr><td colspan=\"13\" align=\"center\"><strong>$report_admissionbook_daterange_alltime</strong></td></tr>";
			$final .=		"<tr>
								<th nowrap='nowrap'>$report_admissionbook_sno</th>
								<th nowrap='nowrap'>$report_admissionbook_admdate</th>
								<th nowrap='nowrap'>$report_admissionbook_no</th>
								<th nowrap='nowrap'>$report_admissionbook_sname</th>
								<th nowrap='nowrap'>$report_admissionbook_oname</th>
								<th nowrap='nowrap'>$report_admissionbook_addy</th>
								<th nowrap='nowrap'>$report_admissionbook_sex</th>
								<th nowrap='nowrap'>$report_admissionbook_age</th>
								<th nowrap='nowrap'>$report_admissionbook_cstate</th>
								<th nowrap='nowrap'>$report_admissionbook_ward</th>
								<th nowrap='nowrap'>$report_admissionbook_consultant</th>
								<th nowrap='nowrap'>$report_admissionbook_ddate</th>
								<th nowrap='nowrap'>$report_admissionbook_diagnosis</th>
							</tr>";
			$counter = 0;
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$counter++;
				$final .= "<tr>
								<td align=\"center\">$counter.</td>
								<td>{$row['date']}</td>
								<td align=\"center\">{$row['id']}</td>
								<td>{$row['sname']}</td>
								<td>{$row['oname']}</td>
								<td>{$row['addy']}</td>
								<td align=\"center\">";
				$final .= 		$row['sex'] == 0 ? $report_male : $report_female;
				$final .=		"</td>
								<td align=\"center\">{$row['age']}</td>
								<td>{$row['cstate']}</td>
								<td>{$row['ward']}</td>
								<td>{$row['doctor']}</td>
								<td>{$row['ddate']}</td>
								<td>" . $dis->getDiagnosis($row["patadm_id"]) . "</td>";
			}	//END while
			$final .= "</table>";
		} else $final = $report_admissionbook_error_noresult;
		echo $final;
	}
?> 
	</div>