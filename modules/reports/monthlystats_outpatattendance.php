<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.



//Get needed vars for search
include_once ("monthlystats_dateinit.php");

$common  = new report_common();

	if ($showReport){
		echo "<div>&nbsp;</div>
				<div><hr /></div>
				<div>&nbsp;</div>";
		$title = "<h2>$report_monthlystats_outpatattendance $report_monthlystats_inpsummary_for <em>$displayDate</em>.</h2>";
		include_once ("printview.php");
		//	echo "<h4>$report_monthlystats_outpatattendance $report_monthlystats_inpsummary_for " . $common->getMonth($month) . ", $year</h4>";
?>

<div>&nbsp;</div>
<script type="text/javascript" src="library/table2Excel.js"></script>
<div id="resultContainer">
<?php
	echo $title;
?>
	<div>&nbsp;</div>
    <table border="1" style="border-color: #999999;" id="reportTable">
    	<thead class="title-row">
        	<tr>
                <th><?php echo $report_monthlystats_sno; ?></th>
                <th><?php echo $report_monthlystats_dept; ?></th>
                <th colspan="2"><?php echo $report_monthlystats_newpatient; ?></th>
                <th colspan="2"><?php echo $report_monthlystats_oldpatient; ?></th>
                <th colspan="2"><?php echo $report_monthlystats_totalatt; ?></th>
                <th><?php echo $report_monthlystats_clinicsessions; ?></th>
			</tr>
        </thead>

		<tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>
            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>
            <td><?php echo strtoupper($sex_male); ?></td>
            <td><?php echo strtoupper($sex_female); ?></td>
            <td>&nbsp;</td>
        </tr>

<?php
	$dis = new report_monthlystats_outpatattendance($month, $year);
	$deptQuery = "SELECT dept.dept_id, lc.$curLangField dept FROM department dept 
					INNER JOIN language_content lc 
					ON lc.langcont_id = dept.langcont_id
					ORDER BY dept";
	$result = $dis->getResults($deptQuery, $report_total, $report_gTotal);
	if (!empty($result[0])){
		echo $result[0];
		
	} else echo "<tr><td colspan=\"7\">$report_monthlystats_dept_error</td></tr>";
?>
    </table>
</div>
<?php
		$exportForm =  "<form method=\"post\" id=\"xc\" name=\"xform\" target=\"_blank\" action=\"export.php?{$_SERVER['QUERY_STRING']}&action=excel\">
							<input type=\"hidden\" name=\"t\" value='$title' />
							<input type=\"hidden\" name=\"v\" value='1' />
							<input type=\"hidden\" name=\"h\" value='1' />
						</form>";
		echo $exportForm;
	}
?>