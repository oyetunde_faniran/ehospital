<?php
require_once("././library/lab/checks.php");
$labdb=new DBConf(); //initialize a dbconnection object
$maindbname=$labdb->dbname;
$labdbname=$labdb->labdbname;
$labdb->labConnect();//point to the lab db
$check=new checks();//retrieve all post vars

//Registration of new lab user
if(isset($submit_lab_reg) && $submit_lab_reg==$lab_reg_submit){	
$error_id=false; $regsuccess=true; $lab_gen_pin=0; $error_reg_hospital_no=false; $error_reg_hospital_exists=false;
	if ($checked!="true"){//client side validation wasn't successful
//		if(empty($lab_pin)){$error_lab_pin=true; $regsuccess=false;}
		if(empty($surname) && empty($othernames) && empty($reg_hospital_no)){$error_id=true; $regsuccess=false;}
		}
	if(strlen($reg_hospital_no)>1){//check if submitted hospital no is valid
		$gg=$maindbname.".registry"; $qq="SELECT * FROM $gg where reg_hospital_no='$reg_hospital_no'";
		$checkno=$labdb->execute($qq);
		if (mysql_num_rows($checkno)<1){$error_reg_hospital_no=true; $regsuccess=false;}
		}
	if(strlen($reg_hospital_no)>1){//check if submitted hospital no is already in use
		$labdb->labConnect();//point to the lab db
		$qq="SELECT * FROM lab_registry where reg_hospital_no='$reg_hospital_no'";
		$checkno=$labdb->execute($qq);
		if (mysql_num_rows($checkno)>0){$error_reg_hospital_exists=true; $regsuccess=false;}
		}		
	if ($regsuccess){
		if(strlen($reg_hospital_no)>1){$reg_query="INSERT INTO lab_registry (reg_hospital_no, lab_date, staff_employee_id, user_comment) VALUES ('$reg_hospital_no', curdate(), '$staff_employee_id', '$user_comment')";}//Fill data for luth patient
		else {$reg_query="INSERT INTO lab_registry (lab_date, surname, othernames, age, sex, staff_employee_id, user_comment) VALUES (curdate(), '$surname', '$othernames', year(curdate())-$age, '$sex', '$staff_employee_id', '$user_comment')";}
		$labdb->execute($reg_query);
		if($labdb){$regsuccess=true; $lab_gen_pin=mysql_insert_id();} else {$regsuccess=false;}
		}

$returnurl="&error_id=$error_id&regsuccess=$regsuccess&lab_gen_pin=$lab_gen_pin&error_reg_hospital_no=$error_reg_hospital_no&error_reg_hospital_exists=$error_reg_hospital_exists";
header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}$returnurl");
exit;
}

//Modification of lab user
if(isset($submit_lab_reg) && $submit_lab_reg==$lab_reg_update){	
$error_lab_pin=false; $error_id=false; $modsuccess=true;
	if ($checked!="true"){//client side validation wasn't successful
		if(empty($labpin)){$error_lab_pin=true; $modsuccess=false;}
		if(empty($surname) && empty($othernames) && empty($reg_hospital_no)){$error_id=true; $modsuccess=false;}
		}
	if ($modsuccess){
		if(strlen($reg_hospital_no)>1){
			$reg_query="UPDATE lab_registry SET reg_hospital_no='$reg_hospital_no', staff_employee_id='$staff_employee_id', user_comment='$user_comment' WHERE lab_pin='$lab_pin'";}//Fill data for luth patient
		else {$reg_query="UPDATE lab_registry SET reg_hospital_no='$reg_hospital_no', surname='$surname', othernames='$othernames', sex='$sex', staff_employee_id='$staff_employee_id', user_comment='$user_comment', age=year(curdate())-$age WHERE lab_pin='$labpin'";}
		$labdb->execute($reg_query);
		if($labdb){$modsuccess=true;} else {$modsuccess=false;}
		}

$returnurl="&error_lab_pin=$error_lab_pin&error_id=$error_id&modsuccess=$modsuccess";
header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}$returnurl");
exit;
}

/*
//Deletion of user
if(isset($lab_user_delete)){	
		$delete_query="DELETE FROM lab_registry where lab_pin='$labpin'";
		$labdb->execute($delete_query);
		if($labdb){$delsuccess=urlencode("User has been successfully deleted");} else {$delsuccess=urlencode("User was NOT successfully deleted");}

$returnurl="&delsuccess=$delsuccess";
header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}$returnurl");
exit;
}
*/
?>