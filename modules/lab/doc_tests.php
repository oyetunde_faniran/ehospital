<script type="text/javascript" src="library/lab/checks.js"></script>
<?php
require_once("././library/lab/checks.php");
$labdb=new DBConf(); //initialize a dbconnection object
$maindbname=$labdb->dbname;
$labdbname=$labdb->labdbname;
$labdb->labConnect();//point to the lab db
$check=new checks();//retrieve all post vars
?>
<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>

<?php //Form submissiion responses display
if(isset($requestsuccess)){
	if($requestsuccess==true){$color="color:blue;";} else {$color="color:red";}
	echo "<ul style=\"$color\">";
	if($requestsuccess==true){echo "<strong>$lab_test_positive : <span style=\"font-size:larger;\" >$lab_no</span>.</strong>";}
	else{//request has failed
		echo "<strong>$lab_test_negative</strong>";
		if($error_test){echo "<li>$lab_test_negative_test</li>";}
		if($bad_lab_pin){echo "<li>$lab_test_negative_pin</li>";}
		if($bad_reg_hospital_no){echo "<li>$lab_test_negative_number</li>";}
		}
	echo "</ul>";
}
?>

<h3 class="lab-section-head" style="color:#003366; font-size:13px; font-style:normal; padding:7px;" id="testreg"><span style="padding-left:1.6em;"><?php echo $lab_test_home_reg; ?></span></h3>
<div>


    <?php
	$lsdeptquery="select lang1, dept_id from {$maindbname}.language_content, mediluth_lab.lab_dept where
{$maindbname}.language_content.langcont_id = mediluth_lab.lab_dept.langcont_id";
	//$lsdept=new DBConf(true);
	$depts=$labdb->execute($lsdeptquery);

	If (isset($_POST['lab_test_id']) and !empty($_POST['lab_test_id'])){//page was previously submitted to get analytes
		$lab_test_id=$_POST['lab_test_id'];
		//$complete="and dept_id='"."$dept_id'"; //string to complete query
		$displayformrow="";
		$displaygetunits="display:none;";
		}
	else {
		$lab_test_id=0;
		//$complete="";
		$displayformrow="display:none;";
		$displaygetunits="";
		}
?>

  <form action="" method="post" id="getanalytesform" name="getanalytesform" onsubmit="return testCheck();">
 <table width="100%" border="0" class="lab-form-table">
  <tr>
	<td width="33%" style="text-align:right; margin-right:10px;"><label><?php echo $lab_test_home_instruct; ?></label></td>
    <td width="67%" align="left"><select name="lab_test_id" id="lab_test_id" onchange="return testCheck();">
      <?php
	   echo "<option value=\"0\">Select a Test</option>";
	   while($deptsrow=mysql_fetch_array($depts, MYSQL_ASSOC)){
		   $dept_id=$deptsrow['dept_id'];
		   $testsquery="select lang1, lab_test_id from {$maindbname}.language_content, mediluth_lab.lab_test where
				{$maindbname}.language_content.langcont_id = mediluth_lab.lab_test.langcont_id AND
				mediluth_lab.lab_test.dept_id='$dept_id' order by lang1";
			$tests=$labdb->execute($testsquery);
            echo "<optgroup label=\"{$deptsrow['lang1']}\">";
			while($testsrow=mysql_fetch_array($tests, MYSQL_ASSOC)){
					if (isset($_POST['lab_test_id']) AND $lab_test_id==$testsrow['lab_test_id']){$selecteddept="selected=\"selected\"";} else {$selecteddept="";}
					echo "<option value=\"".$testsrow['lab_test_id']."\"$selecteddept>".$testsrow['lang1']."</option>";
					}//end inner while
			echo "</optgroup>";
           }
		  ?>
    </select></td>
  </tr>

 <noscript>
  <tr style="<?php echo $displaygetunits; ?>">
      <td>&nbsp;</td>
      <td align="left"><input class="btn" name="getunits" type="submit" value="<?php echo $lab_tests_home_getunits; ?>" /></td>
  </tr>
</noscript>

<tr style="<?php echo $displayformrow; ?> vertical-align:top;">
<td style="text-align:right; margin-right:10px;"><?php echo $lab_test_reg_hospitalno; ?></td>
<td>
<?php
	$hospitalNo = isset($_GET["hospitalno"]) ? $_GET["hospitalno"] : "";
?>
	<input type="text" id="reg_hospital_no" name="reg_hospital_no" value="<?php echo $hospitalNo; ?>" readonly="readonly" />
    <input type="hidden" name="reghospitalno" value="reghospitalno" /></td>
</tr>


<?php
//get currently selected test id and use in query to get analytes
$lstitlequery="select specimen_id, specimen from specimen where lab_test_id='$lab_test_id' order by specimen";
$lsspecimens=$labdb->execute($lstitlequery);
if (mysql_num_rows($lsspecimens)<1){$displayspecimen="display:none;";} else {$displayspecimen="";}
?>

<tr style="<?php echo $displayspecimen; ?> vertical-align:top;">
<td style="text-align:right; margin-right:10px;"><?php echo $lab_test_specimen; ?></td>
<td>
  <table width="300px" border="0" class="lab-table" style="<?php echo $displayspecimen; ?>">

<?php
	  $i=0;
	  while($specimentitles=mysql_fetch_array($lsspecimens)){
		  if ($i%2==0){$class="class=\"tr-row\"";} else {$class="class=\"tr-row2\"";} $i++; //to handle zebra stripes
      echo "<tr $class>";
      echo "<td><input type=\"checkbox\" name=\"specimen_{$specimentitles['specimen_id']}\" id=\"specimen_{$specimentitles['specimen_id']}\" value=\"{$specimentitles['specimen_id']}\"  />".$specimentitles['specimen']."</td>";
	  echo "</tr>";
	  }
	?>
 </table>
</td>
</tr>

<tr style="<?php echo $displayformrow; ?> vertical-align:top;">
<td style="text-align:right; margin-right:10px;"><?php echo $lab_test_other_specimen; ?></td>
<td><input type="text" id="other" name="other" style="width:250px" /></td>
</tr>

<tr style="<?php echo $displayformrow; ?> vertical-align:top;">
<td style="text-align:right; margin-right:10px;"><?php echo $lab_test_comments; ?></td>
<td><textarea id="comments" name="comments" rows="5" style="width:250px"></textarea></td>
</tr>

	<?php
//get currently selected test id and use in query to get analytes
$lstitlequery="select analyte_id, analyte, extra from analytes where lab_test_id='$lab_test_id' AND extra NOT IN('special') order by analyte";
$lstitle=$labdb->execute($lstitlequery);
if (mysql_num_rows($lstitle)<1){$displayanalytes="display:none;";} else {$displayanalytes="";}
?>
<tr style="<?php echo $displayanalytes; ?> vertical-align:top;">
<td style="text-align:right; margin-right:10px;"><?php echo $lab_test_analytes; ?></td>
<td>

  <table width="300px" border="0" class="lab-table" style="<?php echo $displayanalytes; ?>">

<?php
	  $i=0;
	  while($titles=mysql_fetch_array($lstitle)){
		  if ($i%2==0){$class="class=\"tr-row\"";} else {$class="class=\"tr-row2\"";} $i++; //to handle zebra stripes
      echo "<tr $class>";
      echo "<td><input type=\"checkbox\" name=\"analyte_{$titles['analyte_id']}\" id=\"analyte_{$titles['analyte_id']}\" value=\"{$titles['analyte_id']}\"  />".$titles['analyte']."</td>";
	  echo "</tr>";
	  }
	?>
 </table>

</td>
</tr>

  <tr style="<?php echo $displayformrow; ?> vertical-align:top;">
      <td>&nbsp;</td>
      <td align="left">
      	<input class="btn" name="regrequest" id="regrequest" type="submit" value="<?php echo $lab_test_regrequest; ?>" style="margin-top:20px;" onclick="setTestNames();" />
<script type="text/javascript">
	function setTestNames(){
		dSel = document.getElementById("lab_test_id");
		dTest = dSel.options[dSel.selectedIndex].text;
		opener.document.getElementById("lab[investigation]").value = opener.document.getElementById("lab[investigation]").value + "\n" + dTest;
		//alert ("TEST NAME: " + opener.document.getElementById("lab[investigation]").value);
		//opener.document.getElementById("labrequests").value) = "";
	}
</script>
        <!--<button>Test</button>-->
	  </td>
  </tr>
</table>
</form>

 </div>
 </div>
