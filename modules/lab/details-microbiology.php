<?php
    //Fetch existing results if available
    $trans_id = isset($_REQUEST['transid']) ? (int)$_REQUEST['transid'] : 0;
    $query_micro = "SELECT * FROM lab_results_microbiology
              WHERE lab_trans_id = '$trans_id'";
    $result_micro = mysql_query($query_micro);
    if (mysql_affected_rows() > 0){
        $row = mysql_fetch_assoc($result_micro);
        $macroscopic = $row['microresult_macroscopic'];
        $microscopy = $row['microresult_microscopic'];
        $direct = $row['microresult_direct'];
        $concentration = $row['microresult_concentration'];
    } else {
        $macroscopic = '';
        $microscopy = '';
        $direct = '';
        $concentration = '';
    }

    //Fetch existing anti-biotic results if available
    $query_anti = "SELECT * FROM lab_results_antibiotics
              WHERE lab_trans_id = '$trans_id'";
    $result_anti = mysql_query($query_anti);
    $existing_anti_array = array();
    if (mysql_affected_rows() > 0){
        while ($row = mysql_fetch_assoc($result_anti)){
            $existing_anti_array[$row['antibio_id']] = $row['antibioresult_result'];
        }
    }









    $display = '';
    $display .= "<p style=\"clear: both; margin-bottom:10px; margin-left:20px; margin-right: 20px; border:1px solid #CCC; width:640px; padding:10px; text-align:center;\">
                        <span style=\"font-size: 16px;\">Macroscopic</span>
                        <textarea name=\"macroscopic\" rows=\"5\" style=\"width:100%;\">$macroscopic</textarea>
                   </p>";
    $display .= "<p style=\"clear: both; margin-bottom:10px; margin-left:20px; margin-right: 20px; border:1px solid #CCC; width:640px; padding:10px; text-align:center;\">
                        <span style=\"font-size: 16px;\">Microscopy</span>
                        <textarea name=\"microscopy\" rows=\"5\" style=\"width:100%;\">$microscopy</textarea>
                   </p>";
    $display .= "<p style=\"clear: both; margin-bottom:10px; margin-left:20px; margin-right: 20px; border:1px solid #CCC; width:640px; padding:10px; text-align:center;\">
                        <span style=\"font-size: 16px;\">Direct</span>
                        <textarea name=\"direct\" rows=\"5\" style=\"width:100%;\">$direct</textarea>
                   </p>";
    $display .= "<p style=\"clear: both; margin-bottom:10px; margin-left:20px; margin-right: 20px; border:1px solid #CCC; width:640px; padding:10px; text-align:center;\">
                        <span style=\"font-size: 16px;\">Concentration</span>
                        <textarea name=\"concentration\" rows=\"5\" style=\"width:100%;\">$concentration</textarea>
                   </p>";

    //Show the list of anti-biotics with the required options
    $query = "SELECT * FROM lab_antibiotics
              WHERE antibio_enabled = '1'
              ORDER BY antibio_name";
    //die($query);
    $conn = new DBConf();
    $db = $conn->labdbname;
    $link_id = $conn->getConnectionID();
    mysql_select_db($db, $conn->getConnectionID());
    $result = mysql_query($query);
    if (mysql_affected_rows($link_id) > 0){
        $display .= '<div style="clear: both; margin-bottom:10px; margin-left:20px; margin-right: 20px; border:1px solid #CCC; width:640px; padding:10px; text-align:left; position: relative; height: 200px; overflow: scroll;">
                        <span style="font-size: 16px; color: #F60; display: inline-block; margin-bottom: 20px;">Sensitivity</span>';
        while ($row = mysql_fetch_assoc($result)){
            $dis_anti_value = isset($existing_anti_array[$row['antibio_id']]) ? $existing_anti_array[$row['antibio_id']] : 'Untested';
            $display .= '<div style="position: relative; border-bottom: 1px solid #CCC; padding: 3px;">
                            <span style="display: inline-block; color: #000; width: 200px;">' . stripslashes($row['antibio_name']) . '</span>
                            <select name="antibiotics[' . $row['antibio_id'] . ']" id="antibiotics_' . $row['antibio_id'] . '">
                                <option ' . ($dis_anti_value == 'Untested' ? 'selected="selected"' : '') . ' value="Untested">Untested</option>
                                <option ' . ($dis_anti_value == 'Resistant' ? 'selected="selected"' : '') . ' value="Resistant">Resistant</option>
                                <option ' . ($dis_anti_value == 'Intermediate' ? 'selected="selected"' : '') . ' value="Intermediate">Intermediate</option>
                                <option ' . ($dis_anti_value == 'Sensitive' ? 'selected="selected"' : '') . ' value="Sensitive">Sensitive</option>
                            </select>
                         </div>';
        }   //END while
        $display .= '</div>';
    }   //END if rows were found

    echo $display;