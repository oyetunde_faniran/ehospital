<?php

require_once("././library/lab/checks.php");
$labdb = new DBConf(); //initialize a dbconnection object
$maindbname = $labdb->dbname;
$labdbname = $labdb->labdbname;
$labdb->labConnect(); //point to the lab db
$check = new checks(); //retrieve all post vars
//Get the pathologist report if available
$path_report = isset($_POST['result_pathologist_report']) ? $_POST['result_pathologist_report'] : '';
$transid = (int)$_REQUEST['transid'];

//die('<pre>RESULT SAVE: ' . print_r($_POST, true));
//Submission of Test Results
if (isset($submitresult) && $submitresult == $lab_result_save) {
    $error_id = false;
    $resultsubmitsuccess = true;
    $error_reg_hospital_no = false;
    $i = 0;
//Get the analyte list
    $analytes = mysql_query("SELECT lab_results.analyte_id as analyteid, analyte, result FROM analytes,lab_results WHERE lab_results.lab_trans_id='{$_REQUEST['transid']}' AND analytes.analyte_id=lab_results.analyte_id;");
    while ($analytes_results = mysql_fetch_array($analytes)) {//update results
        $theanalyte = $analytes_results['analyteid'];
        mysql_query("UPDATE lab_results SET result = '{$_POST[$theanalyte]}' WHERE lab_trans_id=$transid AND analyte_id='{$analytes_results['analyteid']}'");
        if (!empty($_POST[$theanalyte])) {
            $i++;
        }//count non-empty results
    }
    if ($i > 0) {//ie one or more analyte results were recorded
        //update results: change status to preliminary and indicate reporting staff
        $update_query_save1 = "UPDATE lab_trans
                            SET status='preliminary',
                                result_by='$labstaff',
                                result_date=curdate(),
                                result_comments='$result_comments',
                                result_path_report = '$path_report'
                            WHERE lab_trans_id=$transid";
        //die("<pre>\n\nSAVE1: $update_query_save1");
        mysql_query($update_query_save1);
    } else {
        //update results: change status to preliminary and indicate reporting staff
        $update_query_save2 = "UPDATE lab_trans
                            SET status='preliminary',
                               result_by='$labstaff',
                               result_date=curdate(),
                               result_comments='$result_comments',
                               result_path_report = '$path_report'
                            WHERE lab_trans_id=$transid";
        //die("<pre>\n\nSAVE2: $update_query_save2");
        mysql_query($update_query_save2);
    }

    //Save any microbiolgy results, if any
    doMicroBiologySave($transid);

    $returnurl = "&resultsubmitsuccess=$resultsubmitsuccess";
    header("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}$returnurl");
    exit;
}





//Verification of Test Results
if (isset($_POST['verifyresult']) && $_POST['verifyresult'] == 'Verify Result') {
    $error_id = false;
    $resultverifysuccess = true;
    $transid = $_REQUEST['transid'];
    $i = 0;
//Get the analyte list
    $analytes = mysql_query("SELECT lab_results.analyte_id as analyteid, analyte, result FROM analytes,lab_results WHERE lab_results.lab_trans_id='{$_REQUEST['transid']}' AND analytes.analyte_id=lab_results.analyte_id;");
    while ($analytes_results = mysql_fetch_array($analytes)) {//update results for various analytes
        $theanalyte = $analytes_results['analyteid'];
        mysql_query("UPDATE lab_results SET result='{$_POST[$theanalyte]}' WHERE lab_trans_id=$transid AND analyte_id='{$analytes_results['analyteid']}'");
        if (!empty($_POST[$theanalyte])) {
            $i++;
        }//count non-empty results
    }
//Update result metadata in lab_trans also
    if ($i > 0) {//ie one or more analyte results were recorded
        //$path_report = isset($_POST['result_pathologist_report']) ? $_POST['result_pathologist_report'] : '';
        //update results: change status to validated and indicate reporting staff
        $update_query_validate = "UPDATE lab_trans
                                SET status='verified',
                                    verified_by='$labstaff',
                                    verified_date=curdate(),
                                    result_comments='$result_comments',
                                    result_path_report = '$path_report'
                                WHERE lab_trans_id=$transid";
        //die("<pre>\n\nVALIDATE: $update_query_validate");
        mysql_query($update_query_validate);
    } else {
        mysql_query("UPDATE lab_trans SET status='validated', authorised_by='$labstaff', authorised_date=curdate(), result_comments='$result_comments' WHERE lab_trans_id=$transid"); //update results: change status to validated and indicate reporting staff
    }

    //Save any microbiolgy results, if any
    doMicroBiologySave($transid);

    $returnurl = "&resultverifysuccess=$resultverifysuccess";
    header("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}$returnurl");
    exit;
}   //END verification








//Validation of Test Results
if (isset($validateresult) && $validateresult == $lab_result_validate) {
    $error_id = false;
    $resultvalidatesuccess = true;
    $transid = $_REQUEST['transid'];
    $i = 0;
//Get the analyte list
    $analytes = mysql_query("SELECT lab_results.analyte_id as analyteid, analyte, result FROM analytes,lab_results WHERE lab_results.lab_trans_id='{$_REQUEST['transid']}' AND analytes.analyte_id=lab_results.analyte_id;");
    while ($analytes_results = mysql_fetch_array($analytes)) {//update results for various analytes
        $theanalyte = $analytes_results['analyteid'];
        mysql_query("UPDATE lab_results SET result='{$_POST[$theanalyte]}' WHERE lab_trans_id=$transid AND analyte_id='{$analytes_results['analyteid']}'");
        if (!empty($_POST[$theanalyte])) {
            $i++;
        }//count non-empty results
    }
//Update result metadata in lab_trans also
    if ($i > 0) {//ie one or more analyte results were recorded
        //$path_report = isset($_POST['result_pathologist_report']) ? $_POST['result_pathologist_report'] : '';
        //update results: change status to validated and indicate reporting staff
        $update_query_validate = "UPDATE lab_trans
                                SET status='validated',
                                    authorised_by='$labstaff',
                                    authorised_date=curdate(),
                                    result_comments='$result_comments',
                                    result_path_report = '$path_report'
                                WHERE lab_trans_id=$transid";
        //die("<pre>\n\nVALIDATE: $update_query_validate");
        mysql_query($update_query_validate);
    } else {
        mysql_query("UPDATE lab_trans SET status='validated', authorised_by='$labstaff', authorised_date=curdate(), result_comments='$result_comments' WHERE lab_trans_id=$transid"); //update results: change status to validated and indicate reporting staff
    }

    //Save any microbiolgy results, if any
    doMicroBiologySave($transid);

    $returnurl = "&resultvalidatesuccess=$resultvalidatesuccess";
    header("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}$returnurl");
    exit;
}   //END result validation


function doMicroBiologySave($transid){
    /*
      $_POST is
      Array
      (
      [125] => Analyte Findings Here
      [macroscopic] => Macroscopic Findings Here
      [microscopy] => Microscopy Findings Here
      [direct] => Direct Findings Here
      [concentration] => Concentration Here
      [antibiotics] => Array
      (
      [16] => Untested
      [3] => Resistant
      )
      [result_comments] => General Summary & Comments Here
      )
     */

//************ BEGIN: Save microbiology results if available
    if (isset($_POST['antibiotics'])) {
        $antibiotics = $_POST['antibiotics'];
        //Remove existing entries for this lab transaction if any
        $query_del = "DELETE FROM lab_results_antibiotics WHERE lab_trans_id = '$transid'";
        mysql_query($query_del);

        //Now, insert the new values
        $query_ins_temp = "INSERT INTO lab_results_antibiotics (lab_trans_id, antibio_id, antibioresult_result) VALUES";
        foreach ($antibiotics as $antibio_id => $value) {
            $query_ins_temp .= "('$transid', '$antibio_id', '$value'),";
        }
        $query_ins = rtrim($query_ins_temp, ',');
        mysql_query($query_ins);
    }   //END if antibiotics are available

    if (isset($_POST['macroscopic'], $_POST['microscopy'], $_POST['direct'], $_POST['concentration'])) {
        //Check whether a row exists for this transaction in the table
        $query_check_micro = "SELECT * FROM lab_results_microbiology
                            WHERE lab_trans_id = '$transid'";
        $result = mysql_query($query_check_micro);
        if (mysql_affected_rows() > 0) { //It exists, so, do UPDATE
            $query = "UPDATE lab_results_microbiology
                    SET microresult_macroscopic = '{$_POST['macroscopic']}',
                        microresult_microscopic = '{$_POST['microscopy']}',
                        microresult_direct = '{$_POST['direct']}',
                        microresult_concentration = '{$_POST['concentration']}'
                    WHERE lab_trans_id = '$transid'";
        } else {    //It does not exist so do INSERT
            $query = "INSERT INTO lab_results_microbiology
                    SET lab_trans_id = '$transid',
                        microresult_macroscopic = '{$_POST['macroscopic']}',
                        microresult_microscopic = '{$_POST['microscopy']}',
                        microresult_direct = '{$_POST['direct']}',
                        microresult_concentration = '{$_POST['concentration']}'";
        }
        mysql_query($query);
    }   //END if microbiolgy results are available
//************ END: Save microbiology results if available
}