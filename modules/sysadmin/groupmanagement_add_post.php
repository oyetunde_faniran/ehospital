<?php
	$group = new admin_group($curLangField);
	$error = $group->createGroup($_POST, $admin_groupmanagement_error);
	if (!$error)
		header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=$admin_groupmanagement_createsuccess");
	else {
		if (!empty($group->errorMsg))
			header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=" . $group->errorMsg);
		else header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=$admin_groupmanagement_creatfailure");
	}
?>