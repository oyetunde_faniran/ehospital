<?php
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');

	// Redirect to the index page:
	$url = BASE_URL . 'index.php';

	// Pass along search terms?

} // End of defined() IF.

    $staffdept = isset($_GET["staffdept"]) ? (int)$_GET["staffdept"] : 0;
    $staffclinic = isset($_GET["staffclinic"]) ? (int)$_GET["staffclinic"] : 0;
    $custompage = isset($_GET["custompage"]) ? (int)$_GET["custompage"] : 0;


	if (isset($_GET["c"])){
		echo "<p class=\"comments\">{$_GET['c']}</p>";
    }
?>

<p>Please, select the required parameters below to continue.</p>

<form action="" method="get">
    <table cellspacing="3" cellpadding="3">
        <tr>
            <th scope="row" align="left"><label for="custompage">Page:</label></th>
            <td>
                <select name="custompage" id="custompage">
                    <option value="0">--Select Page--</option>
<?php
    if (!empty($custompage)){
		echo admin_Tools::getCustomFormPage4DropDown($custompage);
    } else {
        echo admin_Tools::getCustomFormPage4DropDown();
    }
?>
                </select>
            </td>
        </tr>
        <tr>
            <th scope="row" align="left"><label for="staffdept">Department:</label></th>
            <td>
                <select name="staffdept" id="staffdept" onchange="processClinics4Depts('staffdept', 'staffclinic');">
                    <option value="0">--Select Department--</option>
<?php
    if (!empty($staffdept)){
		echo admin_Tools::getStaffDepts4DropDown($curLangField, $staffdept);
    } else {
        echo admin_Tools::getStaffDepts4DropDown($curLangField);
    }
?>
                </select>
            </td>
        </tr>
        <tr>
            <th scope="row" align="left"><label for="staffclinic">Unit / Clinic:</label></th>
            <td>
                <select name="staffclinic" id="staffclinic">
                    <option value="0">--Select Clinic--</option>
<?php
    if (!empty($staffclinic) && !empty($staffdept)){
        $user = new admin_user();
        $result = $user->getClinics4DropDown($staffdept, $staffclinic);
        echo $result;
    }
?>
                </select>
                <span id="retplan_loader" style="display: none; padding-left: 10px; color: #999;"><?php echo $loading_label; ?></span>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input name="p" type="hidden" value="<?php echo $p; ?>" />
                <input name="m" type="hidden" value="<?php echo $m; ?>" />
                <input name="sButton1" class="btn" type="submit" value="Continue" />
            </td>
        </tr>
    </table>
</form>


<script type="text/javascript" language="javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/staffreg.js"></script>


<?php
    if (!empty($staffdept) && !empty($staffclinic) && !empty($custompage)){
        try {
            //Get and display available form elements that can be used in the FormBuilder
            $fb = new FormBuilder();
            $element_types = $fb->getFormElements();
            if (empty($element_types)){
                throw new Exception('Sorry! The form builder cannot be used now because it has not been fully configured.');
            }
            $element_select = '<div>';
            foreach ($element_types as $element){
                $element_select .= '<img style="display: inline; cursor: pointer;" alt="' . $element['custype_name'] . '" title="' . $element['custype_name'] . '" src="./images/form-builder/' . $element['custype_imagefile'] . '" ' . $element['custype_extra_attribute'] . '  />';
            }
            $element_select .= '</div>';
        } catch (Exception $e) {
            $element_select = $e->getMessage();
        }   //END try..catch


        //Get existing form elements that has been set up for this clinic before
        $existing_elems = $fb->getClinicFormElements($staffclinic, $custompage, false);
        if (!empty($existing_elems)){
            $existingelements_btn = ' <input type="button" value="Save New Arrangement" id="save-sorting-button" class="btn2" />'
                                    . ' <input type="button" value="Preview Form" id="preview-form-button" class="btn2" />';
        } else {
            $existingelements_btn = '';
        }
        $existing_elems_title = '<h2 style="margin-top: 10px;">'
                . 'EXISTING FORM ELEMENTS'
                . '</h2>' . $existingelements_btn;
        $existing_elems_list = '<ul id="sortable" style="margin-top: 10px;">';
        $existing_elems_display_inner = '';
        foreach ($existing_elems as $elem){
            if (!empty($elem['options'])){
                $tooltip_class = 'tooltip-options-class';
                $tooltip_text = ' title="OPTIONS: ' . htmlspecialchars($elem['options']) . '" ';
            } else {
                $tooltip_text = $tooltip_class = '';
            }
            $existing_elems_display_inner .= '<li id="' . $elem['customfield_id'] . '" ' . $tooltip_text . ' class="ui-state-default ' . $tooltip_class . '"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
                                    . $elem['customfield_name'] . ' <em style="font: 12px normal;">[' . $elem['custype_name'] . ']</em><em style="font: 12px normal; color: #F00; margin-left: 10px; cursor: pointer; text-decoration: underline;" onclick="doElementDelete(' . $elem['customfield_id'] . ');">Delete</em> '
//                                    . '<span style="margin: 10px;"><img src="./images/edit.png" /></span>'
//                                    . '<span style="margin: 10px;"><img src="./images/view.png" /></span>'
//                                    . '<span style="margin: 10px;"><img src="./images/delete.png" /></span>'
                                    . '</li>';
        }
        $existing_elems_list .= $existing_elems_display_inner . '</ul>';
        $existing_elems_display = $existing_elems_title . (!empty($existing_elems_display_inner) ? $existing_elems_list : 'No form element has been added yet for the selected page/department/clinic/unit.');

        echo '<hr /><p>Please, click a form element below to add it to the form to be built.</p>' . $element_select . '<hr />' . $existing_elems_display;
    }   //END if dept, clinic & page have been selected
?>

<div id="form-preview-dialog" title="Form Preview" style="display: none;">
    <p>
        <input type="button" value="Reload Form Preview" id="reload-form-preview-button" class="btn2" />
    </p>
    <div id="form-preview-dialog-contents">
        <img src="./images/_loader.gif" />
    </div>
</div>
<div id="form-builder-dialog" title="Form Builder" style="display: none;">
    <div>
        <form action="" method="post" id="formbuilder-form">
            <table cellpadding="3" cellspacing="3" border="0">
                <tr>
                    <td><strong>FIELD LABEL:</strong></td>
                    <td><input type="text" name="field_name" id="field_name" max_length="100" size="40" /></td>
                </tr>
                <tr id="options-builder" style="display: none;">
                    <td valign="top"><strong>OPTIONS:</strong></td>
                    <td>
                        <div>
                            <div style="margin-bottom: 10px;">
                                <input type="button" class="btn2" id="add-new-option-button" value="Add New Option" />
                            </div>
                            <div id="options-container">
                                <div><input type="text" size="40" name="options[]" id="options_1" /></div>
                                <div><input type="text" size="40" name="options[]" id="options_2" /></div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <input name="form_type" type="hidden" value="element-addition" id="form_type" />
            <input name="field_type" type="hidden" value="0" id="field_type" />
        </form>
    </div>
</div>

<link rel="stylesheet" href="./library/jquery-ui-1.10.3/themes/base/jquery.ui.all.css">
<script src="./library/jquery-ui-1.10.3/ui/jquery.ui.core.js"></script>
<script src="./library/jquery-ui-1.10.3/ui/jquery.ui.widget.js"></script>
<script src="./library/jquery-ui-1.10.3/ui/jquery.ui.mouse.js"></script>
<script src="./library/jquery-ui-1.10.3/ui/jquery.ui.sortable.js"></script>
<script src="./library/jquery-ui-1.10.3/ui/jquery.ui.draggable.js"></script>
<script src="./library/jquery-ui-1.10.3/ui/jquery.ui.position.js"></script>
<script src="./library/jquery-ui-1.10.3/ui/jquery.ui.resizable.js"></script>
<script src="./library/jquery-ui-1.10.3/ui/jquery.ui.button.js"></script>
<script src="./library/jquery-ui-1.10.3/ui/jquery.ui.dialog.js"></script>
<script src="./library/jquery-ui-1.10.3/ui/jquery-ui.js"></script>
<script type="text/javascript">
    nextDocument = 3;
    form_preview_contents = '';

    $(function() {
        $( "#form-builder-dialog" ).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 400,
            width: 600,
            modal: true,
            buttons: {
                'Add Form Element': function() {
                    $('#formbuilder-form').submit();
                },
                'Cancel': function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    });

    $(function() {
        $( "#form-preview-dialog" ).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 600,
            width: 700,
            modal: true,
            buttons: {
                Close: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    });

    $('#preview-form-button').click(function(){
        str = new String(form_preview_contents);
        if(str.length == 0){
            getFormPreview(<?php echo "$staffclinic, $custompage"; ?>);
        } else {
            $('#form-preview-dialog-contents').html(form_preview_contents);
        }
        $('#form-preview-dialog').dialog('open');
    });


    $('#reload-form-preview-button').click(function(){
        $('#form-preview-dialog-contents').html('<img src="./images/_loader.gif" />');
        getFormPreview(<?php echo "$staffclinic, $custompage"; ?>);
    });


    function getFormPreview(dis_clinic, dis_page){
        $.ajax({
            url: 'index.php?p=ajax&m=sysadmin&t=formbuilder-get-preview&clinic=' + dis_clinic + '&page=' + dis_page,
            type: 'GET',
            dataType: "html",
            success: function(data){
//                alert(data);
                form_preview_contents = data;
                $('#form-preview-dialog-contents').html(form_preview_contents);
            }
        });
    }


    function addFormElement(eType){
        $('#form-builder-dialog').dialog('open');
        $('#field_type').val(eType);
        //document.getElementById('field_type').value = eType;
        switch (eType){
            case 1: //Textbox
            case 2: //Textarea
            case 5: //Section title
            case 6: //Instruction (Ordinary Text)
                $('#options-builder').fadeOut();
                break;
            case 3: //Drop-Down
            case 4: //Multi-Select Drop-Down
                $('#options-builder').fadeIn();
                dis_default_options = '<div><input type="text" size="40" name="options[]" id="options_1" /></div>';
                dis_default_options += '<div><input type="text" size="40" name="options[]" id="options_2" /></div>';
                $('#options-container').html(dis_default_options);
                break;
        }   //END switch()
    }

	function addElement(){
		try {
			cont = document.getElementById("options-container");
			newDoc = "<input type=\"text\"  size=\"40\" name=\"options[]\" id=\"options" + nextDocument + "\" />";
			newDoc = newDoc + " <input type=\"button\" value=\"Remove\" onclick=\"removeElement(" + nextDocument + ");\" />";

			newDiv = document.createElement("div");
			newDiv.id = "newoption" + nextDocument;
			newDiv.innerHTML = newDoc;
			cont.appendChild(newDiv);

			nextDocument++;
		} catch (e) {
			alert ("An error occurred while trying to add a new custom field option.");
		}
	}

	function removeElement(whichDoc){
		c = "newoption" + whichDoc;
		try {
			c = document.getElementById(c);
			c.parentNode.removeChild(c);
		} catch (e) {
			alert ("An error occurred while trying to remove the custom field option.");
		}
	}

    $('#add-new-option-button').click(function(){
        addElement();
    });

    $('#save-sorting-button').click(function(){
//        sorted_list = $( "#sortable" ).sortable("serialize", { key: "sortedlist" });
        sorted_list = $( "#sortable" ).sortable("toArray");
//        alert(sorted_list);
        ajax_obj = $.ajax({
            url: 'index.php?p=ajax&m=sysadmin&t=formbuilder-sort',
//            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            type: 'GET',
            data: 'order=' + sorted_list,
            dataType: "html",
            success: function(data){
                if (data == 1){
                    alert('New arrangement saved successfully');
                } else {
                    alert('Sorry, new arrangement could not be saved. Please, try again.');
                }
            }
        });
    });


    $(function() {
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
    });


    $( ".tooltip-options-class" ).tooltip({
//        hide: {
//            effect: "explode",
//            delay: 250
//        },
        show: {
            effect: "slideDown",
            delay: 250
        },
        position: {
            my: "left top",
            at: "left bottom"
        }
    });


    function doElementDelete(elem_id){
        if (confirm('Are you sure you want to delete the selected custom field?')){
            window.location = 'index.php?' + <?php echo "'custompage=$custompage&staffdept=$staffdept&staffclinic=$staffclinic'"; ?> + '&p=formbuilder-elem-delete&m=sysadmin&id=' + elem_id;
        }
    }



</script>
<style>
    #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
    #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
    #sortable li span { position: absolute; margin-left: -1.3em; }
</style>