<?php
	session_start();
    
    //***CHAT STUFF:    Update this log-in session in the users_session table to show that the user is still active and not idle
    include_once ("./classes/DBConf.php");
    include_once ("./classes/admin_user.php");
    include_once ("./classes/admin_Tools.php");
    $user = new admin_user();
    $userID = isset($_SESSION[session_id() . "userID"]) ? $_SESSION[session_id() . "userID"] : 0;
    $user->loginSession_Delete($userID);
    
	session_destroy();
	header ("Location: index.php");
?>