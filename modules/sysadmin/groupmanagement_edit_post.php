<?php
	$id = isset($_GET["d"]) ? (int)$_GET["d"] : 0;
	$group = new admin_group($curLangField, $id);
	$error = $group->editGroup($_POST, $admin_groupmanagement_error);
	if (!$error)
		header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=$admin_editsuccess");
	else {
		if (!empty($group->errorMsg))
			header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=" . $group->errorMsg);
		else header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=$admin_editfailure");
	}
?>