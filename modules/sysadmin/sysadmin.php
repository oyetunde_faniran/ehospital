<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<!-- <h3><?php //echo $admin_header; ?></h3>
<div>&nbsp;</div>-->
<?php
	$tocArray = array (
					"menumanagement" 		=> $admin_menumanagement,
					"groupmanagement" 		=> $admin_groupmanagement,
					"rightmanagement"		=> $admin_rightmanagement,
					"usermanagement"		=> $admin_usermanagement
					//"audittrail"			=> $admin_audittrail,
				);
    $links = "<table cellpadding=\"5\" cellspacing=\"5\" border=\"0\">";
	foreach ($tocArray as $key=>$value){
		$links .= "<tr><td><a href=\"index.php?p=$key&m=sysadmin\">$value</a></td></tr>";
	}
	$links .= "</table>";
	echo $links;
?>