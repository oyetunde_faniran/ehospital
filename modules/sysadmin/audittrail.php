<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	//Pass along search terms?

	//Create an audit trail object
	$trailer = new admin_AuditTrail($_SESSION[session_id() . "langField"]);

	//A new audit trail object is about to be created, so, destroy any search criteria that may be existing in session before
	$_SESSION[session_id() . "auditTrailSCriteria"] = NULL;

} //End of defined() IF.

?>

<div>

<script type="text/javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/menumanagement.js"></script>

<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.sortable.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.tabs.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
<script type="text/javascript">
$(function() {
    $("#tabs").tabs().find(".ui-tabs-nav").sortable({axis:'x'});
});

	$(function() {
		$('#fromdate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});
	});

	$(function() {
		$('#todate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});
	});

	$(function() {
		$("#accordion").accordion({
									  collapsible: true,
									  active: false,
									  clearStyle: true
								  });
	});
</script>

<p><?php echo $audittrail_story; ?></p>
	<form method="post" action="reps.php?p=audittrail_view&m=sysadmin" name="audittrailform" autocomplete="off">
    	<input name="sButton" type="submit" class="btn" value="<?php echo $audittrail_showAudit; ?>" />
        <div>&nbsp;</div>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1"><?php echo $audittrail_usersearch; ?></a></li>
                <li><a href="#tabs-2"><?php echo $audittrail_datesearch; ?></a></li>
                <li><a href="#tabs-3"><?php echo $audittrail_pagesearch; ?></a></li>
            </ul>
            <div id="tabs-1">
            	<p><?php echo $audittrail_story_users; ?></p>
                <!--USERS-->


                    <div id="accordion">
<?php
	$textArray = array(
					   	"sno" 		=> $audittrail_sno,
						"staffno" 	=> $audittrail_staffno,
						"name" 		=> $audittrail_staffname,
						"nouser" 	=> $audittrail_nouser,
						"dept"		=> $audittrail_dept
					   );
	echo $trailer->getUsersInGroups($textArray);
?>
                    </div>    
            </div>

            <div id="tabs-2">
            	<p><?php echo $audittrail_story_date; ?></p>
                <!--DATE-->
<?php
	$toDate = date("Y-m-d");

	//Get the timestamp for three days ago from now (3 days in seconds = 3days * 24hours * 60mins * 60secs)
	$fromDate = time() - (3 * 24 * 60 * 60);
	$fromDate = date("Y-m-d", $fromDate);
?>
                    <table>
                      <tr>
                        <th><label for="fromdate"><?php echo $audittrail_from; ?></label></th>
                        <td><input name="fromdate" id="fromdate" type="text" size="10" value="<?php echo $fromDate; ?>" maxlength="10" /></td>
                        <th><label for="todate"><?php echo $audittrail_to; ?></label></th>
                        <td><input name="todate" id="todate" type="text" size="10" maxlength="10" value="<?php echo $toDate; ?>" /></td>
                      </tr>
    
                    </table>
            </div>

            <div id="tabs-3"><!--PAGE-->
            	<p><?php echo $audittrail_story_page; ?></p>
                <p><?php echo $audittrail_story_page2; ?></p>
<?php
	echo admin_menu::getMenu4AuditTrail($_SESSION[session_id() . "langField"], 1, $admin_menumanagement_nomenu);
?>
            </div>

        </div>
    </form>

</div>