<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>

<!-- <h3><?php //echo $admin_menumanagement_edit; ?></h3>
<div>&nbsp;</div>-->
<?php
	if (isset($_GET["c"]))
		echo "<p class=\"comments\">{$_GET['c']}</p>"
?>
<div>
    <table border="0" cellspacing="5" cellpadding="5">
        <tr>
            <td>
<?php
	$id = isset($_GET["d"]) ? (int)$_GET["d"] : 0;
	$menuProps = admin_menu::getMenuProps($curLangField, $id);
	if (is_array($menuProps)){
?>
<form action="" method="post" name="menueditform">
	<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th scope="row" align="left"><label for="menu"><?php echo $admin_menumanagement_menu; ?></label></th>
    <td>
    	<input name="menu" id="menuname" value="<?php echo $menuProps["$curLangField"]; ?>" type="text" maxlength="255" />
    </td>
  </tr>
  <tr>
    <th scope="row" align="left"><label for="url"><?php echo $admin_menumanagement_url; ?></label></th>
    <td>
    	<input name="url" id="url" type="text" value="<?php echo $menuProps["menu_url"]; ?>" size="40" maxlength="255" />
    </td>
  </tr>
  <tr>
    <th scope="row" align="left"><label for="order"><?php echo $admin_menumanagement_order; ?></label></th>
    <td>
    	<input name="order" id="order" type="text" value="<?php echo $menuProps["menu_order"]; ?>" size="5" maxlength="5" />
<?php
	echo "<em>$admin_menumanagement_orderdesc</em>";
?>
    </td>
  </tr>




  <tr>
    <th scope="row" align="left">
    	<label for="parent"><?php echo $admin_menumanagement_module; ?></label>
    </th>
    <td>
    	<select name="module" id="module">
        	<option value=""></option>
<?php
	echo admin_menu::getModules4DropDown($curLangField, $admin_menumanagement_all, $menuProps["module_id"]);
?>
        </select>
    </td>
  </tr>




  <tr>
    <th scope="row" align="left">
    	<label for="parent"><?php echo $admin_menumanagement_parent; ?></label>
    </th>
    <td>
    	<select name="parent" id="parent">
<?php
	echo admin_menu::getMenu4DropDown($curLangField, $menuProps["menu_parentid"]);
?>
        </select>
        <?php
			echo "<em>$admin_menumanagement_parentdesc</em>";
		?>
    </td>
  </tr>


  <tr>
    <td colspan="2">
    	<label for="publish"><input name="publish" id="publish" value="1" 
<?php
	if ($menuProps["menu_publish"] == 1)
		echo " checked=\"checked\"";
?>
         type="checkbox" />
        <?php echo $admin_menumanagement_publish; ?></label>
    </td>
  </tr>


  <tr>
    <td colspan="2">
    	<label for="dropdown"><input name="dropdown" id="dropdown" value="1" 
<?php
	if ($menuProps["menu_dropdown"] == 1)
		echo " checked=\"checked\"";
?>
         type="checkbox" />
        <?php echo $admin_menumanagement_dropdown; ?></label>
    </td>
  </tr>


  <tr>
    <th scope="row" align="left">&nbsp;</th>
    <td>
    	<input name="sButton" type="submit" value="<?php echo $admin_button_savechanges; ?>" />
        <input name="d" type="hidden" value="<?php echo $id; ?>" />
    </td>
  </tr>
</table>
</form>


<?php
		echo "<p><a href=\"index.php?p=menumanagement&m=sysadmin\">&laquo; $admin_menumanagement_back</a></p>";
	} else echo "<p class=\"comments\">$admin_menumanagement_invalidlink1<a href=\"index.php?p=menumanagement&m=sysadmin\">$admin_menumanagement_invalidlink2</a></p>";
?>


            </td>
        </tr>
    </table>
</div>