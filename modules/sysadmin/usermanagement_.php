<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<h3><?php echo $admin_usermanagement; ?></h3>
<div>&nbsp;</div>
<div>
<?php
	echo "$admin_usermanagement_story
			<ul class=\"story\">
				<li>$admin_usermanagement_story1</li>
				<li>$admin_usermanagement_story2</li>
				<li>$admin_usermanagement_story3</li>
			</ul>";
?>
</div>
<div>&nbsp;</div>
<div>
    <table border="0" cellspacing="5" cellpadding="5">
<!--    	<tr>
        	<td>
            	<div>&nbsp;</div>
                <div class="addMenu">
                	<img src="images/addgroup.png" /> 
                    <a href="index.php?p=groupmanagement_add&m=sysadmin"><?php //echo $admin_groupmanagement_addlink; ?></a>
                </div>
            </td>
        </tr>-->
        <tr>
            <td>
<?php
	$user = new admin_user(0, 0, $curLangField);
	$headerArray = array (
						"sno" => $admin_usermanagement_sno,
						"staffNo" => $admin_usermanagement_staffNo,
						"staffName" => $admin_usermanagement_staffName,
						"dept" => $admin_usermanagement_dept,
						"userConfig" => $admin_usermanagement_userConfig
					);
	//echo admin_group::getGroup($curLangField, $admin_groupmanagement_nogroups);
	echo $user->getStaffList($headerArray);
?>
            </td>
        </tr>
    </table>
</div>