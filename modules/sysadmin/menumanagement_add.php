<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<script type="text/javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/menumanagement.js"></script>
<script type="text/javascript" language="javascript" src="library/cod_formvalidator.js"></script>

<!-- <h3><?php //echo $admin_menumanagement_add; ?></h3>
<div>&nbsp;</div>-->
<?php
	$isIE = strstr(strtolower($_SERVER['HTTP_USER_AGENT']), "msie");
	if (isset($_GET["c"]))
		echo "<p class=\"comments\">{$_GET['c']}</p>"
?>
<div>
    <table border="0" cellspacing="5" cellpadding="5">
        <tr>
            <td>

<form action="" method="post" name="menuaddform">
	<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th scope="row" align="left"><label for="menu"><?php echo $admin_menumanagement_menu; ?></label></th>
    <td>
    	<input name="menu" id="menuname" value="
<?php
	if (isset($_POST["menu"]))
		echo $_POST["menu"];
?>" type="text" maxlength="255" />
    </td>
  </tr>
  <tr>
    <th scope="row" align="left"><label for="url"><?php echo $admin_menumanagement_url; ?></label></th>
    <td>
    	<input name="url" id="url" type="text" value="
<?php
	if (isset($_POST["url"]))
		echo $_POST["url"];
?>" size="40" maxlength="255" />
    </td>
  </tr>
  <tr>
    <th scope="row" align="left"><label for="order"><?php echo $admin_menumanagement_order; ?></label></th>
    <td>
    	<input name="order" id="order" type="text" value="
<?php
	if (isset($_POST["order"]))
		echo $_POST["order"];
?>" size="5" maxlength="5" />
<?php
	echo "<em>$admin_menumanagement_orderdesc</em>";
?>
    </td>
  </tr>


<?php
	if (!$isIE){
?>
  <tr>
    <th scope="row" align="left">
    	<label for="parent"><?php echo $admin_menumanagement_module; ?></label>
    </th>
    <td>
    	<select name="module" id="module">
        	<option value=""></option>
<?php
	if (isset($_POST["module"]))
		echo admin_menu::getModules4DropDown($curLangField, $admin_menumanagement_all, $_POST["module"]);
	else echo admin_menu::getModules4DropDown($curLangField, $admin_menumanagement_all, 0);
?>
        </select>
    </td>
  </tr>
<?php
	}	//END if isIE
?>


  <tr>
    <th scope="row" align="left">
    	<label for="parent"><?php echo $admin_menumanagement_parent; ?></label>
    </th>
    <td>
    	<div>
    		<select name="parentmenu" id="parentmenu">
            	<option value=""></option>
<?php
	/*if ($isIE)
		//if the user agent (browser) is IE, then show all the available menu at once, else use ajax to load per module
		echo admin_menu::getMenu4DropDown($curLangField, 0);
	else echo "<option value=\"1\">Main Menu</option>";*/
	//echo "<option value=\"1\">Main Menu</option>";
	echo admin_menu::getMenu4DropDown($curLangField, 1);
?>
	        </select>

<!--    	<span class=\"padLeft\">
        	<strong><?php //echo $admin_menumanagement_filter; ?></strong>
    	<select name="filter" id="filter" onchange="loadParents(this);">
<?php
	//echo admin_menu::getModules4DropDown($curLangField, $admin_menumanagement_all, -1);
?>
        </select>
        </span>-->
        </div>
        <?php
			echo "<em>$admin_menumanagement_parentdesc</em>";
		?>
    </td>
  </tr>

  <tr>
    <td colspan="2">
    	<label for="publish"><input name="publish" id="publish" value="1" 
<?php
	if ($_POST){
		if (isset($_POST["publish"]))
			echo " checked=\"checked\"";
	} else echo " checked=\"checked\"";
?>
         type="checkbox" />
        <?php echo $admin_menumanagement_publish; ?></label>
    </td>
  </tr>

  <tr>
    <td colspan="2">
    	<label for="dropdown"><input name="dropdown" id="dropdown" value="1" 
<?php
	if ($_POST){
		if (isset($_POST["dropdown"]))
			echo " checked=\"checked\"";
	} else echo " checked=\"checked\"";
?>
         type="checkbox" />
        <?php echo $admin_menumanagement_dropdown; ?></label>
    </td>
  </tr>

  <tr>
    <th scope="row" align="left">&nbsp;</th>
    <td>
    	<input name="sButton" type="submit" class="btn" value="<?php echo $admin_menumanagement_add; ?>" />
    </td>
  </tr>
</table>
</form>
<p>
<?php
	echo "<a href=\"index.php?p=menumanagement&m=sysadmin\">&laquo; $admin_menumanagement_back</a>";
?>
</p>

            </td>
        </tr>
    </table>
</div>