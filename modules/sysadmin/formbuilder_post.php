<?php
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');

	// Redirect to the index page:
	$url = BASE_URL . 'index.php';

} // End of defined() IF.
//die ('<pre>' . print_r(filter_input_array(INPUT_POST), true));
$post = filter_input_array(INPUT_POST);
if (!empty($post)){
    //Get a form builder object
    $fb = new FormBuilder();

    //Get all the needed variables
    $user_id = $_SESSION[session_id() . 'userID'];
    $field_name = filter_input(INPUT_POST, 'field_name', FILTER_SANITIZE_STRING);
    $field_type = filter_input(INPUT_POST, 'field_type', FILTER_SANITIZE_NUMBER_INT);
    $clinic_id = filter_input(INPUT_GET, 'staffclinic', FILTER_SANITIZE_NUMBER_INT);
    $page_id = filter_input(INPUT_GET, 'custompage', FILTER_SANITIZE_NUMBER_INT);
    $options_array = isset($post['options']) ? $post['options'] : array();

    //Save the new form element
    $success = $fb->saveFormElement($user_id, $field_name, $field_type, $clinic_id, $page_id, $options_array);
    $msg = $success ? 'Custom field addition was successful' : (!empty($fb->errorMsg) ? $fb->errorMsg : 'Custom field addition failed!');
    header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=$msg");
}   //END if $_POST is not empty