<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.
//echo $_POST["username"];
?>
<script type="text/javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/menumanagement.js"></script>
<script type="text/javascript" language="javascript" src="library/cod_formvalidator.js"></script>

<!-- <h3><?php //echo $admin_usermanagement_config; ?></h3>
<div>&nbsp;</div>-->

<div>
    <table border="0" cellspacing="5" cellpadding="5">
        <tr>
            <td>
<?php
	$id = isset($_GET["d"]) ? (int)$_GET["d"] : 0;
	$u = new admin_user($id, 0, $curLangField);
	$user = $u->getBankTellerDeets();
//	echo "<pre>" . print_r ($user, true) . "</pre>";
	if (is_array($user)){
		if (isset($_GET["c"]))
			echo "<p class=\"comments\">{$_GET['c']}</p>"
?>
<form action="" method="post" name="groupaddform">
	<table border="0" class="userTable">
  <tr>
    <th scope="row" align="left"><label for="group"><?php echo $admin_usermanagement_tellername; ?>:</label></th>
    <td>
<?php
	if (isset($user["staffname"]))
		echo $user["staffname"];
?>
    </td>
  </tr>

<tr>
    <th scope="row" align="left"><label for="group"><?php echo $admin_usermanagement_bank; ?>:</label></th>
    <td>
<?php
	if (isset($user["bank"]))
		echo $user["bank"];
?>
    </td>
  </tr>


<?php
//The administrator is not allowed to modify the username & password of users. He can only
//change the group of a user or disable / enable a user
if (!isset($user["user"], $user["status"])){
?>
  <tr>
    <th scope="row" align="left"><label for="username"><?php echo $admin_usermanagement_username; ?>:</label></th>
    <td>
    	<input name="username" id="username" value="
<?php
	if (isset($_POST["username"]))
		echo $_POST["username"];
?>" type="text" maxlength="30" size="20" />
    </td>
  </tr>
  <tr>
    <th scope="row" align="left"><label for="password"><?php echo $admin_usermanagement_password; ?>:</label></th>
    <td>
    	<input name="password" id="password" type="password" value="" maxlength="30" size="20" />
    </td>
  </tr>
  <tr>
    <th scope="row" align="left"><label for="cpassword"><?php echo $admin_usermanagement_cpassword; ?>:</label></th>
    <td>
    	<input name="cpassword" id="cpassword" type="password" value="" maxlength="30" size="20" />
    </td>
  </tr>

<?php
}
?>




  <tr>
    <th scope="row" align="left"><label for="group"><?php echo $admin_groupmanagement_group; ?>:</label></th>
    <td>
    	<select name="group" id="group">
        	<option value=""></option>
<?php
	if (!empty($user["groupID"]))
		echo admin_group::getGroups4DropDown ($curLangField, $user["groupID"]);
	else echo admin_group::getGroups4DropDown ($curLangField);
?>
        </select>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    	<label for="enabled">
    	<input name="enabled" id="enabled" type="checkbox" 
<?php
	if (isset($user["status"])){
		if ($user["status"])
			echo "checked=\"checked\"";
	} else echo "checked=\"checked\"";
?> />
	<strong><?php echo $admin_usermanagement_enableuser; ?></strong>
		</label>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    	<input name="sButton" class="btn" type="submit" value="<?php echo $admin_button_savechanges; ?>" />
        <input type="hidden" name="u" value="<?php if (isset($user["userID"])) echo $user["userID"]; else echo 0; ?>" />
    </td>
  </tr>
</table>
</form>

<?php
		echo "<p><a href=\"index.php?p=usermanagement&m=sysadmin\">&laquo; $admin_usermanagement_back</a></p>";
	} else echo "<p>$admin_menumanagement_invalidid1 <a href=\"index.php?p=groupmanagement&m=sysadmin\">$admin_menumanagement_invalidid2</a></p>";
?>

            </td>
        </tr>
    </table>

</div>