<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<script type="text/javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/menumanagement.js"></script>
<script type="text/javascript" language="javascript" src="library/cod_formvalidator.js"></script>

<div>
    <table border="0" cellspacing="5" cellpadding="5">
        <tr>
            <td>
<?php
	if (isset($_GET["c"]))
		echo "<p class=\"comments\">{$_GET['c']}</p>"
?>
<form action="" method="post" name="changepassform" autocomplete="off">
	<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th scope="row" align="left"><label for="currentuser"><?php echo $admin_changepass_curuser; ?>:</label></th>
    <td>
    	<input name="currentuser" id="currentuser" value="" type="text" maxlength="30" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="currentpass"><?php echo $admin_changepass_curpass; ?>:</label></th>
    <td>
    	<input name="currentpass" id="currentpass" value="" type="password" maxlength="30" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="newuser"><?php echo $admin_changepass_newuser; ?>:</label></th>
    <td>
    	<input name="newuser" id="newuser" value="" type="text" maxlength="30" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="newpass"><?php echo $admin_changepass_newpass; ?>:</label></th>
    <td>
    	<input name="newpass" id="newpass" value="" type="password" maxlength="30" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="newcpass"><?php echo $admin_changepass_newcpass; ?>:</label></th>
    <td>
    	<input name="newcpass" id="newcpass" value="" type="password" maxlength="30" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left">&nbsp;</th>
    <td>
    	<input name="sButton" type="submit" class="btn" value="<?php echo $admin_button_savechanges; ?>" />
    </td>
  </tr>
</table>
</form>

            </td>
        </tr>
    </table>

</div>