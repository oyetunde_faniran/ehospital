<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	//Pass along search terms?

	//Create an audit trail object
	$trailer = new admin_AuditTrail($_SESSION[session_id() . "langField"]);

} //End of defined() IF.

?>

<div class="auditTrail">
<?php
	if (isset($_GET["d"])){
		$auditID = (int)$_GET["d"];
		$trailer = new admin_AuditTrail($_SESSION[session_id() . "langField"]);
		$text = array (
						   "story"		=> $admin_audittrail_postdata_story,
						   "fieldName"	=> $admin_audittrail_postdata_fieldname,
						   "fieldValue"	=> $admin_audittrail_postdata_fieldvalue,
						   "noPost"		=> $admin_audittrail_postdata_nopost
					   );
		echo $trailer->getPostedData($auditID, $text);
	} else echo "$audittrail_searchCriteria <a href=\"index.php?p=audittrail&m=sysadmin\">$audittrail_searchCriteria1</a>.";
?>
</div>


