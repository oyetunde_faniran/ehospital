<?php
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');

	// Redirect to the index page:
	$url = BASE_URL . 'index.php';

} // End of defined() IF.

//Get all the required GET variables
$customfield_id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$custompage = filter_input(INPUT_GET, 'custompage', FILTER_SANITIZE_NUMBER_INT);
$staffdept = filter_input(INPUT_GET, 'staffdept', FILTER_SANITIZE_NUMBER_INT);
$staffclinic = filter_input(INPUT_GET, 'staffclinic', FILTER_SANITIZE_NUMBER_INT);

//Delete the form field
$fb = new FormBuilder();
$success = $fb->deleteFormElement($customfield_id);

//Redirect to the custom field management page
$msg = 'Custom field deleted successfully.';
header ("Location: index.php?custompage=$custompage&staffdept=$staffdept&staffclinic=$staffclinic&p=formbuilder&m=sysadmin&c=$msg");