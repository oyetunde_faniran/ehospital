<?php
	$id = isset($_GET["d"]) ? (int)$_GET["d"] : 0;
	$user = new admin_user(0, $_POST, $curLangField);
	$errorMsgs = array (
						"admin_usermanagement_error_user" 		=> $admin_usermanagement_error_user,
						"admin_usermanagement_error_userpass"	=> $admin_usermanagement_error_userpass,
						"admin_usermanagement_error_pass1"		=> $admin_usermanagement_error_pass1,
						"admin_usermanagement_error_cpass"		=> $admin_usermanagement_error_cpass,
						"admin_usermanagement_error_passcpass"	=> $admin_usermanagement_error_passcpass,
						"admin_usermanagement_error_group"		=> $admin_usermanagement_error_group
					);
	$error = $user->updateUser($id, $errorMsgs);
	if (!$error)
		header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=$admin_editsuccess");
	else {
		if (!empty($user->errorMsg))
			$_GET["c"] = $user->errorMsg;
		else $_GET["c"] = $admin_editfailure;
	}
?>