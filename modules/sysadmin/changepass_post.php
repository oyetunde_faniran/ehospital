<?php
	$id = isset($_GET["d"]) ? (int)$_GET["d"] : 0;
	$user = new admin_user(0, $_POST, $curLangField);
	$error = $user->changePass();
	if (!$error)
		header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=$admin_editsuccess");
	else {
		if (!empty($user->errorMsg))
			header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=" . $user->errorMsg);
		else header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=$admin_editfailure");
	}
?>