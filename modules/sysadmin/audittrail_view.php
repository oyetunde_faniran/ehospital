<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	//Pass along search terms?

	//Create an audit trail object
	$trailer = new admin_AuditTrail($_SESSION[session_id() . "langField"]);

} //End of defined() IF.

?>

<div class="auditTrail">
<?php
	//Get a new search criteria from post
	if ($_POST){
		$_SESSION[session_id() . "auditTrailSCriteria"] = $_POST;
		//die ("session variable set");
	}

	if (isset($_SESSION[session_id() . "auditTrailSCriteria"])){
		echo "<h3><a href=\"index.php?p=audittrail&m=sysadmin\">&laquo; $audittrail_newsearch</a></h3>";
		$trailer = new admin_AuditTrail($_SESSION[session_id() . "langField"]);
		echo $trailer->getAuditTrail($_SESSION[session_id() . "auditTrailSCriteria"], $audittrail_noResult);
		echo "<h3><a href=\"index.php?p=audittrail&m=sysadmin\">&laquo; $audittrail_newsearch</a></h3>";
	} else echo "$audittrail_searchCriteria <a href=\"index.php?p=audittrail&m=sysadmin\">$audittrail_searchCriteria1</a>.";
?>
</div>


