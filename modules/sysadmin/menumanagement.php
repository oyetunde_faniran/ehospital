<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<script type="text/javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/menumanagement.js"></script>

<!-- <h3><?php //echo $admin_menumanagement; ?></h3>
<div>&nbsp;</div>-->
<div><?php echo $admin_menumanagement_story; ?></div>
<div>
    <table border="0" cellspacing="5" cellpadding="5">
    	<tr>
        	<td>
            	<div>&nbsp;</div>
                <div class="addMenu">
                	<img src="images/addmenu.png" /> 
                    <a href="index.php?p=menumanagement_add&m=sysadmin"><?php echo $admin_menumanagement_addlink; ?></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>
<?php
	echo admin_menu::getMenu($_SESSION[session_id() . "langField"], 1, $admin_menumanagement_nomenu);
?>
            </td>
        </tr>
    </table>
</div>