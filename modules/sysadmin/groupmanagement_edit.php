<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<script type="text/javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/menumanagement.js"></script>
<script type="text/javascript" language="javascript" src="library/cod_formvalidator.js"></script>

<!-- <h3><?php //echo $admin_groupmanagement_edittitle; ?></h3>
<div>&nbsp;</div>-->

<div>
    <table border="0" cellspacing="5" cellpadding="5">
        <tr>
            <td>
<?php
	$id = isset($_GET["d"]) ? (int)$_GET["d"] : 0;
	$g = new admin_group($curLangField, $id);
	$group = $g->getGroupDeets();
	if (is_array($group)){
		if (isset($_GET["c"]))
			echo "<p class=\"comments\">{$_GET['c']}</p>"
?>
<form action="" method="post" name="groupaddform">
	<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th scope="row" align="left"><label for="group"><?php echo $admin_groupmanagement_group; ?>:</label></th>
    <td>
    	<input name="group" id="group" value="
<?php
	echo $group["groupname"];
?>" type="text" maxlength="50" />
    </td>
  </tr>
  <tr>
    <th scope="row" align="left"><label for="groupdesc"><?php echo $admin_groupmanagement_desc; ?>:</label></th>
    <td>
    	<input name="groupdesc" id="groupdesc" type="text" value="
<?php
	echo $group["group_desc"];
?>" size="40" maxlength="255" />
    </td>
  </tr>
  <tr>
    <th scope="row" align="left">&nbsp;</th>
    <td>
    	<input name="sButton" type="submit" value="<?php echo $admin_button_savechanges; ?>" />
    </td>
  </tr>
</table>
</form>

<?php
		echo "<p><a href=\"index.php?p=groupmanagement&m=sysadmin\">&laquo; $admin_groupmanagement_back</a></p>";
	} else echo "<p>$admin_menumanagement_invalidid1 <a href=\"index.php?p=groupmanagement&m=sysadmin\">$admin_menumanagement_invalidid2</a></p>";
?>

            </td>
        </tr>
    </table>

</div>