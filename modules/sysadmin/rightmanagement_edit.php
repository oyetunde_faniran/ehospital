<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

	$id = isset($_GET["d"]) ? (int)$_GET["d"] : 0;
	$g = new admin_group($curLangField, $id);
	$group = $g->getGroupDeets();
	if (is_array($group))
		$title = $admin_rightmanagement_editing . "<strong>'" . $group["groupname"] . "'</strong>";
	else $title = $admin_rightmanagement_unknowngroup;
	echo "<script type=\"text/javascript\" language=\"javascript\">
				g = \"{$group['groupname']}\";
			</script>";
?>
<script type="text/javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/menumanagement.js"></script>

<!-- <h3><?php //echo $admin_rightmanagement_edit; ?></h3>
<div>&nbsp;</div>-->
<div><?php echo $admin_rightmanagement_edit_story; ?></div>
<div>&nbsp;</div>
<div><?php echo $admin_rightmanagement_story2; ?></div>
<div>
    <table border="0" cellspacing="5" cellpadding="5">
    	<tr>
        	<td>
            	<div>&nbsp;</div>
                <div class="addMenu"> <?php echo $title; ?> </div>
            </td>
        </tr>
        <tr>
            <td>
<?php
	$text = array (
					"add"		=> $admin_rightmanagement_add,
					"addc"		=> $admin_rightmanagement_addc,
					"remove"	=> $admin_rightmanagement_remove,
					"added"		=> $admin_rightmanagement_added,
					"nomenu"	=> $admin_menumanagement_nomenu,
					"adding"	=> $admin_rightmanagement_adding,
					"removing"	=> $admin_rightmanagement_removing
				);
	echo admin_menu::getMenu4RightsEdit($curLangField, 1, $id, $text);
?>
            </td>
        </tr>
    </table>
<?php
	echo "<p><a href=\"index.php?p=rightmanagement&m=sysadmin\">&laquo; $admin_rightmanagement_back</a></p>";
?>
</div>