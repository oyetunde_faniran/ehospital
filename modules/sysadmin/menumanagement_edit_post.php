<?php
	$menu = new admin_menu ($curLangField, (int)$_POST["d"]);
	$error = $menu->editMenuItem($_POST, $admin_menumanagement_error_allfields);
	if (!$error)
		header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=$admin_editsuccess");
	else {
		if (!empty($menu->errorMsg))
			header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=" . $menu->errorMsg);
		else header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=$admin_editfailure");
	}
?>