<?php
	$user = new admin_user(0, $_POST, $curLangField);
	$errMsgs = array (
					  "uniqueID" => $staffreg_error_uniqueID,
					  "emptyUP" => $staffreg_error_emptyUP,
					  "2shortUP" => $staffreg_error_2shortUP,
					  "uniqueuser" => $staffreg_error_uniqueuser,
					  "cpass" => $staffreg_error_cpass,
					  "allfields" => $staffreg_error_allfields
				);

	if (isset($_POST["t"]))	//Hospital Staff
		$error = $user->registerStaff($errMsgs);
	elseif (isset($_POST["b1"]))	//Bank Teller
			$error = $user->registerBankTeller($errMsgs);

	if (!$error)
		header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=" . urlencode($staffreg_success));
	else {
		if (!empty($user->errorMsg))
			$_GET["c"] = $user->errorMsg;
		else $_GET["c"] = $staffreg_failure;
	}
?>