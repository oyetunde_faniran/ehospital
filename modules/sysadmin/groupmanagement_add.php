<?php # Script 2.6 - search.inc.php

/* 
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	
	// Pass along search terms?
	
} // End of defined() IF.

?>
<script type="text/javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/menumanagement.js"></script>
<script type="text/javascript" language="javascript" src="library/cod_formvalidator.js"></script>

<!--<h3><?php //echo $admin_groupmanagement_addtitle; ?></h3>
<div>&nbsp;</div>-->
<?php
	//$isIE = strstr(strtolower($_SERVER['HTTP_USER_AGENT']), "msie");
	if (isset($_GET["c"]))
		echo "<p class=\"comments\">{$_GET['c']}</p>"
?>
<div>
    <table border="0" cellspacing="5" cellpadding="5">
        <tr>
            <td>

<form action="" method="post" name="groupaddform">
	<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th scope="row" align="left"><label for="group"><?php echo $admin_groupmanagement_group; ?>:</label></th>
    <td>
    	<input name="group" id="group" value="
<?php
	if (isset($_POST["group"]))
		echo $_POST["group"];
?>" type="text" maxlength="50" />
    </td>
  </tr>
  <tr>
    <th scope="row" align="left"><label for="groupdesc"><?php echo $admin_groupmanagement_desc; ?>:</label></th>
    <td>
    	<input name="groupdesc" id="groupdesc" type="text" value="
<?php
	if (isset($_POST["groupdesc"]))
		echo $_POST["groupdesc"];
?>" size="40" maxlength="255" />
    </td>
  </tr>
  <tr>
    <th scope="row" align="left">&nbsp;</th>
    <td>
    	<input name="sButton" class="btn" type="submit" value="<?php echo $admin_groupmanagement_addtitle; ?>" />
    </td>
  </tr>
</table>
</form>
<p>
<?php
	echo "<a href=\"index.php?p=groupmanagement&m=sysadmin\">&laquo; $admin_groupmanagement_back</a>";
?>
</p>

            </td>
        </tr>
    </table>
</div>