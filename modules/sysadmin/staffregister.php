<?php # Script 2.6 - search.inc.php

/*
 *	This is the search content module.
 *	This page is included by index.php.
 *	This page expects to receive $_GET['terms'].
 */
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');

	// Redirect to the index page:
	$url = BASE_URL . 'index.php';

	// Pass along search terms?

} // End of defined() IF.

?>
<script type="text/javascript" language="javascript" src="library/admin_ajax/ajaxmachine.js"></script>
<script type="text/javascript" language="javascript" src="library/admin_ajax/staffreg.js"></script>
<script type="text/javascript" language="javascript" src="library/cod_formvalidator.js"></script>


<link type="text/css" href="library/admin_jquery/themes/base/ui.accordion.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/base/ui.base.css" rel="stylesheet" />
<link type="text/css" href="library/admin_jquery/themes/base/ui.theme.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
<script type="text/javascript">
$(function() {
    $("#accordion").accordion({
								  collapsible: true,
								  active: false,
								  clearStyle: true
							  });
});
</script>



<?php
	if (isset($_GET["c"]))
		echo "<p class=\"comments\">{$_GET['c']}</p>";
	if (!(isset($_GET["c"]) && stristr($_GET["c"], "Your registration was successful"))){
?>
<div>

<div id="accordion">
	<h3><a href="#"><?php echo $staffreg_staff; ?></a></h3>
	<div>
<div><?php echo $staffreg_staffstory; ?></div>
<table border="0" cellspacing="5" cellpadding="5">
        <tr>
            <td>

<form action="" method="post" name="staffregform" autocomplete="off">
	<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th scope="row" align="left"><label for="staffempid"><?php echo $staffreg_no; ?>:</label></th>
    <td>
    	<input name="staffempid" id="staffempid" value="
<?php
	if (isset($_POST["staffempid"]))
		echo $_POST["staffempid"];
?>" type="text" maxlength="50" />
    </td>
  </tr>
  <tr>
    <th scope="row" align="left"><label for="title"><?php echo $staffreg_stafftitle; ?>:</label></th>
    <td>
    	<input name="title" id="title" type="text" value="
<?php
	if (isset($_POST["title"]))
		echo $_POST["title"];
?>" size="5" maxlength="10" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="sname"><?php echo $staffreg_sname; ?>:</label></th>
    <td>
    	<input name="sname" id="sname" value="
<?php
	if (isset($_POST["sname"]))
		echo $_POST["sname"];
?>" type="text" maxlength="50" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="onames"><?php echo $staffreg_onames; ?>:</label></th>
    <td>
    	<input name="onames" id="onames" value="
<?php
	if (isset($_POST["onames"]))
		echo $_POST["onames"];
?>" type="text" maxlength="50" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="phone"><?php echo $staffreg_phone; ?>:</label></th>
    <td>
    	<input name="phone" id="phone" value="
<?php
	if (isset($_POST["phone"]))
		echo $_POST["phone"];
?>" type="text" maxlength="11" size="12" /> <em>e.g. 08012345678 [Required for notification]</em>
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="email"><?php echo $staffreg_email; ?>:</label></th>
    <td>
    	<input name="email" id="email" value="
<?php
	if (isset($_POST["email"]))
		echo $_POST["email"];
?>" type="text" maxlength="100" /><em> [Required for notification]</em>
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="staffdept"><?php echo $staffreg_dept; ?>:</label></th>
    <td>
    	<select name="staffdept" id="staffdept" onchange="processClinics4Depts('staffdept', 'staffclinic');">
        	<option value="0">--Select Department--</option>
<?php
	if (isset($_POST["staffdept"]))
		echo admin_Tools::getStaffDepts4DropDown($curLangField, $_POST["staffdept"]);
	else echo admin_Tools::getStaffDepts4DropDown($curLangField);
?>
		</select>
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="staffclinic">Unit / Clinic:</label></th>
    <td>
    	<select name="staffclinic" id="staffclinic">
        	<option value="0">--Select Clinic--</option>
		</select>
        <span id="retplan_loader" style="display: none; padding-left: 10px; color: #999;"><?php echo $loading_label; ?></span>
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="username"><?php echo $staffreg_username; ?>:</label></th>
    <td>
    	<input name="username" id="username" value="
<?php
	if (isset($_POST["username"]))
		echo $_POST["username"];
?>" type="text" maxlength="30" /> (8 - 30 Characters)
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="password"><?php echo $staffreg_password; ?>:</label></th>
    <td>
    	<input name="password" id="password" type="password" maxlength="30" /> (8 - 30 Characters)
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="cpassword"><?php echo $staffreg_cpassword; ?>:</label></th>
    <td>
    	<input name="cpassword" id="cpassword" type="password" maxlength="30" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left">&nbsp;</th>
    <td>
    	<input name="sButton" class="btn" type="submit" value="<?php echo $staffreg_register; ?>" />
        <input name="t" type="hidden" value="<?php echo md5(time()); ?>" />
    </td>
  </tr>
</table>
</form>
            </td>
        </tr>
    </table>
	</div>




	<h3><a href="#"><?php echo $staffreg_bankstaff; ?></a></h3>
	<div>
<div><?php echo $staffreg_bankstory; ?></div>
<table border="0" cellspacing="5" cellpadding="5">
        <tr>
            <td>

<form action="" method="post" name="tellerregform" autocomplete="off">
	<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th scope="row" align="left"><label for="title1"><?php echo $staffreg_stafftitle; ?>:</label></th>
    <td>
    	<input name="title" id="title1" type="text" value="
<?php
	if (isset($_POST["title"]))
		echo $_POST["title"];
?>" size="5" maxlength="10" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="sname1"><?php echo $staffreg_sname; ?>:</label></th>
    <td>
    	<input name="sname" id="sname1" value="
<?php
	if (isset($_POST["sname"]))
		echo $_POST["sname"];
?>" type="text" maxlength="50" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="onames1"><?php echo $staffreg_onames; ?>:</label></th>
    <td>
    	<input name="onames" id="onames1" value="
<?php
	if (isset($_POST["onames"]))
		echo $_POST["onames"];
?>" type="text" maxlength="50" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="phone1"><?php echo $staffreg_phone; ?>:</label></th>
    <td><input name="phone" id="phone1" value="
<?php
	if (isset($_POST["phone"]))
		echo $_POST["phone"];
?>" type="text" maxlength="11" size="12" /></td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="email1"><?php echo $staffreg_email; ?>:</label></th>
    <td>
    	<input name="email" id="email1" value="
<?php
	if (isset($_POST["email"]))
		echo $_POST["email"];
?>" type="text" maxlength="20" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="bank"><?php echo $staffreg_bank; ?>:</label></th>
    <td>
    	<select name="bank" id="bank">
        	<option value="Skye Bank">Skye Bank</option>
            <!--<option value="First Bank">First Bank</option>-->
        </select>
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="username1"><?php echo $staffreg_username; ?>:</label></th>
    <td>
    	<input name="username" id="username1" value="
<?php
	if (isset($_POST["username"]))
		echo $_POST["username"];
?>" type="text" maxlength="30" /> (8 - 30 Characters)
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="password1"><?php echo $staffreg_password; ?>:</label></th>
    <td>
    	<input name="password" id="password1" type="password" maxlength="30" /> (8 - 30 Characters)
    </td>
  </tr>

  <tr>
    <th scope="row" align="left"><label for="cpassword1"><?php echo $staffreg_cpassword; ?>:</label></th>
    <td>
    	<input name="cpassword" id="cpassword1" type="password" maxlength="30" />
    </td>
  </tr>

  <tr>
    <th scope="row" align="left">&nbsp;</th>
    <td>
    	<input name="sButton1" class="btn" type="submit" value="<?php echo $staffreg_register; ?>" />
        <input name="b1" type="hidden" value="<?php echo md5(time() + 1); ?>" />
    </td>
  </tr>
</table>
</form>
            </td>
        </tr>
    </table>
	</div>



</div>


<?php
	}
?>
<p>
<?php
	echo "<a href=\"index.php\">&laquo; $staffreg_back</a>";
?>
</p>

</div>