<?php
function getChildren($parentID, $prefix, $sNoPrefix, $startRowStyle){
        global $invClass;
        global $catArray;
        $catArray=(!empty($catArray)&&(is_array($catArray)))?$catArray:array();
        $retVal = "";
        $disChildren = $invClass->getChildCategories($parentID);
        if ($startRowStyle == " class=\"tr-row\" "){
            $rowStyle1 = " class=\"tr-row2\" ";
            $rowStyle2 = " class=\"tr-row\" ";
        } else {
            $rowStyle1 = " class=\"tr-row\" ";
            $rowStyle2 = " class=\"tr-row2\" ";
        }
		$juggleRows = true;
        $sNo = 0;
        foreach ($disChildren as $cat){
			$sNo++;
			$rowStyle = $juggleRows ? $rowStyle1 : $rowStyle2;
			$juggleRows = !$juggleRows;
                        $checked = in_array($cat["inventory_category_id"], $catArray) ? " checked=\"checked\" " : "";
			$retVal .= "<tr $rowStyle>
							<td>" . $sNoPrefix . $sNo . "</td>
							<td id=\"payment_item_" . $cat["inventory_category_id"] . "\"><label for=\"category_" . $cat["inventory_category_id"] . "\">$prefix" . stripslashes($cat["inventory_category_name"]) . "</label></td>
							<td align=\"center\"><input type=\"checkbox\" name=\"cat[]\" id=\"category_" . $cat["inventory_category_id"] . "\" value=\"" . $cat["inventory_category_id"]."\" $checked /></td></tr>";
            $sNoPrefixNew = $sNoPrefix . $sNo . ".";
            $retVal .= getChildren($cat["inventory_category_id"], $prefix . "<span style=\"color: #AAA;\">------&raquo;</span> ", $sNoPrefixNew, $rowStyle);
		}   //END foreach
        
        return $retVal;
        
    }   //END getChildren()





//        $invClass = new inventory_category_class();
//	$display = "		<table cellpadding=\"3\" cellspacing=\"3\" style=\"border: 5px solid #CCC\">
//                                        <tr>
//                                            <td><strong>S/NO</strong></td>
//                                            <td><strong>PRODUCT CATEGORY</strong></td>
//                                            <td>&nbsp;</td>
//                                        </tr>";
//        $display .= getChildren(0, "", "", " class=\"tr-row\" ");
//        $display .= "               </table>";
//	echo $display;
?>