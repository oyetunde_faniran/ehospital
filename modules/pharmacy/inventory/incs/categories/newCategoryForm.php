<?php
$createCategory = new inventory_category_class();
if (isset($_POST['setup'])) {
    $name = trim($_POST['categoryName']);
    $desc = trim($_POST['categoryDescription']);
    $parent = $_POST['parentCategory'];

    $report = $createCategory->addNewCategory($name, $desc, $parent);
    echo $report;
    if ($report == $inventoryCategoryCreationSuccessNews) {
        unset($_POST);
    }
}
?>
<!--<h2><?php //echo $inv_categoryForm_createLabel_prompt; ?></h2>-->

<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST" enctype="multipart/form-data">

    <table border="0">

        <tr><td><?php echo $inv_categoryForm_name_label; ?></td><td><input type="text" name="categoryName" <?php if (isset($_POST['categoryName'])) {
    echo "value=\"" . $_POST['categoryName'] . "\"";
} ?> /></td></tr>

        <tr><td valign="top"><?php echo $inv_categoryForm_description_label; ?></td><td><textarea rows="4" wrap="soft" title="Describe the category" name="categoryDescription"><?php if (isset($_POST['categoryDescription'])) {
    echo $_POST['categoryDescription'];
} ?></textarea></td></tr>

        <tr><td><?php echo $inv_categoryForm_parent_label; ?></td>
            <td>
                <select name="parentCategory">
                    <?php if(!isset($_GET['add'])){?>
                    <option selected value="0">No Parent</option><?php 
                    } else {
                        $catname=$createCategory->getCategoryName($_GET['add']);?>
                       <option selected value="<?php echo $_GET['add'];?>"><?php echo $catname;?></option>     
                         <?php 
                    }
                    $cats=$createCategory->get_existing_items('inventory_categories','inventory_category_name','asc');
                    if(is_string($cats)){echo $cats;}else{
                        while($cat=$cats->fetch_assoc()){
                            echo "<option value=\"".$cat['inventory_category_id']."\">".$cat['inventory_category_name']."</option>";
                        }
                    }
                    ?>
                </select></td>
        </tr>

        <tr><td>&nbsp;</td><td><input type="submit" name="setup" value="<?php echo $inv_SubmitBtnValue; ?>" class="button" /></td></tr>
    </table>
</form>