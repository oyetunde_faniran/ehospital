<?php 
$pageClass=new inventory_presentation_class();

if(isset($_POST['update'])){
    $id=$_POST['presentation_id'];
    $name=trim($_POST['presentationName']);
    $desc=trim($_POST['presentationDescription']);
    $author=$_SESSION[session_id()."userID"];
    
    if($pageClass->updatepresentation($id,$name,$desc)){
        echo $inventory_presentation_successfully_updated;
    }else{
        echo $inventory_presentation_update_failure_message;
    }
}


$presentationProperties=$pageClass->getpresentationProperties($_GET['edit']);
if(is_string($presentationProperties)){
    echo $presentationProperties;
        exit;
        }
echo "<h2>".$inventoryEditingpresentation ."
    &nbsp'".$presentationProperties['inventory_presentation_name']. "'</h2>"; ?>
<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="POST" enctype="multipart/form-data">
    <table border="0">

        <tr><td><?php echo $inv_presentationForm_name_label; ?></td><td><input type="text" name="presentationName" value="<?php echo $presentationProperties['inventory_presentation_name']; ?>" /></td></tr>

        <tr><td valign="top"><?php echo $inv_presentation_description_label; ?></td><td><textarea rows="4" wrap="soft" title="<?php echo $inventory_presentation_describe_the_presentation;?> " name="presentationDescription"><?php echo $presentationProperties['inventory_presentation_description']; ?></textarea></td></tr>

        <tr><td>&nbsp;</td><td><input type="hidden" value="<?php echo $presentationProperties['inventory_presentation_id']; ?>" name="presentation_id" /><input type="submit" name="update" value="<?php echo $inventory_update; ?>" class="button" /></td></tr>
    </table>
</form>