<?php
    //die ("<pre>" . print_r ($_POST, true) . "</pre>");

    $manName = isset($_POST["manufacturer_name"]) ? $_POST["manufacturer_name"] : "";
    $manAddress = isset($_POST["manufacturer_address"]) ? $_POST["manufacturer_address"] : "";
    $manRCNo = isset($_POST["manufacturer_rcno"]) ? $_POST["manufacturer_rcno"] : "";

    $manObj = new inventory_manufacturer();
    $saved = $manObj->addManufacturer($manName, $manAddress, $manRCNo, $_SESSION[session_id() . "userID"]);

    if ($saved){	//Operation successful
        $c = "The manufacturer was added successfully.";
        $c = urlencode($c);
        $page = $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING'] . "&c=" . $c;
        header ("Location: $page");
    } else {
            $_GET["c"] = !empty($manObj->errorMsg) ? $manObj->errorMsg : "Sorry! Operation failed. Please, try again.";
    }
?>