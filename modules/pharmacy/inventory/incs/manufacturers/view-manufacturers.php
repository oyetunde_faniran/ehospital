<?php
    $manObj = new inventory_manufacturer();
    $allMan = $manObj->getAllManufacturers();
    if (count($allMan) > 0){
        $display = "<table cellpadding=\"3\" cellspacing=\"3\">
                        <tr class=\"title-row\">
                            <td>" . $report_admissionbook_sno . "</td>
                            <td>" . $inv_manufacturerForm_name_label . "</td>
                            <td>" . $inv_manufacturerForm_address_label . "</td>
                            <td>" . $inv_vendorRCNO_label . "</td>
                            <td>Date Created</td>
                            <td><em>" . $inventory_item_unit_edit_label . "</em></td>
                        </tr>";
        $sNo = 0;
        $juggleRows = true;
        $rowStyle1 = " class=\"tr-row\" ";
        $rowStyle2 = " class=\"tr-row2\" ";
        foreach ($allMan as $disMan){
            $rowStyle = $juggleRows ? $rowStyle1 : $rowStyle2;
            $juggleRows = !$juggleRows;
            $display .= "<tr $rowStyle>
                            <td>" . (++$sNo) . ".</td>
                            <td>" . stripslashes($disMan["inventory_manufacturer_name"]) . "</td>
                            <td>" . stripslashes($disMan["inventory_manufacturer_address"]) . "</td>
                            <td>" . stripslashes($disMan["inventory_manufacturer_rcno"]) . "</td>
                            <td>" . stripslashes($disMan["dateCreated"]) . "</td>
                            <td><a href=\"index.php?p=inventory-edit-manufacturer&m=pharmacy&id=" . $disMan["inventory_manufacturer_id"] . "\"><img border=\"0\" src=\"images/edit.png\" title=\"" . $inventory_item_unit_edit_label . "\" alt=\"" . $inventory_item_unit_edit_label . "\" /></a></td>
                         </tr>";
        }
        $display .= "</table>";
    } else $display = "No manufacturer added yet.";
    echo $display;
?>
