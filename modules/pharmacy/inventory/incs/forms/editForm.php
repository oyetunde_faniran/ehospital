<?php 

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



$pageClass= new inventory_forms_class();

if(isset($_POST['update'])){
    $id=$_POST['form_id'];
    $name=trim($_POST['formName']);
    $desc=trim($_POST['formDescription']);
    $author=$_SESSION[session_id()."userID"];
    
    if($pageClass->updateForm($id,$name,$desc,$author)){
        echo $inventory_form_successfully_updated;
    }else{
        echo $inventory_form_update_failure_message;
    }
}


$formProperties=$pageClass->getformProperties($_GET['edit']);
if(is_string($formProperties)){
    echo $formProperties;
        exit;
        }
echo "<h2>".$inventoryEditingForm ."
    &nbsp'".$formProperties['inventory_form_name']. "'</h2>"; ?>
<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="POST" enctype="multipart/form-data">
    <table border="0">

        <tr><td><?php echo $inv_formsForm_name_label; ?></td><td><input type="text" name="formName" value="<?php echo $formProperties['inventory_form_name']; ?>" /></td></tr>

        <tr><td valign="top"><?php echo $inv_formForm_description_label; ?></td><td><textarea rows="4" wrap="soft" title="<?php echo $inventory_form_describe_the_form;?> " name="formDescription"><?php echo $formProperties['inventory_form_description']; ?></textarea></td></tr>

        <tr><td>&nbsp;</td><td><input type="hidden" value="<?php echo $formProperties['inventory_form_id']; ?>" name="form_id" /><input type="submit" name="update" value="<?php echo $inventory_update; ?>" class="button" /></td></tr>
    </table>
</form>