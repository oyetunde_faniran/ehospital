<?php 
$createMedicalName = new inventory_medical_name_class();
if (isset($_POST['setup'])) {
    $name=trim($_POST['medicalName']);
    $report = $createMedicalName->createNewMedicalName($name);
    echo $report;
    if ($report == $inventoryMedicalNameCreationSuccessNews) {
        unset($_POST);
    } 
}
?>

<h2><?php echo $inv_medicalNameForm_createLabel_prompt;?></h2>

<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST" enctype="multipart/form-data">

    <table border="0">

        <tr><td><?php echo $inv_medicalNameForm_name_label; ?></td><td><input type="text" name="medicalName" <?php if (isset($_POST['medicalName'])) {
    echo "value=\"" . $_POST['medicalName'] . "\"";
} ?> /></td></tr>

        <tr><td>&nbsp;</td><td><input type="submit" name="setup" value="<?php echo $inventory_setup; ?>" class="button" /></td></tr>
    </table>
</form>