<?php
    $hospitalNumber = isset($_GET["h"]) ? $_GET["h"] : "";
    $transNo = isset($_GET["t"]) ? $_GET["t"] : "";
    $pat = new Patient();
    $patInfo = $pat->getPatientInfo($hospitalNumber);
    //If the hospital number is invalid, then go to the hospital number entry page for dispensing
    if (!is_array($patInfo)){
        header ("Location: index.php?p=inventory_dispense_item&m=pharmacy");
    }
    
    //die ("<pre>" . print_r ($_POST, true) . "</pre>");
    $dispenseArray = isset($_POST["dispensequantity"]) ? $_POST["dispensequantity"] : array();
    $drugLabelArray = isset($_POST["druglabel"]) ? $_POST["druglabel"] : array();
    $batchDetailsArray = isset($_POST["batchdetails"]) ? $_POST["batchdetails"] : array();
    $invDispense = new inventory_dispense();
    //$dispenseArray, $drugLabelArray, $batchDetailsArray, $hospitalNumber, $locationID, $userID
    $error = $invDispense->doDispense($dispenseArray, $drugLabelArray, $batchDetailsArray, $hospitalNumber, $inventory_locationID, $_SESSION[session_id() . "userID"]);
    if (!$error){	//Operation successful
        //If the action was carried out based on a transaction, update the service rendered flag of the transaction to "Yes"
        if (!empty($transNo)){
            $inv = new invoice();
            $inv->updateServiceRenderedFlag($transNo);
        }
        $c = "Dispense was carried out successfully and the stock updated.";
        $c = urlencode($c);
        $dispenseID = $invDispense->dispenseID;
        $page = "index.php?p=inventory_dispense_item_act_details&m=pharmacy&d=" . $dispenseID;
        header ("Location: $page");
    } else {
            $_GET["c"] = !empty($invDispense->errorMsg) ? $invDispense->errorMsg : "Sorry! Operation failed. Please, try again.";
    }
?>