<?php
    //Get either the hospital number or transaction ID from $_GET
    $hospitalNumber = isset($_GET["h"]) ? $_GET["h"] : "";
    $transID = isset($_GET["t"]) ? $_GET["t"] : "";
    
    //Get the patient info to be sure the hospital number is OK
    if ($_POST || !empty($hospitalNumber)){
        $hospitalNumber = isset($_POST["hospitalnumber"]) ? $_POST["hospitalnumber"] : $hospitalNumber;
        $pat = new Patient();
        $patInfo = $pat->getPatientInfo($hospitalNumber);
        //If the hospital number is valid, then go to the actual dispensing page
        if (is_array($patInfo)){
            header ("Location: index.php?p=inventory_dispense_item_act&m=pharmacy&h=$hospitalNumber&t=$transID");
        }
        
        if ($_POST){
            echo "<div class=\"comments\">The hospital number you entered is invalid.</div>";
        }
        
    }
    
    if (isset($_GET["c"])){
        echo "<div class=\"comments\">" . $_GET["c"] . "</div>";
    }
    
?>
    Please, enter the hospital number of the patient that drugs would be dispensed to.
    <form action="" method="post">
        <table cellpadding="3" cellspacing="3">
            <tr>
                <td><label>HOSPITAL NUMBER:</label></td>
                <td><input type="text" name="hospitalnumber" id="hospitalnumber" value="<?php echo $hospitalNumber; ?>" /></td>
                <td><input type="submit" name="sButton" id="submit-button" value="Continue" /></td>
            </tr>
        </table>
    </form>