<?php 
$createLocation = new inventory_location_class();
if (isset($_POST['setup'])) {
    $name=trim($_POST['locationName']);
    $desc=trim($_POST['locationDescription']);

    $report = $createLocation->createNewLocation($name,$desc);
    echo $report;
    if ($report == $inventoryLocationCreationSuccessNews) {
        unset($_POST);
    } 
}
?>

<h2><?php echo $inv_locationForm_createLabel_prompt;?></h2>

<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST" enctype="multipart/form-data">

    <table border="0">

        <tr><td><?php echo $inv_locationForm_name_label; ?></td><td><input type="text" name="locationName" <?php if (isset($_POST['locationName'])) {
    echo "value=\"" . $_POST['locationName'] . "\"";
} ?> /></td></tr>

        <tr><td valign="top"><?php echo $inv_locationForm_description_label; ?></td><td><textarea rows="4" name="locationDescription"><?php if (isset($_POST['locationDescription'])) {
    echo $_POST['locationDescription'];
} ?> </textarea> </td></tr>

        <tr><td>&nbsp;</td><td><input type="submit" name="setup" value="<?php echo $inventory_setup; ?>" class="button" /></td></tr>
    </table>
</form>