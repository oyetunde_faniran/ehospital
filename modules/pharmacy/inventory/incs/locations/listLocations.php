<?php include 'modules/pharmacy/inventory/incs/javascriptOperations.php';
$pageClass = new inventory_location_class();


if(isset($_GET['remove'])){
    $report=$pageClass->removeLocation($_GET['remove']);
    echo $report;
}

echo "<h2>".$inventory_locations."</h2>";

$rows=$pageClass->listLocations();
if(is_string($rows)){
    echo $rows;
    }else{
?>
<table width="100%">
    <tr><th align="left"><?php echo $inv_locationForm_name_label;?></th><th><?php echo $inventory_edit;?></th></tr>
    <tr><td colspan="3"><hr/></td></tr>
    <?php while ($row = $rows->fetch_assoc()) {?>
    <tr>
        
        <td align="left" title="<?php echo $row['inventory_location_description'];?>"><?php echo $row['inventory_location_name'];?></td>
        
        <td align="center"><a href="index.php?p=inventory edit location&m=pharmacy&folder=inventory_list&&inventoryAction=locations&edit=<?php echo $row['inventory_location_id'];?>" title="<?php echo $inventory_click_to_edit;?>" ><img src="images/btn_edit.gif" /></a></td>
    
    </tr>
        <tr><td colspan="3"><hr style="border: 1px dashed #CCC" /></td></tr>
    <?php }?>
</table>
    <?php }?>
<h2><a href="index.php?p=inventory add location&m=pharmacy&folder=inventory_list&&inventoryAction=locations&addNew=1"><?php echo $inventory_add_new_location;?></a></h2>