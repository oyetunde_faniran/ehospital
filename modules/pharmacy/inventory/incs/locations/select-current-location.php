<?php
    $userObj = new admin_user();
    $currentLocations = $userObj->getCurrentUserLocations($_SESSION[session_id() . "userID"], $_SESSION[session_id() . "deptID"]);
    //print_r ($currentLocations);
    if (count($currentLocations) > 0){
        $comments = isset($_GET["c"]) ? "<div class=\"comments\">" . $_GET["c"] . "</div>" : "";
        $display = $comments. '<p>Please, select the location that you would like to work with.</p>
                    <form action="" method="post" name=\"locationconfigureform\" id=\"locationconfigureform\">
                       <select name="currentlocation" id="current-location">
                            <option value="0">--Select--</option>';
        foreach ($currentLocations as $locID => $loc){
            $selected = $inventory_locationID == $locID ? " selected=\"selected\" " : "";
            $display .= "<option $selected value=\"" . $locID. "\">" . $loc . "</option>";
        }
        $display .= "";
        $display .= '   </select>
                        <input class="btn" type="submit" value="Select Location" name="sButton" id="sButton" />
                    </form>';
    } else $display = "You don't have any locations assigned to you. Please, contact your adminstrator.";
    echo $display;
?>
