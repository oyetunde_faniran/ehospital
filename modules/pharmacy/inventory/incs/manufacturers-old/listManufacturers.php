<?php  include 'modules/pharmacy/inventory/incs/javascriptOperations.php';
$pageClass = new inventory_manufacturer_class();


if(isset($_GET['remove'])){
    $report=$pageClass->removeManufacturer($_GET['remove']);
    echo $report;
}


echo "<h2>".$inventory_manufacturers."</h2>";

$rows=$pageClass->listManufacturers();
if(is_string($rows)){
    echo $rows;
    }else{
?>
<table width="100%">
    <tr><th align="left"><?php echo $inv_manufacturerForm_name_label;?></th><th><?php echo $inventory_edit;?></th><th><?php echo $inventory_remove;?></th></tr>
    <tr><td colspan="3"><hr/></td></tr>
    <?php while ($row = $rows->fetch_assoc()) {?>
    <tr>
        
        <td align="left" title="<?php echo $row['inventory_manufacturer_address'];?>"><a href="index.php?p=inventory show manufacturer&m=pharmacy&folder=inventory_list&&inventoryAction=vendors&show=<?php echo $row['inventory_manufacturer_id'];?>"><?php echo $row['inventory_manufacturer_name'];?></a></td>
        
        <td align="center"><a href="index.php?p=inventory edit manufacturer&m=pharmacy&folder=inventory_list&&inventoryAction=vendors&edit=<?php echo $row['inventory_manufacturer_id'];?>" ><img src="images/btn_edit.gif" /></a></td>
        <td align="center"><a href="index.php?p=inventory remove manufacturer&m=pharmacy&folder=inventory_list&&inventoryAction=vendors&remove=<?php echo $row['inventory_manufacturer_id'];?>" onClick="javascript:confirm_to_delete(this.href, '<?php echo $inventory_manufacturer;?>'); return false; "> 
                <img src="images/b_drop.png" title="<?php echo $inventory_click_to_remove_manufacturer; ?>" />
            </a>
        </td>
    
    </tr>
        <tr><td colspan="3"><hr style="border: 1px dashed #CCC" /></td></tr>
    <?php }?>
</table>
    <?php }?>
<h2><a href="index.php?p=inventory add manufacturer&m=pharmacy&folder=inventory_list&&inventoryAction=vendors&addNew=1"><?php echo $inventory_add_new_manufacturer;?></a></h2>