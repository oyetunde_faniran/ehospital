<script src="library/inventory/ajaxFunctions.js"></script>
<?php
include_once ("./includes/pagination-init-pharmacy.php");
$transfer=new inventory_transfer_class();

$unitsClass=new inventory_units();

if(isset($_POST['transfer'])&&(!empty($_POST['qty']))&&(!empty($_POST['destLocation']))){
    
if(!$transfer->checkPost($_POST)){
    
    $destination=(int)($_POST['destLocation']);
    $genRec=$transfer->record_transfer($inventory_locationID,$_POST['destLocation']);
    
    foreach ($_POST['qty'] as $itemId=>$unitId){
         $qtyForUse=0;
         $batchId=$_POST['batch'][$itemId];
//         echo "batch Id =".$batchId;
         
        foreach ($unitId as $qtity=>$value){
           
            $lowestQty=$unitsClass->convertToLowestUnit($qtity, $itemId, $value);
            $qtyForUse+=$lowestQty;
            
//            echo "Item Id =".$itemId." <br />Unit Id=".$qtity." <br />Qty to be transfered =".$lowestQty."<br /> while the transfer Id Id=".$genRec."<br />The source Location =".$inventory_locationID."<br />Destination location=".$_POST['destLocation']."<br /><hr style=\"border:dashed 1px #CCC\" />";
        }
        $batchDetailsInfo=$transfer->getBatchDetailsInfo($itemId, $batchId);
        $batchDetailsId=$batchDetailsInfo['inventory_batchdetails_id'];
        $batchDetailsExpirydate=$batchDetailsInfo['inventory_batchdetails_expirydate'];
//        $allowableQty=$transfer->getItemQtyForBatch($itemId,$batchId);
//        if($qtyForUse>$allowableQty){
//            $warning= "You have chosen a total transfer quantity fro one or more items greater than the available quantity for such item for the chosen batch. Please choose valid tranfer quantites and retry<br />";
//        }else
            if($qtyForUse>0){
            $transferred=$transfer->recordTransferDetails($genRec, $itemId, $batchDetailsId,$batchDetailsExpirydate, $qtyForUse, '1');
        }            

        
        
//         echo "Total transfereable quantity for this item for this batch is= ".$transfer->getItemQtyForBatch($itemId,$batchId)."<br/>";
//         echo "Batch Details Id=".$transfer->getBatchDetailsId($itemId, $batchId);
//         
    }
      echo (isset($transferred))?"<div class=\"comments\">".$transferred."</div>":"";
    }else{
        echo "<div class=\"comments\">".$transfer->checkPost($_POST)."</div>";
        } 
}

$args["total"] = $transfer->listItemsInStock($inventory_locationID, true);
$paginationText = Paginator::doPagination($args);
$paginationText = "<div class=\"pagination-container\">" . $paginationText . "</div>";
echo $paginationText;
?>


<form name="transferForm" action="" method="POST" enctype="multipart/form-data" onSubmit="return confirmDest();">
    
    <table border="0" width="98%">
        <tr class="title-row"><th align="left" colspan="4"><?php echo $inv_transferForm_Source_label;?></th><th align="left" colspan="3"><?php echo $inv_transferForm_destination_label;?></th></tr>
       
        <tr><th colspan="4" align="left"><?php echo $transfer->getLocationName($inventory_locationID);?></th><th colspan="3" align="left">
                
                <select name="destLocation" id="destLocation"><option selected value="0">Select</option>
                <?php $locations=$transfer->getAllLocationsExceptDis($inventory_locationID);
                while($location=$locations->fetch_assoc()){
                    echo "<option value=\"".$location['inventory_location_id']."\">".$location['inventory_location_name']."</option>";
                }?></select>
            
            </th></tr></table> 
<?php $stockItems=$transfer->listItemsInStock($inventory_locationID);

if(is_string($stockItems)){
    echo $stockItems;
        }else{
            $counter=1;
    echo "<table width=\"98%\" border=\"0\">
        <tr class=\"title-row\"><th align=\"right\">#</th><th align=\"left\">".$inventory_item_commercial_name."</th><th align=\"left\">".$inventory_item_medical_name."</th><th align=\"left\">".$inventory_manufacturer."</th><th align=\"left\">".$inventory_presentation."</th><th align=\"left\">".$inventory_form."</th><th align=\"left\">".$inventory_strength."</th><th align=\"left\">".$inventory_batch_expiry."</th><th align=\"left\">".$inventory_stock_bal."</th></tr>";
while($stockItem=$stockItems->fetch_assoc()){
    if($counter%2){$bgColor="tr-row";}else{$bgColor="tr-row2";}
    echo "<tr class=\"".$bgColor."\"><td align=\"right\">".$counter."</td>
        <td align=\"left\">".$stockItem['drug_name']."</td>
            <td align=\"left\">".$transfer->getMedicalName($stockItem['drug_desc'])."</td>
                <td align=\"left\">".$transfer->getManufacturerName($stockItem['drug_manufacturer'])."</td>
                    <td align=\"left\">".$transfer->getPresentationName($stockItem['drug_presentation'])."</td>
                        <td align=\"left\">".$transfer->getFormName($stockItem['drug_dosageform'])."</td>
                            <td align=\"left\">".$stockItem['drug_strength']."</td>
                                <td align=\"left\"><select name=\"batch[".$stockItem['drug_id']."]\" onchange=\"showItemStockBal(".$stockItem['drug_id'].",this.value)\"><option value=\"\">Select batch</option>";  $batches=$transfer->getItemStockBatches($stockItem['drug_id']);
    while($batch=$batches->fetch_assoc()){
        echo "<option value=\"".$batch['inventory_batch_id']."\">".$batch['inventory_batch_id']." (".$batch['expiryDate'].")</option>";
    }
    echo "</select></td><td align=\"left\"><div id=\"splitQties".$stockItem['drug_id']."\"></div></td></tr>";$counter++;
}
    
    ?> <tr><td colspan="8">&nbsp;</td><td align="right"><input type="submit" name="transfer" value="<?php echo $inventory_move;?>" class="btn" onclick="confirmDest();"/></td></tr>
    </table></form>
<?php
    echo $paginationText;
}
?>        
       
