<?php 
$receipt=new inventory_transfer_class();
if(!empty($_GET['transfer'])){
    $id=((int)($_GET['transfer']));
    $transfered=$receipt->record_receipt($id,$inventory_locationID);
    if($transfered){
        $transferedItems=$receipt->getItemsInDisTransfer($id);
        if(is_string($transferedItems)){
            echo "<div class=\"comment\">".$transferedItems."</div>";
        }else{
            foreach($transferedItems as $item=>$value){
                $itemId=$value['inventory_item_id'];
                $recordDetails=$receipt->record_receipt_details($itemId, $id, $transfered);
            }
            echo (!empty($recordDetails))?"<div class=\"comment\">".$recordDetails."</div>":"<div class=\"comment\">Unable to record details of this receipt</div>";
        }
    }
}
?>