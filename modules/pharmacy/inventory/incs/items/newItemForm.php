<?php
include 'modules/pharmacy/inventory/incs/javascriptOperations.php';

$pageClass = new inventory_item_class();

if ($_POST) {
    //die ("<pre>" . print_r ($_POST, true) . "</pre>");
    extract($_POST);

    if (empty($cat)) {
        //echo $inventory_noCategory_msg;
        $_GET["comments"] = $inventory_noCategory_msg;
    } else {
        $medicalName = (int)$medicalName;
        $presentation = (int)$presentation;
        $form = (int)$form;
        $manufacturer = (int)$manufacturer;

        $report = $pageClass->addNewItem($itemName, $medicalName, $reorderLevel, $cat, $form, $strength, $presentation, $manufacturer, $nafdac, $location/*, $sellingprice, $markup, $pricetouse*/, $sideeffects);
        $invUnit = new inventory_units();
        //$error = $invUnit->saveItemUnits($pageClass->itemID, $_POST["unit"], $_POST["unitquantity"], $_SESSION[session_id() . "userID"]);
        $error = $invUnit->saveItemUnits($pageClass->itemID, $_POST["unit"], $_POST["unitquantity"], $_POST["unitprice"], $_SESSION[session_id() . "userID"]);
        //echo $report;
        if ($report == $inventoryItemCreationSuccessNews) {
            //unset($_POST);
            $page = $_SERVER["PHP_SELF"] . "?" . $_SERVER["QUERY_STRING"] . "&comments=" . urlencode($report);
            header("Location: " . $page);
        }
    }
}

//if ($_POST) {
//    //die ("<pre>" . print_r ($_POST, true) . "</pre>");
//    extract($_POST);
//
//    if (!isset($cat)) {
//        //echo $inventory_noCategory_msg;
//        $_GET["comments"] = $inventory_noCategory_msg;
//    } else {
//        $medicalName = $pageClass->getMedicalName($medicalName);
//        $presentation = $pageClass->getPresentationName($presentation);
//        $form = $pageClass->getFormName($form);
//        $manufacturer = $pageClass->getManufacturerName($manufacturer);
//
//        $report = $pageClass->addNewItem($itemName, $medicalName, $reorderLevel, $cat, $form, $strength, $presentation, $manufacturer, $nafdac, $location/*, $sellingprice, $markup, $pricetouse*/, $sideeffects);
//        $invUnit = new inventory_units();
//        $error = $invUnit->saveItemUnits($pageClass->itemID, $_POST["unit"], $_POST["unitquantity"], $_POST["unitprice"], $_SESSION[session_id() . "userID"]);
//        //echo $report;
//        if ($report == $inventoryItemCreationSuccessNews) {
//            //unset($_POST);
//            $page = $_SERVER["PHP_SELF"] . "?" . $_SERVER["QUERY_STRING"] . "&comments=" . urlencode($report);
//            header("Location: " . $page);
//        }
//    }
//}


if (!empty($_GET["comments"])) {
    echo "<div class=\"comments\">" . $_GET["comments"] . "</div>";
}
?>
<!--<h2><?php//echo $inv_itemForm_create_prompt; ?></h2>
-->

<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST" enctype="multipart/form-data">

    <div id="item-tabs">
        <ul>
            <li><a href="#tabs-1"><?php echo $inventory_item_info_label; ?></a></li>
            <li><a href="#tabs-2"><?php echo $inventory_item_category_label; ?></a></li>
            <li><a href="#tabs-3"><?php echo $inventory_item_unit_label; ?></a></li>
            <!--<li><a href="#tabs-4"><?php echo "Item Pricing"; ?></a></li>-->
        </ul>




        <div id="tabs-1">
            <table border="0" width="100%">
<tr><td><?php echo $inventory_item_medical_name; ?></td><td>
                        <select name="medicalName">
                            <option value="">&nbsp;</option>
                            <?php $medicalNames = $pageClass->findMedicalNames();
                            if (is_string($medicalNames)) { ?>
                                <option value=""><?php echo $medicalNames; ?></option>
                            <?php } else {
                                while ($medicalName = $medicalNames->fetch_assoc()) { ?>
                                    <option value="<?php echo $medicalName['inventory_medical_name_id']; ?>"><?php echo $medicalName['inventory_medical_name']; ?></option>
                                <?php
                                }
                            }
                            ?>
                        </select></td></tr>
                <tr><td width="20%"><?php echo $inventory_item_commercial_name; ?></td><td><input type="text" name="itemName" <?php if (isset($_POST['itemName'])) {
    echo "value=\"" . $_POST['itemName'] . "\"";
} ?> /></td></tr>

                

                <tr><td><?php echo $inventory_manufacturer; ?></td><td>
                        <select name="manufacturer">
                            <option value="">&nbsp;</option>
                            <?php $forms = $pageClass->findItemManufacturers();
                            if (is_string($forms)) { ?>
                                <option value=""><?php echo $forms; ?></option>
                            <?php } else {
                                while ($form = $forms->fetch_assoc()) { ?>
                                    <option value="<?php echo $form['inventory_manufacturer_id']; ?>"><?php echo $form['inventory_manufacturer_name']; ?></option>
                                <?php
                                }
                            }
                            ?>
                        </select></td></tr>

                <tr><td><?php echo $inventory_form; ?><td>
                        <select name="form">
                            <option value="">&nbsp;</option>
                            <?php $forms = $pageClass->findItemForms();
                            if (is_string($forms)) { ?>
                                <option value=""><?php echo $forms; ?></option>
                            <?php } else {
                                while ($form = $forms->fetch_assoc()) { ?>
                                    <option value="<?php echo $form['inventory_form_id']; ?>"><?php echo $form['inventory_form_name']; ?></option>
    <?php
    }
}
?>
                        </select></td></tr>

    <tr><td><?php echo $inventory_presentation; ?></td><td>
        <select name="presentation"><option value="">&nbsp;</option>
            <?php $forms = $pageClass->findItemPresentations();
            if (is_string($forms)) { ?>
                <option value=""><?php echo $forms; ?></option>
            <?php } else {
                while ($form = $forms->fetch_assoc()) { ?>
                    <option value="<?php echo $form['inventory_presentation_id']; ?>"><?php echo $form['inventory_presentation_name']; ?></option>
    <?php
    }
    }
?>
                        </select></td></tr>

                <tr><td><?php echo $inventory_strength; ?></td><td>
                        <input type="text" name="strength" /></td></tr>

                <tr>
                    <td>NAFDAC No.</td>
                    <td><input type="text" name="nafdac" /></td>
                </tr>
                
                
                <tr>
                    <td>Side Effects</td>
                    <td>
                        <textarea rows="10" cols="50" name="sideeffects" id="side-effects"></textarea>
                    </td>
                </tr>
                

                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="hidden" name="reorderLevel" value="0" />
                        <?php
                        $locations = $pageClass->get_existing_items('inventory_locations', 'inventory_location_name', 'asc');
                        if (!$locations) {
                            echo $inventory_location_dbconnnect_error;
                        } elseif (is_string($locations)) {
                            echo $locations;
                        } else {
                            while ($location = $locations->fetch_array()) {
                                echo "<input type=\"hidden\" value=\"0\" name=\"location[" . $location['inventory_location_id'] . "]\" />";
                            }
                        }
                        ?>
                        <input type="button" id="next-to-tab-2" value=" <?php echo $inventory_item_next_label; ?> &raquo; " class="btn" />
                    </td>
                </tr>


            </table>
        </div>









        <div id="tabs-2">
            <?php
            include_once ("./modules/pharmacy/inventory/recursive-hierarchical-function.php");
            $invClass = new inventory_category_class();
            $display = "<table cellpadding=\"3\" cellspacing=\"3\" style=\"border: 5px solid #CCC\">
                    <tr>
                        <td><strong>S/NO</strong></td>
                        <td><strong>PRODUCT CATEGORY</strong></td>
                        <td>&nbsp;</td>
                    </tr>";
            $display .= getChildren(0, "", "", " class=\"tr-row\" ");
            $display .= "</table>";
            echo $display;
            ?>
            <div style="margin-top: 10px;">
                <input type="button" id="prev-to-tab-1" value=" &laquo; <?php echo $inventory_item_prev_label; ?> " class="btn" />
                <input type="button" id="next-to-tab-3" value=" <?php echo $inventory_item_next_label; ?> &raquo; " class="btn" />
            </div>
        </div>

        <div id="tabs-3">
<?php
include_once ("./modules/pharmacy/inventory/incs/items/new-item-units-tab.php");
?>
            <div style="margin-top: 10px;">
                <input type="button" id="prev-to-tab-2" value=" &laquo; <?php echo $inventory_item_prev_label; ?> " class="btn" />
                <input type="submit" name="setup" value=" <?php echo $inventory_setup; ?> " class="btn" />
                <!--<input type="button" id="next-to-tab-4" value=" <?php echo $inventory_item_next_label; ?> &raquo; " class="btn" />-->
            </div>
        </div>
        
        
        <!--<div id="tabs-4">
            <table>
                <tr>
                    <td>
                        <input type="radio" name="pricetouse" id="pricetouse-1" checked="checked" value="1" /> <label for="pricetouse-1">Use Selling Price (&#8358;):</label>
                    </td>
                    <td>
                        <input type="text" name="sellingprice" size="10" maxlength="20" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="radio" name="pricetouse" id="pricetouse-2" value="2" /> <label for="pricetouse-2">Use Mark-Up (&#8358;):</label>
                    </td>
                    <td>
                        <input type="text" name="markup" size="10" maxlength="20" />
                    </td>
                </tr>
            </table>
            <div style="margin-top: 10px;">
                <input type="button" id="prev-to-tab-3" value=" &laquo; <?php echo $inventory_item_prev_label; ?> " class="btn" />
                <input type="submit" name="setup" value=" <?php echo $inventory_setup; ?> " class="btn" />
            </div>
        </div>-->
        
        
    </div>
</form>



<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.sortable.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.tabs.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
<script type="text/javascript">
    $(function() {
        $("#item-tabs").tabs().find(".ui-tabs-nav").sortable({axis:'x'});
    });

    $('#next-to-tab-2').click(function(){
        $("#item-tabs").tabs("select", 1);
    });
    $('#next-to-tab-3').click(function(){
        $("#item-tabs").tabs("select", 2);
    });
    $('#prev-to-tab-1').click(function(){
        $("#item-tabs").tabs("select", 0);
    });
    $('#prev-to-tab-2').click(function(){
        $("#item-tabs").tabs("select", 1);
    });
    $('#next-to-tab-4').click(function(){
        $("#item-tabs").tabs("select", 3);
    });
    $('#prev-to-tab-3').click(function(){
        $("#item-tabs").tabs("select", 2);
    });
    
    function confirmSubmit(disForm){
        if (confirm("Are you sure you are ready to add this item?"))
            disForm.submit();
    }
        
</script>