<?php 
include 'modules/pharmacy/inventory/incs/javascriptOperations.php';
$units=new inventory_units();
$pageClass=new inventory_item_class();
if(isset($_GET['item'])&& is_numeric($_GET['item'])){
   $item=$pageClass->showItem($_GET['item']); 
   if(is_string($item)){
       echo $item;
       }else{

           ?>
<style>
    table tr {
        border-bottom: dashed 1px #CCC;
    }
</style>
 <div id="item-tabs">
        <ul>
            <li><a href="#tabs-1"><?php echo $inventory_item_info_label; ?></a></li>
            <li><a href="#tabs-2"><?php echo $inventory_item_balance_label; ?></a></li>
            <li><a href="#tabs-3"><?php echo $inventory_item_unit_label; ?></a></li>
            <!--<li><a href="#tabs-4"><?php echo "Item Pricing"; ?></a></li>-->
        </ul>



        <div id="tabs-1">
    <table border="0" width="100%">
        <?php $c=1; if($c%2){$trClass="tr-row";}else{$trClass="tr-row2";}?>
        <tr class="<?php echo $trClass;$c++;?>"><td><?php echo $inventory_item_medical_name;?></td><td>
               <?php echo $pageClass->getMedicalName($item['drug_desc']);?></td></tr>
        <tr class="<?php echo $trClass;$c++;?>"><td><?php echo $inventory_item_commercial_name;?></td><td><?php echo $item['drug_name']; ?></td></tr>
        
        
        <tr class="<?php echo $trClass;$c++;?>"><td valign="top"><?php echo $inventory_categories;?></td><td>
                <?php $pageClass->getItemCategories($item['drug_id']);
                ?>
                </td></tr>
        
        <tr class="<?php echo $trClass;$c++;?>"><td><?php echo $inventory_manufacturer;?></td><td>
                <?php echo $pageClass->getManufacturerName($item['drug_manufacturer']);?></td></tr>
        
         <tr class="<?php echo $trClass;$c++;?>"><td><?php echo $inventory_form;?><td>
                <?php echo $pageClass->getFormName($item['drug_dosageform']);?></td></tr>
        
         <tr class="<?php echo $trClass;$c++;?>"><td><?php echo $inventory_presentation;?></td><td>
                 <?php echo $pageClass->getPresentationName($item['drug_presentation']);?></td></tr>
         
                  <tr class="<?php echo $trClass;$c++;?>"><td><?php echo $inventory_strength;?></td><td>
                         <?php echo $item['drug_strength'];?></td></tr>

<tr class="<?php echo $trClass;$c++;?>"><td>NAFDAC No.</td><td>
                          <?php echo $item['inventory_item_nafdac'];?></td></tr>                  
    </table>
        </div>
            <div id="tabs-2">
                <table width="98%">
                    <tr class="<?php echo $trClass;$c++;?>"><td align="left"><?php echo $inventory_reorder_level;?></td><td colspan="2"><?php echo $inventory_balance;?>
                          </td></tr>
<tr class="<?php echo $trClass;$c++;?>">
    <td align="left" valign="top"><?php $itemUnits=$units->showInAllUnits($pageClass->getReorderLevel($item['drug_id']), $item['drug_id']);
  
                $display = "<table cellpadding=\"3\" cellspacing=\"3\">";
    
                foreach ($itemUnits as $disUnit){
                    $display .= "<tr>
                                    <td>" . $disUnit["quantity"] . "</td>
                                    <td>" . $disUnit["name"] . "</td>
                                    
                                 </tr>";
                }   //END foreach
              
            
                 $display .= "</table>";
                echo $display; ?></td><td valign="top">
        
        <table width="100%"><tr><td> <?php 
    $bal2=$pageClass->getLocationTransitQtyForItem($item['drug_id']);
    $totalQty=$pageClass->getLocationAvailBalForItem($item['drug_id'])+$bal2;
    
    
    echo "<strong>".$inventory_stock_avail_bal."</strong>";
        
    $itemUnits=$units->showInAllUnits($pageClass->getLocationAvailBalForItem($item['drug_id']), $item['drug_id']);
  
                $display = "<table cellpadding=\"3\" cellspacing=\"3\">";
    
                foreach ($itemUnits as $disUnit){
                    $display .= "<tr>
                                    <td>" . $disUnit["quantity"] . "</td>
                                    <td>" . $disUnit["name"] . "</td>
                                    
                                 </tr>";
                }   //END foreach
              
            
                 $display .= "</table>";
                echo $display;?>
                </td><td>
                <?php echo "<strong>".$inventory_stock_in_transit."</strong>";
                
                $itemUnits=$units->showInAllUnits($pageClass->getLocationTransitQtyForItem($item['drug_id']), $item['drug_id']);
                $display = "<table cellpadding=\"3\" cellspacing=\"3\">";
                     
                foreach ($itemUnits as $disUnit){
                    $display .= "<tr>
                                    <td>" . $disUnit["quantity"] . "</td>
                                    <td>" . $disUnit["name"] . "</td>
                                    
                                 </tr>";}
                //END foreach
                $display .= "</table>";
                echo $display;
                ?>    
                    
                </td></tr></table>
        
       
   </td>
</tr>
    </table></div>
     <div id="tabs-3">
         <?php $unitsArray=$units->getItemUnits($item['drug_id']);
         //foreach ($unitsArray as $unit){
//             if($unit['inventory_units_items_child_id']!='0'){echo $units->getUnitName(($unit['inventory_unit_id']))."<p>";}
//             echo $unit['inventory_units_items_child_quantity']." ".$units->getUnitName($unit['inventory_unit_id']).  " makes 1 ";
if(is_array($unitsArray)&&(!empty($unitsArray))){
             for($i=0; $i<count($unitsArray); $i++){
                 if(($i>0)&&($i<count($unitsArray)-1)){
                     echo $units->getUnitName(($unitsArray[$i]['inventory_unit_id']))."<p>";
                     }
if($i<count($unitsArray)-1){
    echo $unitsArray[$i]['inventory_units_items_child_quantity']." ";
    } 
    
echo $units->getUnitName($unitsArray[$i]['inventory_unit_id']);

if($i<count($unitsArray)-1){
    echo " make 1 ";
        }
    }
}
        //  }
         
         ?>
     </div>
 </div>
<?php   } 
    }
?>

<link type="text/css" href="library/admin_jquery/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.sortable.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.tabs.js"></script>
<script type="text/javascript" src="library/admin_jquery/ui/ui.accordion.js"></script>
<script type="text/javascript">
    $(function() {
        $("#item-tabs").tabs().find(".ui-tabs-nav").sortable({axis:'x'});
    });

    $('#next-to-tab-2').click(function(){
        $("#item-tabs").tabs("select", 1);
    });
    $('#next-to-tab-3').click(function(){
        $("#item-tabs").tabs("select", 2);
    });
    $('#prev-to-tab-1').click(function(){
        $("#item-tabs").tabs("select", 0);
    });
    $('#prev-to-tab-2').click(function(){
        $("#item-tabs").tabs("select", 1);
    });
    $('#next-to-tab-4').click(function(){
        $("#item-tabs").tabs("select", 3);
    });
    $('#prev-to-tab-3').click(function(){
        $("#item-tabs").tabs("select", 2);
    });
    
    function confirmSubmit(disForm){
        if (confirm("Are you sure you are ready to add this item?"))
            disForm.submit();
    }
        
</script>
