<?php
include 'modules/pharmacy/inventory/incs/javascriptOperations.php';
include_once ("./includes/pagination-init-pharmacy.php");

$pageClass = new inventory_item_class();

if (isset($_GET['remove'])) {
    $report = $pageClass->removeItem($_GET['remove']);
    echo $report;
}

//echo "<h2>".$inventory_items."</h2>";
//$mainCategories =
$args["total"] = $pageClass->listItems('', true);
$paginationText = Paginator::doPagination($args);
$paginationText = "<div class=\"pagination-container\">" . $paginationText . "</div>";
$items = $pageClass->listItems('');
if (is_string($items)) {
    echo $items;
} else {
    $dUnits = new inventory_units();
    echo $paginationText;
?>
    <table width="100%">
        <tr class="title-row">
            <th align="left"><?php echo $inventory_item_medical_name; ?></th>
            <th align="left"><?php echo $inventory_item_commercial_name; ?></th>
            <th align="left"><?php echo $inventory_manufacturer;?></th>
            <th align="left"><?php echo $inventory_presentation;?></th>
            <th align="left"><?php echo $inventory_strength;?></th>
            <th align="left"><?php echo $inventory_form;?></th>
            <th align="left"><?php echo $inventory_stock_bal; ?></th>
            <th align="right"><?php echo $inventory_action;?></th>
            
        </tr>
<?php

    $juggle = true;
    $rowStyle1 = " class=\"tr-row\" ";
    $rowStyle2 = " class=\"tr-row2\" ";
    while ($item = $items->fetch_assoc()) {
        $transitQty=$pageClass->qtyInTransit($item['drug_id']);
        $rowStyle = $juggle ? $rowStyle1 : $rowStyle2;
        $juggle = !$juggle;
        $bal=$pageClass->getLocationAvailBalForItem($item['drug_id'])+$pageClass->getLocationTransitQtyForItem($item['drug_id']);
//        $bal=$item['bal']+$transitQty;
        $allUnits = $dUnits->showInAllUnits($bal, $item['drug_id']);
        //die ("<pre>" . $item['bal'] . " --> " . print_r ($allUnits, true) . "</pre>");
        $disUnit = "<table cellpadding=\"3\" cellspacing=\"3\">
                        <tr>
                            <th align=\"left\">UNIT</th>
                            <th align=\"left\">QUANTITY</th>
                         </tr>";
        foreach ($allUnits as $unit){
            $disUnit .= "<tr>
                            <td align=\"left\">" . stripslashes($unit["name"]) . "</td>
                            <td align=\"center\">" . stripslashes($unit["quantity"]) . "</td>
                         </tr>";
        }
        $disUnit .= "</table>";
        echo "<tr $rowStyle>
                <td align=\"left\" title=\"" . $inventory_click_to_see_details . "\"><a href=\"index.php?p=inventory show item&m=pharmacy&item=" . $item['drug_id'] . "\">" . $pageClass->getMedicalName($item['drug_desc']) . "</a></td>
                <td align=\"left\" title=\"" . $inventory_click_to_see_details . "\"><a href=\"index.php?p=inventory show item&m=pharmacy&item=" . $item['drug_id'] . "\">" . $item['drug_name'] . "</a></td>
                <td align=\"left\">".$pageClass->getManufacturerName($item['drug_manufacturer'])."</td>
                <td align=\"left\">".$pageClass->getPresentationName($item['drug_presentation'])."</td>
                <td align=\"left\">".$item['drug_strength']."</td>
                <td align=\"left\">".$pageClass->getFormName($item['drug_dosageform'])."</td>
                <td align=\"left\" valign=\"top\">".$disUnit."</td>
                <td align=\"center\" valign=\"top\">
                <p><a href=\"index.php?p=inventory show item&m=pharmacy&item=" . $item['drug_id'] . "\">".$inventory_view."</a></p>
                    <p>
        <a href=\"index.php?p=inventory_edit_item&m=pharmacy&item=" . $item['drug_id'] . "\">".$inventory_edit."</a>            
</p>
<p>
        <a href=\"index.php?p=inventory_manage_reorderLevel&m=pharmacy&item=" . $item['drug_id'] . "\">".$inventory_reorder_level."</a>            
</p>
</td>
              </tr>";
    }
?>
    </table>
<?php
        echo $paginationText;
    }
?>