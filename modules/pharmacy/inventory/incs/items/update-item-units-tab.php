<?php
    $invUnit = new inventory_units();
    $allUnits = $invUnit->getAllUnits();
    $allUnitsDD4JS = $allUnitsDD = "";
    if (is_array($allUnits)){
        foreach ($allUnits as $unit){
            $allUnitsDD .= "<option value=\"" . $unit["inventory_unit_id"] . "\">" . stripslashes($unit["inventory_unit_name"]) . "</option>";
            $allUnitsDD4JS .= "newDoc += '<option value=\"" . $unit["inventory_unit_id"] . "\">" . stripslashes($unit["inventory_unit_name"]) . "</option>';\n";
        }
    }
    $display = "";
?>
    <div style="margin-bottom: 10px;">
        You can click the &quot;Add More&quot; button to add to the above units for measuring this item.
    </div>
    <div style="margin-bottom: 10px;">
        <input type="button" id="more-units" onclick="addMore();" value=" Add More " class="btn" />
    </div>

    <div>
        <span style="display: none;" id="unit-quantity-container-1">
            <input type="text" size="5" maxlength="10" value="1" name="unitquantity[]" id="unit-quantity-1" />
        </span>
        <select name="unit[]" id="unit-1">
            <option value="0">--Select--</option>
<?php
    echo $allUnitsDD;
?>
        </select>
        <span style="display: none;" id="units-will-make-container-1" style="display: none;"> will make one (1) of the next unit.</span>
    </div>
    <div id="units-container"></div>
    
    
    
    <script type="text/javascript">
        nextDocument = 2;
        allDocuments = 1;
        function addMore(){
            $("#unit-quantity-container-1").fadeIn();
            $("#units-will-make-container-1").fadeIn();
            try {
                cont = document.getElementById("units-container");
                newDoc = '<span id="unit-quantity-container-' + nextDocument + '"><input type="text" size="5" maxlength="10" value="1" name="unitquantity[]" id="unit-quantity-' + nextDocument + '" /></span>';
                newDoc += ' <select name="unit[]" id="unit-' + nextDocument + '">';
                newDoc += '<option value=\"0\">--Select--</option>';

<?php
    echo $allUnitsDD4JS;
?>
                newDoc += '</select> will make one (1) of the next unit.';
                newDoc += ' <input type=\"button\" value=\"Remove\" class=\"btn\" onclick=\"removeDocument(' + nextDocument + ');\" />';

                newDiv = document.createElement("div");
                newDiv.id = "add-on-units-container-" + nextDocument;
                newDiv.innerHTML = newDoc;
                cont.appendChild(newDiv);

                nextDocument++;
                allDocuments++;
            } catch (e) {
                alert ("An error occurred while trying to add a new unit.");
            }
        }

        function removeDocument(whichDoc){
            c = "add-on-units-container-" + whichDoc;
            try {
                c = document.getElementById(c);
                c.parentNode.removeChild(c);
                allDocuments--;
                if (allDocuments == 1){
                    $("#unit-quantity-container-1").fadeOut();
                    $("#units-will-make-container-1").fadeOut();
                }
            } catch (e) {
                alert ("An error occurred while trying to remove a unit.");
            }
        }
    </script>