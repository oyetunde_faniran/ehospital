<?php
$createUnit = new inventory_units_class();
if (isset($_POST['setup'])) {
    $name = trim($_POST['unitName']);
    $parent = $_POST['parentUnit'];

    $report = $createUnit->addNewUnit($name,$parent);
    echo $report;
    if ($report == $inventoryUnitCreationSuccessNews) {
        unset($_POST);
    }
}
?>
<!--<h2><?php //echo $inv_unitForm_createLabel_prompt; ?></h2>-->

<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST" enctype="multipart/form-data">

    <table border="0">

        <tr><td><?php echo $inv_unitForm_name_label; ?></td><td><input type="text" name="unitName" <?php if (isset($_POST['unitName'])) {
    echo "value=\"" . $_POST['unitName'] . "\"";
} ?> /></td></tr>

        <tr><td><?php echo $inv_unitForm_parent_label; ?></td>
            <td>
                <select name="parentUnit">
                    <?php if(!isset($_GET['add'])){?>
                    <option selected value="0"><?php echo $inventory_no_parent;?></option><?php 
                    } else {
                        $catname=$createUnit->getUnitName($_GET['add']);?>
                       <option selected value="<?php echo $_GET['add'];?>"><?php echo $catname;?></option>     
                         <?php 
                    }
                    $cats=$createUnit->get_existing_items('inventory_units','inventory_unit_name','asc');
                    if(is_string($cats)){
                        echo $cats;
                        }elseif(!$cats){
                            echo $inventory_error_getting_existing_units;
                            }else{
                        while($cat=$cats->fetch_assoc()){
                            echo "<option value=\"".$cat['inventory_unit_id']."\">".$cat['inventory_unit_name']."</option>";
                        }
                    }
                    ?>
                </select></td>
        </tr>

        <tr><td>&nbsp;</td><td><input type="submit" name="setup" value="<?php echo $inv_SubmitBtnValue; ?>" class="button" /></td></tr>
    </table>
</form>