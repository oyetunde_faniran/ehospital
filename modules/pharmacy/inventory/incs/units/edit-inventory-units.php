<?php
    $id = isset($_GET["id"]) ? (int)$_GET["id"] : 0;
    $invUnit = new inventory_units();
    $disUnit = $invUnit->getUnit($id);
    if (is_array($disUnit)){
        echo isset($_GET["c"]) ? "<div class=\"comments\">" . $_GET["c"] . "</div>" : "";
?>
        <form action="" method="post">
            <table cellpadding="3" cellspacing="3">
                <tr>
                    <td>
                        <label for="item-unit-name"><?php echo strtoupper($inventory_item_unit_name) . ":"; ?></label>
                    </td>
                    <td>
                        <input type="text" name="itemunitname" id="item-unit-name" value="<?php echo isset($disUnit["inventory_unit_name"]) ? $disUnit["inventory_unit_name"] : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="item-unit-description"><?php echo strtoupper($inventory_item_unit_desc) . ":"; ?></label>
                    </td>
                    <td>
                        <textarea name="itemdescription" id="item-unit-description" rows="5" cols="20"><?php echo isset($disUnit["inventory_unit_desc"]) ? $disUnit["inventory_unit_desc"] : ""; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="checkbox" name="enabled" id="enabled" value="1" <?php echo $disUnit["inventory_unit_enabled"] == 1 ? "checked=\"checked\"" : ""; ?> />
                        <label for="enabled"><?php echo ucwords(strtolower($inventory_item_unit_enabled_label)); ?></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <input type="submit" name="sButton" id="submit-button" value="<?php echo ucwords($admin_button_savechanges); ?>" class="btn" />
                    </td>
                </tr>
            </table>
        </form>
<?php
    } else echo $inventory_item_unit_unknown_unit;
?>