<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



$pageClass= new inventory_units_class();

if(isset($_POST['update'])){
    $id=$_POST['unit_id'];
    $name=trim($_POST['unitName']);
    $parent=$_POST['parentUnit'];
    $author=$_SESSION[session_id()."userID"];
    
    if($pageClass->updateUnit($id,$name,$author,$parent)){
        echo $inventory_unit_successfully_updated;
    }else{
        echo $inventory_unit_update_failure_message;
    }
}


$unitProperties=$pageClass->getUnitProperties($_GET['edit']);
if(is_string($unitProperties)){
    echo $unitProperties;
        exit;
        }
echo "<h2>".$inventory_editingUnit ."&nbsp'". $unitProperties['inventory_unit_name']. "'</h2>"; ?>
<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="POST" enctype="multipart/form-data">
    <table border="0">

        <tr><td><?php echo $inv_unitForm_name_label; ?></td><td><input type="text" name="unitName" value="<?php echo $unitProperties['inventory_unit_name']; ?>" /></td></tr>

       <tr><td><?php echo $inv_unitForm_parent_label; ?></td>
            <td>
                <select name="parentUnit">
                    <option selected value="<?php echo $unitProperties['inventory_unit_parent']; ?>"><?php echo $pageClass->getUnitName($unitProperties['inventory_unit_parent']);?></option><option value="0">None</option>
                    <?php
                    $cats=$pageClass->get_existing_items('inventory_units','inventory_unit_name','asc');
                    if(is_string($cats)){echo $cats;}else{
                        while($cat=$cats->fetch_assoc()){
                            if($cat['inventory_unit_id']!==$unitProperties['inventory_unit_id']){
                            echo "<option value=\"".$cat['inventory_unit_id']."\">".$cat['inventory_unit_name']."</option>";
                            }
                        }
                    }
                    ?>
                </select></td>
        </tr>

        <tr><td>&nbsp;</td><td><input type="hidden" value="<?php echo $unitProperties['inventory_unit_id']; ?>" name="unit_id" /><input type="submit" name="update" value="<?php echo $inventory_update; ?>" class="button" /></td></tr>
    </table>
</form>