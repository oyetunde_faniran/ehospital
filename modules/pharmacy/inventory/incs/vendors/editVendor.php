<?php 
$vendor=new inventory_vendor_class();

if(isset($_POST['update'])){
    $name=trim($_POST['vendorName']);
    $address=trim($_POST['vendorAddress']);
    $yrOfInc=trim($_POST['vendorIncYr']);
    $license=trim($_POST['vendorLicense']);
    $tel=trim($_POST['vendorTel']);
    $rcno=trim($_POST['vendorRcNo']);
    $mobile=trim($_POST['vendorMobile']);
    $email=trim($_POST['vendorEmail']);
    $website=trim($_POST['vendorWebsite']);
    
    $updated=$vendor->updateVendor($name,$address,$yrOfInc, $rcno, $license, $tel, $mobile, $email,$website);
    
    if($updated==$inventory_vendor_updated_successfully){
        unset($_POST);
    }
    echo $updated;
}
if(($_GET['edit'])&&(is_numeric($_GET['edit']))){
$vendorInfo=$vendor->getVendorDetails($_GET['edit']);
?>
    
<h2><?php echo $inv_vendorForm_updateLabel_prompt."&nbsp;'".$vendorInfo['inventory_vendor_name']."'";?></h2>

<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="POST" enctype="multipart/form-data">
    
    <table border="0">
        
        <tr><td><?php echo $inv_vendorForm_name_label;?></td><td><input type="text" name="vendorName" <?php if(isset($_POST['vendorName'])){
            echo "value=\"".$_POST['vendorName']."\"";
            }else{
                echo "value=\"".stripslashes($vendorInfo['inventory_vendor_name'])."\"";
                }?> /></td></tr>
        <tr><td valign="top"><?php echo $inv_vendorForm_address_label;?></td><td><textarea rows="4" name="vendorAddress"><?php  if(isset($_POST['vendorAddress'])){
            echo $_POST['vendorAddress'];
            }else{
                echo stripslashes($vendorInfo['inventory_vendor_address']);
            } ?></textarea> </td></tr>
        
        <tr><td><?php echo $inv_vendorIncYr_label;?></td><td><input type="text" name="vendorIncYr" <?php if(isset($_POST['vendorIncYr'])){
            echo "value=\"".$_POST['vendorIncYr']."\"";}else{
            echo "value=\"".stripslashes($vendorInfo['inventory_vendor_yrOfInc'])."\"";
        }?> /></td></tr>
        
        <tr><td><?php echo $inv_vendorRCNO_label;?></td><td><input type="text" name="vendorRcNo" <?php if(isset($_POST['vendorRcNo'])){
            echo "value=\"".$_POST['vendorRcNo']."\"";
            }  else {
             echo "value=\"".stripslashes($vendorInfo['inventory_vendor_rcno'])."\"";
            }?> /></td></tr>
        
        <tr><td><?php echo $inv_vendorLicense_label;?></td><td><input type="text" name="vendorLicense" <?php if(isset($_POST['vendorLicense'])){
            echo "value=\"".$_POST['vendorLicense']."\"";
            }else{
                echo "value=\"".stripslashes($vendorInfo['inventory_vendor_license'])."\"";
            }?> /></td></tr>
        
        <tr><td><?php echo $inv_vendorTel_label;?></td><td><input type="text" name="vendorTel" <?php if(isset($_POST['vendorTel'])){
            echo "value=\"".$_POST['vendorTel']."\"";
            }else{
                echo "value=\"".stripslashes($vendorInfo['inventory_vendor_tel'])."\"";
            }?> /></td></tr>
        
                <tr><td><?php echo $inv_vendorMobile_label;?></td><td><input type="text" name="vendorMobile" <?php if(isset($_POST['vendorMobile'])){
                    echo "value=\"".$_POST['vendorMobile']."\"";
                    }else{
                     echo "value=\"".stripslashes($vendorInfo['inventory_vendor_mobile'])."\"";   
                    }?> /></td></tr>
     
<tr><td><?php echo $inv_vendorEmail_label;?></td><td><input type="text" name="vendorEmail" <?php if(isset($_POST['vendorEmail'])){
    echo "value=\"".$_POST['vendorEmail']."\"";
    }  else {
     echo "value=\"".stripslashes($vendorInfo['inventory_vendor_email'])."\"";
    }?> /></td></tr>                
         
<tr><td><?php echo $inv_vendorWebsite_label;?></td><td><input type="text" name="vendorWebsite" <?php if(isset($_POST['vendorWebsite'])){
    echo "value=\"".$_POST['vendorWebsite']."\"";
    }  else {
       echo  "value=\"".stripslashes($vendorInfo['inventory_vendor_website'])."\"";
    }?> /></td></tr> 

        <tr><td>&nbsp;</td><td><input type="submit" name="update" value="<?php echo $inventory_update;?>" class="button" /></td></tr>
    </table>
</form>
<?php } ?>