<link type="text/css" href="modules/pharmacy/inventory/css/inventoryStyles.css" rel="stylesheet" />

<div id="inv_incPgContent"><?php

    $inv_include_page=(isset($_GET['folder']))?$_GET['folder']:"";
    
    switch ($inv_include_page){
    
        case 'inventory_list':
            include 'incs/inventoryList.php';
            break;
            
        case 'inventory_new':
            include 'incs/inventoryAdd.php';
            break;

        default: 
            include "incs/inventoryMain.php";
            break;
        
    } ?></div>