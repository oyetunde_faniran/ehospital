<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>
<?php
  
//$scat = new servicecat();
$service = new service();
//delete row with ID
if (isset($id))
 $service->Delete_row_from_key($_REQUEST['langid'],$_REQUEST['id'],$lang_curTB);
 
 ////save new record
// if (isset($addservice) || !empty($addservice)){
// 	$i=0;
//	foreach ($_POST as $key=>$value) {	 
//		if($key=='p' or  $key=='addservice' or $key=='reset'){ //don't include these fields 
//				
//		}else{  $service->service[$i]=$key;
//		
//			$service->service2[$i]=$value;
//			$i++;
//		  }
//		 
//		 }
//   // exit();
//	$service->Save_Active_Row_as_New('service',$lang_curDB,$lang_curTB);
//	}

//edit existing record

if(isset($_POST['edit'])|| !empty($_GET['edit'])){
$i=0;
 foreach ($_POST as $key=>$value) {	if($key=='p' or  $key=='editservice' or $key=='reset' or $key=='edit' or $key=='service_id' or $key=='langid' or $key=='service_amount'){
				
		}else{  $service->service[$i]=$key;
		
			$service->service2[$i]=$value;
			$i++;
		  }
		  
		   }

    $message=$service->Save_Active_Row($_POST,$_POST['langid'],$_POST['service_id'],'service','service_amount',$lang_curDB,$lang_curTB);
	
}

?>
<?php
	$parent = isset($_GET["paret"]) ? (int)$_GET["paret"] : 1;	//Parent id
	$disItem = isset($_GET["i"]) ? (int)$_GET["i"] : 1;	//Parent id

	if (isset($_POST['sButton'])){
		$response = $service->addItem($_POST,$lang_curDB,$lang_curTB);
		if ($response){
		
		}
			//header ("Location: {$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}&c=Success");
		else $_GET["c"] = "Service / Billing Constraint Addition Failed";
	}
	
?>

<script language="javascript" type="text/javascript">
function display(id1,id2){  //show only 1st
		document.getElementById(id1).style.display = 'block';
		document.getElementById(id2).style.display = 'none';
		}
function display1(id1){  //show only 1st
		document.getElementById(id1).style.display = 'block';
		//document.getElementById(id2).style.display = 'none';
		}
</script>

<h3>
  <hr />
</h3>
<?php
	if (isset($_GET["c"]))
		echo "<p style=\"color: #990000;\">{$_GET['c']}</p>";
	$lineage = $service->getLineage($parent, $disItem,$lang_curDB,$lang_curTB);
	if (!empty($lineage))
		echo "<p>$lineage</p>";
?>
<p>
<form action="" method="post" name="addservice">
<input type="hidden"  id="p" name="p" value="service" />
<table border="0" cellspacing="5" cellpadding="5"  width="500px">
  <tr>
    <th  align="left" >
		<?php echo $lang_select_unitservice ?> :    </th>
    <td align="left">
		<select name="parent">
	<?php	
	if($disItem!=1)
        	echo "<option value=\"$disItem\">".$service->getItemName($disItem,$lang_curDB,$lang_curTB)."</option>";
			

	echo $service->getChildren4DropDown($disItem,$lang_curDB,$lang_curTB);
?>
        </select>
    </td>
  </tr>
  <tr>
    <td align="left"><input name="service_type" type="radio" value="radiobutton" onClick="display('addcon','addservice')" checked="checked">
    <?php echo $lang_radio_option1 ; ?></td>
    <td  align="left"><input name="service_type" type="radio" value="radiobutton" onClick="display1('addservice')"><?php echo $lang_radio_option2 ; ?></td>
  </tr>
  <tr id='addcon' style="display:block" >
    <td align="left" ><?php echo $lang_service_name ?>:</th> <th align="left"  width="90">&nbsp;</td>
    <td align="left" ><input name="langcont_id" type="text" size="20" maxlength="255" /></td>
  </tr>
    <tbody id='addservice' style="display:none">
    <tr >
    <td  align="left"  colspan="2"><em><?php echo $lang_instruction ?></em>
	</td></tr>
	<tr ><td  align="left"   ><?php echo $lang_service_desc ?>:</td>
    <td align="left"><input name="itemdesc" type="text" size="20" maxlength="255" /></td>
  </tr>
  <tr>
    <td align="left"><?php echo $lang_service_amount ?>:</td>
    <td align="left"><input name="itemamount" type="text" size="20" maxlength="255" /></td>
  </tr>
  </tbody>
  
  <tr><td><input name="sButton" type="submit" value="<?php echo $lang_button_serv ?>" /></td>
     <td  align="center" ></td>
  </tr>
</table>
</form>
</p>
<?php
	echo $service->getChildren($disItem,$lang_curDB,$lang_curTB);
?>
<p>
</p>


