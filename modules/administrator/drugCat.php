<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>

<?php  
$dcat = new drugcategory();
//delete row with ID
if (isset($id))
 $dcat->Delete_row_from_key($_REQUEST['langid'],$_REQUEST['id'],$lang_curTB);
 
 //save new record
 //if (isset($_POST['adddrug']) || !empty($_POST['adddrug'])){
// 	$i=0;
//	foreach ($_POST as $key=>$value) {	 
//		if($key=='p' or  $key=='adddrug' or $key=='reset' or $key=='drugcat_id' or $key=='langid'){ //don't include these fields 
//				
//		}else{  $dcat->dcat[$i]=$key;
//		
//			$dcat->dcat2[$i]=$value;
//			$i++;
//		  }
//		 
//		 }
//   // exit();
//	$dcat->Save_Active_Row_as_New('drugcategory',$lang_curDB,$lang_curTB);
//	}

//edit existing record
if(isset($_POST['edit']) || !empty($_POST['edit'])){
$i=0;
 foreach ($_POST as $key=>$value) {	if($key=='p' or  $key=='editdrug' or $key=='reset' or $key=='edit' or $key=='drugcat_id' or $key=='langid'){
				
		}else{  $dcat->dcat[$i]=$key;
		
			$dcat->dcat2[$i]=$value;
			$i++;
		  }
		  
		   }
		   
    $message=$dcat->Save_Active_Row($_POST['langid'],$_POST['drugcat_id'],'drugcategory',$lang_curDB,$lang_curTB);

}

?>


<script language="javascript" type="text/javascript">
function display(id1){  //show only 1st
		if (document.form1.openclose.value == 'open') {
		    document.getElementById(id1).style.display = 'none';
			document.form1.openclose.value = 'close';
			document.getElementById('imageopenclose').src = './images/expand.jpg';
			document.getElementById('textopenclose').innerHTML = 'Add Drug Category';
			document.getElementById('mesgopenclose').innerHTML = '';
			document.getElementById('saveopenclose').innerHTML = '';
		}else{
			document.getElementById(id1).style.display = 'block';
			document.form1.openclose.value = 'open';
			document.getElementById('imageopenclose').src = './images/collapse.jpg';
            document.getElementById('textopenclose').innerHTML = 'Close this form';
		}		
}

</script>
<script language="javascript" type="text/javascript">
<?php /*?>function validate(form){
var  msg='';
  if (document.form.ward_id.value=="") msg="You Need to select the Clinic.";
 if (document.form.clinic_id.value=="") msg="You Need to select the Clinic.";
  if (document.form.ward_name.value=="") msg="You Need to type the Ward name.";
   if (document.form.ward_bedspaces.value=="") msg="You Need to type the Bed Spaces.";
    if (document.form.siderooms.value=="") msg="You Need to type the Side Rooms.";
 if (!msg=''){
  alert(msg);
  return false;
 }
  alert(msg);

 document.frmWard.submit();
 return;
}<?php */?>
function validate(form){
  if (document.form.langcont_id.value == "") {
    alert( "Please Specify Drug Category Name" );
    document.form.langcont_id.focus();
    return false ;
  }
  
  }
function doHandleAll() {
		with (document.standardView) {
			if(elements['allCheck'].checked == false){
				doUnCheckAll();
			}
			else if(elements['allCheck'].checked == true){
				doCheckAll();
			}
		}
	}

	function doCheckAll() {
		with (document.standardView) {
			for (var i=0; i < elements.length; i++) {
				if (elements[i].type == 'checkbox') {
					elements[i].checked = true;
				}
			}
		}
	}

	function doUnCheckAll() {
		with (document.standardView) {
			for (var i=0; i < elements.length; i++) {
				if (elements[i].type == 'checkbox') {
					elements[i].checked = false;
				}
			}
		}
	}

	function clear_form() {
		document.standardView.search_value.value='';
		document.standardView.search_value.options[0].selected=true;
	}
	function returnSearch() {

		if (document.standardView.ward_field.value == -1) {
			alert("Select the field to search!");
			document.standardView.ward_field.Focus();
			return false;
		};
		<?php /*?>document.standardView.capMode.value = 'SearchMode';
		document.standardView.submit();<?php */?>
		
	}
	
	function returnDelete() {
		$check = 0;
		with (document.standardView) {
			for (var i=0; i < elements.length; i++) {
				if ((elements[i].type == 'checkbox') && (elements[i].checked == true) && (elements[i].name == 'chkLocID[]')){
					$check = 1;
				}
			}
		}

		if ($check == 1){

			var res = confirm("Deletion might affect employee information, Job Titles. Do you want to delete ?");

			if(!res) return;

			document.standardView.delState.value = 'DeleteMode';
			document.standardView.pageNO.value=1;
			document.standardView.submit();
		}else{
			alert("Select at least one record to delete");
		}
	}
	
</script>
<script type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>


<form name="form1" id="form1">
 <input type="hidden"  id="openclose" name="openclose" value="open" />


<div  ><img 
						id="imageopenclose"
						title="Expand" 
						alt="Expand" 
						src="./images/collapse.jpg" 
						style="border: none"
						onClick="display('addform');" 
						 
						 />&nbsp;&nbsp; <label   id="textopenclose" onClick="display('addform');">Close this form</label>	 
																</div></form>




		
			
<table id="addform" style="display:block" width="350"  cellpadding="2" cellspacing="2"  class="tblborder" border="1"><tr><td>
<?php
	if (isset($_GET["c"]))
		echo "<p id='saveopenclose' class=\"comments\">{$_GET['c']}</p>"
?>																
<label  class="errorMesg" id="mesgopenclose"><?php if (isset($_POST['adddrug']) || !empty($_POST['adddrug'])) echo "ERROR: ".$successORfailure ; ?></label>
<table    width="340"   bgcolor="#F9F9F9"> 
<form action="./index.php" method="post" name="form"  id="form" onsubmit="MM_validateForm('drugcat_name','','R');return document.MM_returnValue"  > 
<tr>
<td ><input type="hidden"  id="p" name="p" value="drug" /><?php echo $lang_drugcat_name ?>:</td><td ></td><td align=left  class="formvalue"><input type="text" name="langcont_id" value="" size="" maxlength="" class=""></input></td>

</tr>
<tr>
<td ></td><td ></td><td><input name="adddrug"  type="submit"  id="adddrug"value="Save"  class="btn" onClick="return  validate(this)"/></td></tr>
<tr><td colspan=3></form></table></td></tr></table>

<form name="standardView" method="post" action="./index.php?p=drug">

			
	<!--<h2><?php //echo $lang_title_key ?> :  <?php //echo $lang_title_value_drug ?></h2>-->
	<!--<img 
						title="Add" 
						alt="Add" 
						src="./images/btn_add.gif" 
						style="border: none"
						onClick="display('addform','addform1','atminter');" 
						onMouseOut="this.src='./images/btn_add.gif" ;" 
						onMouseOver="this.src='./images/btn_add_02.gif';" />--><?php /*?><img 
							title="Delete" 
							alt="Delete" 
							style="border: none;" 
							src="./images/btn_delete.gif"
							onclick="returnDelete();" 
							onmouseout="this.src='./images/btn_delete.gif';" 
							onmouseover="this.src='./images/btn_delete_02.gif';" />	<?php */?>											
		<br />
	<hr />
		
					
	
			        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="addform1" style="display:block">
			<tr class="title-row">
									<td width="50" ><strong>SNo.</strong></td>
									<td  width="" ><strong><?php echo $lang_drugcat_name ?></strong></td>
									<td   colspan="2"><strong><?php //echo $lang_action ?></strong></td>
    										
			</tr>
			
			<?php
			
			  			
			if(empty($search_field)||empty($search_value)){
			//list all
			
			$dcat->allrows($lang_curDB,$lang_curTB);
			}else
			//return search
			$dcat->rowSearch($search_field,$search_value,$lang_curDB,$lang_curTB) ;
			?>
  </table>

</form>

</body>
</html>
