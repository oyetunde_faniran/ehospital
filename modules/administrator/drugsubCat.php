<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>

<?php  
$dcat = new drugcategory();
$dsubcat = new drug_subcat();
//delete row with ID
if (isset($id))
 $dsubcat->Delete_row_from_key($_REQUEST['langid'],$_REQUEST['id'],$lang_curTB);
 
 //save new record
 if (isset($_POST['adddrugsub']) || !empty($_POST['adddrugsub'])){
 	$i=0;
	foreach ($_POST as $key=>$value) {	 
		if($key=='p' or  $key=='adddrugsub' or $key=='reset' or $key=='drugsub_id' or $key=='langid'){ //don't include these fields 
				
		}else{  $dsubcat->dsubcat[$i]=$key;
		
			$dsubcat->dsubcat2[$i]=$value;
			$i++;
		  }
		 
		 }
   // exit();
	$dsubcat->Save_Active_Row_as_New('drug_subcat',$lang_curDB,$lang_curTB);
	}

//edit existing record
if(isset($_POST['edit']) || !empty($_POST['edit'])){
$i=0;
 foreach ($_POST as $key=>$value) {	if($key=='p' or  $key=='editsubdrug' or $key=='reset' or $key=='edit' or $key=='drugsub_id' or $key=='langid'){
				
		}else{  $dsubcat->dsubcat[$i]=$key;
		
			$dsubcat->dsubcat2[$i]=$value;
			$i++;
		  }
		  
		   }
		   
    $message=$dsubcat->Save_Active_Row($_POST['langid'],$_POST['drugsub_id'],'drug_subcat',$lang_curDB,$lang_curTB);

}

?>


<script language="javascript" type="text/javascript">
function validate(form){
		  if (document.form.langcont_id.value == "") {
			alert( "Please enter Drug Sub Category" );
			document.form.langcont_id.focus();
			return false ;
		  }
    }
function display(id1){  //show only 1st
		if (document.form1.openclose.value == 'open') {
		    document.getElementById(id1).style.display = 'none';
			document.form1.openclose.value = 'close';
			document.getElementById('imageopenclose').src = './images/expand.jpg';
			document.getElementById('textopenclose').innerHTML = 'Add Drug Sub Category';
			document.getElementById('mesgopenclose').innerHTML = '';
			document.getElementById('saveopenclose').innerHTML = '';
		}else{
			document.getElementById(id1).style.display = 'block';
			document.form1.openclose.value = 'open';
			document.getElementById('imageopenclose').src = './images/collapse.jpg';
            document.getElementById('textopenclose').innerHTML = 'Close this form';
		}		
}

</script><script language="javascript" type="text/javascript">
<?php /*?>function validate(form){
var  msg='';
  if (document.form.ward_id.value=="") msg="You Need to select the Clinic.";
 if (document.form.clinic_id.value=="") msg="You Need to select the Clinic.";
  if (document.form.ward_name.value=="") msg="You Need to type the Ward name.";
   if (document.form.ward_bedspaces.value=="") msg="You Need to type the Bed Spaces.";
    if (document.form.siderooms.value=="") msg="You Need to type the Side Rooms.";
 if (!msg=''){
  alert(msg);
  return false;
 }
  alert(msg);

 document.frmWard.submit();
 return;
}<?php */?>

	
</script>

<form name="form1" id="form1">
 <input type="hidden"  id="openclose" name="openclose" value="open" />


<div  ><img 
						id="imageopenclose"
						title="Expand" 
						alt="Expand" 
						src="./images/collapse.jpg" 
						style="border: none"
						onClick="display('addform');" 
						 
						 />&nbsp;&nbsp; <label   id="textopenclose" onClick="display('addform');">Close this form</label>	 
																</div></form>


<table id="addform" style="display:block" width="420"  cellpadding="2" cellspacing="2"  class="tblborder" border="1"><tr><td>
<?php
	if (isset($_GET["c"]))
		echo "<p id='saveopenclose' class=\"comments\">{$_GET['c']}</p>"
?>																
<label  class="errorMesg" id="mesgopenclose"><?php if (isset($_POST['adddrugsub']) || !empty($_POST['adddrugsub'])) echo "ERROR: ".$successORfailure ; ?></label>
<table    width="400"   bgcolor="#F9F9F9"> 
 
<form action="./index.php?p=drugsub" method="post" name="form"  id="form" > 

<tr>
	<td align=right  class=""><input type="hidden"  id="p" name="p" value="drugsub" /><?php echo $lang_drugsubcat_name ?>:</td><td ></td><td align=left  class="formvalue"><input type="text" name="langcont_id" value="" size="" maxlength="" class=""></input></td>

</tr>
<tr>
	<td align=right  class=""><?php echo $lang_drugcat_name ?>:</td><td ></td><td align=left  class="formvalue"><select  name="drugcat_id"  size="" maxlength="" class="forminput"><?php $dcat->getdrugcat($lang_curDB,$lang_curTB) ?></select></td>

</tr>
<tr>
	<td  align=right></td><td ></td><td ></td>
</tr>
<tr>
	<td align=right></td><td ></td><td><input name="adddrugsub"  type="submit" class="btn"  id="adddrugsub"value="Save"  onClick="return  validate(this)"/></td>
</tr>

<tr><td colspan=3></form></table></td></tr></table>

<form name="standardView" method="post" action="./index.php?p=drug">
		
					
		
	
			<br>
			<hr>
	
	
		<table  border="0" cellpadding="0" cellspacing="0" id="addform1" >
			<tr class="title-row">
									<td width="" ><strong>SNo.</strong></td>
									<td  width="" ><?php echo $lang_drugcat_name	 ?></td>
									<td  width="" ><?php echo $lang_drugsubcat_name ?></td>
									<td  ><?php //echo $lang_action ?></td>
    										
			</tr>
			
			<?php
			
			  			
			if(empty($search_field)||empty($search_value)){
			//list all
			
			$dsubcat->allrows($lang_curDB,$lang_curTB);
			}else
			//return search
			$dsubcat->rowSearch($search_field,$search_value,$lang_curDB,$lang_curTB) ;
			?>
  </table>

</form>

</body>
</html>
