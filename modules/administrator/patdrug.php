<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>
<?php  
$patdrug = new drug();
$dsubcat = new drug_subcat();
$dtype= new drug_type();
//delete row with ID
if (isset($id))
 $patdrug->Delete_row_from_key($_REQUEST['id']);
 
 //save new record
 //if (isset($_POST['addpdrug']) || !empty($_POST['addpdrug'])){
// 	$i=0;
//	foreach ($_POST as $key=>$value) {	 
//		if($key=='p' or  $key=='addpdrug' or $key=='reset'){ //don't include these fields 
//				
//		}else{  $patdrug->drugs[$i]=$key;
//		
//			$patdrug->drugs2[$i]=$value;
//			$i++;
//		  }
//		 
//		 }
//   // exit();
//	
//	$patdrug->Save_Active_Row_as_New();
//	}

//edit existing record
if(isset($_POST['edit'])|| !empty($_POST['edit'])){
$i=0;
 foreach ($_POST as $key=>$value) {	if($key=='p' or  $key=='editpdrug' or $key=='reset' or $key=='edit'){
				
		}else{  $patdrug->drugs[$i]=$key;
		
			$patdrug->drugs2[$i]=$value;
			$i++;
		  }
		  
		   }
		   
    $message=$patdrug->Save_Active_Row($_POST['drug_id'],'drug');

}

?>


<script language="javascript" type="text/javascript">
function display(id1){  //show only 1st
		if (document.form1.openclose.value == 'open') {
		    document.getElementById(id1).style.display = 'none';
			document.form1.openclose.value = 'close';
			document.getElementById('imageopenclose').src = './images/expand.jpg';
			document.getElementById('textopenclose').innerHTML = 'Add Drug';
			document.getElementById('mesgopenclose').innerHTML = '';
			document.getElementById('saveopenclose').innerHTML = '';
		}else{
			document.getElementById(id1).style.display = 'block';
			document.form1.openclose.value = 'open';
			document.getElementById('imageopenclose').src = './images/collapse.jpg';
            document.getElementById('textopenclose').innerHTML = 'Close this form';
		}		
}

</script>
<script language="javascript" type="text/javascript">
<?php /*?>function validate(form){
var  msg='';
  if (document.form.ward_id.value=="") msg="You Need to select the Clinic.";
 if (document.form.clinic_id.value=="") msg="You Need to select the Clinic.";
  if (document.form.ward_name.value=="") msg="You Need to type the Ward name.";
   if (document.form.ward_bedspaces.value=="") msg="You Need to type the Bed Spaces.";
    if (document.form.siderooms.value=="") msg="You Need to type the Side Rooms.";
 if (!msg=''){
  alert(msg);
  return false;
 }
  alert(msg);

 document.frmWard.submit();
 return;
}<?php */?>
function validate(patdrug){
  if (document.patdrug.drug_item_no.value == "") {
    alert( "Please Specify Drug Item No" );
    document.patdrug.drug_item_no.focus();
    return false ;
  }
   if (document.patdrug.drugsub_id.value == "") {
    alert( "Please select subcategory name" );
    document.patdrug.drugsub_id.focus();
    return false ;
  }
  }
	
</script>

<script type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>

<form name="form1" id="form1">
	 <input type="hidden"  id="openclose" name="openclose" value="close" />


			<div  ><img 
						id="imageopenclose"
						title="Expand" 
						alt="Expand" 
						src="./images/expand.jpg" 
						style="border: none"
						onClick="display('addform');" 
						 
						 />&nbsp;&nbsp; <label   id="textopenclose" onClick="display('addform');">Add Drug</label>	 
																</div>
</form>

<table id="addform" style="display:none" width="600"  cellpadding="2" cellspacing="2"  class="tblborder" border="1"><tr><td>
<?php
	if (isset($_GET["c"]))
		echo "<p id='saveopenclose' class=\"comments\">{$_GET['c']}</p>"
?>																
<label  class="errorMesg" id="mesgopenclose"><?php if (isset($_POST['addpdrug']) || !empty($_POST['addpdrug'])) echo "ERROR: ".$successORfailure ; ?></label>
<table       bgcolor="#F9F9F9"> 
<form action="./index.php" method="POST" ecntype="text" name="patdrug"  enctype="multipart/form-data">
 <input type="hidden"  id="p" name="p" value="pdrug" />
<tr>
<td align=right  class="formdetails"><?php echo $lang_drugItemno ?>:</td><td width="10"></td><td align=left width="200" class="formvalue"><input type="text" name="drug_item_no" value="" size="" maxlength="" class="forminput"></input></td>
</tr>
<tr>
<td align=right class="formdetails"><?php echo $lang_drugcat ?>:</td><td width="10"></td><td align=left width="200" class="formvalue"><select  name="drugsub_id"  size="" maxlength="" class="forminput"><?php $dsubcat->getsubdrugcat($lang_curDB,$lang_curTB) ?></select></td>

</tr>
<tr>
<td align=right ><?php echo $lang_drug_desc ?>:</td><td width="10"></td><td ><textarea  name="drug_desc"  size="" maxlength="" class=""></textarea></td>
</tr>
<tr>
<td align=right><?php echo $lang_drug_expirydate ?>:</td><td width="10"></td><td align=left width="200" class="formvalue"><input type="text" name="drug_expirydate" onclick="show_calendar('document.patdrug.drug_expirydate',document.patdrug.drug_expirydate.value)" value=""/> </td>
</tr>
<tr>
<td align=right><?php echo $lang_drug_reorderlevel ?>:</td><td width="10"></td><td align=left width="200" class="formvalue"><input type="text" name="drug_reorderlevel" value="" size="" maxlength="" class=""></input></td>
</tr>
<tr>
<td align=right><?php echo $lang_drug_manufacturer ?>:</td><td width="10"></td><td align=left width="200" class="formvalue"><textarea  name="drug_manufacturer"  size="" maxlength="" class=""></textarea></td>
</tr>
<tr>
<td align=right><?php echo $lang_drugtype_drop ?>:</td><td ></td><td align=left  class="formvalue"><select  name="drugtype_id"  size="" maxlength="" class="forminput"><?php $dtype->getdrug_type($lang_curDB,$lang_curTB) ?></select></td>

</tr>
<tr>

<td align=right><?php echo $lang_drug_unitpack ?>:</td><td width="10"></td><td align=left width="200" class="formvalue"><input type="text" name="drug_unitpack" value="" size="" maxlength="" class=""></input></td>
</tr>
<tr>
<td align=right><?php echo $lang_drug_price ?></td><td width="10"></td><td align=left width="200" class="formvalue"><input type="text" name="drug_price" value="" size="" maxlength="" class=""></input></td>
</tr>
<tr>
<td align=right><?php echo $lang_drug_qtyinstock ?></td><td width="10"></td><td align=left width="200" class="formvalue"><input type="text" name="drug_qtyinstock" value="" size="" maxlength="" class=""></input></td>
</tr>
<tr>
<td ></td><td ></td><td ></td>
</tr>
<tr>
<td ></td><td ></td><td><input name="addpdrug"  type="submit"  id="addpdrug"value="Save" class="btn"  onClick="return  validate(this)"/></td></tr>

<tr><td colspan=3></form></table></td></tr></table>
			
			<hr />
			<table  border="0" cellpadding="0" cellspacing="0" id="addform1" style="display:block">
			<tr class="title-row">
				<!--<td   >
									<input type="checkbox" class="checkbox" name="allCheck" value="" onClick="doHandleAll();" />								<?php //echo $lang_sno; ?>
                 </td>-->
									<td  ><strong>SNo.</strong></td>
									<td  ><strong><?php echo $lang_drug_desc	 ?>	</strong></td>
									
								<td  ><strong><?php echo $lang_drug_expirydate  ?></strong></td>
    								<td  ><strong><?php echo $lang_drug_reorderlevel  ?></strong></td>
									<td  ><strong><?php echo $lang_drug_manufacturer  ?></strong></td>
									<td  >&nbsp;</td>		
			</tr>
			
			<?php
			
			  			
			if(empty($search_field)||empty($search_value)){
			//list all
			
			$patdrug->allrows();
			}else
			//return search
			$patdrug->rowSearch($search_field,$search_value) ;
			?>
			
</table>


</body>
</html>

