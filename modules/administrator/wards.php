<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	//require_once ('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>

<?php  
// constructor
$ward = new wards();
$clinic = new clinic();
$getlang= new language();

//delete row with ID
if (isset($id))
 $ward->Delete_row_from_key($_REQUEST['langid'],$_REQUEST['id'],$lang_curTB);

	//echo $ward_bedspaces;
	//print_r($_POST); 
	//exit();
//save new record
//if (isset($_POST['addward']) || !empty($_POST['addward'])){
//
// 	$i=0;
//	
//	foreach ($_POST as $key=>$value) {	 
//		if($key=='p' or  $key=='addward' or $key=='reset' or $key=='ward_id' or $key=='langid'){ //don't include these fields 
//				
//		}else{ 
//		 $ward->ward[$i]=$key;
//		 $ward->ward2[$i]=$value;
//		
//			$i++;
//		  }
//		 
//		 }
//		//echo "entered";
// // exit();
//	$ward->Save_Active_Row_as_New('wards',$lang_curDB,$lang_curTB);
//	}
  
 //edit existing record
if(isset($_POST['edit'])|| !empty($_POST['edit'])){
$i=0;
 foreach ($_POST as $key=>$value) {	if($key=='p' or  $key=='editbtn' or  $key=='editward' or $key=='reset' or $key=='edit' or $key=='ward_id' or $key=='langid' or $key=='oldTotal'){
				
		}else{  $ward->ward[$i]=$key;
		
			$ward->ward2[$i]=$value; 
			$i++;
		  }
		  
		   }
	$message=$ward->Save_Active_Row($_POST['langid'],$_POST['ward_id'],'wards',$lang_curDB,$lang_curTB);

}

?>



<script language="javascript" type="text/javascript">
function display(id1){  //show only 1st
		if (document.form1.openclose.value == 'open') {
		    document.getElementById(id1).style.display = 'none';
			document.form1.openclose.value = 'close';
			document.getElementById('imageopenclose').src = './images/expand.jpg';
			document.getElementById('textopenclose').innerHTML = 'Add Drug Priority';
			document.getElementById('mesgopenclose').innerHTML = '';
			document.getElementById('saveopenclose').innerHTML = '';
		}else{
			document.getElementById(id1).style.display = 'block';
			document.form1.openclose.value = 'open';
			document.getElementById('imageopenclose').src = './images/collapse.jpg';
            document.getElementById('textopenclose').innerHTML = 'Close this form';
		}		
}

</script><script language="javascript" type="text/javascript">
<?php /*?>function validate(form){
var  msg='';
  if (document.form.ward_id.value=="") msg="You Need to select the Clinic.";
 if (document.form.clinic_id.value=="") msg="You Need to select the Clinic.";
  if (document.form.ward_name.value=="") msg="You Need to type the Ward name.";
   if (document.form.ward_bedspaces.value=="") msg="You Need to type the Bed Spaces.";
    if (document.form.siderooms.value=="") msg="You Need to type the Side Rooms.";
 if (!msg=''){
  alert(msg);
  return false;
 }
  alert(msg);

 document.frmWard.submit();
 return;
}<?php */?>
function validate(form){
  if (document.form.clinic_id.value == "") {
    alert( "Please select Clinic " );
    document.form.clinic_id.focus();
    return false ;
  }
   if (document.form.ward_name.value == "") {
    alert( "Please enter ward description" );
    document.form.ward_name.focus();
    return false ;
  }
  }
function doHandleAll() {
		with (document.standardView) {
			if(elements['allCheck'].checked == false){
				doUnCheckAll();
			}
			else if(elements['allCheck'].checked == true){
				doCheckAll();
			}
		}
	}

	function doCheckAll() {
		with (document.standardView) {
			for (var i=0; i < elements.length; i++) {
				if (elements[i].type == 'checkbox') {
					elements[i].checked = true;
				}
			}
		}
	}

	function doUnCheckAll() {
		with (document.standardView) {
			for (var i=0; i < elements.length; i++) {
				if (elements[i].type == 'checkbox') {
					elements[i].checked = false;
				}
			}
		}
	}

	function clear_form() {
		document.standardView.ward_name.value='';
		document.standardView.loc_code.options[0].selected=true;
	}
	function returnSearch() {

		if (document.standardView.ward_field.value == -1) {
			alert("Select the field to search!");
			document.standardView.ward_field.Focus();
			return false;
		};
		<?php /*?>document.standardView.capMode.value = 'SearchMode';
		document.standardView.submit();<?php */?>
		
	}
	
	function returnDelete() {
		$check = 0;
		with (document.standardView) {
			for (var i=0; i < elements.length; i++) {
				if ((elements[i].type == 'checkbox') && (elements[i].checked == true) && (elements[i].name == 'chkLocID[]')){
					$check = 1;
				}
			}
		}

		if ($check == 1){

			var res = confirm("Deletion might affect employee information, Job Titles. Do you want to delete ?");

			if(!res) return;

			document.standardView.delState.value = 'DeleteMode';
			document.standardView.pageNO.value=1;
			document.standardView.submit();
		}else{
			alert("Select at least one record to delete");
		}
	}
	
</script>
<script type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>




<!--<input type="hidden"  id="addWard" name="capMode" value="addWard" />-->
		

  <?php // echo $lang_drugtype_title ?>


<form name="form1" id="form1">
 <input type="hidden"  id="openclose" name="openclose" value="open" />


<div  ><img 
						id="imageopenclose"
						title="Expand" 
						alt="Expand" 
						src="./images/collapse.jpg" 
						style="border: none"
						onClick="display('addform');" 
						 
						 />&nbsp;&nbsp; <label   id="textopenclose" onClick="display('addform');">Close this form</label>	 
																</div></form>
<table id="addform" style="display:block" width="350"  cellpadding="2" cellspacing="2"  class="tblborder" border="1"><tr><td>
<?php
	if (isset($_GET["c"]))
		echo "<p id='saveopenclose' class=\"comments\">{$_GET['c']}</p>"
?>																
<label  class="errorMesg" id="mesgopenclose"><?php if (isset($_POST['addward']) || !empty($_POST['addward'])) echo "Error:<br> ".$successORfailure ; ?></label>
<table    width="340"   bgcolor="#F9F9F9"> 

 <form action="./index.php" method="post" name="form"  id="form" onsubmit="MM_validateForm('clinic_id','','R','ward_name','','R','ward_bedspaces','','RisNum','ward_siderooms','','RisNum');return document.MM_returnValue"  > 
 <input type="hidden"  id="p" name="p" value="ward" />


<tr>
<td colspan="3"  ><?php // echo $lang_form_title ?> </td>
</tr>
<!--<tr>
<td align=right width="200" ><?php //echo $lang_ward_id ?> :</td><td width="10"></td><td align=left width="200" ><input type="text" name="ward_id" value="" size=""  readonly /></td>
</tr>-->
<tr>
<td align=right width="200" ><?php echo $lang_clinic_name ?></td><td width="10"></td><td align=left width="200" ><select name="clinic_id"><?php $clinic->getclinic($lang_curDB,$lang_curTB)?></select></td>
</tr>
<tr>
<td align=right width="200" ><?php echo $lang_ward_name ?></td><td width="10"></td><td align=left width="200" ><input type="text" name="langcont_id" value="" size=""  /></td>
</tr>
<tr>
<td align=right width="200" ><?php echo $lang_ward_bedspaces ?>:</td><td width="10"></td><td align=left width="200" ><input type="text" name="ward_bedspaces" value="" size="3"  /></td>
</tr>
<tr>
<td align=right width="200" ><?php echo $lang_ward_siderooms ?>:</td><td width="10"></td><td align=left width="200" ><input type="text" name="ward_siderooms" value="" size="3"   /></td>
</tr>
<tr>
<td width="200" align=right></td><td width="10"></td><td width="200"></td>
</tr>
<tr>

<td width="200" align=right></td><td width="10"></td><td width="200"><input name="addward"  type="submit" value="Save"  onClick="return  validate(this)" class="btn"/></td>
</tr>
<tr><td colspan=3></td></tr></form></table></td></tr></table>


<form name="standardView" method="post" action="./index.php?p=ward">

		
		<div >
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="22%" style="white-space: nowrap;"><h3><?php //echo $lang_search_button_label ?></h3></td>
				<td  align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			</table>
		</div>
	
			<!--<table  border="0" cellpadding="2" cellspacing="0" class="">
				<tr>
					<td >
						<label for="loc_code" style="float: left; padding-right: 10px;"><?php echo $lang_search_by ?>:</label><br>
						<select  name="ward_field">
							<?php echo $ward->GetWardFields()	 ?>				</select>					</td>
					<td  class="dataLabel" style="white-space: nowrap;">
						<label for="ward_value" style="float: left; padding-right: 10px;"><?php //echo $lang_search_for ?></label><br>
						<input type=text size="20" name="ward_value"  value="" />					</td>
					<td align="right" >
<?php /*?>						<img 
							title="Search" 
							alt="Search" 
							src="./images/btn_search.gif" 
							onclick="returnSearch();" 
							onmouseout="this.src='./images/btn_search.gif';" 
							onmouseover="this.src='./images/btn_search_02.gif';" /><?php */?>&nbsp;&nbsp;
						<img 
							title="Clear" 
							alt="Clear"
							src="./images/btn_clear.gif"
							onclick="clear_form();" 
							onmouseout="this.src='./images/btn_clear.gif';" 
							onmouseover="this.src='./images/btn_clear_02.gif';" /><input name="search" type="submit" value="<?php echo $lang_search_button_label ?>" onclick="returnSearch();"/>					</td>
				</tr>
			</table>-->
	
	
	<hr />
		<table width="100%" border="0" cellpadding="5" cellspacing="2" id="addform1" style="display:block">
			<tr class="title-row">
				<td width="50"  style="white-space: nowrap;">
									<strong>SNo.</strong></td>
									<td  ><strong><?php echo $lang_ward_name	 ?></strong></td>
									<td  ><strong><?php echo $lang_clinic_name	 ?></strong></td>
									<td  ><strong><?php echo $lang_ward_bedspaces ?></strong></td>
								<td  ><strong><?php echo $lang_ward_siderooms  ?></strong></td>
    		</tr>
			<?php
				  			
			if(empty($search_field)||empty($search_value)){
			//list all
			
			$ward->allrows($lang_curDB,$lang_curTB);
			}else
			//return search
			$ward->rowSearch($search_field,$search_value,$lang_curDB,$lang_curTB) ;
			?>
			
		   
 		  </table>

</form>


<SCRIPT type="text/javascript" src="./includes/js/jquery-1.3.2.min.js"></SCRIPT>
<script type="text/javascript">
	$(function() {
		$("#wards").click(function(){
		 var da = $('#score').val();
		 var da2 = $('#score').val();
			$("#editwards").load('editWard.php',{dateval:da});  
									  
									  }
									  );
	});

	</script>