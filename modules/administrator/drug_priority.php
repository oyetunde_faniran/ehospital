<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	//require_once ('./includes/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
?>

<?php  
// constructor
$drugtype = new drug_type();
$getlang= new language();

//delete row with ID
if (isset($id))
 $drugtype->Delete_row_from_key($_REQUEST['langid'],$_REQUEST['id'],$lang_curTB);

	//echo $drugtype_bedspaces;
	//print_r($_POST); 
	//exit();
//save new record
//if (isset($_POST['adddrugtype']) || !empty($_POST['adddrugtype'])){
//
// 	$i=0;
//	//echo $i;
//	//print_r($_POST); 
//	foreach ($_POST as $key=>$value) {	 
//		if($key=='p' or  $key=='adddrugtype' or $key=='reset' or $key=='drugtype_id' or $key=='langid'){ //don't include these fields 
//				
//		}else{ 
//		 $drugtype->dtype[$i]=$key;
//		 $drugtype->dtype2[$i]=$value;
//		
//			$i++;
//		  }
//		 
//		 }
//		//echo "entered";
// // exit();
//	$xlp_error=$drugtype->Save_Active_Row_as_New('drug_type',$lang_curDB,$lang_curTB);
//			if (!empty($xlp_error[2])){
//					//echo "Enter";
//					$successORfailure=NULL;
//					$successORfailure=$xlp_error[1];	
//					//echo $successORfailure;
//				}
//	
//	}
  
 //edit existing record
if(isset($_POST['edit'])|| !empty($_POST['edit'])){
$i=0;
 foreach ($_POST as $key=>$value) {	if($key=='p' or  $key=='editbtn' or  $key=='editdrugtype' or $key=='reset' or $key=='edit' or $key=='drugtype_id' or $key=='langid'){
				
		}else{  $drugtype->dtype[$i]=$key;
		
			$drugtype->dtype2[$i]=$value; 
			$i++;
		  }
		  
		   }
		   
    $message=$drugtype->Save_Active_Row($_POST['langid'],$_POST['drugtype_id'],'drug_type',$lang_curDB,$lang_curTB);

}
if(isset($_POST['editdrugorder'])){
	

	foreach ($_POST as $key=>$value){
	if($key=='editdrugorder'){
				
		}else{
		//echo "<p>'$key' : '$value'</p>";
		$pKey = ltrim($key, "hidden");
		
	//	echo $drugtype->reOrder($pKey,$value);
	 $drugtype->reOrder($pKey,$value);
		}
	}
}
?>
<script type="text/JavaScript">
function validate(form){
		  if (document.form.langcont_id.value == "") {
			alert( "Please enter Drug form" );
			document.form.langcont_id.focus();
			return false ;
		  }
    }

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>


<script language="javascript" type="text/javascript">
function display(id1){  //show only 1st
		if (document.form1.openclose.value == 'open') {
		    document.getElementById(id1).style.display = 'none';
			document.form1.openclose.value = 'close';
			document.getElementById('imageopenclose').src = './images/expand.jpg';
			document.getElementById('textopenclose').innerHTML = 'Add Drug Priority';
			document.getElementById('mesgopenclose').innerHTML = '';
			document.getElementById('saveopenclose').innerHTML = '';
		}else{
			document.getElementById(id1).style.display = 'block';
			document.form1.openclose.value = 'open';
			document.getElementById('imageopenclose').src = './images/collapse.jpg';
            document.getElementById('textopenclose').innerHTML = 'Close this form';
		}		
}

</script>


<!--<input type="hidden"  id="addWard" name="capMode" value="addWard" />-->
		

  <?php // echo $lang_drugtype_title ?>


<form name="form1" id="form1">
 <input type="hidden"  id="openclose" name="openclose" value="open" />


<div  ><img 
						id="imageopenclose"
						title="Expand" 
						alt="Expand" 
						src="./images/collapse.jpg" 
						style="border: none"
						onClick="display('addform');" 
						 
						 />&nbsp;&nbsp; <label   id="textopenclose" onClick="display('addform');">Close this form</label>	 
																</div></form>
<table id="addform" style="display:block" width="350"  cellpadding="2" cellspacing="2"  class="tblborder" border="1"><tr><td>
<?php
	if (isset($_GET["c"]))
		echo "<p id='saveopenclose' class=\"comments\">{$_GET['c']}</p>"
?>																
<label  class="errorMesg" id="mesgopenclose"><?php if (isset($_POST['adddrugtype']) || !empty($_POST['adddrugtype'])) echo "ERROR: ".$successORfailure ; ?></label>
<table    width="340"   bgcolor="#CCCCCC"> 

 <form action="./index.php" method="post" name="form"  id="form" onsubmit="MM_validateForm('langcont_id','','R');return document.MM_returnValue" enctype="multipart/form-data"> 
 <input type="hidden"  id="p" name="p" value="drugtype" />

<tr>
<td   ><?php echo $lang_drugtype_name ?></td><td width="10"></td><td align=left width="200" ><input type="text" name="langcont_id" value="" size=""  /></td>
</tr>
<tr>

<td  ></td><td width="10"></td><td width="200"><input name="adddrugtype"  type="submit" value="Save"  class="btn" onClick="return  validate(this)"/></td>
</tr>
<tr><td colspan=3></td></tr></form></table></td></tr></table>


<form name="standardView" method="post" action="./index.php?p=drugtype">

			<BR />
			<div class="innerPageborder"><h2><?php // echo $lang_title_key ?>  <?php //echo $lang_title_value_drugtype ?></h2></div>
		
			
		<a href="./index.php?p=drugorder" title="Re-order Drug Usage" target="_self">Prioritise Drug Usage</a>
		<div style="width: 98%;">
					
		</div>
			<hR />
	
	
			<table width="100%" border="0" cellpadding="5" cellspacing="2" id="addform1" style="display:block">
			<tr class="title-row">
				<td ><strong>SNo.</strong></td>
				<td  ><strong><?php echo $lang_drugtype_name	 ?></strong></td>
									
    		</tr>
			<?php
			
			  			
			if(empty($search_field)||empty($search_value)){
			//list all
			
			$drugtype->allrows($lang_curDB,$lang_curTB);
			}else
			//return search
			$drugtype->rowSearch($search_field,$search_value,$lang_curDB,$lang_curTB) ;
			?>
  </table>

</form>

</body>
</html>

