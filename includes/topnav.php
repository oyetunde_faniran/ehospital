<div id="nav-bar" class="width">
<div id="nav-box">
<div id="menu-bg">
<ul class="dropdown dropdown-horizontal" id="nav" name="nav">
      <li id="n-home"><a href="index.php">Home</a></li>
      <li id="n-music"><a href="index.php?p=sysadmin&m=sysadmin" class="dir">System Administration</a>
          <ul>
<?php
	$tocArray = array (
					"menumanagement" 		=> $admin_menumanagement,
					"groupmanagement" 		=> $admin_groupmanagement,
					"rightmanagement"		=> $admin_rightmanagement,
					"usermanagement"		=> $admin_usermanagement,
					"audittrail"			=> $admin_audittrail,
				);
    $links = "";
	foreach ($tocArray as $key=>$value){
		$links .= "<li><a href=\"index.php?p=$key&m=sysadmin\">$value</a></li>";
	}
	echo $links;
?>
          </ul>
      </li>
      <li id="n-music"><span class="dir">Modules</span>
          <ul>
            <li class="first"><a href="index.php?p=sysadmin&m=sysadmin" class="dir">System Administration</a>
                <ul>
<?php
	echo $links;
?>
                </ul>
            </li>
            <li class="first"><a href="#" class="dir">Patient Care</a>
                <ul>
                  <li><a href="#">Register Patient</a></li>
                  <li><a href="#">View Patient</a></li>
                </ul>
            </li>
            <li><a href="index.php?p=reports&m=reports" class="dir">Reports</a>
                <ul>
                  <li><a href="index.php?p=eveningreport&m=reports"><?php echo $report_evening_report; ?></a></li>
                  <li><a href="index.php?p=morningreport&m=reports"><?php echo $report_morning_report; ?></a></li>
                  <li><a href="index.php?p=admissionbook&m=reports"><?php echo $report_adm_book; ?></a></li>
                  <li><a href="index.php?p=bedstateward&m=reports"><?php echo $report_bedstate; ?></a></li>
                  <li><a href="index.php?p=bedstatewards&m=reports"><?php echo $report_bedstatewards; ?></a></li>
                  <li><a href="index.php?p=dailybed&m=reports"><?php echo $report_dailybed_statements; ?></a></li>
                  <li><a href="index.php?p=appointment&m=reports"><?php echo $report_appointment_listing; ?></a></li>
                  <li><a href="index.php?p=accident&m=reports"><?php echo $report_accident; ?></a></li>
                  <li><a href="index.php?p=outpatmonthlyatt&m=reports"><?php echo $report_monthly_attendances_outpatient; ?></a></li>
                  <li><a href="index.php?p=monthlystats&m=reports"><?php echo $report_monthlystats; ?></a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Billing</a>
                <ul>
                  <li><a href="#">Billing 1</a></li>
                </ul>
            </li>
             <li><a href="#" class="dir">Pharmacy</a>
                <ul>
                  <li><a href="#">Pharmacy 1</a></li>
                </ul>
            </li>
  			<li><a href="#" class="dir">Laboratory</a>
                <ul>
                  <li><a href="#">Laboratory 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Account/Audit/Finance</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Human Resources</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Tender</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Message Centre</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Fixed Asset Manager</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
            <li><a href="#" class="dir">Document Manager</a>
                <ul>
                  <li><a href="#">Link 1</a></li>
              </ul>
            </li>
        </ul>
      </li>
      <li id="n-music"><a href="#" class="dir">Support</a>
          <ul>
            <li><a href="#">Link 1</a></li>
          </ul>
      </li>
      <li id="n-news"><a href="#" class="dir">Alerts</a>
          <ul>
            <li><a href="#">News 1</a></li>
          </ul>
      </li>
      <li id="n-news"><a href="index.php?p=logout&m=sysadmin" class="dir">Log Out</a>
      </li>

          </ul>
          </div>
</div>
</div>