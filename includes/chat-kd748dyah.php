<?php
    //Output chat stuffs to the browser iff the user is logged-in & not idle
    if ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"]){
?>

        <div id="chat-online-dialog" title="Online Users" style="display:none;">
<?php
    $u = new admin_user();
    $onlineUsers = $u->getOnlineUsers4Chat($_SESSION[session_id() . "userID"]);
    //echo "<pre>" . print_r ($onlineUsers, true) . "</pre>";
    $display = "";
    foreach ($onlineUsers as $disUser){
        $disName = $disUser["staff_title"] . " " . $disUser["staff_surname"] . " " . $disUser["staff_othernames"];
        $chatName = admin_Tools::cleanUp4Chat($disName);
        if ($disUser["usersession_idle"] == 1){
            $status = "idle";
            $title = "$disName is idle";
        } else {
            $status = "online";
            $title = "$disName is online";
        }
        $display .= "<div>
                        <a href=\"javascript:void(0)\" onclick=\"javascript:beginChat('$chatName')\" title=\"$title\" class=\"chat-online-links-{$status} chat-{$status}-user\">$disName</a>
                     </div>";
    }
    echo !empty($display) ? $display : "Sorry! There are no other users on-line to chat with.";
?>
        </div>

        <link type="text/css" href="library/admin_jquery/themes/base/ui.dialog.css" rel="stylesheet" />
        <link type="text/css" href="library/admin_jquery/themes/base/ui.theme.css" rel="stylesheet" />
        <link type="text/css" href="library/admin_jquery/themes/smoothness/ui.core.css" rel="stylesheet" />
        <link type="text/css" href="library/admin_jquery/themes/smoothness/ui.base.css" rel="stylesheet" />
        <link type="text/css" rel="stylesheet" media="all" href="css/chat.css" />

        <script type="text/javascript" src="library/admin_jquery/ui/ui.core.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/ui.draggable.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/ui.resizable.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/ui.dialog.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/effects.core.js"></script>
        <script type="text/javascript" src="library/admin_jquery/ui/effects.highlight.js"></script>
        <script type="text/javascript" src="library/admin_jquery/external/bgiframe/jquery.bgiframe.js"></script>
        <script type="text/javascript" src="library/chat.js"></script>


        <script type="text/javascript">
            $(function() {
                $("#chat-online-dialog").dialog({
                    bgiframe: true,
                    autoOpen: false,
                    height: 500,
                    width: 400,
                    modal: true,
                    buttons: {
                        Cancel: function() {
                            $(this).dialog('close');
                        }
                    },

                    close: function() {
                    }
                });

                $('#show-those-online-for-chat').click(function() {
                    $('#chat-online-dialog').dialog('open');
                });

            });
            
            function beginChat(chatName){
                $("#chat-online-dialog").dialog('close');
                chatWith(chatName);
            }
            
            
            
            function getOnlineUsers(){
                $.ajax({
                    url: "index.php?p=ajax&m=sysadmin&t=get-online-users&d=1",
                    cache: false,
                    dataType: "json",
                    success: function(data) {
                        allOnlineUsers = "";
                        $.each(data.onlineusers, function(i,user){
                            disTitle = user.displayName + " is " + user.status;
                            allOnlineUsers += "<div>";
                            allOnlineUsers += "<a href=\"javascript:void(0)\" onclick=\"javascript:beginChat('" + user.name + "')\" title=\"" + disTitle + "\" class=\"chat-online-links-" + user.status + " chat-" + user.status + "-user\">" + user.displayName + "</a>";
                            allOnlineUsers += "</div>";
                        });
                        //alert (allOnlineUsers);
                        allOnlineUsers = allOnlineUsers == "" ? "Sorry! There are no other users on-line to chat with." : allOnlineUsers;
                        document.getElementById("chat-online-dialog").innerHTML = allOnlineUsers;
                        setTimeout('getOnlineUsers();',10000);
                }});
            }   //END getOnlineUsers()
            
            setTimeout('getOnlineUsers();', 10000);
            
        </script>

<?php
    //echo "<pre>" . print_r ($_SESSION, true) . "</pre>";
}   //END if user is logged in
?>