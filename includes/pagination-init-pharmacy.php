<?php
	//Safely, retrieve needed indexes
	$pageText = defined("SITE_PAGINATION_PAGES_TEXT") ? SITE_PAGINATION_PAGES_TEXT : "recpage";
	$recsPerPageText = defined("SITE_PAGINATION_PAGERECS_TEXT") ? SITE_PAGINATION_PAGERECS_TEXT : "pagerecs";
	
	//Get page from the url
	$page = isset($_GET[$pageText]) ? (int)$_GET[$pageText] : 1;
	$page = !empty($page) ? $page : 1;

	//The number of records per page
	$recsPerPage = 50;

	//***Remove all vars needed for pagination from the query string and pass others alongside the baseURL
		$qString = $_SERVER['QUERY_STRING'];	//e.g. a=b&c=d&e=f
		$newQString = "";
		if (!empty($qString)){
			$eachAssoc = explode ("&", $qString);	//$eachAssoc becomes e.g. Array (0 => "a=b", 1 => "c=d", 2 => "e=f")
			foreach ($eachAssoc as $assoc){
				$varParts = explode ("=", $assoc);	//$varParts becomes e.g. Array (0 => "a", 1 => "b")
				
				//If the first element (= var name) is not one of the pagination variables, then add it to the new query string
				if ($varParts[0] != $pageText && $varParts[0] != $recsPerPageText){
					if (empty($newQString))
						$newQString .= $assoc;
					else $newQString .= "&" . $assoc;
				}
			}
		}
	//***

	//Put all needed vars for the pagination method in an array
	$args["recordsPerPage"] 		= $recsPerPage;
	$args["pageText"] 				= $pageText;
	$args["recordsPerPageText"]		= $recsPerPageText;
	$args["curPage"]				= $page;
	$args["curATagParent"]			= "span";
	$args["baseURL"]				= !empty($newQString) ? "?" . $newQString : "";

	//Get the starting point for this page
	$limitStart = ($page - 1) * $recsPerPage;
?>