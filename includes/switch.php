<?php
//die ("<pre>" . print_r ($_POST, true) . "</pre>");
	switch ($p) {


//***************************Laboratory Section******************************
		case 'register':
			if ($_POST){
				if (file_exists("./modules/lab/registercheck.php"))
					include_once ("./modules/lab/registercheck.php");
			}
			$page = $m.'/register.php';
			$page_title = $lab_reg_title;
			break;

		case 'details':
			if ($_POST){
				if (file_exists("./modules/lab/detailscheck.php"))
					include_once ("./modules/lab/detailscheck.php");
			}
			$page = $m.'/details.php';
			$page_title = $lab_details_title;
			break;

		case 'doc_details':
			if ($_POST){
				if (file_exists("./modules/lab/detailscheck.php"))
					include_once ("./modules/lab/detailscheck.php");
			}
			$page = $m.'/doc_details.php';
			$page_title = $lab_details_title;
			break;

		case 'tests':
			if ($_POST){
				if (file_exists("./modules/lab/testcheck.php"))
					include_once ("./modules/lab/testcheck.php");
			}
			$page = $m.'/tests.php';
			$page_title = $lab_test_title;
			break;

		case 'doc_tests':
			if ($_POST){
				if (file_exists("./modules/lab/testcheck.php"))
					include_once ("./modules/lab/testcheck.php");
			}
			$page = $m.'/doc_tests.php';
			$page_title = $lab_test_title;
			break;
//***************************Laboratory Section ends******************************/
//***************************Patient Care Begins******************************//

		case 'advsearch':
			$page_title = $advancedsearch_label;
			$page = $m.'/patadvancedsearch.php';
			break;

		case 'register_patient':
			$page_title = 'Patient Registration';
			$page = $m.'/patb4reg.php';
			break;

        case 'patients_list':
			$page_title = 'List of Patients';
			$page = "patient_care/patients-list-all.php";
			break;

        case 'patients_list_view_individual':
			$page_title = 'Patient Details';
			$page = "patient_care/patient-all-details.php";
			break;

		case 'reg_form':
			//die ("<pre>" . print_r($_POST, true) . "</pre>");
			$page = $m.'/patregistration.php';
			$page_title = 'Patient Registration';
			$reg_dept = isset($_POST["reg_dept"]) ? $_POST["reg_dept"] : 0;
			$reg_clinic = isset($_POST["reg_clinic"]) ? $_POST["reg_clinic"] : 0;;
			$patype = isset($_POST["patype"]) ? $_POST["patype"] : 0;
			$reg_id = isset($_POST["reg_id"])? $_POST["reg_id"]: "";
		  	$select = new patSelectOptions();
			if($patype==1){
				header("Location:index.php?p=referer&m=patient_care&regid=".$reg_id."&regdept=".$reg_dept."&regclinic=".$reg_clinic);
			}
			break;

		case 'insert_registry':
                        //die ("<pre>" . print_r ($_POST, true) . "</pre>");
			$reg = new patRegistration;

			$_POST = admin_Tools::doEscape($_POST, $reg->conn);

			$reg->reg  = $_POST['reg'];
			$reg->temp  = $_POST['temp'];
			$reg->dest  = $_POST['dest'];

			if(isset($_POST['regextra'])){
				$reg->regextra = $_POST['regextra'];
			}

			$reg_dept = $_POST["reg_dept"];
			$reg_clinic = $_POST["reg_clinic"];

			$regid = $reg->registerPatient();
            if ($regid != 0){
                $page = $m.'/patconfirmreg.php';

                header("Location:index.php?p=confirmreg&m=patient_care&regid=" . $regid . "&regdept=" . $reg_dept . "&regclinic=" . $reg_clinic);
            } else {
                //$disMsg = urlencode("The hospital number you entered (" . $reg->reg['hospital_oldno'] . ") is not in an acceptable format or it has been assigned to another patient.");
                $disMsg = urlencode("Patient registration failed. Please, try again.");
                header ("Location: index.php?p=register_patient&m=patient_care&c=$disMsg");
            }
			break;

		case 'confirmreg':
			$page = $m.'/patconfirmreg.php';
			$page_title = 'Confirm Registration';

			break;


        //To view the medical history of a patient
        case 'view-patient-history':
			$page = 'patient_care/patient-medical-history.php';
			$page_title = 'Patient Medical History';

			break;


		case 'edit_reg':
			$page = $m.'/pateditpatient.php';
			$reg = new patRegistration;

			$reg_id = isset($_GET["regid"]) ? (int)$_GET["regid"] : 0;
			$reg_clinic = isset($_GET["regclinic"]) ? (int)$_GET["regclinic"] : 0;
			$reg_dept = isset($_GET["regdept"]) ? (int)$_GET["regdept"] : 0;

			$reg->Load_from_key($reg_id);
			$select = new patSelectOptions;

			$page_title = 'Edit Patient Details';
			break;

		case 'update_reg':
			//die ("<pre>" . print_r($_POST, true) . "</pre>");
			$reg = new patRegistration;

			$_POST = admin_Tools::doEscape($_POST, $reg->conn);

			if(isset($_POST['regextra'])){
				$reg->regextra = $_POST['regextra'];
			}
			$reg->temp  = $_POST['temp'];
			$reg->dest  = $_POST['dest'];
			$reg->reg  = $_POST['reg'];
			$reg_dept = $_POST["reg_dept"];
			$reg_clinic = $_POST["reg_clinic"];

			$regid = $reg->updatePatient();
            $reg->updateRegextra($regid);

            $justEditing = isset($_POST["jed"]) ? $_POST["jed"] : 0;
            //echo "<pre>" . print_r ($_GET, true) . "</pre>";
            if ($justEditing == 0){
                //echo "Not";
                header("Location:index.php?p=confirmreg&m=patient_care&regid=".$regid."&regdept=".$reg_dept."&regclinic=".$reg_clinic);
            } else {
                //echo "Just editing";
                $disMsg = urlencode("Your changes were successfully saved.");
                header ("Location: index.php?m=patient_care&p=edit_reg&regid=" . $regid . "&jed=1&c=" . $disMsg);
            }

			break;


		case 'referer':
			$page = $m.'/patreferer.php';
			$page_title = 'Registration Details';
			break;

		case 'insert_patreffer':
			//die ("<pre>" . print_r($_POST, true) . "</pre>");

			//Confirm that the maximum number of appointments for the chosen date has not been reached if need be
			if (isset($_POST["max_check"])){
				$appDate = isset($_POST["app_date"]) ? $_POST["app_date"] : "0000-00-00";
				$clinicID = isset($_POST["reg_clinic"]) ? $_POST["reg_clinic"] : 0;
				$clinicObj = new Clinic_New($curLangField, $clinicID);
                $appObj = new patAppointment();
				$appCount = $appObj->getAppointmentCount($appDate);
				$maxAppCount = $clinicObj->getMaxAppointment();
				//echo ("APPS: $appCount --- MAX: $maxAppCount");
                if ($appCount >= $maxAppCount){
					//die ("Good");
					//echo "  ";
                    $msg = urlencode("Maximum number of appointments for the chosen date has been reached.
                                        Choose, another date or uncheck the check box in the form to prevent checking if the maximum number of appointments have been reached.");
                    header("Location: index.php?p=referer&m=patient_care&regid={$_POST['reg_id']}&regdept={$_POST['reg_dept']}&regclinic={$_POST['reg_clinic']}&msg=$msg");
                    exit();
                } //else die ("APPS: $appCount --- MAX: $maxAppCount" . " ---WHY?");
			} //else die ("No check");
			//die ("o to!");

            //Set the date to be inserted into the DB to the date selected by the official using the provided calendar
            $_POST["reg"]["app_starttime"] = $_POST["app_date"] . " 00:00:00";
            $_POST["reg"]["app_endtime"] = $_POST["app_date"] . " 23:59:59";

			$reg = new patRegistration();
			$treat = new patTreatment();
			//die ("<pre>" . print_r ($_POST, true) . "</pre>");

			$reg->reg  = $_POST['reg'];
			$reg->reg_dept = $_POST['reg_dept'];
			$reg->reg_clinic = $_POST['reg_clinic'];

			if($reg->reg_dept == '6'){
				$tablename = "maternity";
				$prefix = "mat";
			} elseif($reg->reg_dept == '8'){
					$tablename = "emergency";
					$prefix = "emerg";
				} else {
						$tablename = "outpatient_admission";
						$prefix = "outp";
					}

			$check = $reg->checkHospitalNo($reg->reg['hospital_no']);
			if($check == 0){
				$page_title = $patient_not_found_label;
				$patuser_detail = 6;
				$page = $m.'/paterrorpage.php';
			} else {
				$patadm_id = $reg->insertPatadm($reg->reg['hospital_no']);
				$reg->insertTransaction($patadm_id);
				$treat->insertConditiontrack($patadm_id,'3');

				$reg->insertReferrer($patadm_id, $tablename, $prefix);
				$reg->insertAppointment($patadm_id);
				header("Location:index.php?p=pat_complete&m=patient_care&a=" . $reg->reg['consultant_id2'] . "&b=" . $reg->reg_dept . "&c=" . $reg->reg_clinic . "&d=" . $reg->reg['hospital_no'] . "&e=" . $reg->reg['consultant_id']);
			}
			break;

		case 'pat_complete':
			$page = $m.'/patregcomplete.php';
			$page_title = 'Registration Completed';
			break;

		case 'casenote':
			$subLinks = "";
			$page_title = 'Treatment';
			$page = $m . '/pat_medicalrecord.php';
			break;

		case 'viewallapp':
			$page_title = $view_appointment_header;
			$patuser = new patUsers;
			$select = new patSelectOptions;
			$patuser_detail = $patuser->getStaffType($myUserid);
			$page = $m.'/patappointment.php';
			break;


		case 'viewallapp_doc':
			$page_title = $view_appointment_header;
			$patuser = new patUsers;
			$select = new patSelectOptions;
			$patuser_detail = $patuser->getStaffType($myUserid);
			$page = $m.'/patappointment_doc.php';
			break;


		case 'pat_treatment':
//            $_POST['hist']['complaint'];
//            $_POST['hist']['complainthist'];
//            $_POST['hist']['medhist'];
//            $_POST['hist']['sochist'];
//            $_POST['hist']['drughist'];
//            $_POST['hist']['sysreview'];
//            $_POST['hist']['others'];
//
//            $_POST['treat']['genexam'];
//            $_POST['treat']['diagnosis'];
//            $_POST['treat']['sysexam'];
//            $_POST['treat']['diffdiagnosis'];
//
//            $_POST['treatment']['treatment'];

//            die ('<pre>' . print_r($_POST, TRUE) . '</pre>');
            $index_array = array(
                                1 => array('index1' => 'hist', 'index2' => 'complaint'),
                                2 => array('index1' => 'hist', 'index2' => 'complainthist'),
                                3 => array('index1' => 'hist', 'index2' => 'medhist'),
                                4 => array('index1' => 'hist', 'index2' => 'sochist'),
                                5 => array('index1' => 'hist', 'index2' => 'drughist'),
                                6 => array('index1' => 'hist', 'index2' => 'sysreview'),
                                7 => array('index1' => 'hist', 'index2' => 'others'),
                                8 => array('index1' => 'treat', 'index2' => 'genexam'),
                                9 => array('index1' => 'treat', 'index2' => 'sysexam'),
                                10 => array('index1' => 'treat', 'index2' => 'diffdiagnosis'),
                                11 => array('index1' => 'treat', 'index2' => 'diagnosis'),
                                12 => array('index1' => 'treatment', 'index2' => 'treatment')
                            );

            for ($k = 1; $k <= 12; $k++){
                if (!empty($_POST['treatment_question'][$k])){
                    foreach ($_POST['treatment_question'][$k] as $value){
                        $_POST[$index_array[$k]['index1']][$index_array[$k]['index2']] .= "\n," . $value;
                    }
                }
            }

            $patDataUploader = new patTreatmentSheetUpload();
            $patDataUploader->doPatTreatment();
			break;


		case "treatmentsheet":
			$page_title = "Treatment Sheet Upload (Patient Selection Page)";
			$page = $m . "/treatmentsheet-upload.php";
			break;

		case "treatmentsheet_entry":
			if ($_POST && $m == "patient_care"){
				//echo ("POST: <pre>" . print_r ($_POST, true) . "</pre>");
				//die ("FILES: <pre>" . print_r ($_FILES, true) . "</pre>");
				if (file_exists("./modules/$m/treatmentsheet-upload-entry-post.php"))
					include_once ("./modules/$m/treatmentsheet-upload-entry-post.php");
			}
			$page_title = "Upload Treatment Sheet";
			$page = $m . "/treatmentsheet-upload-entry.php";
			break;

        case "treatmentsheet_view":
			$page_title = "View Treatment Sheets";
			$page = $m . "/treatmentsheet-view.php";
			break;

        case 'pat_viewtreatment':
			//$subLinks = " ";
			$treats = new patTreatment;
			$page_title = "Medical Record";
			$app_id = $_GET['a']? $_GET['a'] : 0;
			$page = $m."/patpatientplan.php";
			$casenote_id =  $_GET['b']? $_GET['b'] : 0;
			break;

		case 'viewappdetails':
			$page_title = "Medical Record";
			$page = $m."/patviewappdetails.php";
			break;


    /*BEGIN Cases Added by Tunde*/
        case 'vitalsigns':
                $page_title = "Patient's Vital Signs (Patient / Clinic Selection Page)";
                $page = $m."/patcare_vitalsigns.php";
                break;

        case 'getlabtestresult4doc':
                $page_title = "Laboratory Test Results Selection Page";
                $page = "$m/doc_labtest_result.php";
                //$page_title = "Patient's Vital Signs (Patient Selection Page)";
                //$page = $m."/patcare_vitalsigns.php";
                break;

        case 'vitalsigns_entry':
                if ($_POST){
                    $postHandler = "modules/$m/patcare_vitalsigns_entry_post.php";
                    if (file_exists($postHandler))
                        include_once ($postHandler);
                }
                $page_title = "Patient's Vital Signs";
                $page = $m."/patcare_vitalsigns_entry.php";
                break;
    /*END Cases Added by Tunde*/
//***************************Patient Care Ends******************************//






//*** BEGIN REPORT STUFFS

		case 'reports':
			$page = $m.'/reports.php';
			$page_title = $report_general_pagetitle;
			break;
		case 'eveningreport':
			$page = $m.'/eveningreport.php';
			$page_title = $report_evening_header;
			break;
		case 'morningreport':
			$page = $m.'/morningreport.php';
			$page_title = $report_morning_header;
			break;
		case 'admissionbook':
			$page = $m.'/admissionbook.php';
			$page_title = $report_admissionbook_header;
			break;
		case 'accountbook':
			$page = $m.'/accountbook.php';
			$page_title = $report_accountbook_header;
			break;
		case 'bedstatewards':
			$page = $m.'/bedstatewards.php';
			$page_title = $report_bedstatewards;
			break;
		case 'bedstateward':
			$page = $m.'/bedstateward.php';
			$page_title = $report_bedstateward_header;
			break;
		case 'dailybed':
			$page = $m.'/dailybed.php';
			$page_title = $report_dailybed_header;
			break;
		case 'appointment':
			$page = $m.'/appointment.php';
			$page_title = $report_appointment_header;
			break;
		case 'accident':
			$page = $m.'/accident.php';
			$page_title = $report_accident_header;
			break;
		case 'patientsummaryward':
			$page = $m.'/patientsummaryward.php';
			$page_title = $report_patientsummaryward_header;
			break;
		case 'outpatmonthlyatt':
			$page = $m.'/outpatmonthlyatt.php';
			$page_title = $report_outpatmonthlyatt_header;
			break;
		case 'outpatmonthlyatt':
			$page = $m.'/outpatmonthlyatt.php';
			$page_title = $report_outpatmonthlyatt_header;
			break;

		//BEGIN Monthly Stats Pages
		case 'monthlystats':
			$page = $m.'/monthlystats.php';
			$page_title = $report_monthlystats;
			break;
		case 'inpatsummary':
			$page = $m.'/monthlystats_inpatsummary.php';
			$page_title = $report_monthlystats_inpsummary_header;
			$subLinks = "<a href=\"index.php?p=monthlystats&m=reports\">Monthly Stats</a>";
			break;
		case 'outpatattendance':
			$page = $m.'/monthlystats_outpatattendance.php';
			$page_title = $report_monthlystats_outpatattendance;
			$subLinks = "<a href=\"index.php?p=monthlystats&m=reports\">Monthly Stats</a>";
			break;
		case 'outpatattendanceage':
			$page = $m.'/monthlystats_outpatattendance_age.php';
			$page_title = $report_monthlystats_outpatattendance_age;
			$subLinks = "<a href=\"index.php?p=monthlystats&m=reports\">Monthly Stats</a>";
			break;
		case 'inpatattendanceage':
			$page = $m.'/monthlystats_inpatattendance_age.php';
			$page_title = $report_monthlystats_inpatattendance_age;
			$subLinks = "<a href=\"index.php?p=monthlystats&m=reports\">Monthly Stats</a>";
			break;
		case 'inpatattendanceadm':
			$page = $m.'/monthlystats_inpatattendanceadm.php';
			$page_title = $report_monthlystats_inpat_adm;
			$subLinks = "<a href=\"index.php?p=monthlystats&m=reports\">Monthly Stats</a>";
			break;
		case 'outpatunitbyunit':
			$page = $m.'/monthlystats_outpatunitbyunit.php';
			$page_title = $report_monthlystats_outpatunits;
			$subLinks = "<a href=\"index.php?p=monthlystats&m=reports\">Monthly Stats</a>";
			break;
		case 'inpatunitbyunit':
			$page = $m.'/monthlystats_inpatunitbyunit.php';
			$page_title = $report_monthlystats_inpatunits;
			$subLinks = "<a href=\"index.php?p=monthlystats&m=reports\">Monthly Stats</a>";
			break;
		case 'inpatunitbyunitadm':
			$page = $m.'/monthlystats_inpatunitbyunitadm.php';
			$page_title = $report_monthlystats_inpatunitsadm;
			$subLinks = "<a href=\"index.php?p=monthlystats&m=reports\">Monthly Stats</a>";
			break;
		case 'patientsource':
			$page = $m.'/monthlystats_patientsource.php';
			$page_title = $report_monthlystats_patientsource;
			$subLinks = "<a href=\"index.php?p=monthlystats&m=reports\">Monthly Stats</a>";
			break;
		case 'inoutdepts':
			$page = $m.'/monthlystats_inoutdepts.php';
			$page_title = $report_monthlystats_inoutdepts;
			$subLinks = "<a href=\"index.php?p=monthlystats&m=reports\">Monthly Stats</a>";
			break;
		case 'deptact':
			$page = $m.'/monthlystats_deptact.php';
			$page_title = $report_monthlystats_deptact;
			$subLinks = "<a href=\"index.php?p=monthlystats&m=reports\">Monthly Stats</a>";
			break;
	//END Monthly Stats Pages

//*** END REPORT STUFFS


//***BEGIN SYS ADMIN STUFFS
		case 'logout':
			include_once ("modules/sysadmin/logout.php");
			break;
		case 'ajax':
			include_once ("modules/sysadmin/admin_ajaxredirector.php");
			//header ("Location: ./modules/sysadmin/admin_ajaxTOC.php?{$_SERVER['QUERY_STRING']}");
			break;
		case 'sysadmin':
			$page = $m.'/sysadmin.php';
			$page_title = $admin_header;
			break;
		case 'menumanagement':
			$page = $m.'/menumanagement.php';
			$page_title = $admin_menumanagement;
			break;
		case 'menumanagement_edit':
			if ($_POST && $m == "sysadmin"){
				if (file_exists("./modules/$m/menumanagement_edit_post.php"))
					include_once ("./modules/$m/menumanagement_edit_post.php");
			}
			$page = $m.'/menumanagement_edit.php';
			$page_title = $admin_menumanagement_edit;
			break;
		case 'menumanagement_add':
			if ($_POST && $m == "sysadmin"){
				if (file_exists("./modules/$m/menumanagement_add_post.php"))
					include_once ("./modules/$m/menumanagement_add_post.php");
			}
			$page = $m.'/menumanagement_add.php';
			$page_title = $admin_menumanagement_add;
			break;
		case 'groupmanagement':
			$page = $m.'/groupmanagement.php';
			$page_title = $admin_groupmanagement;
			break;
		case 'groupmanagement_add':
			if ($_POST && $m == "sysadmin"){
				if (file_exists("./modules/$m/groupmanagement_add_post.php"))
					include_once ("./modules/$m/groupmanagement_add_post.php");
			}
			$page = $m.'/groupmanagement_add.php';
			$page_title = $admin_groupmanagement_addtitle;
			break;
		case 'groupmanagement_edit':
			if ($_POST && $m == "sysadmin"){
				if (file_exists("./modules/$m/groupmanagement_edit_post.php"))
					include_once ("./modules/$m/groupmanagement_edit_post.php");
			}
			$page = $m.'/groupmanagement_edit.php';
			$page_title = $admin_groupmanagement_edittitle;
			break;
		case 'rightmanagement':
			$page = $m.'/rightmanagement.php';
			$page_title = $admin_rightmanagement;
			break;
		case 'accessdenied':
			$page = $m.'/accessdenied.php';
			$page_title = $admin_accessdenied;
			break;
		case 'rightmanagement_edit':
			if ($_POST && $m == "sysadmin"){
				if (file_exists("./modules/$m/rightmanagement_edit_post.php"))
					include_once ("./modules/$m/rightmanagement_edit_post.php");
			}
			$page = $m.'/rightmanagement_edit.php';
			$page_title = $admin_rightmanagement_edit;
			break;
		case 'usermanagement':
			$page = $m.'/usermanagement.php';
			$page_title = $admin_usermanagement;
			break;
		case 'usermanagement_edit':
			if ($_POST && $m == "sysadmin"){
				if (file_exists("./modules/$m/usermanagement_edit_post.php"))
					include_once ("./modules/$m/usermanagement_edit_post.php");
			}
			$page = $m.'/usermanagement_edit.php';
			$page_title = $admin_usermanagement_config;
			break;
		case 'usermanagement_bankedit':
			if ($_POST && $m == "sysadmin"){
				if (file_exists("./modules/$m/usermanagement_bankedit_post.php"))
					include_once ("./modules/$m/usermanagement_bankedit_post.php");
			}
			$page = $m.'/usermanagement_bankedit.php';
			$page_title = $admin_usermanagement_config;
			break;
		case 'audittrail':
			$page = $m.'/audittrail.php';
			$page_title = $admin_audittrail;
			break;
		case 'audittrail_allpagenames':
			$page = $m.'/audittrail_allpagenames.php';
			$page_title = $admin_audittrail_allpagenames;
			break;
		case 'audittrail_post':
			$page = $m.'/audittrail_post.php';
			$page_title = $admin_audittrail_allpagenames;
			break;
		case 'staffregister':
			if ($_POST && $m == "sysadmin"){
				if (file_exists("./modules/$m/staffregister_post.php"))
					include_once ("./modules/$m/staffregister_post.php");
			}
			$page = $m.'/staffregister.php';
			$page_title = $staffreg_title;
			break;
		case 'changepass':
			if ($_POST && $m == "sysadmin"){
				if (file_exists("./modules/$m/changepass_post.php"))
					include_once ("./modules/$m/changepass_post.php");
			}
			$page = $m.'/changepass.php';
			$page_title = $admin_changepass_title;
			break;

	//***END SYS ADMIN STUFFS


	//***BEGIN: BILLING STUFFS ADDED BY TUNDE

		case "viewfinreports":
			if ($_POST && $m == "reports"){
				//if (file_exists("./modules/$m/changepass_post.php"))
				//	include_once ("./modules/$m/changepass_post.php");
			}
			$page = $m . '/financial-reports.php';
			$page_title = "TRANSACTION REPORTS";
            $subLinks = "<a href=\"index.php?p=viewfinreports&m=reports\">Financial Reports</a>";
			break;


        case "viewfinreports-personal":
			$page = $m . '/financial-reports-individual.php';
			$page_title = "TRANSACTION REPORTS (TELLER)";
			break;

        case "print-invoice":
			$page = 'modules/patient_care/billing/print-invoice.php';
            include_once ($page);
            exit();
			break;

	//***END: BILLING STUFFS ADDED BY TUNDE





//********BEGIN Ahmed's modules

	//manage wards--------------------
	case 'ward':

		$m='administrator/';
		if (isset($_POST['addward']) || !empty($_POST['addward'])){

			if (file_exists("./modules/$m/wards_post.php")){
			  //echo "enter";
					include_once ("./modules/$m/wards_post.php");
				}
			}
		$page = $m.'wards.php';
		$page_title = $lang_PageTitle_ward;
		break;

	case 'editward':

		$m='administrator/';

		$page = $m.'EditWard.php';

		$page_title = $lang_PageTitle_editward;
		break;

		//manage drug category--------------------

	case 'editdrug':

		$m='administrator/';

		$page = $m.'editDrugcat.php';

		$page_title = $lang_PageTitle_editdrug;
		break;
	case 'drug':

		$m='administrator/';
	if (isset($_POST['adddrug']) || !empty($_POST['adddrug'])){
		if (file_exists("./modules/$m/drugCat_post.php"))
					include_once ("./modules/$m/drugCat_post.php");
			}

		$page = $m.'drugCat.php';
		$page_title = $lang_PageTitle_drug;
		break;


		//manage drugs sub category--------------------

	case 'editdrugsub':

		$m='administrator/';
		//
		$page = $m.'editsubDrugcat.php';

		$page_title = $lang_PageTitle_editdrugsub;
		break;
   case 'drugsub':
		$m='administrator/';
		if (isset($_POST['adddrugsub']) || !empty($_POST['adddrugsub'])){
		if (file_exists("./modules/$m/drugsubCat_post.php"))
					include_once ("./modules/$m/drugsubCat_post.php");
			}
		$page = $m.'drugsubCat.php';

		$page_title = $lang_PageTitle_subdrug;
		break;


		//manage drugs--------------------

	case 'editpdrug':

		$m='administrator/';
		//
		$page = $m.'editpatdrug.php';

		$page_title = $lang_PageTitle_editpdrug;
		break;
   case 'pdrug':
		$m='administrator/';
		if (isset($_POST['addpdrug']) || !empty($_POST['addpdrug'])){
			if (file_exists("./modules/$m/patdrug_post.php")){
                include_once ("./modules/$m/patdrug_post.php");
            }
        }
		$page = $m.'patdrug.php';
		$page_title = $lang_PageTitle_pdrug;
		break;

		//manage drugs order--------------------

  case 'editdrugtype':
		$m='administrator/';
		//
		$page = $m.'editdrug_priority.php';
		$page_title = $lang_drugtype_edit;
		break;
   case 'drugtype':

	$m='administrator/';
	   if (isset($_POST['adddrugtype']) || !empty($_POST['adddrugtype'])){
				if (file_exists("./modules/$m/drug_priority_post.php"))
					include_once ("./modules/$m/drug_priority_post.php");
			}
		$page = $m.'drug_priority.php';

		$page_title = $lang_drugtype_title;
		break;
	case 'drugorder':

		$m='administrator/';

		$page = $m.'moverow2.php';

		$page_title = $lang_drugtypeReorder_title;
		break;

		//manage service caegory--------------------

	case 'editscat':

		$m='administrator/';

		$page = $m.'editservicecat.php';

		$page_title = $lang_PageTitle_editscat;
		break;
	case 'scat':

		$m='administrator/';

		$page = $m.'servicecat.php';

		$page_title = $lang_PageTitle_scat;
		break;

		//manage services--------------------

	case 'editservice':

		$m='administrator/';

		$page = $m.'editservice.php';

		$page_title = $lang_PageTitle_editservice;
		break;

	case 'service':

		$m='administrator/';

		$page = $m.'service.php';

		$page_title = $lang_PageTitle_service;
		break;

			//manage service names--------------------

	case 'editservicename':

		$m='administrator/';

		$page = $m.'editservicename.php';

		$page_title = $lang_PageTitle_editservicename;
		break;

	case 'servicename':
        $m='administrator/';
		if (isset($_POST['addservicename']) || !empty($_POST['addservicename'])){

			if (file_exists("./modules/$m/servicename_post.php")){

					include_once ("./modules/$m/servicename_post.php");
				}
			}
		$page = $m.'servicename.php';
		$page_title = $lang_PageTitle_servicename;
		break;

	case 'billing':
		$m='patient_care/billing/';
		$page = $m . 'billing.php';
		$page_title = $lang_PageTitle_billing;
		break;

    case 'custom-invoice':
        $m = 'patient_care/billing/';
		$page = $m . 'custom-invoice.php';
		$page_title = 'Create a Custom Invoice';
        if ($_POST && file_exists("./modules/patient_care/billing/custom-invoice-post.php")){
            include_once ("./modules/patient_care/billing/custom-invoice-post.php");
        }
		break;

	case 'invoice':
		$m='patient_care/billing/';
		$page = $m.'invoice.php';
		$page_title = $lang_PageTitle_invoicing;
		break;

	case 'pharmacybill2':
		$m='patient_care/phamarcy/';
		$page = $m.'pharmacy_dashboard.php';
		$page_title = $lang_PageTitle_druddispensing;
		break;
	case 'pharmacydashboard':
		$m='patient_care/phamarcy/';
		$page = $m.'patients_awaitgdrugs.php';
		$page_title = $lang_PageTitle_druddispensing;
		break;
	case 'pharmacybill':
		$m='patient_care/phamarcy/';
		$page = $m.'doctorprescription.php';
		$page_title = $lang_PageTitle_druddispensing;
		break;
	case 'labtestbill':
		$m='patient_care/billing/';
		$page = $m.'doctorlabtest.php';
		$page_title = $lang_PageTitle_labservice;
		break;

	case 'labtestbill2':
		$m='patient_care/billing/';
		$page = $m.'labtest_dashboard.php';
		$page_title = $lang_PageTitle_labservice;
		break;

	case 'invoicedrug':
		$m='patient_care/phamarcy/';
		$page = $m.'invoicedrug.php';
		$page_title = $lang_PageTitle_druddispensing;
		break;

	case 'invoice4service':
		$m='patient_care/billing/';
		$page = $m.'invoice4service.php';
		$page_title = $lang_PageTitle_invoicing;
		break;

	case 'receipt':
		$m='patient_care/billing/';
		$page = $m.'receipt.php';
		$page_title = $lang_PageTitle_receipting;
		break;
		//manage diagnosis--------------------

	case 'editdiagnosis':

		$m='administrator/';
		$page = $m.'editdiagnosis.php';
		$page_title = $lang_PageTitle_editdrug;
		break;

	case 'diagnosis':

		$m='administrator/';
		$page = $m.'diagnosis.php';
		$page_title = $lang_PageTitle_drug;
		break;
	//manage inpatient------------------------------

	case 'registry':
		$m='patient_care/';
		$page = $m.'registry.php';
		$page_title = $lang_PageTitle_registry;
		break;

   case 'inp':
		//die("entered");
		$m='patient_care/admission/';
		if (isset($_POST['addinp']) || !empty($_POST['addinp'])){

			if (file_exists("./modules/$m/inpatientAdmission_post.php")){

					include_once ("./modules/$m/inpatientAdmission_post.php");
				}
			}

		$page = $m.'inpatientAdmission.php';
		//echo $page;
		$page_title = $lang_PageTitle_inpatient;
		break;

 case 'viewinp':

		$m='patient_care/admission/';

		$page = $m.'viewInpatientAdmission.php';

		$page_title = $lang_PageTitle_inpatientview;
		break;

	case 'patinp':
	//die("entered");
		$m='patient_care/admission/';
		if (isset($_POST['addpatinp']) || !empty($_POST['addpatinp'])){
		if (file_exists("./modules/$m/inp_deposit_post.php"))
					include_once ("./modules/$m/inp_deposit_post.php");
			}
		$page = $m.'inp_deposit.php';

		$page_title = $lang_PageTitle_inpatientdeposit;
		break;


	case 'viewadm_charges':

		$m='patient_care/billing/';

		$page = $m.'viewadm_charges.php';

		$page_title = $lang_PageTitle_admcharges;
		break;

	case 'viewservice_charges':

		$m='patient_care/billing/';

		$page = $m.'viewservice_charges.php';

		$page_title = $lang_PageTitle_admcharges;
		break;

	case 'viewinvoices':

		$m='patient_care/billing/';

		$page = $m.'viewinvoices.php';

		$page_title = $lang_patient_invoices;
		break;

	case 'viewinvoice':

		$m='patient_care/billing/';

		$page = $m.'viewinvoice.php';

		$page_title = $lang_PageTitle_admcharges;
		break;


	case 'viewinvoice4service':

		$m='patient_care/billing/';

		$page = $m.'viewinvoice4service.php';

		$page_title = "Invoice / Receipt";
		break;
	case 'viewinvoice4reg':

		$m='patient_care/billing/';

		$page = $m.'viewinvoice4reg.php';
		$page_title = $lang_PageTitle_admcharges;
		break;

	case 'viewinvoice4drug':

		$m='patient_care/phamarcy/';

		$page = $m.'viewinvoice4drug.php';
		$page_title = $lang_PageTitle_admcharges;
		break;

	case 'viewreceipt':

		$m='patient_care/billing/';

		$page = $m.'viewreceipt.php';

		$page_title = $lang_PageTitle_admchargesReceipt;
		break;

	case 'inpDischarge':

		$m='patient_care/admission/';
		if(isset($_POST['edit'])|| !empty($_POST['edit'])){

		if (file_exists("./modules/$m/inpatientDischarge_post.php"))
					include_once ("./modules/$m/inpatientDischarge_post.php");
			}
		$page = $m.'inpatientDischarge.php';

		$page_title = $lang_PageTitle_inpatientdischarge;
		break;
		//patient Admission

	case 'editinp':

		$m='patient_care/admission/';
				if(isset($_POST['edit'])|| !empty($_POST['edit'])){
			//echo "entered";
		//exit();
		if (file_exists("./modules/$m/inpatientDischarge_post.php"))
					include_once ("./modules/$m/inpatientDischarge_post.php");
			}

		$page = $m.'editinpatientAdmission.php';

		$page_title = $lang_PageTitle_inpatientedit;
		break;

		//patient Admission
	case 'pat':

		$m='admissionDischarge/';

		$page = $m.'patientAdmission.php';

		$page_title = $lang_PageTitle_patient;
		break;

	case 'editpat':

		$m='admissionDischarge/';

		$page = $m.'editpatientAdmission.php';

		$page_title = $lang_PageTitle_patient;
		break;

	//-----------------Drugs

	case 'editdrugint':

		$m='patient_care/phamarcy/';

		$page = $m.'editDruginteraction.php';

		$page_title = $lang_PageTitle_drug;
		break;

	case 'drugint':

		$m='patient_care/phamarcy/';

		$page = $m.'druginteraction.php';

		$page_title = $lang_PageTitle_drug;
		break;
//********BEGIN Ahmed's modules

	//manage wards--------------------
	case 'ward':

		$m='administrator/';
		if (isset($_POST['addward']) || !empty($_POST['addward'])){

			if (file_exists("./modules/$m/wards_post.php")){
			  //echo "enter";
					include_once ("./modules/$m/wards_post.php");
				}
			}
		$page = $m.'wards.php';
		$page_title = $lang_PageTitle_ward;
		break;

	case 'editward':

		$m='administrator/';

		$page = $m.'EditWard.php';

		$page_title = $lang_PageTitle_editward;
		break;

		//manage drug category--------------------

	case 'editdrug':

		$m='administrator/';

		$page = $m.'editDrugcat.php';

		$page_title = $lang_PageTitle_editdrug;
		break;
	case 'drug':

		$m='administrator/';
	if (isset($_POST['adddrug']) || !empty($_POST['adddrug'])){
		if (file_exists("./modules/$m/drugCat_post.php"))
					include_once ("./modules/$m/drugCat_post.php");
			}

		$page = $m.'drugCat.php';
		$page_title = $lang_PageTitle_drug;
		break;


		//manage drugs sub category--------------------

	case 'editdrugsub':

		$m='administrator/';
		//
		$page = $m.'editsubDrugcat.php';

		$page_title = $lang_PageTitle_editdrugsub;
		break;
   case 'drugsub':
		$m='administrator/';
		if (isset($_POST['adddrugsub']) || !empty($_POST['adddrugsub'])){
		if (file_exists("./modules/$m/drugsubCat_post.php"))
					include_once ("./modules/$m/drugsubCat_post.php");
			}
		$page = $m.'drugsubCat.php';

		$page_title = $lang_PageTitle_subdrug;
		break;


		//manage drugs--------------------

	case 'editpdrug':

		$m='administrator/';
		//
		$page = $m.'editpatdrug.php';

		$page_title = $lang_PageTitle_editpdrug;
		break;
   case 'pdrug':
		$m='administrator/';
		if (isset($_POST['addpdrug']) || !empty($_POST['addpdrug'])){
			if (file_exists("./modules/$m/patdrug_post.php")){
			  		include_once ("./modules/$m/patdrug_post.php");
				}
			}
		$page = $m.'patdrug.php';
		$page_title = $lang_PageTitle_pdrug;
		break;

		//manage drugs order--------------------

  case 'editdrugtype':
		$m='administrator/';
		//
		$page = $m.'editdrug_priority.php';
		$page_title = $lang_drugtype_edit;
		break;
   case 'drugtype':

	$m='administrator/';
	   if (isset($_POST['adddrugtype']) || !empty($_POST['adddrugtype'])){
				if (file_exists("./modules/$m/drug_priority_post.php"))
					include_once ("./modules/$m/drug_priority_post.php");
			}
		$page = $m.'drug_priority.php';

		$page_title = $lang_drugtype_title;
		break;
	case 'drugorder':

		$m='administrator/';

		$page = $m.'moverow2.php';

		$page_title = $lang_drugtypeReorder_title;
		break;

		//manage service caegory--------------------

	case 'editscat':

		$m='administrator/';

		$page = $m.'editservicecat.php';

		$page_title = $lang_PageTitle_editscat;
		break;
	case 'scat':

		$m='administrator/';

		$page = $m.'servicecat.php';

		$page_title = $lang_PageTitle_scat;
		break;

		//manage services--------------------

	case 'editservice':

		$m='administrator/';

		$page = $m.'editservice.php';

		$page_title = $lang_PageTitle_editservice;
		break;

	case 'service':

		$m='administrator/';

		$page = $m.'service.php';

		$page_title = $lang_PageTitle_service;
		break;

			//manage service names--------------------

	case 'editservicename':

		$m='administrator/';

		$page = $m.'editservicename.php';

		$page_title = $lang_PageTitle_editservicename;
		break;

	case 'servicename':
        $m='administrator/';
		if (isset($_POST['addservicename']) || !empty($_POST['addservicename'])){

			if (file_exists("./modules/$m/servicename_post.php")){

					include_once ("./modules/$m/servicename_post.php");
				}
			}
		$page = $m.'servicename.php';
		$page_title = $lang_PageTitle_servicename;
		break;

	case 'billing':
		$m='patient_care/billing/';
		$page = $m.'billing.php';
		$page_title = $lang_PageTitle_billing;
		break;

	case 'invoice':
		$m='patient_care/billing/';
		$page = $m.'invoice.php';
		$page_title = $lang_PageTitle_invoicing;
		break;

	case 'pharmacybill2':
		$m='patient_care/phamarcy/';
		$page = $m.'pharmacy_dashboard.php';
		$page_title = $lang_PageTitle_druddispensing;
		break;
	case 'pharmacydashboard':
		$m='patient_care/phamarcy/';
		$page = $m.'patients_awaitgdrugs.php';
		$page_title = $lang_PageTitle_druddispensing;
		break;
	case 'pharmacybill':
		$m='patient_care/phamarcy/';
		$page = $m.'doctorprescription.php';
		$page_title = $lang_PageTitle_druddispensing;
		break;
	case 'labtestbill':
		$m='patient_care/billing/';
		$page = $m.'doctorlabtest.php';
		$page_title = $lang_PageTitle_labservice;
		break;

	case 'labtestbill2':
		$m='patient_care/billing/';
		$page = $m.'labtest_dashboard.php';
		$page_title = $lang_PageTitle_labservice;
		break;

	case 'invoicedrug':
		$m='patient_care/phamarcy/';
		$page = $m.'invoicedrug.php';
		$page_title = $lang_PageTitle_druddispensing;
		break;

	case 'invoice4service':
		$m='patient_care/billing/';
		$page = $m.'invoice4service.php';
		$page_title = $lang_PageTitle_invoicing;
		break;

	case 'receipt':
		$m='patient_care/billing/';
		$page = $m.'receipt.php';
		$page_title = $lang_PageTitle_receipting;
		break;
		//manage diagnosis--------------------

	case 'editdiagnosis':

		$m='administrator/';
		$page = $m.'editdiagnosis.php';
		$page_title = $lang_PageTitle_editdrug;
		break;

	case 'diagnosis':

		$m='administrator/';
		$page = $m.'diagnosis.php';
		$page_title = $lang_PageTitle_drug;
		break;
	//manage inpatient------------------------------

	case 'registry':
		$m='patient_care/';
		$page = $m.'registry.php';
		$page_title = $lang_PageTitle_registry;
		break;

/*   case 'inp':
		$m='patient_care/admission/';
		if (isset($_POST['addinp']) || !empty($_POST['addinp'])){

			if (file_exists("./modules/$m/inpatientAdmission_post.php")){

					include_once ("./modules/$m/inpatientAdmission_post.php");
				}
			}

		$page = $m.'inpatientAdmission.php';
		//echo $page;
		$page_title = $lang_PageTitle_inpatient;
		break;*/

 case 'viewinp':

		$m='patient_care/admission/';

		$page = $m.'viewInpatientAdmission.php';

		$page_title = $lang_PageTitle_inpatientview;
		break;

	case 'patinp':

		$m='patient_care/admission/';
		if (isset($_POST['addpatinp']) || !empty($_POST['addpatinp'])){
		if (file_exists("./modules/$m/inp_deposit_post.php"))
					include_once ("./modules/$m/inp_deposit_post.php");
			}
		$page = $m.'inp_deposit.php';

		$page_title = $lang_PageTitle_inpatientdeposit;
		break;


	case 'viewadm_charges':

		$m='patient_care/billing/';

		$page = $m.'viewadm_charges.php';

		$page_title = $lang_PageTitle_admcharges;
		break;

	case 'viewservice_charges':

		$m='patient_care/billing/';

		$page = $m.'viewservice_charges.php';

		$page_title = $lang_PageTitle_admcharges;
		break;

	case 'viewinvoices':

		$m='patient_care/billing/';

		$page = $m.'viewinvoices.php';

		$page_title = $lang_patient_invoices;
		break;

	case 'viewinvoice':

		$m='patient_care/billing/';

		$page = $m.'viewinvoice.php';

		$page_title = $lang_PageTitle_admcharges;
		break;


	case 'viewinvoice4service':

		$m='patient_care/billing/';

		$page = $m.'viewinvoice4service.php';

		$page_title = $lang_PageTitle_admcharges;
		break;

	case 'viewinvoice4drug':

		$m='patient_care/phamarcy/';

		$page = $m.'viewinvoice4drug.php';
		$page_title = $lang_PageTitle_admcharges;
		break;

	case 'viewreceipt':

		$m='patient_care/billing/';

		$page = $m.'viewreceipt.php';

		$page_title = $lang_PageTitle_admchargesReceipt;
		break;

	case 'inpDischarge':

		$m='patient_care/admission/';
		if(isset($_POST['edit'])|| !empty($_POST['edit'])){

		if (file_exists("./modules/$m/inpatientDischarge_post.php"))
					include_once ("./modules/$m/inpatientDischarge_.php");
			}
		$page = $m.'inpatientDischarge.php';

		$page_title = $lang_PageTitle_inpatientdischarge;
		break;
		//patient Admission

	case 'editinp':

		$m='patient_care/admission/';
				if(isset($_POST['edit'])|| !empty($_POST['edit'])){
			//echo "entered";
		//exit();
		if (file_exists("./modules/$m/inpatientDischarge_post.php"))
					include_once ("./modules/$m/inpatientDischarge_post.php");
			}

		$page = $m.'editinpatientAdmission.php';

		$page_title = $lang_PageTitle_inpatientedit;
		break;

		//patient Admission
	case 'pat':

		$m='admissionDischarge/';

		$page = $m.'patientAdmission.php';

		$page_title = $lang_PageTitle_patient;
		break;

	case 'editpat':

		$m='admissionDischarge/';

		$page = $m.'editpatientAdmission.php';

		$page_title = $lang_PageTitle_patient;
		break;

	//-----------------Drugs

	case 'editdrugint':

		$m='patient_care/phamarcy/';

		$page = $m.'editDruginteraction.php';

		$page_title = $lang_PageTitle_drug;
		break;

	case 'drugint':

		$m='patient_care/phamarcy/';

		$page = $m.'druginteraction.php';

		$page_title = $lang_PageTitle_drug;
		break;








        /***    BEGIN: Pharmacy Inventory    ***/
            case 'inventory':
		$page = $m . '/inventory/inventory-index-jfd6739enj.php';
                $subLinks = "<a href=\"index.php?p=inventory list items&m=pharmacy\">".$inventory_items."</a>
                    <a href=\"index.php?p=inventory list categories&m=pharmacy\">".$inventory_categories."</a>
                        <a href=\"index.php?p=inventory list locations&m=pharmacy\">".$inventory_locations."</a>
                        <a href=\"index.php?p=inventory list vendors&m=pharmacy\">".$inventory_Vendors."</a>
                   <a href=\"index.php?p=inventory list presentations&m=pharmacy\">".$inventory_presentations."</a>
                       <a href=\"index.php?p=inventory list forms&m=pharmacy\">".$inventory_forms."</a>";

                $page_title = $inventory_index_page_title;

        //die ($page);
		break;

                    case "inventory_ajax":
                    include_once ("modules/pharmacy/inventory/ajax_operations.php");
                    break;

                    case 'inventory list items':
                    $page=$m.'/inventory/incs/items/itemList.php';
                    $subLinks="<a href=\"index.php?p=inventory add item&m=pharmacy\">".$inventory_add_item."</a>";
                    $page_title=$inventory_items;
                    break;

                    case 'inventory show item':
                    $page=$m.'/inventory/incs/items/showItem.php';
                    $subLinks="<a href=\"index.php?p=inventory add item&m=pharmacy\">".$inventory_add_item."</a><a href=\"index.php?p=inventory list items&m=pharmacy\">".$inventory_items."</a>";
                    $page_title=$inventory_items;
                    break;

                case 'inventory show item category':
                    $page=$m.'/inventory/incs/items/itemCategoryList.php';
                    $subLinks="<a href=\"index.php?p=inventory add item&m=pharmacy\">".$inventory_add_item."</a><a href=\"index.php?p=inventory list items&m=pharmacy\">".$inventory_items."</a>";
                    $page_title=$inventory_items;
                    break;

                case 'inventory add item':
                    $page=$m.'/inventory/incs/items/newItemForm.php';
                    $subLinks="<a href=\"index.php?p=inventory list items&m=pharmacy\">".$inventory_items."</a>";
                    $page_title=$inv_itemForm_create_prompt;
                break;

                case 'inventory_edit_item':
                    $page=$m.'/inventory/incs/items/editItemForm.php';
                    $subLinks="<a href=\"index.php?p=inventory list items&m=pharmacy\">".$inventory_items."</a>";
                    $page_title=$inventory_edit_item;
                break;

                case 'inventory show location stock':
		$page = $m . '/inventory/incs/items/locationStock.php';
                    $subLinks="<a href=\"index.php?p=inventory list items&m=pharmacy\">".$inventory_items."</a><a href=\"index.php?p=inventory add items&m=pharmacy\">".$inventory_add_item."</a>";
                $page_title=$inventory_item_locations_distribution;
                break;



                case 'inventory show item locations distribution':
		$page = $m . '/inventory/incs/items/itemLocationsDistribution.php';
                    $subLinks="<a href=\"index.php?p=inventory list items&m=pharmacy\">".$inventory_items."</a>";
                $page_title=$inventory_item_locations_distribution;
                break;

                case 'inventory delete category':
		$page = $m . '/inventory/incs/categories/categoryList.php';
                $subLinks = "<a href=\"index.php?p=inventory add category & m=pharmacy\">".$inventory_add_category."</a>
                            <a href=\"index.php?p=inventory list categories & m=pharmacy\">".$inventory_categories."</a>";
                $page_title = $inventory_categories;
                break;

                case 'inventory list categories':
		$page = $m . '/inventory/incs/categories/categoryList.php';

                $subLinks = "<a href=\"index.php?p=inventory add category&m=pharmacy\">".$inventory_add_category."</a>";
                $page_title = $inventory_categories;
                break;

                case 'inventory edit category':
		$page = $m . '/inventory/incs/categories/categoryEdit.php';

                $subLinks = "<a href=\"index.php?p=inventory add category&m=pharmacy\">".$inventory_add_category."</a><a href=\"index.php?p=inventory list categories&m=pharmacy\">".$inventory_categories."</a>";
                $page_title = $inventory_categories;
                break;

                case 'inventory add category':
		$page = $m . '/inventory/incs/categories/newCategoryForm.php';

                $subLinks = "<a href=\"index.php?p=inventory list categories&m=pharmacy\">".$inventory_categories."</a>";
                $page_title = $inv_categoryForm_createLabel_prompt;
                break;

                case 'inventory list vendors':
		$page = $m . '/inventory/incs/vendors/vendorList.php';

                $subLinks = "<a href=\"index.php?p=inventory add vendor&m=pharmacy\">".$inventory_add_vendor."</a>";
               $page_title = $inventory_vendor;
               break;

                case 'inventory edit vendor':
		$page = $m . '/inventory/incs/vendors/editVendor.php';

                $subLinks = "<a href=\"index.php?p=inventory list vendors&m=pharmacy\">".$inventory_list_vendors."</a>
                    <a href=\"index.php?p=inventory add vendor&m=pharmacy\">".$inventory_add_vendor."</a>";
               $page_title = $inventory_vendor;
               break;

            case 'inventory list vendors':
		$page = $m . '/inventory/incs/vendors/vendorList.php';

                $subLinks = "<a href=\"index.php?p=inventory add vendor&m=pharmacy\">".$inventory_add_vendor."</a>";
               $page_title = $inventory_vendor;
               break;

                case 'inventory show vendor':
		$page = $m . '/inventory/incs/vendors/showVendor.php';

                $subLinks = "<a href=\"index.php?p=inventory list vendors&m=pharmacy\">".$inventory_list_vendors."</a>
                    <a href=\"index.php?p=inventory add vendor&m=pharmacy\">".$inventory_add_vendor."</a>";
               $page_title = $inventory_vendor;
               break;

               case 'inventory delete vendor':
                $page = $m . '/inventory/incs/vendors/vendorList.php';

                $subLinks = "<a href=\"index.php?p=inventory list vendors&m=pharmacy\">".$inventory_vendors."</a>";
               $page_title = $inventory_vendor;
               break;

                case 'inventory add vendor':
		$page = $m . '/inventory/incs/vendors/vendorForm.php';

                $subLinks = "<a href=\"index.php?p=inventory list vendors&m=pharmacy\">".$inventory_list_vendors."</a>";
               $page_title = $inventory_vendor;
               break;

                case 'inventory list locations':
        	$page = $m . '/inventory/incs/locations/listLocations.php';

                $subLinks = "<a href=\"index.php?p=inventory add location&m=pharmacy\">".$inventory_add_new_location."</a>";
                $page_title = $inventory_location;
                break;

                case 'inventory edit location':
        	$page = $m . '/inventory/incs/locations/editLocation.php';

                $subLinks = "<a href=\"index.php?p=inventory add location&m=pharmacy\">".$inventory_add_new_location."</a><a href=\"index.php?p=inventory list locations&m=pharmacy\">".$inventory_locations."</a>";
                $page_title = $inventory_location;
                break;

                case 'inventory add location':
        	$page = $m . '/inventory/incs/locations/locationForm.php';

                $subLinks = "<a href=\"index.php?p=inventory list locations&m=pharmacy\">".$inventory_locations."</a>";
                $page_title = $inventory_location;
                break;

                case 'inventory delete location':
        	$page = $m . '/inventory/incs/locations/listLocations.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_location;
                break;

                case 'inventory list manufacturers':
		$page = $m . '/inventory/incs/manufacturers/listManufacturers.php';

                $subLinks = "<a href=\"index.php?p=inventory add manufacturer&m=pharmacy \">".$inventory_add_new_manufacturer."</a>";
                $page_title = $inventory_manufacturer;
                break;

                case 'inventory edit manufacturer':
		$page = $m . '/inventory/incs/manufacturers/editManufacturer.php';

                $subLinks = "<a href=\"index.php?p=inventory list manufacturers&m=pharmacy\">".$inventory_manufacturers."</a>";
                $page_title = $inventory_manufacturer;
                break;

                case 'inventory add manufacturer':
		$page = $m .'/inventory/incs/manufacturers/newManufacturerForm.php';

                $subLinks = "<a href=\"index.php?p=inventory list manufacturers&m=pharmacy\">".$inventory_manufacturers."</a>";
                $page_title = $inventory_manufacturer;
                break;

                case 'inventory show manufacturer':
		$page = $m .'/inventory/incs/manufacturers/showManufacturer.php';

                $subLinks = "<a href=\"index.php?p=inventory list manufacturers&m=pharmacy\">".$inventory_manufacturers."</a>";
                $page_title = $inventory_manufacturer;
                break;

                case 'inventory delete manufacturers':
		$page = $m . '/inventory/incs/manufacturers/listManufacturers.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_manufacturer;
                break;

                case 'inventory list forms':
		$page = $m . '/inventory/incs/forms/listForms.php';

                $subLinks = "<a href=\"index.php?p=inventory add form&m=pharmacy\">".$inventory_add_new_form."</a>";
                $page_title = $inventory_form;
                break;

                case 'inventory add form':
		$page = $m . '/inventory/incs/forms/newInventoryForm.php';

                $subLinks = "<a href=\"index.php?p=inventory list forms&m=pharmacy\">".$inventory_forms."</a>";
                $page_title = $inventory_form;
                break;

                case 'inventory edit form':
		$page = $m . '/inventory/incs/forms/editForm.php';

                $subLinks = "<a href=\"index.php?p=inventory list forms&m=pharmacy\">".$inventory_forms."</a>
                    <a href=\"index.php?p=inventory add form&m=pharmacy\">".$inventory_add_new_form."</a>";
                $page_title = $inventory_form;
                break;

                case 'inventory delete form':
		$page = $m . '/inventory/incs/forms/listForms.php';

                $subLinks = "<a href=\"index.php?p=inventory list forms&m=pharmacy\">".$inventory_forms."</a>";
                $page_title = $inventory_form;
                break;

                case 'inventory list presentations':
		$page = $m . '/inventory/incs/presentations/listPresentations.php';

                $subLinks = "<a href=\"index.php?p=inventory add presentation&m=pharmacy\">Add new presentation</a>";
                $page_title = $inventory_presentations;
                break;

                case 'inventory edit presentation':
		$page = $m . '/inventory/incs/presentations/editPresentation.php';

                $subLinks = "<a href=\"index.php?p=inventory list presentations&m=pharmacy\">".$inventory_presentations."</a>";
                $page_title = $inventory_presentation;
                break;

                case 'inventory add presentation':
		$page = $m . '/inventory/incs/presentations/newPresentationForm.php';

                $subLinks = "<a href=\"index.php?p=inventory list presentations&m=pharmacy\">".$inventory_presentations."</a>";
                $page_title = $inventory_presentations;
                break;

                case 'inventory delete presentation':
		$page = $m . '/inventory/incs/presentations/listPresentations.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_presentations;
                break;

//                case 'inventory add unit':
//                $page = $m. '/inventory/incs/units/newUnitForm.php';
//                $subLinks="<a href=\"index.php?p=inventory list units&m=pharmacy\">".$inventory_item_units."</a>";
//                $page_title = $inventory_unit_add;
//                break;
//
//                case 'inventory list units':
//                $page = $m. '/inventory/incs/units/unitList.php';
//                $subLinks="<a href=\"index.php?p=inventory add unit&m=pharmacy\">".$inventory_add_unit."</a>";
//                $page_title = $inventory_units;
//                break;
//
//                case 'inventory edit unit':
//                $page = $m. '/inventory/incs/units/editUnit.php';
//                $subLinks="<a href=\"index.php?p=inventory list units&m=pharmacy\">".$inventory_units."</a>
//                            <a href=\"index.php?p=inventory add unit&m=pharmacy\">".$inventory_add_unit."</a>";
//                $page_title = $inventory_units;
//                break;

                case 'inventory-add-unit':
                    $page = $m. '/inventory/incs/units/add-inventory-unit.php';
                    $subLinks="<a href=\"index.php?p=inventory-list-units&m=pharmacy\">" . $inventory_item_units . "</a>";
                    $page_title = $inventory_unit_add;
                    if ($_POST && $m == "pharmacy"){
                        if (file_exists("./modules/$m/inventory/incs/units/posthandlers/add-inventory-unit-post.php"))
                            include_once ("./modules/$m/inventory/incs/units/posthandlers/add-inventory-unit-post.php");
                    }
                    break;

                case 'inventory-list-units':
                    $page = $m. '/inventory/incs/units/view-inventory-units.php';
                    $subLinks="<a href=\"index.php?p=inventory-add-unit&m=pharmacy\">" . $inventory_add_unit . "</a>";
                    $page_title = $inventory_units;
                    break;

                case 'inventory-edit-unit':
                    $page = $m . '/inventory/incs/units/edit-inventory-units.php';
                    $subLinks="<a href=\"index.php?p=inventory-list-units&m=pharmacy\">" . $inventory_units . "</a>
                                <a href=\"index.php?p=inventory-add-unit&m=pharmacy\">" . $inventory_add_unit . "</a>";
                    $page_title = $inventory_units;
                    if ($_POST && $m == "pharmacy"){
                        if (file_exists("./modules/$m/inventory/incs/units/posthandlers/edit-inventory-unit-post.php"))
                            include_once ("./modules/$m/inventory/incs/units/posthandlers/edit-inventory-unit-post.php");
                    }
                    break;

                case 'inventory list transfers':
		$page = $m . '/inventory/incs/transfers/listTransfers.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_transfers;
                break;

                case 'inventory edit transfer':
		$page = $m . '/inventory/incs/transfers/editTransfer.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_transfer;
                break;

                case 'inventory add transfer':
		$page = $m . '/inventory/incs/transfers/newTransferForm.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_transfer;
                break;

                case 'inventory delete transfer':
		$page = $m . '/inventory/incs/transfers/listTransfers.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_transfer;
                break;

                case 'inventory list purchaseOrders':
		$page = $m . '/inventory/incs/purchaseOrders/listPurchaseOrders.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_purchase_order;
                break;

                case 'inventory edit purchaseOrder':
		$page = $m . '/inventory/incs/purchaseOrders/editPurchaseOrder.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_purchase_order;
                break;

                case 'inventory add purchaseOrder':
		$page = $m . '/inventory/incs/purchaseOrder/newPurchaseOrderForm.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_purchase_order;
                break;

                case 'inventory delete purchaseOrder':
		$page = $m . '/inventory/incs/purchaseOrder/listPurchaseOrders.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_purchase_order;
                break;

                case 'inventory list strengths':
		$page = $m . '/inventory/incs/strengths/listStrengths.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_strengths;
                break;

                case 'inventory edit strength':
		$page = $m . '/inventory/incs/strengths/editStrength.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_strength;
                break;

                case 'inventory add strength':
		$page = $m . '/inventory/incs/strengths/newStrengthForm.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_strength;
                break;

                case 'inventory delete strength':
		$page = $m . '/inventory/incs/strengths/listStrengths.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_strengths;
                break;

                case 'inventory list rejection reasons':
		$page = $m . '/inventory/incs/rejections/listRejections.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_rejections;
                break;

                case 'inventory edit rejection reason':
		$page = $m . '/inventory/incs/rejections/editRejection.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_rejection;
                break;

                case 'inventory add rejection reason':
		$page = $m . '/inventory/incs/rejections/newRejectionReasonForm.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_rejection_reason;
                break;

                case 'inventory delete rejection reason':
		$page = $m . '/inventory/incs/rejections/listRejections.php';

                //$subLinks = "<a href=\"#\"></a>";
                $page_title = $inventory_rejections;
                break;

                case 'inventory add medicalName':
                $page = $m. '/inventory/incs/medicalNames/newMedicalNameForm.php';
                $page_title=$inventory_new_medical_name;

                $subLinks="<a href=\"index.php?p=inventory list medicalNames&m=pharmacy\">".$inventory_medicalNames."</a>";

                break;

                case 'inventory list medicalNames':
        	$page = $m . '/inventory/incs/medicalNames/listMedicalNames.php';

                $subLinks = "<a href=\"index.php?p=inventory add medicalName&m=pharmacy\">".$inventory_add_new_medicalName."</a>";
                $page_title = $inventory_medicalNames;
                break;

                 case 'inventory delete medicalName':
        	$page = $m . '/inventory/incs/medicalNames/listMedicalNames.php';

                $subLinks = "<a href=\"index.php?p=inventory add medicalName&m=pharmacy\">".$inventory_add_new_medicalName."</a>";
                $page_title = $inventory_medicalNames;
                break;

                case 'inventory edit medicalName':
        	$page = $m . '/inventory/incs/medicalNames/editMedicalName.php';

                $subLinks = "<a href=\"index.php?p=inventory add medicalName&m=pharmacy\">".$inventory_add_new_medicalName."</a><a href=\"index.php?p=inventory list medicalNames&m=pharmacy\">".$inventory_medicalNames."</a>";
                $page_title = $inventory_medical_name;
                break;

                case "inventory_item_transfers":
                $page=$m. '/inventory/incs/transfers/listTransfers.php';
                $page_title=$inventory_transfers;
                break;

                case "inventory_view_transfer":
                $page=$m.'/inventory/incs/transfers/viewTransfer.php';
                $page_title=$inventory_view_transfer_details;
                break;

                case "send_stock_transfer":
                $page=$m.'/inventory/incs/transfers/newTransferForm.php';
                $page_title=$inventory_new_transfer;
                break;


                case "receive_stock_transfer":
                $page=$m.'/inventory/incs/transfers/transfer_receipt_form.php';
                $page_title=$inventory_receive_transfer;
                break;

                case "inventory_edit_transfer":
                $page=$m.'/inventory/incs/transfers/editTransfer.php';
                $page_title=$inventory_edit_transfer;
                break;

            case "inventory_withdraw_transfer":
                $page=$m.'/inventory/incs/transfers/withdrawTransfer.php';
                $page_title=$inventory_withdraw_transfer;
                break;

                case "inventory_delete_transfer":
                $page=$m.'/inventory/incs/transfers/listTransfers.php';
                $page_title=$inventory_transfers;
                break;

                case "inventory_receive_new_stock":
                $page=$m.'/inventory/incs/restock/new_stock_form.php';
                $page_title=$inventory_new_stock_form;
                if ($_POST){
                    //die ("<pre>" . print_r ($_POST, true) . "</pre>");
                    $invStock = new inventory_stock_class();
                    $itemArray = isset($_POST["qty"]) ? $_POST["qty"] : array();
                    $costPrice = isset($_POST["costprice"]) ? $_POST["costprice"] : array();
                    $expiryDate = isset($_POST["expirydate"]) ? $_POST["expirydate"] : array();
                    $locationID = $inventory_locationID;
                    $vendorID = isset($_POST["vendor"]) ? $_POST["vendor"] : 0;

                    $error = $invStock->receiveStock($itemArray, $inventory_locationID, $vendorID, $_SESSION[session_id() . "userID"], $costPrice, $expiryDate);
                    if (!$error){
                        $page = $_SERVER["PHP_SELF"] . "?" . $_SERVER["QUERY_STRING"] . "&c=" . urlencode($inventory_restock_success);
                        header ("Location: $page");
                    } else {
                        $_GET["c"] = $inventory_restock_failure;
                    }
                }
                break;

                case "inventory_list_received_stock":
                $page=$m.'/inventory/incs/restock/list_received_stock.php';
                $page_title=$inventory_received_stock_items;
                break;

                case "view_supply_details":
                $page=$m.'/inventory/incs/restock/supply_details.php';
                $page_title=$inventory_supply_details;
                break;

                case 'inventory_dispense_item':
                    $page=$m.'/inventory/incs/dispense/newDispenseForm.php';
                    $page_title=$inventory_dispense_item;
                    break;

                case 'inventory_dispense_item_act':
                    $page=$m.'/inventory/incs/dispense/dispense-drug-form.php';
                    $page_title=$inventory_dispense_item;
                    if ($_POST && $m == "pharmacy"){
                        if (file_exists("./modules/$m/inventory/incs/dispense/posthandlers/dispense-drug-form-post.php"))
                            include_once ("./modules/$m/inventory/incs/dispense/posthandlers/dispense-drug-form-post.php");
                    }
                    break;

                //Show the details of a dispense action
                case 'inventory_dispense_item_act_details':
                    $page = $m . '/inventory/incs/dispense/dispense-drug-complete-details.php';
                    $page_title = "Dispense Details";
                    break;

                case 'inventory-manage-locations-access':
                    $page = $m . '/inventory/incs/locations/assign-locations-form.php';
                    $page_title = "Assign Users to Locations";
                    if ($_POST && $m == "pharmacy"){
                        if (file_exists("./modules/$m/inventory/incs/locations/posthandlers/assign-locations-form-post.php"))
                            include_once ("./modules/$m/inventory/incs/locations/posthandlers/assign-locations-form-post.php");
                    }
                    break;

                case 'inventory-select-location':
                    $page = $m . '/inventory/incs/locations/select-current-location.php';
                    $page_title = "Select Location to Work With";
                    if ($_POST && $m == "pharmacy"){
                        if (file_exists("./modules/$m/inventory/incs/locations/posthandlers/select-current-location-post.php"))
                            include_once ("./modules/$m/inventory/incs/locations/posthandlers/select-current-location-post.php");
                    }
                    break;

            case 'inventory_manage_reorderLevel':
                $page=$m.'/inventory/incs/items/reorder_level.php';
                $page_title=$inventory_manage_reorder_level;
                break;

            case 'inventory-list-manufacturers':
                $page = 'pharmacy/inventory/incs/manufacturers/view-manufacturers.php';
                $page_title = "List of Manufacturers";//$inventory_manufacturer;
                break;

            case 'inventory-add-manufacturer':
                $page = 'pharmacy/inventory/incs/manufacturers/add-manufacturer.php';
                $page_title = "Add New Manufacturer";//$inventory_manufacturer;
                if ($_POST && $m == "pharmacy"){
                    if (file_exists("./modules/$m/inventory/incs/manufacturers/posthandlers/add-manufacturer-post.php"))
                        include_once ("./modules/$m/inventory/incs/manufacturers/posthandlers/add-manufacturer-post.php");
                }
                break;

            case 'inventory-edit-manufacturer':
                $page = 'pharmacy/inventory/incs/manufacturers/edit-manufacturer.php';
                $page_title = "Edit Manufacturer";//$inventory_manufacturer;
                if ($_POST && $m == "pharmacy"){
                    if (file_exists("./modules/$m/inventory/incs/manufacturers/posthandlers/edit-manufacturer-post.php"))
                        include_once ("./modules/$m/inventory/incs/manufacturers/posthandlers/edit-manufacturer-post.php");
                }
                break;

            /***    END: Pharmacy Inventory    ***/

            case 'formbuilder':
                $page = 'sysadmin/formbuilder.php';
                $page_title = "Form Builder";//$inventory_manufacturer;
                if ($_POST && $m == "sysadmin"){
                    if (file_exists("./modules/$m/formbuilder_post.php")){
                        include_once ("./modules/$m/formbuilder_post.php");
                    }
                }
                break;

            case 'formbuilder-elem-delete':
                $dis_page = './modules/sysadmin/formbuilder-delete.php';
                if (file_exists($dis_page)){
                    include_once ($dis_page);
                }
                break;









		// Default is to include the main page.
		default:
			if ($_POST)
				include_once ("./modules/main.inc_post.php");
			$page = 'main.inc.php';
			$page_title = $home_pageTitle;
			break;

	} // End of main switch.






?>