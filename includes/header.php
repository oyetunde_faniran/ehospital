<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <?php
        echo $page_title;
        ?>
    </title>

    <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/ehospital.css" rel="stylesheet" type="text/css" />
    <link href="css/menu.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/jquery/new_ui/jquery-ui.min.css"/>

<!--    Tweak fro boostrap-->
    <style type="text/css">
        .text-primary {
            color: #080;
        }
        .bg-primary {
            background-color: #080;
        }
        a.bg-primary:hover,
        a.bg-primary:focus {
            background-color: #060;
        }


        .btn-primary {
            background-color: #080;
            border-color: #040;
        }
        .btn-primary:focus,
        .btn-primary.focus {
            color: #fff;
            background-color: #060;
            border-color: #020;
        }
        .btn-primary:hover {
            background-color: #060;
            border-color: #020;
        }
        .btn-primary:active,
        .btn-primary.active,
        .open > .dropdown-toggle.btn-primary {
            background-color: #060;
            border-color: #020;
        }

        .btn-primary .badge {
            color: #080;
            background-color: #fff;
        }
        .label-primary {
            background-color: #080;
        }
        .label-primary[href]:hover,
        .label-primary[href]:focus {
            background-color: #060;
        }

        .list-group-item.active > .badge,
        .nav-pills > .active > a > .badge {
            color: #080;
            background-color: #fff;
        }

        .list-group-item.active,
        .list-group-item.active:hover,
        .list-group-item.active:focus {
            z-index: 2;
            color: #fff;
            background-color: #080;
            border-color: #080;
        }

        .panel-primary {
            border-color: #080;
        }
        .panel-primary > .panel-heading {
            color: #fff;
            background-color: #080;
            border-color: #080;
        }
        .panel-primary > .panel-heading + .panel-collapse > .panel-body {
            border-top-color: #080;
        }
        .panel-primary > .panel-heading .badge {
            color: #080;
            background-color: #fff;
        }
        .panel-primary > .panel-footer + .panel-collapse > .panel-body {
            border-bottom-color: #080;
        }


    </style>


    <script type="text/javascript" src="js/jquery/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="js/jquery/new_ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="DataTables/datatables.js"></script>

    <script type="text/javascript">
        //hack for jquery.browser
        // Limit scope pollution from any deprecated API
        (function() {

            var matched, browser;

            // Use of jQuery.browser is frowned upon.
            // More details: http://api.jquery.com/jQuery.browser
            // jQuery.uaMatch maintained for back-compat
            jQuery.uaMatch = function( ua ) {
                ua = ua.toLowerCase();

                var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
                    /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
                    /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
                    /(msie) ([\w.]+)/.exec( ua ) ||
                    ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
                    [];

                return {
                    browser: match[ 1 ] || "",
                    version: match[ 2 ] || "0"
                };
            };

            matched = jQuery.uaMatch( navigator.userAgent );
            browser = {};

            if ( matched.browser ) {
                browser[ matched.browser ] = true;
                browser.version = matched.version;
            }

            // Chrome is Webkit, but Webkit is also Safari.
            if ( browser.chrome ) {
                browser.webkit = true;
            } else if ( browser.webkit ) {
                browser.safari = true;
            }

            jQuery.browser = browser;

            jQuery.sub = function() {
                function jQuerySub( selector, context ) {
                    return new jQuerySub.fn.init( selector, context );
                }
                jQuery.extend( true, jQuerySub, this );
                jQuerySub.superclass = this;
                jQuerySub.fn = jQuerySub.prototype = this();
                jQuerySub.fn.constructor = jQuerySub;
                jQuerySub.sub = this.sub;
                jQuerySub.fn.init = function init( selector, context ) {
                    if ( context && context instanceof jQuery && !(context instanceof jQuerySub) ) {
                        context = jQuerySub( context );
                    }

                    return jQuery.fn.init.call( this, selector, context, rootjQuerySub );
                };
                jQuerySub.fn.init.prototype = jQuerySub.fn;
                var rootjQuerySub = jQuerySub(document);
                return jQuerySub;
            };

        })();
    </script>


    <?php
    //Stores the list of pages that require JQuery 1.9.1
    $jquery191_array = array(
        'casenote',
        'formbuilder'
    );

    //Stores the list of pages that require JQuery 1.9.1 and does not want the chat page included
    $jquery191_nochat_array = array(
        'formbuilder',
    );

    //    if (in_array($p, $jquery191_array)){
    //        echo '<script type="text/javascript" src="./library/jquery-1.9.1.js"></script>';
    //    } else {
    //        echo '<script type="text/javascript" src="library/admin_jquery/jquery-1.3.2.min.js"></script>';
    //    }

    ?>
        <script   type="text/javascript" src="myajax3.js"></script>

</head>