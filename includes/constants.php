<?php

//The name of the directory where XML & PDF treatment sheet files are stored
define ('FTP_TREATMENT_FILES_DIR', 'ftp-files-asdfhj6rtkaf/');

define ('FTP_TREATMENT_SCRIPT_NAME', 'ftp-treatment-retriever-adfkn87ayir.php');

//The maximum size of file that can be uploaded by users
define ("FILES_MAX_SIZE", "1048576");

//The maximum size of file that can be uploaded by users written in a form that can be easily understood by users
define ("FILES_MAX_SIZE_TEXT", "1MB");

//Allowed File Types
define ("FILES_ALLOWED_TYPES", "JPG, PNG & GIF");

//The user group ID of Billing Officials
define ("BILLING_GROUP_ID", 6);

//The user group ID of Super Admin
define ("SUPER_ADMIN_GROUP_ID", 1);

//The transaction cost been charged by the bank
define ("TRANSACTION_COST", 0);

//The ID of the row that contains the text "TRANSACTION COST" in pat_serviceitem
define ("TRANSACTION_COST_SERVICE_ID", 277);

//The number of characters in hospital numbers
define ("HOSP_NUMBER_XTER_COUNT", 7);

//Sender name for SMS Notifications
define ('SMS_SENDER_NAME', 'e-Hospital');


//Name of organisation to be used as title of Receipts
//    define ('ORGANISATION_NAME', 'Lagos University Teaching Hospital');
define ('ORGANISATION_NAME', 'e-Hospital');