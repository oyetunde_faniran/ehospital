<?php
//    die (dirname(__FILE__) . ' --- ' . __FILE__);
    //$root_dir = isset($root_dir) ? $root_dir : '/home/upltest/public_html/ehospital';
    $root_dir = isset($root_dir) ? $root_dir : '';
	//die ($_SERVER['HTTP_USER_AGENT']);
	//Filter out IE
	if(preg_match("/MSIE/", $_SERVER['HTTP_USER_AGENT'])){
		$msg = "<h2>INTERNET EXPLORER ERROR!!!</h2>
				<p>Sorry! This application can not be accessed with Internet Explorer. Please, use any other web browser. You can download the following
					supported web browsers from our server in less than a minute.
					<ul>
						<li><a href=\"firefox.exe\">Mozilla Firefox</a></li>
						<li><a href=\"google-chrome.exe\">Google Chrome</a></li>
					</ul>
				</p>";
		die ($msg);
	}

	session_start();
	ob_start();

    include_once('database.php');
    include_once('constants.php');


	$idleTime = 60 * 60;	//Maximum idle time allowed for users after which a user must login again
    define ("IDLE_TIME", $idleTime);
/*	$subLinks = "<a href=\"index.php\">Home</a>
					<a href=\"index.php?p=changepass&m=sysadmin\">Change Password</a>
					<a href=\"index.php?p=logout&m=sysadmin\">Log Out</a>";*/

	//Ensure the login flag (session variable) is set
	$_SESSION[session_id() . "luth_loggedin"] = isset($_SESSION[session_id() . "luth_loggedin"]) ? $_SESSION[session_id() . "luth_loggedin"] : false;

	require_once $root_dir . "/functions/autoload.php";
	require_once $root_dir . "/functions/helper.php";

	//Get the actual file name requested
	$all = explode("/", $_SERVER['PHP_SELF']);
	$disPage = end($all);


	//let user log out without other checks***************
	$page = isset($_GET["p"]) ? $_GET["p"] : "";
	$mod = isset($_GET["m"]) ? $_GET["m"] : "";
	if ($page == "logout" && $mod == "sysadmin"){
		//session_destroy();
		header ("Location: logout.php");
	}

	//Ensure that a user is logged in before visiting any page apart from the home page & staff registration page
	$_SESSION[session_id() . "luth_idle"] = isset($_SESSION[session_id() . "luth_idle"]) ? $_SESSION[session_id() . "luth_idle"] : false;

	if (!$_SESSION[session_id() . "luth_loggedin"]){

		//Make all variables in the query string available as individual variables in this current scope
		parse_str($_SERVER['QUERY_STRING']);
		if (
				!(
					(
						($disPage == "" || $disPage == "index.php") && isset($next)	//Is next part of the variables?
					)

					||

					(
						$disPage == "index.php" && stristr($_SERVER['QUERY_STRING'], "p=staffregister&m=sysadmin")
					)

                    ||

					(
						$disPage == "BranchCollectUpdate_URL.php"
					)

                    ||

					(
						$disPage == FTP_TREATMENT_SCRIPT_NAME
					)

					/*||

					(//index.php?p=ajax&m=sysadmin&t=deptclinics&d=4
						$disPage == "index.php" && stristr($_SERVER['QUERY_STRING'], "p=ajax&m=sysadmin&t=deptclinics"
                    )
					)*/
				)
			)
		{
			$cont = urlencode($_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING']);
			header ("Location: index.php?next=$cont");
		}/* else {
				die ($disPage . $_SERVER['QUERY_STRING']);
		}*/

	} else {
			//Force username / password entry if user has been idle for more than $idleTime (= 10 minutes by default)
			$i = isset($_GET["i"]) ? (int)$_GET["i"] : 0;
			// die("--->{$root_dir}");
            $user = new admin_user();
			if (isset($_COOKIE[session_id() . "eldi"])){
				setcookie(session_id() . "eldi", md5(time()), time() + $idleTime);	//Reset cookie
				$_SESSION[session_id() . "luth_idle"] = false;						//Set idle status to false

                //***CHAT STUFF:    Update this log-in session in the users_session table to show that the user is still active and not idle
                $user->loginSession_Update($_SESSION[session_id() . "userID"], false);
			} else {
                $_SESSION[session_id() . "luth_idle"] = true;

                //***CHAT STUFF:    Update this log-in session in the users_session table to show that this user is now idle
                $user->loginSession_Update($_SESSION[session_id() . "userID"], true);
            }

			if ($_SESSION[session_id() . "luth_idle"] && $i <= 0){
				  $cont = urlencode($_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING']);
				  header ("Location: index.php?next=$cont&i=1");
			}


			//Check to be sure that the user is allowed to visit this page | Log this page visit

			$request = explode("/", $_SERVER['REQUEST_URI']);
			$request = end($request);
			$page = admin_user::parseURL($request);



			/***	START AUDIT TRAIL	***/
			//Form the variables to be logged
			$trailer = new admin_AuditTrail($_SESSION[session_id() . "langField"]);
			$logArray["user"] = $_SESSION[session_id() . "userID"];
			if ($_POST){
				$logArray["action"] = $trailer->getActionID(1);

				//Log the content of submitted forms only if they do not contain a username & password
				if (!isset($_POST["username"]) && !isset($_POST["password"]))
					$logArray["post"] = serialize($_POST);
				else $logArray["post"] = "";
			} else {
					$logArray["action"] = $trailer->getActionID(0);
					$logArray["post"] = "";
				}
			$logArray["menuIDs"] = admin_menu::getMenuIDFromP($page["p"], $page["page"]);
			$logArray["url"] = $request;
			/***	END AUDIT TRAIL (Actual Logging occurs outside this comment block)	***/

			//Do log, then allow / deny access to the page
			$allowed = admin_user::isAllowedURL($_SESSION[session_id() . "urlArray"]);

			//4Testing only. This line must be commented
			$allowed = true;

			if (!$allowed){
				/***	START AUDIT TRAIL LOGGING	***/
					$trailer->logVisit($logArray, 0);
				/***	END AUDIT TRAIL LOGGING	***/

				header ("Location: index.php?p=accessdenied&m=sysadmin");

			} else {
					/***	START AUDIT TRAIL LOGGING	***/
						$trailer->logVisit($logArray, 1);
					/***	END AUDIT TRAIL LOGGING	***/
				}
		}




	// Errors are emailed here.
	$contact_email = 'address@example.com';

	//This is used to set the time zone in date_default_timezone_set() to prevent a warning from being displayed when calling function date()
	$timezone = "Africa/Lagos";
	date_default_timezone_set($timezone);

	// Determine whether we're working on a local server
	// or on the real server:
	if (stristr($_SERVER['HTTP_HOST'], 'local') || (substr($_SERVER['HTTP_HOST'], 0, 7) == '192.168')) {
		$local = TRUE;
	} else {
		$local = FALSE;
	}

	// Determine location of files and the URL of the site:
	// Allow for development on different servers.
	if ($local) {

		// Always debug when running locally:
		$debug = TRUE;

		// Define the constants:

		//define ('BASE_URI', '/path/to/html/folder/');
		define ('BASE_URI', 'C:/xampp/htdocs/ehospital');
		//C:\AppServ\www\luth
		define ('BASE_URL',	'http://localhost/ehospital/');
		//define ('DB', '/path/to/mysql.inc.php');

	} else {

		define ('BASE_URI', '/path/to/live/html/folder/');
		define ('BASE_URL',	'http://www.example.com/');
	//	define ('DB', '/path/to/live/mysql.inc.php');

	}

	/*
	 *	Most important setting...
	 *	The $debug variable is used to set error management.
	 *	To debug a specific page, add this to the index.php page:

	if ($p == 'thismodule') $debug = TRUE;
	require_once('./includes/config.inc.php');

	 *	To debug the entire site, do

	$debug = TRUE;

	 *	before this next conditional.
	 */

	// Assume debugging is off.
	if (!isset($debug)) {
		$debug = FALSE;
	}
	# ***** SETTINGS ***** #
	# ******************** #
	//require_once "./functions/autoload.php";

	//set_error_handler (array("Error_handler", "my_error_handler"));


	//user setting
	if ($_SESSION[session_id() . "luth_loggedin"]){
		$a = array( 'user1' => array('id'=>1,'dept_id'=>'General Medicine','user_id'=>1,'user_name'=>'Ademola from Skye'),
					'user2' =>  array('id'=>2,'dept_id'=>'GEC','user_id'=>2,'user_name'=>'Ademola from Skye'),
					'user3' => array('id'=>3,'dept_id'=>'Maternity','user_id'=>3,'user_name'=>'Ademola from Skye') );



		$a1		= "General Medicine";						//dept name of the current user
		$a2		= $_SESSION[session_id() . "userID"];		//userid of the current user
		$deptid	= $_SESSION[session_id() . "deptID"];		//dept id of the current user
		$a3		= $_SESSION[session_id() . "staffName"];	//name of the current user

		//For lab
		$labstaff = $_SESSION[session_id() . "userID"]; //change to $_SESSION['loggedinuser'] later


		$lang_curDB = "lang1";
		$lang_curTB = "language_content";
	}



	//language setting
	$lang = "english";
	if(isset($_GET["lang"])){
        $language = $lang;
	}else{
        $language = "english";
	}
	require_once $root_dir . "/common/lang/".$language.".php";


    //Temporary Value for location ID in the pharmacy inventory module
    $inventory_locationID = isset($_SESSION[session_id() . "inventory_locationID"]) ? $_SESSION[session_id() . "inventory_locationID"] : 0;
    $inventory_locationName = isset($_SESSION[session_id() . "inventory_locationName"]) ? $_SESSION[session_id() . "inventory_locationName"] : "";

?>