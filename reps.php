<?php # Script 2.4 - index.php

	/*
	 *	This is the main page.
	 *	This page includes the configuration file,
	 *	the templates, and any content-specific modules.
	 */

	// Require the configuration file before any PHP code:
	require_once ('./includes/config.inc.php');
	//admin_rights::removeChildrenRights(1);
	//exit();
	// Validate what page to show:
	if (isset($_GET['p'])) {
		$p = $_GET['p'];
	} elseif (isset($_POST['p'])) { // Forms
		$p = $_POST['p'];
	} else {
		$p = NULL;
	}

	if (isset($_GET['m'])) {
		$m = $_GET['m'];
	} elseif (isset($_POST['m'])) { // Modules
		$m = $_POST['m'];
	} else {
		$m = NULL;
	}

	// Determine what page to display:
if ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"]){

	include_once ("./includes/switch_reps.php");

} else {	//END / ELSE if logged in
		//Staff Registration page
		if ($disPage == "index.php" && stristr($_SERVER['QUERY_STRING'], "p=staffregister&m=sysadmin")){
			if ($_POST){
				if (file_exists("./modules/$m/staffregister_post.php"))
					include_once ("./modules/$m/staffregister_post.php");
			}
			$page = $m.'/staffregister.php';
			$page_title = $staffreg_title;// die ("$staffreg_title");
		} else {
			//index page
			if ($_POST)
				include_once ("./modules/main.inc_post.php");
			$page = 'main.inc.php';
			$page_title = $home_pageTitle;
		}
	}

	// Make sure the file exists:
	if (!file_exists('./modules/' . $page)) {
		$page = 'main.inc.php';
		$page_title = 'Welcome To LUTH Hospital Management System';
	}

	include_once ("./includes/header.php");

?>


<body <?php if ($p=="reg_form" || $p=="confirmreg" || $p=="edit_reg" || $p=="casenote") {?> onLoad="init()" <?php } ?>>
<div id="header" class="width"></div>

<!-- BEGIN Top Navigation Bar-->
<!--<div id="nav-bar" class="width">
<?php
	//include_once ("includes/links.php");
?>
</div>-->
<?php
	if ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"]){
		echo $_SESSION[session_id() . "menu"];
	}
?>
<!-- END Top Navigation Bar-->



<!-- BEGIN Div for Main Body Content-->
<div class="content width">

<!-- BEGIN Main Body-->
  <div class="content-column">
        <span class="contentColumnSpan <?php echo ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"] && !empty($subLinks)) ? "" : " splash-width";	//Echo style rule that would stretch the width of this column if there are no sub-links?>"><?php echo $page_title; ?></span>

        <div class="splash-box <?php echo ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"] && !empty($subLinks)) ? "" : " splash-width";	//Echo style rule that would stretch the width of this column if there are no sub-links?>">
              <!--<p>Please Enter your Username and Password below</p>
              <form action="" method="get" class="form" >
                <label>Username</label>
                <input name="username" type="text" />
                <br  />
                <label>Password</label>
                <input name="password" type="password" />
                <div class="welcome-btn-position"><input name="submitt" type="button" value="Sign In" class="btn" /></div>
              </form>-->
<?php
	include_once ("./modules/" . $page);
?>
        </div>
  </div>
<!-- END Main Body-->


<!-- BEGIN Related Links / Sub-Links-->
<?php
	include_once ("includes/sublinks.php");
?>
<!-- END Related Links / Sub-Links-->


</div>
<!-- END Div for Main Body Content-->



<!-- BEGIN Footer-->
<?php
	include_once ("./includes/footer.php");
?>
<!-- END Footer-->
</body>
</html>