<html>
  <head>
    <title>Sketch.js - Simple Canvas-based Drawing for jQuery</title>
    <style type='text/css'>
      body { font-family: "Open Sans", sans-serif; color: #444; }
      h1, h2, h3, h4 { font-family: Yellowtail; font-weight: normal; color: #06a; }

      #wrapper { width: 800px; margin: 0 auto; }

      header { text-align: center; }
      header h1 { font-size: 46px; text-align: center; margin: 15px 10px; }

      article h1 { font-size: 26px; margin: 0 0 15px; }

      article canvas {
        width: 800px;
        height: 300px;
        border: 1px solid #ccc;
      }

      pre.source {
        background: #e5eeee;
        padding: 10px 20px;
        width: 760px;
        overflow-x: auto;
        border: 1px solid #acc;
      }

      code { background: #cff; }

      .tools { margin-bottom: 10px; }
      .tools a {
        border: 1px solid black; height: 30px; line-height: 30px; padding: 0 10px; vertical-align: middle; text-align: center; text-decoration: none; display: inline-block; color: black; font-weight: bold;
      }
    </style>
  </head>
  <body>    
<!--<div class="tools">
  <a href="#colors_sketch" data-download="png" style="float: right; width: 100px;">Download</a>
</div>-->
<canvas id="colors_sketch" width="800" height="300" style="border: 1px solid #900;"></canvas>
<script type="text/javascript" src="./library/jquery-1.9.1.js"></script>
<script type="text/javascript" src="./library/sketch.min.js"></script>
<script type="text/javascript">
  $(function() {
//    $.each(['#f00', '#ff0', '#0f0', '#0ff', '#00f', '#f0f', '#000', '#fff'], function() {
//      $('#colors_demo .tools').append("<a href='#colors_sketch' data-color='" + this + "' style='width: 10px; background: " + this + ";'></a> ");
//    });
//    $.each([3, 5, 10, 15], function() {
//      $('#colors_demo .tools').append("<a href='#colors_sketch' data-size='" + this + "' style='background: #ccc'>" + this + "</a> ");
//    });
    $('#colors_sketch').sketch({defaultSize:1});
  });
  
//  function getCanvasContents(){
//      alert(document.getElementById('colors_sketch').value);
//  }
</script>
<!--<button onclick="getCanvasContents();">Get Contents</button>-->

  </body>
</html>