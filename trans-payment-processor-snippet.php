<span id="pay-button-cont" style="color: #900; font-weight: bold;">
    <strong>PAYMENT MODE:</strong>
    <select name="paymentmode" id="paymentmode" onchange="showPOStidCont();">
        <option value="0">--Select--</option>
        <option value="1" selected="selected">Cash</option>
        <option value="2">POS</option>
        <!--<option value="3">Mobile Money</option>-->
    </select>
    <input name="savereceipt" type="button" value="Process Payment" id="btn" class="btn" onclick="confirmPayment(this.form, 'pay-button-cont');">
    <div id="pos-trans-id-container" style="display: none;">
        <span>POS Transaction ID:</span>
        <input type="text" name="pos_trans_id" id="pos-trans-id" />
    </div>
</span>

<script type="text/javascript" src="js/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
    function confirmPayment(disForm, container){
        payMode = document.getElementById("paymentmode");
        payModeSelected = payMode.options[payMode.selectedIndex].value;
        pos_tid = document.getElementById('pos-trans-id').value;

        //Confirm that a payment method was selected
        if (payModeSelected != 0){
            //If the selected payment method is POS, then the transaction ID generated on the POS must be entered
            if (payModeSelected == 2 && pos_tid == ''){
                alert ('Please enter the transaction ID generated on the POS for this transaction.');
                return 0;
            }

            if (confirm('Are you sure you have collected the required amount from the patient and ready to process this payment?')){
                //Payment Method
                newNode = document.createElement("input");
                newNode.type = "hidden";
                newNode.name = "paymentmode";
                newNode.value = payModeSelected;
                disForm.appendChild(newNode);

                //POS Transaction ID
                newNode = document.createElement("input");
                newNode.type = "hidden";
                newNode.name = "pos_trans_id";
                newNode.value = pos_tid;
                disForm.appendChild(newNode);

                doPay(disForm, container);
            }
        } else alert ("Please, select the mode of payment before trying to process payment.");
    }
	function doPay(disForm, container){
		container = document.getElementById(container);
		container.innerHTML = "<img src='images/ajax-loader.gif' /> Processing Payment...";

		try{
			$.ajax({
			  type: 'POST',
			  url: "trans-up.php",
			  data: $("#" + disForm.id).serialize(),
			  success: function(data){
							if (data != "--1--0--0--1--"){
								container.innerHTML = "<div>Payment Processed Successfully <br />&nbsp;<br />AMOUNT COLLECTED SO FAR TODAY: <u>NGN " + data + "</u>" + '<input style="margin-top: 20px; display: block;" id="print-receipt-trigger" type="button" class="btn" value="Print Receipt" />' + "</div>";
                                $('#print-receipt-trigger').click(function(){
                                    newWindow = window.open("index.php?p=print-invoice&no=<?php echo isset($_GET['pattotal_id']) ? (int)$_GET['pattotal_id'] : 0; ?>", "printInvoice", "status,menubar,height=400,width=350");
                                    newWindow.focus();
                                });
							} else {
									container.innerHTML = "Payment Processing Failed! Please, refresh the page to try again.";
							}
						},
			  dataType: "html"
			});
		} catch (e) {}

	}	//END doPay()


    function showPOStidCont(){
        try {
            payMode = document.getElementById("paymentmode");
            payModeSelected = payMode.options[payMode.selectedIndex].value;
            tid_container = document.getElementById('pos-trans-id-container');
            if (payModeSelected == 2){
                $('#pos-trans-id-container').fadeIn();
            } else {
                $('#pos-trans-id-container').fadeOut();
            }
        } catch (e) {}
    }

</script>