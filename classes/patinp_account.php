<?php
/** Contains variables, methods and other functionalitieds to manage patient admission
 *@author ahmed rufai
 * @package service name
 * Create Date: 26-08-2009
 */

require_once './classes/DBConf.php';

Class patinp_account {

 /**
     *
     * @var array holds array of fields in the patinp_account table
     */
    public $patinp=array() ;
    /**
     * @var array holds values to be stored in the patinp_account fields
       */
    public $patinp2=array() ;
	public $connection;
    public $conn;
	public $patadm_purpose;
	
	 function  __construct() {
     $this->conn = new DBConf();
    }
	
   

    /**
     * Load one row into class vars.
     *
     * @param int $key_row
     * 
     */
	public function Load_from_key($key_row){
	   $sql= "Select *,DATE_FORMAT(patinpacc_date,'%b %e, %Y') 'INITIAL DATE' from patinp_account where patadm_id = \"$key_row\" ";
				$result = $this->conn->execute($sql);
				//echo $sql;
				//exit();
	if($this->conn->hasRows($result)){
		while($row = mysql_fetch_array($result)){
		
			$this->patinpacc_id = $row["patinpacc_id"];
			$this->patadm_id = $row["patadm_id"];
			$this->patinpacc_date = $row["INITIAL DATE"];
			$this->patinpacc_type = $row["patinpacc_type"];
			$this->patadm_purpose = $row["patadm_purpose"];
		}
		return 1;
		//die ("good");
	} else return 0;//die("bad");
	
/*	$hasRow = $retVal;
	if ($hasRow === true)
		die ("o sise");
	elseif ($hasRow === false)
			die ("ko sise");
		else die ("ki lo de");*/
	//die ("<pre>" . print_r($this, 1) . "</pre>");
		//return $retVal;
	}

    public function Load_from_table(){
		$result = $this->conn->execute("Select * from patinp_account");
			}
    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     *
     */
	public function Delete_row_from_key($key_row){
		$this->conn->execute("DELETE FROM patinp_account WHERE patinpacc_id = $key_row");
	}

    /**
     * Update the active class variables on table
     */
	
public function Save_Active_Row($id,$tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("UPDATE drugcategory set drugcat_name = \"$this->drugcat_name\" where drugcat_id = \"$this->drugcat_id\"");*/
		
		try {
		$q=count($this->patinp);
		
		
		//for ($i = 0; $i < $q; $i++) {
//			if($this->patinp[$i]=='langcont_id')
//		//	$qq="UPDATE $langtb set $curLnag_field ='".$this->patinp2[$i]."' where langcont_id = \"$lang_id\"";
//				
//		$this->conn->execute("UPDATE $langtb set $curLnag_field ='".$this->patinp2[$i]."' where langcont_id = \"$lang_id\"");
//
//		}
		
			
	$sql = "UPDATE $tablename SET ";	
	
				$qq=count($this->patinp);
				$q=count($this->patinp);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						//if($this->patinp[$i]=='langcont_id')
//						     $sql.=    $this->patinp[$i] .'="'.$lang_id.'" ';
//						else
         			  		  $sql.=    $this->patinp[$i] .'="'.$this->patinp2[$i].'" ';
			 
					 }
					 else{
					    // if($this->patinp[$i]=='langcont_id')
//						     $sql.=      $this->patinp[$i] .'="'.$lang_id.'" ,';
//						 else
						     $sql.=      $this->patinp[$i] .'="'.$this->patinp2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			$result2 =$this->conn->execute("SELECT * from $tablename");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
	//	echo $sql;
	//exit();
          $has_saved=$this->conn->execute($sql);
		 return $has_saved;
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		
		
		
		
	}

    /**
     * Save the active class vars as a new row on table
     */
	 
	public function Save_Active_Row_as_New($tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("Insert into drugcategory (drugcat_name) values (\"$this->drugcat_name\"");*/
		try {
	
		// server validation begins
					$getLang= new language();		
					$xlp_formfield =$this->patinp;
					$xlp_fieldmessage = array('Please Load Specific Patient ','Please Select Admission Purpose','Please Indicate Date Admission Initiated','Indicate Account Type');
		            $xlp_error=$getLang->xlpildator($xlp_formfield,$xlp_fieldmessage);
					if (!empty($xlp_error[2]))
					throw new Exception;
					
	   //sever validation ends
	   
		$q=count($this->patinp);
	
		for ($i = 0; $i < $q; $i++) {
			if($this->patinp[$i]=='langcont_id'){
			$qq="Insert into $langtb ($curLnag_field) values ('".$this->patinp2[$i]."')";
			//echo $qq;
			    $this->conn->execute($qq);
				$content_id= mysql_insert_id();
			}	
		}

		$sql = "INSERT INTO $tablename SET ";	
	
				$qq=count($this->patinp);
				//$q=count($this->patinp);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->patinp[$i]=='langcont_id')
						     $sql.=    $this->patinp[$i] .'="'.$content_id.'" ';
						else
         			  		  $sql.=    $this->patinp[$i] .'="'.$this->patinp2[$i].'" ';
			 
					 }
					 else{
					     if($this->patinp[$i]=='langcont_id')
						     $sql.=      $this->patinp[$i] .'="'.$content_id.'" ,';
						 else
						     $sql.=      $this->patinp[$i] .'="'.$this->patinp2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			//$sql.=" ,drugtype_order=".$this->getNext_order();
		//echo $sql;
		// exit();
		$result=$this->conn->execute($sql);
				 $list=array();
				if($this->conn->hasRows($result,  0)){
					 $list[3]=true;
					return $list;	 
					}
        } catch (Exception $e) {
		// echo "$xlp_error[2]";
		// exit();
            return $xlp_error;
        }
		
	}
	
	//public function GetSearchFields(){
//		echo 	"<option value='-1'>Select Field</option>";	
//		$result =$this->conn->execute("SELECT * from patinp_account order by patinpacc_id");
//				$i = 0;
//				
//			while ($i < mysql_num_fields($result)) {
// //   echo "Information for column $i:<br />\n";
//	  		  $meta = mysql_fetch_field($result);
//	
// 		   if (!$meta) {
//      //  echo "No information available<br />\n";
//  		  }
//  //  echo "$meta->name";
//  
//		if($meta->name=='langcont_id')
//	echo 	"<option value=". $meta->name.">".'Drug Priority'."</option>";
//   else
//	echo 	"<option value=". $meta->name.">".$meta->name."</option>";
//	   
//	   
//	   
//	   
//	    $i++;
//}
///*mysql_free_result($result);
//	return $keys;*/
//	}
	
/**
     * list all rows from a specified table
     * @param string $curLnag_field
     * @param string $langtb
     */
	
function allrows($curLnag_field,$langtb) {
        
		try {
	 $getlang= new language();
	$db2 = new DBConf();
		 $sql = 'SELECT
*
FROM
patinp_account 
';
$pageindex='patinp';
$pager = new PS_Pagination($db2,$sql,200,10,$pageindex);
$rs = $pager->paginate();
           

           // $res = $this->conn->execute($sql);
			$i=1;
           while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["patinpacc_id"].'" /></td>
       
		<td>'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</td>
		    
        <td>'.'<a href = "./index.php?jj=delete&p=drugtype&id='.$row["patinpacc_id"].'&langid='.$row["langcont_id"].'"></a></td><td>'.'<a href = "./index.php?p=editdrugtype&patinpacc_id='.$row["patinpacc_id"].'&langid='.$row["langcont_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
        } catch (Exception $e) {
	
        }

  
    }
	 /**
     * list rows that match specified criteria
     * @param string $search_field
     * @param string $search_value
     * @param string $curLnag_field
     * @param string $langtb
     */
	function rowSearch($search_field,$search_value,$curLnag_field,$langtb){
		 try {
		  $getlang= new language();
		 $db2 = new DBConf();
		$sql = "Select * from  patinp_account where  ".$search_field."='".$search_value."'" ;
		$res =$this->conn->execute($sql);
		
$pageindex='drug';
$pager = new PS_Pagination($db2,$sql,2,10,$pageindex);
$rs = $pager->paginate();
	  // echo "entered";
	     $i=1;
		while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["drugcat_id"].'" />'.$ii.'</td>
        <td>'.$row["patinpacc_id"].'</td>
		<td>'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</td>
		       
        <td>'.'<a href = "./index.php?jj=delete&p=drugtype&id='.$row["patinpacc_id"] .'"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td><td>'.'<a href = "./index.php?p=editdrugtype&patinpacc_id='.$row["patinpacc_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
		} catch (Exception $e) {
        }
	}
	

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->conn->execute("SELECT patinpacc_id from patinp_account order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["patinpacc_id"];
				$i++;
			}
	return $keys;
	}

    /**
     * Close mysql connection
     */
	public function endpatinp_account(){
		$this->connection->CloseMysql();
	}

}