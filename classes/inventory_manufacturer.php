<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventory_manufacturer
 *
 * @author Tunde
 */
class inventory_manufacturer {

    /**
     * @var Connection object   An object of the Connection class, which handles communication with the DB
     */
    protected $conn;
    /**
     * @var int The ID of the manufacturer under consideration
     */
    protected $manufacturerID;
    /**
     *
     * @var boolean = true whenever an error occurs during any operation
     */
    public $error;
    /**
     *
     * @var string  Contains the error message of the error that occurred during the last operation if any
     */
    public $errorMsg;

    /**
     * Class Constructor
     * @param int $manufacturerID   The ID of the manufacturer under consideration
     */
    function __construct($manufacturerID = 0) {
        $this->conn = new DBConf();
        $this->manufacturerID = (int) $manufacturerID;
        $this->error = false;
        $this->errorMsg = "";
    }   //END __construct



    public function getAllManufacturers(){
        $retVal = array();
        $query = "SELECT *, DATE_FORMAT(inventory_manufacturer_date_created, '%M %d, %Y') 'dateCreated' FROM inventory_manufacturers
                    ORDER BY inventory_manufacturer_name";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $retVal[] = $row;
            }
        }
        return $retVal;
    }   //END getAllManufacturers()





    public function getManufacturer($id){
        $id = (int)$id;
        $query = "SELECT *, DATE_FORMAT(inventory_manufacturer_date_created, '%M %d, %Y') 'dateCreated' FROM inventory_manufacturers
                    WHERE inventory_manufacturer_id = '$id'";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $retVal = mysql_fetch_assoc($result);
        } else $retVal = "";
        return $retVal;
    }   //END getManufacturer()




    public function updateManufacturer($manID, $manName, $manAddress, $manRCNo){
        $manID = (int)$manID;
        $manName = admin_Tools::doEscape($manName, $this->conn);
        $manAddress = admin_Tools::doEscape($manAddress, $this->conn);
        $manRCNo = admin_Tools::doEscape($manRCNo, $this->conn);
        try {
            if (empty($manName)){
                throw new Exception();
            }
            $query = "UPDATE inventory_manufacturers
                        SET inventory_manufacturer_name = '$manName',
                            inventory_manufacturer_address = '$manAddress',
                            inventory_manufacturer_rcno = '$manRCNo'
                        WHERE inventory_manufacturer_id = '$manID'";
            $result = $this->conn->run($query);
            if ($this->conn->hasRows($result)){
                $retVal = true;
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            $retVal = false;
        }
        return $retVal;
    }   //END updateManufacturer()




    public function addManufacturer($manName, $manAddress, $manRCNo, $userID){
        $manName = admin_Tools::doEscape($manName, $this->conn);
        $manAddress = admin_Tools::doEscape($manAddress, $this->conn);
        $manRCNo = admin_Tools::doEscape($manRCNo, $this->conn);
        $userID = (int)$userID;
        try {
            if (empty($manName)){
                throw new Exception();
            }
            $query = "INSERT INTO inventory_manufacturers
                        SET inventory_manufacturer_name = '$manName',
                            inventory_manufacturer_address = '$manAddress',
                            inventory_manufacturer_rcno = '$manRCNo',
                            inventory_manufacturer_date_created = NOW(),
                            user_id = '$userID'";
            $result = $this->conn->run($query);
            if ($this->conn->hasRows($result)){
                $retVal = true;
            } else throw new Exception();
        } catch (Exception $e) {
            $retVal = false;
        }
        return $retVal;
    }   //END addManufacturer()




}   //END class
?>
