<?php
/**
 * Contains nifty tools that are used in a number of other classes in the "Reports" package
 *
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */

/**
 * Contains nifty tools that are used in a number of other classes in the "Reports" package
 *
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */
class report_common{

    /**
     *
     * @var DBConf  An object of the DBConf class
     */
	protected $conn;


/**
 *  Class Constructor
 */
	public function __construct(){
		$this->conn = new DBConf();
	}   //END __construct()



/**
 * Formats $fromdate and $todate for use in a query
 * Expected possible values are All / Today / <Date/Datetime Value>
 * @param string $fromdate  Can contain a valid date in the form YYYY-MM-DD or the strings "All" or "Today"
 * @param string $todate    Can contain a valid date in the form YYYY-MM-DD or the strings "All" or "Today"
 * @return array            Returns the formatted date combined into an array alongside a flag that tells the calling function if the dates should be used as filters or not
 */
	public function formatDate($fromdate, $todate){
		$fromdate = mysql_real_escape_string($fromdate, $this->conn->getConnectionID());
		$todate = mysql_real_escape_string($todate, $this->conn->getConnectionID());
		if (strtolower($fromdate) != "all" && strtolower($todate) != "all"){
			if (strtolower($fromdate) == "today")
				$fromdate = " DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ";
			else $fromdate = "'$fromdate 00:00:00'";

			if (strtolower($todate) == "today")
				$todate = " DATE_FORMAT(NOW(), '%Y-%m-%d 23:59:59')";
			else $todate = "'$todate 23:59:59'";
			$noFilter = false;
		} else $noFilter = true;
		return array ("fromdate" => $fromdate, "todate" => $todate, "noFilter" => $noFilter);
	} //END method formatDate()



/**
 * Gets the patient name with hospital number $id from the registry table
 * @param string $id    The hospital number of the patient under consideration
 * @return string       Returns either the patient name on success or the String "Unknown Patient" on failure
 */
	public function getPatient($id){
		$query = "SELECT CONCAT_WS (' ', reg_surname, reg_othernames) patientname FROM registry
					WHERE reg_hospital_no = '$id'";
		$result = $this->conn->execute ($query);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			$row = mysql_fetch_row($result);
			return $row[0];
		}
		return "Unknown Patient";
	}   //END getPatient()



/**
 * Gets the staff name with employee ID $id form the staff table
 * @param string $id    The employee ID of the staff under consideration
 * @return string       Returns the employee ID on success or the String "Unknown Staff" on failure
 */
	public function getStaff($id){
		$query = "SELECT CONCAT_WS (' ', staff_title, staff_surname, staff_othernames) staffname FROM staff
					WHERE staff_employee_id = '$id'";
		$result = $this->conn->execute ($query);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			$row = mysql_fetch_row($result);
			return $row[0];
		}
		return "Unknown Staff";
	}   //END getStaff()




/**
 * Gets the rows to be displayed as result for a search and sends it to table builder for processing
 * @param string $query The final query that would be used to generate the actual report
 * @return string       The generated HTML containing the report
 */
	public function getResults($query){
		$result = $this->conn->execute($query);
	//Now build the table to be displayed
		//Get the next record to start from
		if (isset($_GET["start"]))
			$start = (int)$_GET["start"];
		else $start = 0;
		$tBuilder = new tableBuilder($query);
		return $tBuilder->buildTable(false, $start);
	}	//END getResults()



/**
 * Adds a selected='selected' attribute to an input control of type option if need be
 * @param string $value     The value that would be compared to the value of $_POST[$field] to know if its the currently-selected item or not
 * @param string $field     The name of the form field under consideration
 * @return string           Returns "selected=\"selected\"" if the value of $_POST[$field] is equal to $value, else ""
 */
	public function selectMe($value, $field){
		if ($_POST && isset($_POST["$field"])){
			if ($value == $_POST["$field"])
				return "selected=\"selected\"";
		} else return "";
	}   //END selectMe()


/**
 * Gets all diagnosis for a patient in a medical transaction with id $id
 * @param int $id   The ID of a patient's current medical transaction in the patient_admission table
 * @return string   Returns HTML containing the diagnosis on success or an empty string on failure
 */
	public function getDiagnosis($id){
		$query = "SELECT d.diagnosis_name 
					FROM diagnosis d INNER JOIN treatment_diagnosis td INNER JOIN treatment t
					ON d.diagnosis_code = td.diagnosis_code AND td.treatment_id = t.treatment_id
					WHERE t.patadm_id = '$id'";// echo "<p>$query</p>";
		$result = $this->conn->execute ($query);
		$final = "";
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			while ($row = mysql_fetch_row($result))
				$final .= "<div>" . $row[0] . "</div>\n";
		}
		return $final;
	}   //END getDiagnosis()




/**
 * Gets all available wards for display in a drop-down box
 * @param string $query     The query that would be used to get the list of wards
 * @param string $postField The name of the field that would contain all wards in the current HTML form
 * @return string           Returns the list of wards with each one enclosed in an <option> tag and related ones enclosed in an <optgroup> tag
 */
	public function getAllWards4DropDown($query, $postField){
		$result = $this->conn->execute ($query);
		$final = "";
		/*$query = "SELECT lc_ward.$curLangField ward, c.clinic_name clinic, w.ward_id 
					FROM language_content lc_ward INNER JOIN wards w INNER JOIN clinic c 
					ON w.langcont_id = lc_ward.langcont_id AND w.clinic_id = c.clinic_id 
					ORDER BY clinic, ward";*/
		if ($result && mysql_affected_rows ($this->conn->getConnectionID()) > 0){
			$lastClinic = "";
			$started = false;
			while ($row = mysql_fetch_array ($result, MYSQL_ASSOC)){
				if ($row["clinic"] == $lastClinic)
					$final .= "<option value=\"{$row['ward_id']}\" " . $this->selectMe($row['ward_id'], $postField) . ">{$row['ward']}</option>\n";
				else {
						if ($started)
							$final .= "</optgroup>";

						$final .= "<optgroup label='" . $row["clinic"] . "'>";
						$final .= "<option value=\"{$row['ward_id']}\" " . $this->selectMe($row['ward_id'], $postField) . ">{$row['ward']}</option>\n";
						$started = true;
						$lastClinic = $row["clinic"];
				}
			}
			$final .= "</optgroup>";
		}
		return $final;
	}   //END getAllWards4DropDown()




/**
 * Gets all available departments for display in a drop-down box
 * @param string $query     The query that would be used to get the list of departments
 * @param string $postField The name of the field that would contain all departments in the current HTML form
 * @return string           Returns the list of departments with each one enclosed in an <option> tag
 */
	public function getAllDepts4DropDown($query, $postField){
		$result = $this->conn->execute ($query);
		$final = "";
		/*$query = "SELECT dept.dept_id, lc.$curLangField dept
					FROM department dept INNER JOIN language_content lc
					ON dept.langcont_id = lc.langcont_id
					ORDER BY dept";*/
		if ($result && mysql_affected_rows ($this->conn->getConnectionID()) > 0){
			while ($row = mysql_fetch_array ($result, MYSQL_ASSOC))
				$final .= "<option value=\"{$row['dept_id']}\" " . $this->selectMe($row['dept_id'], $postField) . ">{$row['dept']}</option>\n";
		}
		return $final;
	}   //END getAllDepts4DropDown()





/**
 * Gets the ID that represents the state "dead" from the conditiontype table
 * @return int  Returns the ID that represents the state "dead" from the conditiontype table
 */
	public function getDeadID(){
		return 5;
		//Get the ct_id from conditiontype table that indicates that a patient is dead
		$condQuery = "SELECT ct.ct_id 
						FROM conditiontype ct INNER JOIN language_content lc
						ON ct.langcont_id = lc.langcont_id
						WHERE LOWER(lc.lang1) = 'dead'";
		$result = $this->conn->execute($condQuery);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) == 1){
			$row = mysql_fetch_row($result);
			$dead = $row[0];
		} else $dead = 0;
		return $dead;
	}   //END getDeadID()




/**
 * Generates the list of months for display in a drop-down box
 * @param int $d    The ID of the currently selected month in the drop-down box
 * @return string   The generated HTML appropriate for display in a drop-down box
 */
	public function getMonthsDropDown($d=1){
		$monthsArray = array (
							"January",
							"February",
							"March",
							"April",
							"May",
							"June",
							"July",
							"August",
							"September",
							"October",
							"November",
							"December"
						);
		$retVal = "";
		foreach ($monthsArray as $k=>$v){
			$k++;
			if ($k == $d)
				$retVal .= "<option value=\"$k\" selected=\"selected\">$v</option>\n";
			else $retVal .= "<option value=\"$k\">$v</option>\n";
		}
		return $retVal;
	}   //END getMonthsDropDown()




/**
 * Gets the name of a month from the number representing the month (1-12)
 * @param int $index    The number (1 - 12) representing the month
 * @return string       Returns the name of the month or the string "[Unknown Month]" if the supplied index is not in the range 1..12
 */
	public function getMonth ($index){
		$monthsArray = array (
							"January",
							"February",
							"March",
							"April",
							"May",
							"June",
							"July",
							"August",
							"September",
							"October",
							"November",
							"December"
						);
		if (array_key_exists($index-1, $monthsArray))
			return $monthsArray[$index - 1];
		else return "[Unknown Month]";
	}   //END getMonth()



/**
 * Creates a drop-down list of years
 * @param int $disYear  The current year
 * @param int $start    The year to start the display from
 * @return string       The generated HTML containing the years enclosed in <option> tags
 */
	public function getYearsDropDown($disYear, $start=1950){
		$retVal = "";

		for ($year = $start; $year<$disYear; $year++)
			$retVal .= "<option value=\"$year\">$year</option>\n";

		$retVal .= "<option value=\"$disYear\" selected=\"selected\">$disYear</option>\n";

		return $retVal;
	}   //END getYearsDropDown()




/**
 * Gets the number of days in a particular month of a specified year
 * @param int $year     The year under consideration
 * @param int $month    The month under consideration
 * @return int          The number of days in the specified month or 0 on failure
 */
	public function getDaysInMonth($year, $month){
		$disDate = "$year-$month-01";
		$query = "SELECT DAY(LAST_DAY('$disDate'))";
		$result = $this->conn->execute ($query);
		if ($this->conn->hasRows($result, 1)){
			$row = mysql_fetch_row($result);
			$retVal = $row[0];
		} else $retVal = 0;
		return $retVal;
	}   //END getDaysInMonth()



/**
 * Formats a string for use in the header of a report
 * @param string $s The string to be formatted
 * @return string   Returns the formatted string
 */
	public function reportHeader($s){
		return $s;//str_replace (" ", " \n", $s);
	}   //END reportHeader()


}   //END class
?>