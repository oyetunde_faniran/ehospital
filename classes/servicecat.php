<?php
/** Contains variables, methods and other functionalitieds to manage service category
 *@author ahmed rufai
 * @package service name
 * Create Date: 26-08-2009
 */
require_once './classes/DBConf.php';

Class servicecat {
    /**
     *
     * @var array holds array of fields in the service category table
     */
    public $scat=array() ;
    /**
     * @var array holds values to be stored in the service category table field
       */
    public $scat2=array() ;
	public $servicecat_id; //int(10)
	public $langcont_id; //int(10)
	public $connection;

    public	$conn;
	
/**
 * 
 * class constructor
 */
function  __construct() {
     $this->conn = new DBConf();
    }

  /**
     * Load one row into class variables, using specified row key
     *
     * @param int $key_row
     *
     */
	public function New_servicecat($langcont_id){
		$this->langcont_id = $langcont_id;
	}

    /**
     * Load one row into class variables, using specified row key
     *
     * @param int $key_row
     *
     */
	public function Load_from_key($key_row){
		$result = $this->conn->execute("Select * from servicecat where servicecat_id = \"$key_row\" ");
		while($row = mysql_fetch_array($result)){
			$this->servicecat_id = $row["servicecat_id"];
			$this->langcont_id = $row["langcont_id"];
		}
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     *
     */
	public function Delete_row_from_key($lang_id,$key_row,$langtb){
		$this->conn->execute("DELETE FROM drugcategory WHERE drugcat_id = $key_row");
		$this->conn->execute("DELETE FROM $langtb WHERE langcont_id = $lang_id");
	}
     

    /**
     * Update the active row on table
     */
	public function Save_Active_Row($lang_id,$id,$tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("UPDATE drugcategory set drugcat_name = \"$this->drugcat_name\" where drugcat_id = \"$this->drugcat_id\"");*/
		
		try {
		$q=count($this->scat);
		
		
		for ($i = 0; $i < $q; $i++) {
			if($this->scat[$i]=='langcont_id')
		//	$qq="UPDATE $langtb set $curLnag_field ='".$this->scat2[$i]."' where langcont_id = \"$lang_id\"";
				
		$this->conn->execute("UPDATE $langtb set $curLnag_field ='".$this->scat2[$i]."' where langcont_id = \"$lang_id\"");

		}
		
			
	$sql = "UPDATE $tablename SET ";	
	
				$qq=count($this->scat);
				$q=count($this->scat);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->scat[$i]=='langcont_id')
						     $sql.=    $this->scat[$i] .'="'.$lang_id.'" ';
						else
         			  		  $sql.=    $this->scat[$i] .'="'.$this->scat2[$i].'" ';
			 
					 }
					 else{
					     if($this->scat[$i]=='langcont_id')
						     $sql.=      $this->scat[$i] .'="'.$lang_id.'" ,';
						 else
						     $sql.=      $this->scat[$i] .'="'.$this->scat2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			$result2 =$this->conn->execute("SELECT * from $tablename");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
		//echo $sql;
	//exit();
         $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		
		
		
		
	}

    /**
     * Save the active class variables as a new row on table
     */
	public function Save_Active_Row_as_New($tablename,$curLnag_field,$langtb){
		
		try {
		$q=count($this->scat);
		
		
		for ($i = 0; $i < $q; $i++) {
			if($this->scat[$i]=='langcont_id'){
			$qq="Insert into $langtb ($curLnag_field) values ('".$this->scat2[$i]."')";
			//echo $qq;
			    $this->conn->execute($qq);
				$content_id= mysql_insert_id();
			}	
		}
		
		$sql = "INSERT INTO $tablename SET ";	
	
				$qq=count($this->scat);
				//$q=count($this->scat);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->scat[$i]=='langcont_id')
						     $sql.=    $this->scat[$i] .'="'.$content_id.'" ';
						else
         			  		  $sql.=    $this->scat[$i] .'="'.$this->scat2[$i].'" ';
			 
					 }
					 else{
					     if($this->scat[$i]=='langcont_id')
						     $sql.=      $this->scat[$i] .'="'.$content_id.'" ,';
						 else
						     $sql.=      $this->scat[$i] .'="'.$this->scat2[$i].'" ,';
			
					 }
  				 $q--;
 			}
		//echo $sql;
		//exit();
		 $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		
		
		
		
	}
	public function GetSearchFields(){
	echo 	"<option value='-1'>Select Field</option>";	
		$result =$this->conn->execute("SELECT * from servicecat order by servicecat_id");
				$i = 0;
				
while ($i < mysql_num_fields($result)) {
 //   echo "Information for column $i:<br />\n";
    $meta = mysql_fetch_field($result);
	
    if (!$meta) {
      //  echo "No information available<br />\n";
    }
  //  echo "$meta->name";
  
if($meta->name=='langcont_id')
	echo 	"<option value=". $meta->name.">".'Service Category'."</option>";
   else
	echo 	"<option value=". $meta->name.">".$meta->name."</option>";
	   
	   
	   
	   
	    $i++;
}
/*mysql_free_result($result);
	return $keys;*/
	}

/**
     * list all rows from a specified table
     * @param string $curLnag_field
     * @param string $langtb
     */
function allrows($curLnag_field,$langtb) {
        
		try {
	 $getlang= new language();
	$db2 = new DBConf();
		 $sql = 'SELECT
*
FROM
servicecat
';
$pageindex='scat';
$pager = new PS_Pagination($db2,$sql,2,10,$pageindex);
$rs = $pager->paginate();
           

           // $res = $this->conn->execute($sql);
			$i=1;
           while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["servicecat_id"].'" /></td>
        <td>'.$row["servicecat_id"].'</td>
		<td>'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</td>
		       
        <td>'.'<a href = "./index.php?jj=delete&p=scat&id='.$row["servicecat_id"].'&langid='.$row["langcont_id"].'"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td><td>'.'<a href = "./index.php?p=editscat&servicecat_id='.$row["servicecat_id"].'&langid='.$row["langcont_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
        } catch (Exception $e) {
	
        }

  
    }
	/**
 * populate options with servicecat name highlighting the option that matches supplied ID
  * @param string $curLnag_field
     * @param string $langtb
 *
 */
	function getScat2($id,$curLnag_field,$langtb) {
        try {
		    $getLang= new language();
			$sql2 = 'select servicecat_id from servicecat  where servicecat_id ='.$id;
			
				 $res2 = $this->conn->execute($sql2);
				 if( $row2 = mysql_fetch_array($res2))
				  $id2=$row2['servicecat_id'];
		
            $sql = 'select servicecat_id,langcont_id from servicecat ';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
			$sel= ($id2==$row['servicecat_id'])? "selected":" ";
			
                echo '<option value ="'.$row['servicecat_id'] .'">'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {


        }
    }
/**
 * populate options with servicecat name
  * @param string $curLnag_field
     * @param string $langtb
 *
 */
	
function getScat($curLnag_field,$langtb) {
        try {
		    $getLang= new language();
            $sql = 'select servicecat_id,langcont_id from servicecat';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                echo '<option value ="'.$row['servicecat_id'] .'">'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
               }
        } catch (Exception $e) {


        }
    }
	/**
     * list rows that match specified criteria
     * @param string $search_field
     * @param string $search_value
     * @param string $curLnag_field
     * @param string $langtb
     */
	function rowSearch($search_field,$search_value,$curLnag_field,$langtb){
		 try {
		  $getlang= new language();
		 $db2 = new DBConf();
		$sql = "Select * from  servicecat  where  ".$search_field."='".$search_value."'" ;
		$res =$this->conn->execute($sql);
		
$pageindex='scat';
$pager = new PS_Pagination($db2,$sql,2,10,$pageindex);
$rs = $pager->paginate();
	  // echo "entered";
	     $i=1;
		while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["servicecat_id"].'" />'.$ii.'</td>
        <td>'.$row["servicecat_id"].'</td>
		<td>'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</td>
		       
        <td>'.'<a href = "./index.php?jj=delete&p=scat&id='.$row["servicecat_id"] .'"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td><td>'.'<a href = "./index.php?p=editscat&servicecat_id='.$row["servicecat_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
		} catch (Exception $e) {
        }
	}
 


    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->conn->execute("SELECT servicecat_id from servicecat order by $column $order");
			while($row =mysql_fetch_array($result)){
				$keys[$i] = $row["servicecat_id"];
				$i++;
			}
	return $keys;
	}

    /**
     * Close mysql connection
     */
	public function endservicecat(){
		$this->conn->CloseMysql();
	}

}