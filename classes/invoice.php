<?php
/**
 * Invoice generation for various categories of services, laboratory, Drug dispensing, Admission charges	e.t.c,methods to returm transaction history and Webpay parameters
 * @package Billing
 *
 * @author ahmed rufai
 *
 * Create Date: 7-11-2009
 *
 *

 *
 */
//echo ("About to include");
include ('library/lab/checks.php');

Class invoice {

    /**
     * @var array $sname  array of value of service names as prescribed by the doctor
     */
    public $sname = array(); //
    /**
     * @var array $sname2 array  of service names field as appeared in the service table of the database
     */
    public $sname2 = array(); //
    /**
     * @var array $sername_id  row ID of service rendered  as prescribed by the doctor 

     */
    public $sername_id; //
    /**
     * @var array $langcont_id row ID of the text in the Language table

     */
    public $langcont_id; //int(10) unsigned
    /**
     * @var array $langcont_id current DB Connection handle

     */
    public $conn;

    /**
     * onstructors for initial settings
     */
    function __construct() {
        $this->conn = new DBConf();
    }

    /**
     * New object to the class. Don�t forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New(); 
     * @param int $langcont_id The row ID of the required Text in the Language Table
     */
    public function New_service_name($langcont_id) {
        $this->langcont_id = $langcont_id;
    }

    /**
     *  loads IDs of both servicename and equivalent language text in the laguage table
     * @param integer $key_row row ID of a particular record from service_name table
     */
    public function Load_from_key($key_row) {
        $result = $this->conn->execute("Select * from service_name where sername_id = \"$key_row\" ");
        while ($row = mysql_fetch_array($result)) {
            $this->sername_id = $row["sername_id"];
            $this->langcont_id = $row["langcont_id"];
        }
    }

    /**
     * Stores transaction details for laboratory services. To use the vars use for exemple echo $class->insertInvoice4service();
     * @param array $_POST form post array
     * @param array $theitemamounts collection of item amounts for current transaction
     * @param array $theitemids collection of item IDs for current transaction
     * @param array $theitemnames  collection of item names for current transaction
     * @param string $curLnag_field column in the language table that contains current language text
     * @param string $langtb  language table 
     * @return array returns an array containing ID of row that stores current transaction total in the pat_transtotal table and a boolean true if the condition is true or returns a single false if it is not true
     */
    public function insertInvoice4service($_POST, $cnote_id, $theitemamounts, $thepatitem_paymentsource, $theitemids, $theitemnames, $theserv_price1, $sumoftheamount1, $sumoftheamount, $curLnag_field, $langtb) {


        try {
            $hospital = new patient_admission;
            $service = new service();

            $patadm_id = $_POST['patadm_id'];
            $department_id = $hospital->getdepartment_id2($_POST['hosp_id'], $curLnag_field, $langtb);

            $transHandler = new TransactionHandler();
            $inserted = $transHandler->insertTransactionTotal($_POST['patadm_id'], $_POST['hosp_id'], $_POST['transno'], $sumoftheamount, $sumoftheamount1, $_POST['paystatus'], $_POST['pattotal_invoice_type'], $_POST['pattotal_paymthd'], $_POST['deptid'], $_POST['clinic_id'], $_POST['user_id']);

//				   $sql_total = "INSERT INTO pat_transtotal SET
//						pattotal_date=now(),
//						patadm_id='".$_POST['patadm_id']."',
//						reg_hospital_no='".$_POST['hosp_id']."',
//                        pattotal_totalamt='".$sumoftheamount."',
//						pattotal_totalamt_nhis='".$sumoftheamount1."',
//						pattotal_transno='".$_POST['transno']."',
//						pattotal_status='".$_POST['paystatus']."',
//						pattotal_invoice_type='".$_POST['pattotal_invoice_type']."',
//						pattotal_paymthd='".$_POST['pattotal_paymthd']."',
//						dept_id='".$_POST['deptid']."',
//						clinic_id='".$_POST['clinic_id']."',
//						user_id='".$_POST['user_id']."'";
//				//die("$sql_total");
//				 //
//				 // exit();
//				   $restotal = $this->conn->execute($sql_total);
            if ($inserted) {
                $thispat_total_id = $transHandler->getInsertID();
                //echo "hh".$thispat_total_id;
                $patservice_type = $_POST['patservice_type'];

                // $sql = " UPDATE patinp_account SET
//		   		
//     			patinpacc_amount='".$_POST['patinpacc_amount']."'
//				WHERE patadm_id='$patadm_id'";
//					
//					  $res = $this->conn->execute($sql);

                $subcount = count($theitemamounts);
                //echo "theitemamounts".$subcount;
                //exit();
                //print_r($theitemids);
                //exit();
                for ($no = 1; $no <= $subcount; $no++) {

                    $sql_patservice = "INSERT INTO pat_serviceitem SET
						patservice_type='$patservice_type',
						patservice_itemid='$theitemids[$no] ',
						patservice_name='$theitemnames[$no]'
						 ";
                    // echo $sql_patservice ;

                    $respservice = $this->conn->execute($sql_patservice);
                    $thispat_serviceitem[$no] = mysql_insert_id();
                }

                $resFinal = $transHandler->insertTransactionItems($theitemamounts, $theserv_price1, $thispat_serviceitem, $thepatitem_paymentsource, $thispat_total_id);

                /*for ($no = 1; $no <= $subcount; $no++) {
                    //$theserviceIDs[$no] = $_POST["chkID$no"];
                    //$theDiscounts[$no] =$_POST["discount$no"];
                    //$thediscountamts[$no] =$_POST["discountamt$no"];
                    $theitemamount = $theitemamounts[$no];
                    $theitemamount1 = $theserv_price1[$no];
                    $thepatitem_paysrc = $thepatitem_paymentsource[$no];
                    //$theitemids =$theitemids[$no];
                    $theitemname = $theitemnames[$no];

                    $transHandler->insertTransactionItems($theitemamount, $theitemamount1, $thispat_serviceitem[$no], $thepatitem_paysrc, $thispat_total_id);

//						$sql_pat_transitem = "INSERT INTO pat_transitem SET
//						patitem_amount='$theitemamount',
//                        patitem_amount_nhis='$theitemamount1',
//						patservice_id='$thispat_serviceitem[$no]',
//                        patitem_paymentsource='$thepatitem_paysrc',
//						pattotal_id='$thispat_total_id'
//						 ";
//					//  die("$sql_pat_transitem"); $sql_pat_transitem ;
//
//					$resFinal = $this->conn->execute($sql_pat_transitem);
                }*/
                //exit();
                //	exit();
                //for ($no = 1; $no <= $subcount; $no++) {
//					
//					$item_no=$servicename->getItem_no($theitemids[$no],$curLnag_field,$langtb);
//					$sql2 = " UPDATE casenote_labtest SET 
//									
//						casenote_labtest.payflag='".$item_no."'
//						WHERE casenote.patadm_id='$patadm_id' AND casenote_labtest.casenote_id=casenote.casenote_id AND casenote_labtest.payflag=0";
//							echo $sql2;
//							exit();
//					}
                if ($resFinal) {

                    for ($no = 1; $no <= $subcount; $no++) {
                        //$item_no=$service->getItem_no($theitemids[$no],$curLnag_field,$langtb);
                        $serviceId_comma_separated = implode(",", $theitemids);

                        $sql2 = " UPDATE casenote_labtest,casenote SET	casenote_labtest.casenote_payflag='1',pattotal_transno='" . $_POST['transno'] . "'
							WHERE casenote.patadm_id='$patadm_id' AND 
							casenote_labtest.casenote_id=casenote.casenote_id AND casenote_labtest.caselabreq_id IN ($cnote_id)
							  ";
                        //die ("<pre>$sql2</pre>");
                        $res = $this->conn->execute($sql2);
                        // die($sql2);
                    }

                    //exit();

                    $list[1] = true;
                    $list[2] = $thispat_total_id;
                    return $list;
                }else
                    return false;
            }
        } catch (Exception $e) {
            
        }
    }

    /**
     *    Stores transaction details for Drugs Dispensing services. To use the vars use for exemple echo $class->insertInvoice4drug();
     * @param array $_POST form post array
     * @param array $thepatitem_paymentsource collection of whom is responsible for payments e.g by nhis,by retainer,by payment
     * @param integer $subcount count of invoice items
     * @param array $thedrug_price1 collection of drug item price for current transaction
     * @param float $sumoftheamount1 sum of item amounts for NHIS items current transaction
     * @param float $sumoftheamount  sum of item amounts for PATIENT current transaction
     * @param array $thedrugqty      collection of drug qty for current transaction
     * @param array $thedrug_price   collection of drug item unit price for current transaction
     * @param array $thedrugid      collection of drug item ID for current transaction
     * @param array $thedrug_desc    collection of drug item DESC for current transaction
     * @param string $curLnag_field column in the language table that contains current language text
     * @param string $langtb  language table
     * @return array returns an array containing ID of row that stores current transaction total in the pat_transtotal table and a boolean true if the condition is true or returns a single false if it is not true

     */
    public function insertInvoice4drug($_POST, $thepatitem_paymentsource, $subcount, $thedrug_price1, $sumoftheamount1, $sumoftheamount, $thedrugqty, $thedrug_price, $thedrugid, $thedrug_desc, $curLnag_field, $langtb) {
        try {
//                    echo ("$_POST,$thepatitem_paymentsource,$subcount,$thedrug_price1,$sumoftheamount1,$sumoftheamount, $thedrugqty,$thedrug_price,$thedrugid,$thedrug_desc,$curLnag_field,$langtb");
//                    echo ('<p>$_POST,$thepatitem_paymentsource,$subcount,$thedrug_price1,$sumoftheamount1,$sumoftheamount, $thedrugqty,$thedrug_price,$thedrugid,$thedrug_desc,$curLnag_field,$langtb');
//                   die  ("<pre>" . print_r ($thedrugqty, true) . "</pre>");
            $servicename = new service();
            $hospital = new patient_admission;


            // $department_id=$hospital->getdepartment_id2($_POST['hosp_id'],$curLnag_field,$langtb);
            $patadm_id = $_POST['patadm_id'];
            $_POST = admin_Tools::doEscape($_POST, $this->conn);

            $transHandler = new TransactionHandler();
            $inserted = $transHandler->insertTransactionTotal($_POST['patadm_id'], $_POST['hosp_id'], $_POST['transno'], $sumoftheamount, $sumoftheamount1, $_POST['paystatus'], $_POST['pattotal_invoice_type'], $_POST['pattotal_paymthd'], $_POST['deptid'], $_POST['clinic_id'], $_POST['user_id']);

//				   $sql_total = "INSERT INTO pat_transtotal SET
//						pattotal_date=now(),
//						patadm_id='".$_POST['patadm_id']."',
//						reg_hospital_no='".$_POST['hosp_id']."',
//						pattotal_transno='".$_POST['transno']."',
//						pattotal_totalamt='".$sumoftheamount."',
//						pattotal_totalamt_nhis='".$sumoftheamount1."',
//						pattotal_status='".$_POST['paystatus']."',
//						pattotal_invoice_type='".$_POST['pattotal_invoice_type']."',
//						pattotal_paymthd='".$_POST['pattotal_paymthd']."',
//						dept_id='".$_POST['deptid']."',
//						clinic_id='".$_POST['clinic_id']."',
//						user_id='".$_POST['user_id']."'";
//
//                         $restotal = $this->conn->execute($sql_total);
            if ($inserted) {
                //die ("Total Inserted");
                $thispat_total_id = $transHandler->getInsertID();
                $patservice_type = $_POST['patservice_type'];


                $subcount = count($thedrugid);

                for ($no = 1; $no <= $subcount; $no++) {

                    $sql_patservice = "INSERT INTO pat_serviceitem SET
                            patservice_type='$patservice_type',
                            patservice_itemid='$thedrugid[$no] ',
                            patservice_name='$thedrug_desc[$no]'
                             ";


                    $respservice = $this->conn->execute($sql_patservice);
                    $thispat_serviceitem[$no] = mysql_insert_id();
                }


                $resFinal = $transHandler->insertTransactionItems($thedrug_price, $thedrug_price1, $thispat_serviceitem, $thepatitem_paymentsource, $thispat_total_id, $thedrugqty);
                /* for ($no = 1; $no <= $subcount; $no++) {

                  $theitemamount1 =$thedrug_price1[$no];
                  $theitemamount =$thedrug_price[$no];
                  $thepatitem_paysrc =$thepatitem_paymentsource[$no];
                  $theitemname =$thedrug_desc[$no];

                  $transHandler->insertTransactionItems($theitemamount, $theitemamount1, $thispat_serviceitem[$no], $thepatitem_paysrc, $thispat_total_id, $thedrugqty[$no]);

                  //						$sql_pat_transitem = "INSERT INTO pat_transitem SET
                  //						patitem_amount='$theitemamount ',
                  //						patitem_amount_nhis='$theitemamount1 ',
                  //						patitem_totalqty='$thedrugqty[$no]',
                  //						patservice_id='$thispat_serviceitem[$no]',
                  //						patitem_paymentsource='$thepatitem_paysrc',
                  //						pattotal_id='$thispat_total_id'
                  //						 ";
                  //
                  //                        $resFinal = $this->conn->execute($sql_pat_transitem);
                  } */


                if ($resFinal) {
                    for ($no = 1; $no < $subcount; $no++) {

                        $sql2 = " UPDATE casenote_prescription,casenote SET
									
						casenote_prescription.payflag='1'
							WHERE casenote.patadm_id='$patadm_id' AND 
							casenote_prescription.casenote_id=casenote.casenote_id 
							  ";

                        $res = $this->conn->execute($sql2);
                    }



                    $list[1] = true;
                    $list[2] = $thispat_total_id;

                    return $list;
                }
            } //else die ("Total NOT Inserted");
            //}
        } catch (Exception $e) {
            
        }
    }

    /**
     *
     * @param array $_POST form post array
     * @param array $thepatitem_paymentsource collection of whom is responsible for payments e.g by nhis,by retainer,by payment
     * @param integer $subcount count of invoice items
     * @param array $theitemamounts1 sum of patient admission item amounts for NHIS items current transaction
     * @param array $theitemamounts  sum of patient admission item amounts for patient current transaction
     * @param array $theitemids    collection of admission item IDs for current transaction
     * @parama array $theitemnames collection of patient item names for current transaction
     * @param string $curLnag_field column in the language table that contains current language text
     * @param string $langtb  language table
     * @return array returns an array containing ID of row that stores current transaction total in the pat_transtotal table and a boolean true if the condition is true or returns a single false if is not true
     *
     */
    public function insertInvoice($_POST, $thepatitem_paymentsource, $subcount, $theitemamounts1, $theitemamounts, $theitemids, $theitemnames, $curLnag_field, $langtb) {
        try {
            $servicename = new service();
            $patadm_id = $_POST['patadm_id'];
            $hospital = new patient_admission;
            $total_amt_nhis = $_POST['patinpacc_amount'] * .1;
            $department_id = $hospital->getdepartment_id2($_POST['hosp_id'], $curLnag_field, $langtb);

            $transHandler = new TransactionHandler();
            $inserted = $transHandler->insertTransactionTotal($_POST['patadm_id'], $_POST['hosp_id'], $_POST['transno'], $_POST['patinpacc_amount'], $total_amt_nhis, $_POST['paystatus'], $_POST['pattotal_invoice_type'], $_POST['pattotal_paymthd'], $department_id, $_POST['clinic_id'], $_POST['user_id']);

//				   $sql_total = "INSERT INTO pat_transtotal SET
//						pattotal_date=now(),
//						patadm_id='".$_POST['patadm_id']."',
//						reg_hospital_no='".$_POST['hosp_id']."',
//						pattotal_transno='".$_POST['transno']."',
//						pattotal_totalamt='".$_POST['patinpacc_amount']."',
//						pattotal_totalamt_nhis='".$total_amt_nhis."',
//						pattotal_status='".$_POST['paystatus']."',
//						pattotal_invoice_type='".$_POST['pattotal_invoice_type']."',
//						pattotal_paymthd='".$_POST['pattotal_paymthd']."',
//						dept_id='".$department_id."',
//						clinic_id='".$_POST['clinic_id']."',
//						user_id='".$_POST['user_id']."'";
//				 // echo $sql_total;
//				 //
//				 // exit();
//				   $restotal = $this->conn->execute($sql_total);
            if ($inserted) {
                $thispat_total_id = $transHandler->getInsertID();

                $patservice_type = $_POST['patservice_type'];

                $sql = " UPDATE patinp_account SET
		   		
     			patinpacc_amount='" . $_POST['patinpacc_amount'] . "'
				WHERE patadm_id='$patadm_id'";

                $res = $this->conn->execute($sql);

                for ($no = 1; $no <= $subcount; $no++) {

                    $sql_patservice = "INSERT INTO pat_serviceitem SET
						patservice_type='$patservice_type',
						patservice_itemid='$theitemids[$no] ',
						patservice_name='$theitemnames[$no]'
						 ";
                    // echo $sql_patservice ;

                    $res = $this->conn->execute($sql_patservice);
                    $thispat_serviceitem[$no] = mysql_insert_id();
                }

                
                $amount = array();
                for ($no = 1; $no <= $subcount; $no++) {
                    $amount[] = $_POST["itemamount$no"];
                }
                $resFinal = $transHandler->insertTransactionItems($amount, $theitemamounts1, $thispat_serviceitem, $thepatitem_paymentsource, $thispat_total_id);

                /*for ($no = 1; $no <= $subcount; $no++) {
                    $theserviceIDs[$no] = $_POST["chkID$no"];
                    $theitemamounts1[$no] = $theitemamounts1[$no];
                    $theitemamounts[$no] = $_POST["itemamount$no"];
                    $theitemids[$no] = $_POST["itemid$no"];
                    $theitemnames[$no] = $_POST["itemname$no"];
                    $thepatitem_paysrc = $thepatitem_paymentsource[$no];


						$sql_pat_transitem = "INSERT INTO pat_transitem SET
					
						patitem_amount_nhis='$theitemamounts1[$no] ',
						patitem_amount='$theitemamounts[$no] ',
						patservice_id='$thispat_serviceitem[$no]',
						patitem_paymentsource='$thepatitem_paysrc',
						pattotal_id='$thispat_total_id'
						 ";
					  // echo $sql_pat_transitem ;

					$resFinal = $this->conn->execute($sql_pat_transitem);
                }*/

                //exit();
                if ($resFinal) {
                    $list[1] = true;
                    $list[2] = $thispat_total_id;
                    return $list;
                } else
                    return false;
            }
        } catch (Exception $e) {

        }
    }

    // function getdrug_name($id,$curLnag_field,$langtb) {
//        try {
//		  $getLang= new language();
//           $sql = 'Select * from service_name  where sername_id='.$id;
//            $res = $this->conn->execute($sql);
//            $row = mysql_fetch_array($res);
//			//echo $sql ;
//		return	$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
//        //    return $row['drug_desc'];
//				 
//            
//			
//        } catch (Exception $e) {
//
//
//        }
//    }

    /**
     * Delete the row by using the key as arg
     *
     * @param integer $key_row
     *
     */
    public function Delete_row_from_key($key_row) {
        $this->conn->execute("DELETE FROM service_name WHERE sername_id = $key_row");
    }

    /**
     * Update the active row  on table
     * @param integer $lang_id The row ID of the required Text in the Language Table
     * @param integer $id The row ID of the transaction to update
     * @param string $tablename the  table to update
     * @param string $curLnag_field column in the language table that contains current language text
     * @param string $langtb  language table
     */
    public function Save_Active_Row($lang_id, $id, $tablename, $curLnag_field, $langtb) {
        try {
            $q = count($this->sname);

            for ($i = 0; $i < $q; $i++) {
                if ($this->sname[$i] == 'langcont_id')
                //	$qq="UPDATE $langtb set $curLnag_field ='".$this->sname2[$i]."' where langcont_id = \"$lang_id\"";
                    $this->conn->execute("UPDATE $langtb set $curLnag_field ='" . $this->sname2[$i] . "' where langcont_id = \"$lang_id\"");
            }


            $sql = "UPDATE $tablename SET ";

            $qq = count($this->sname);
            $q = count($this->sname);

            for ($i = 0; $i < $qq; $i++) {
                //$this->sname[$i].'-'.$this->sname2[$i];


                if (($q - 1) == 0) {
                    if ($this->sname[$i] == 'langcont_id') {
                        $sql.= $this->sname[$i] . '="' . $lang_id . '" ';
                    }
                    else
                        $sql.= $this->sname[$i] . '="' . $this->sname2[$i] . '" ';
                }
                else {
                    if ($this->sname[$i] == 'langcont_id')
                        $sql.= $this->sname[$i] . '="' . $lang_id . '" ,';
                    else
                        $sql.= $this->sname[$i] . '="' . $this->sname2[$i] . '" ,';
                }

                $q--;
            }
            //echo $sql;
            //exit();
            $result2 = $this->conn->execute("SELECT * from $tablename");
            $i = 0;
            while ($i < mysql_num_fields($result2)) {
                $meta = mysql_fetch_field($result2);
                if ($meta->primary_key == 1)
                    $key1 = $meta->name;
                $i++;
            }
            $sql.="  WHERE  " . $key1 . "=" . $id;
            //echo $sql;
            //exit();
            $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values ' . $e->getMessage();
        }
    }

    /**
     * Save the active var class as a new row on table
     * @param string $tablename table where new row will be stored
     * @param string $curLnag_field column in the language table that contains current language text
     * @param string $langtb  language table
     */
    public function Save_Active_Row_as_New($tablename, $curLnag_field, $langtb) {
        /* $this->conn->execute("Insert into drugcategory (drugcat_name) values (\"$this->drugcat_name\""); */
        try {
            $q = count($this->sname);


            for ($i = 0; $i < $q; $i++) {
                if ($this->sname[$i] == 'langcont_id') {
                    $qq = "Insert into $langtb ($curLnag_field) values ('" . $this->sname2[$i] . "')";
                    //echo $qq;
                    $this->conn->execute($qq);
                    $content_id = mysql_insert_id();
                }
            }


            $sql = "INSERT INTO $tablename SET ";

            $qq = count($this->sname);
            //$q=count($this->sname);

            for ($i = 0; $i < $qq; $i++) {


                if (($q - 1) == 0) {
                    if ($this->sname[$i] == 'langcont_id')
                        $sql.= $this->sname[$i] . '="' . $content_id . '" ';
                    else
                        $sql.= $this->sname[$i] . '="' . $this->sname2[$i] . '" ';
                }
                else {
                    if ($this->sname[$i] == 'langcont_id')
                        $sql.= $this->sname[$i] . '="' . $content_id . '" ,';
                    else
                        $sql.= $this->sname[$i] . '="' . $this->sname2[$i] . '" ,';
                }
                $q--;
            }
            //	$sql.=" ,drugtype_order=".$this->getNext_order();

            $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values ' . $e->getMessage();
        }
    }

    /**
     * Populate select option tag with search field
     */
    public function GetSearchFields() {
        echo "<option value='-1'>Select Field</option>";
        $result = $this->conn->execute("SELECT * from service_name order by sername_id");
        $i = 0;

        while ($i < mysql_num_fields($result)) {
            //   echo "Information for column $i:<br />\n";
            $meta = mysql_fetch_field($result);

            if (!$meta) {
                //  echo "No information available<br />\n";
            }
            //  echo "$meta->name";

            if ($meta->name == 'langcont_id')
                echo "<option value=" . $meta->name . ">" . 'service name' . "</option>";
            else
                echo "<option value=" . $meta->name . ">" . $meta->name . "</option>";




            $i++;
        }
        /* mysql_free_result($result);
          return $keys; */
    }

    /**
     *
     * @param integer $id
     * @param string $curLnag_field column in the language table that contains current language text
     * @param string $langtb  language table
     * @return string returns service item name
     */
    function getdrug_name($id, $curLnag_field, $langtb) {
        try {
            $getLang = new language();
            $sql = 'Select * from service_name  where sername_id=' . $id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
            //echo $sql ;
            return $getLang->getlang_content($row['langcont_id'], $curLnag_field, $langtb);
            //    return $row['drug_desc'];
        } catch (Exception $e) {

        }
    }

    /**
     *
     * @param integer $id the current transaction Number
     * @return string  returns corresponding medical transaction ID for supply trans number
     */
    function getPatadm_id($id) {
        try {

            $sql = "Select patadm_id from pat_transtotal where pattotal_transno='$id'";
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
            //echo $sql ;
            return $row['patadm_id'];
            //    return $row['drug_desc'];
        } catch (Exception $e) {

        }
    }

    /**
     *
     * @param integer $id  transaction Number
     * @return integer returns invoice type for the supplied trans number e.g 1 for admission,2 for drug
     */
    function getInvoicetype($id) {
        try {

            $sql = "Select pattotal_invoice_type from pat_transtotal where pattotal_transno='$id'";
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
            //echo $sql ;
            return $row['pattotal_invoice_type'];
            //    return $row['drug_desc'];
        } catch (Exception $e) {

        }
    }

    /**
     *
     * @param integer $pattotal_id row ID of a particular transaction total in the pat_transtotal table
     * @return float returns sum of discounts
     */
    function getDiscount($pattotal_id) {
        try {
            $sql = "Select sum(patitem_discount) as total_discounts from   pat_transitem pitem inner join pat_serviceitem pservice
		   on
		 
       	   pitem.patservice_id=pservice.patservice_id  
		   where pitem.pattotal_id='$pattotal_id'";
            $res = $this->conn->execute($sql);
            if ($res) {

                $row = mysql_fetch_array($res);

                return $row['total_discounts'];
            }
        } catch (Exception $e) {

        }
    }

    /**
     * @param array $_POST
     * @return array returns an array containing ID of row that stores current transaction total in the pat_transtotal table and a boolean true if the condition is true or returns a single false if is not true
     */
    function updatePattotal($_POST) {
        try {
            $pattotal_id = $_POST['pattotal_id'];
            $hosp_id = $_POST['hosp_id'];
            $patadm_id = $_POST['patadm_id'];
            $sql2 = " UPDATE pat_transtotal SET
		   		pattotal_receiptdate='" . $_POST['pattotal_receiptdate'] . "',
		   	    pattotal_receiptno='" . $_POST['transno'] . "',
				pattotal_tellerno='" . $_POST['pattotal_tellerno'] . "',
				pattotal_acctno='" . $_POST['pattotal_acctno'] . "',
				pattotal_status='" . $_POST['pay_status'] . "',
				pattotal_receiptmanual='" . $_POST['pattotal_receiptmanual'] . "'
				WHERE pattotal_id='$pattotal_id'";
            //echo $sql2;
            //exit();

            $resUpdate1 = $this->conn->execute($sql2);
            if ($this->conn->hasRows($resUpdate1, 0)) {

                $sql = " UPDATE patinp_account SET
		   		
		   	    pattrans_no='" . $_POST['transno'] . "'
				
				WHERE patadm_id='$patadm_id'";
                $res = $this->conn->execute($sql);
                $list[1] = true;
                $list[2] = $pattotal_id;
                $list[3] = $hosp_id;
                return $list;
            }else
                return false;
        } catch (Exception $e) {

        }
    }

    function updatePattotal4receipt($_POST) {
        try {

            $pattotal_id = $_POST['pattotal_id'];
            $hosp_id = $_POST['hosp_id'];
            $patadm_id = $_POST['patadm_id'];
            $sql2 = " UPDATE pat_transtotal SET
		   		pattotal_receiptdate=now(),
				pattotal_status='1',
				pattotal_paymthd='" . $_POST['pattotal_paymthd'] . "',
		   	    pattotal_receiptno='" . $_POST['transno'] . "'
				WHERE pattotal_id='$pattotal_id'";
            //echo $sql2;
            // exit();

            $resUpdate1 = $this->conn->execute($sql2);
            if ($this->conn->hasRows($resUpdate1, 0)) {


                $list[1] = true;
                $list[2] = $pattotal_id;
                $list[3] = $hosp_id;
                return $list;
            }else
                return false;
        } catch (Exception $e) {

        }
    }

    /**
     *
     * @param integer $id row ID of a particular transaction total in the  pat_transtotal table
     * @return array of values relating to the ID
     */
    function getTotalpay($id) {
        try {
            $getLang = new language();
            $sql = "Select *, DATE_FORMAT(pattotal_date,'%b %e, %Y') 'TRANSACTION DATE', MONTH(pattotal_date) 'TRANSACTION MONTH', DAY(pattotal_date) 'TRANSACTION DAY', YEAR(pattotal_date) 'TRANSACTION YEAR' FROM pat_transtotal WHERE pattotal_id='$id'";
            $res = $this->conn->execute($sql);
            // echo $sql;
            if ($res) {
                $row = mysql_fetch_array($res);
                return $row;
            }
        } catch (Exception $e) {

        }
    }

    /**
     *
     * @param integer $id row ID of a particular transaction total in the  pat_transtotal table
     * @return array of values relating to the ID
     */
    function getTotalpay2($id) {
        try {
            $hospital = new patient_admission();
            $med_id = $hospital->getCurrentmedical_trans_id($id);
            $sql = "Select * from pat_transtotal where patadm_id='$med_id'";
            $res = $this->conn->execute($sql);
            // echo $sql;
            if ($res) {

                $row = mysql_fetch_array($res);

                return $row;
            }
        } catch (Exception $e) {

        }
    }

    /**
     *
     * @param string $id transaction no of a particular transaction total in the  pat_transtotal table
     * @return array of values relating to the ID
     */
    function getTotalpay3($id) {
        try {
            //$hospital= new patient_admission();
            //$med_id=$hospital->getCurrentmedical_trans_id($id);
            $sql = "Select * from pat_transtotal where pattotal_transno='$id'";
            $res = $this->conn->execute($sql);
            // echo $sql;
            if ($res) {

                $row = mysql_fetch_array($res);

                return $row;
            }
        } catch (Exception $e) {

        }
    }

    /**
     *
     * @param string $hosp_id hospital Number
     * @param integer $invoice_type invoice type
     * @return array of values relating to the supplied hospital Number
     */
    function getPaymentHistory($hosp_id, $invoice_type) {
        try {
            $patinp = new patinp_account();
            $hospital = new patient_admission();
            $inp = new inpatient_admission();
            $invoice = new invoice();
            $wards = new wards();
//$Numword= new Numword();
            $servicename = new service();
            $invoice_type = $invoice_type;

//$hosp_id=$_GET['hosp_id'];
//$invoice=$invoice->getTotalpay2($hosp_id);

            $Dbconnect = new DBConf();
            $med_id = $hospital->getCurrentmedical_trans_id($hosp_id);
            if ($invoice_type == 1) {
                $sql = "SELECT * , DATE_FORMAT(pattotal_date,'%b %e, %Y') 'TRANSACTION DATE'
FROM pat_transtotal
WHERE patadm_id='$med_id'
AND pattotal_invoice_type='1'
ORDER BY  `pat_transtotal`.`pattotal_transno` DESC 
LIMIT 0 , 3";
                $res20 = $Dbconnect->execute($sql);
            }
            if ($invoice_type == 2) {
                $sql = "SELECT * , DATE_FORMAT(pattotal_date,'%b %e, %Y') 'TRANSACTION DATE'
FROM pat_transtotal
WHERE patadm_id='$med_id'
AND pattotal_invoice_type='2'
ORDER BY  `pat_transtotal`.`pattotal_transno` DESC 
LIMIT 0 , 3";
                $res20 = $Dbconnect->execute($sql);
            }
            if ($invoice_type == 4) {
                $sql = "SELECT * , DATE_FORMAT(pattotal_date,'%b %e, %Y') 'TRANSACTION DATE'
FROM pat_transtotal
WHERE patadm_id='$med_id'
AND pattotal_invoice_type='4'
ORDER BY  `pat_transtotal`.`pattotal_transno` DESC 
LIMIT 0 , 3";

                $res20 = $Dbconnect->execute($sql);
            }

            $cnt2 = 1;
            $rows = NULL;
            while ($rowservicename = mysql_fetch_array($res20)) {

                //$row = mysql_fetch_array($res);

                $rows[$cnt2] = $rowservicename;
                //print_r($rows[$cnt2]);
                $cnt2++;
            }

            //exit();
            return $rows;
        } catch (Exception $e) {

        }
    }

    /**
     *
     * @param integer $id current medical transaction ID
     * @return array returns a row from pat_transtotal table that corresponds to suuplied medical trans ID
     */
    function confirmPay($id) {
        try {
            $getLang = new language();
            $sql = "Select pattotal_status from pat_transtotal where patadm_id='$id'";
            // echo $sql;
            //exit();
            $res = $this->conn->execute($sql);
            if ($res) {

                $row = mysql_fetch_array($res);

                return $row['pattotal_status'];
            }
        } catch (Exception $e) {

        }
    }

    /**
     *
     * @param string $id transaction number
     * @return string hospital number corresponding to the supplied trans number
     */
    function gethosp_no($id) {
        try {
            //$getLang= new language();
            $sql = "Select reg_hospital_no from pat_transtotal where pattotal_transno='$id'";
            // echo $sql;
            //exit();
            $res = $this->conn->execute($sql);
            if ($res) {

                $row = mysql_fetch_array($res);

                return $row['reg_hospital_no'];
            }
        } catch (Exception $e) {

        }
    }

    /**
     *
     * @param string $id row ID of a particular transaction total in the  pat_transtotal table
     * @return string transaction number corresponding to the supplied row ID
     */
    function getTrans_no($id) {
        try {
            //$getLang= new language();
            $sql = "Select pattotal_transno from pat_transtotal where pattotal_id='$id'";
            // echo $sql;
            //exit();
            $res = $this->conn->execute($sql);
            if ($res) {

                $row = mysql_fetch_array($res);

                return $row['pattotal_transno'];
            }
        } catch (Exception $e) {

        }
    }

    /* function getlang_content($id,$curLnag_field,$langtb) {
      try {
      $sql = 'Select '.$curLnag_field.'  from '. $langtb.'  where langcont_id='.$id;
      $res = $this->conn->execute($sql);
      $row = mysql_fetch_array($res);
      return $row[$curLnag_field];
      } catch (Exception $e) {


      }
      } */

    /**
     * populates option tags of a specified select element with service names and highlight one corresponding to the supplied servicename ($id)ID
     * @param integer $id service ID to a particular service name or description
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     */
    function getservice_name2($id, $curLnag_field, $langtb) {
        try {
            $row2 = array();
            $getLang = new language();
            $sql2 = 'select sername_id from service_name  where sername_id =' . $id;

            $res2 = $this->conn->execute($sql2);
            if ($res2) {
                $row2 = mysql_fetch_array($res2);
                $id2 = $row2['sername_id'];
            }

            $sql = 'select sername_id,langcont_id from service_name';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                $sel = ($id2 == $row['sername_id']) ? "selected" : " ";
                echo '<option value ="' . $row['sername_id'] . '">' . $getLang->getlang_content($row['langcont_id'], $curLnag_field, $langtb) . '</option>';
            }
        } catch (Exception $e) {

        }
    }

    /**
     * populates option tags of a specified select element with service names
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     */
    function getservice_name($curLnag_field, $langtb) {
        try {
            $getLang = new language();
            $sql = 'select sername_id,langcont_id from service_name';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                echo '<option value ="' . $row['sername_id'] . '">' . $getLang->getlang_content($row['langcont_id'], $curLnag_field, $langtb) . '</option>';
            }
        } catch (Exception $e) {

        }
    }

    /**
     * *
     *
     */
    function getTestId_4paidItem_array($transno) {
        try {
            $serviceId = null;
            $sql = "Select pattotal_id, reg_hospital_no from pat_transtotal where pattotal_transno='$transno'";

            $res = $this->conn->execute($sql);
            if ($res) {
                $row = mysql_fetch_array($res);
                $pattotal_id = $row['pattotal_id'];
                $hospital_id = $row['reg_hospital_no'];
                $sql3 = "SELECT pt.patservice_id, ps.patservice_itemid FROM pat_transitem  pt INNER JOIN  pat_serviceitem  ps
				ON pt.patservice_id=ps.patservice_id
				WHERE pt.pattotal_id='$pattotal_id'";

                $res3 = $this->conn->execute($sql3);
                $sn = 0;
                while ($row3 = mysql_fetch_array($res3)) {
                    $serviceId[$sn] = $row3['patservice_itemid'];
                    $sn++;
                }

                $serviceId_comma_separated = implode(",", $serviceId);


                $sql4 = "SELECT DISTINCT pa.reg_hospital_no, cl.casenote_id , cl.caselabreq_testid
						FROM casenote_labtest cl
						INNER JOIN casenote c
						INNER JOIN patient_admission pa 
						ON cl.casenote_id = c.casenote_id
						AND c.patadm_id = pa.patadm_id
						WHERE cl.pattotal_transno='$transno' ";
                //die ("<pre>$sql4</pre>");
                //$res4=mysql_query($sql4);
                $res4 = $this->conn->execute($sql4);
                $sn2 = 0;
                while ($row4 = mysql_fetch_array($res4)) {
                    $testID[$sn2++] = $row4['caselabreq_testid'];
                }
            }

            //die(print_r($testID, true));
            return $testID;
        } catch (Exception $e) {

        }
    }

    /**
     *
     * @param string $curLnag_field column in the language table that contains current language text
     * @param string $langtb  language table
     */
    function allrows($curLnag_field, $langtb) {

        try {
            $getlang = new language();
            $db2 = new DBConf();
            $sql = 'SELECT
*
FROM
service_name 
';
            $pageindex = 'servicename';
            $pager = new PS_Pagination($db2, $sql, 10, 10, $pageindex);
            $rs = $pager->paginate();


            // $res = $this->conn->execute($sql);
            $i = 1;
            while ($row = mysql_fetch_array($rs)) {

                // $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
                echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="' . $row["sername_id"] . '" /></td>
       
		<td>' . $getlang->getlang_content($row['langcont_id'], $curLnag_field, $langtb) . '</td>
		    
        <td>' . '<a href = "./index.php?jj=delete&p=servicename&id=' . $row["sername_id"] . '&langid=' . $row["langcont_id"] . '"></a></td><td>' . '<a href = "./index.php?p=editservicename&sername_id=' . $row["sername_id"] . '&langid=' . $row["langcont_id"] . '"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
                $i++;
            }
?>
            <tr>
                <td  colspan="5"><?php echo $pager->renderFullNav(); ?></td>
            </tr></table>
<?
        } catch (Exception $e) {

        }
    }

    /**
     * indicates whether an invoice has been raised over a particular medical transaction ($patadm_id) in respect of admission charges
     * @param integer $patadm_id current medical transaction ID
     * @return boolean returns true or false
     */
    function hasinvoiced($patadm_id) {
        try {
            $sql = "SELECT  patinpacc_amount FROM  patinp_account WHERE
					     patadm_id='$patadm_id' AND patinpacc_amount <=0";
            $res = $this->conn->execute($sql);

            //echo $sql;
            //exit();
            if ($this->conn->hasRows($res, 1)) {
                // if($res){
                $row = mysql_fetch_array($res);
                //$cnt=mysql_num_rows($res);
//			
//			$patinpacc_amount=$row['patinpacc_amount'];
//			if($patinpacc_amount=="")
                return true;
            }else
                return false;
        } catch (Exception $e) {

        }
    }

    /**
     * indicates whether an invoice has been raised over a particular medical transaction ($patadm_id) in respect of laboratory services
     * @param integer $patadm_id current medical transaction ID
     * @return boolean returns true or false
     */
    function hasinvoicedservice($patadm_id) {
        try {

            $sql = "SELECT * FROM casenote_labtest inner join casenote
									ON 
						casenote_labtest.casenote_id=casenote.casenote_id
							WHERE casenote.patadm_id='$patadm_id'  
							 AND casenote_labtest.casenote_payflag='0'";
            $res = $this->conn->execute($sql);

            //echo $sql;
            //exit();
            if ($this->conn->hasRows($res)) {
                $row = mysql_fetch_array($res);
                //$cnt=mysql_num_rows($res);
//			
//			$patinpacc_amount=$row['patinpacc_amount'];
//			if($patinpacc_amount=="")
                return true;
            }else
                return false;
        } catch (Exception $e) {

        }
    }

    /**
     *
     * indicates whether payment was made on invoice over a particular medical transaction ($patadm_id) in respect of admission services
     * @param integer $patadm_id current medical
     *
     */
    function haspaid($patadm_id) {
        try {
            $sql = "SELECT  pattrans_no FROM  patinp_account WHERE
					     patadm_id='$patadm_id' ";
            $res = $this->conn->execute($sql);
            if ($res) {
                $row = mysql_fetch_array($res);
                $cnt = mysql_num_rows($res);
                $pattrans_no = $row['pattrans_no'];
                if (empty($pattrans_no))
                    return false;
                else
                    return true;
            }
        } catch (Exception $e) {

        }
    }

    /**
     * Returns NHIS plan ID
     * @param string $hosp_id specific hospital number of which the NHIS plan ID will be returned
     * @return integer NHIS plan ID
     */
    function nhis_covered($hosp_id) {
        try {
            $nhisplan_id = NULL;

            $sql_nhis = "SELECT rn.nhisplan_id FROM  registry r
							INNER JOIN registry_nhis rn ON  r.reg_id=rn.reg_id 
							WHERE  r.reg_hospital_no='$hosp_id'
							";
            $result_nhis = $this->conn->execute($sql_nhis);

            if ($this->conn->hasRows($result_nhis, 1)) {

                $row_plan_id = mysql_fetch_array($result_nhis);
                $nhisplan_id = $row_plan_id['nhisplan_id'];
            }
            //die($nhisplan_id );
            return $nhisplan_id;
        } catch (Exception $e) {

        }
    }

    /**
     * Returns rows that match specified criteria
     * @param string $search_field field in the database table
     * @param string $search_value value to search for
     * @param string $curLnag_field column in the language table that contains current language text
     * @param string $langtb  language table
     */
    function rowSearch($search_field, $search_value, $curLnag_field, $langtb) {
        try {
            $getlang = new language();
            $db2 = new DBConf();
            $sql = "Select * from  service_name where  " . $search_field . "='" . $search_value . "'";
            $res = $this->conn->execute($sql);

            $pageindex = 'servicename';
            $pager = new PS_Pagination($db2, $sql, 2, 10, $pageindex);
            $rs = $pager->paginate();
            // echo "entered";
            $i = 1;
            while ($row = mysql_fetch_array($rs)) {

                // $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
                echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="' . $row["drugcat_id"] . '" />' . $ii . '</td>
        <td>' . $row["sername_id"] . '</td>
		<td>' . $getlang->getlang_content($row['langcont_id'], $curLnag_field, $langtb) . '</td>
		       
        <td>' . '<a href = "./index.php?jj=delete&p=servicename&id=' . $row["sername_id"] . '"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td><td>' . '<a href = "./index.php?p=editservicename&sername_id=' . $row["sername_id"] . '"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
                $i++;
            }
?>
            <tr>
                <td  colspan="5"><?php echo $pager->renderFullNav(); ?></td>
            </tr></table>
<?
        } catch (Exception $e) {

        }
    }

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
    public function GetKeysOrderBy($column, $order) {
        $keys = array();
        $i = 0;
        $result = $this->conn->execute("SELECT sername_id from service_name order by $column $order");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["sername_id"];
            $i++;
        }
        return $keys;
    }

    /**
     * Close mysql connection
     */
    public function endservice_name() {
        $this->connection->CloseMysql();
    }

    
    
    //Added by Faniran, Oyetunde
    //$idType should contain either "trans-id" for pattotal_id or "custom-trans-id" for pattotal_transno
    public function getTransaction($transID, $idType = "custom-trans-id"){
        $column = $idType == "trans-id" ? "pattotal_id" : "pattotal_transno";
        $transID = admin_Tools::doEscape($transID, $this->conn);
        $query = "SELECT ptt.*, pttx.*
                    FROM pat_transtotal ptt LEFT JOIN pat_transtotal_extra pttx
                    ON ptt.pattotal_id = pttx.pattotal_id
                    WHERE ptt." . $column . "= '$transID'";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $retVal = mysql_fetch_assoc($result);
        } else $retVal = "";
        return $retVal;
    }   //END getTransaction()
    
    
    
    //$idType should contain either "trans-id" for pattotal_id or "custom-trans-id" for pattotal_transno
    public function getTransactionDetails($transID, $idType = "custom-trans-id"){
        $column = $idType == "trans-id" ? "pattotal_id" : "pattotal_transno";
        $transID = admin_Tools::doEscape($transID, $this->conn);
        $query = "SELECT psi.patservice_name 'item', pti.*, ptix.*
                    FROM pat_transitem pti
                        LEFT JOIN pat_transitem_extra ptix ON pti.pattrans_id = ptix.pattrans_id
                        INNER JOIN pat_serviceitem psi ON pti.patservice_id = psi.patservice_id
                        INNER JOIN pat_transtotal ptt ON pti.pattotal_id = ptt.pattotal_id
                    WHERE ptt." . $column . " = '$transID'";
        $result = $this->conn->run($query);
        $retVal = array();
        if ($this->conn->hasRows($result)){
            while($row = mysql_fetch_assoc($result)){
                $retVal[] = $row;
            }
        }
        return $retVal;
    }   //END getTransaction()
    
    
    
    //$idType should contain either "trans-id" for pattotal_id or "custom-trans-id" for pattotal_transno
    public function updateServiceRenderedFlag($transID, $idType = "custom-trans-id"){
        $column = $idType == "trans-id" ? "pattotal_id" : "pattotal_transno";
        $transID = admin_Tools::doEscape($transID, $this->conn);
        $query = "UPDATE pat_transtotal
                    SET pattotal_servicerendered = '1'
                    WHERE $column = '$transID'";
        $result = $this->conn->run($query);
    }
    
    
    
}