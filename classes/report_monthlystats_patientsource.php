<?php
/**
 * Handles "In-Patient Unit-by-Unit Analysis" report
 * 
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */

/**
 * Handles "In-Patient Unit-by-Unit Analysis" report
 *
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */
class report_monthlystats_patientsource{
    /**
     * @var object Connection object
     */
	public $conn;

    /**
     * @var object report_common class object
     */
	protected $common;

    /**
     * @var int The number of the month for which the report would be generated
     */
	protected $month;

    /**
     * @var int The year for which the report would be generated
     */
	protected $year;


/**
 * Class constructor
 *
 * @param int $month    The number of the month for which the report would be generated (01 - 12)
 * @param int $year     The year for which the report would be generated
 */
	public function __construct($month, $year){
		$this->conn = new DBConf();
		$this->common = new report_common();
		$this->month = $month;
		$this->year = $year;
		$this->days = $this->common->getDaysInMonth($year, $month);
		$this->periodStart = "$year-$month-01 00:00:00";
		$this->periodEnd = "$year-$month-" . $this->days . " 23:59:59";
	}   //END __construct()




/**
 * Forms an array of values from the result set of running the query stored in $query
 *
 * @param string $query     The query to be executed to form the array
 * @return array            Returns the generated array with the following elements "ref_id" => "patients"
 */
	protected function getArray($query){
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			//Store all in an array
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$newArray[$row["refid"]] = $row["patients"];
		} else $newArray = array ();
		return $newArray;
	}   //END getArray()
	




/**
 * Gets an array containing the number of new patients for each referrer category
 * @param string $type     Can be either "in" or "out" for inpatient and outpatient respectively
 * @return array           The value returned by getArray()
 */
	protected function getNewArray($type){
		$table_inPeriod = "temp_inperiod_" . $type;
		$table_notInPeriod = "temp_notinperiod_" . $type;
		$query = "SELECT COUNT(intable.patadm_id) patients, intable.refferer_id refid FROM $table_inPeriod intable
					WHERE intable.patadm_id NOT IN
					(
						SELECT * FROM $table_notInPeriod
					)
					GROUP BY intable.refferer_id";
		return $this->getArray($query);
	}   //END getNewArray()



/**
 * Gets an array containing the total number of patients for each referrer category
 * @param string $type     Can be either "in" or "out" for inpatient and outpatient respectively
 * @return array           The value returned by getArray()
 */
	protected function getTotalArray($type){
		$table = "temp_inperiod_" . $type;
		$query = "SELECT COUNT(patadm_id) patients, refferer_id refid 
					FROM $table
					GROUP BY refferer_id";
		return $this->getArray($query);
	}   //END getTotalArray()




/**
 * Generates the actual report to be displayed to the user
 * 
 * @param string $sourceQuery   The query to get the list of sources of referrals alongside their IDs
 * @param string $totalInWords  The string "Total" in the currently selected language
 * @return                      The generated HTML for the report
 */
	public function getResults($sourceQuery, $totalInWords){
		//Get the list of all the depts available in the hospital
		$result = $this->conn->execute ($sourceQuery);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			//Store all in an array
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$sourceArray[$row["sourceid"]] = $row["source"];
		} else $sourceArray = array();

		//Drop temporary tables & indexes created if existing
		$query = "DROP TABLE IF EXISTS temp_notinperiod_in";
		$this->conn->execute ($query);
		$query = "DROP TABLE IF EXISTS temp_inperiod_in";
		$this->conn->execute ($query);
		$query = "DROP TABLE IF EXISTS temp_notinperiod_out";
		$this->conn->execute ($query);
		$query = "DROP TABLE IF EXISTS temp_inperiod_out";
		$this->conn->execute ($query);

		//INPATIENTS: Create a temporary table and store a distinct list of all the transactions not in the month under consideration
		$query = "CREATE TEMPORARY TABLE temp_notinperiod_in
					SELECT DISTINCT patadm_id
					FROM inpatient_admission
					WHERE inp_dateattended < '" . $this->periodStart . "'";
		$this->conn->execute ($query);

		//INPATIENTS: Create a temporary table and store a distinct list of all the transactions in the month under consideration
		$query = "CREATE TEMPORARY TABLE temp_inperiod_in
					SELECT DISTINCT patadm_id, refferer_id 
					FROM inpatient_admission
					WHERE inp_dateattended BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'";
		$this->conn->execute ($query);

		//OUTPATIENTS: Create a temporary table and store a distinct list of all the transactions not in the month under consideration
		$query = "CREATE TEMPORARY TABLE temp_notinperiod_out
					SELECT DISTINCT app.patadm_id FROM appointment app
					WHERE app.app_starttime < '" . $this->periodStart . "'";
		$this->conn->execute ($query);

		//OUTPATIENTS: Create a temporary table and store a distinct list of all the transactions in the month under consideration
		$query = "CREATE TEMPORARY TABLE temp_inperiod_out
					SELECT DISTINCT app.patadm_id, outp.refferer_id 
					FROM appointment app INNER JOIN outpatient_admission outp
					ON app.patadm_id = outp.patadm_id
					WHERE app.app_starttime BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'";
		$this->conn->execute ($query);

		//Create indexes on the temp tables above
		$query = "CREATE INDEX patadm_id ON temp_notinperiod_in (patadm_id)";
		$this->conn->execute ($query);
		$query = "CREATE INDEX patadm_id ON temp_inperiod_in (patadm_id)";
		$this->conn->execute ($query);
		$query = "CREATE INDEX patadm_id ON temp_notinperiod_out (patadm_id)";
		$this->conn->execute ($query);
		$query = "CREATE INDEX patadm_id ON temp_inperiod_out (patadm_id)";
		$this->conn->execute ($query);

		//Get a list of old patients & total attendances into an array
		//PARAMS: 0 for male, 1 for female
		$newInArray = $this->getNewArray("in");			//New in-patients
		$newOutArray = $this->getNewArray("out");		//New Out-patients
		$totalInArray = $this->getTotalArray("in");		//total in-patients
		$totalOutArray = $this->getTotalArray("out");	//total out-patients

		//Drop the temporary tables created
		$query = "DROP TABLE temp_inperiod_out";
		$this->conn->execute ($query);
		$query = "DROP TABLE temp_notinperiod_out";
		$this->conn->execute ($query);
		$query = "DROP TABLE temp_inperiod_in";
		$this->conn->execute ($query);
		$query = "DROP TABLE temp_notinperiod_in";
		$this->conn->execute ($query);

		//Init needed vars
		$counter = 0;
		$total = 0;
		$new_final = $total_final = 0;
		$result = "";
		$juggleRows = false;

		foreach ($sourceArray as $sourceid => $source){
			//Get the figures to be shown
			$counter++;
			$newIn = array_key_exists($sourceid, $newInArray) ? $newInArray[$sourceid] : 0;
			$newOut = array_key_exists($sourceid, $newOutArray) ? $newOutArray[$sourceid] : 0;
			$totalIn = array_key_exists($sourceid, $totalInArray) ? $totalInArray[$sourceid] : 0;
			$totalOut = array_key_exists($sourceid, $totalOutArray) ? $totalOutArray[$sourceid] : 0;
			$new_actual = $newIn + $newOut;
			$total_actual = $totalIn + $totalOut;
			$percent = $total_actual != 0 ? ($new_actual * 100 / $total_actual) : 0;
			$new_final += $new_actual;
			$total_final += $total_actual;
			$rowClass = $juggleRows ? " class=\"tr-row2\"" : " class=\"tr-row\"";
			$juggleRows = !$juggleRows;
			

			$result .= "<tr $rowClass>\n
							<th align=\"left\">$counter.</th>\n
							<th align=\"left\">$source</th>\n
							<td align=\"right\">$new_actual</td>\n
							<td align=\"right\">$total_actual</td>\n
							<td align=\"right\">$percent%</td>\n
						</tr>\n";
		}	//END foreach

		if (!empty($result)){
			//Add the row containing totals to the result
			$percent_final = $total_final != 0 ? ($new_final * 100 / $total_final) : 0;
			$result .= "<tr>\n
							<th colspan=\"2\" align=\"center\">" . strtoupper($totalInWords) . "</th>\n
							<th align=\"right\">$new_final</th>\n
							<th align=\"right\">$total_final</th>\n
							<th align=\"right\">" . round($percent_final, 2) . "%</th>\n
						</tr>\n";
		}

		return $result;

	}	//END getResults()

}
?>