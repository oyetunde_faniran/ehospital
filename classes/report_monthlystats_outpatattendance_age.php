<?php
/**
 * Handles "Attendances at Out-Patient Clinic by Age Group" report
 * 
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */

/**
 * Handles "Attendances at Out-Patient Clinic by Age Group" report
 *
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */
class report_monthlystats_outpatattendance_age{
    /**
     * @var object Connection object
     */
	public $conn;

    /**
     * @var object report_common class object
     */
	protected $common;

    /**
     * @var int The number of the month for which the report would be generated
     */
	protected $month;

    /**
     * @var int The year for which the report would be generated
     */
	protected $year;



/**
 * Class constructor
 *
 * @param int $month    The number of the month for which the report would be generated (01 - 12)
 * @param int $year     The year for which the report would be generated
 */
	public function __construct($month, $year){
		$this->conn = new DBConf();
		$this->common = new report_common();
		$this->month = $month;
		$this->year = $year;
		$this->days = $this->common->getDaysInMonth($year, $month);
		$this->periodStart = "$year-$month-01 00:00:00";
		$this->periodEnd = "$year-$month-" . $this->days . " 23:59:59";
	}   //END __construct()

	

/**
 * Gets the age distribution of patients in departments with respect to the supplied parameters
 * @param int $sex          0 = Male, 1 = Female
 * @param int $lBound       The number of days, years ... (as specified by $type) to be deducted from the current date to form the lower bound of a date interval to use for the report
 * @param int $uBound       The number of days, years ... (as specified by $type) to be deducted from the current date to form the upper bound of a date interval to use for the report
 * @param string $type      The type of time period to deduct from the current date e.g. DAY, YEAR ...
 * @return array            An array containing the ID of all departments with the corresponding number of patients in a particular age group
 */
	protected function getStats($sex, $lBound, $uBound, $type = "DAY"){
		$query = "SELECT COUNT(*) patients, app.dept_id, DATEDIFF(CURDATE(), reg.reg_dob) datediff
					FROM appointment app INNER JOIN patient_admission pat INNER JOIN registry reg
					ON app.patadm_id = pat.patadm_id AND pat.reg_hospital_no = reg.reg_hospital_no
					WHERE (app.app_starttime BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "')
						AND reg.reg_dob BETWEEN DATE_ADD(CURDATE(), INTERVAL -$uBound $type) AND DATE_ADD(CURDATE(), INTERVAL -$lBound $type)
						AND reg.reg_gender = '$sex'
					GROUP BY app.dept_id";
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			//Store all in an array
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$newArray[$row["dept_id"]] = $row["patients"];
		} else $newArray = array ();
		return $newArray;
	}   //END getStats()




/**
 * Generates the actual report to be shown to the user
 * @param string $deptQuery     The query that would be used to generate the list of all departments alongside their IDs
 * @param string $totalInWords  The string literal "Total" in the currently selected language
 * @return string               The generated HTML of the report
 */
	public function getResult($deptQuery, $totalInWords){
		//Get the list of all the depts available in the hospital
		$result = $this->conn->execute ($deptQuery);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			//Store all in an array
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$deptArray[$row["dept_id"]] = $row["dept"];
		} else $deptArray = array();

		//Get the data to fill each column
		//0-28 days
		$col1MArray = $this->getStats(0, 0, 28);	//male
		$col1FArray = $this->getStats(1, 0, 28);	//female

		//1 - 59 months ***(29 days - 1770 days)
		$col2MArray = $this->getStats(0, 29, 1770);	//male
		$col2FArray = $this->getStats(1, 29, 1770);	//female

		//5 - 9 years ***(1771 days - 3285 days)
		$col3MArray = $this->getStats(0, 1771, 3285);	//male
		$col3FArray = $this->getStats(1, 1771, 3285);	//female

		//10 - 19 years
		$col4MArray = $this->getStats(0, 3286, 6935);	//male
		$col4FArray = $this->getStats(1, 3286, 6935);	//female

		//20 - 40 years
		$col5MArray = $this->getStats(0, 6936, 14600);	//male
		$col5FArray = $this->getStats(1, 6936, 14600);	//female

		//>=40 years (40 - 500 years --> lol)
		$col6MArray = $this->getStats(0, 14601, 500);	//male
		$col6FArray = $this->getStats(1, 14601, 91250);	//female

		//Init needed vars
		for ($counter = 1; $counter <= 6; $counter++){
			${"col" . $counter . "MTotal"} = 0;	//Inits a var like col2MArray to 0
			${"col" . $counter . "FTotal"} = 0;
		}
		$sno = 0;
		$grandTotal = 0;
		$totalMTotal = 0;
		$totalFTotal = 0;
		$sessionsTotal = 0;
		$result = "";
		$grandTotal = 0;
		$grandMTotal = $grandFTotal = 0;
		$juggleRows = false;

		foreach ($deptArray as $deptid=>$dept){
			//Get the figures to be shown
			$sno++;
			$rowMTotal = $rowFTotal = $cSession = 0;
			for ($counter = 1; $counter <= 6; $counter++){
				//Get the vars
				${"col" . $counter . "Male"} = array_key_exists($deptid, ${"col" . $counter . "MArray"}) ? ${"col" . $counter . "MArray"}[$deptid] : 0;
				${"col" . $counter . "Female"} = array_key_exists($deptid, ${"col" . $counter . "FArray"}) ? ${"col" . $counter . "FArray"}[$deptid] : 0;
				
				//Get column totals
				${"col" . $counter . "MTotal"} += ${"col" . $counter . "Male"};
				${"col" . $counter . "FTotal"} += ${"col" . $counter . "Female"};
				$rowMTotal += ${"col" . $counter . "Male"};
				$rowFTotal += ${"col" . $counter . "Female"};
				$cSession = $rowMTotal + $rowFTotal;
				$grandMTotal += ${"col" . $counter . "Male"};
				$grandFTotal += ${"col" . $counter . "Female"};
				$grandTotal += ${"col" . $counter . "Male"} + ${"col" . $counter . "Female"};
			}
			$rowClass = $juggleRows ? " class=\"tr-row\"" : " class=\"tr-row2\"";
			$juggleRows = !$juggleRows;

			$result .= "<tr $rowClass>\n
							<th align=\"left\">$sno.</th>\n
							<th align=\"left\">$dept</th>\n
							
							<td align=\"right\">$col1Male</td>\n
							<td align=\"right\">$col1Female</td>\n
							
							<td align=\"right\">$col2Male</td>\n
							<td align=\"right\">$col2Female</td>\n
							
							<td align=\"right\">$col3Male</td>\n
							<td align=\"right\">$col3Female</td>\n
							
							<td align=\"right\">$col4Male</td>\n
							<td align=\"right\">$col4Female</td>\n
							
							<td align=\"right\">$col5Male</td>\n
							<td align=\"right\">$col5Female</td>\n
							
							<td align=\"right\">$col6Male</td>\n
							<td align=\"right\">$col6Female</td>\n
							
							<td align=\"right\">$rowMTotal</td>\n
							<td align=\"right\">$rowFTotal</td>\n

							<td align=\"right\">$cSession</td>\n
						</tr>\n";
		}	//END foreach

		if (!empty($result)){
			//Add the row containing totals to the result
			$result .= "<tr>\n
							<th align=\"left\" colspan=\"2\"><h2>" . strtoupper($totalInWords) . "</h2></th>\n
							
							<th align=\"right\">$col1MTotal</th>\n
							<th align=\"right\">$col1FTotal</th>\n
							
							<th align=\"right\">$col2MTotal</th>\n
							<th align=\"right\">$col2FTotal</th>\n
							
							<th align=\"right\">$col3MTotal</th>\n
							<th align=\"right\">$col3FTotal</th>\n
							
							<th align=\"right\">$col4MTotal</th>\n
							<th align=\"right\">$col4FTotal</th>\n
							
							<th align=\"right\">$col5MTotal</th>\n
							<th align=\"right\">$col5FTotal</th>\n

							<th align=\"right\">$col6MTotal</th>\n
							<th align=\"right\">$col6FTotal</th>\n

							<th align=\"right\">$grandMTotal</th>\n
							<th align=\"right\">$grandFTotal</th>\n

							<th align=\"right\"><h2>$grandTotal</h2></th>\n
						</tr>\n";
		}

		return $result;

	}	//END getResults()

}
?>