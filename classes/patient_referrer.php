<?php
/*
 *Contains class variables & methods to select, save, update and delete rows from patient_referrer table
 *@package patient_referrer
 *@author: Rufai Ahmed
 *Create Date: 26-08-2009
 */
 require_once './classes/DBConf.php';
 
Class patient_referrer {

	public $refferer_id; //int(10) unsigned
	public $langcont_id; //int(10) unsigned
	public $connection;
/**
 * class constructor
 */
	function  __construct() {
        $this->conn = new DBConf();
    }
    
 
    /**
     * temporarily holds intemediate values from the database table or other script in the class
     * @param int $langcont_id
     * @param string $drugtype_order
     */
	public function New_patient_referrer($langcont_id){
		$this->langcont_id = $langcont_id;
	}

    /**
     *Load one row into class variables using specified row key
     * @param int $key_row
     */
	public function Load_from_key($key_row){
		$result = $this->conn->execute("Select * from patient_referrer where refferer_id = \"$key_row\" ");
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$this->refferer_id = $row["refferer_id"];
			$this->langcont_id = $row["langcont_id"];
		}
	}

   /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     *
     */
	public function Delete_row_from_key($key_row){
		$this->conn->execute("DELETE FROM patient_referrer WHERE refferer_id = $key_row");
	}

    /**
     *Update the active class vars on table row
          */
	public function Save_Active_Row(){
		$this->conn->execute("UPDATE patient_referrer set langcont_id = \"$this->langcont_id\" where refferer_id = \"$this->refferer_id\"");
	}

    /**
     * Save the active class vars as a new row on table
     */
	public function Save_Active_Row_as_New(){
		$this->conn->execute("Insert into patient_referrer (langcont_id) values (\"$this->langcont_id\"");
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->conn->execute("SELECT refferer_id from patient_referrer order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["refferer_id"];
				$i++;
			}
	return $keys;
	}
/**
 *returns referrer name
 * @param int $id referrer id
 * @param string $curLnag_field column in the language table that contains current language text
 * @param string $langtb  language table
 * @return string
 *
 */
function getReferrer_name($id,$curLnag_field,$langtb) {
        try {
		$getlang= new language();
           $sql = 'Select * from patient_referrer where refferer_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
            return $getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
						
        } catch (Exception $e) {


        }
    }
/**
 *populates option tags with referrer names from patient_referrer table
 * @param string $curLnag_field column in the language table that contains current language text
 * @param string $langtb  language table
 */
function getReferrers($curLnag_field,$langtb) {
        try {
			 $getlang= new language();
           $sql = 'Select * from patient_referrer';
            $res = $this->conn->execute($sql);
			echo '<option value ="-1">---</option>';
            while ($row = mysql_fetch_array($res)) {
            echo '<option value ='.$row['refferer_id'] .' >'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
				 
            }
			
        } catch (Exception $e) {


        }
    }
	
	/**
     *populates option tags with referrer names from patient_referrer table
     * @param int $id represents inp_id (admission ID)
     * @param string $tablename
     * @param string $curLnag_field column in the language table that contains current language text
     * @param string $langtb  language table

     */
	function getReferrers2($id,$tablename,$curLnag_field,$langtb) {
        try {
			 $getLang= new language();
			$sql2 = "select refferer_id from ". $tablename." where inp_id =".$id;
			
				 $res2 = $this->conn->execute($sql2);
				if($res2){
               $row2 = mysql_fetch_array($res2);
				  $id2=$row2['refferer_id'];
                }
           $sql = 'Select * from patient_referrer';
            $res = $this->conn->execute($sql);
			echo '<option value ="-1">---</option>';
            while ($row = mysql_fetch_array($res)) {
			$sel= ($id2==$row['refferer_id'])? "selected":" ";
            echo '<option value ="'.$row['refferer_id'] .'"'. $sel.'>'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
				 
            }
			
        } catch (Exception $e) {


        }
    }
    /**
     * Close mysql connection
     */
	public function endpatient_referrer(){
		$this->connection->CloseMysql();
	}

}