<?php
/**
 * Generates the HTML for the drop-down list for different entities
 *
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */

/**
 * Generates the HTML for the drop-down list for different entities
 *
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */
class patSelectOptions {
//private $select="";


/**
 * Class Constructor
 */
    function  __construct() {
        $this->conn = new DBConf();
    }   //END __construct()


/**
 * Gets the ID & name of all countries or that of the selected one if the $nationality_id parameter is specified
 * @param int $nationality_id   The ID of a country in the nationality table
 * @return mixed                array of the details on success, NULL on failure
 */
    function selectNationality($nationality_id = 0) {
        try {
            $query = "SELECT * FROM nationality" ;
			if($nationality_id != 0){
			$query .= " WHERE nationality_id='".$nationality_id."'";
			}
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
				 extract($row);
				 $select = array('nationality_id' => $nationality_id,
				 				 'nationality_name' => $nationality_name);
				return $select;
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectNationality()



/**
 * Gets the ID & name of all states or that of the selected one if the $state_id parameter is specified
 * @param int $state_id         The ID of a state in the states table
 * @return mixed                array of the details on success, NULL on failure
 */
    function selectState($state_id=0) {
        try {
            $query = "SELECT state_id,  state_name FROM state" ;
			if($state_id != 0){
			$query .= " WHERE state_id='".$state_id."'";
			}

            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
				 extract($row);
				 $select = array('state_id' => $state_id,
				 				 'state_name' => $state_name);
            		}
					return $select;
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectState()



/**
 * Generates a drop-down list of retainers / HMOs
 * @param int $isretainer   Can be either 0 or 1. 1 = Retainer, while 0 = NHIS
 * @param int $curValue     The ID of the currently selected item
 */
    function selectRetainer($isretainer, $curValue = "") {
        try {
            $query = "SELECT retainership_id, retainership_company FROM retainership WHERE retainership_isretainer='" . $isretainer . "'" ;

            $res = $this->conn->execute($query);

            if ($res) {
			    while ($row = mysql_fetch_array($res)) {
					if ($row['retainership_id'] == $curValue)
                		echo '<option value=' . $row['retainership_id'] . ' selected=\"selected\">' . $row['retainership_company'] . '</option>';
					else echo '<option value=' . $row['retainership_id'] . '>' . $row['retainership_company'] . '</option>';
            	}
			}
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectRetainer()




/**
 * Gets the name of a retainer / HMO
 * @param int $retainership_id The ID of the retainer whose name is to be fetched
 */
     function selectRetainerName($retainership_id) {
        try {
            $query = "SELECT * FROM retainership WHERE retainership_id='".$retainership_id."'" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
                  echo $row['retainership_company'];
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectRetainerName()




/**
 * Generates a drop-down list of all condition types
 * @param string $curLangField      The name of the column in the language table that correponds to the currently-selected language
 */
   function selectCondition($curLangField) {
        try {
            $query = "SELECT *
					 FROM
					 	conditiontype as c, language_content as cl
					 WHERE cl.langcont_id = c.langcont_id AND c.ct_type='0' " ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
                  echo '<option value='.$row['ct_id'].'>'.$row["$curLangField"].'</option>';
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectCondition()





/**
 * Gets the name & id of a department
 * @param int $dept_id          The ID of the department under consideration
 * @param string $curLangField  The name of the column in the language table that correponds to the currently-selected language
 * @return mixed                Returns an array containing the id & name on success or false on failure
 */
   function selectDepartment($dept_id=0, $curLangField) {
        try {
            $query = "SELECT cl.$curLangField as name, d.dept_id as id
					 FROM department d LEFT JOIN language_content cl ON cl.langcont_id = d.langcont_id
					 WHERE true " ;
			if($dept_id != 0){
                $query .= " AND d.dept_id='".$dept_id."'";
			}
            $res = $this->conn->execute($query);

            if ($res) {
			    while ($row = mysql_fetch_array($res)) {
                     $select = array('dept_id' => $row['id'],
                                     'dept_name' => $row["name"]);
					return $select;
            	}
            } else return FALSE;
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectDepartment()



/**
 * Gets the name & id of a clinic
 * @param int $clinic_id        The ID of the clinic under consideration
 * @param string $curLangField  The name of the column in the language table that correponds to the currently-selected language
 * @return mixed                Returns an array containing the id & name on success or false on failure
 */
   function selectClinic($clinic_id = 0, $curLangField) {
        try {
            $query = "SELECT cl.$curLangField as name, c.clinic_id as id
					 FROM
					 	clinic c LEFT JOIN language_content cl ON cl.langcont_id = c.langcont_id
					 WHERE true" ;
			if($clinic_id != 0){
                $query .= " AND clinic_id='".$clinic_id."'";
			}

            $res = $this->conn->execute($query);

            if ($res) {
			    while ($row = mysql_fetch_array($res)) {
                     $select = array('clinic_id' => $row['id'],
                                     'clinic_name' => $row["name"]);
                        return $select;
            	}
            }else return FALSE;
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectClinic()




/**
 * Gets a drop-down list of possible referrer sources
 * @param string $curLangField  The name of the column in the language table that correponds to the currently-selected language
 */
     function selectReferrer($curLangField) {
        try {
            $query = "SELECT cl.$curLangField as name, p.refferer_id as id
					 FROM patient_referrer p INNER JOIN language_content cl ON cl.langcont_id = p.langcont_id
					 WHERE true" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
                  echo '<option value='.$row['id'].'>'.$row["name"].'</option>';
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectReferrer()



/**
 * Gets a drop-down list of all consultants
 */
  function selectConsultant_old(){
        try {
            $query = "SELECT *
					 FROM
					 	consultant as c, staff as s
					 WHERE
						c.staff_employee_id=s.staff_employee_id" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {

                  echo '<option value='.$row['consultant_id'].'>'.$row["staff_surname"].' '.$row["staff_othernames"].'</option>';
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectConsultant_old()




/**
	Returns all the consultants in a clinic with clinic_id $clinicID in a drop-down list
	@param int The clinic ID of the clinic whose list of consultants has to be returned in a drop-down list
*/
  function getClinicConsultants4DD($clinicID) {
        try {
			$query = "SELECT * FROM consultant cons INNER JOIN staff s
						ON cons.staff_employee_id = s.staff_employee_id
						WHERE cons.clinic_id = '$clinicID'";
            //die ("<pre>$query</pre>");
            $res = $this->conn->execute($query);
			$retVal = "";
            if ($this->conn->hasRows ($res)) {
				while ($row = mysql_fetch_array($res)) {
					$retVal .= "<option value=" . $row['consultant_id'] . ">" . $row["staff_surname"] . " " . $row["staff_othernames"] . "</option>";
				}
            }
        } catch (Exception $e) {
			set_error_handler (array("Error_handler", "my_error_handler"));
        }
		return $retVal;
    }   //END getClinicConsultants4DD()





/**
 * Gets a list of departments and their clinics
 * @param string $curLangField  The name of the column in the language table that correponds to the currently-selected language
 * @return <type>
 */
   function selectJqueryDepartAndClinic($curLangField) {
        try {
            $query = "SELECT cl.$curLangField as name, d.dept_id as id
					 FROM department d LEFT JOIN language_content cl ON cl.langcont_id = d.langcont_id WHERE dept_isclinical = '1'
                    ORDER BY cl.lang1" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     $i = 0;
				 $num = mysql_num_rows($res);
				if($num>0){
			     while ($row = mysql_fetch_array($res)) {
				 $deptid = $row['id'];
				 if($i==0){
                  $selectoptions = '"'.$row["name"].'":{
				  					"key":'.$deptid.',
									"values" : {';
				  }else{
                  $selectoptions = ',"'.$row["name"].'":{
				  					"key":'.$deptid.',
									"values" : {';
				   }
				$query2 = "SELECT cl.$curLangField as name, c.clinic_id as id
					 FROM clinic c LEFT JOIN language_content cl ON cl.langcont_id = c.langcont_id
                     WHERE c.dept_id='$deptid'
					 ORDER BY name" ;

           	   	$res2 = $this->conn->execute($query2);
				$num2 = mysql_num_rows($res2);
				if($num2>0){

                 $selectoptions .= '"Select Clinic" :0';
			     while ($row2 = mysql_fetch_array($res2)) {

                  $selectoptions .= ',"'.$row2["name"].'" :'.$row2['id'];

            		}//end second while loop
					}else{
                 	$selectoptions .= '"Select Clinic" :0';
					//$selectoptions .= '"----------"'.':0';
					}
				$selectoptions .= ' 	}';

             		$selectoptions .= '				 }';
			echo $selectoptions;
					$i +=1;

            		}//end first while loop
					}else{
	                $selectoptions .= '"Select Department" :0';
					//$selectoptions .= '"----------"'.':0';
					}

            } else return false;//end if
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectJqueryDepartAndClinic()



/**
 * Gets the name of a country from its ID
 * @param int $nationality_id   The ID of the country in the nationality table
 */
    function selectNationalityName($nationality_id) {
        try {
            $query = "SELECT * FROM nationality WHERE nationality_id='".$nationality_id."'" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			    while ($row = mysql_fetch_array($res)) {
                    echo $row['nationality_name'];
            	}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectNationalityName()



/**
 * Gets the name of a state from its ID
 * @param int $state_id   The ID of the state in the state table
 */
    function selectStateName($state_id) {
        try {
            $query = "SELECT * FROM state WHERE state_id='".$state_id."'" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
                  echo $row['state_name'];
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectStateName()





/**
 *  Gets a list of clinics alongside their attached consultants for use in JQuery features
 */
   function selectJqueryConsultantAndClinic() {
        try {
            $query = "SELECT *
					 	FROM clinic c
							INNER JOIN language_available l
							INNER JOIN language_content  lc
						ON  lc.langcont_id = c.langcont_id
							AND lc.lang_id = l.lang_id
					 WHERE true
						" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);
            if ($res) {
			     $i = 0;
			     while ($row = mysql_fetch_array($res)) {
                    $lang_field = $row["lang_field"];
                    if($i==0){
                        $selectoptions = '"'.$row["$lang_field"].'":{
                                          "key":'.$row['clinic_id'].',
                                          "values" : {';
                    } else {
                        $selectoptions = ',"'.$row["$lang_field"].'":{
                                          "key":'.$row['clinic_id'].',
                                          "values" : {';
                    }
                    $query2 = "SELECT CONCAT_WS(' ', UPPER(s.staff_title), UPPER(s.staff_surname), LOWER(s.staff_othernames)) AS consultant,
                                 con.consultant_id as id
                                FROM consultant con LEFT JOIN staff s
                                ON con.staff_employee_id = s.staff_employee_id
                                WHERE  con.clinic_id='{$row['clinic_id']}'" ;

                    $res2 = $this->conn->execute($query2);
                    $q = 0;
                    while ($row2 = mysql_fetch_array($res2)) {
                        extract($row2);
                        if($q==0){
                            $selectoptions .= '"'.$consultant.'" :'.$id;
                        } else {
                            $selectoptions .= ',"'.$consultant.'" :'.$id;
                        }
                        $q +=1;
                    }   //end second while loop

                    $selectoptions .= ' 	}
                                         }';
                    echo $selectoptions;
                    $i +=1;

            	}//end first while loop

            }else return FALSE;
		 } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectJqueryConsultantAndClinic()



/**
 * Gets a list of countries alongside their attached states for use in JQuery features
 * @param int $others
 */
   function selectJqueryNationalityAndState($others) {
        try {
            $query = "SELECT *
					 FROM nationality" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     $i = 0;
			     while ($row = mysql_fetch_array($res)) {
				 $nationality_id = $row['nationality_id'];
				 if($i==0){
                  $selectoptions = '"'.$row["nationality_name"].'":{
				  					"key":'.$row['nationality_id'].',
									"values" : {';
				  }else{
                  $selectoptions = ',"'.$row["nationality_name"].'":{
				  					"key":'.$row['nationality_id'].',
									"values" : {';
				   }
				$query2 = "SELECT *
					 FROM state
					 WHERE nationality_id='$nationality_id'" ;

            $res2 = $this->conn->execute($query2);
                 $selectoptions .= '"Select State" : 0';
			     while ($row2 = mysql_fetch_array($res2)) {

				// if($q==0){
                  //$selectoptions .= '"'.$row2["state_name"].'" :'.$row2['state_id'];
				  //}else{
                  $selectoptions .= ',"'.$row2["state_name"].'" :'.$row2['state_id'];

				  //}
				 // $q +=1;

            		}//end second while loop
				if($nationality_id != 158)
				$selectoptions .= ',"'.$others.'"'.':38';

				$selectoptions .= ' 	}';
             		$selectoptions .= '				 }';
			echo $selectoptions;
					$i +=1;

            		}//end first while loop

            } else return false;//end if
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectJqueryNationalityAndState()



/**
 * Gets a drop-down list of the plans for each HMO / retainer
 * @param string $type          Can be "retainer" for retainer plans or "hmo" for HMO plans
 * @param int $retainerID       The ID of the retainer or HMO
 * @param int $curPlan          The ID of the currently selected plan in the drop-down box
 * @return string               The generated HTML for the drop-down
 */
	public function selectRetainerPlans($type, $retainerID=0, $curPlan=0){
		switch ($type){
			case "retainer":	//Get retainer plans
					$query = "SELECT retplan_id 'planid', retplan_name 'plan' FROM retainership_plan
								WHERE retainership_id = '$retainerID'
								ORDER BY plan";
					break;
			default:	//Get NHIS plans
					$query = "SELECT nhisplan_id 'planid', nhisplan_name 'plan' FROM nhis_plan
								ORDER BY plan";
		}
//		die ($query);
		$result = $this->conn->execute($query);
		$retVal = "";
		if ($this->conn->hasRows($result)){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				if ($row["planid"] == $curPlan)
					$retVal .= "<option value=\"{$row['planid']}\" selected=\"selected\">{$row['plan']}</option>\n";
				else $retVal .= "<option value=\"{$row['planid']}\">{$row['plan']}</option>\n";
			}
		}
		return $retVal;
	}   //END selectRetainerPlans()


}   //END class

?>
