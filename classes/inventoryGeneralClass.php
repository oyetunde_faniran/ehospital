<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventoryCreationFormProcessingClass
 *
 * @author upperlink
 */
class inventoryGeneralClass {
    public $conn;
    
    public function __construct() {
        $conn = new DBConf();
        $this->conn = $conn->mysqliConnect();
        //$this->conn=new mysqli('localhost','root','','mediluth_skye');
    }
    
    
    public function get_existing_items($table,$orderCollumn,$orderStyle){
            global $report;
            global $none_in_existence;
            $items=$this->conn->query("select * from $table order by $orderCollumn $orderStyle");
          
            if(!$items){
                return FALSE;
                }
                if($items->num_rows<1){
                    $report=$none_in_existence;
                    return $report;
                }
                return $items;
        }
        
          public function getMedicalName($id){
              global $inventory_dbconnnect_error;
              global $inventory_unknown;
        $nameQuery=$this->conn->query("select inventory_medical_name from inventory_medical_names where inventory_medical_name_id='".$id."'");
        if(!$nameQuery){
            $report=$inventory_dbconnnect_error;
        }elseif($nameQuery->num_rows<1){
            $report=$inventory_unknown;
        }else{
            $name=$nameQuery->fetch_object();
            $report=$name->inventory_medical_name;
        }
        return $report;
    }
    
          public function getCommercialName($id){
              global $inventory_dbconnnect_error;
              global $inventory_unknown;
        $nameQuery=$this->conn->query("select drug_name from drug where drug_id='".$id."'");
        if(!$nameQuery){
            $report=$inventory_dbconnnect_error;
        }elseif($nameQuery->num_rows<1){
            $report=$inventory_unknown;
        }else{
            $name=$nameQuery->fetch_object();
            $report=$name->drug_name;
        }
        return $report;
    }

    
    public function getManufacturerName($id){
        global $inventory_dbconnnect_error;
        global $inventory_unknown;
        $nameQuery=$this->conn->query("select inventory_manufacturer_name from inventory_manufacturers where inventory_manufacturer_id='".$id."'");
        if(!$nameQuery){
            $report=$inventory_dbconnnect_error;
        }elseif($nameQuery->num_rows<1){
            $report=$inventory_unknown;
        }else{
            $name=$nameQuery->fetch_object();
            $report=$name->inventory_manufacturer_name;
        }
        return $report;
}
    
    public function getPresentationName($id){
        global $inventory_dbconnnect_error;
        global $inventory_unknown;
        $nameQuery=$this->conn->query("select inventory_presentation_name from inventory_presentations where inventory_presentation_id='".$id."'");
        if(!$nameQuery){
            $report=$inventory_dbconnnect_error;
        }elseif($nameQuery->num_rows<1){
            $report=$inventory_unknown;
        }else{
            $name=$nameQuery->fetch_object();
            $report=$name->inventory_presentation_name;
        }
        return $report;
    }
     
    
    public function getReorderLevel($item,$location=""){
         global $inventory_dbconnnect_error;
         $location= empty($location)?$_SESSION[session_id() . "inventory_locationID"]:$location;
         $query=$this->conn->query("select inventory_reorder_level from inventory_items_reorder_level where inventory_item_id='".$item."' and inventory_location_id='".$location."'");
         if(!$query){
             $report=$inventory_dbconnnect_error;
         }elseif($query->num_rows<1){
             $report="0";
             }
         else{
             $result=$query->fetch_object();
             $report=$result->inventory_reorder_level;
         }
         return $report;
     }
 
   public function getFormName($id){
        global $inventory_dbconnnect_error;
        global $inventory_unknown;
        $nameQuery=$this->conn->query("select inventory_form_name from inventory_forms where inventory_form_id='".$id."'");
        if(!$nameQuery){
            $report=$inventory_dbconnnect_error;
        }elseif($nameQuery->num_rows<1){
            $report=$inventory_unknown;
        }else{
            $name=$nameQuery->fetch_object();
            $report=$name->inventory_form_name;
        }
        return $report;
    }
    
    public function getLocationName($id){
        global $inventory_failed_to_search;
        global $unknownLocation;
        $nameQuery=$this->conn->query("select inventory_location_name from inventory_locations where inventory_location_id='".$id."'");
        if(!$nameQuery){$report=$inventory_failed_to_search;
        return $report;
        }
        
        if($nameQuery->num_rows<1){
            $report=$unknownLocation;
            return $report;
        }
        if($nameQuery){
            $name=$nameQuery->fetch_object();
            $medicalName=$name->inventory_location_name;
            return $medicalName;
        }
    }

    
    public function getVendorName($id){
        global $inventory_failed_to_search;
        global $unknown;
        $nameQuery=$this->conn->query("select inventory_vendor_name from inventory_vendors where inventory_vendor_id='".$id."'");
        if(!$nameQuery){$report=$inventory_failed_to_search;
        return $report;
        }
        
        if($nameQuery->num_rows<1){
            $report=$unknown;
            return $report;
        }
        if($nameQuery){
            $name=$nameQuery->fetch_object();
            $medicalName=$name->inventory_vendor_name;
            return $medicalName;
        }
    }
    
    public function getLocationAvailBalForItem($itemId,$locationId=""){
        global $inventory_dbconnnect_error;
        
        $locationId= empty($locationId)?$_SESSION[session_id() . "inventory_locationID"]:$locationId; 
        $queryResource=$this->conn->query("select IFNULL(sum(ibd.inventory_batchdetails_quantity),0) as bal 
            from inventory_batch_details as ibd, 
            inventory_batch as ib 
            where ibd.inventory_batch_id=ib.inventory_batch_id 
            and ib.inventory_location_id='".$locationId."' 
                and ibd.drug_id='".$itemId."'");
        if(!$queryResource){
            $report=$inventory_dbconnnect_error;
            }else{
                $report=$queryResource->fetch_assoc();
            }
            return $report['bal'];
    }
    
    public function getLocationTransitQtyForItem($itemId){
        $queryResource=$this->conn->query("select IFNULL(sum(itd.inventory_item_total_qty),0) as bal2 from inventory_transfer_details itd, inventory_batch_details ibd, inventory_batch ib where itd.inventory_item_id='".$itemId."' and itd.inventory_item_item_status='1' and itd.inventory_batchdetails_id=ibd.inventory_batchdetails_id and ibd.inventory_batch_id=ib.inventory_batch_id and ib.inventory_location_id='".$_SESSION[session_id() . "inventory_locationID"]."'");
        
         if(!$queryResource){
            $report=$inventory_dbconnnect_error;
            }else{
                $report=$queryResource->fetch_assoc();
            }
            return $report['bal2'];
    }
    
    public function getTransferStatus($id){
        $statusQuery=$this->conn->query("select inventory_transfer_status from inventory_transfer_status where inventory_transfer_status_id='".$id."'");
        $report=$statusQuery->fetch_object();
        return $report->inventory_transfer_status;
    }
    
    public function getUserName($id){
        global $inventory_failed_to_search;
        global $unknown;
        $nameQuery=$this->conn->query("select staff.staff_title, staff.staff_surname, staff.staff_othernames from staff, users where staff.staff_employee_id=users.staff_employee_id and users.user_id='".$id."'");
        if(!$nameQuery){$report=$inventory_failed_to_search;
        return $report;
        }
        
        if($nameQuery->num_rows<1){
            $report=$unknown;
            return $report;
        }
        if($nameQuery){
            $name=$nameQuery->fetch_assoc();
            $medicalName=$name['staff_title']."&nbsp;".$name['staff_surname']."&nbsp;".$name['staff_othernames'];
            return $medicalName;
        }
    }
    

    public function itIsParent($id){
        global $report;
        global $inventorySubcategorySystemFailureMsg;
        $found=$this->conn->query("select * from inventory_categories where inventory_category_parent='".$id."' order by inventory_category_name asc");
        if(!$found){$report=$inventorySubcategorySystemFailureMsg;
            return $report;
        }
        if($found->num_rows<1){
        return FALSE;
        }
        return $found;
    }
    
  
    
    public function countChildren($id){
        $childrenCount=$this->conn->query("select * from inventory_categories where inventory_category_parent='".$id."'");
        if(!$childrenCount){
            return FALSE;
        }
        return $childrenCount->num_rows;
    }
    
    public function tabulateCategories($id=0){
        $childrenQuery=$this->conn->query("select * from inventory_categories where inventory_category_parent='".$id."'");
        if($childrenQuery->num_rows>0){
            if(isset($counter)){
                $counter=$counter++;
            } else {
                $counter=0;
                }
            echo "<ul style=\"margin-left:10px\">";
            while($cats=$childrenQuery->fetch_assoc()){
            echo "<li class=\""; 
            echo ($counter%2)?"recursive-cats":"recursive-cats-odd";
                             $counter+=1;
          echo "\">".$cats['inventory_category_name']."<span style=\"margin-left:20px; text-align:right\"><input type=\"checkbox\" name=\"cat[]\" value=\"".$cats['inventory_category_id']." \" />";
            if($this->itIsParent($cats['inventory_category_id'])){
                $this->tabulateCategories($cats['inventory_category_id']);
                }
            echo "</span></li>";
            }               

        echo "</ul>";
        }
    }
    
    public function findMedicalNames(){
        global $report;
        global $inventory_none_in_existence;
        global $inventory_medical_name_query_error_msg;
        $foundMedicalNames=$this->conn->query("select * from inventory_medical_names order by inventory_medical_name asc");
        if(!$foundMedicalNames){
            $report=$inventory_medical_name_query_error_msg;
            return $report;
        }
        if($foundMedicalNames->num_rows<1){
            $report=$inventory_none_in_existence;
            return $report;
        }
        return $foundMedicalNames;
    }

    
    public function findItemForms(){
        global $report;
        global $inventory_none_in_existence;
        global $inventory_form_query_error_msg;
        $foundFormNames=$this->conn->query("select * from inventory_forms order by inventory_form_name asc");
        if(!$foundFormNames){
            $report=$inventory_form_query_error_msg;
            return $report;
        }
        if($foundFormNames->num_rows<1){
            $report=$inventory_none_in_existence;
            return $report;
        }
        return $foundFormNames;
    }

    public function findItemManufacturers(){
        global $report;
        global $inventory_none_in_existence;
        global $inventory_manufacturer_query_error_msg;
        $foundManufacturerNames=$this->conn->query("select * from inventory_manufacturers order by inventory_manufacturer_name asc");
        if(!$foundManufacturerNames){
            $report=$inventory_manufacturer_query_error_msg;
            return $report;
        }
        if($foundManufacturerNames->num_rows<1){
            $report=$inventory_none_in_existence;
            return $report;
        }
        return $foundManufacturerNames;
    }    
    
    public function findItemPresentations(){
        global $report;
        global $inventory_none_in_existence;
        global $inventory_manufacturer_query_error_msg;
        $foundManufacturerNames=$this->conn->query("select * from inventory_presentations order by inventory_presentation_name asc");
        if(!$foundManufacturerNames){
            $report=$inventory_manufacturer_query_error_msg;
            return $report;
        }
        if($foundManufacturerNames->num_rows<1){
            $report=$inventory_none_in_existence;
            return $report;
        }
        return $foundManufacturerNames;
    }
    
    public function getItemUnits($id){
        global $report;
        global $none_in_existence;
        global $inventory_dbconnnect_error;
        $unitsQuery=$this->conn->query("select * from inventory_unit_items where inventory_items_id='".$id."'");
        if(!$unitsQuery){
            $report=$inventory_dbconnnect_error;
            return $report;
        }
        if($unitsQuery->num_rows<1){
            $report=$none_in_existence;
            return $report;
        }
        return $unitsQuery;
    }
    
     public function getUnitName($id){
            global $report;
            global $none_in_existence;
            global $inventory_dbconnnect_error;
            
            $unitQuery=$this->conn->query("select * from inventory_units where inventory_unit_id='".$id."'");
            if(!$unitQuery){
                $report=$inventory_dbconnnect_error;
                return $report;
            }
            if($unitQuery->num_rows!='1'){
                $report=$none_in_existence;
                return $report;
            }
            $name=$unitQuery->fetch_assoc();
            return $name['inventory_unit_name'];
        }
    
}

?>