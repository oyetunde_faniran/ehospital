<?php
/**
 * Handles the export of reports to PDF & Excel
 * 
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */

/**
 * Handles the export of reports to PDF & Excel
 *
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */
class report_PDFExcel{
    /**
     * @var string The title of the Excel file
     */
	protected $title;

    /**
     * @var array Contains a multi-dimensional array of the values to be written to the Excel file
     */
	protected $contents;

    /**
     * @var boolean The error flag that would be set to true whenever an error occurrs during any operation
     */
	public $error;

    /**
     * @var array Contains an array with the elements "colHeader" & "rowHeader" which are of type boolean so that column / row headers can be shown in bold face when set to true respectively.
     */
	protected $options;

    /**
     * @var object Stores the generated Excel object
     */
	protected $excel;


/**
 * Class constructor
 * 
 * @param string $title The title of the Excel file
 * @param array $contents Should contain a multi-dimensional array of the values to be written to the Excel file
 * @param array $options Should contain an array with the elements "colHeader" & "rowHeader" which are of type boolean so that column / row headers can be shown in bold face when set to true respectively.
 */
	public function __construct($title, $contents, $options){
		try{
			if (is_string($title) && !empty($title))
				$this->title = $title;
			else throw new Exception;

			if (is_array($contents)){
				$this->contents = $contents;
				if (is_array($options) && array_key_exists("colHeader", $options) && array_key_exists("rowHeader", $options))
					$this->options = $options;
				else throw new Exception;
			} else throw new Exception;

		} catch (Exception $e) {
				$this->error = true;
			}
	}   //END __construct()



/**
 * Does the actual export to excel
 *
 * @param string $type It determines the format of Excel to which the document would be exported. Can be "2007" (MS Excel 2007) or any other string for MS Excel 2003
 * @param string $exportFile The path of the file to write the Excel contents to
 * @param boolean $doActualExport Flag that determines whether the generated Excel file should be written to $exportFile or simply held in the Excel File object
 * @return boolean Returns true if the operation was successful. False otherwise.
 */
	public function doExcelExport($type, $exportFile, $doActualExport = true){
		$objPHPExcel = new PHPExcel();

		//Set properties
		$objPHPExcel->getProperties()->setCreator("Upperlink Nigeria Ltd.");
		$objPHPExcel->getProperties()->setLastModifiedBy("Upperlink Nigeria Ltd.");
		$objPHPExcel->getProperties()->setTitle($this->title);
		$objPHPExcel->getProperties()->setSubject($this->title);
		$objPHPExcel->getProperties()->setDescription($this->title);

		//Set Font
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Tahoma');
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);

		//Set header and footer
		$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&G&C&H' . $this->title);
		$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);

		//Write data into it
		$objPHPExcel->setActiveSheetIndex(0);
		$row = 1;
		$firstRow = false;
		foreach ($this->contents as $rowContent){
			$col = 'A';
			foreach ($rowContent as $colValue){
				$objPHPExcel->getActiveSheet()->setCellValue($col . $row, $colValue);
				if (!$firstRow && $this->options["colHeader"]){
					$objPHPExcel->getActiveSheet()->getStyle($col . $row)->getFont()->setBold(true);
					//echo "<p>Cell " . $col . $row . " set to bold</p>";
				}

				if ($this->options["rowHeader"] && $col == 'A'){
					$objPHPExcel->getActiveSheet()->getStyle($col . $row)->getFont()->setBold(true);
					//echo "<p>Cell " . $col . $row . " set to bold</p>";
				}
				$col++;
			}
			$firstRow = true;
			$row++;
		}	//END foreach for each row

		//Set Title
		$objPHPExcel->getActiveSheet()->setTitle($this->title);

		$retVal = true;
		if ($doActualExport){
			try {
				$objWriter = $type=="2007" ? PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007') : PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save($exportFile);
			} catch (Exception $e) {
					$retVal = false;
				}
		}
		$this->excel = $objPHPExcel;
		return $retVal;
	}   //END doExcelExport()



/**
 * Exports to PDF by first calling method doExcelExport() to create an Excel object holding the required data, then exporting the Excel object to PDF
 *
 * @param string $type It determines the format of Excel to which the document would be exported. Can be "2007" (MS Excel 2007) or any other string for MS Excel 2003
 * @param string $exportFile The path of the file to write the PDF contents to
 * @return boolean Returns true if the operation was successful. False otherwise.
 */
	public function doPDFExport($type, $exportFile){
		$this->doExcelExport($type, $exportFile, false);
		if (is_object($this->excel)){
			try {
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
				$objWriter->setSheetIndex(0);
				$objWriter->save($exportFile);
				$retVal = true;
			} catch (Exception $e) {
				$retVal = false;
				//echo "<pre>" . print_r($e, true) . "</pre>";
			}
		} else $retVal = false;
		return $retVal;
	}   //END doPDFExport()

}	//END class
?>