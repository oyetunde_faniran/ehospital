<?php
/**
 * Contains diverse methods that can be used from different classes in the System_Administration package (and even other packages)
 *
 * @author Faniran, Oyetunde
 *
 * @package System_Administration
 */

/**
 * Contains diverse methods that can be used from different classes in the System_Administration package (and even other packages)
 *
 * @author Faniran, Oyetunde
 *
 * @package System_Administration
 */
class admin_Tools{
	function __construct(){
	}




/**
 * Escapes any value in the parameter $toEscape
 * @param mixed $toEscape   The data to be escaped using mysql_real_escape_string with the current magic quotes setting taken into consideration. Can be any data type (even an array).
 * @param object $conn      A DBConf object
 * @return mixed            Returns the escaped data
 */
//	public static function doEscape_new($toEscape, $conn, $trimNot = array(), $touchNot = array()){
//        echo "TO ESCAPE: --$toEscape--<p>";
//        if (is_array($toEscape)){
//            foreach ($toEscape as $key=>$value){
//                if (!is_array($value)){
//                    if (!in_array($key, $touchNot)){
//                        if (!in_array($key, $trimNot)){
//                            $value = trim ($value);
//                        }
//                        $value = get_magic_quotes_gpc() ? stripslashes ($value) : $value;
//                        $toEscape[$key] = mysql_real_escape_string (htmlentities(strip_tags($value), ENT_QUOTES), $conn->getConnectionID());
//                    }
//                } else $toEscape[$key] = $this->doEscape ($value, $conn->getConnectionID(), $trimNot, $touchNot);
//            }
//        } //else $toEscape = mysql_real_escape_string (htmlentities(strip_tags(stripslashes (trim ($toEscape))), ENT_QUOTES), $conn->getConnectionID());
//        else $toEscape = mysql_real_escape_string (htmlentities($toEscape), $conn->getConnectionID());
//        echo "AFTER ESCAPING: --$toEscape--<p>";
//		return $toEscape;
//	}   //END doEscape()





    public static function doEscape($toEscape, $conn){
		if (get_magic_quotes_gpc()){
			if (is_array($toEscape)){
				foreach ($toEscape as $key=>$value){
					if (!is_array($value))
						$toEscape[$key] = mysql_real_escape_string (stripslashes (trim ($value)), $conn->getConnectionID());
					else $toEscape[$key] = self::doEscape ($value, $conn);
				}
			} else $toEscape = mysql_real_escape_string (stripslashes (trim ($toEscape)), $conn->getConnectionID());
		} else {
				if (is_array($toEscape)){
					foreach ($toEscape as $key=>$value){
						if (!is_array($value))
							$toEscape[$key] = mysql_real_escape_string (trim ($value), $conn->getConnectionID());
						else $toEscape[$key] = self::doEscape ($value, $conn);
					}
				} else $toEscape = mysql_real_escape_string (trim ($toEscape), $conn->getConnectionID());
			}
		return $toEscape;
	}









/**
 * Formats the $dateString like this January 12, 2009
 * @param string $dateString    Any accepted date string in PHP
 * @return date                 Returns the formatted data like this example January 12, 2009
 */
	public static function formatDate($dateString){
		return date("F d, Y", strtotime($dateString));
	}



/**
 * Uses a regular expression to validate a date string of the format (YYYY-MM-DD) as a valid date
 * @param <type> $dateValue
 * @return <type>
 */
	public static function isValidDate($dateValue){
		if (preg_match("/^[0-9]{4,4}[-][0-9]{2,2}[-][0-9]{2,2}$/", $dateValue))
			$retVal = true;
		else $retVal = false;
		return $retVal;
	}



/**
 * Inserts the value $s into the language table (language_content)
 * @param string $s             The value to insert into the language table
 * @param string $langField     The current language column in the language table
 * @return int                  Returns the ID of the inserted item in the language table
 */
	public static function insert2Lang($s, $langField){
		$conn = new DBConf();
		$query = "INSERT INTO language_content ($langField)
					VALUES ('$s')";
		$conn->execute($query);
		return mysql_insert_id ($conn->getConnectionID());
	}



/**
 * Updates a value with ID $id in the language table (language_content)
 * @param int $id               The row ID in the table from which the language table ID would be fetched
 * @param mixed $newValue       The new value that would be used to update the appropriate row
 * @param string $langField     The current language column in the language table
 * @param string $tableName     The name of the table from which the language content table ID would be fetched
 * @param string $tableIDField  The name of the column in the table from which the language content table ID would be fetched
 * @return int                  The ID of the row that was updated in the language table
 */
	public static function updateLang($id, $newValue, $langField, $tableName, $tableIDField){
		$conn = new DBConf();
		$query = "UPDATE language_content lc
					SET $langField = '$newValue'
					WHERE lc.langcont_id = (SELECT t.langcont_id FROM $tableName t WHERE $tableIDField = '$id')";
		$conn->execute($query);
		return mysql_insert_id ($conn->getConnectionID());
	}



/**
 * Gets all available staff departments enclosed with an <option> tag
 * @param string $curLangField  The corresponding column in the language table for the current language
 * @param int $chosen   The ID of the currently selected value in the selected drop-down box so that the selected="selected" attribute can be added to it
 * @return string       Returns the generated HTML that would show the list of all departments in a drop-down box
 */
	public static function getStaffDepts4DropDown($curLangField, $chosen = 0){
		$conn = new DBConf();
		$retVal = "";
		$query = "SELECT d.dept_id id, lc.$curLangField dept FROM department d INNER JOIN language_content lc
					ON d.langcont_id = lc.langcont_id
					ORDER BY dept";
		$result = $conn->execute($query);
		if ($conn->hasRows($result)){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				if ($chosen == $row["id"])
					$retVal .= "<option value=\"{$row['id']}\" selected=\"selected\">{$row['dept']}</option>\n";
				else $retVal .= "<option value=\"{$row['id']}\">{$row['dept']}</option>\n";
			}
		}
		return $retVal;
	}   //END getStaffDepts4DropDown()




    public static function getCustomFormPage4DropDown($chosen = 0){
		$conn = new DBConf();
		$ret_val = "";
		$query = "SELECT custpage_id 'id', custpage_name 'name' FROM customfields_pages
                  ORDER BY custpage_name";
		$result = $conn->execute($query);
		if ($conn->hasRows($result)){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				if ($chosen == $row["id"]){
					$ret_val .= "<option value=\"{$row['id']}\" selected=\"selected\">{$row['name']}</option>\n";
                } else {
                    $ret_val .= "<option value=\"{$row['id']}\">{$row['name']}</option>\n";
                }
			}
		}
		return $ret_val;
	}   //END getCustomFormPage4DropDown()




    public static function getRandNumber($randStringLength){
		$timestring = microtime();
		$secondsSinceEpoch = (int)substr($timestring, strrpos($timestring, " "), 100);
		$microseconds = (double)$timestring;
		$seed = mt_rand (0,1000000000) + 10000000 * $microseconds + $secondsSinceEpoch;
		mt_srand ($seed);
		$randString = "";
		for($i=0; $i < $randStringLength; $i++){
			$randString .= mt_rand(0, 9);
		}
		return($randString);
	}


    public static function cleanUp4Chat($string){
        $retVal = "";
        $acceptable = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $charCount = strlen($string);
        for ($startPos = 0; $startPos < $charCount; $startPos++){
            $char = substr($string, $startPos, 1);
            $retVal .= strpos($acceptable, $char) != false ? $char : "_";
        }
        return $retVal;
    }   //END cleanUp4Chat()



    public static function getCountries(){
        $conn = new DBConf();
        $query = "SELECT * FROM nationality
                  ORDER BY nationality_name";
        $result = $conn->run($query);
        $retVal = array();
        if ($conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $retVal[] = $row;
            }
        }
        return $retVal;
    }   //END getCountries()




    public static function getAllStates(){
        $conn = new DBConf();
        $query = "SELECT * FROM state
                  WHERE state_id <= 37  #Exclude the row that says 'Outside Nigeria'
                  ORDER BY state_name";
        $result = $conn->run($query);
        $retVal = array();
        if ($conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $retVal[] = $row;
            }
        }
        return $retVal;
    }   //END getAllStates()



}	//END class
?>