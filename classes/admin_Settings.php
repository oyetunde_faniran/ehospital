<?php
/**
 * Description of admin_Settings
 *
 * @author Faniran, Oyetunde
 */
class admin_Settings {

    /**
     * @var Connection object   An object of the Connection class, which handles communication with the DB
     */
    protected $conn;
    /**
     * @var int The ID of the admin_Settings under consideration
     */
    protected $settingsID;
    /**
     *
     * @var boolean = true whenever an error occurs during any operation
     */
    public $error;
    /**
     *
     * @var string  Contains the error message of the error that occurred during the last operation if any
     */
    public $errorMsg;

    /**
     * Class Constructor
     * @param int $admin_SettingsID        The ID of the admin_Settings under consideration
     */
    function __construct($settingsID = 0) {
        $this->conn = new DBConf();
        $this->settingsID = (int)$settingsID;
        $this->error = false;
        $this->errorMsg = "";
    }   //END __construct
    
    
    
    public function getSettings($settingsName){
        $retVal = "";
        $settingsName = admin_Tools::doEscape($settingsName, $this->conn);
        $query = "SELECT settings_value FROM settings
                    WHERE settings_name = '$settingsName'";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $row = mysql_fetch_assoc($result);
            $retVal = $row["settings_value"];
        }
        return $retVal;
    }   //END getSetting()
    
    
    
    
}   //END class
?>
