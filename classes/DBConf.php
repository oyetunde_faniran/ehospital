<?php

if(!defined('DBNAME')){
    require_once('../includes/database.php');
}
/**
 * Handles database connections and queries
 *
 * @package System_Administration
 */

/**
 * Handles database connections and queries
 *
 * @package System_Administration
 */
class DBConf {
    /**
     * @var string Database Account Username
     */
    protected $user;

    /**
     * @var string Database Account Password
     */
<<<<<<< HEAD
    protected $pass = 'dekunle';	//'*(Luth-Upper)8620;';
=======
    protected $pass;
>>>>>>> c4024653383db378a9198c2e3b07c7a7b485a159

    /**
     * @var string Database Server Host
     */
<<<<<<< HEAD
    protected $dbhost = '127.0.0.1';
=======
    protected $dbhost;
>>>>>>> c4024653383db378a9198c2e3b07c7a7b485a159

    /**
     * @var string Main Database Name
     */
    public $dbname;

    /**
     * @var string Laboratory Database Name
     */
	public $labdbname = LABDBNAME;

    /**
     * @var string Chat Database Name
     */
	public $chatDBname = CHATDBNAME;

    /**
     * @var string Database Connection Handle
     */
    public $dbh; // Database connection handle


    /**
     * Class Constructor
     * @param string $dbname
     * @param string $dbhost
     * @param string $user
     * @param string $pass
     */
    public function  __construct($dbname = DBNAME, $dbhost = DBHOST, $user = DBUSER, $pass = DBPASSWD) {
        $this->user = $user;
        $this->pass = $pass;
        $this->dbhost = $dbhost;
        $this->dbname = $dbname;
        $this->connect();
    }   //END __construct()



/**
 * Connects to the main database for the application in the database server
 */
    public function connect() {
        try {
            $this->dbh = mysql_pconnect($this->dbhost, $this->user, $this->pass);
            mysql_select_db($this->dbname, $this->dbh);// or die ("<p>" . mysql_error() . "</p>");
        } catch (Exception $e) {
			//set_error_handler (array("Error_handler", "my_error_handler"));
        }
    }   //END connect()




    public function mysqliConnect(){
        return new mysqli($this->getDBHost(), $this->getUsername(), $this->getPassword(), $this->getDBName());
    }




/**
 * Connects to the payment records database for the application in the database server
 */
    public function payConnect() {
        try {
            $this->dbh = mysql_connect($this->dbhost, $this->user, $this->pass);

            mysql_select_db("mediluth_branchcollect", $this->dbh);// or die ("<p>" . mysql_error() . "</p>");
        } catch (Exception $e) {
			//set_error_handler (array("Error_handler", "my_error_handler"));
        }
    }   //END connect()


//lab modification ***********************************************************************************************
/**
 * Connects to the laboratory database for the application in the database server
 */
    public function labConnect() {
        try {
            //$this->dbh = mysql_pconnect($this->dbhost, $this->user, $this->pass);
			$this->dbname = 'mediluth_lab';
            mysql_select_db($this->dbname, $this->dbh);
        } catch (Exception $e) {
			set_error_handler (array("Error_handler", "my_error_handler"));
        }
    }   //END labConnect()
//lab modification ***********************************************************************************************




/**
 * Runs a query and returns a result set
 * @param string $query     Contains the query to be executed
 * @return resource         The result set retrieved after the execution of the query
 */
    public function execute($query, $debug = FALSE) {
		//echo "<p><pre>$query;</pre></p>";
        if ($debug){
            file_put_contents('debug-payment-query.txt', $query, FILE_APPEND);
        }
        try {
            if(!$this->dbh) {
                $this->connect();
            }
            $ret = mysql_query($query, $this->dbh);// or die("<p>$query</p>" . "<p>" . mysql_error() . "</p>");
            if(!$ret/* || !mysql_affected_rows ($this->getConnectionID()) > 0*/) {
                throw new Exception;
            }
            return $ret;
        } catch (Exception $e) {
			//echo("<font color=red><p>" . mysql_error() . "</p><pre>$query</pre></font>");
			//set_error_handler (array("Error_handler", "my_error_handler"));
        }
    }   //END execute()


    public function run($query){
        return $this->execute($query);
    }

    public function runQuery($query){
        return $this->execute($query);
    }


    public function getResultArray($result){
        $ret_val = array();
        try {
            if (!is_resource($result)){
                throw new Exception();
            }
            if ($this->hasRows($result)){
                while ($row = mysql_fetch_assoc($result)){
                    $ret_val[] = $row;
                }
            }
        } catch (Exception $e) {

        }
        return $ret_val;
    }   //END getResultArray()


    /*function getEtestResult($query) {
        try {
            $res = $this->execute($query);
            return mysql_fetch_array($res);
        } catch (Exception $e) {
			//set_error_handler (array("Error_handler", "my_error_handler"));
        }
    }*/



/**
 * Returns the database connection ID
 * @return resource     The database connection handle
 */
	function getConnectionID(){
		return $this->dbh;
	}   //END getConnectionID()





/**
 * Determines if one or more rows were affected after a query was executed
 * @param resource $result          Result set returned by execute()
 * @param int $expectedRows         An optional parameter that specifies the number of rows expected in the result set. Defaults to zero (0) in which case the number of expected rows is unlimited.
 * @return boolean                  true if the expected number of rows were affected, false otherwise
 */
	public function hasRows($result, $expectedRows = 0){
		if ($expectedRows){
			//The number of rows expected to be returned is known in advance (1 most times)
			if ($result && mysql_affected_rows($this->dbh) == $expectedRows)
				$retVal = true;
			else $retVal = false;
		} else {
				if ($result && mysql_affected_rows($this->dbh) > 0)
					$retVal = true;
				else $retVal = false;
			}
		return $retVal;
	}   //END hasRows()


/**
 * Gets the username to connect to the DB Server
 * @return string Returns the username to connect to the DB Server
 */
    public function getUsername(){
        return $this->user;
    }



/**
 * Gets the password to connect to the DB Server
 * @return string Returns the password to connect to the DB Server
 */
    public function getPassword(){
        return $this->pass;
    }



/**
 * Gets the Database Server Host
 * @return string Returns the DB Server Host to connect to the DB Server
 */
    public function getDBHost(){
        return $this->dbhost;
    }



/**
 * Gets the Database Name
 * @return string Returns the DB Name
 */
    public function getDBName(){
        return $this->dbname;
    }



/**
 * Gets the Chat Database Name
 * @return string Returns the DB Name
 */
    public function getChatDBName(){
        return $this->chatDBname;
    }





/**
 * Starts a database transaction
 */
    public function startTransaction(){
        $query = "START TRANSACTION";
        $this->execute($query);
    }


/**
 * Commits a database transaction
 */
    public function commitTransaction(){
        $query = "COMMIT";
        $this->execute($query);
    }


/**
 * Rolls back a database transaction
 */
    public function rollBackTransaction(){
        $query = "ROLLBACK";
        $this->execute($query);
        //die ("<p>Dieing in roll back...</p>");
    }




}   //END class
?>
