<?php
/**
 * Handles patient appointment
 * 
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */

/**
 * Handles patient appointment
 *
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */
class patAppointment {
    /**
     * @var array Stores the details of an appointment
     */
    public	$appoint = array();



/**
 * Class Constructor
 */
    function  __construct() {
        $this->conn = new DBConf();
    }   //END __construct()
	



 /**
  * Gets the details of an appointment
  * @param int $app_id      The ID of the appointment in the appointment table
  * @return array           The details of the appointment
  */
   function viewAppointment($app_id) {
        try {
            $query = "SELECT clinic_id, dept_id, patadm_id, 
							TIME_FORMAT(TIME(app_starttime), '%r') as starttime, 
							TIME_FORMAT(TIME(app_endtime), '%r') as endtime,
							DATE(app_starttime) as date, app_type
					  FROM appointment 
					  WHERE app_id = '".$app_id."' " ;
								
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);
     	    $num = mysql_num_rows($res) ;

            if ($res) {
				if($num>0){
					while($row = mysql_fetch_assoc($res)){
					extract($row);
					$app =  array('num' => $num,
								  'app_clinic' => $clinic_id, 
								  'app_dept' => $dept_id,
								  'app_patadm' => $patadm_id,
								  'app_start' => $starttime,
								  'app_end' => $endtime,
								  'app_type' => $app_type,
								  'app_date' => $date,
								  'app_id' =>  $app_id
									  );
						 }
				}else{
					$app =  array('num' => $num,
								  'app_clinic' => 0, 
								  'app_dept' => 0,
								  'app_patadm' => 0,
								  'app_start' => "",
								  'app_end' => "",
								  'app_type' => 0,
								  'app_date' => "",
								  'app_id' =>  0									  
									  );
				}
			return $app;
            }else return false;
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END viewAppointment()



/**
 * Inserts a new appointment into the appointment table
 * @return mixed    Returns the insert ID on success or false on failure
 */
	function insertAppointment(){
		try{
			$starttime = $this->appoint['date'].' '.$this->appoint['startime'].':00';
			$endtime = $this->appoint['date'].' '.$this->appoint['endtime'].':00';
		
            $query = "INSERT INTO `appointment` SET `patadm_id` = '".$this->appoint['patadm_id']."', 
												 	`consultant_id` = '".$this->appoint['consultant_id']."', 
													`app_starttime` = '".$starttime."', 
												 	`app_endtime` = '".$endtime."', 
													`app_status` = '0', 
												 	`user_id` = '".$this->appoint['user_id']."', 
													`dept_id` = '".$this->appoint['dept_id']."', 
												 	`clinic_id` = '".$this->appoint['clinic_id']."',
												  	`app_type` = '".$this->appoint['app_type']."' ";
							  
            $res = $this->conn->execute($query);
			if($res){
                $id = mysql_insert_id();
                return $id;
			} else return false;

		} catch (Exception $e) {
			set_error_handler (array("Error_handler", "my_error_handler"));
		}
	}   //END insertAppointment()



    /**
     * Gets the number of existing appointments for a particular day in a clinic
     * @param $forDate String The date for which the number of appointments should be retrieved. If empty $this->appoint['date'] is used.
     * @return int Returns the number of existing appointments for a particular day in a clinic
     */
    function getAppointmentCount($forDate = ""){
        $forDate = !empty($forDate) ? $forDate : $this->appoint['date'];
        $query = "SELECT COUNT(*) 'count' FROM appointment
                    WHERE clinic_id = '1'
                            AND DATE('" . $forDate . "') BETWEEN DATE(app_starttime) AND DATE(app_endtime)";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $row = mysql_fetch_array($result, MYSQL_ASSOC);
            $retVal = $row["count"];
        } else $retVal = 0;   //END if()
        return $retVal;
    }   //END getAppointmentCount()
	




/**
 * Updates the details of an appointment
 * @param int $app_id   The ID of the appointment in the appointment table
 * @return boolean      Returns true on success of false on failure
 */
	function updateAppointment($app_id){
		try{
			$starttime = $this->appoint['date'].' '.$this->appoint['startime'].':00';
			$endtime = $this->appoint['date'].' '.$this->appoint['endtime'].':00';
		
            $query = "UPDATE `appointment` SET 		`app_starttime` = '".$starttime."', 
												 	`app_endtime` = '".$endtime."', 
												  	`app_type` = '".$this->appoint['app_type']."'
											WHERE app_id = '".$app_id."'";
							  
            $res = $this->conn->execute($query);
			if($res){
			return TRUE; 
			
			}else return FALSE;

		}catch (Exception $e){
		
			set_error_handler (array("Error_handler", "my_error_handler"));
			
		}
	}   //END updateAppointment()



    public function setAppointmentStatus($appID, $status = 1){
        $appID = (int)$appID;
        $status = $status == 1 ? 1 : 0;
        $query = "UPDATE appointment
                    SET app_status = '$status'
                    WHERE app_id = '$appID'";
        $result = $this->conn->execute($query);
    }




/**
 * Searches for a future appointment of a patient
 * @param int $patadm_id            The ID of this particular medical case in the patient_admission table
 * @param int $status               An optional status of the appointment
 * @param int $consultant_id        An optional consultant ID that the patient is attached to
 * @return mixed                    Returns the appointment ID on success or false on failure
 */
   function checkAppointment($patadm_id, $status=NULL, $consultant_id =0 ) {
        try {
            $query = "SELECT app.app_id
					  FROM appointment app LEFT JOIN pat_transtotal pt
					  ON app.patadm_id = pt.patadm_id
					  WHERE app.patadm_id = '".$patadm_id."' AND DATE(app.app_starttime)> CURDATE() AND pt.pattotal_newstatus != ''" ;

			if($status != NULL)
				$query .= " AND pattotal_status='$status'";

			if($consultant_id != 0)
				$query .= " AND app.consultant_id='$consultant_id'";

			//die ("<pre>$query</pre>");

            //  $res = mysql_query($query);
            $res = $this->conn->execute($query);
     	    $num = mysql_num_rows($res) ;

            if ($res) {
				if ($num > 0){
					while ($row = mysql_fetch_assoc($res)) {
						$app_id = $row["app_id"];
					}
				} else {
					$app_id = 0;
				}
				return $app_id;
			} else return false;
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END checkAppointment()




/**
 * Executes the supplied query to get a drop-down list of all departments
 * @param string $query     The query that would be used to generate the list of departments
 * @return string           The generated HTML
 */
	public function getDepts($query){
		$result = $this->conn->execute($query);
		if ($result){
			$final = "<option value=\"All\">All</option>\n";
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$final .= "<option value=\"{$row['dept_id']}\" " . $this->selectMe($row['dept_id'], "dept") . ">{$row['dept_name']}</option>\n";
			return $final;
		} else return "<option value=\"\" disabled=\"disabled\">No Department Found</option>";
	}   //END getDepts()




/**
 * Generates a drop-down list of all consultants
 * @return string   The generated HTML of the drop-down list
 */
	public function getDoctors4DropDown(){
/*		$query = "SELECT  DISTINCT app.consultant_id, CONCAT_WS(' ', s.staff_title, UPPER(s.staff_surname), s.staff_othernames) doctor
					FROM consultant cons INNER JOIN staff s INNER JOIN appointment app
					ON app.consultant_id = cons.consultant_id AND cons.staff_employee_id = s.staff_employee_id
					ORDER BY s.staff_surname, s.staff_othernames";*/
		$query = "SELECT cons.consultant_id, CONCAT_WS(' ', s.staff_title, UPPER(s.staff_surname), s.staff_othernames) doctor
					FROM consultant cons INNER JOIN staff s
					ON cons.staff_employee_id = s.staff_employee_id
					ORDER BY s.staff_surname, s.staff_othernames";
		$result = $this->conn->execute($query);
		if ($result){
			$final = "<option value=\"All\">All</option>\n";
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$final .= "<option value=\"{$row['consultant_id']}\" " . $this->selectMe($row['consultant_id'], "doctor") . ">{$row['doctor']}</option>\n";
			return $final;
		} else return "<option value=\"\" disabled=\"disabled\">No Doctor Found</option>";
	}   //END getDoctors4DropDown()




/**
 * Gets a list of all the IDs of all consultants that have at least one appointment attached to them in the appointment table
 * @return mixed    Returns an array of the IDs on success or false on failure.
 */
	public function getDoctors(){
		$query = "SELECT DISTINCT consultant_id FROM appointment
					ORDER BY consultant_id";
		$result = $this->conn->execute($query);
		$final = array();
		if ($result){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				array_push ($final, $row["consultant_id"]);
			return $final;
		} else return false;
	}   //END getDoctors()




/**
 * Gets the name, department and unit of a consultant
 * @param string $query     The query that would be used to get the details of the consultant
 * @return mixed            The generated HTML of the doctor details on success or false on failure
 */
	public function getDoctorDeets($query){
		//echo "<pre>$query</pre>";
		$result = $this->conn->execute($query);
		if ($result){
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			$final = "<div><table width=\"100%\" cellpadding=\"3\" cellspacing=\"3\" border=\"0\" height = \"30\" style=\"border-collapse: collapse;\">";
			$final .= "<tr>
							<td width=\"60%\">NAME: {$row['doctor']}</td>
							<td width=\"20%\" align=\"right\">{$row['department']}</td>
							<td width=\"20%\" align=\"left\"> ({$row['unit']})</td>
						</tr>";
			$final .= "</table></div>";
			return $final;
		} else return false;
	}   //END getDoctorDeets()




/**
 * Gets the name, department and unit of a consultant
 * @param string $fQuery     The query (which would be further modified in this method) that would be used to get the details of the consultant
 * @param int $consultantID  The ID of the consultant in the consultants table whose details is to be retrieved
 * @return mixed             The generated HTML of the doctor details on success or false on failure
 */
	public function getDoctorDeets_new($fQuery, $consultantID){
		//echo "<pre>$query</pre>";
		//Get the staff_employee_id from the consultantID
		$query = "SELECT staff_employee_id FROM consultant
					WHERE consultant_id = '$consultantID'"; //echo "<pre>$query</pre>";
		$result = $this->conn->execute ($query);
		if ($this->conn->hasRows($result)){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			$staffID = $row["staff_employee_id"];
		} else $staffID = 0;
		
		$fQuery = sprintf($fQuery, $staffID);
		$result = $this->conn->execute($fQuery); //echo "<pre>$fQuery</pre>";
		if ($result){
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			$final = "<div><table width=\"100%\" cellpadding=\"3\" cellspacing=\"3\" border=\"0\" height = \"30\" style=\"border-collapse: collapse;\">";
			$final .= "<tr>
							<td width=\"60%\">NAME: {$row['doctor']}</td>
							<td width=\"20%\" align=\"right\">{$row['department']}</td>
							<td width=\"20%\" align=\"left\"> ({$row['unit']})</td>
						</tr>";
			$final .= "</table></div>";
			return $final;
		} else return false;
	}   //END getDoctorDeets_new()




/**
 * Generates the string selected="selected" which would be added to the list of attributes of an option tag in a drop-down list
 * @param string $value     The value that would be checked if equal to the value of the current item
 * @param string $field     The name of the field to check in the $_POST table
 * @return string           Returns selected="selected" if the value of the checked field matches $value, else ""
 */
	protected function selectMe($value, $field){
		if ($_POST && isset($_POST["$field"])){
			if ($value == $_POST["$field"])
				return "selected=\"selected\"";
		} else return "";
	}   //END selectMe()



/**
 * Gets all future appointment dates (with respect to the current date) in the appointment table
 *
 * @param $whichDate    The date to select in the drop-down box
 * @return string       The generated HTML drop-down box containing the list of dates
 */
	public function getDates($whichDate){
		//Date:	treatment
		$query = "SELECT DISTINCT app_starttime FROM appointment
					WHERE date(app_starttime) >= CURDATE()	
					ORDER BY app_starttime ";
		$result = $this->conn->execute($query);
		if ($result){
			if ($_POST)
				$final = "<option value=\"Today\" " . $this->selectMe("Today", $whichDate) . ">Today</option>
							<option value=\"All\" " . $this->selectMe("All", $whichDate) . ">All</option>";
			else $final = "<option value=\"Today\" selected=\"selected\">Today</option>
							<option value=\"All\" >All</option>";
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$final .= "<option value=\"{$row['app_starttime']}\"" . $this->selectMe($row['app_starttime'], $whichDate) . ">{$row['app_starttime']}</option>\n";
			return $final;
		} else return "<option value=\"\" disabled=\"disabled\">No Treatment Yet</option>";
	}   //END getDates()



/**
 * Generates the HTML that is used to display the appointment list
 * @global string $status1
 * @global string $status2
 * @param string $query     The main query that is used to generate the appointment list
 * @return string           THe generated HTML that is used to display the appointment list
 */
	public function getResults($query){
	global $status1, $status2;
//        echo ("In class... <pre>$query</pre>");
		$result = $this->conn->execute($query);
//        while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
//				echo ("<pre>" . print_r($row, true) . "</pre>");
//        die ("...END...");
	//Now build the table to be displayed
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
/*			//Get the next record to start from
			if (isset($_GET["start"]))
				$start = (int)$_GET["start"];
			else $start = 0;
			$tBuilder = new tableBuilder($query);
			return $tBuilder->buildTable(false, $start);*/

			$serialNo = 0;
			$tableBody = "";
			$user = new patUsers();
			
			//Determine if the current user is a consultant or health records official
			$staffID = $user->getEmployeeIDfromUserID($_SESSION[session_id() . "userID"]);	//Get the staff_employee_id of the currently logged-in user


            //Get the staff ID of the consultant to which this doctor is assigned (if any)
            $thisDoc = new Doctor("lang1", $_SESSION[session_id() . "staffID"]);
            $consultantEmpIDAssignedTo = $thisDoc->getConsultantEmpIDAttachedTo();
            $staffID = !empty($consultantEmpIDAssignedTo) ? $consultantEmpIDAssignedTo : $staffID;


			$isConsultant = is_array($user->isConsultant($staffID)) ? true : false;
			$juggleRows = true;
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				//echo ("<pre>" . print_r($row, true) . "</pre>");
				$link = "index.php?p=casenote&m=patient_care&aa={$row['app_id']}";
				$rowClass = $juggleRows ? " class=\"tr-row2\"" : " class=\"tr-row\"";
				$juggleRows = !$juggleRows;
				$tableBody .= "<tr $rowClass>
									<td>" . (++$serialNo) . ".</td>";

				//Determine whether to show treatment link or not by comparing the appointment date to today's date
				if ($row["app_status"] == "0"){
					$date_today = strtotime(date("Y-m-d"));	//Get today's timestamp
					$date_app = strtotime($row["date"]);	//Get the appointment date's timestamp
					$dateDiff = $date_app - $date_today;	//Difference of the two timestamps above
					$appStatus = $dateDiff < 0 ? "Appointment Missed" : ($row["paid"] ? "Pending" : "Payment Pending");
				} else $appStatus = "Treated";

				//For consultants
				if ($isConsultant){
					if ($row["app_status"] == "0" && $row["paid"]){	//Patient not treated yet, enable treatment link
						$tableBody .= "<td><a target=\"_blank\" href=\"$link\">{$row['hospital_no']}</a></td>
										<td><a target=\"_blank\" href=\"$link\">{$row['name']}</a></td>";
					} else {	//Patient treated .:. no link
							$tableBody .= "<td>{$row['hospital_no']}</td>
											<td>{$row['name']}</td>";
					}
				} else {	//For health records official
						$tableBody .= "<td>{$row['hospital_no']}</td>
										<td>{$row['name']}</td>";
					}
				$tableBody .=	"	<td>{$row['sex']}</td>
									<td>{$row['age']}</td>
									<td>{$row['date']}</td>
									<td>{$row['starttime']}</td>
									<td>{$row['endtime']}</td>
									<td>{$row['appoint_type']}</td>
									<td>$appStatus</td>
								</tr>";
			}
			
		} else $tableBody = false;	//END if (rows returned)
		//file_put_contents ("appoint.txt", $tableBody);
		//exit();
		return $tableBody;
	}	//END getResults()




/**
 * Generates the HTML that is used to display the appointment list
 * @global string $status1
 * @global string $status2
 * @param string $query     The main query that is used to generate the appointment list
 * @return string           THe generated HTML that is used to display the appointment list
 */
	public function getResults4DataEntry($query){
	global $status1, $status2;
//        echo ("In class... <pre>$query</pre>");
		$result = $this->conn->execute($query);
//        while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
//				echo ("<pre>" . print_r($row, true) . "</pre>");
//        die ("...END...");
	//Now build the table to be displayed
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
/*			//Get the next record to start from
			if (isset($_GET["start"]))
				$start = (int)$_GET["start"];
			else $start = 0;
			$tBuilder = new tableBuilder($query);
			return $tBuilder->buildTable(false, $start);*/

			$serialNo = 0;
			$tableBody = "";
			$user = new patUsers();

			//Determine if the current user is a consultant or health records official
			$staffID = $user->getEmployeeIDfromUserID($_SESSION[session_id() . "userID"]);	//Get the staff_employee_id of the currently logged-in user
			//$isConsultant = is_array($user->isConsultant($staffID)) ? true : false;
			$juggleRows = true;
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				//echo ("<pre>" . print_r($row, true) . "</pre>");
				//$link = "index.php?p=casenote&m=patient_care&aa={$row['app_id']}";
                $link = "index.php?p=treatmentsheet_entry&m=patient_care&aa={$row['app_id']}";
				$rowClass = $juggleRows ? " class=\"tr-row2\"" : " class=\"tr-row\"";
				$juggleRows = !$juggleRows;
				$tableBody .= "<tr $rowClass>
									<td>" . (++$serialNo) . ".</td>";

				//Determine whether to show treatment link or not by comparing the appointment date to today's date
				if ($row["app_status"] == "0"){
					$date_today = strtotime(date("Y-m-d"));	//Get today's timestamp
					$date_app = strtotime($row["date"]);	//Get the appointment date's timestamp
					$dateDiff = $date_app - $date_today;	//Difference of the two timestamps above
					$appStatus = $dateDiff < 0 ? "Appointment Missed" : ($row["paid"] ? "Pending" : "Payment Pending");
				} else $appStatus = "Treated";

				//For consultants
				if (true /*$isConsultant*/){
					//if (/*$row["app_status"] == "1" &&*/ $row["paid"]){	//Patient not treated yet, enable treatment link
                    if ($appStatus == "Treated" || $appStatus == "Pending"){	//Patient not treated yet, enable treatment link
						$tableBody .= "<td><a href=\"$link\">{$row['hospital_no']}</a></td>
										<td><a href=\"$link\">{$row['name']}</a></td>";
					} else {	//Patient treated .:. no link
							$tableBody .= "<td>{$row['hospital_no']}</td>
											<td>{$row['name']}</td>";
					}
				} else {	//For health records official
						$tableBody .= "<td>{$row['hospital_no']}</td>
										<td>{$row['name']}</td>";
					}
				$tableBody .=	"	<td>{$row['sex']}</td>
									<td>{$row['age']}</td>
									<td>{$row['date']}</td>
									<td>{$row['starttime']}</td>
									<td>{$row['endtime']}</td>
									<td>{$row['appoint_type']}</td>
									<td>$appStatus</td>
								</tr>";
			}

		} else $tableBody = false;	//END if (rows returned)
		//file_put_contents ("appoint.txt", $tableBody);
		//exit();
		return $tableBody;
	}	//END getResults4DataEntry()




/**
 * Gets the appointment list for the current date for nurses with links to the vital signs page included
 * @param string $query     The main query that is used to generate the appointment list
 * @return string           The generated HTML that shows the appointment list for nurses
 */
	public function getAppointments4Nurses($query, $clinicID){
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			$retVal = "";
			$sNo = 0;
			$juggleRows = false;
			$vitalSigns = new patVitalSigns();
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				//Determine whether to show treatment link or not by comparing the appointment date to today's date
				if ($row["app_status"] == "0"){
					$date_today = strtotime(date("Y-m-d"));	//Get today's timestamp
					$date_app = strtotime($row["date"]);	//Get the appointment date's timestamp
					$dateDiff = $date_app - $date_today;	//Difference of the two timestamps above
					$appStatus = $dateDiff < 0 ? "Appointment Missed" : "Pending";
				} else $appStatus = "Treated";

				//Init needed variables
				$link = "index.php?p=vitalsigns_entry&m=patient_care&id={$row['app_id']}&dc=$clinicID";
				$rowClass = $juggleRows ? " class=\"tr-row2\"" : " class=\"tr-row\"";
				$juggleRows = !$juggleRows;
				$vitalSignsStatus = $vitalSigns->getVitalSignsStatus($row["patadm_id"]);
				$vitalSignsStatusText = $vitalSignsStatus ? "Already Taken" : ($row["paid"] ? "Pending" : "Payment Pending");
				$editText1 = $vitalSignsStatus ? " [Edit] " : "";
				$editText2 = $vitalSignsStatus ? " [Edit] " : "";

				/*$retVal .= "<tr $rowClass>
								<td>" . (++$sNo) . ".</td>
								<td><a target=\"_blank\" href=\"$link\">{$row['hospital_no']}</a>$editText</td>
								<td><a target=\"_blank\" href=\"$link\">{$row['name']}</a> $editText</td>
								<td>{$row['sex']}</td>
								<td>{$row['age']}</td>
								<td>{$row['date']}</td>
								<td>{$row['starttime']}</td>
								<td>{$row['endtime']}</td>
								<td>{$row['appoint_type']}</td>
								<td>$vitalSignsStatusText</td>
							</tr>";*/
				$retVal .= "<tr $rowClass>
								<td>" . (++$sNo) . ".</td>";
                if ($row["paid"]){
                    $retVal .= "<td><a target=\"_blank\" href=\"$link\">{$row['hospital_no']}</a> <span id=\"showEdit{$row['app_id']}_1\" class=\"redText\">$editText1</span></td>
                                <td><a target=\"_blank\" href=\"$link\">{$row['name']}</a> <span id=\"showEdit{$row['app_id']}_2\" class=\"redText\">$editText2</span></td>";
                } else {
                        $retVal .= "<td>{$row['hospital_no']} <span id=\"showEdit{$row['app_id']}_1\" class=\"redText\">$editText1</span></td>
                                    <td>{$row['name']} <span id=\"showEdit{$row['app_id']}_2\" class=\"redText\">$editText2</span></td>";
                }
				$retVal .= "    <td>{$row['sex']}</td>
								<td>{$row['age']}</td>
								<td>{$row['date']}</td>
								<td>{$row['starttime']}</td>
								<td>{$row['endtime']}</td>
								<td>{$row['appoint_type']}</td>
								<td><span id=\"appstatus{$row['app_id']}\">$vitalSignsStatusText</span></td>
							</tr>";
			}	//END while
		} else $retVal = false;
		return $retVal;
	}	//END getAppointments4Nurses()





/**
 * Gets the details of the patient who has an appointment with ID $appID
 * 
 * @param int $appID        The appointment ID under consideration
 * @param int $clinicID     The clinic ID
 * @return mixed            Returns an array containing the details on success or false on failure
 */
	public function getPatAppointmentDeets($appID, $clinicID){
		$appID = admin_Tools::doEscape($appID, $this->conn);
		$clinicID = admin_Tools::doEscape($clinicID, $this->conn);
		$query = "SELECT CONCAT_WS(' ', UPPER(reg.reg_surname), reg.reg_othernames) AS 'patName', reg.reg_hospital_no, app.patadm_id
					FROM appointment app INNER JOIN patient_admission pat INNER JOIN registry reg
					ON app.patadm_id = pat.patadm_id AND pat.reg_hospital_no = reg.reg_hospital_no
					WHERE app.app_id = '$appID' 
						AND DATE(app_starttime) = CURDATE() 
						AND app.clinic_id = '$clinicID'";	//die ("<pre>$query</pre>");
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result, 1)){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			$retVal = array(
							"patDeets" => "{$row['patName']} [HOSPITAL NO.: {$row['reg_hospital_no']}]",
							"patAdmID" => $row["patadm_id"]
						);
		} else $retVal = false;
		return $retVal;
	}   //END getPatAppointmentDeets()
    
    
    
    public function getAppointment($appID){
		$appID = (int)$appID;
		$query = "SELECT app.*, CONCAT_WS(' ', UPPER(reg.reg_surname), reg.reg_othernames) AS 'patName', reg.reg_hospital_no, app.patadm_id,
                    DATE_FORMAT(app_starttime, '%M %d, %Y') 'appDate'
					FROM appointment app INNER JOIN patient_admission pat INNER JOIN registry reg
					ON app.patadm_id = pat.patadm_id AND pat.reg_hospital_no = reg.reg_hospital_no
					WHERE app.app_id = '$appID' ";
//        die ("<pre>$query</pre>");
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result, 1)){
			$retVal = mysql_fetch_array ($result, MYSQL_ASSOC);
//			$retVal = array(
//							"patDeets" => "{$row['patName']} [HOSPITAL NO.: {$row['reg_hospital_no']}]",
//							"patAdmID" => $row["patadm_id"]
//						);
		} else $retVal = array();
		return $retVal;
	}   //END getPatAppointmentDeets()

    
    
    public function getLastPatAdmID($hospital_no){
        $query = "SELECT MAX(patadm_id) 'max_patadmid' FROM patient_admission 
                    WHERE reg_hospital_no = '$hospital_no'";
        $result = $this->conn->execute($query);
		if ($this->conn->hasRows($result, 1)){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
            $ret_val = $row['max_patadmid'];
        } else $ret_val = 0;
        return $ret_val;
    }   //END getLastPatAdmID()
    
    
    


}   //END class

?>
