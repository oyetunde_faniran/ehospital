<?php
/**
 * Various functions for Admission mgt, wards mgt,service mgt, invoicing and payment engine
 * @package adminssion
 * @author: Ahmed Rufai
 * 
 * Create Date: 09-11-2009
 * 
 * 
 */
require_once './classes/DBConf.php';

Class patient_admission {
/**
 *
 * @var array collection of table fields
 */
public $pat=array() ;
/**
 *
 * @var array collection of corresponding values for the table field
 */
public $pat2=array() ;
	/**
     * class constructor
     */
	function  __construct() {
        $this->conn = new DBConf();
    }
    

    /**
     * New object to the class. 
     *
     */
	public function New_patient_admission($reg_hospital_no,$ctc_id){
		$this->reg_hospital_no = $reg_hospital_no;
		$this->ctc_id = $ctc_id;
	}

    /**
      /**
     *  loads hospital number and condition type category ID(ctc_id) that match the medical trans ID ($key_row) into the class;
     * @param integer $key_row current medical trans ID
     *
     */
	public function Load_from_key($key_row){
		$result = $this->conn->execute("Select * from patient_admission where patadm_id = \"$key_row\" ");
		while($row = mysql_fetch_array($result)){
			$this->patadm_id = $row["patadm_id"];
			$this->reg_hospital_no = $row["reg_hospital_no"];
			$this->ctc_id = $row["ctc_id"];
		}
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param integer $key_row row ID of record to be deleted
     *
     */
	public function Delete_row_from_key($key_row){
		$this->conn->execute("DELETE FROM patient_admission WHERE patadm_id = $key_row");
	}

    /**
     * Update the active row that matches the supplied ID  on table
     *
     * @param integer $id
     */
	public function Save_Active_Row($id){
		
	
	try {
	
	$sql = "UPDATE patient_admission SET ";	
	
				$qq=count($this->pat);
				$q=count($this->pat);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
			
         			    $sql.=    $this->pat[$i] .'="'.$this->pat2[$i].'" ';
			 
					 }
					 else{
						  $sql.=      $this->pat[$i] .'="'.$this->pat2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			$result2 =$this->conn->execute("SELECT * from patient_admission");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
	
         $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
	
	
	
	}

    /**
      * Save the active pairs of field and value  as a new row on table
     */
	public function Save_Active_Row_as_New(){
		
	try {
		$sql = "INSERT INTO patient_admission SET ";	
	
				$qq=count($this->pat);
				$q=count($this->pat);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
			
         			    $sql.=    $this->pat[$i] .'="'.$this->pat2[$i].'" ';
			 
					 }
					 else{
						  $sql.=      $this->pat[$i] .'="'.$this->pat2[$i].'" ,';
			
					 }
  				 $q--;
 			}
	
	// echo $sql;
       $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
	
	
	}
    /**
     * returns condition type category description that matches supplied ID
     * @param integer $id
     * @return string
     */
function getContypecat_name($id) {
        try {
           $sql = 'Select * from conditiontypecat  where ctc_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
            return $row['ctc_name'];
				 
            
			
        } catch (Exception $e) {


        }
    }
    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */

public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->connection->RunQuery("SELECT patadm_id from patient_admission order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["patadm_id"];
				$i++;
			}
	return $keys;
	}
	/**
     * returns all rows from a specified table
     */
function allrows() {
        
		try {
	$db2 = new DBConf();
		 $sql = 'SELECT
*
FROM
patient_admission,conditiontypecat
WHERE
patient_admission.ctc_id =  conditiontypecat.ctc_id
';
$pagelink32='pat';
$pager = new PS_Pagination($db2,$sql,2,10,$pagelink32);
$rs = $pager->paginate();
           

           // $res = $this->conn->execute($sql);
			$i=1;
            while ($row = mysql_fetch_assoc($rs)) {
             
			 $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["patadm_id"].'" /></td>
        <td>'.$row["patadm_id"].'</td>
		<td>'.$row["reg_hospital_no"].'</td>
		<td>'.$cat_name.'</td>
        
        <td>'.'<a href = "./index.php?jj=delete&p=pat&id='.$row["patadm_id"] .'"><img src="./images/btn_delete_02.gif"  border=0/></a></td><td>'.'<a href = "./index.php?p=editpat&patadm_id='.$row["patadm_id"] .'"><img src="./images/btn_edit.gif"  border=0/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
        } catch (Exception $e) {
	
        }

  
    }
	/**
     * returns rows that match specified condition
     * @param string $search_field field in a table
     * @param string $search_value value to search for
     */
	function rowSearch($search_field,$search_value){
		 try {
		 $db2 = new DBConf();
		$sql = "Select * from  patient_admission  where  ".$search_field."='".$search_value."'" ;
		$res =$this->conn->execute($sql);
		
		$pagelink32='pat';
$pager = new PS_Pagination($db2,$sql,2,10,$pagelink32);
$rs = $pager->paginate();
	  // echo "entered";
	     $i=1;
		while ($row = mysql_fetch_array($rs)) {
             
			 $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["patadm_id"].'" />'.$ii.'</td>
        <td>'.$row["patadm_id"].'</td>
		<td>'.$row["reg_hospital_no"].'</td>
		<td>'.$cat_name.'</td>
        
        <td>'.'<a href = "./index.php?jj=delete&p=pat&id='.$row["patadm_id"] .'"><img src="./images/btn_delete_02.gif"  border=0/></a></td><td>'.'<a href = "./index.php?p=editpat&patadm_id='.$row["patadm_id"] .'"><img src="./images/btn_edit.gif"  border=0/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
		} catch (Exception $e) {
        }
	}
    /**
     * returns
     */
	function getCondtypecat() {
        try {
            $sql = 'select ctc_id,ctc_name from conditiontypecat ';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                echo '<option value ="'.$row['ctc_id'] .'">'.$row['ctc_name'].'</option>';
   
            }
        } catch (Exception $e) {


        }
    }
/**
 * populates option tags of a specified select element with condition type category name(ctc_name) and highlight one corresponding to the supplied patadm_id ($id)ID
 * @param integer $id medical trans ID
 */
function getCondtypecat2($id) {
        try {
			$sql2 = 'select ctc_id from patient_admission where patadm_id ='.$id;
				 $res2 = $this->conn->execute($sql2);
                 $row2 = mysql_fetch_array($res2);
				 if($row2)
				  $id2=$row2['ctc_id'];
		
            $sql = 'select ctc_id,ctc_name from conditiontypecat ';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
			$sel= ($id2==$row['ctc_id'])? "selected":" ";
			
                echo '<option value ="'.$row['ctc_id'] .'">'.$row['ctc_name'].'</option>';
   
            }
        } catch (Exception $e) {
        }
    }
/**
 *
 */
function get_admNodropdown() {
        try {
           $sql = 'Select * from patient_admission';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
            echo '<option value ='.$row['patadm_id'] .' >'.$row['reg_hospital_no'].'</option>';
				 
            }
			
        } catch (Exception $e) {


        }
    }
	function get_admNodropdown2($id) {
        try {
                
				 $sql2 = 'Select * from inpatient_admission where inp_id ='.$id;
				 $res2 = $this->conn->execute($sql2);
				 if( $row2 = mysql_fetch_array($res2))
				  $id2=$row2['patadm_id'];
            $sql = 'select * from patient_admission ';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
             	
				
				
				$sel= ($id2==$row['patadm_id'])? "selected":" ";
				 //if()
				
				
				 
	echo '<option value ='.$row['patadm_id'] .'  ' .$sel.' >'.$row['reg_hospital_no'].'</option>';
				 
            }
			
        } catch (Exception $e) {
        }
    }
	/**
     * returns patient hospital number for a specified  medical trans ID
     * @param integer $id medical trans ID
     * @return string hospital number
     *
     */
function gethospital_name($id) {
        try {
           $sql = 'Select * from patient_admission  where patadm_id='.$id;
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
			//echo $sql ;
            return $row['reg_hospital_no'];
			}	 
            			
        } catch (Exception $e) {


        }
    }
   /**
    * returns purpose of an admission e.g Tuberculosis patient, regular admission
    * @param integer $id medical trans ID (patadm_id)
    * @return string
    */
function getDeposit_purpose($id) {
        try {
           $sql = "Select max(patadm_id),patadm_purpose from patinp_account where patadm_id='$id' AND pattrans_no IS NOT NULL";
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
			//echo $sql ;
            return $row['patadm_purpose'];
			}	 
            
			
        } catch (Exception $e) {


        }
    }
    /**
     * returns patient full name and hospital no based on supplied hospital number
     * @param integer $id medical trans ID
     * @return string patient full name concatenated with hospital number
     */

function getPatient_name($id) {
        try {
		 $id2=$this->gethospital_name($id);
           $sql = "Select reg_hospital_no, concat_ws(' ',reg_surname,reg_othernames) AS fullname from registry  where  
		   reg_hospital_no='$id2'";
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
		    $fullname=$row['fullname'];
            return $fullname.'<br \>  Hospital No.:  '.$row['reg_hospital_no'];
			//echo $sql ;
			}	 
          } catch (Exception $e) {
        }
    }

/**
 *
  * returns patient full name only based on supplied hospital number
     * @param integer $id medical trans ID
     * @return string patient full name only
 */
function getPatient_nameonly($id) {
        try {
		 $id2=$this->gethospital_name($id);
           $sql = "Select reg_hospital_no,concat_ws(' ',reg_surname,reg_othernames) AS fullname from registry  where  
		   reg_hospital_no='$id2'";
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
		    $fullname=$row['fullname'];
            return $fullname;
			//return  $sql ;
			}	 
          } catch (Exception $e) {
        }
    }
/**
      * returns patient surname only based on supplied hospital number
     * @param string $id hospital number
     * @return string patient surname only
 */
function getPatient_surname($id) {
        try {
           $sql = "Select reg_surname AS surname from registry  where  
		   reg_hospital_no='$id'";
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
		    $surname=$row['surname'];
            return $surname;
			//return  $sql ;
			}	 
          } catch (Exception $e) {
        }
    }
/**
 *
 * returns patient othername only based on supplied hospital number
     * @param string $id hospital number
     * @return string patient othername only
 */
function getPatient_othernames($id) {
        try {
           $sql = "Select reg_othernames AS othernames from registry  where  
		   reg_hospital_no='$id'";
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
		    $othernames=$row['othernames'];
            return $othernames;
			//return  $sql ;
			}	 
          } catch (Exception $e) {
        }
    }
/**
 *returns transaction number
 * @return string
 */
function get_tran_id()
{
$tran_id = "";
$tran_id .= $this->randomString(rand(6, 16));
return $tran_id;
}

/**
 * returns randomised string
 * @param strting $randStringLength
 * @return string
 */
function randomString($randStringLength)
	{
	$timestring = microtime();
	$secondsSinceEpoch=(integer) substr($timestring, strrpos($timestring, " "), 100);
	$microseconds=(double) $timestring;
	$seed = mt_rand(0,1000000000) + 10000000 * $microseconds + $secondsSinceEpoch;
	mt_srand($seed);
	$randstring = "";
	for($i=0; $i < $randStringLength; $i++)
		{
		$randstring .= mt_rand(0, 9);
		}
	return($randstring);
	}
	/**
     * returns patience address
     * @param integer $id med trans ID number
     * @return string
     */
function getPatient_address($id) {
        try {
		 $id2=$this->gethospital_name($id);
           $sql = "Select * from registry  where reg_hospital_no='$id2'";
            $res = $this->conn->execute($sql);
			  
				 
            if($res){
			$row = mysql_fetch_array($res);
			//echo $sql ;
			$reg_address=$row['reg_address'];
            return $reg_address;
			}
        } catch (Exception $e) {
        }
    }
   /**
     * returns patience age
     * @param integer $id med trans ID number
     * @return integer
     */
	
function getPatient_age($id) {
        
		try {
		 $id2=$this->gethospital_name($id);
           $sql = "Select year(curdate())- year(reg_dob) as dob1 from registry  where reg_hospital_no='$id2'";
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
			//echo $sql ;
			$reg_dob=$row['dob1'] ;
            return $reg_dob;
		}
			
        } catch (Exception $e) {
        }
    }
	/**
     * returns patience gender
     * @param integer $id med trans ID number
     * @return integer
     */

function getPatient_gender($id) {
        try {
		 $id2=$this->gethospital_name($id);
           $sql = "Select * from registry  where reg_hospital_no='$id2'";
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
			//echo $sql ;
			$reg_gender=$row['reg_gender'] ;
            return $reg_gender;
				 
            }
			
        } catch (Exception $e) {
        }
    }
	
/**
 * returns quantity of drugs prescribed by the doctor over a medical trans ID (patadm_id)
 * @param integer $id med trans ID
 * @return integer
 */
function getPatient_DrugQty($id) {
        try {
		 //$id2=$this->gethospital_name($id);
           $sql = "SELECT count(cp.payflag) AS qtydrug FROM casenote c
		    INNER JOIN 	casenote_prescription  cp 
			 ON  c.casenote_id=cp.casenote_id WHERE cp.payflag='0' AND c.patadm_id='$id'";
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
			//echo $sql ;
			$qtydrug=$row['qtydrug'] ;
            return $qtydrug;
			}
		} catch (Exception $e) {
        }
    }

/**
 * returns number of tests prescribed by the doctor over a medical trans ID (patadm_id)
 * @param integer $id med trans ID
 * @return integer
 */

function getPatient_testQty($id) {
        try {
		 //$id2=$this->gethospital_name($id);
           $sql = "SELECT count(cl.casenote_payflag) AS qtytest FROM casenote c INNER JOIN casenote_labtest cl  ON   c.casenote_id=cl.casenote_id WHERE cl.casenote_payflag='0' AND c.patadm_id='$id'";
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
			//echo $sql ;
			$qtytest=$row['qtytest'] ;
            return $qtytest;
			}
		} catch (Exception $e) {
        }
    }

/**
 * returns clinic name
 * @param integer $id med trans ID
  * @param string $curLnag_fie ld column in the language table that contains current language text
 * @param string $langtb  language table
 * @return string
 */
function getClinic_name($id,$curLnag_field,$langtb) {
        try {
		 $getlang= new language();
		 $id2=$this->getNurse_id($id);
           $sql = "Select * from clinic  where clinic_id='$id2'";
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
			//echo $sql ;
			$clinic_name=$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
            return $clinic_name;
				} 
            
			
        } catch (Exception $e) {
        }
    }
    /**
     * returns consultant name
     * @param integer $id med trans ID
      * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     * @return string
     */
	function getConsultant_name($id,$curLnag_field,$langtb) {
        try {
		 //$getlang= new language();
		 $id2=$this->getPatient_type($id);
		 
		 if($id2==2)
			 $tabl="appointment";
		 elseif($id2==3)
		 	$tabl="appointment";
		 else
		 	$tabl="emergency";
		 
		 $sql = "SELECT unkn.consultant_id,c.staff_employee_id, concat_ws(' ',s.staff_title,s.staff_surname,staff_othernames) AS consultant FROM $tabl unkn, consultant c, staff s WHERE unkn.consultant_id=c.consultant_id  AND
		   	s.staff_employee_id=c.staff_employee_id 	AND
		    unkn.patadm_id=".$id2;
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
			//echo $sql ;
			$consultant_name=$row['consultant'];
            return $consultant_name;
				 
            }
			
        } catch (Exception $e) {
        }
    }
    /**
     * returns consultant ID
     * @param string $id hospital number
       * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     * @return integer
     */
	function getConsultant_id($id,$curLnag_field,$langtb) {
        try {
		 //$getlang= new language();
		 $id2=$this->getPatient_type($id);
		 $patadm_id=$this->getCurrentmedical_trans_id($id);
		// die($id2);
		 if($id2==2)
			 $tabl="appointment";
		 elseif($id2==3)
		 	$tabl="appointment";
		 else
		 	$tabl="emergency";
		 
		 $sql = "SELECT unkn.consultant_id,c.staff_employee_id, concat_ws(' ',s.staff_title,s.staff_surname,staff_othernames) AS consultant FROM $tabl unkn, consultant c, staff s WHERE unkn.consultant_id=c.consultant_id  AND
		   	s.staff_employee_id=c.staff_employee_id 	AND
		    unkn.patadm_id=".$patadm_id;
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
			//echo $sql ; exit();
			$consultant_id=$row['consultant_id'];
           return $consultant_id;
		   //echo  $sql;
				 
            }
			
        } catch (Exception $e) {
        }
    }
    /**
     *
      * @param string $id hospital no
       * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     * @return string
     */
	function getConsultant_name2($id,$curLnag_field,$langtb) {
        try {
		 //$getlang= new language();
		 $id2=$this->getPatient_type2($id);
		 $patadm_id=$this->getCurrentmedical_trans_id($id);
		 if($patadm_id>0){
				 if($id2==2 ){
					$tabl="appointment";
					$tabl2="Inpatient";
				 }elseif($id2==3){
				     $tabl="appointment";
					 $tabl2="Outpatient";
				 }else{	
				    $tabl="emergency";
					$tabl2="No Attached Consultant";
					
					}
				 $sql = "SELECT unkn.consultant_id,c.staff_employee_id, concat_ws(' ',s.staff_title,s.staff_surname,staff_othernames) 
				 AS consultant FROM $tabl unkn, consultant c, staff s WHERE unkn.consultant_id=c.consultant_id  AND
					s.staff_employee_id=c.staff_employee_id AND
					unkn.patadm_id='$patadm_id'";
					
					$res = $this->conn->execute($sql);
					if($res){
					$row = mysql_fetch_array($res);
					
					$consultant_name=$row['consultant'];
					//if($tabl2!='No Attached Consultant')
						return $consultant_name;
					//else
						//return $tabl2;	 
					}
			}
        } catch (Exception $e) {
        }
    }
    /**
     * returns consultant name
     * @param string $id hospital no
    * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     * @return string
     */
	function getClinic_name2($id,$curLnag_field,$langtb) {
        try {
		 //$getlang= new language();
		 $id2=$this->getPatient_type2($id);
		 //echo "qqqq".$id2;
		 //exit();
		 $patadm_id=$this->getCurrentmedical_trans_id($id);
		 if($patadm_id>0){
				 if($id2==2 ){
					$tabl="appointment";
					$tabl2="Inpatient";
				 }elseif($id2==3){
				     $tabl="appointment";
					 $tabl2="Outpatient";
				 }else{	
				    $tabl="emergency";
					$tabl2="No Attached Consultant";
					
					}
				 $sql = "SELECT c.clinic_id FROM $tabl unkn, consultant c, staff s WHERE unkn.consultant_id=c.consultant_id  AND
					s.staff_employee_id=c.staff_employee_id AND
					unkn.patadm_id=".$patadm_id;
				
					$res = $this->conn->execute($sql);
					$n=mysql_num_rows($res);
					if($n>0){
					$row = mysql_fetch_array($res);
					
					$clinic_id=$row['clinic_id'];
					//		if($tabl2!='A & E Clinic'){
							  $clinic= new clinic(); 
								
								return $clinic->getClinic_name($clinic_id,$curLnag_field,$langtb);;
								
						//	}else
							//	return $tabl2;	 
					}
			}
        } catch (Exception $e) {
        }
    }
    /**
     *
     * returns consultant ID
     * @param string $id hospital number ID
       * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     * @return integer
     */
	function getClinic_id2($id,$curLnag_field,$langtb) {
        try {
		 //$getlang= new language();
		 $id2=$this->getPatient_type2($id);
		 $patadm_id=$this->getCurrentmedical_trans_id($id);
		 if($patadm_id>0){
				 if($id2==2){
					$tabl="appointment";
					$tabl2="Inpatient";
				 }elseif($id2==3){
				     $tabl="appointment";
					 $tabl2="Outpatient";
				 }else{	
				    $tabl="emergency";
					$tabl2="No Attached Consultant";
					
					}
			
				 $sql = "SELECT c.clinic_id FROM $tabl unkn, consultant c, staff s WHERE unkn.consultant_id=c.consultant_id  AND
					s.staff_employee_id=c.staff_employee_id AND
					unkn.patadm_id=".$patadm_id;
				   // echo $sql;
					//exit();
					$res = $this->conn->execute($sql);
					$n=mysql_num_rows($res);
					if($n>0){
					$row = mysql_fetch_array($res);
					$clinic_id=$row['clinic_id'];
							//if($tabl2!='A & E Clinic'){
							  
								
								return $clinic_id;
								
							//}else
								//return $clinicId4AandE;	 
					}
			}
        } catch (Exception $e) {
        }
    }
	/**
     *
     * returns department ID
     * @param string $id hospital no
       * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     * @return integer
     */
	function getdepartment_id2($id,$curLnag_field,$langtb) {
        try {
		 //$getlang= new language();
		 $id2=$this->getPatient_type2($id);
		 $patadm_id=$this->getCurrentmedical_trans_id($id);
		 if($patadm_id>0){
				 if($id2==2){
					$tabl="appointment";
					$tabl2="Inpatient";
				 }elseif($id2==3){
				     $tabl="appointment";
					 $tabl2="Outpatient";
				 }else{	
				    $tabl="emergency";
					$tabl2="A & E Clinic";
           			//$department_id=1; //it must have id of 1 from the database					}
			}
			
				 $sql = "SELECT unkn.dept_id FROM $tabl unkn, consultant c, staff s WHERE unkn.consultant_id=c.consultant_id  AND
					s.staff_employee_id=c.staff_employee_id AND
					unkn.patadm_id=".$patadm_id;
				    //echo $sql;
					//exit();
					$res = $this->conn->execute($sql);
					$n=mysql_num_rows($res);
					if($n>0){
					$row = mysql_fetch_array($res);
					$dept_id=$row['dept_id'];
							//if($tabl2!='A & E Clinic'){
							  
								
								return $dept_id;
								
							//}else
								//return $department_id;	 
					}
			}
        } catch (Exception $e) {
        }
    }
	
	//function getTransactionList($id,$curLnag_field,$langtb) {
//        try {
//		 $getlang= new language();
//		 $id2=$this->getPatient_type($id);
//		 
//		 if($id2==1 || $id2==2)
//		 	$tabl="appointment";
//		 else
//		 	$tabl="patient_transfer";
//		 
//		 
//           $sql = "SELECT unkn.consultant_id,c.staff_employee_id, concat_ws(' ',s.staff_title,s.staff_surname,staff_othernames) AS consultant FROM 
//		   $tabl unkn, consultant c, staff s WHERE unkn.consultant_id=c.consultant_id  AND
//		   	s.staff_employee_id=c.staff_employee_id 	AND
//		    unkn.patadm_id=".$id;
//            $res = $this->conn->execute($sql);
//			if($res){
//            $row = mysql_fetch_array($res);
//			//echo $sql ;
//			$consultant_name=$row['consultant'];
//            return $consultant_name;
//				 
//            }
//			
//        } catch (Exception $e) {
//        }
//    }
/**
 * returns patient type id
 * @param string $id hospital no
 * @return integer
 */
	function getPatient_type($id) {
        try {
		           $id=$this->getCurrentmedical_trans_id($id);
				   $sql = "Select max(pct_id) as current_row from pat_conditiontrack where patadm_id='$id'";
					 $res = $this->conn->execute($sql);
						if($res){
							$row = mysql_fetch_array($res); 
							$curRow=$row['current_row'];
									$sql2 = "Select ctc_id  from pat_conditiontrack where pct_id='$curRow'";
									$res2 = $this->conn->execute($sql2);
									$row2 = mysql_fetch_array($res2); 
									$ctc_id=$row2['ctc_id'];			
            return $ctc_id;
			}	
        } catch (Exception $e) {
        }
    }
    /**
 * returns patient type id
 * @param string $id hospital no
 * @return integer
 */
	function getPatient_type2($id) {
	//die ("--> '$id'");
        try {
		     
	        $id2=$this->getCurrentmedical_trans_id($id); //die (">>>'$id2'");
			if($id!=""){
					 $sql = "Select max(pct_id) 'current_row' from pat_conditiontrack where patadm_id='$id2'";
					 $res = $this->conn->execute($sql);
						if($res){
							$row = mysql_fetch_array($res); 
							$curRow=$row['current_row'];
									$sql2 = "Select ctc_id  from pat_conditiontrack where pct_id='$curRow'";
									$res2 = $this->conn->execute($sql2);
									$row2 = mysql_fetch_array($res2); 
									$ctc_id=$row2['ctc_id'];
														
							return $ctc_id;
					}	
			}
        } catch (Exception $e) {
        }
    }
    /**
    * returns patient type ID
     * @param string $id hospital no
       * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     * @return string
     */
	function getPatient_typename($id,$curLnag_field,$langtb) {
	
        try {
		      $getlang= new language();
	        $id=$this->getCurrentmedical_trans_id($id);
			if($id!=""){
					 $sql = "Select max(pct_id) as current_row from pat_conditiontrack where patadm_id='$id'";
					 $res = $this->conn->execute($sql);
						if($res){
							$row = mysql_fetch_array($res); 
							$curRow=$row['current_row'];
									$sql2 = "Select ctc.langcont_id from pat_conditiontrack ct inner join conditiontypecat ctc on ct.ctc_id=ctc.ctc_id where ct.pct_id='$curRow'";
									//echo $sql;						
									//echo $sql2;
									//exit();
									$res2 = $this->conn->execute($sql2);
									$row2 = mysql_fetch_array($res2); 
									$langcont_id=$row2['langcont_id'];			
									return $getlang->getlang_content($langcont_id,$curLnag_field,$langtb);
					}	
			}
        } catch (Exception $e) {
        }
    }
	
	function getNurse_id($id) {
        try {
		           $sql ="Select * from nurses where staff_employee_id='$id'";
            $res = $this->conn->execute($sql);
				if($res){
            $row = mysql_fetch_array($res);
			//echo $sql ;
			$clinic_id=$row['clinic_id'];
            return $clinic_id;
				}
        } catch (Exception $e) {
        }
    }
    /**
     * list of patient awaiting drugs
     * @param string $hosp_id
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table

     */
	function getPatientAwaitingDrugs_list($hosp_id,$dept_id,$curLnag_field,$langtb) {
        try {
		           $patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
			       $department_id=$this->getdepartment_id2($hosp_id,$curLnag_field,$langtb);	   
				   $sql = "SELECT  * FROM casenote c INNER JOIN 
				   			casenote_prescription  cp  ON 
				  		 c.casenote_id=cp.casenote_id WHERE
					     c.patadm_id ='$patadm_id'";
						 
            $res = $this->conn->execute($sql);
				if($res){
				       echo "<table>";
					   echo "<TR><TD>Items</TD><TD>Amount (NGN)</TD></TR>";
          				   while ($row = mysql_fetch_array($res)) {
								
             				   $list=$this->getCurrentmedical_trans_items($hosp_id,$row['sername_id'],$dept_id,$curLnag_field,$langtb);
               					echo $list['1'];
								$sum=$sum+$list['2'];
						}
						echo '<tr><td>Total</td><td>'.number_format($sum,2).'</td></tr>';
						echo "</table>";
				}
        } catch (Exception $e) {
        }
    }
/**
     * list of patient tests
     * @param string $hosp_id
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table

     */
function getService_list($hosp_id,$dept_id,$curLnag_field,$langtb) {
        try {
		           $patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
			       $departmentt_id=$this->getdepartment_id2($hosp_id,$curLnag_field,$langtb);	   
				   $sql = "SELECT  * FROM casenote c INNER JOIN 
				   			casenote_labtest cl  ON 
				  		 c.casenote_id=cl.casenote_id WHERE
					     c.patadm_id ='$patadm_id'";
						 
            $res = $this->conn->execute($sql);
				if($res){
				       echo "<table>";
					   echo "<TR><TD>Items</TD><TD>Amount (NGN)</TD></TR>";
          				   while ($row = mysql_fetch_array($res)) {
								
             				   $list=$this->getCurrentmedical_trans_items($hosp_id,$row['sername_id'],$dept_id,$curLnag_field,$langtb);
               					echo $list['1'];
								$sum=$sum+$list['2'];
						}
						echo '<tr><td>Total</td><td>'.number_format($sum,2).'</td></tr>';
						echo "</table>";
				}
        } catch (Exception $e) {
        }
    }
    /**
     *
    * list of patient tests for invoice generation
     * @param string $hosp_id
     * @param integer $dept_id
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     */
function getService_list4invoice($hosp_id,$dept_id,$curLnag_field,$langtb) {
        try {
					$sum=0;
					$patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
	$cnt=0;
		           			   
				   $sql = "SELECT  * FROM casenote c INNER JOIN 
				   			casenote_labtest cl  ON 
				  		 c.casenote_id=cl.casenote_id WHERE
					     c.patadm_id ='$patadm_id'";
						 
            $res = $this->conn->execute($sql);
				if($res){
				       echo "<table>";
					   echo "<TR><TD>Tick</TD><TD>Items</TD><TD>Amount (NGN)</TD></TR>";
          				   while ($row = mysql_fetch_array($res)) {
								
             				   $list=$this->getCurrentmedical_trans_items4invoice1($hosp_id,$row['sername_id'],$dept_id,$curLnag_field,$langtb);
               					echo $list['1'];
								$sum=$sum+$list['2'];
								$cnt=$cnt+1;
						}
						echo '<tr><td>Total</td><td>==></td><td>'.number_format($sum,2).'</td><td><input type=submit name="invoice" value="Open Invoice Page" ></input><input type=hidden name="subcount" value="'.$cnt.'" ></input></td></tr>';
						//echo '<tr><td>Total</td><td>'.number_format($sum,2).'</td></tr>';
						echo "</table>";
				}
        } catch (Exception $e) {
        }
    }
 /**
     *
    * list of patient tests for invoice generation
     * @param string $hosp_id
     * @param integer $dept_id
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     */
function getService_list4invoice1($_POST,$servId_array,$show_nhis_label,$hosp_id,$dept_id,$curLnag_field,$langtb) {
        try {
		           $patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
				   
			//	   $sql = "SELECT  * FROM casenote c INNER JOIN 
//				   			casenote_labtest cl  ON 
//				  		 c.casenote_id=cl.casenote_id WHERE
//					     c.patadm_id ='$patadm_id'";
//						 
        //    $res = $this->conn->execute($sql);
				//if($res){
				if (is_array($_POST["select30"])) {
				     //  echo "<table cellspacing=\"5\" cellpadding=\"5\">";
					  // echo "<TR><TD>Tick</TD><TD>Items</TD><TD>Amount (NGN)</TD></TR>";
          				   $sno=1;
						   $sum=0;
						 //  while ($row = mysql_fetch_array($res)) {
							foreach ($_POST["select30"] as $key=>$value) {
								//die("$value");
             				   $list=$this->getCurrentmedical_trans_items4invoice1a($hosp_id,$servId_array,$show_nhis_label,$value,$dept_id,$sno,$curLnag_field,$langtb);
               					if(!empty($list)){
								echo $list[1];
								$sum=$sum+$list[2];
								$sno++;
								}
						}
						echo '<tr><td>&nbsp;</td><td>&nbsp;</td><td>Total</td><td>&nbsp;</td><td align=right>&#8358;'.number_format($sum,2).'</td></tr>';
						echo '<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><input type=hidden name="itemtotal" value="'.$sum.'" ></input></td></tr>';
						
						
						//echo '<tr><td>Total</td><td>'.number_format($sum,2).'</td></tr>';
					//	echo "</table>";
				}
        } catch (Exception $e) {
        }
    }
	/**
     * returns drugs prescribed by the doctor
     * @param integer $patadm_id medical trans ID
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     * @return string
     */
	function getDoctorprescription($patadm_id,$curLnag_field,$langtb) {
       try {
		        $sql = "SELECT  cp.casepres_drugname,casepres_drugdesc FROM casenote c INNER JOIN casenote_prescription cp  ON   c.casenote_id=cp.casenote_id WHERE
 c.patadm_id ='$patadm_id' AND cp.payflag='0'";
// echo $sql;
            $res = $this->conn->execute($sql);
				$sno=1;
				$str="";
				while($row = mysql_fetch_array($res)){
            
			$str.=$sno.'. '.$casepres_drugname=$row['casepres_drugname']."\n";
			$str.="\t".$row['casepres_drugdesc']."\n";
			
          $sno++;
		  
				}
				
				  return $str;
				  
       } catch (Exception $e) {
	     }
    }
    
    
    
    /**
     * Retrieves all appointments attached to a patient admission instance
     * @param int $pat_adm_id   The ID of the patient admission instance
     * @return array            An multi-dimensional array of the appointment IDs & appointment date or an empty array on failure
     */
    function getAppIDsFromPatAdmID($pat_adm_id){
        $pat_adm_id = (int)$pat_adm_id;
        $query = "SELECT app_id, DATE_FORMAT(app_starttime, '%M %d, %Y') 'app_date' FROM appointment
                    WHERE patadm_id = '$pat_adm_id'";
        $result = $this->conn->execute($query);
        $ret_val = array();
        if ($this->conn->hasRows($result)){
            while ($row = mysql_fetch_array($result)){
                $ret_val[] = $row;
            }
        }
        return $ret_val;
    }   //END getAppIDsFromPatAdmID()
    
    
    /**
     *
      * returns tests prescribed by the doctor
     * @param integer $patadm_id medical trans ID
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     * @return string
     */
	function getDoctorlabtest($patadm_id,$curLnag_field,$langtb) {
       try {
		       $service=new service();
			   
			    $sql = "SELECT  * FROM casenote c INNER JOIN casenote_labtest cl  ON c.casenote_id=cl.casenote_id WHERE
 c.patadm_id ='$patadm_id' AND cl.casenote_payflag='0'";
  //die($sql);

        $res = $this->conn->execute($sql);
				echo "<select multiple id=\"select1\"> ";
	 	while($row=mysql_fetch_array($res)){
		$str.="<input type=\"hidden\" name=\"cnote_id[]\" id=\"cnote_id[]\"  value=\"{$row['caselabreq_id']}\" />";
        $service_id=$row['service_id'];
        
		?>
	 
 			 <option value="<?php echo $row['service_id'] ?>"><?php echo $service-> getItemName($service_id,$curLnag_field,$langtb); ?></option>
  			
 
		<?
		}
		echo "</select>";
		echo $str;
				
				 
       } catch (Exception $e) {
	     }
    }
	 
    /**  
     *list admission Items and their corresponding prices
       * @param integer $adm_id admission number
      * returns drugs prescribed by the doctor
     * * @param integer $dept_id
     * @param string hospital number
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table

     */
	 
	 
function getAdmission_Charges($hosp_id,$adm_id,$dept_id,$curLnag_field,$langtb) {
        try {
		           $patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
				   
				      echo "<table>";
					   echo "<TR><TD>Items</TD><TD>Amount (NGN)</TD></TR>";
          						
							//	exit() ;
             				$list = $this->getCurrentmedical_trans_items2($hosp_id,$adm_id,$dept_id,$curLnag_field,$langtb);
							if(!empty($list[1]))echo $list[1];
							if(!empty($list[2]))echo $list[2];
							//echo "<input type='text' name='patinpacc_amount' value='". $list[2]."' />";
						echo "</table>";
				
        } catch (Exception $e) {
        }
    }
 /**
     * list admission Items and their corresponding prices for invoice purpose
       * @param integer $adm_id admission number
  * * @param integer $dept_id
      * @param string hospital numbeer
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table

     */

function getAdmission_Charges4invoice($hosp_id,$adm_id,$dept_id,$curLnag_field,$langtb) {
        try {
		           $patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
				   
				      echo "<table>";
					   echo "<TR><TD align=right>&nbsp;</TD><TD>Items</TD><TD>Amount (NGN)</TD></TR>";
          						
							//	exit() ;
             				$list = $this->getCurrentmedical_trans_items4invoice($hosp_id,$adm_id,$dept_id,$curLnag_field,$langtb);
							if(!empty($list[1]))echo $list[1];
							if(!empty($list[2]))echo $list[2];
							//echo "<input type='text' name='patinpacc_amount' value='". $list[2]."' />";
						echo "</table>";
				
        } catch (Exception $e) {
        }
    }
  /**
     * list admission Items and their corresponding prices for invoice purpose
       * @param integer $adm_id admission number
   * @param integer $patitem_paymentsource
   *  @param integer $nhis_status
       * @param integer $dept_id
      * @param string hospital number
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table

     */
function getAdmission_Charges4invoice2($hosp_id,$adm_id,$dept_id,$patitem_paymentsource,$nhis_status,$curLnag_field,$langtb) {
        try {
		           $patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
				   
				   //   echo "<table>";
					 //  echo "<TR><TD align=right>Tick for Invoice</TD><TD>Items</TD><TD>Amount (NGN)</TD></TR>";
          						
							//	exit() ;
             				$list = $this->getCurrentmedical_trans_items4invoice2($hosp_id,$adm_id,$dept_id,$patitem_paymentsource,$nhis_status,$curLnag_field,$langtb);
							if(!empty($list[1]))echo $list[1];
							if(!empty($list[2]))echo $list[2];
							//echo "<input type='text' name='patinpacc_amount' value='". $list[2]."' />";
						//echo "</table>";
				
        } catch (Exception $e) {
        }
    }
    /**
     * tranverses service table for service items and prices based on supplied parameter
     * @param integer $adm_id admission number
      * @param integer $dept_id
      * @param string hospital number
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     */
function getCurrentmedical_trans_items4invoice($hosp_id,$adm_id,$dept_id,$curLnag_field,$langtb) {
        try {
		           
				   $getlang= new language();
				  $patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
				   $department_id=$this->getdepartment_id2($hosp_id,$curLnag_field,$langtb);
				   $sql_deptid = "SELECT  * FROM service  WHERE dusc_id='$department_id' AND service_type='Dept'";
				    
                   $res = $this->conn->execute($sql_deptid);
				if($res){
				         $row = mysql_fetch_array($res);
					   	 $service_id=$row['service_id'];
 					      $sql_clinicid = "SELECT  * FROM service  WHERE service_parentid='$service_id' ";
                        //echo  $sql_clinicid;
						  //exit();
						  $res2 = $this->conn->execute($sql_clinicid);
						  if($res2){
						  		   while ($row2 = mysql_fetch_array($res2)) {
						     		  $dusc_id=$row2['dusc_id'];
									  $service_id=$row2['service_id'];
						   			  $clinic_id=$this->getClinic_id2($hosp_id,$curLnag_field,$langtb)   ;
						    			//echo $clinic_id;
										//echo "ggggg".$dusc_id;
										
										if($dusc_id==$clinic_id){
												
									
												$deClinic_id=$dusc_id;
											    $deservice_id=$service_id;
									
						                break;
								   		}
									
									}
									//	exit();	
											//get list of constraint for the clinic							
								    		if ($deClinic_id!="" && $deservice_id!=""){
										//	echo "enter";
											$counter=1;
											$cnt=0;
											$enddoloop = true;
											$tt1='yes';
											$tt='no';
											$str=NULL ;
											$sum=0;
											          do { 
														 $sql_constraintlist = "SELECT  * FROM service WHERE service_parentid='$deservice_id' ";
                       									 $res3 = $this->conn->execute($sql_constraintlist);
													//	echo $sql_constraintlist;
													//exit();
																			if ($res3){	
																																								
																																				
																							 while ($row3 = mysql_fetch_array($res3)) {
																							 //echo "enter";
																							 if($row3['service_type']=='Servicename')
																							 {
																					                 if($tt1=='no'){				
																									
																									$serv=$row3['service_type'];
																									//if($row3['dusc_id']==$sername_id)
//																							           {
																									 //  echo $counter.'-'.'product id'. $sername_id;
																									//echo $counter.'-'.'dusc id'. $row3['dusc_id'];
																									//echo "<br />";
																									   
																									 //  $enddoloop = false;
																									  $theService_id= $row3['service_id'];
																									  $theitem_id= $row3['dusc_id'];
																									  
																									  $query2 = "SELECT n.service_id, n.dusc_id, a.servamount_amount, n.service_type 
											FROM service n INNER JOIN service_amount a
												ON n.service_id = a.service_id
												WHERE n.service_id = '$theService_id'";
												
												
												//echo  $query2;
								$result2 = $this->conn->execute($query2);
	
							if ($this->conn->hasRows($result2)){
								$servicename= new service();
								$rowservicename = mysql_fetch_array($result2);
								$tt="yes";
								$cnt++;
								$str.= '<tr>';
								$str.= '<td><input type="checkbox" class="checkbox" name="ID['.$cnt.']" value="'.$rowservicename['service_id'].'"  disabled="disabled" checked></input><input type="hidden" name="chkID'.$cnt.'" value="'.$rowservicename['service_id'].'"</input></td>';
								$str.= "<td>".$servicename->getTablename('Servicename',$rowservicename['dusc_id'],$curLnag_field,$langtb)."</td>";
								$str.= "<td>".number_format($rowservicename['servamount_amount'],2)."</td>";
								
								$str.= '</tr>';
							}						
										$sum=$sum+$rowservicename['servamount_amount'];					//										  break;
																									   }
																									   
																							 }				
																								if($row3['service_type']=='Constraint')
																								 { 			   
																									//echo $counter;
																									
																											$hasConstraint = $this->getConstraint2($hosp_id,$row3['dusc_id'],$curLnag_field,$langtb);
																											
																											
																											if ($hasConstraint==$adm_id){
																								      		      $deservice_id=$row3['service_id'];
														//														  echo "deservice_id".$deservice_id;
															//													  echo  "service type".	$row3['service_type'];
																									                $tt1='no';																 						
																													break;
																											} 
																									}
																									//		$enddoloop = true;
																												
																												
																							}
												//exit();
																																								
																					}else $enddoloop = false;
																					$counter++;
																					if($tt=='yes' && $serv='Servicename'){ $enddoloop = false; break; }
																				} while ($enddoloop && $counter < 5);
															           //	exit();
																		
													//	}
								$list[1]=$str;
								$list[2]='<tr><td>Total</td><td>'.number_format($sum,2).'<input type=hidden name=patinpacc_amount value='.$sum.' ></input></td><td><input type=submit name="invoice" value="Open Invoice Page" ></input><input type=hidden name="subcount" value="'.$cnt.'" ></input></td></tr>';
											}
												
						}
						
				}
				if(!empty($list))
				return $list;
        } catch (Exception $e) {
        }
    }
	/**
     * tranverses service table for service items and prices based on supplied parameter
     * @param integer $adm_id admission number
     * @param integer $patitem_paymentsource
     *  @param integer $nhis_status
     * @param integer $dept_id
     * @param string hospital number
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     */
function getCurrentmedical_trans_items4invoice2($hosp_id,$adm_id,$dept_id,$patitem_paymentsource,$nhis_status,$curLnag_field,$langtb) {
        try {
		          $getlang= new language();
				  $patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
				   $department_id=$this->getdepartment_id2($hosp_id,$curLnag_field,$langtb);
				   $sql_deptid = "SELECT  * FROM service  WHERE dusc_id='$department_id' AND service_type='Dept'";
				    
                   $res = $this->conn->execute($sql_deptid);
				if($res){
				         $row = mysql_fetch_array($res);
					   	 $service_id=$row['service_id'];
 					      $sql_clinicid = "SELECT  * FROM service  WHERE service_parentid='$service_id' ";
                        //echo  $sql_clinicid;
						  //exit();
						  $res2 = $this->conn->execute($sql_clinicid);
						  if($res2){
						  		   while ($row2 = mysql_fetch_array($res2)) {
						     		  $dusc_id=$row2['dusc_id'];
									  $service_id=$row2['service_id'];
						   			  $clinic_id=$this->getClinic_id2($hosp_id,$curLnag_field,$langtb)   ;
						    			//echo $clinic_id;
										//echo "ggggg".$dusc_id;
										
										if($dusc_id==$clinic_id){
												
									
												$deClinic_id=$dusc_id;
											    $deservice_id=$service_id;
									
						                break;
								   		}
									
									}
									//	exit();	
											//get list of constraint for the clinic							
								    		if ($deClinic_id!="" && $deservice_id!=""){
										//	echo "enter";
											$counter=1;
											$cnt=0;
											$enddoloop = true;
											$tt1='yes';
											$tt='no';
											
											$str=NULL ;
											$sum=0;
											          do { 
														 $sql_constraintlist = "SELECT  * FROM service WHERE service_parentid='$deservice_id' ";
                       									 $res3 = $this->conn->execute($sql_constraintlist);
													//	echo $sql_constraintlist;
													//exit();
																			if ($res3){	
																																								
																																				
																							 while ($row3 = mysql_fetch_array($res3)) {
																							 //echo "enter";
																							 if($row3['service_type']=='Servicename')
																							 {
																					                 if($tt1=='no'){				
																									
																									$serv=$row3['service_type'];
																									//if($row3['dusc_id']==$sername_id)
//																							           {
																									 //  echo $counter.'-'.'product id'. $sername_id;
																									//echo $counter.'-'.'dusc id'. $row3['dusc_id'];
																									//echo "<br />";
																									   
																									 //  $enddoloop = false;
																									  $theService_id= $row3['service_id'];
																									  $theitem_id= $row3['dusc_id'];
																									  
																									  $query2 = "SELECT n.service_id, n.dusc_id, a.servamount_amount, n.service_type 
											FROM service n INNER JOIN service_amount a
												ON n.service_id = a.service_id
												WHERE n.service_id = '$theService_id'";
												
												
												//echo  $query2;
								$result2 = $this->conn->execute($query2);
	
							if ($this->conn->hasRows($result2)){
								$servicename= new service();
								$rowservicename = mysql_fetch_array($result2);
								$tt="yes";
								$cnt++;
								$itemname=$servicename->getTablename('Servicename',$rowservicename['dusc_id'],$curLnag_field,$langtb);
								$str.= '<tr>';
								
								$str.= '<td >'.$cnt.'<input type="hidden" name="chkID'.$cnt.'" value="'.$rowservicename['service_id'].'"></input>
													<input type="hidden" name="itemname'.$cnt.'" value="'.$itemname.'"></input>
						
													<input type="hidden" name="itemamount'.$cnt.'" value="'.$rowservicename['servamount_amount'].'"></input>
								                    <input type="hidden" name="itemid'.$cnt.'" value="'.$rowservicename['service_id'].'"></input></td>';
													
													
								$str.= "<td >".$servicename->getTablename('Servicename',$rowservicename['dusc_id'],$curLnag_field,$langtb)."</td>";
								
								$str.= "<td >".number_format($rowservicename['servamount_amount'],2)."</td>";
								$str.= "<td >".number_format($rowservicename['servamount_amount'],2)."</td>";
								if($patitem_paymentsource=='1'){
								$str.= "<td >".number_format($rowservicename['servamount_amount']*.1,2)."</td>";
								$str.= "<td >".$nhis_status."</td>";
								}
								//$str.= "<td >".$this->discounttype($cnt)."</td>";
								//$str.= '<td ><input type="text" name="discountamt'.$cnt.'" value=""/></td>';
								$str.= '<input type="hidden" name="patitem_paymentsource'.$cnt.'" value="'.$patitem_paymentsource.'"></input></tr>';
							}						
															//										  break;
										$sum=$sum+$rowservicename['servamount_amount'];	
																									   }
																									   
																							 }				
																								if($row3['service_type']=='Constraint')
																								 { 			   
																									//echo $counter;
																									
																											$hasConstraint = $this->getConstraint2($hosp_id,$row3['dusc_id'],$curLnag_field,$langtb);
																											
																											
																											if ($hasConstraint==$adm_id){
																								      		      $deservice_id=$row3['service_id'];
														//														  echo "deservice_id".$deservice_id;
															//													  echo  "service type".	$row3['service_type'];
																									                $tt1='no';																 						
																													break;
																											} 
																									}
																									//		$enddoloop = true;
																												
																												
																							}
												//exit();
																																								
																					}else $enddoloop = false;
																					$counter++;
																					if($tt=='yes' && $serv='Servicename'  ){ $enddoloop = false; break; }
																				} while ($enddoloop && $counter < 5);
															           //	exit();
																		
													//	}
								$list[1]=$str;
								$list[2]='<tr><td colspan=3>Total</td><td>'.number_format($sum,2).'<input type=hidden name=patinpacc_amount value='.$sum.' ></input><input type=hidden name="subcount" value="'.$cnt.'" ></input></td></tr>';
											}
								
								
								
						}
						
				}
				if(!empty($list))
				return $list;
        } catch (Exception $e) {
        }
    }
     /**
     * tranverses service table for service items and prices based on supplied parameter
     * @param integer $adm_id admission number
      * @param integer $dept_id
      * @param string hospital number
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     */
	function getCurrentmedical_trans_items2($hosp_id,$adm_id,$dept_id,$curLnag_field,$langtb) {
        try {
		           
				   $getlang= new language();
				  $patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
				   $department_id=$this->getdepartment_id2($hosp_id,$curLnag_field,$langtb);
				   $sql_deptid = "SELECT  * FROM service  WHERE dusc_id='$department_id' AND service_type='Dept'";
				    
                   $res = $this->conn->execute($sql_deptid);
				if($res){
				         $row = mysql_fetch_array($res);
					   	 $service_id=$row['service_id'];
 					      $sql_clinicid = "SELECT  * FROM service  WHERE service_parentid='$service_id' ";
                        //echo  $sql_clinicid;
						  //exit();
						  $res2 = $this->conn->execute($sql_clinicid);
						  if($res2){
						  		   while ($row2 = mysql_fetch_array($res2)) {
						     		  $dusc_id=$row2['dusc_id'];
									  $service_id=$row2['service_id'];
						   			  $clinic_id=$this->getClinic_id2($hosp_id,$curLnag_field,$langtb)   ;
						    			//echo $clinic_id;
										//echo "ggggg".$dusc_id;
										
										if($dusc_id==$clinic_id){
												
									
												$deClinic_id=$dusc_id;
											    $deservice_id=$service_id;
									
						                break;
								   		}
									
									}
									//	exit();	
											//get list of constraint for the clinic							
								    		if ($deClinic_id!="" && $deservice_id!=""){
										//	echo "enter";
											$counter=1;
											$enddoloop = true;
											$tt1='yes';
											$tt1='no';
											$str=NULL ;
											$sum=0;
											          do { 
														 $sql_constraintlist = "SELECT  * FROM service WHERE service_parentid='$deservice_id' ";
                       									 $res3 = $this->conn->execute($sql_constraintlist);
													//	echo $sql_constraintlist;
													//exit();
																			if ($res3){	
																																								
																																				
																							 while ($row3 = mysql_fetch_array($res3)) {
																							 //echo "enter";
																							 if($row3['service_type']=='Servicename')
																							 {
																					                 if($tt1=='no'){				
																									
																									$serv=$row3['service_type'];
																									//if($row3['dusc_id']==$sername_id)
//																							           {
																									 //  echo $counter.'-'.'product id'. $sername_id;
																									//echo $counter.'-'.'dusc id'. $row3['dusc_id'];
																									//echo "<br />";
																									   
																									 //  $enddoloop = false;
																									  $theService_id= $row3['service_id'];
																									  $theitem_id= $row3['dusc_id'];
																									  
																									  $query2 = "SELECT n.service_id, n.dusc_id, a.servamount_amount, n.service_type 
											FROM service n INNER JOIN service_amount a
												ON n.service_id = a.service_id
												WHERE n.service_id = '$theService_id'";
												
												
												//echo  $query2;
								$result2 = $this->conn->execute($query2);
	
							if ($this->conn->hasRows($result2)){
								$servicename= new service();
								$rowservicename = mysql_fetch_array($result2);
								$tt="yes";
								
								$str.= '<tr>';
								$str.= "<td>".$servicename->getTablename('Servicename',$rowservicename['dusc_id'],$curLnag_field,$langtb)."</td>";
								$str.= "<td>".number_format($rowservicename['servamount_amount'],2)."</td>";
								
								$str.= '</tr>';
							}						
										$sum=$sum+$rowservicename['servamount_amount'];					//										  break;
																									   }
																									   
																							 }				
																								if($row3['service_type']=='Constraint')
																								 { 			   
																									//echo $counter;
																									
																											$hasConstraint = $this->getConstraint2($hosp_id,$row3['dusc_id'],$curLnag_field,$langtb);
																											
																											
																											if ($hasConstraint==$adm_id){
																								      		      $deservice_id=$row3['service_id'];
														//														  echo "deservice_id".$deservice_id;
															//													  echo  "service type".	$row3['service_type'];
																									                $tt1='no';																 						
																													break;
																											} 
																									}
																									//		$enddoloop = true;
																												
																												
																							}
												//exit();
																																								
																					}else $enddoloop = false;
																					$counter++;
																					if($tt=='yes' && $serv='Servicename'  ){ $enddoloop = false; break; }
																				} while ($enddoloop && $counter < 5);
															           //	exit();
																		
													//	}
								$list[1]=$str;
								$list[2]='<tr><td>Total</td><td>'.number_format($sum,2).'<input type=hidden name=patinpacc_amount value='.$sum.' ></input></td></tr>';
											}
								
								
								
						}
						
				}
				if(!empty($list))
				return $list;
        } catch (Exception $e) {
        }
    }

	/**
     * used by other functions that tranverses service table
     * @param string $hosp_id
     * @param integer $disItem
    * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     * @return integer
     */
	
	
	function getConstraint2($hosp_id,$disItem,$curLnag_field,$langtb){
	$retVal = array();
	$getlang= new language();
	$status=false;
	$query = "SELECT * FROM service_constraint
				WHERE servcon_id = '$disItem'";
	$result = $this->conn->execute($query);
	if ($this->conn->hasRows($result, 1)){
		$row = mysql_fetch_array ($result, MYSQL_ASSOC);
		$langcont_id=$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
		$sercon_reftable=$row['sercon_reftable'];
		$servcon_refcolumn=$row['servcon_refcolumn'];
				
				
				if($servcon_refcolumn=='admission'){
				   		$status=1;									
								
					}elseif($servcon_refcolumn=='shortstay'){
					$status=2;
					
					
					}elseif($servcon_refcolumn=='icuward'){
					
					$status=4;
					
					}	elseif($servcon_refcolumn=='icutheatre'){
					$status=5;
					
					
					}	elseif($servcon_refcolumn=='tetanus'){
					$status=7;
					
					
					}elseif($servcon_refcolumn=='burns'){
					
					$status=6;
					
					}else{
					if($servcon_refcolumn=='sideward'){
					$status=3;
					
					}
					}									
									
				
				
		
		
		
	}
	return $status;
}

 /**
     * tranverses service table for service items and prices based on supplied parameter
     * @param integer $sername_id service id
      * @param integer $dept_id
      * @param string hospital number
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
  * @return array
     */
function getCurrentmedical_trans_items($hosp_id,$sername_id,$dept_id,$curLnag_field,$langtb) {
        try {
		           
				   $getlang= new language();
				  $patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
				   $department_id=$this->$this->getdepartment_id2($hosp_id,$curLnag_field,$langtb)   ;
				   $sql_deptid = "SELECT  * FROM service  WHERE dusc_id='$department_id' AND service_type='Dept'";
				    
                   $res = $this->conn->execute($sql_deptid);
				if($res){
				         $row = mysql_fetch_array($res);
					   	 $service_id=$row['service_id'];
 					      $sql_clinicid = "SELECT  * FROM service  WHERE service_parentid='$service_id' ";
                        //  echo  $sql_clinicid;
//						  exit();
						  $res2 = $this->conn->execute($sql_clinicid);
						  if($res2){
						  		   while ($row2 = mysql_fetch_array($res2)) {
						     		  $dusc_id=$row2['dusc_id'];
									  $service_id=$row2['service_id'];
						   			  $clinic_id=$this->getClinic_id2($hosp_id,$curLnag_field,$langtb)   ;
						    			//echo 'dusc'.$dusc_id;
//										echo 'clinic'.$clinic_id;
										if($dusc_id==$clinic_id){
												$deClinic_id=$dusc_id;
												$deservice_id=$service_id;
										
						                break;
								   		}
									
									}
								//	echo 'service_id'.$deservice_id;
//										exit();	//get list of constraint for the clinic							
								    		if ($deClinic_id!="" && $deservice_id!=""){
										//	echo "enter";
											$counter=1;
											$enddoloop = true;
											
											          do { 
														 $sql_constraintlist = "SELECT  * FROM service WHERE service_parentid='$deservice_id' ";
                       									 $res3 = $this->conn->execute($sql_constraintlist);
														//echo $sql_constraintlist;
													///exit();
																			if ($this->conn->hasRows($res3)){
																																							
																																				
																							 while ($row3 = mysql_fetch_array($res3)) {
																							
																							 if($row3['service_type']=='Servicename')
																							 {
																							 	
																									
																									
																									if($row3['dusc_id']==$sername_id)
																							           {
																									 //  echo $counter.'-'.'product id'. $sername_id;
																									//echo $counter.'-'.'dusc id'. $row3['dusc_id'];
//																									exit();
																									//echo "<br />";
																									   $enddoloop = false;
																									  $theService_id= $row3['service_id'];
																									  $theitem_id= $row3['dusc_id'];
																									  
																									  $query2 = "SELECT n.service_id, n.dusc_id, a.servamount_amount, n.service_type 
											FROM service n INNER JOIN service_amount a
												ON n.service_id = a.service_id
												WHERE n.service_id = '$theService_id'";
												
												
												//echo  $query2;
								$result2 = $this->conn->execute($query2);
	
							if ($this->conn->hasRows($result2)){
								$servicename= new service();
								$rowservicename = mysql_fetch_array($result2);
								$str.= '<tr>';
								$str.= "<td>".$servicename->getTablename('Servicename',$rowservicename['dusc_id'],$curLnag_field,$langtb)."</td>";
								$str.= "<td>".number_format($rowservicename['servamount_amount'],2)."</td>";
								
								$str.= '</tr>';
							}						
										$sum=$sum+$rowservicename['servamount_amount'];		
																									   break;
																									   }
																									   
																							 }				
																							if($row3['service_type']=='Constraint')
																							 { 			   
																											$hasConstraint = $this->getConstraint($hosp_id,$row3['dusc_id'],$curLnag_field,$langtb);
																											if ($hasConstraint){
																								      		      $deservice_id=$row3['service_id'];
																												 // echo "deservice_id".$deservice_id;
//																												  echo  "service type".	$row3['service_type'];
//																												  
//																													echo 'counter'.$counter ;
																									//exit();	
																													break;
																											} 
																							}
																											$enddoloop = true;
																												
																												
																							}
												//exit();
																		
																							
																					}else $hasParent = false;
																					$counter++;
																				} while ($enddoloop && $counter < 5);
															           //	exit();
																		
												
												$list[1]=$str;
								$list[2]=$sum;
													//	}
											}
								
								
								
						}
				}
				if(!empty($list))
				return $list;
        } catch (Exception $e) {
        }
    }
/**
     * tranverses service table for service items and prices based on supplied parameter
     * @param integer $sername_id service id
      * @param integer $dept_id
      * @param string hospital number
     * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
  * @return array
     */
function getCurrentmedical_trans_items4invoice1($hosp_id,$sername_id,$dept_id,$curLnag_field,$langtb) {
        try {
		           
				   $getlang= new language();
				  //$patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
				   $department_id=$this->getdepartment_id2($hosp_id,$curLnag_field,$langtb);
				   $sql_deptid = "SELECT  * FROM service  WHERE dusc_id='$department_id' AND service_type='Dept'";
				    
                   $res = $this->conn->execute($sql_deptid);
				if($res){
				         $row = mysql_fetch_array($res);
					   	 $service_id=$row['service_id'];
 					      $sql_clinicid = "SELECT  * FROM service  WHERE service_parentid='$service_id' ";
                        //  echo  $sql_clinicid;
//						  exit();
						  $res2 = $this->conn->execute($sql_clinicid);
						  if($res2){
						  		   while ($row2 = mysql_fetch_array($res2)) {
						     		  $dusc_id=$row2['dusc_id'];
									  $service_id=$row2['service_id'];
						   			  $clinic_id=$this->getClinic_id2($hosp_id,$curLnag_field,$langtb)   ;
						    			//echo 'dusc'.$dusc_id;
//										echo 'clinic'.$clinic_id;
										if($dusc_id==$clinic_id){
												$deClinic_id=$dusc_id;
												$deservice_id=$service_id;
										
						                break;
								   		}
									
									}
								//	echo 'service_id'.$deservice_id;
								
//										exit();	//get list of constraint for the clinic							
								    		if ($deClinic_id!="" && $deservice_id!=""){
										//	echo "enter";
											$counter=1;
											$enddoloop = true;
											$str="";
											$sum=0;
											$cnt=0;
											          do { 
														 $sql_constraintlist = "SELECT  * FROM service WHERE service_parentid='$deservice_id' ";
                       									 $res3 = $this->conn->execute($sql_constraintlist);
														//echo $sql_constraintlist;
													///exit();
																			if ($this->conn->hasRows($res3)){
																																							
																																				
																							 while ($row3 = mysql_fetch_array($res3)) {
																							
																							 if($row3['service_type']=='Servicename')
																							 {
																							 	
																									
																									
																									if($row3['dusc_id']==$sername_id)
																							           {
																									 //  echo $counter.'-'.'product id'. $sername_id;
																									//echo $counter.'-'.'dusc id'. $row3['dusc_id'];
//																									exit();
																									//echo "<br />";
																									   $enddoloop = false;
																									  $theService_id= $row3['service_id'];
																									  $theitem_id= $row3['dusc_id'];
																									  
																									  $query2 = "SELECT n.service_id, n.dusc_id, a.servamount_amount, n.service_type 
											FROM service n INNER JOIN service_amount a
												ON n.service_id = a.service_id
												WHERE n.service_id = '$theService_id'";
												
												
												//echo  $query2;
								$result2 = $this->conn->execute($query2);
	
							if ($this->conn->hasRows($result2)){
								$servicename= new service();
								$rowservicename = mysql_fetch_array($result2);
								$str.= '<tr>';
								$str.= '<td><input type="checkbox" class="checkbox" name="ID['.$cnt.']" value="'.$rowservicename['service_id'].'" disabled="disabled"  checked></input></td><td>'.$servicename->getTablename('Servicename',$rowservicename['dusc_id'],$curLnag_field,$langtb).'</td>';
								$str.= '<td>'.number_format($rowservicename['servamount_amount'],2).'</td>';
								
								$str.= '</tr>';
							}						
										$sum=$sum+$rowservicename['servamount_amount'];		
																									   break;
																									   }
																									   
																							 }				
																							if($row3['service_type']=='Constraint')
																							 { 			   
																											$hasConstraint = $this->getConstraint($hosp_id,$row3['dusc_id'],$curLnag_field,$langtb);
																											if ($hasConstraint){
																								      		      $deservice_id=$row3['service_id'];
																												 // echo "deservice_id".$deservice_id;
//																												  echo  "service type".	$row3['service_type'];
//																												  
//																													echo 'counter'.$counter ;
																									//exit();	
																													break;
																											} 
																							}
																											$enddoloop = true;
																												
																												
																							}
												//exit();
																		
																							
																					}else $hasParent = false;
																					$counter++;
																				} while ($enddoloop && $counter < 5);
															           //	exit();
																		
												
												$list[1]=$str;
								$list[2]=$sum;
													//	}
											}
								
								
								
						}
				}
				return $list;
        } catch (Exception $e) {
        }
    }
/**
     * tranverses service table for service items and prices based on supplied parameter
     * @param integer $sername_id service id
     * param integer $sno
      * @param integer $dept_id
      * @param string hospital number
     * @param string $curLnag_fie ld column in the language table that contains current language text
        * @param string $langtb  language table
      * @return array
     */
	function getCurrentmedical_trans_items4invoice1a($hosp_id,$servId_array,$show_nhis_label,$sername_id,$dept_id,$sno,$curLnag_field,$langtb) {
        try {
		           $getlang= new language();
				   $deClinic_id=0;
				   $deservice_id=0;
				  //$patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
				   //$department_id=$this->getdepartment_id2($hosp_id,$curLnag_field,$langtb);
				   $sql_deptid = "SELECT  * FROM service  WHERE dusc_id='$dept_id' AND service_type='Dept'";
				    //echo $sql_deptid . "<-->" . $_SESSION[session_id() . "deptID"];
					//exit();
					$list=array();
                   $res = $this->conn->execute($sql_deptid);
				if($res){
				         $row = mysql_fetch_array($res);
					   	 $service_id=$row['service_id'];
 					      $sql_clinicid = "SELECT  * FROM service  WHERE service_parentid='$service_id' ";
                      // die("$sql_clinicid") ;
//						
						  $res2 = $this->conn->execute($sql_clinicid);
						  if($res2){
						  		   while ($row2 = mysql_fetch_array($res2)) {
						     		  $dusc_id=$row2['dusc_id'];
									  $service_id=$row2['service_id'];
                                      $deservice_id=$service_id;
						   			 // $clinic_id=$this->getClinic_id2($hosp_id,$curLnag_field,$langtb)   ;
						    			//echo 'dusc'.$dusc_id;
										//echo 'clinic'.$clinic_id;
//										if($dusc_id==$clinic_id){
//												$deClinic_id=$dusc_id;
//												$deservice_id=$service_id;
//
//						                break;
//								   		}
									
									//}
								//	echo 'service_id'.$deservice_id;
//										exit();	//get list of constraint for the clinic							
								          
								    		//if ($deClinic_id!="" && $deservice_id!=""){
										if ($deservice_id!=""){
                                        //	echo "enter";
											$counter=1;
											//$cnt1=1;
											$enddoloop = true;
											
											          do { 
														 $sql_constraintlist = "SELECT  * FROM service WHERE service_parentid='$deservice_id' ";
                       									 $res3 = $this->conn->execute($sql_constraintlist);
														//echo $sql_constraintlist;
													//exit();
																			if ($this->conn->hasRows($res3)){
																																							
																							
																							 while ($row3 = mysql_fetch_array($res3)) {
																							
																							 if($row3['service_type']=='Servicelab')
																							 {
																							 //	echo 'product id'. $sername_id;
																									//echo 'dusc id'. $row3['dusc_id'];
                                                                                               // die("entered");
																									
																									
																									if($row3['service_id']==$sername_id)
																							           {
																									  //echo 'product id'. $sername_id;
																									//echo 'dusc id'. $row3['dusc_id'];
																									//exit();
																									//echo "<br />";
																									   $enddoloop = false;
																									  $theService_id= $row3['service_id'];
																									  $theitem_id= $row3['service_id'];
																									  
																									  $query2 = "SELECT n.service_id, n.dusc_id, a.servamount_amount, n.service_type 
											FROM service n INNER JOIN service_amount a
												ON n.service_id = a.service_id
												WHERE n.service_id = '$theService_id'";
												
												
												//echo  $query2;
								$result2 = $this->conn->execute($query2);
	
							if ($this->conn->hasRows($result2)){
								$servicename= new service();
								$rowservicename = mysql_fetch_array($result2);
								//$cnt1=$cnt1+1;
								$str="";
								$sum=0;
								$itemname=$servicename->getTablename($rowservicename['service_type'],$rowservicename['dusc_id'],$curLnag_field,$langtb);
								$str.= '<tr>';
								$str.= '<td align=center>'.$sno.'</td><td><input type="hidden"  name="check[]" value="'.$sno.'"   checked></input></td>
								<td>'.$servicename->getTablename($rowservicename['service_type'],$rowservicename['dusc_id'],$curLnag_field,$langtb).'<input type="hidden" name="itemname'.$sno.'" value="'.$itemname.'"></input>
													<input type="hidden" name="itemamount'.$sno.'" value="'.$rowservicename['servamount_amount'].'"></input>
								                    <input type="hidden" name="itemid'.$sno.'" value="'.$rowservicename['service_id'].'"></input></td>';
								$str.= '<td  class="value">'.number_format($rowservicename['servamount_amount'],2).'</td>';
                                $str.= '<td class="value">'.number_format($rowservicename['servamount_amount'],2).'</td>';
                                $itemID = $rowservicename['dusc_id'];
                               //print_r($servId_array);
                               // die("item-$itemID");
							   $servId_array = !empty($servId_array) ? $servId_array : array();
                                if(in_array("$itemID",$servId_array)){
                                      $nhis_status="NHIS Covered";
                                      $patitem_paymentsource='1';
                                  }
                                  else{
                                      $patitem_paymentsource='3';
                                      $nhis_status="";
                                  }

								$str.= '<td class="value">'. $nhis_status.'<input type="hidden" name="patitem_paymentsource'.$sno.'" value="'.$patitem_paymentsource.'"></input></td>';

								$str.= '</tr>';
							}						
										$sum=$sum+$rowservicename['servamount_amount'];		
																									   break;
																									   }
																									   
																							 }				
																							if($row3['service_type']=='Constraint')
																							 { 			   
																											$hasConstraint = $this->getConstraint($hosp_id,$row3['dusc_id'],$curLnag_field,$langtb);
																											if ($hasConstraint){
																								      		      $deservice_id=$row3['service_id'];
																												 // echo "deservice_id".$deservice_id;
//																												  echo  "service type".	$row3['service_type'];
//																												  
//																													echo 'counter'.$counter ;
																									//exit();	
																													break;
																											} 
																							}
																											$enddoloop = true;
																												
																												
																							}
												//exit();
																		
																							
																					}else $hasParent = false;
																					$counter++;
																				} while ($enddoloop && $counter < 5);
															           //	exit();
																		
												if(!empty($str)){
												$list[1]=$str;
								                $list[2]=$sum;
														}
											}
								
								
                                   }
						}
				}
				return $list;
        } catch (Exception $e) {
        }
    }
/**
     * used by other functions that tranverses service table
     * @param string $hosp_id
     * @param integer $disItem
    * @param string $curLnag_fie ld column in the language table that contains current language text
     * @param string $langtb  language table
     * @return boolean
     */
	function getConstraint($hosp_id,$disItem,$curLnag_field,$langtb){
	$retVal = array();
	$getlang= new language();
	$status=false;
	$query = "SELECT * FROM service_constraint
				WHERE servcon_id = '$disItem'";
	$result = $this->conn->execute($query);
	if ($this->conn->hasRows($result, 1)){
		$row = mysql_fetch_array ($result, MYSQL_ASSOC);
		$langcont_id=$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
		$sercon_reftable=$row['sercon_reftable'];
		$servcon_refcolumn=$row['servcon_refcolumn'];
				if($servcon_refcolumn=='reg_gender')
				{
							   $query1 = "SELECT $servcon_refcolumn FROM $sercon_reftable
							WHERE  reg_hospital_no= '$hosp_id'";
							$result1 = $this->conn->execute($query1);
							   if($result1){  
							             $row1 = mysql_fetch_array ($result1, MYSQL_ASSOC);
							            $deConstraint=$row1['reg_gender']; 
										 if($deConstraint==0) $deConstraintcode='male';
										 if($deConstraint==1) $deConstraintcode='female';
													if($deConstraintcode==strtolower($langcont_id))					 
															$status = true;
													else    $status=false;
											}
				
			}elseif($servcon_refcolumn=='ctc_id'){
				                 $patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
								$query1 = "SELECT $sercon_reftable.$servcon_refcolumn FROM $sercon_reftable , pat_conditiontrack 
							WHERE  $sercon_reftable.ctc_id=pat_conditiontrack.ctc_id AND pat_conditiontrack.patadm_id= '$patadm_id'";
							//echo $query1;
							//exit();
							$result1 = $this->conn->execute($query1);
							   if($result1){  
							       $row1 = mysql_fetch_array ($result1, MYSQL_ASSOC);
							           $deConstraint=$row1['ctc_id']; 
										 if($deConstraint==1) $deConstraintcode='inpatient';
										 if($deConstraint==2) $deConstraintcode='outpatient';
										 if($deConstraint==3) $deConstraintcode='emergency';
													
													//
													if($deConstraintcode==strtolower($langcont_id))					 
															$status = true;
													else    $status=false;
													//echo 'constraint-'.$deConstraintcode;
													//echo '<br>langid-'.$langcont_id;			
										 }
				
				
				
				}else{
				
				if($servcon_refcolumn=='reg_dob'){
				   				 $patadm_id = $this->getCurrentmedical_trans_id($hosp_id);
								$query1 = "SELECT year(curdate())- year(reg_dob) as dob1 FROM $sercon_reftable
							WHERE  reg_hospital_no= '$hosp_id'";
							$result1 = $this->conn->execute($query1);
							   if($result1){  
							          $row1 = mysql_fetch_array ($result1, MYSQL_ASSOC);
							           $deConstraint=$row1['dob1']; 
										 if($deConstraint<12) $deConstraintcode='paediatrics';
										 if($deConstraint>12) $deConstraintcode='adult';
										 		if($deConstraintcode==strtolower($langcont_id))					 
															$status = true;
													else    $status=false;
																
										 }
								
								
					}					
				
				}
		
		
		
	}
	return $status;
}

/**
 *returns  medical trans ID for the supplied hospital number
 * @param string $id hospital number
 * @return integers
 */

	
	function getCurrentmedical_trans_id($id) {
        try {
		           $sql = "SELECT  max(pa.patadm_id)AS patadm_id FROM patient_admission pa,pat_conditiontrack  pc WHERE
					     pa.patadm_id=pc.patadm_id AND 
				         reg_hospital_no='$id'"; //die ("<pre>$sql</pre>");
			//die ($this->conn->dbname);			 
            $res = $this->conn->execute($sql);
				if($res){
            $row = mysql_fetch_array($res);
			//echo $sql ;
			//exit();
			$patadm_id=$row['patadm_id']; //die ("***'$sql' '$patadm_id'");
			return $patadm_id;
				}
        } catch (Exception $e) {
        }
    }
//		function discounttype($id) {
//        try {
//		  $str= "<select id='discount".$id."' name='discount".$id."' onChange='showtext(this.value)' >";
//		  $str.= "<option value='0'>Flat</option>";
//		  $str.="<option value='1'>Percentage</option>"  ;
//		  $str.="</select>";
//		  return $str;
//
//        } catch (Exception $e) {
//        }
//    }
/**
    * list patients admission
    * @param string $curLnag_fie ld column in the language table that contains current language text
    * @param string $langtb  language table
 * @return string
 */
function getpendingAdmissionlist_id($curLnag_field,$langtb) {
        try {
		   $wards= new wards();
            $sql = "SELECT * FROM pat_admrecommend WHERE patadmrec_flag='0' ";
            $res = $this->conn->execute($sql);
           $mesg2="";			
			if ($this->conn->hasRows($res,  0)){
					while ($row = mysql_fetch_array($res)) {
					$patadm_id=$row["patadm_id"];
						$mesg1= '<tr><td ><a href="index.php?p=patinp&patadm_id='.$patadm_id.'" >'.$this->getPatient_name($patadm_id).'<br> (Ward - '.$wards->getWard_name($row["ward_id"],$curLnag_field,$langtb).' - Recommended)</a></td></tr>';
					   $mesg2.=$mesg1;
					   }
					$mesg[1]=true;
					$mesg[0]=$mesg2;
					return $mesg;   
			   }else{
			     $mesg[1]=false;
			     $mesg[0]='<tr><td >No Patient Pnding Admission</td></tr>';
				 return $mesg;
			   }
        } catch (Exception $e) {
      }
    }


/**
 *
 * @param integer $patadm_id medical trans ID
 * @param integer $inp row ID of active patient on admission in inpatient_admission table
 */
function getActionLinks($patadm_id,$inp) {
        try {
           
                echo '<tr><td width="168"><a href="./index.php?p=inp" >Go to Admission</a></td></tr>';
                echo '<tr><td width="168"><a href="./index.php?p=inpDischarge&patadm_id='.$patadm_id.'&inp_id='.$inp.'" >Discharge this Patient</a></td></tr>';
			 	
			    
			
        } catch (Exception $e) {
      }
    }




public function GetSearchFields(){
	echo 	"<option value='-1'>Select Field</option>";	
		$result =$this->conn->execute("SELECT * from patient_admission order by patadm_id");
				$i = 0;
				
while ($i < mysql_num_fields($result)) {
 //   echo "Information for column $i:<br />\n";
    $meta = mysql_fetch_field($result);
	
    if (!$meta) {
      //  echo "No information available<br />\n";
    }
  //  echo "$meta->name";

		
	echo 	"<option value=". $meta->name.">".$meta->name."</option>";
	    $i++;
}
/*mysql_free_result($result);
	return $keys;*/
	}
    /**
     * Close mysql connection
     */
	public function endpatient_admission(){
		$this->connection->CloseMysql();
	}

}


// ------------------------------------------------------------------------

