<?php
/**
 * Handles "Monthly Department Activity Analysis" report
 * 
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */

/**
 * Handles "Monthly Department Activity Analysis" report
 *
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */

class report_monthlystats_deptact{
    /**
     * @var object Connection object
     */
	public $conn;

    /**
     * @var object report_common class object
     */
	protected $common;

    /**
     * @var int The number of the month for which the report would be generated
     */
	protected $month;

    /**
     * @var int The year for which the report would be generated
     */
	protected $year;

    /**
     * @var int Stores the start date that would be considered in the report generation as formed from the $month & $year
     */
	protected $periodStart;

    /**
     * @var int Stores the end date that would be considered in the report generation as formed from the $month & $year
     */
	protected $periodEnd;


/**
 * Class constructor
 *
 * @param int $month    The number of the month for which the report would be generated (01 - 12)
 * @param int $year     The year for which the report would be generated
 */
	public function __construct($month, $year){
		$this->conn = new DBConf();
		$this->common = new report_common();
		$this->month = $month;
		$this->year = $year;
		$this->days = $this->common->getDaysInMonth($year, $month);
		$this->periodStart = "$year-$month-01 00:00:00";
		$this->periodEnd = "$year-$month-" . $this->days . " 23:59:59";
	}   //END __construct()



/**
 * Forms an array of values from the result set of running the query stored in $query
 * 
 * @param string $query     The query to be executed to form the array
 * @return array            Returns the generated array with the following elements "dept_id" => "patients"
 */
	protected function getArray($query){
		$query = empty($query) ? "SELECT COUNT(*) patients, dept_id FROM department" : $query;
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			//Store all in an array
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$newArray[$row["dept_id"]] = $row["patients"];
		} else $newArray = array ();
		return $newArray;
	}   //END getArray()
	


/**
 * Gets the number of beds available in each ward
 * @return array The generated array of dept_id => number of beds as generated by getArray()
 */
	protected function getBedsAvail(){	//Beds Available
		$query = "SELECT IFNULL(SUM(w.ward_bedspaces) + SUM(w.ward_siderooms), 0) patients, c.dept_id
					FROM wards w INNER JOIN clinic c
					ON w.clinic_id = c.clinic_id
					GROUP BY c.dept_id";
		return $this->getArray($query);
	}   //END getBedsAvail()


/**
 * Gets the average daily occupancy for the "Inpatient Summary" report
 *
 * @param array $deptArray An array containing all IDs of all available departments
 * @return array The generated array of dept_id => value as generated by getArray()
 */
	protected function getAvgDoccu($deptArray){	//Average Daily Occupancy
		$resArray = array ();
		foreach ($deptArray as $deptid=>$dept){
			$query = "SELECT get_avgbeds_available_perdept('" . $this->month . "', '" . $this->year . "', $deptid)";
			$result = $this->conn->execute($query);
			$row = mysql_fetch_row ($result);
			$resArray[$deptid] = $row[0];
		}
		return $resArray;
	}   //END getAvgDoccu()


/**
 * Gets the total number of vacant beds
 * @return array The generated array of dept_id => value as generated by getArray()
 */
	protected function getBedsVac(){	//Beds Vacant
		$query = "SELECT COUNT(*) patients, c.dept_id
					FROM inpatient_admission inp INNER JOIN wards w INNER JOIN clinic c
					ON inp.ward_id = w.ward_id AND w.clinic_id = c.clinic_id
					WHERE inp.inp_dateattended <= '" . $this->periodEnd . "'
						AND inp.inp_datedischarged IS NULL 
						OR inp.inp_datedischarged = '0000-00-00 00:00:00' 
						OR inp.inp_datedischarged > '" . $this->periodEnd . "'
					GROUP BY dept_id";
		return $this->getArray($query);
	}   //END getBedsVac()


/**
 * Gets the percentage of occupancy
 * @return array The generated array of dept_id => value as generated by getArray()
 */
	protected function getPOccu(){	//% of Occupancy
		//Calculate as follows: total patient days / available bed days
		$query = "";
		return $this->getArray($query);
	}   //END getPOccu()


/**
 * Gets the total number of admissions
 * @return array The generated array of dept_id => value as generated by getArray()
 */
	protected function getAdm(){	//Admissions
		$query = "SELECT COUNT(*) patients, c.dept_id
					FROM inpatient_admission inp INNER JOIN wards w INNER JOIN clinic c
					ON inp.ward_id = w.ward_id AND w.clinic_id = c.clinic_id
					WHERE inp.inp_dateattended BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
					GROUP BY dept_id";
		return $this->getArray($query);
	}   //END getAdm()


/**
 * Gets the total number of discharges
 * @return array The generated array of dept_id => value as generated by getArray()
 */
	protected function getDischarge(){	//Discharges
		$deadID = $this->common->getDeadID();
		$query = "SELECT COUNT(*) patients, c.dept_id
					FROM inpatient_admission inp INNER JOIN wards w INNER JOIN clinic c
					ON inp.ward_id = w.ward_id AND w.clinic_id = c.clinic_id
					WHERE inp.inp_datedischarged BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
						AND inp.inp_conddischargedid <> '$deadID'
					GROUP BY dept_id";
		return $this->getArray($query);
	}   //END getDischarge()


/**
 * Gets the number of deaths per ward
 * @return array The generated array of dept_id => value as generated by getArray()
 */
	protected function getDeath(){	//Deaths
		$deadID = $this->common->getDeadID();
		$query = "SELECT COUNT(*) patients, c.dept_id
					FROM inpatient_admission inp INNER JOIN wards w INNER JOIN clinic c
					ON inp.ward_id = w.ward_id AND w.clinic_id = c.clinic_id
					WHERE inp.inp_datedischarged BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
						AND inp.inp_conddischargedid = '$deadID'
					GROUP BY dept_id";
		return $this->getArray($query);
	}   //END getDeath()



/**
 * Gets the total number of deaths in all departments / wards
 * @return int The total number of deaths
 */
	protected function getTotalDeaths(){	//returns the total number of deaths that occurred in all the wards for a month
		$deadID = $this->common->getDeadID();
		$query = "SELECT COUNT(*) patients FROM inpatient_admission
					WHERE inp_dateattended BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
							AND inp_conddischargedid = '$deadID'";
		$result = $this->conn->execute ($query);
		if ($this->conn->hasRows($result, 1)){
			$row = mysql_fetch_row($result);
			$retVal = $row[0];
		} else $retVal = 0;
		return $retVal;
	}   //END getTotalDeaths()



/**
 * Gets the total available bed days
 * @return array The generated array of dept_id => value as generated by getArray()
 */
	protected function getTBedDays(){	//Total Available Bed Days
		//Get the last day of the month (= number of days in the month) into a variable on the server
		$query = "SELECT DAY(LAST_DAY('" . $this->year . "-" . $this->month . "-01')) INTO @days";
		$result = $this->conn->execute($query);
/*		if ($this->conn->hasRows($result, 1)){
			$row = mysql_fetch_row($result);
			$days = $row[0];
		}*/
		$query = "SELECT (SUM(w.ward_bedspaces) + SUM(w.ward_siderooms)) * @days AS patients, c.dept_id 
					FROM wards w INNER JOIN clinic c
					ON w.clinic_id = c.clinic_id
					GROUP BY c.dept_id";
		return $this->getArray($query);
	}   //END getTBedDays()




/**
 * Gets the total patient days per ward
 * @return array The generated array of dept_id => value as generated by getArray()
 */
	protected function getTPatDays(){	//Total Patient Days
		//Get the last day of the month (= number of days in the month) into a variable on the server
		$query = "SELECT DAY(LAST_DAY('" . $this->year . "-" . $this->month . "-01')) INTO @days";
		$result = $this->conn->execute($query);
		$query = "SELECT c.dept_id, 
					SUM(
						DATEDIFF(
								IFNULL(inp.inp_datedischarged, CONCAT('" . $this->year . "-" . $this->month . "-', @days, ' 23:59:59'))
								, inp.inp_dateattended
						)
					) patients 
					FROM inpatient_admission inp INNER JOIN wards w INNER JOIN clinic c
					ON inp.ward_id = w.ward_id AND w.clinic_id = c.clinic_id
					WHERE inp.inp_dateattended BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
					GROUP BY dept_id";
		return $this->getArray($query);
	}   //END getTPatDays()




/**
 * Gets the average stay length per ward
 * @return array The generated array of dept_id => value as generated by getArray()
 */
	protected function getAvgStayLength(){	//Average stay length
		$query = "SELECT COUNT(*) patients, c.dept_id
					FROM inpatient_admission inp INNER JOIN wards w INNER JOIN clinic c
					ON inp.ward_id = w.ward_id AND w.clinic_id = c.clinic_id
					WHERE inp.inp_dateattended BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
					GROUP BY dept_id";
		return $this->getArray($query);
	}   //END getAvgStayLength()


/**
 * Gets the turn-over interval
 * @return array The generated array of dept_id => value as generated by getArray()
 */
	protected function getTurnOverInterval(){	//Turn Over Interval
		$query = "";
		return $this->getArray($query);
	}   //END getTurnOverInterval()




/**
 * Generates the actual report by calling all other appropriate protected methods to get all the required values and then generate the required HTML with it
 * @param string $deptQuery The query that would be used to get the list of all departments alongside their IDs
 * @param string $total     The string "Total" in the currently selected language
 * @param string $gTotal    The string "Grand Total" in the currently selected language
 * @return array            An array containing the generated HTML and all raw values in a multi-dimensional array
 */
	public function getResults($deptQuery, $total, $gTotal){
		//Get the list of all the depts available in the hospital
		$result = $this->conn->execute ($deptQuery);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			//Store all in an array
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$deptArray[$row["dept_id"]] = $row["dept"];
		} else $deptArray = array();
		
		$bedsAvailArray = $this->getBedsAvail();
		$avgDoccuArray = $this->getAvgDoccu($deptArray);
		$bedsVacArray = $this->getBedsVac();
//		$pOccuArray = $this->getPOccu();
		$admArray = $this->getAdm();
		$dischargeArray = $this->getDischarge();
		$deathArray = $this->getDeath();
//		$pdOverAdmArray = $this->getPDOverAdm();
//		$pdOverTdeathsArray = $this->getPDOverTdeaths();
		$tBedDaysArray = $this->getTBedDays();
		$tPatDaysArray = $this->getTPatDays();
		$avgStayLengthArray = $this->getAvgStayLength();
		$turnOverIntervalArray = $this->getTurnOverInterval();

		//Init needed vars
		$counter = 0;
		$bedsAvailTotal = 0;
		$avgDoccuTotal = 0;
		$bedsVacTotal = 0;
		$pOccuTotal = 0;
		$admTotal = 0;
		$dischargeTotal = 0;
		$deathTotal = 0;
		$pdOverAdmTotal = 0;
		$pdOverTdeathsTotal = 0;
		$tBedDaysTotal = 0;
		$tPatDaysTotal = 0;
		$avgStayLengthTotal = 0;
		$totalPatients = 0;
		$turnOverIntervalTotal = 0;
		$result = "";
		$totalDeaths = $this->getTotalDeaths();
		$rowsArray = $colsArray = array();
		$juggleRow = false;		//Used as a flag to juggle the classes attached to rows
		foreach ($deptArray as $deptid=>$dept){
			//Get the figures to be shown
			$counter++;

			//Get Values
			$bedsAvail = array_key_exists($deptid, $bedsAvailArray) ? $bedsAvailArray[$deptid] : 0;
			$avgDoccu = round($bedsAvail - (array_key_exists($deptid, $avgDoccuArray) ? $avgDoccuArray[$deptid] : 0), 2);
			$bedsVac = $bedsAvail - (array_key_exists($deptid, $bedsVacArray) ? $bedsVacArray[$deptid] : 0);
			$pOccu = 0;
			$adm = array_key_exists($deptid, $admArray) ? $admArray[$deptid] : 0;
			$discharge = array_key_exists($deptid, $dischargeArray) ? $dischargeArray[$deptid] : 0;
			$death = array_key_exists($deptid, $deathArray) ? $deathArray[$deptid] : 0;
			$pdOverAdm = $adm != 0 ? round((float)($death / $adm) * 100, 2) : 0;	//array_key_exists($deptid, $pdOverAdmArray) ? $pdOverAdmArray[$deptid] : 0;
			$pdOverTdeaths = $totalDeaths != 0 ? round((float)($death / $totalDeaths) * 100, 2) : 0;
			$tBedDays = array_key_exists($deptid, $tBedDaysArray) ? $tBedDaysArray[$deptid] : 0;
			$tPatDays = array_key_exists($deptid, $tPatDaysArray) ? $tPatDaysArray[$deptid] : 0;
			$pOccu = $tBedDays != 0 ? round($tPatDays / $tBedDays * 100) : 0;
			$patients = array_key_exists($deptid, $avgStayLengthArray) ? $avgStayLengthArray[$deptid] : 0;
			$patientsDenominator = $patients == 0 ? 1 : $patients;
			$avgStayLength = round((float)$tPatDays / (float)$patientsDenominator, 2);
			$turnOverInterval = array_key_exists($deptid, $turnOverIntervalArray) ? $turnOverIntervalArray[$deptid] : 0;

			//Calculate totals
			$bedsAvailTotal += $bedsAvail;
			$avgDoccuTotal += $avgDoccu;
			$bedsVacTotal += $bedsVac;
//			$pOccuTotal += $pOccu;
			$admTotal += $adm;
			$dischargeTotal += $discharge;
			$deathTotal += $death;
			$pdOverAdmTotal += $pdOverAdm;
			$pdOverTdeathsTotal += $pdOverTdeaths;
			$tBedDaysTotal += $tBedDays;
			$tPatDaysTotal += $tPatDays;
			$totalPatients += $patients;
			$turnOverIntervalTotal += $turnOverInterval;
			$rowClass = $juggleRow ? " class=\"tr-row2\"" : " class=\"tr-row\"";
			$juggleRow = !$juggleRow;

			$result .= "<tr $rowClass>\n
							<th align=\"left\">$counter.</th>\n
							<th align=\"left\">$dept</th>\n

							<td align=\"right\">$bedsAvail</td>\n
							<td align=\"right\">$avgDoccu</td>\n
							<td align=\"right\">$bedsVac</td>\n

							<td align=\"right\">$pOccu%</td>\n
							<td align=\"right\">$adm</td>\n
							<td align=\"right\">$discharge</td>\n

							<td align=\"right\">$death</td>\n
							<td align=\"right\">$pdOverAdm%</td>\n
							<td align=\"right\">$pdOverTdeaths%</td>\n

							<td align=\"right\">$tBedDays</td>\n
							<td align=\"right\">$tPatDays</td>\n
							<td align=\"right\">$avgStayLength</td>\n

							<!--<td align=\"right\">0</td>\n-->

						</tr>\n";
			//To be used in PDF & Excel Export
			$rowsArray[$counter] = array($counter . ".", $dept, $bedsAvail, $avgDoccu, $bedsVac, $pOccu . "%", $adm, $discharge, $death, $pdOverAdm . "%", $pdOverTdeaths . "%", $tBedDays, $tPatDays, $avgStayLength);
			$colsArray[$counter] = array ();
			//TO be used in drawing charts
			foreach ($rowsArray[$counter] as $value)
				array_push ($colsArray[$counter], $value);
			//$chartArray[$deptid] = "[$bedsAvail, $avgDoccu, $bedsVac, $pOccu, $adm, $discharge, $death, $pdOverAdm, $pdOverTdeaths, $tBedDays, $tPatDays, $avgStayLength]";
		}	//END foreach

		if (!empty($result)){
			//Init vars
			$pdOverAdmTotal = $admTotal != 0 ? (round(100 * (float)$deathTotal / (float)$admTotal, 2)) : 0;
			$avgStayLengthTotal = $totalPatients != 0 ? (round((float)$tPatDaysTotal / (float)$totalPatients, 2)) : 0;
			$pOccuTotal = $tBedDaysTotal != 0 ? round($tPatDaysTotal / $tBedDaysTotal * 100) : 0;

			//Add the row containing totals to the result
			$result .= "<tr class=\"title-row\">\n
							<td>&nbsp;</td>\n
							<th align=\"left\">" . strtoupper($total) . "</th>\n
							<th align=\"right\">$bedsAvailTotal</th>\n
							<th align=\"right\">$avgDoccuTotal</th>\n
							<th align=\"right\">$bedsVacTotal</th>\n
							<th align=\"right\">$pOccuTotal%</th>\n

							<th align=\"right\">$admTotal</th>\n
							<th align=\"right\">$dischargeTotal</th>\n
							<th align=\"right\">$deathTotal</th>\n

							<th align=\"right\">$pdOverAdmTotal%</th>\n
							<th align=\"right\">$pdOverTdeathsTotal%</th>\n
							<th align=\"right\">$tBedDaysTotal</th>\n

							<th align=\"right\">$tPatDaysTotal</th>\n
							<th align=\"right\">$avgStayLengthTotal</th>\n
							<!--<th align=\"right\">0</th>\n-->
						</tr>\n";
			$rowsArray[$counter + 1] = array("", strtoupper($total), $bedsAvailTotal, $avgDoccuTotal, $bedsVacTotal, $pOccuTotal . "%", $admTotal, $dischargeTotal, $deathTotal, $pdOverAdmTotal . "%", $pdOverTdeathsTotal . "%", $tBedDaysTotal, $tPatDaysTotal, $avgStayLengthTotal);
		}

		return array($result, $rowsArray);

	}	//END getResult()

}
?>