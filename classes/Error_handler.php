<?php
/**
 * Handles errors that occur in the application especially during development
 * 
 * @package System_Administration
 */

/**
 * Handles errors that occur in the application especially during development
 *
 * @package System_Administration
 */
class Error_handler {


/**
 * The actual method that handles errors
 * @param int $errno        The PHP error number of the error that occurred
 * @param string $errstr    The PHP error description of the error that occurred
 * @param string $errfile   The name of the file i which the error occurred
 * @param int $errline      The line of code on which the error occurred
 * @deprecated              Deprecated since version 1.0
 */
    function myErrorHandler($errno, $errstr, $errfile, $errline){
        switch ($errno) {
            case E_USER_ERROR:
                echo "<b>My ERROR</b> [$errno] $errstr<br />\n";
                echo "  Fatal error in line $errline of file $errfile";
                echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
                echo "Aborting...<br />\n";
                exit(1);
                break;
            case E_USER_WARNING:
                echo "<b>My WARNING</b> [$errno] $errstr<br />\n";
                break;
            case E_USER_NOTICE:
                echo "<b>My NOTICE</b> [$errno] $errstr<br />\n";
                break;
            default:
                echo "Unkown error type: [$errno] $errstr<br />\n";
                break;
        }
    }   //END myErrorHandler()



/**
 * The actual method that handles errors
 * @param int $e_number        The PHP error number of the error that occurred
 * @param string $e_message    The PHP error description of the error that occurred
 * @param string $e_file       The name of the file i which the error occurred
 * @param int $e_line          The line of code on which the error occurred
 * @param array $e_vars        An array containing all defined variables in the current context
 */
    static function my_error_handler ($e_number, $e_message, $e_file, $e_line, $e_vars) {

        global $debug, $contact_email;

        // Build the error message.
        $message = "An error occurred in script '$e_file' on line $e_line: \n<br />$e_message\n<br />";

        // Add the date and time.
        $message .= "Date/Time: " . date('n-j-Y H:i:s') . "\n<br />";

        // Append $e_vars to the $message.
        $message .= "<pre>" . print_r ($e_vars, 1) . "</pre>\n<br />";

        if ($debug) { // Show the error.

            echo '<p class="error">' . $message . '</p>';
            exit;
        } else {

            // Log the error:
            error_log ($message, 1, $contact_email); // Send email.

            // Only print an error message if the error isn't a notice or strict.
            if ( ($e_number != E_NOTICE) && ($e_number < 2048)) {
                echo '<p class="error">A system error occurred. We apologize for the inconvenience.</p>';
            }

        } // End of $debug IF.

    } // End of my_error_handler() definition.
    // Use my error handler:


}   //END class
?>
