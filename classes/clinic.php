<?php
/**
 * Performs a number of tasks on clinics like creating editing and viewing.
 * @author Rufai, Ahmed
 *
 * @package Patient_Care
 */

/**
 * Performs a number of tasks on clinics like creating editing and viewing.
 *
 * @author Rufai, Ahmed
 *
 * @package Patient_Care
 */


Class clinic {
    public $drug=array() ;
    public $drug2=array() ;



/**
 * Class constructor
 */
	 function  __construct() {
        $this->conn = new DBConf();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     */
	public function New_clinic($clinic_name){
		$this->clinic_name = $clinic_name;
	}

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * 
     */
	public function Load_from_key($key_row){
		$result = $this->connection->RunQuery("Select * from clinic where clinic_id = \"$key_row\" ");
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$this->clinic_id = $row["clinic_id"];
			$this->clinic_name = $row["clinic_name"];
		}
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     *
     */
	public function Delete_row_from_key($key_row){
		$this->connection->RunQuery("DELETE FROM clinic WHERE clinic_id = $key_row");
	}

    /**
     * Update the active row table on table
     */
	public function Save_Active_Row(){
		$this->connection->RunQuery("UPDATE clinic set clinic_name = \"$this->clinic_name\" where clinic_id = \"$this->clinic_id\"");
	}

    /**
     * Save the active var class as a new row on table
     */
	public function Save_Active_Row_as_New(){
		$this->connection->RunQuery("Insert into clinic (clinic_name) values (\"$this->clinic_name\"");
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->connection->RunQuery("SELECT clinic_id from clinic order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["clinic_id"];
				$i++;
			}
	return $keys;
	}



/**
 * Gets all the clinics into a drop-down box and selects the clinic of the ward under consideration
 * @param int $id                   The ID of the ward under consideration
 * @param string $curLnag_field     The name of the column in the language table that corresponds to the currently-selected language
 * @param string $langtb            The name of the language table
 */
    function getclinic2($id,$curLnag_field,$langtb) {
            try {
                $getLang = new language();
                $sql2 = 'select clinic_id from wards  where ward_id ='.$id;
                $res2 = $this->conn->execute($sql2);
                if ($row2 = mysql_fetch_array($res2))
                    $id2=$row2['clinic_id'];

                $sql = 'select clinic_id , langcont_id from clinic ';

                $res = $this->conn->execute($sql);
                while ($row = mysql_fetch_array($res)) {
                    $sel = ($id2 == $row['clinic_id']) ? " selected " : " ";
                    echo '<option value ="'.$row['clinic_id'] .'"'. $sel .'>'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
                }   //END while
            } catch (Exception $e) {
            }
    }   //END getclinic2()





/**
 * Gets all clinics without selecting anyone in particular
 * @param string $curLnag_field     The name of the column in the language table that corresponds to the currently-selected language
 * @param string $langtb            The name of the language table
 */
    function getclinic($curLnag_field,$langtb) {
        try {
		    $getLang= new language(); 
            $sql = 'select clinic_id,langcont_id from clinic ';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                echo '<option value ="'.$row['clinic_id'] .'">'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {
        }
    }



/**
 * Get the name of a clinic
 * @param int $id                   The clinic ID
 * @param string $curLnag_field     The name of the column in the language table that corresponds to the currently-selected language
 * @param string $langtb            The name of the language table
 * @return string                   Returns the name of the clinic with ID $id
 */
    function getClinic_name($id,$curLnag_field,$langtb) {
        try {
		     $getLang= new language(); 
            $sql = "select * from clinic where 
			clinic_id =".$id ;
          
		    $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			
            return $getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);

        } catch (Exception $e) {
	
        }
    }



	public function endclinic(){
		$this->connection->CloseMysql();
	}

}   //END class
?>