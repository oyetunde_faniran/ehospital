<?php
/**
 * Used to traverse the service hierarchical tree
 *
 * @author Tunde
 * @package Patient_Care
 */
class Pat_Service_Traverser {

    /**
     * @var int The ID (in the department, unit, service or billing constraint table) of the node  from which searching would start.
     */
    protected $startNodeID;

    /**
     * @var string  The type of the node from which searching would start (Dept/Unit/Servicename/Constraint)
     */
    protected $startNodeType;

    /**
     * @var string  Holds the text that would be searched for to locate a particular entity
     */
    protected $searchText;

    /**
     * @var string  The type of the node that is being searched for (Dept/Unit/Servicename/Constraint)
     */
    protected $searchType;

    /**
     * @var boolean Used as a flag that is set to true once the entity being searched for is found
     */
    protected $found;

    /**
     * @var int The ID of the node that matches the text of the search subject
     */
    protected $foundID;

    /**
     * @var DBConf  An object of the DBConf class
     */
    protected $conn;

/**
 * Class Constructor
 * 
 * @param int $startNodeID          The ID (in the department, unit, service or billing constraint table) of the node  from which searching would start.
 * @param string $startNodeType     The type of the node from which searching would start (Dept/Unit/Servicename/Constraint)
 * @param string $searchText        The text that would be searched for to locate the required node in the service tree
 * @param string $searchType        The type of the node that is being searched for (Dept/Unit/Servicename/Constraint)
 */
    function __construct($startNodeID, $startNodeType, $searchText = "", $searchType = ""){
        $this->startNodeID = (int)$startNodeID;
        $this->startNodeType = $startNodeType;
        $this->conn = new DBConf();
        $this->searchText = admin_Tools::doEscape($searchText, $this->conn);
        $this->searchType = $searchType;
        $this->found = false;
        $this->foundID = 0;
    }   //END __construct()




/**
 * Sets a new value for $this->startNodeID
 * @param <type> $newStartNodeID
 */
    public function setStartNodeID($newStartNodeID){
        $this->startNodeID = $newStartNodeID;
    }   //END setStartNodeID()



/**
 * Sets a new value for $this->startNodeType
 * @param <type> $newStartNodeID
 */
    public function setStartNodeType($newStartNodeType){
        $this->startNodeType = $newStartNodeType;
    }   //END setStartNodeType()




/**
 * Sets a new value for $this->searchText
 * @param <type> $newStartNodeID
 */
    public function setSearchText($newSearchText){
        $this->searchText = $newSearchText;
    }   //END setSearchText()



/**
 * Sets a new value for $this->SearchType
 * @param <type> $newStartNodeID
 */
    public function setSearchType($newSearchType){
        $this->searchType = $newSearchType;
    }   //END setSearchType()



/**
 * Gets the service ID of the node from which searching would start whose ID in its own table is specified in $this->startNodeID and initiates the tree traversal
 */
    public function getServiceID(){
        if (
                empty ($this->startNodeID) ||
                empty ($this->startNodeType) ||
                empty ($this->searchText) ||
                empty ($this->searchType)
            ){
            $retVal = false;
            $this->error = true;
        } else {
            //Get the row ID of this item in the services table
            $query = "SELECT service_id 'id' FROM `service`
                        WHERE dusc_id = '" . $this->startNodeID . "' AND service_type = '" . $this->startNodeType . "'";
            //die ("$query");
            $result = $this->conn->execute($query);
            if ($this->conn->hasRows($result)){
                $row = mysql_fetch_array($result, MYSQL_ASSOC);
                $id = $row["id"];
                //die ("Starting Point");
                $retVal = $this->traverseServiceTree($id, $this->startNodeType);
            } else $retVal = false;
        }
        return $retVal;
    }   //END doTraversal()


/**
 * Recursively traverses the service tree to get to a particular item whose type can be any of (dept(d)/unit(u)/service(s)/billing constraint(s))
 * 
 * @param int $curID        The ID of the current item whose children would be searched
 * @param string $curType   The type of the current item (Dept/Unit/Servicename/Constraint)
 */
    protected function traverseServiceTree($curID, $curType){
        switch ($curType){
            case "Dept":    //Depts would have clinics under them. So, search for clinics under this department.
                $table = " clinic ";
                $column = "clinic_id";
                break;
            case "Unit":    //Clinics would have services under them. So, search for services under this clinic.
                $table = " service_name ";
                $column = "sername_id";
                break;
            case "Servicename":    //Services may (not) have billing constraints under them. So, search for billing constraints under this service.
                $table = " service_constraint ";
                $column = "servcon_id";
                break;
            case "Constraint":    //Constraints may (not) have other billing constraints under them. So, search for other billing constraints under this constraint.
                $table = " service_constraint ";
                $column = "servcon_id";
                break;
            default:
                $retVal = 0;
        }   //END switch

        $retVal = 0;

        if (!empty($table) && !empty($column)){
            //Check all the nodes at the current level to confirm if the node that is sought after is there
            $query_actualSearch = "SELECT s.service_id 'id'
                                    FROM `service` s INNER JOIN $table t INNER JOIN language_content lc
                                    ON s.dusc_id = t.$column AND t.langcont_id = lc.langcont_id
                                    WHERE s.service_parentid = '$curID'
                                    AND LOWER(lc.lang1) LIKE LOWER('%" . $this->searchText . "%')";
            /*echo ("<p>
                      <div>CURRENT ID: $curID</div>
                      <div>CURRENT TYPE: $curType</div>
                      <pre>$query_actualSearch</pre>
                      <hr />
                   </p>");*/
            $result = $this->conn->execute ($query_actualSearch);

            if ($this->conn->hasRows($result)){
                //NODE FOUND
                $this->found = true;
                $row = mysql_fetch_array ($result, MYSQL_ASSOC);
                $this->foundID = $retVal = $row["id"];
            } else {
                //NODE NOT FOUND. So, continue with search by moving to the children of each node at the current level

                //Get all the children of the current node
                $query = "SELECT service_id 'id', service_type 'type' FROM `service`
                            WHERE service_parentid = '$curID'";
                $result = $this->conn->execute ($query);    //This result set is used to search all the nodes on the same level
                if ($this->conn->hasRows($result)){
                    while (($row = mysql_fetch_array($result, MYSQL_ASSOC)) && !$this->found){
                        $retVal = $this->traverseServiceTree($row["id"], $row["type"]);
                    }   //END while
                }

            }   //END else part of if result has rows
        }   //END if (!empty(table & column))

        return $retVal;

    }   //END traverse2Item()




/**
 * Gets the price attached to a particular service using the service ID
 * @param int $serviceID    The service ID of the service under consideration
 * @return float            Returns the price attached to a service with service ID $serviceID
 */
    public function getServicePrice($serviceID){
        $serviceID = (int)$serviceID;
        $query = "SELECT servamount_amount 'amount' FROM service_amount
                    WHERE service_id = '$serviceID'";
        $result = $this->conn->execute($query);
        if ($this->conn->hasRows($result)){
            $retVal = mysql_fetch_array ($result, MYSQL_ASSOC);
            $retVal = $retVal["amount"];
        } else $retVal = 0;
        return $retVal;
    }   //END getServicePrice()




/**
 * Gets the name of a particular service using the service ID
 * @param int $serviceID    The service ID of the service under consideration
 * @return string           Returns the name of the service under consideration
 */
    public function getServiceName($serviceID){
        $serviceID = (int)$serviceID;
        $retVal = "";

        //First of all, get the service type so as to know the actual table from which the name would be fetched
        $query = "SELECT service_type 'type', dusc_id 'id' FROM service WHERE service_id = '$serviceID'";
        $result = $this->conn->execute($query);

        if ($this->conn->hasRows($result)){
            $row = mysql_fetch_array ($result, MYSQL_ASSOC);
            $serviceType = $row["type"];
            $tableRowID = $row["id"];

            //Now, determine the correct table from which the name would be fetched
            switch ($serviceType){
                case "Dept":    //Depts would have clinics under them. So, search for clinics under this department.
                $table = " department ";
                $column = "dept_id";
                break;
            case "Unit":    //Clinics would have services under them. So, search for services under this clinic.
                $table = " clinic ";
                $column = "clinic_id";
                break;
            case "Servicename":    //Services may (not) have billing constraints under them. So, search for billing constraints under this service.
                $table = " service_name ";
                $column = "sername_id";
                break;
            case "Constraint":    //Constraints may (not) have other billing constraints under them. So, search for other billing constraints under this constraint.
                $table = " service_constraint ";
                $column = "servcon_id";
                break;
            }   //END switch()

            //Continue if appropriate table & column names were found
            if (!empty($table) && !empty($column)){
                $query = "SELECT lc.lang1 'name'
                            FROM $table t INNER JOIN language_content lc
                            ON t.langcont_id = lc.langcont_id
                            WHERE t.$column = '$tableRowID'";
                $result = $this->conn->execute($query);
                if ($this->conn->hasRows($result)){
                    $row = mysql_fetch_array ($result, MYSQL_ASSOC);
                    $retVal = $row["name"];
                }   //END if has results
            }   //END if !empty table & column

        }

        return $retVal;
    }   //END getServiceName()



}   //END class
?>
