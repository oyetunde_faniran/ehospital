<?php
/**
 * Handles patient transfer from one clinic to the other
 * 
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */

/**
 * Handles patient transfer from one clinic to the other
 *
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */
class patTransfer {
public	$trans  = array();
private $trn = array();



/**
 * Class Constructor
 */
    function  __construct() {
        $this->conn = new DBConf();
    }   //END __construct()
	


/**
 * Gets the details of a patient transfer
 * @param int $trans_id     The transfer ID
 * @return mixed            Returns an array containing the details of the transfer or false on failure.
 */
   function viewTransfer($trans_id) {
        try {
            $query = "SELECT *
					  FROM patient_transfer 
					  WHERE transfer_id = '".$trans_id."' " ;
								
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);
     	    $num = mysql_num_rows($res) ;

            if ($res) {
              while($row = mysql_fetch_assoc($res)){
                  extract($row);
                  $trn =  array('from_clinic' => $transfer_fromclinicid,
                                'from_dept' => $transfer_fromdeptid,
                                'trans_patadm' => $patadm_id,
                                'to_clinic' => $transfer_toclinicid,
                                'to_dept' => $transfer_todeptid,
                                'trans_consultid' => $consultant_id,
                                'trans_date' => $transfer_date,
                                'trans_status' =>  $transfer_status,
                                'trans_id' => $trans_id,
                                'trans_remark' => $transfer_remark
                                    );
                  return $trn;
              }     //END while()
			
            }else return FALSE;
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END viewTransfer()



/**
 * Saves the details of a new patient transfer
 * @return mixed    Returns the ID of the inserted row or false on failure
 */
	function insertTransfer(){
		try{
		
            $query = "INSERT INTO `patient_transfer` SET `patadm_id` = '".$this->trans['patadm_id']."', 
												 	`consultant_id` = '".$this->trans['consultant_id']."', 
												 	`user_id` = '".$this->trans['user_id']."', 
													`transfer_fromdeptid` = '".$this->trans['from_deptid']."', 
												 	`transfer_fromclinicid` = '".$this->trans['from_clinicid']."',
													`transfer_toclinicid` = '".$this->trans['to_clinicid']."', 
													`transfer_todeptid` = '".$this->trans['to_deptid']."',
													`transfer_remark`  = '".$this->trans['comment']."',
													`transfer_date` = NOW()
													 ";
							  
            $res = $this->conn->execute($query);
			if($res){
			$id = mysql_insert_id();
			return $id; 
			
			}else return FALSE;

		}catch (Exception $e){
		
			set_error_handler (array("Error_handler", "my_error_handler"));
			
		}
	}   //END insertTransfer()
	



/**
 * Updates the details of a patient transfer instance
 * @param int $trans_id     The ID of the transfer
 * @return boolean          Returns true on success or false on failure
 */
	function updateTransfer($trans_id){
		try{
            $query = "UPDATE `patient_transfer` SET	`transfer_toclinicid` = '".$this->trans['to_clinicid']."', 
													`transfer_todeptid` = '".$this->trans['to_deptid']."',
													`transfer_remark`  = '".$this->trans['comment']."',
													`transfer_date` = NOW()
											WHERE transfer_id = '".$trans_id."'";
							  
            $res = $this->conn->execute($query);
			if($res){
			return TRUE; 
			
			}else return FALSE;

		}catch (Exception $e){
		
			set_error_handler (array("Error_handler", "my_error_handler"));
			
		}
	}   //END updateTransfer()




/**
 * Confirms the details of a patient transfer instance
 * @param int $patadm_id        The ID of the medical transaction in the patient_admission table
 * @param int $consultant_id    The ID of the consultant initiating the transfer
 * @return mixed                The ID of the transfer on success or false on failure
 */
   function checkTransfer($patadm_id, $consultant_id) {
        try {
            $query = "SELECT transfer_id
					  FROM patient_transfer 
					  WHERE patadm_id = '".$patadm_id."' AND DATE(transfer_date)= CURDATE() AND consultant_id = '$consultant_id'" ;

            //  $res = mysql_query($query);
            $res = $this->conn->execute($query);
     	    $num = mysql_num_rows($res) ;

            if ($res) {
				if($num>0){
					while($row = mysql_fetch_assoc($res)){
					$trans_id = $row["transfer_id"];
				}
				}else{
				$trans_id =0;
				}
				return $trans_id;
				}else return false;
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END checkTransfer()
	
	
}   //END class

?>
