<?php
/**
 * Description of TransactionHandler
 *
 * @author Faniran, Oyetunde
 */
class TransactionHandler {

    protected $conn;
    protected $insertID;

    function __construct(){
        $this->conn = new DBConf();
    }



    public function insertTransactionTotal($patAdmID, $hospitalNo, $transNo, $amount, $amountNHIS, $status, $invoiceType, $payMethod, $deptID, $clinicID, $userID, $newStatus = 123456){
        //Add the transaction cost
        $amount += TRANSACTION_COST;
        $amountNHIS += TRANSACTION_COST;

        //Now, do the insertion
        $addQuery = $newStatus != 123456 ? " pattotal_newstatus = '" . $newStatus . "', " : "";
        $query = "INSERT INTO pat_transtotal SET
						pattotal_date = NOW(),
						patadm_id = '" . $patAdmID . "',
						reg_hospital_no = '" . $hospitalNo . "',
						pattotal_transno = '" . $transNo . "',
						pattotal_totalamt = '" . $amount . "',
						pattotal_totalamt_nhis = '" . $amountNHIS . "',
						pattotal_status = '" . $status . "',
						pattotal_invoice_type = '" . $invoiceType . "',
						pattotal_paymthd = '" . $payMethod . "',
						dept_id = '" . $deptID . "',
						clinic_id = '" . $clinicID . "',
                        $addQuery
						user_id = '" . $userID . "'";
        $result = $this->conn->run($query);
        $this->insertID = mysql_insert_id($this->conn->getConnectionID());
        return $this->conn->hasRows($result);
    }   //END insertTransactionTotal()


    public function getInsertID(){
        return $this->insertID;
    }   //END getInsertID()



    public function insertTransactionItems($amount, $amountNHIS, $serviceID, $paymentSource, $patTotalID, $totalQty = 0){
        //Insert the items
        $totalCount = count($amount);
        $inserted = 0;
        foreach ($amount as $index => $disAmount){
            $addQuery = is_array($totalQty) ? " patitem_totalqty = '" . $totalQty[$index] . "', " : "";
            $query = "INSERT INTO pat_transitem
                        SET patitem_amount = '" . $disAmount . "',
                            patitem_amount_nhis = '" . $amountNHIS[$index] . "',
                            patservice_id = '" . $serviceID[$index] . "',
                            patitem_paymentsource = '" . (is_array($paymentSource) ? $paymentSource[$index] : $paymentSource) . "',
                            $addQuery
                            pattotal_id = '$patTotalID'";
            $result = $this->conn->run($query);
            if ($this->conn->hasRows($result, 1)){
                $inserted++;
            }
        }

        //Now, insert the transaction cost as another item
        if ($inserted > 0 && TRANSACTION_COST > 0){
            $query = "INSERT INTO pat_transitem
                        SET patitem_amount = '" . TRANSACTION_COST . "',
                            patitem_amount_nhis = '" . TRANSACTION_COST . "',
                            patservice_id = '" . TRANSACTION_COST_SERVICE_ID . "',
                            patitem_paymentsource = '3',    #Patient pays the transaction cost
                            patitem_totalqty = '1',
                            pattotal_id = '$patTotalID'";
            $result = $this->conn->run($query);
        }

        return $inserted > 0 ? true : false;
    }   //END insertTransactionItems()
    
    
    
    public function getTransactionDetails($customTransID){
        $allItems = array();
        $itemCount = 0;
        $paid = false;
        
        $customTransID = admin_Tools::doEscape($toEscape, $this->conn->getConnectionID());
        $query = "SELECT psi.patservice_name 'itemName', pti.patitem_totalqty 'itemQuantity',
                        CASE ptt.pattotal_status
                            WHEN '0' THEN 'Not Paid'
                            WHEN '1' THEN 'Paid'
                        END 'paid'
                    FROM pat_serviceitem psi
                        INNER JOIN pat_transitem pti
                        INNER JOIN pat_transtotal ptt
                    ON psi.patservice_id = pti.patservice_id
                        AND pti.pattotal_id = ptt.pattotal_id
                    WHERE ptt.pattotal_transno = '$customTransID'";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $allItems[] = $row;
            }
        }
        //$retVal = array("items" => $allItems, "itemCount" => $itemCount, "paid" => $paid);
        return $allItems;
    }   //END getTransactionDetails()
    
    
    
    public function serviceRendered4Trans($customTransID){
        $customTransID = admin_Tools::doEscape($toEscape, $this->conn->getConnectionID());
        $query = "UPDATE pat_transtotal
                    SET pattotal_status = '1'
                    WHERE pattotal_transno = '$customTransID'";
        $result = $this->conn->run($query);
    }



}   //END class
?>
