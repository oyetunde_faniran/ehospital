<?php
/**
 * Handles all operations that has to do with page menu
 * 
 * @author Faniran, Oyetunde
 *
 * @package Patient_Care
 */

/**
 * Handles all operations that has to do with page menu
 *
 * @author Faniran, Oyetunde
 *
 * @package System_Administration
 */
class FileUploader {
	protected $conn;
	protected $iResizer;
	public $oldImageLoc;
	public $newImageLoc;
	public $errorMsg;
	public $error;


/**
 * Class Constructor
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param <type> $id            The ID in the menu table of the current menu under consideration. Defaults to zero (0);
 */
	function __construct($oldImageLoc = "", $newImageLoc =""){
		$this->conn = new DBConf();
		$this->oldImageLoc = $oldImageLoc;
		$this->newImageLoc = $newImageLoc;
		$this->error = false;
		$this->errorMsg = "";
	}
	
	
	public function saveImage($imageType, $resize = true, $newWidth = 1024, $newHeight = 1024){
		$this->error = false;
		$this->errorMsg = "";
		try {
			if (empty($this->oldImageLoc) || empty($this->newImageLoc)){
				$this->errorMsg = "Invalid File";
				throw new Exception();
			}
			
			$extension = $this->getImageExtension($imageType);
			if (empty($extension)){
				$this->errorMsg = "Invalid File Type";
				throw new Exception();
			}
			
			if ($resize){
				$this->iResizer = new ImageResizer($this->oldImageLoc, $this->newImageLoc);
				$this->iResizer->resize($newWidth, $newHeight, $extension);
			} else {
				@move_uploaded_file ($this->oldImageLoc, $this->newImageLoc . "." . $extension);
			}
			
		} catch (Exception $e) {
			$this->error = true;
		}
		
		return !$this->error;
		
	}	//END saveImage()




	public function getImageExtension($imageType){
		switch ($imageType){
			case "image/jpeg":
			case "image/pjpeg":
				$ext = "jpg";
				break;
			case "image/gif":
				$ext = "gif";
				break;
			case "image/png":
				$ext = "png";
				break;
			default:
				$ext = "";
		}	//END switch
		return $ext;
	}


	public function logCaseNote($appID, $imageDescription = "", $file){
        $imageDescription = admin_Tools::doEscape($imageDescription, $this->conn);
        $imageDescription = $imageDescription != "Type Document Description Here" ? $imageDescription : "";
        $appID = (int)$appID;
		$query = "INSERT INTO casenote_files
					VALUES ('0', '$appID', '$imageDescription', '$file', NOW(), '" . $_SESSION[session_id() . "userID"] . "')";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result))
			return true;
		else return false;
	}



    public function getUploadedDocuments($appID){
        $query = "SELECT c.*,
                    DATE_FORMAT(casefiles_datetime, '%M %d, %Y') 'date',
                    CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) 'staffName'
                    FROM casenote_files c INNER JOIN staff s
                    ON c.user_id = s.user_id
                    WHERE c.app_id = '$appID'";
        //die ("<pre>$query<pre>");

        $result = $this->conn->run($query);

        if ($this->conn->hasRows($result)){
            $sNo = 0;
            $juggleRows = true;
            $retVal = "<table border=\"1\" cellpadding=\"3\" cellspacing=\"3\">
                        <tr>
                            <td><strong>S/NO</strong></td>
                            <td><strong>DESCRIPTION</strong></td>
                            <td><strong>UPLOADED BY</strong></td>
                            <td><strong>DATE UPLOADED</strong></td>
                            <td><strong>PREVIEW</strong></td>
                        </tr>";
            while ($row = mysql_fetch_array ($result, MYSQL_ASSOC)){
                $fileName = $row["casefiles_filename"];
                $thumb = "./modules/patient_care/casenotes/thumbs/$fileName";
                $bigImage = "./modules/patient_care/casenotes/$fileName";
                if (!empty($fileName) && file_exists($thumb) && file_exists($bigImage)){
                    $classRow = $juggleRows ? " class=\"tr-row\" " : " class=\"tr-row2\" ";
                    $retVal .= "<tr $classRow>
                                    <td>" . (++$sNo) . ".</td>
                                    <td>" . stripslashes($row["casefiles_desc"]) . "</td>
                                    <td>" . stripslashes($row["staffName"]) . "</td>
                                    <td>" . $row["date"] . "</td>
                                    <td><a target=\"_blank\" href=\"$bigImage\"><img border=\"0\" src=\"$thumb\" /></a></td>
                                </tr>";
                    $juggleRows = !$juggleRows;
                }
            }   //END while()
            $retVal .= "</table>";
        } else $retVal = "No treatment sheet uploaded yet for this patient for this appointment.";
        return $retVal;
    }



    public function getAppIDFromCaseNoteID($caseNoteID){
        $caseNoteID = (int)$caseNoteID;
        $query = "SELECT app_id FROM casenote
                    WHERE casenote_id = '$caseNoteID'";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $row = mysql_fetch_array($result, MYSQL_ASSOC);
            $retVal = $row["app_id"];
        } else $retVal = 0;
        return $retVal;
    }


}   //END class



?>