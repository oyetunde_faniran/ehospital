<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventoryCreationFormProcessingClass
 *
 * @author upperlink
 */
class inventory_category_class extends inventoryGeneralClass{
    public $conn;
    
    public function __construct() {
        $conn = new DBConf();
        $this->conn = $conn->mysqliConnect();
        //$this->conn=new mysqli('localhost','root','','mediluth_skye');
    }
    
        public function addNewCategory($name,$desc,$parent){
        global $report;
        $report = "";
        global $inventoryCategoryCreationSuccessNews;
        global $inventoryCategoryCreationFailureNews;
        global $inventory_no_categoryName_error_msg;
        if (strlen($name) < 1) {
            $report=$inventory_no_categoryName_error_msg;
            return $report;
        }
        
       
        $createCategory = $this->conn->query("insert into inventory_categories (inventory_category_name, inventory_category_parent, inventory_category_description, inventory_category_date_created, user_id) values ('".$name."','".$parent."','" .$desc. "',CURRENT_TIMESTAMP, '" .$_SESSION[session_id()."userID"]."')");
        if (!$createCategory) {
            $report=$inventoryCategoryCreationFailureNews;
            return $report;
        } 
            $report=$inventoryCategoryCreationSuccessNews;
            return $report;
    }
    
    public function itIsParent($id){
        global $report;
        global $inventorySubcategorySystemFailureMsg;
        $found=$this->conn->query("select * from inventory_categories where inventory_category_parent='".$id."' order by inventory_category_name asc");
        if(!$found){$report=$inventorySubcategorySystemFailureMsg;
            return $report;
        }
        if($found->num_rows<1){
        return FALSE;
        }
        return $found;
    }
    
    public function showAllCategories(){
        global $report;
        global $click_to_edit;
        global $click_to_remove;
        global $click_to_add_category;
        global $inventorySubcategorySystemFailureMsg;
        $categories=$this->conn->query("select * from inventory_categories where inventory_category_parent='0' order by inventory_category_name");
        if(!$categories){
        $report=$inventorySubcategorySystemFailureMsg;    
            return $report;
              }
        if($categories->num_rows<1){
            return FALSE;
        }
        echo "<ul class=\"categoriesList\">";
        while($category=$categories->fetch_assoc()){
            global $count;
            $count=  $this->countChildren($category['inventory_category_id']);
            echo "<li class=\"closed\" onClick=\"javascript:toggleDisplay(this)\" title=\"".$category['inventory_category_description']."\">".$category['inventory_category_name']."( " .$count." )<a href=\"index.php?p=inv_index&m=pharmacy&folder=inventory_list&&inventoryAction=categories&edit=".$category['inventory_category_id']."\"><img src=\"images/btn_edit.gif\" title=\"".$click_to_edit."\" /></a>&nbsp;&nbsp;<a href=\"index.php?p=inv_index&m=pharmacy&folder=inventory_new&&inventoryAction=new category&add=".$category['inventory_category_id']."\"><img src=\"images/btn_add_02.gif\" title=\"".$click_to_add_category."\" /></a>&nbsp;&nbsp;";
        if($this->itIsParent($category['inventory_category_id'])){
                $this->showSubCategories($category['inventory_category_id']);
            }else{ 
          echo "&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 
           <a href=\"index.php?p=inv_index&m=pharmacy&folder=inventory_list&&inventoryAction=categories&remove=".$category['inventory_category_id']."\" onClick=\"javascript:confirm_to_delete_inventory_category(this.href); return false;\"><img src=\"images/b_drop.png\" title=\"".$click_to_remove."\" /></a>";}
            echo "</li>";
        }
        echo "</ul>";
    }


    public function showSubCategories($id){
        global $result;
        global $click_to_remove;
        global $click_to_edit;
        global $click_to_add_category;
        $subcats=$this->conn->query("select * from inventory_categories where inventory_category_parent='".$id."' order by inventory_category_name");
        if(!$subcats){$result=$inventorySubcategorySystemFailureMsg;
        return $result;
        }
       if($subcats->num_rows>0){?>
       <ul class="categoriesList">
       <?php while($subcat=$subcats->fetch_assoc()){
           global $count;
            $count=  $this->countChildren($subcat['inventory_category_id']);
           echo "<li class=\"closed\" onClick=\"javascript:toggleDisplay(this)\" title=\"".$subcat['inventory_category_description']."\"><table width=\"100%\" align=\"left\" border=\"0\"><tr><td align=\"left\" width=\"150px\">".$subcat['inventory_category_name']."( ".$count." )</td><td align=\"left\" width=\"50px\"><a href=\"index.php?p=inv_index&m=pharmacy&folder=inventory_list&&inventoryAction=categories&edit=".$subcat['inventory_category_id']."\"><img src=\"images/btn_edit.gif\" title=\"".$click_to_edit."\" /></a>&nbsp;&nbsp;</td><td align=\"left\" width=\"50px\"><a href=\"index.php?p=inventory_index&m=pharmacy&folder=inventory_list&&inventoryAction=new category&add=".$subcat['inventory_category_id']."\"><img src=\"images/btn_add_02.gif\" title=\"".$click_to_add_category."\" /></a>&nbsp;</td>";
           if($this->itIsParent($subcat['inventory_category_id'])){
               $this->showSubCategories($subcat['inventory_category_id']);
                }else{echo "<td align=\"left\" width=\"50px\">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <a href=\"index.php?p=inv_index&m=pharmacy&folder=inventory_list&&inventoryAction=categories&remove=".$subcat['inventory_category_id']."\" onClick=\"javascript:confirm_to_delete(this.href,'category'); return false;\"><img src=\"images/b_drop.png\" title=\"".$click_to_remove."\" /></a></td>";}
                echo "</tr></table></li>";
            }echo "</ul>";
        }
    }
    
    public function getCategoryName($id){
        global $report;
        $catnames = $this->conn->query("select inventory_category_name from inventory_categories where inventory_category_id='" . $id . "'");
        if (!$catnames) {
            $report = "System Error: Unable to check category name this time. Please retry";
            return $report;
    }
    if($catnames->num_rows<1){
        $report="None";
        return $report;
        }
        $catname=$catnames->fetch_object();
        $name=$catname->inventory_category_name;
        return $name;
    }
    
    public function getCategoryProperties($id){
        global $report;
        $properties = $this->conn->query("select * from inventory_categories where inventory_category_id='" . $id . "'");
        if (!$properties) {
            $report = "System Error: Unable to check category name this time. Please retry";
            return $report;
    }
    if($properties->num_rows<1){
        $report="No such category found. Please retry!";
        return $report;
        }
        $property=$properties->fetch_array();
        return $property;
    }
    
    public function updateCategory($id, $name, $desc,$author,$parent){
        $inventory_category_update=$this->conn->query("update inventory_categories set inventory_category_name='".$name."',inventory_category_description='".$desc."',user_id='".$author."', inventory_category_parent='".$parent."' where inventory_category_id='$id'");
        if($inventory_category_update){
            return True;
            }else{
                return FALSE;
            }
    }
    
    public function removeCategory($id){
        global $report;
        global $inventory_category_delete_success_msg;
        global $inventory_category_delete_error_msg;
        if($this->itIsParent($id)){
            $report="This category has sub Categories and so cannot be removed untill its children categories have all been removed";
            return $report;
        }
        
        $removed=  $this->conn->query("delete from inventory_categories where inventory_category_id='".$id."'");
        if(!$removed){
            $report=$inventory_category_delete_error_msg;
            return $report;
            }else{
                $report=$inventory_category_delete_success_msg;
                return $report;
            }
    }
   
    public function countChildren($id){
        $childrenCount=$this->conn->query("select * from inventory_categories where inventory_category_parent='".$id."'");
        if(!$childrenCount){
            return FALSE;
        }
        return $childrenCount->num_rows;
    }
   
    
    public function listCategories($id) {
        global $report;
        global $click_to_edit;
        global $click_to_remove;
        global $click_to_add_category;
        global $inventory_category;
        global $inventory_no_category_found_msg;
        global $inventorySubcategorySystemFailureMsg;
        $categories = $this->conn->query("select * from inventory_categories where inventory_category_parent='" . $id . "' order by inventory_category_name");
        if (!$categories) {
            $report = $inventorySubcategorySystemFailureMsg;
            return $report;
        }
        if ($categories->num_rows < 1) {
            $report=$inventory_no_category_found_msg;
            return $report;
        }
        return $categories;
        }
        
    public function getChildren($parentID){
        $retVal = "";
        $disChildren = $prodCat->getChildCategories($parentID);
        if ($startRowStyle == " class=\"tr-row\" "){
            $rowStyle1 = " class=\"tr-row2\" ";
            $rowStyle2 = " class=\"tr-row\" ";
        } else {
            $rowStyle1 = " class=\"tr-row\" ";
            $rowStyle2 = " class=\"tr-row2\" ";
        }
		$juggleRows = true;
        $sNo = 0;
        foreach ($disChildren as $cat){
			$sNo++;
            $currentCount++;
			$rowStyle = $juggleRows ? $rowStyle1 : $rowStyle2;
			$juggleRows = !$juggleRows;
			$retVal .= "<tr $rowStyle>
							<td>" . $sNoPrefix . $sNo . "</td>
							<td id=\"payment_item_" . $cat["prodcat_id"] . "\"><label for=\"category_" . $cat["prodcat_id"] . "\">$prefix" . stripslashes($cat["prodcat_name"]) . "</label></td>
							<td align=\"center\"><input type=\"checkbox\" name=\"category[]\" id=\"category_" . $cat["prodcat_id"] . "\" value=\"" . $cat["prodcat_id"] . "\" /></td>
						</tr>";
            $sNoPrefixNew = $sNoPrefix . $sNo . ".";
            $retVal .= getChildren($cat["prodcat_id"], $institutionID, $prefix . "<span style=\"color: #AAA;\">------&raquo;</span> ", $sNoPrefixNew, $rowStyle);
		}   //END foreach
        
        return $retVal;
        
    }   //END getChildren()
    
    
    public function getChildCategories($parentID){
        $retVal = array();
        $parentID = (int)$parentID;
        $query = "SELECT * FROM inventory_categories
                    WHERE inventory_category_parent = '$parentID'
                    ORDER BY inventory_category_name";
        $result = $this->conn->query($query);
        if ($result->num_rows > 0){
            while ($row = $result->fetch_assoc()){
                $retVal[] = $row;
            }
        }
        return $retVal;
    }   //END getChildCategories()


}
?>

        