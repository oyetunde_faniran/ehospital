<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 26-08-2009
 * 
 * Version of MYSQL_to_PHP: 1.0
 * 
 * License: LGPL 
 * 
 */
require_once './classes/DBConf.php';

Class inpatient_admission {

public $inp=array() ;
public $inp2=array() ;
	/*public $inp_id; //int(10)
	public $patadm_id; //int(10)
	public $inp_dateattended; //datetime
	public $inp_datedischarged; //datetime
	public $inp_dischargedto; //varchar(255)
	public $inp_conddischarged; //varchar(255)
	public $inp_refferer; //varchar(255)
	public $ward_id; //int(10)
	public $ct_id; //int(10)
	public $conn;*/

	 function  __construct() {
        $this->conn = new DBConf();
    }

    /**
     * New object to the class. Don�t forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New(); 
     *
     */
	public function set_inpatient_admission($patadm_id,$inp_dateattended,$inp_datedischarged,$inp_dischargedto,$inp_conddischarged,$inp_refferer,$ward_id,$ct_id){
		$this->patadm_id = $patadm_id;
		$this->inp_dateattended = $inp_dateattended;
		$this->inp_datedischarged = $inp_datedischarged;
		$this->inp_dischargedto = $inp_dischargedto;
		$this->inp_conddischarged = $inp_conddischarged;
		$this->inp_refferer = $inp_refferer;
		$this->ward_id = $ward_id;
		$this->ct_id = $ct_id;
		
	}

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param key_table_type $key_row
     * 
     */
	public function Load_from_key($key_row){
		$result = $this->conn->execute("Select * from inpatient_admission where inp_id = \"$key_row\" ");
		while($row = mysql_fetch_array($result)){
			$this->inp_id = $row["inp_id"];
			$this->patadm_id = $row["patadm_id"];
			$this->inp_dateattended = $row["inp_dateattended"];
			$this->inp_datedischarged = $row["inp_datedischarged"];
			$this->inp_dischargedto = $row["inp_dischargedto"];
			$this->inp_reffererdesc = $row["inp_reffererdesc"];
			$this->inp_conddischargedid = $row["inp_conddischargedid"];
			$this->refferer_id = $row["refferer_id"];
			$this->ward_id = $row["ward_id"];
			$this->ct_id = $row["ct_id"];
		}
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param key_table_type $key_row
     *
     */
	public function Delete_row_from_key($key_row){
		$this->conn->execute("DELETE FROM inpatient_admission WHERE inp_id = $key_row");
	}

    /**
     * Update the active row table on table
     */
	public function Save_Active_Row($id,$id2,$id3,$id4,$tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("UPDATE drugcategory set drugcat_name = \"$this->drugcat_name\" where drugcat_id = \"$this->drugcat_id\"");*/
		
		try {
		$q=count($this->inp);
		
		
		for ($i = 0; $i < $q; $i++) {
			if($this->inp[$i]=='langcont_id')
		//	$qq="UPDATE $langtb set $curLnag_field ='".$this->inp2[$i]."' where langcont_id = \"$lang_id\"";
				
		$this->conn->execute("UPDATE $langtb set $curLnag_field ='".$this->inp2[$i]."' where langcont_id = \"$lang_id\"");

		}
		
			
	$sql = "UPDATE $tablename SET ";	
	
				$qq=count($this->inp);
				$q=count($this->inp);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->inp[$i]=='langcont_id')
						     $sql.=    $this->inp[$i] .'="'.$lang_id.'" ';
						else
         			  		  $sql.=    $this->inp[$i] .'="'.$this->inp2[$i].'" ';
			 
					 }
					 else{
					     if($this->inp[$i]=='langcont_id')
						     $sql.=      $this->inp[$i] .'="'.$lang_id.'" ,';
						 else
						     $sql.=      $this->inp[$i] .'="'.$this->inp2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			$result2 =$this->conn->execute("SELECT * from $tablename");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
		
         $res=$this->conn->execute($sql);
		 if (mysql_affected_rows()==1){
		 	
		 	if(!empty($id3) && $id2=='editinp'){
		
					if($id4!=$id3){
			
					    $num=$this->getBedspaces_available($id3);
						 
						 $num=$num+1;
						 
						 $sql3="UPDATE wards set ward_availablebedspaces='$num' WHERE ward_id='$id3'";
						 $res3=$this->conn->execute($sql3);
						 if (mysql_affected_rows()==1) {
						 $num=$this->getBedspaces_available($id4);
						 
						 $num=$num-1;
								 $sql4="UPDATE wards set ward_availablebedspaces='$num' WHERE ward_id='$id4'"; 
								 $res4=$this->conn->execute($sql4);
								 
							 }
					}
			}
		        
		} 
		// return true;
        
		
		
		
		} catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		
		
		
		
	}

    /**
     * Save the active var class as a new row on table
     */
	public function Save_Active_Row_as_New($id,$id2,$tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("Insert into drugcategory (drugcat_name) values (\"$this->drugcat_name\"");*/
		try {
		$q=count($this->inp);
		
		
		for ($i = 0; $i < $q; $i++) {
			if($this->inp[$i]=='langcont_id'){
			$qq="Insert into $langtb ($curLnag_field) values ('".$this->inp2[$i]."')";
			//echo $qq;
			    $this->conn->execute($qq);
				$content_id= mysql_insert_id();
			}	
		}
		
		$sql = "INSERT INTO $tablename SET ";	
	
				$qq=count($this->inp);
				//$q=count($this->inp);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->inp[$i]=='langcont_id')
						     $sql.=    $this->inp[$i] .'="'.$content_id.'" ';
						else
         			  		  $sql.=    $this->inp[$i] .'="'.$this->inp2[$i].'" ';
			 
					 }
					 else{
					     if($this->inp[$i]=='langcont_id')
						     $sql.=      $this->inp[$i] .'="'.$content_id.'" ,';
						 else
						     $sql.=      $this->inp[$i] .'="'.$this->inp2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			$sql2="UPDATE pat_admrecommend set patadmrec_flag=0 WHERE patadm_id=".$id;
			
			 	echo $sql;
			 $res=$this->conn->execute($sql);
		  if(mysql_affected_rows()==1)
			 $res=$this->conn->execute($sql2);
		   if ($res){
		 		 $num=$this->getBedspaces_available($id2);
				 $num=$num-1;
		   		 $sql3="UPDATE wards set ward_availablebedspaces='$num' WHERE ward_id='$id'";
			     $res3=$this->conn->execute($sql3);
		      
		   }
		   
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->conn->execute("SELECT inp_id from inpatient_admission order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["inp_id"];
				$i++;
			}
	return $keys;
	}
	
		function getCondtype_id($curLnag_field,$langtb) {
        try {
		    $getlang= new language();
            $sql = 'select * from conditiontype WHERE ct_type="0"';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                echo '<option value ="'.$row['ct_id'] .'">'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {


        }
    }
	function getBedspaces_available($ward_id) {
        try {
		    $getlang= new language();
            $sql = "SELECT * FROM wards WHERE ward_id='$ward_id'";
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
             return $row['ward_availablebedspaces'];
            }
        } catch (Exception $e) {


        }
    }
	
	function getCondtype_id2($id,$table,$curLnag_field,$langtb) {
        try {
		    $getLang= new language();
			$sql2 = 'select ct_id from '. $table . ' where inp_id ='.$id;
			
				 $res2 = $this->conn->execute($sql2);
				 if( $row2 = mysql_fetch_array($res2))
				  $id2=$row2['ct_id'];
            $sql = 'select * from conditiontype WHERE ct_type="0"';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
			$sel= ($id2==$row['ct_id'])? "selected":" ";
                echo '<option value ="'.$row['ct_id'].'"'. $sel.'>'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {


        }
    }
	function getCondtype_id3($id,$table,$curLnag_field,$langtb) {
        try {
		    $getLang= new language();
			$sql2 = 'select ct_id from '. $table . ' where inp_id ='.$id;
			
				 $res2 = $this->conn->execute($sql2);
				 if( $row2 = mysql_fetch_array($res2))
				  $id2=$row2['ct_id'];
            $sql = 'select * from conditiontype WHERE ct_type="1"';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
			$sel= ($id2==$row['ct_id'])? "selected":" ";
                echo '<option value ="'.$row['ct_id'].'"'. $sel.'>'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {


        }
    }
	function getCondtype_name($id,$curLnag_field,$langtb) {
        try {
		$getLang= new language();
            $sql = "select * from conditiontype where 
			ct_id =".$id ;
          
		    $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
			return $getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
        } catch (Exception $e) {

	
        }
    }
	
	
function rowSearch($search_field,$search_value){
		 try {
		 $db2 = new DBConf();
		$sql = "Select * from  inpatient_admission  where  ".$search_field."='".$search_value."'" ;
		$pagelink32='inp';
$pager = new PS_Pagination($db2,$sql,200,10,$pagelink32);
          $rs = $pager->paginate();
		//$res =$this->conn->execute($sql);
	  // echo "entered";
	     $i=1;
		 while ($row = mysql_fetch_array($rs)) {
            $hospital= new patient_admission();
		    $wards= new wards();
			$refer= new patient_referrer();
	        $patadm=$hospital->gethospital_name($row["patadm_id"]);
			$fullname=$hospital->getPatient_name($row["patadm_id"]);
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkwardID[]" value="'.$row["patadm_id"].'" />'.$i.'</td>
        <td>'.$patadm.'</td>
        <td>'.$fullname.'</td>
        <td>'.$row["inp_dateattended"] .'</td>
       	<td>'.$row["inp_dischargedto"].'</td>
			<td>'.$refer->getReferrer_name($row["refferer_id"]).'</td>
			<td>'.$row["inp_reffererdesc"].'</td>
			<td>'.$wards->getWard_name($row["ward_id"]).'</td>
        <td>'.'<a href = "./index.php?jj=delete&p=inp&id='.$row["inp_id"] .'"><img src="./images/btn_delete_02.gif"  style="border:  
		none"/></a></td>
		<td>'.'<a href = "./index.php?p=editinp&patadm_id='.$row["patadm_id"].'&inp_id='.$row["inp_id"] .'"><img 
		src="./images/btn_edit.gif"  style="border: none"/></a></td>
		<td>'.'<a href = "./index.php?p=viewinp&patadm_id='.$row["patadm_id"].'&inp_id='.$row["inp_id"] .'"><img 
		src="./images/viewemprep_o.gif"  style="border: none"/>Patient Details</a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
		} catch (Exception $e) {
        }
	}
	
	public function GetSearchFields(){
	echo 	"<option value='-1'>Select Field</option>";	
		$result =$this->conn->execute("SELECT * from inpatient_admission order by inp_id");
				$i = 0;
				
while ($i < mysql_num_fields($result)) {
 //   echo "Information for column $i:<br />\n";
    $meta = mysql_fetch_field($result);
	
    if (!$meta) {
      //  echo "No information available<br />\n";
    }
  //  echo "$meta->name";

		
	echo 	"<option value=". $meta->name.">".$meta->name."</option>";
	    $i++;
}
/*mysql_free_result($result);
	return $keys;*/
	}



	
function allrows($curLnag_field,$langtb) {
        try {
		$db2 = new DBConf();
            $sql = "SELECT
* FROM
inpatient_admission ,
pat_conditiontrack 
WHERE
inpatient_admission.patadm_id =  pat_conditiontrack.patadm_id AND
pat_conditiontrack.ctc_id='1'   AND 
(inpatient_admission.inp_datedischarged IS NULL OR inpatient_admission.inp_datedischarged='0000-00-00 00:00:00')
";
//echo $sql;
//exit();
  $pagelink32='inp';
$pager = new PS_Pagination($db2,$sql,200,10,$pagelink32);
$rs = $pager->paginate();
            //$res = $this->conn->execute($sql);
			$i=1;
            while ($row = mysql_fetch_array($rs)) {
             
			$hospital= new patient_admission();
		    $wards= new wards();
			$refer= new patient_referrer();
	        $patadm=$hospital->gethospital_name($row["patadm_id"]);
			$fullname=$hospital->getPatient_name($row["patadm_id"]);
		
			    echo' <tr>
        <td><input type="checkbox"  name="chkwardID[]" value="'.$row["patadm_id"].'" /></td>
    	<td >'.$patadm.'</td>
        <td >'.$fullname.'</td>
        <td >'.$row["inp_dateattended"] .'</td>
       	<td>'.$wards->getWard_name($row["ward_id"],$curLnag_field,$langtb).'</td>
        <td>'.'<a href = "./index.php?jj=delete&p=inp&id='.$row["inp_id"] .'"></a></td><td>'.'<a href = "./index.php?p=editinp&patadm_id='.$row["patadm_id"].'&inp_id='.$row["inp_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
       <td>'.'<a href = "./index.php?p=viewinp&patadm_id='.$row["patadm_id"].'&inp_id='.$row["inp_id"] .'" title="Go into Ward"><img 
		src="./images/viewemprep_o.gif"  style="border: none"/>Details</a></td>
	    </tr>';
		
			   // echo' <tr>
//        <td><input type="checkbox"  name="chkwardID[]" value="'.$row["patadm_id"].'" /></td>
//    	<td >'.$patadm.'</td>
//        <td >'.$fullname.'</td>
//        <td >'.$row["inp_dateattended"] .'</td>
//       	<td >'.$refer->getReferrer_name($row["refferer_id"],$curLnag_field,$langtb).'</td>
//		<td>'.$row["inp_reffererdesc"].'</td>
//		<td>'.$wards->getWard_name($row["ward_id"],$curLnag_field,$langtb).'</td>
//        <td>'.'<a href = "./index.php?jj=delete&p=inp&id='.$row["inp_id"] .'"></a></td><td>'.'<a href = "./index.php?p=editinp&patadm_id='.$row["patadm_id"].'&inp_id='.$row["inp_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
//       <td>'.'<a href = "./index.php?p=viewinp&patadm_id='.$row["patadm_id"].'&inp_id='.$row["inp_id"] .'" title="Go into Ward"><img 
//		src="./images/viewemprep_o.gif"  style="border: none"/>Details</a></td>
//	    </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
        } catch (Exception $e) {
	
        }
 

    }
    /**
     * Close mysql conn
     */
	public function endinpatient_admission(){
		$this->conn->CloseMysql();
	}

}

