<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventoryCreationFormProcessingClass
 *
 * @author upperlink
 */
class inventory_vendor_class extends inventoryGeneralClass {
    public $conn;
    
    public function __construct() {
        $conn = new DBConf();
        $this->conn = $conn->mysqliConnect();
        //$this->conn=new mysqli('localhost','root','','mediluth_skye');
    }
    
   public function addNewVendor($name,$address,$yrOfInc, $rcno, $license, $tel, $mobile, $email,$website){
       global $report;
       global $inventorySystemFailureAddingVendor;
       global $inventory_vendor_added_successfully;
       global $inventory_no_vendorName_error_msg;


       if(strlen($name)<1){
           $report=$inventory_no_vendorName_error_msg;
           return $report;
       }
       
       $addVendorQuery=$this->conn->query("insert into inventory_vendors (inventory_vendor_name,inventory_vendor_address, inventory_vendor_yrOfInc, inventory_vendor_rcno,  inventory_vendor_license, inventory_vendor_tel, inventory_vendor_mobile, inventory_vendor_email,inventory_vendor_website,user_id,inventory_vendor_date_created) 
           values 
           ('".$name."','".$address."','".$yrOfInc."','".$rcno."','".$license."','".$tel."','".$mobile."','".$email."','".$website."','".$_SESSION[session_id()."userID"]."',CURRENT_TIMESTAMP)");
       
       if(!$addVendorQuery){
           $report=$inventorySystemFailureAddingVendor;
           return $report;
       }
       $report=$inventory_vendor_added_successfully;
       return $report;
   }
   
    public function updateVendor($name,$address,$yrOfInc, $rcno, $license, $tel, $mobile, $email,$website){
       global $report;
       global $inventorySystemFailureUpdateVendor;
       global $inventory_vendor_updated_successfully;
       global $inventory_no_vendorName_error_msg;


       if(strlen($name)<1){
           $report=$inventory_no_vendorName_error_msg;
           return $report;
       }
       
       $updateVendorQuery=$this->conn->query("update inventory_vendors set inventory_vendor_name='".$name."',inventory_vendor_address='".$address."', inventory_vendor_yrOfInc='".$yrOfInc."', inventory_vendor_rcno='".$rcno."', inventory_vendor_license='".$license."', inventory_vendor_tel='".$tel."', inventory_vendor_mobile='".$mobile."', inventory_vendor_email='".$email."',inventory_vendor_website='".$website."',user_id='".$_SESSION[session_id()."userID"]."',inventory_vendor_date_created=CURRENT_TIMESTAMP"); 
       
       if(!$updateVendorQuery){
           $report=$inventorySystemFailureUpdateVendor;
           return $report;
       }
       $report=$inventory_vendor_updated_successfully;
       return $report;
   }
   
   public function listVendors(){
       global $report;
       global $inventory_vendor_list_failure_msg;
       global $inventory_noVendor_msg;
       $vendorQuery=$this->conn->query("select * from inventory_vendors order by inventory_vendor_name asc");
       if(!$vendorQuery){
           $report=$inventory_vendor_list_failure_msg;
           return $report;
       }
       if($vendorQuery->num_rows<1){
           $report=$inventory_noVendor_msg;
           return $report;
       }
       
       return $vendorQuery;
       }
       
       public function getVendorDetails($id){
           global $report;
           global $inventory_vendor_info_failure_msg;
           global $inventory_vendor_notFound_msg;
           $details=  $this->conn->query("select * from inventory_vendors where inventory_vendor_id='".$id."'");
           if(!$details){
               $report=$inventory_vendor_info_failure_msg;
               return $report;
           }
           if($details->num_rows!=1){
               $report=$inventory_vendor_notFound_msg;
               return $report;
           }
           $vendor=$details->fetch_assoc();
       return $vendor;
       }
       
       public function removeVendor($id){
        global $report;
        global $inventory_vendor_delete_success_msg;
        global $inventory_vendor_delete_error_msg;
       
        
        $removed=  $this->conn->query("delete from inventory_vendors where inventory_vendor_id='".$id."'");
        if(!$removed){
            $report=$inventory_vendor_delete_error_msg;
            return $report;
            }else{
                $report=$inventory_vendor_delete_success_msg;
                return $report;
            }
   
       }

}

?>
