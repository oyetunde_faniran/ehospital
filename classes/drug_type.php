<?php
/*
 *Contains class variables & methods to select, save, update and delete rows from drug type table
 *@package drug_type
 *@author: Rufai Ahmed
 *Create Date: 7-11-2009
 */
require_once './classes/DBConf.php';

Class drug_type {

 /**
  *
  * @var array holds array of fields in the drugs type table
  */
    public $dtype=array() ;
    /**
     * @var array holds values to be stored in the drug type table field
       */
    public $dtype2=array() ;
	public $drugtype_id; //int(10) unsigned
	public $langcont_id; //int(10) unsigned
	public $drugtype_order; //smallint(5) unsigned
	public $connection;
    public $conn;
	
	/**
     * Class constructor
     */
	 function  __construct() {
     $this->conn = new DBConf();
    }
	
    /**
     * temporarily holds intemediate values from the database table or other script in the class
     * @param int $langcont_id
     * @param string $drugtype_order
     */
     
	public function New_drug_type($langcont_id,$drugtype_order){
		$this->langcont_id = $langcont_id;
		$this->drugtype_order = $drugtype_order;
	}

    /**
     *Load one row into class variables based on specified row key
     * @param int $key_row
     */
     
    
	public function Load_from_key($key_row){
		$result = $this->conn->execute("Select * from drug_type where drugtype_id = \"$key_row\" ");
		while($row = mysql_fetch_array($result)){
			$this->drugtype_id = $row["drugtype_id"];
			$this->langcont_id = $row["langcont_id"];
			$this->drugtype_order = $row["drugtype_order"];
		}
	}
/**
 * holds result handle of sql execution on a table
 */
    public function Load_from_table(){
		$result = $this->conn->execute("Select * from drug_type");
			}
    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     *
     */
	public function Delete_row_from_key($key_row){
		$this->conn->execute("DELETE FROM drug_type WHERE drugtype_id = $key_row");
	}

    /**
     *Update the active row on table
     * @param int $lang_id
     * @param int $id
     * @param string $tablename
     * @param string $curLnag_field
     * @param string $langtb
     */
     
public function Save_Active_Row($lang_id,$id,$tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("UPDATE drugcategory set drugcat_name = \"$this->drugcat_name\" where drugcat_id = \"$this->drugcat_id\"");*/
		
		try {
		$q=count($this->dtype);
		
		
		for ($i = 0; $i < $q; $i++) {
			if($this->dtype[$i]=='langcont_id')
		//	$qq="UPDATE $langtb set $curLnag_field ='".$this->dtype2[$i]."' where langcont_id = \"$lang_id\"";
				
		$this->conn->execute("UPDATE $langtb set $curLnag_field ='".$this->dtype2[$i]."' where langcont_id = \"$lang_id\"");

		}
		
			
	$sql = "UPDATE $tablename SET ";	
	
				$qq=count($this->dtype);
				$q=count($this->dtype);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->dtype[$i]=='langcont_id')
						     $sql.=    $this->dtype[$i] .'="'.$lang_id.'" ';
						else
         			  		  $sql.=    $this->dtype[$i] .'="'.$this->dtype2[$i].'" ';
			 
					 }
					 else{
					     if($this->dtype[$i]=='langcont_id')
						     $sql.=      $this->dtype[$i] .'="'.$lang_id.'" ,';
						 else
						     $sql.=      $this->dtype[$i] .'="'.$this->dtype2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			$result2 =$this->conn->execute("SELECT * from $tablename");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
		//echo $sql;
	//exit();
         $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		
		
		
		
	}

    /**
     * Save as a new row on table
     * @param string $tablename
     * @param string $curLnag_field
     * @param string $langtb
     * @return array
     */
    
	public function Save_Active_Row_as_New($tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("Insert into drugcategory (drugcat_name) values (\"$this->drugcat_name\"");*/
		try {
					// server validation begins
					$getLang= new language();		
					$xlp_formfield =$this->dtype;
					$xlp_fieldmessage = array('Please specify Drug Form');
		            $xlp_error=$getLang->xlpildator($xlp_formfield,$xlp_fieldmessage);
					if (!empty($xlp_error[2]))
					throw new Exception;
					//sever validation ends
					
					$q=count($this->dtype);
					for ($i = 0; $i < $q; $i++) {
						if($this->dtype[$i]=='langcont_id'){
						$qq="Insert into $langtb ($curLnag_field) values ('".$this->dtype2[$i]."')";
						//echo $qq;
							$this->conn->execute($qq);
							$content_id= mysql_insert_id();
						}	
					}
				
				$sql = "INSERT INTO $tablename SET ";	
			
						$qq=count($this->dtype);
						//$q=count($this->dtype);
		
					for ($i = 0; $i < $qq; $i++) {
		
			  
							if(($q-1)==0){
								if($this->dtype[$i]=='langcont_id')
									 $sql.=    $this->dtype[$i] .'="'.$content_id.'" ';
								else
									  $sql.=    $this->dtype[$i] .'="'.$this->dtype2[$i].'" ';
					 
							 }
							 else{
								 if($this->dtype[$i]=='langcont_id')
									 $sql.=      $this->dtype[$i] .'="'.$content_id.'" ,';
								 else
									 $sql.=      $this->dtype[$i] .'="'.$this->dtype2[$i].'" ,';
					              
							 }
						 $q--;
					}
					$sql.=" ,drugtype_order=".$this->getNext_order();
				 $result=$this->conn->execute($sql);
				 $list=array();
				if($this->conn->hasRows($result,  0)){
					 $list[3]=true;
					return $list;	 
					}			 
        } catch (Exception $e) {
		   //echo "$xlp_error[1]";
            return $xlp_error;
        }
	}
	/**
     * populate option tags with table field
     */
	public function GetSearchFields(){
		echo 	"<option value='-1'>Select Field</option>";	
		$result =$this->conn->execute("SELECT * from drug_type order by drugtype_id");
				$i = 0;
				
			while ($i < mysql_num_fields($result)) {
 //   echo "Information for column $i:<br />\n";
	  		  $meta = mysql_fetch_field($result);
	
 		   if (!$meta) {
      //  echo "No information available<br />\n";
  		  }
  //  echo "$meta->name";
  
		if($meta->name=='langcont_id')
	echo 	"<option value=". $meta->name.">".'Drug Priority'."</option>";
   else
	echo 	"<option value=". $meta->name.">".$meta->name."</option>";
	   
	   
	   
	   
	    $i++;
}
/*mysql_free_result($result);
	return $keys;*/
	}
	/**
     *
     * @param integer $id
     * @param string $curLnag_field
     * @param string $langtb
     * @return string
     */
	function getdrug_name($id,$curLnag_field,$langtb) {
        try {
		  $getLang= new language();
           $sql = 'Select * from drug_type  where drugtype_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
		return	$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
        //    return $row['drug_desc'];
				 
            
			
        } catch (Exception $e) {


        }
    }
	/**
     *get next order ID
     * @return int
     */
	function getNext_order() {
        try {
	
           $sql = 'Select max(drugtype_order) as drugtype_order from drug_type';
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			
		return	$row['drugtype_order']+1;
              
			
        } catch (Exception $e) {


        }
    }
    /**
     *
     * @param int $id row ID
     * @param int $id2 order ID
     * 
     *
     */
	function reOrder($id,$id2) {
        try {
	
           $sql = 'UPDATE drug_type SET
		                 drugtype_order='.$id2.'  WHERE  drugtype_id='.$id;
						 
            $res = $this->conn->execute($sql);
            
			
		//return	$sql ;
              
			
        } catch (Exception $e) {


        }
    }
/*function getlang_content($id,$curLnag_field,$langtb) {
        try {
           $sql = 'Select '.$curLnag_field.'  from '. $langtb.'  where langcont_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			return $row[$curLnag_field];
		} catch (Exception $e) {


        }
    }*/
    /**
     *
     * @param <type> $id
     * @param string $curLnag_field
     * @param string $langtb
     */
function getdrug_type2($id,$curLnag_field,$langtb) {
        try {
		    $getLang= new language();
			$sql2 = 'select drugtype_id from drug_type  where drugtype_id ='.$id;
			
				 $res2 = $this->conn->execute($sql2);
				 if($res2){
                     $row2 = mysql_fetch_array($res2);
                      $id2=$row2['drugtype_id'];
                 }
            $sql = 'select drugtype_id,langcont_id from drug_type';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
			$sel= ($id2==$row['drugtype_id'])? "selected":" ";
			
                echo '<option value ="'.$row['drugtype_id'] .'"'. $sel.'>'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {


        }
    }
/**
 * populate option tags of selct element
 * @param string $curLnag_field
     * @param string $langtb
 */
	
function getdrug_type($curLnag_field,$langtb) {
        try {
		    $getLang= new language();
            $sql = 'select drugtype_id,langcont_id from drug_type';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                echo '<option value ="'.$row['drugtype_id'] .'">'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
               }
        } catch (Exception $e) {


        }
    }

	/**
     *
     * @param string $curLnag_field
     * @param string $langtb
     */
function allrows($curLnag_field,$langtb) {
        try {
	 $getlang= new language();
	$db2 = new DBConf();
		 $sql = 'SELECT
*
FROM
drug_type order by drugtype_order
';
$pageindex='drugtype';
$pager = new PS_Pagination($db2,$sql,10,10,$pageindex);
$rs = $pager->paginate();
 $offset=$pager->offset;          

           // $res = $this->conn->execute($sql);
			$i=1;
           while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    if ($i%2 ==0) {$bgcolor = "tr-row";} else {$bgcolor = "tr-row2";} 
			    echo' <tr class="'.$bgcolor.'">
        <td>'.(++$offset).'</td>
       
		<td><a href = "./index.php?p=editdrugtype&drugtype_id='.$row["drugtype_id"].'&langid='.$row["langcont_id"] .'">'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</a></td>
		    
       
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
        } catch (Exception $e) {
	
        }

  
    }
    /**
     *
     * @param string $search_field
     * @param string $search_value
    * @param string $curLnag_field
     * @param string $langtb
     */
	
	function rowSearch($search_field,$search_value,$curLnag_field,$langtb){
		 try {
		  $getlang= new language();
		 $db2 = new DBConf();
		$sql = "Select * from  drug_type where  ".$search_field."='".$search_value."'" ;
		$res =$this->conn->execute($sql);
		
$pageindex='drug';
$pager = new PS_Pagination($db2,$sql,100,10,$pageindex);
$rs = $pager->paginate();
	  // echo "entered";
	     $i=1;
		while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["drugcat_id"].'" />'.$ii.'</td>
        <td>'.$row["drugtype_id"].'</td>
		<td>'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</td>
		       
        <td>'.'<a href = "./index.php?jj=delete&p=drugtype&id='.$row["drugtype_id"] .'"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td><td>'.'<a href = "./index.php?p=editdrugtype&drugtype_id='.$row["drugtype_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
		} catch (Exception $e) {
        }
	}
	

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->conn->execute("SELECT drugtype_id from drug_type order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["drugtype_id"];
				$i++;
			}
	return $keys;
	}

    /**
     * Close mysql connection
     */
	public function enddrug_type(){
		$this->connection->CloseMysql();
	}

}