<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventoryCreationFormProcessingClass
 *
 * @author upperlink
 */
class inventory_medical_name_class extends inventoryGeneralClass {

    public $conn;

    public function __construct() {
        $conn = new DBConf();
        $this->conn = $conn->mysqliConnect();
        //$this->conn=new mysqli('localhost','root','','mediluth_skye');
    }

    public function createNewMedicalName($name) {
        global $report;
        global $inventoryMedicalNameCreationSuccessNews;
        global $inventoryMedicalNameCreationFailureNews;
        global $inventory_no_medicalName_error_msg;
        if (strlen(trim($name)) < 1) {
            $report=$inventory_no_medicalName_error_msg;
            return $report;
        }
        $createMedicalName = $this->conn->query("insert into inventory_medical_names (inventory_medical_name, inventory_medical_name_date_created, user_id) values ('" . $name . "',CURRENT_TIMESTAMP, '" . $_SESSION[session_id() . "userID"] . "')");
        if (!$createMedicalName) {
            $report=$inventoryMedicalNameCreationFailureNews;
            return $report;
        } 
            $report=$inventoryMedicalNameCreationSuccessNews;

            return $report;
    }

   
    public function get_existing_items($table, $orderCollumn, $orderStyle) {
        global $items;
        global $report;
        global $none_in_existence;
        $items = $this->conn->query("select * from $table order by $orderCollumn $orderStyle");

        if (!$items) {

            return FALSE;
        }
        if ($items->num_rows < 1) {
            $report = $inventory_none_in_existence;
            return $report;
        }
        return $items;
    }

    public function listMedicalNames() {
        global $report;
        global $inventory_no_medical_name_msg;
        global $inventory_medical_name_dbconnnect_error;
        $findMedicalNameQuery = $this->conn->query("Select * from inventory_medical_names order by inventory_medical_name asc");
        if (!$findMedicalNameQuery) {
            $report = $inventory_medical_name_dbconnnect_error;
            return $report;
        }
        if($findMedicalNameQuery->num_rows<1){
            $report=$inventory_no_medical_name_msg;
            return $report;
        }
        return $findMedicalNameQuery;
        }

        public function getMedicalNameProperties($id){
            global $report;
            $medical_namePropertyQuery=  $this->conn->query("select * from inventory_medical_names where inventory_medical_name_id='".$id."'");
            if(!$medical_namePropertyQuery){
                $report=$inventory_medical_name_dbconnnect_error;
                return $report;
            }
            if($medical_namePropertyQuery->num_rows!=1){
                $report=$inventory_medical_name_notFound_msg;
                return $report;
            }
            $medical_nameProperty=$medical_namePropertyQuery->fetch_assoc();
            return $medical_nameProperty;
        }
        
        public function updateMedicalName($id,$name,$author){
            global $report;
            $updateQuery=  $this->conn->query("update inventory_medical_names set inventory_medical_name='".$name."',user_id='".$author."' where inventory_medical_name_id='".$id."'");
            if(!$updateQuery){
                return FALSE;
            }else{
                return TRUE;
            }
                }
                
        public function removeMedicalName($id){
            global $report;
            global $inventory_medical_name_delete_success_msg;
            global $inventory_medical_name_delete_error_msg;
            
        $removed=  $this->conn->query("delete from inventory_medical_names where inventory_medical_name_id='".$id."'");
        if(!$removed){
            $report=$inventory_medical_name_delete_error_msg;
            return $report;
            }else{
                $report=$inventory_medical_name_delete_success_msg;
                return $report;
            }
    }
        }
?>
