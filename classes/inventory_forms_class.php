<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventoryCreationFormProcessingClass
 *
 * @author upperlink
 */
class inventory_forms_class extends inventoryGeneralClass{
    public $conn;
    
    public function __construct() {
        $conn = new DBConf();
        $this->conn = $conn->mysqliConnect();
        //$this->conn=new mysqli('localhost','root','','mediluth_skye');
    }
    
    public function createNewForm($name, $description) {
        global $report;
        global $inventoryFormCreationSuccessNews;
        global $inventoryFormCreationFailureNews;
        global $inventory_no_formName_error_msg;
        if (strlen($name) < 1) {
            $report=$inventory_no_formName_error_msg;
            return $report;
        }
       
       $createForm = $this->conn->query("insert into inventory_forms (inventory_form_name, inventory_form_description, inventory_form_date_created, user_id) values ('".$name."','".$description."',CURRENT_TIMESTAMP, '".$_SESSION[session_id() . "userID"]."')");
       
        if (!$createForm) {
            $report=$inventoryFormCreationFailureNews;
            return $report;
            }
        return $inventoryFormCreationSuccessNews;
    }
    
     public function listForms() {
        global $report;
        global $inventory_Form_dbconnnect_error;
        $findFormQuery = $this->conn->query("Select * from inventory_forms order by inventory_form_name");
        if (!$findFormQuery) {
            $report = $inventory_form_dbconnnect_error;
            return $report;
        }
        if($findFormQuery->num_rows<1){
            $report=$inventory_no_form_msg;
            return $report;
        }
        return $findFormQuery;
        }

        
        public function updateForm($id,$name,$desc,$author){
            global $report;
            $updateQuery=  $this->conn->query("update inventory_forms set inventory_form_name='".$name."', inventory_form_description='".$desc."', user_id='".$author."' where inventory_form_id='".$id."'");
            if(!$updateQuery){
                return FALSE;
            }else{
                return TRUE;
            }
                }
                
        public function disableForm($id){
            global $report;
            global $inventory_form_delete_success_msg;
            global $inventory_form_delete_error_msg;
            
        $removed=  $this->conn->query("delete from inventory_forms where inventory_form_id='".$id."'");
        if(!$removed){
            $report=$inventory_form_delete_error_msg;
            return $report;
            }else{
                $report=$inventory_form_delete_success_msg;
                return $report;
            }
    }
        
    public function getFormProperties($id){
            global $report;
            $formPropertyQuery=  $this->conn->query("select * from inventory_forms where inventory_form_id='".$id."'");
            if(!$formPropertyQuery){
                $report=$inventory_form_dbconnnect_error;
                return $report;
            }
            if($formPropertyQuery->num_rows!=1){
                $report=$inventory_location_notFound_msg;
                return $report;
            }
            $formProperty=$formPropertyQuery->fetch_assoc();
            return $formProperty;
        }
        
  }

?>