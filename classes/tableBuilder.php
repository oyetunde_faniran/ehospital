<?php
/**
Builds a table from an SQL query and also includes pagination navigation panels at the top & bottom of the main table.
*/
class tableBuilder{
	protected $conn;
	protected $query;

	//Pass the query to be executed to build the table as a parameter to the constructor
	public function __construct($query){
		$this->conn = new DBConf();
		$this->query = $query;
	}


	function pagination($page_count, $num, $start, $PHP_SELF, $cut_off){
		//Strip start=xxx from query string
		$disq = explode("&", $_SERVER['QUERY_STRING']);		
		$queryString = "";
		foreach ($disq as $v){
			if (!stristr($v, "start=") && !empty($v))
				$queryString .= "&$v";
		}
		$q = ltrim($queryString, "&");

		//Continue with other stuffs
		$newnum = (real)$num / (real)$page_count;
		$newnum = ceil($newnum);
		if(!isset($page))
			$page = 1;
		$final = "";
		if($newnum >= 2){
			$final = "
						<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" bgcolor=\"#eeeeee\" align=\"center\" style=\"border-right:solid 1px #999999; border-bottom:solid 1px #999999;\">
						<tr>
						<td style=\"border-top:solid 1px #999999; border-left:solid 1px #999999; font-family: Segoe UI, Tahoma, Verdana; font-size: 11px;\" nowrap>
						";
			
			if(isset($start) && $start != 0){
				$final .= "&laquo;  <a href=\"$PHP_SELF?$q&start=";
				$final .= $start - $page_count;
				$final .= "\">back</a> ";
			}
			else{
				$final .= "&laquo; back ";
			}
			
			$total_pages = $newnum;
			if($newnum > $cut_off)
				$newnum = $cut_off;
			
			$cur_page = ($start + $page_count) / $page_count;
			
			if($cur_page > $cut_off)
				$page = $cur_page - $cut_off + 1;
			
			if($cur_page > $cut_off){
				$start_page = $page * $page_count - $page_count;
			}
			else{
				$start_page = 0;
			}
			
			for($i=0; $i<$newnum;$i++){
				if($start == ($page * $page_count) - $page_count){
					$final .= "<b><font size=\"2\" color=\"brown\">$page</font></b> ";
				}
				else{
					$final .= "<a href=\"$PHP_SELF?$q&start=$start_page\">$page</a> ";
				}
				$page++;
				$start_page = $start_page + $page_count;
			}
			
			if($newnum >= 2 && $cur_page < $newnum && $cur_page <= $total_pages){
				$final .= " <a href=\"$PHP_SELF?$q&start=";
				$final .= $start + $page_count;
				$final .= "\">next</a> &raquo;";
			} elseif($cur_page >= $total_pages){
					$final .= " next &raquo;";
			} else{
					$final .= " <a href=\"$PHP_SELF?$q&start=";
					$final .= $start + $page_count;
					$final .= "\">next</a> &raquo;";
			}
			
			$final .= "</td>
						</tr>
						<tr>
						<td style=\"border-top:solid 1px #999999; border-left:solid 1px #999999; font-family: Verdana; font-size: 11px;\" nowrap>
						<div align=\"center\" style=\"font-family: Segoe UI, Tahoma, Verdana; font-size: 11px; color: #666666;\">Page " . round($cur_page) . " of $total_pages<div>
						</td>
						</tr>
						</table>";
		}//if any results at top
		return $final;
	}//END method




/**
$includePagination	:	Flag that specifies if the pagination / navigation panel should be included in the final result
$start				:	The number of the next row to start display
$page_count 		:	The maximum number of records to be shown per page
$cut_off			:	The maximum number of pages to be shown in the navigation panel
*/
	public function buildTable($includePagination = true, $start=0, $page_count=100, $cut_off=20){
		//Since $start would be included in the query, to prevent SQL injection, confirm that $start is an integer by casting it to int.
		//If $start is not an integer, then it will be set to 0
		$start = (int)$start;

		//Determine the total number of rows to be shown
		$query = $this->query;
		$result = $this->conn->execute($query);
		if ($result)
			$rows4Pagination = mysql_num_rows($result);

		//Get the actual number of rows that will be shown on this page
		$query2 = $this->query . " LIMIT $start, $page_count"; //die ("<pre>$query2</pre>");
		$result = $this->conn->execute($query2);
		
		//Continue parsing the query result
		$finalTable = "";
		if ($result && mysql_num_rows($result) > 0){
			$rows = mysql_num_rows($result);

			//Include Pagination or not?
			if ($includePagination)
				$pagination = $this->pagination($page_count, $rows4Pagination, $start, $_SERVER['PHP_SELF'], $cut_off);

			$finalTable = "<table width=\"100%\" cellpadding=\"5\" cellspacing=\"5\" border=\"1\" style=\"border-collapse: collapse;\">";
			
			//Retrieve the first row & use it to form the table headers
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			$serialNo = $start + 1;
			$tableHeader = "<thead class=\"title-row\">\n
								<th>S/NO</th>\n";
			$tableBody = "<tr class=\"tr-row\">\n
							<td>$serialNo.</td>\n";
			foreach ($row as $key=>$value){
				$tableHeader .= "<th>" . strtoupper($key) . "</th>\n";
				$tableBody .= "<td>$value</td>\n";
			}
			$tableHeader .= "</thead>\n";
			$tableBody .= "</tr>\n";
			$juggleRows = true;

			//Now add every other row from the result to the table
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$rowClass = $juggleRows ? " class=\"tr-row2\"" : " class=\"tr-row\"";
				$juggleRows = !$juggleRows;
				$tableBody .= "<tr $rowClass>\n
								<td>" . (++$serialNo) . ".</td>";
				foreach ($row as $value)
					$tableBody .= "<td>$value</td>\n";
				$tableBody .= "</tr>\n";
			}
			$finalTable .= $tableHeader . $tableBody . "</table>";

			//Include Pagination or not?
			if ($includePagination)
				$finalTable = "<p>$pagination</p><p>" . $finalTable . "</p><p>$pagination</p>";

			return $finalTable;
		} else return "<p>No Result Found</p>";	//END if (number of rows) / if ($result)
	}	//END method
}	//END class

?>