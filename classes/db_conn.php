<?php
if(!defined('DBNAME')){
	require_once('../includes/database.php');
}
class db_conn{
public $dbhost;
public $dbuser;
public $dbpass;
public $dbname;
public $conn;
public $result;
public $error;

function __construct($dbname = DBNAME, $dbhost = DBHOST, $user = DBUSER, $pass = DBPASSWD){
$this->dbhost = $dbhost;
$this->dbuser = $user;
$this->dbpass = $pass;
//$this->dbname = "upltest_cmuledu_portal";
$this->dbname = $dbname;
if(isset($_SESSION['persistent'])){
	$this->dbhost = "p:".$this->dbhost;
	}
$this->conn = new mysqli($this->dbhost ,$this->dbuser ,$this->dbpass,$this->dbname);
if(mysqli_connect_errno()){
	die ("cannot connect to database".mysqli_connect_error());
	}
$this->error = $this->conn->error;
}


function query($new_query){
	$this->result = $this->conn->query($new_query);
	if(strstr($new_query,"update")){
		$this->matched = reset(explode(" ",end(explode("matched: ",$this->conn->info))));
		}
	return $this->result;
	}
	
function close(){
	$this->conn->close();
	}


}
?>