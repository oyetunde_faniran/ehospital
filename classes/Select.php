<?php
/**
 * Generates drop-down lists of a number of things
 * 
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */

/**
 * Generates drop-down lists of a number of things
 *
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */
class Select {

/**
 * Class Constructor
 */
    function  __construct() {
        $this->conn = new DBConf();
    }   //END __construct()




/**
 * Generates a drop-down list of all countries in the nationality table
 */
    function selectNationality() {
        try {
            $query = "SELECT * FROM nationality" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
                  echo '<option value='.$row['nationality_id'].'>'.$row['nationality_name'].'</option>';
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectNationality()


/**
 * Generates a drop-down list of all states in Nigeria
 */
    function selectState() {
        try {
            $query = "SELECT * FROM state" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
                  echo '<option value='.$row['state_id'].'>'.$row['state_name'].'</option>';
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectState()



/**
 * Generates a drop-down list of all registered retainers of the hospital
 */
    function selectRetainer() {
        try {
            $query = "SELECT * FROM retainership" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
                  echo '<option value='.$row['retainership_id'].'>'.$row['retainership_company'].'</option>';
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectRetainer()

}   //END class


?>
