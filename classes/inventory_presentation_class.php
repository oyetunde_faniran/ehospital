<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventoryCreationFormProcessingClass
 *
 * @author upperlink
 */
class inventory_presentation_class extends inventoryGeneralClass {
    public $conn;
    
    public function __construct() {
        $conn = new DBConf();
        $this->conn = $conn->mysqliConnect();
        //$this->conn=new mysqli('localhost','root','','mediluth_skye');
    }
    
    public function createNewPresentation($name, $description) {
        global $report;
        global $inventoryPresentationCreationSuccessNews;
        global $inventoryPresentationCreationFailureNews;
        global $inventory_no_presentationName_error_msg;
        if (strlen($name) < 1) {
            $report=$inventory_no_presentationName_error_msg;
            return $report;
        }
       
       $createPresentation = $this->conn->query("insert into inventory_presentations (inventory_presentation_name, inventory_presentation_description, inventory_presentation_date_created, user_id) values ('".$name."','".$description."',CURRENT_TIMESTAMP, '".$_SESSION[session_id() . "userID"]."')");
       
        if (!$createPresentation) {
            $report=$inventoryPresentationCreationFailureNews;
            return $report;
            }
        return $inventoryPresentationCreationSuccessNews;
    }
    
     public function listPresentations() {
        global $report;
        global $inventory_no_presentation_msg;
        global $inventory_presentation_dbconnnect_error;
        $findPresentationQuery = $this->conn->query("Select * from inventory_presentations order by inventory_presentation_name");
        if (!$findPresentationQuery) {
            $report = $inventory_presentation_dbconnnect_error;
            return $report;
        }
        if($findPresentationQuery->num_rows<1){
            $report=$inventory_no_presentation_msg;
            return $report;
        }
        return $findPresentationQuery;
        }

        
        public function updatePresentation($id,$name,$desc){
            global $report;
            $updateQuery=  $this->conn->query("update inventory_presentations set inventory_presentation_name='".$name."', inventory_presentation_description='".$desc."', user_id='".$_SESSION[session_id() . "userID"]."' where inventory_presentation_id='".$id."'");
            if(!$updateQuery){
                return FALSE;
            }else{
                return TRUE;
            }
                }
                
        public function removePresentation($id){
            global $report;
            global $inventory_presentation_delete_success_msg;
            global $inventory_presentation_delete_error_msg;
            
        $removed=  $this->conn->query("delete from inventory_presentations where inventory_presentation_id='".$id."'");
        if(!$removed){
            $report=$inventory_presentation_delete_error_msg;
            return $report;
            }else{
                $report=$inventory_presentation_delete_success_msg;
                return $report;
            }
    }
        
    public function getPresentationProperties($id){
            global $report;
            $presentationPropertyQuery=  $this->conn->query("select * from inventory_presentations where inventory_presentation_id='".$id."'");
            if(!$presentationPropertyQuery){
                $report=$inventory_presenatation_dbconnnect_error;
                return $report;
            }
            if($presentationPropertyQuery->num_rows!=1){
                $report=$inventory_presentation_notFound_msg;
                return $report;
            }
            $presentationProperty=$presentationPropertyQuery->fetch_assoc();
            return $presentationProperty;
        }
        
  }

?>