<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventoryCreationFormProcessingClass
 *
 * @author upperlink
 */
class inventory_manufacturer_class extends inventoryGeneralClass {
    public $conn;
    
    public function __construct() {
        $conn = new DBConf();
        $this->conn = $conn->mysqliConnect();
        //$this->conn=new mysqli('localhost','root','','mediluth_skye');
    }
    
    public function addNewManufacturer($name,$address,$rcno){
        global $report;
        $report = "";
        global $inventoryManufacturerSuccessfullyAdded;
        global $inventorySystemFailureAddingManufacturer;
        global $inventory_please_indicate_manufacturers_name;
        if (strlen($name) < 1) {
            $report=$inventory_please_indicate_manufacturers_name;
            return $report;
        }
        
        $createmanufacturer = $this->conn->query("insert into inventory_manufacturers (inventory_manufacturer_name, inventory_manufacturer_address, inventory_manufacturer_rcno, inventory_manufacturer_date_created, user_id) values ('".$name."','".$address."','" .$rcno. "',CURRENT_TIMESTAMP,'" .$_SESSION[session_id()."userID"]."')");
        if (!$createmanufacturer) {
            $report=$inventorySystemFailureAddingManufacturer;
            return $report;
            } 
            $report=$inventoryManufacturerSuccessfullyAdded;
            return $report;
    }
    
    
        public function get_existing_items($table,$orderCollumn,$orderStyle){
             global $items;
            global $report;
            $items=$this->conn->query("select * from $table order by $orderCollumn $orderStyle");
          
            if(!$items){
                 
                return FALSE;
                }
                if($items->num_rows<1){$report=$none_in_existence;
                    return $report;
                }
                return $items;
        }
        
        
        public function listManufacturers(){
            global $report;
            global $inventory_manufacturer_list_failure_news;
            global $inventory_no_manufacturer_found;
            
            $listed=$this->conn->query("select * from inventory_manufacturers order by inventory_manufacturer_name asc");
            if(!$listed){
                $report=$inventory_manufacturer_list_failure_news;
                return $report;
            }
            if($listed->num_rows<1){
                $report=$inventory_manufacturer_list_failure_news;
                return $report;
            }
                  return $listed;
        }
        
         public function getmanufacturerDetails($id){
           global $report;
           global $inventory_manufacturer_info_failure_msg;
           global $inventory_manufacturer_notFound_msg;
           $details=  $this->conn->query("select * from inventory_manufacturers where inventory_manufacturer_id='".$id."'");
           if(!$details){
               $report=$inventory_manufacturer_info_failure_msg;
               return $report;
           }
           if($details->num_rows!=1){
               $report=$inventory_manufacturer_notFound_msg;
               return $report;
           }
           $manufacturer=$details->fetch_assoc();
       return $manufacturer;
       }
       
        public function updateManufacturer($name,$address,$rcno){
       global $report;
       global $inventorySystemFailureUpdateManufacturer;
       global $inventory_manufacturer_updated_successfully;
       global $inventory_no_manufacturerName_error_msg;


       if(strlen($name)<1){
           $report=$inventory_no_manufacturerName_error_msg;
           return $report;
       }
       
       $updateQuery=$this->conn->query("update inventory_manufacturers set inventory_manufacturer_name='".$name."',inventory_manufacturer_address='".$address."', inventory_manufacturer_rcno='".$rcno."', inventory_manufacturer_author='".$_SESSION[session_id()."userID"]."',inventory_manufacturer_date_created=CURRENT_TIMESTAMP"); 
       
       if(!$updateQuery){
           $report=$inventorySystemFailureUpdateManufacturer;
           return $report;
       }
       $report=$inventory_manufacturer_updated_successfully;
       return $report;
   }



   public function getManufacturer($id){
       $conn = new DBConf();
       $id = (int)$id;
       $query = "SELECT * FROM inventory_manufacturers
                    WHERE inventory_manufacturer_id = '$id'";
       $result = $conn->run($query);
       if ($conn->hasRows($result)){
           $retVal = mysql_fetch_assoc($result);
       } else $retVal = "";
       return $retVal;
   }
   
       
}

?>
