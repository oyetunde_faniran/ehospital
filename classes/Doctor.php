<?php
/**
 * Contains diverse methods that can be used for operations to be carried out on doctors
 * 
 * @author Faniran, Oyetunde
 *
 * @package Patient_Care
 */

class Doctor {
    /**
     * @var object An object of the DBConf class
     */
	protected $conn;

    /**
     * @var int Stores the clinic ID of the clinic under consideration
     */
	protected $staffID;

    /**
     * @var string Stores the name of the column in the language table that corresponds to the currently-selected language.
     */
	protected $curLangField;
	



/**
 * Class Constructor
 * @param string The name of the column in the language table that corresponds to the currently-selected language.
 * @param int The clinic ID of the clinic under consideration
 */
	function __construct ($curLangField = "lang1", $staffID = ""){
		$this->conn = new DBConf();
		$this->staffID = $staffID;
		$this->curLangField = $curLangField;
	}   //END __construct()




/**
 * Gets the consultant ID of the consultant to which the doctor with staff ID $staffID or $this->staffID (if $staffID is empty) is attached
 * @param $staffID string 	The staff ID of the doctor for which the consultant (s)he is attached to would be fetched
 * @return int 				The consultant ID of the consultant to which the doctor under consideration is attached. If none, then 0 is returned.
 */
	public function getConsultantIDAttachedTo($staffID = ""){
		$staffID = !empty($staffID) ? $staffID : $this->staffID;
		$query = "SELECT consultant_id 'id' FROM consultant_doctors
					WHERE staff_employee_id = '$staffID' AND CURDATE() <= consultantdoc_appdate";
		$result = $this->conn->run($query);
		if ($this->conn->hasRows($result)){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			$retVal = $row["id"];
		} else $retVal = 0;
		return $retVal;
	}



/**
 * Gets the employee ID of the consultant to which the doctor with staff ID $staffID or $this->staffID (if $staffID is empty) is attached
 * @param $staffID string 	The staff ID of the doctor for which the consultant (s)he is attached to would be fetched
 * @return int 				The employee ID of the consultant to which the doctor under consideration is attached. If none, then 0 is returned.
 */
	public function getConsultantEmpIDAttachedTo($staffID = ""){
		$staffID = !empty($staffID) ? $staffID : $this->staffID;
		$query = "SELECT c.staff_employee_id 'empID'
					FROM consultant c INNER JOIN consultant_doctors cd
					ON c.consultant_id = cd.consultant_id
					WHERE cd.staff_employee_id = '$staffID' AND CURDATE() <= consultantdoc_appdate";
		$result = $this->conn->run($query);
		if ($this->conn->hasRows($result)){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			$retVal = $row["empID"];
		} else $retVal = 0;
		return $retVal;
	}   //END getConsultantEmpIDAttachedTo()
    
    
    
    
    
    public function getNameOfDoctorForAppointment($appID = 0, $patAdmID = 0, $deptID = 0, $clinicID = 0){
        $appID = (int)$appID;
        $patAdmID = (int)$patAdmID;
        $deptID = (int)$deptID;
        $clinicID = (int)$clinicID;
        if (!empty($appID)){
            $where = " app.app_id = '$appID' ";
        } else {
            $where = " app.patadm_id = '$patAdmID' AND app.dept_id = '$deptID' AND app.clinic_id = '$clinicID' ";
        }
        $query = "SELECT CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) 'doctor'
                    FROM staff s
                        INNER JOIN consultant c
                        INNER JOIN appointment app
                    ON s.staff_employee_id = c.staff_employee_id
                        AND c.consultant_id = app.consultant_id
                    WHERE $where";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $row = mysql_fetch_assoc($result);
            $retVal = $row["doctor"];
        } else $retVal = "";
        return $retVal;
    }   //END getNameOfDoctorForAppointment()
    
    


	
}	//END class
?>