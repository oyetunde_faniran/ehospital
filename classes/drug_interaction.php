<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 26-08-2009
 * 
 * Version of MYSQL_to_PHP: 1.0
 * 
 * License: LGPL 
 * 
 */
require_once './classes/DBConf.php';

Class drug_interaction {
    public $drugint=array() ;
    public $drugint2=array() ;
	/*public $drugint_id; //int(10)
	public $drug_id; //int(10)
	public $drugint_sideffect; //varchar(255)
	public $drugint_contraindication; //varchar(255)
	public $drugint_indication; //varchar(255)
	public $user_id; //int(10)
	public $connection;*/

 function  __construct() {
        $this->conn = new DBConf();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
	public function New_drug_interaction($drug_id,$drugint_sideffect,$drugint_contraindication,$drugint_indication,$user_id){
		$this->drug_id = $drug_id;
		$this->drugint_sideffect = $drugint_sideffect;
		$this->drugint_contraindication = $drugint_contraindication;
		$this->drugint_indication = $drugint_indication;
		$this->user_id = $user_id;
	}

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param key_table_type $key_row
     * 
     */
	public function Load_from_key($key_row){
		$result = $this->connection->RunQuery("Select * from drug_interaction where drugint_id = \"$key_row\" ");
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$this->drugint_id = $row["drugint_id"];
			$this->drug_id = $row["drug_id"];
			$this->drugint_sideffect = $row["drugint_sideffect"];
			$this->drugint_contraindication = $row["drugint_contraindication"];
			$this->drugint_indication = $row["drugint_indication"];
			$this->user_id = $row["user_id"];
		}
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param key_table_type $key_row
     *
     */
	public function Delete_row_from_key($key_row){
		$this->connection->RunQuery("DELETE FROM drug_interaction WHERE drugint_id = $key_row");
	}

    /**
     * Update the active row table on table
     */
public function Save_Active_Row($id,$tablename){
		/*$this->conn->execute("UPDATE drug_interaction  set drugcat_name = \"$this->drugcat_name\" where drugint_id = \"$this->drugint_id\"");*/
		
		try {
	
	$sql = "UPDATE $tablename SET ";	
	
				$qq=count($this->drugint);
				$q=count($this->drugint);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
			
         			    $sql.=    $this->drugint[$i] .'="'.$this->drugint2[$i].'" ';
			 
					 }
					 else{
						  $sql.=      $this->drugint[$i] .'="'.$this->drugint2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			$result2 =$this->conn->execute("SELECT * from $tablename");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
	
         $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		
		
		
		
	}

    /**
     * Save the active var class as a new row on table
     */
public function Save_Active_Row_as_New(){
		/*$this->conn->execute("Insert into drug_interaction  (drugcat_name) values (\"$this->drugcat_name\"");*/
		try {
		$sql = "INSERT INTO drug_interaction  SET ";	
	
				$qq=count($this->drugint);
				$q=count($this->drugint);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
			
         			    $sql.=    $this->drugint[$i] .'="'.$this->drugint2[$i].'" ';
			 
					 }
					 else{
						  $sql.=      $this->drugint[$i] .'="'.$this->drugint2[$i].'" ,';
			
					 }
  				 $q--;
 			}
		
		 $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		
		
		
		
	}
public function GetSearchFields(){
	echo 	"<option value='-1'>Select Field</option>";	
		$result =$this->conn->execute("SELECT * from drug_interaction  order by drugint_id");
				$i = 0;
				
while ($i < mysql_num_fields($result)) {
 //   echo "Information for column $i:<br />\n";
    $meta = mysql_fetch_field($result);
	
    if (!$meta) {
      //  echo "No information available<br />\n";
    }
  //  echo "$meta->name";

		
	echo 	"<option value=". $meta->name.">".$meta->name."</option>";
	    $i++;
}
/*mysql_free_result($result);
	return $keys;*/
	}
	
function allrows($patadm_id) {
        
		try {
	$db2 = new DBConf();
/*		 $sql = 'SELECT
*
FROM
drug_interaction d,treatment_prescription tp,treatment t WHERE 
d.drug_id=tp.drug_id AND
 tp.treatment_id=t.treatment_id AND   
t.patadm_id='.$patadm_id
;*/
 $sql = 'SELECT 
 *
FROM
treatment_prescription tp,treatment t WHERE 
 tp.treatment_id=t.treatment_id '
;

//echo $sql ;
$pagelink32='drugint';
$pager = new PS_Pagination($db2,$sql,200,10,$pagelink32);
$hospital= new patient_admission();
$drug= new drug();
$rs = $pager->paginate();
           

           // $res = $this->conn->execute($sql);
			$i=1;
          while ($row = mysql_fetch_array($rs)) {
             
			 $drug_name=$drug->getdrug_name($row["drug_id"])  ;
			// $hospital=$hospital->gethospital_name($row["patadm_id"])  ;
			 
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["drugint_id"].'" /></td>
        <td>'.$row["drugint_id"].'</td>
		<td>'.$drug_name.'</td>
		 
		    <td>'.$row['drugint_sideffect'].'</td>   
			 <td>'.$row['drugint_contraindication'].'</td>  
        <td>'.'<a href = "./index.php?jj=delete&p=drugint&id='.$row["drugint_id"] .'"><img src="./images/btn_delete_02.gif"  border=0/></a></td><td>'.'<a href = "./index.php?p=editdrugint&drugint_id='.$row["drugint_id"] .'"><img src="./images/btn_edit.gif"  border=0/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
        } catch (Exception $e) {
	
        }

  
    }
    
function rowSearch($search_field,$search_value){
		 try {
		 $db2 = new DBConf();
		$sql = "Select * from  drug_interaction   where  ".$search_field."='".$search_value."'" ;
		$res =$this->conn->execute($sql);
		
		$pagelink32='drugint';
$pager = new PS_Pagination($db2,$sql,200,10,$pagelink32);
 $rs = $pager->paginate();
	  // echo "entered";
	     $i=1;
		while ($row = mysql_fetch_array($rs)) {
             
			 $drug_name=$this->getdrug_name($row["drug_id"])  ;
			 $hospital_no=$this->gethospital_name($row["patadm_id"])  ;
			 
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["drugint_id"].'" /></td>
        <td>'.$row["drugint_id"].'</td>
		<td>'.$drug_name.'</td>
		  <td>'.$hospital_no.'</td>     
		    <td>'.$drugint_sideffect.'</td>   
			 <td>'.$drugint_contraindication.'</td>  
        <td>'.'<a href = "./index.php?jj=delete&p=drugint&id='.$row["drugint_id"] .'"><img src="./images/btn_delete_02.gif"  border=0/></a></td><td>'.'<a href = "./index.php?p=editdrugint&drugint_id='.$row["drugint_id"] .'"><img src="./images/btn_edit.gif"  border=0/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<? 
		} catch (Exception $e) {
        }
	}
    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->connection->RunQuery("SELECT drugint_id from drug_interaction order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["drugint_id"];
				$i++;
			}
	return $keys;
	}

	
	
	

    /**
     * Close mysql connection
     */
	public function enddrug_interaction(){
		$this->connection->CloseMysql();
	}

}



// ------------------------------------------------------------------------

class PS_Pagination {
	var $php_self;
	var $rows_per_page; //Number of records to display per page
	var $total_rows; //Total number of rows returned by the query
	var $links_per_page; //Number of links to display per page
	var $sql;
	var $debug = false;
	var $conn;
	var $page;
	var $max_pages;
	var $offset;
	
	/**
	 * Constructor
	 *
	 * @param resource $connection Mysql connection link
	 * @param string $sql SQL query to paginate. Example : SELECT * FROM users
	 * @param integer $rows_per_page Number of records to display per page. Defaults to 10
	 * @param integer $links_per_page Number of links to display per page. Defaults to 5
	 */
	 
	function PS_Pagination($connection, $sql, $rows_per_page = 10, $links_per_page = 10) {
		$this->conn = $connection;
		$this->sql = $sql;
		$this->rows_per_page = $rows_per_page;
		$this->links_per_page = $links_per_page;
		$this->php_self = htmlspecialchars($_SERVER['PHP_SELF']);
		if(isset($_GET['page'])) {
			$this->page = intval($_GET['page']);
		}
	}
	
	/**
	 * Executes the SQL query and initializes internal variables
	 *
	 * @access public
	 * @return resource
	 */
	function paginate() {
		if(!$this->conn) {
			if($this->debug) echo "MySQL connection missing<br />";
			return false;
		}
		
		$all_rs = @mysql_query($this->sql);
		if(!$all_rs) {
			if($this->debug) echo "SQL query failed. Check your query.<br />";
			return false;
		}
		$this->total_rows = mysql_num_rows($all_rs);
		//mysql_close($all_rs);
		
		$this->max_pages = ceil($this->total_rows/$this->rows_per_page);
		//Check the page value just in case someone is trying to input an aribitrary value
		if($this->page > $this->max_pages || $this->page <= 0) {
			$this->page = 1;
		}
		
		//Calculate Offset
		$this->offset = $this->rows_per_page * ($this->page-1);
		
		//Fetch the required result set
		$rs = @mysql_query($this->sql." LIMIT {$this->offset}, {$this->rows_per_page}");
		if(!$rs) {
			if($this->debug) echo "Pagination query failed. Check your query.<br />";
			return false;
		}
		return $rs;
	}
	
	/**
	 * Display the link to the first page
	 *
	 * @access public
	 * @param string $tag Text string to be displayed as the link. Defaults to 'First'
	 * @return string
	 */
	function renderFirst($tag='First') {
		if($this->page == 1) {
			return $tag;
		}
		else {
			return '<a href="'.$this->php_self.'?page=1&p=drug">'.$tag.'</a>';
		}
	}
	
	/**
	 * Display the link to the last page
	 *
	 * @access public
	 * @param string $tag Text string to be displayed as the link. Defaults to 'Last'
	 * @return string
	 */
	function renderLast($tag='Last') {
		if($this->page == $this->max_pages) {
			return $tag;
		}
		else {
			return '<a href="'.$this->php_self.'?page='.$this->max_pages.'&p=drug">'.$tag.'</a>';
		}
	}
	
	/**
	 * Display the next link
	 *
	 * @access public
	 * @param string $tag Text string to be displayed as the link. Defaults to '>>'
	 * @return string
	 */
	function renderNext($tag=' &gt;&gt;') {
		if($this->page < $this->max_pages) {
			return '<a href="'.$this->php_self.'?page='.($this->page+1).'&p=drug">'.$tag.'</a>';
		}
		else {
			return $tag;
		}
	}
	
	/**
	 * Display the previous link
	 *
	 * @access public
	 * @param string $tag Text string to be displayed as the link. Defaults to '<<'
	 * @return string
	 */
	function renderPrev($tag='&lt;&lt;') {
		if($this->page > 1) {
			return '<a href="'.$this->php_self.'?page='.($this->page-1).'&p=drug">'.$tag.'</a>';
		}
		else {
			return $tag;
		}
	}
	
	/**
	 * Display the page links
	 *
	 * @access public
	 * @return string
	 */
	function renderNav() {
		for($i=1;$i<=$this->max_pages;$i+=$this->links_per_page) {
			if($this->page >= $i) {
				$start = $i;
			}
		}
		
		if($this->max_pages > $this->links_per_page) {
			$end = $start+$this->links_per_page;
			if($end > $this->max_pages) $end = $this->max_pages+1;
		}
		else {
			$end = $this->max_pages;
		}

			
		$links = '';
		
	 for( $i=$start ; $i<$end ; $i++) {
			if($i == $this->page) {
				$links .= " $i ";
			}
			else {
				$links .= ' <a href="'.$this->php_self.'?page='.$i.'&p=drug">'.$i.'</a> ';
			}
		}
		
		return $links;
	}
	
	/**
	 * Display full pagination navigation
	 *
	 * @access public
	 * @return string
	 */
	function renderFullNav() {
		return $this->renderFirst().'&nbsp;'.$this->renderPrev().'&nbsp;'.$this->renderNav().'&nbsp;'.$this->renderNext().'&nbsp;'.$this->renderLast();	
	}
	
	/**
	 * Set debug mode
	 *
	 * @access public
	 * @param bool $debug Set to TRUE to enable debug messages
	 * @return void
	 */
	function setDebug($debug) {
		$this->debug = $debug;
	}
}