<?php
class Payment{
    /**
     *
     * @var int The ID of the user handling the payment
     */
    protected $userID;

    /**
     *
     * @var float   The amount of money to be paid
     */
    protected $amount;

    /**
     *
     * @var Connection  An object of the Connection class.
     */
    protected $conn;

    public $error;

    public $errorMsg;


	function __construct($userID, $amount = 0){
        $this->conn = new DBConf();
        $this->conn->payConnect();  //Connect to the payment records database
        $this->userID = (int)$userID;
        $this->amount = $amount;    //doEscape on data before assignment
        $this->error = false;
        $this->errorMsg = "";
	}
	

    /**
     * Gets the total amount a user has collected for the current day.
     * @return string   The cummulative amount a user has collected today
     */
	public function getTotalDayPayment(){
        $query = "SELECT SUM(trans_total_amount) AS 'amount' FROM transaction
                    WHERE trans_status = '1'
                            AND trans_staffid = '" . $this->userID . "'
                            AND DATE(trans_datetime) = CURDATE()";
        //die ($query);
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result, 1)){
            $row = mysql_fetch_array($result, MYSQL_ASSOC);
            $retVal = number_format($row["amount"], 2);
            //die ($retVal . "---");
        } else $retVal = "0.00";
        return $retVal;
	}	//END getTotalDayPayment()
	



    /**
     * Sets the status of the transaction to paid and also records the transaction on BranchCollect Mini.
     * @param Array $paramArray     An array that contains the details of the transaction and that must have the following elements
     *                              - amount
     *                              - transID
     *                              - custNo
     *                              - othernames
     *                              - lastname
     *                              - staffName
     * @return  Returns the error status at the end of the operation.i.e. true if the operation failed, else false.
     */
    public function processPayment($paramArray){
        //$this->conn->payConnect();  //Using Branch Collect Mini's DB
        try {
            //die ("About to insert");
            //Escape and turn the array keys into scalar variables
            $paramArray = admin_Tools::doEscape($paramArray, $this->conn);
            foreach ($paramArray as $paramKey => $paramValue){
                $$paramKey = $paramValue;
            }

            //Make sure there's a user ID and staff name attached to this transaction
            if (empty($this->userID) || empty($staffName)){
                $this->errorMsg = "Please, supply all the required information.";
                throw new Exception();
            }

            

            $query = "INSERT INTO transaction
                        VALUES ('0', '$transID', '$custNo', '$othernames', '$lastname', NOW(), '$amount', '1', '" . $this->userID . "', '$staffName')";
            //return $query;
            $result = $this->conn->run($query);
            if (!$this->conn->hasRows($result, 1)){
                $this->errorMsg = "Transaction Failed!";
                throw new Exception();
            }
        } catch (Exception $e) {
            $this->error = true;
            //die ($_SESSION[session_id() . "userID"] . "--<pre>" . print_r ($e, true) . "</pre>");
        }   //END try..catch

        return $this->error;

    }   //END processPayment()


	
}	//END class
?>