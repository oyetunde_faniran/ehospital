<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventoryCreationFormProcessingClass
 *
 * @author upperlink
 */
class inventory_strengths_class  extends inventoryGeneralClass{
    public $conn;
    
    public function __construct() {
        $conn = new DBConf();
        $this->conn = $conn->mysqliConnect();
        //$this->conn=new mysqli('localhost','root','','mediluth_skye');
    }
    
    public function createNewLocation($name, $description) {
        global $report;
        $report = "";
        global $inventoryLocationCreationSuccessNews;
        global $inventoryLocationCreationFailureNews;
        global $inventory_no_locationName_error_msg;
        if (strlen($name) < 1) {
            $report.=$inventory_no_locationName_error_msg;
        }
        if($report!=""){
            return $report;
            }
        $createLocation = $this->conn->query("insert into inventory_location (inventory_location_name, inventory_location_description, inventory_location_date_created, user_id) values ('".$name."','".$description."',CURRENT_TIMESTAMP, '".$_SESSION[session_id() . "userID"]."')");
        if (!$createLocation) {
            $report.=$inventoryLocationCreationFailureNews;
        } else {
            $report.=$inventoryLocationCreationSuccessNews;
        }
        return $report;
    }
    
    public function addNewCategory($name,$desc,$parent){
        global $report;
        $report = "";
        global $inventoryCategoryCreationSuccessNews;
        global $inventoryCategoryCreationFailureNews;
        global $inventory_no_categoryName_error_msg;
        if (strlen($name) < 1) {
            $report.=$inventory_no_categoryName_error_msg;
        }
        if ($report != "") {
            return $report;
        }
        $createCategory = $this->conn->query("insert into inventory_categories (inventory_category_name, inventory_category_parent, inventory_category_description, inventory_category_date_created, user_id) values ('".$name."','".$parent."','" .$desc. "',CURRENT_TIMESTAMP, '" .$_SESSION[session_id()."userID"]."')");
        if (!$createCategory) {
            $report.=$inventoryCategoryCreationFailureNews;
        } else {
            $report.=$inventoryCategoryCreationSuccessNews;
        }
        return $report;
    }
    
    public function addNewManufacturer($name,$address,$rcno){
        global $report;
        $report = "";
        global $inventoryManufacturerSuccessfullyAdded;
        global $inventorySystemFailureAddingManufacturer;
        global $inventory_please_indicate_manufacturers_name;
        if (strlen($name) < 1) {
            $report.=$inventory_please_indicate_manufacturers_name;
        }
        if ($report != "") {
            return $report;
        }
        $createmanufacturer = $this->conn->query("insert into inventory_manufacturers (inventory_manufacturer_name, inventory_manufacturer_address, inventory_manufacturer_rcno, inventory_manufacturer_date_created, user_id) values ('".$name."','".$address."','" .$rcno. "',CURRENT_TIMESTAMP,'" .$_SESSION[session_id()."userID"]."')");
        if (!$createmanufacturer) {
            $report.=$inventorySystemFailureAddingManufacturer;
        } else {
            $report.=$inventoryManufacturerSuccessfullyAdded;
        }
        return $report;
    }
    
    
        public function get_existing_items($table,$orderCollumn,$orderStyle){
             global $items;
            global $report;
            $items=$this->conn->query("select * from $table order by $orderCollumn $orderStyle");
          
            if(!$items){
                 
                return FALSE;
                }
                if($items->num_rows<1){$report=$none_in_existence;
                    return $report;
                }
                return $items;
        }
        
    public function createNewSection() {
        $result = "New section successfully created";
        return $result;
    }
    
    
    public function createRejectionReason(){
    $result="New rejection reason successfully created";
    return $result;
    }
    
    public function createManufacturer($uche){
        $result="New manufacturer successfully created";
        return $result;
    }

    
    public function createNewVendor(){
    $result="New vendor successfully created";
    return $result;
    }

    
    public function createNewItem(){
    $result="New item successfully created";
    return $result;
    }
    
    
    public function createNewForm(){
    $result="New form successfully created";
    return $result;
    }
    
    
    public function createNewTransfer(){
    $result="New transfer successfully documented";
    return $result;
    }
    
    
    public function createNewStrengthForm(){
    $result="New item strngth successfully created";
    return $result;
    }
    
    
    public function createPurchaseOrder(){
    $result="New purchase order successfully created";
    return $result;
    }
    
    
    public function createNewPresentation(){
    $result="New presentation successfully created";
    return $result;
    }

}

?>
