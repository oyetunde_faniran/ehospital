<?php
/**
 * Handles the sending / logging of SMS'
 * 
 * @author Faniran, Oyetunde
 *
 * @package System_Administration
 */

/**
 * Handles the sending / logging of SMS'
 *
 * @author Faniran, Oyetunde
 *
 * @package Messaging
 */
class SMSer{
    /**
     * @var int The user ID of the currently logged-in user
     */
	protected $userID;

    /**
     * @var object An object of the DBConf class
     */
	protected $conn;

    /**
     * @var boolean =true if an error occurred during the last operation in the class else =false
     */
	public $error;

    /**
     * @var string  Would containing the error message of any error that occurs during the course of performing an operation in the class.
     */
	public $errorMsg;

    /**
     *
     * @var string  The username of the account to be used for sending the SMS on http://www.mysmslink.com
     */
	private $username;

    /**
     *
     * @var string  The password of the account to be used for sending the SMS on http://www.mysmslink.com
     */
	private $password;


/**
 * Class constructor
 * 
 * @param int $userID   The user ID of the currently logged-in user
 */
	function __construct($userID){
		$this->conn = new DBConf();
		$this->userID = $userID;
		$this->username = "cacdemo";
		$this->password = "CAC#Demo1";
	}	//END __construct()





/*
	protected function logSMSSend($recipient, $sender, $msg, $event, $sendResponse){
		$recipient = $this->conn->doEscape($recipient);
		$sender = $this->conn->doEscape($sender);
		$msg = $this->conn->doEscape($msg);
		$event = $this->conn->doEscape($event);
		$sendResponse = $this->conn->doEscape($sendResponse);
		$query = "INSERT INTO sms_logger
					VALUES ('0', '" . $this->userID . "', '$msg', NOW(), '1', '$sendResponse')";
		$this->conn->runQuery ($query);
	}
  */




/**
 * Sends the specified SMS to the specified recipient
 * @param string $recipient     The phone number of the recipient of the message in international format without a "+"
 * @param string $sender        The sender name that would appear to the recipient
 * @param string $msg           The actual SMS to send to the recipient
 * @return string               The raw response from the SMS server
 */
	public function sendSMS ($recipient, $sender, $msg) {
	/* Variables together with the values to be transferred. */
		$un = $this->username; /* customer username for gaining access to MySMSLink */
		$up = $this->password; /* customer password */
		$msg = stripslashes($msg); /* SMS text */

		/* production of the required URL */
		$url = "http://www.mysmslink.com/sms_send_out.php?"
				. "un=$un"
				. "&up=" . urlencode($up)
				. "&recipient=" . urlencode($recipient)
				. "&sender=" . urlencode($sender)
				. "&msg=" . urlencode($msg);
			/* invocation of URL*/
			//die ($url);
		if (($f = @fopen($url, "r")))
		{
			$sendResponse = fgets($f, 255);
		} else $sendResponse = 0;
		
		//Log this instance of message sending
		//$this->logSMSSend($recipient, $sender, $msg, $event, $sendResponse);
//		die ($sendResponse);
		return $sendResponse;
	}	//END sendSMS()



}	//END class SMSer
?>