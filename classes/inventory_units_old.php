<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventory_units
 *
 * @author Faniran, Oyetunde
 */
class inventory_units {

    /**
     * @var DBConf object   An object of the DBConf class, which handles communication with the DB
     */
    protected $conn;
    /**
     * @var int The ID of the inventory_units under consideration
     */
    protected $unitID;
    /**
     *
     * @var boolean = true whenever an error occurs during any operation
     */
    public $error;
    /**
     *
     * @var string  Contains the error message of the error that occurred during the last operation if any
     */
    public $errorMsg;

    /**
     * Class Constructor
     * @param int $inventory_unitsID        The ID of the inventory_units under consideration
     */
    function __construct($unitID = 0) {
        $this->conn = new DBConf();
        $this->unitID = (int) $unitID;
        $this->error = false;
        $this->errorMsg = "";
    }//END __construct

    
    /**
     * Creates a new unit to be used for one / more inventory items
     * @param string $unitName          The name of the unit to be created
     * @param string $unitDescription   The description of the unit to be created
     * @param int $enabled              The enabled state of the unit to be created (0 = Disabled, 1 = Enabled)
     * @param int $userID               The user ID of the user that is trying to create the unit
     * @return boolean                  The error status at the end of the operation. (Returns true if an error occurred, else false) 
     */
    public function createUnit($unitName, $unitDescription, $enabled, $userID){
        $this->error = false;
        $this->errorMsg = "";
        $unitName = admin_Tools::doEscape($unitName, $this->conn);
        $unitDescription = admin_Tools::doEscape($unitDescription, $this->conn);
        $userID = (int)$userID;
        $enabled = $enabled == 1 ? 1 : 0;
        try {
            if (empty($unitName)){
                $this->errorMsg = "Please, enter a valid name for the unit to be created.";
                throw new Exception();
            }
            
            $query = "INSERT INTO inventory_units
                        SET inventory_unit_name = '$unitName',
                            inventory_unit_desc = '$unitDescription',
                            inventory_unit_dateadded = NOW(),
                            inventory_unit_enabled = '$enabled',
                            user_id = '$userID'";
            $result = $this->conn->run($query);
            if (!$this->conn->hasRows($result)){
                $this->errorMsg = "Unable to create a new unit. Please, try again.";
                throw new Exception();
            }
        } catch (Exception $e) {
            $this->error = true;
        }
        return $this->error;
    }   //END createUnit()
    
    
    
    
    /**
     * Updates the information about a unit to be used for one / more inventory items
     * @param string $unitID            The ID of the unit under consideration
     * @param string $unitName          The new name of the unit
     * @param string $unitDescription   The new description of the unit
     * @param boolean $enabled          = true if the unit should be enabled, else false
     * @param int $userID               The user ID of the user that is updating it
     * @return boolean                  The error status at the end of the operation. (Returns true if an error occurred, else false) 
     */
    public function updateUnit($unitID, $unitName, $unitDescription, $enabled, $userID){
        $this->error = false;
        $this->errorMsg = "";
        $unitName = admin_Tools::doEscape($unitName, $this->conn);
        $unitDescription = admin_Tools::doEscape($unitDescription, $this->conn);
        $enabled = $enabled ? true : false;
        $userID = (int)$userID;
        $unitID = (int)$unitID;
        try {
            if (empty($unitName)){
                $this->errorMsg = "Please, enter a valid name for the unit to be created.";
                throw new Exception();
            }
            
            $query = "UPDATE inventory_units
                        SET inventory_unit_name = '$unitName',
                            inventory_unit_desc = '$unitDescription',
                            inventory_unit_enabled = '" . ($enabled ? "1" : "0") . "',
                            inventory_unit_updating_user_id = '$userID',
                            inventory_unit_datelastupdated = NOW()
                        WHERE inventory_unit_id = '$unitID'";
            //die ("<pre>$query</pre>");
            $result = $this->conn->run($query);
            if (!$this->conn->hasRows($result)){
                $this->errorMsg = "You did not make any change. So, nothing was saved.";
                throw new Exception();
            }
        } catch (Exception $e) {
            $this->error = true;
        }
        return $this->error;
    }   //END updateUnit()
    
    
    

    /**
     * Gets all item units or only enabled ones depending on the value of $enabledOnly
     * @param boolean $enabledOnly  = true if only enabled units should be fetched, else all units would be fetched
     * @return array              An array containing all the fetched units 
     */
    public function getAllUnits($enabledOnly = true){
        $query = "SELECT * FROM inventory_units";
        $query .= $enabledOnly ? " WHERE inventory_unit_enabled = '1' " : "";
        $query .= " ORDER BY inventory_unit_name ";
        $result = $this->conn->run($query);
        $retVal = array();
        if ($this->conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $retVal[] = $row;
            }
        }
        return $retVal;
    }   //END getAllUnits()
    
    
    
    /**
     * Gets the details of the unit with unit ID $unitID
     * @param int $unitID   The ID of the unit under consideration
     * @return array        The details of the unit in an array 
     */
    public function getUnit($unitID = 0){
        $unitID = empty($unitID) ? $this->unitID : (int)$unitID;
        $query = "SELECT * FROM inventory_units
                    WHERE inventory_unit_id = '$unitID'";
        //die ($query);
        $result = $this->conn->run($query);
        $retVal = "";
        if ($this->conn->hasRows($result)){
            $retVal = mysql_fetch_assoc($result);
        }
        return $retVal;
    }   //END getUnit()
    
    
    
    /**
     * Converts the quantity in a certain unit for an item to the corresponding quantity in its lowest unit
     * @param int $unitID       The ID of the unit under consideration
     * @param int $itemID       The ID of the item that is making use of the unit
     * @param int $quantity     The quantity of the item in the unit with unit ID $unitID
     * @return int              The quantity in the lowest unit 
     */
    public function convertToLowestUnit($unitID, $itemID, $quantity){
        $this->error = false;
        $this->errorMsg = "";
        $unitID = (int)$unitID;
        $itemID = (int)$itemID;
        $curQuantity = $quantity = (double)$quantity;
        try {
            //Get info about the unit
            $query = "SELECT * FROM inventory_unit_items
                        WHERE inventory_unit_id = '$unitID'
                            AND inventory_items_id = '$itemID'";
            $result = $this->conn->run($query);
            if (!$this->conn->hasRows($result)){
                $this->errorMsg = "Invalid Unit Selected";
                throw new Exception();
            }
            
            $unitInfo = mysql_fetch_assoc($result);
            $curChild = $unitInfo["inventory_units_items_child_id"];
            
            while ($curChild != 0){
                //Get info about the child, which is the next lower unit
                $query = "SELECT * FROM inventory_unit_items
                            WHERE inventory_unit_items_id = '$curChild'
                                AND inventory_items_id = '$itemID'";
                $result = $this->conn->run($query);
                if ($this->conn->hasRows($result)){
                    $unitInfo = mysql_fetch_assoc($result);
                    $curQuantity = $curQuantity * ($unitInfo["inventory_units_items_child_quantity"] != 0 ? $unitInfo["inventory_units_items_child_quantity"] : 1);
                    $curChild = $unitInfo["inventory_units_items_child_id"];
                } else $curChild = 0;  //Just in case
            }
            
        } catch (Exception $e) {
            $this->error = true;
        }
        return $curQuantity;
    }   //END convertToLowestUnit()
    
    
    
    /**
     * Gets the representation of the quantity of an item in all units
     * @param number $quantity  The quantity in unit with ID $unitID (or the lowest unit if this is not specified) to be represented in all other units
     * @param int $itemID       The ID of the item under consideration
     * @param int $unitID       The unit ID of the unit under consideration, if not specified the lowest unit is used by default
     * @return array            Returns the representation of the quantity of an item in all units 
     */
    public function showInAllUnits($quantity, $itemID, $unitID = 0){
        //echo "<p>QUANTITY: '$quantity', ITEM ID: '$itemID'</p>";
        $unitID = (int)$unitID;
        $itemID = (int)$itemID;
        $quantity = (double)$quantity;
        $retVal = array ();
        $disUnitID = $curChild = 0;
        $curQty = 1;
        try {
            //Make sure the quantity is in terms of the lowest unit
            $remQuantity = $quantity = $this->convertToLowestUnit($unitID, $itemID, $quantity);
            //Make sure we are using the lowest unit
            $query = "SELECT * FROM inventory_unit_items
                        WHERE inventory_items_id = '$itemID'
                            AND inventory_units_items_child_id = '0'";
            $result = $this->conn->run($query);
            if ($this->conn->hasRows($result)){
                $row = mysql_fetch_assoc($result);
                $disUnitID = $row["inventory_unit_id"];
                $curChild = $row["inventory_unit_items_id"];
                $curQty = $row["inventory_units_items_child_quantity"] != 0 ? $row["inventory_units_items_child_quantity"] : 1;
            } else {
                $this->errorMsg = "Invalid Item Selected";
                throw new Exception();
            }
            
            //Now, do the actual operation
            $done = false;
            do {
                //Get info about this unit so as to get the name
                $disUnit = $this->getUnit($disUnitID);

                //Get the quantity for this unit after using the possible quantity to convert to the next higher unit
                $disQuantity = $remQuantity % $curQty;
                //echo "<p>$remQuantity % $curQty = $disQuantity";
                $temp = $remQuantity;
                //Now, get the remaining quantity to be used for the next higher unit
                $remQuantity = ($remQuantity - $disQuantity) / $curQty;
                
                //echo "<br />$remQuantity = ($temp - $disQuantity) / $curQty</p>";
                
                //Get info about the next higher unit to be used in the next iteration or as a condition to exit this loop
                $query = "SELECT * FROM inventory_unit_items
                            WHERE inventory_units_items_child_id = '$curChild'";
                $result = $this->conn->run($query);
                if ($this->conn->hasRows($result)){
                    //Now add to the final result to be returned
                    $retVal[] = array("id" => $disUnitID,
                                      "name" => $disUnit["inventory_unit_name"],
                                      "quantity" => $disQuantity
                                );
                    $row = mysql_fetch_assoc($result);
                    $disUnitID = $row["inventory_unit_id"];
                    $curChild = $row["inventory_unit_items_id"];
                    $curQty = $row["inventory_units_items_child_quantity"] != 0 ? $row["inventory_units_items_child_quantity"] : 1;
                } else {
                    $done = true;
                    $disQuantity = $remQuantity;
                    //Now add to the final result to be returned
                    $retVal[] = array("id" => $disUnitID,
                                      "name" => $disUnit["inventory_unit_name"],
                                      "quantity" => $disQuantity
                                );
                }
            } while (!$done);

        } catch (Exception $e) {
            $this->error = true;
        }
        //echo "<pre>" . print_r ($retVal, true) . "</pre>";
        return $retVal;
    }   //END showInAllUnits()
    
    
    
    
    /**
     * Associates all the units in $units array to the item with ID $itemID
     * @param int $itemID       The ID of the item under consideration
     * @param array $units      An array containing the IDs of the units to be associated with the item
     * @param array $quantities An array containing the corresponding quantities to be recorded against each unit as the number of that unit that would be equivalent to one unit of its direct parent unit
     * @param int $userID       The ID of the user carrying out this action
     * @return boolean          The error status at the end of the operation (true if an error occurred else false) 
     */
    public function saveItemUnits($itemID, $units, $quantities, $userID){
        $this->error = false;
        $this->errorMsg = "";
        $itemID = (int)$itemID;
        $userID = (int)$userID;
        $units = is_array($units) ? admin_Tools::doEscape($units, $this->conn) : array();
        $quantities = is_array($quantities) ? admin_Tools::doEscape($quantities, $this->conn) : array();
        
        try {
            $unitCount = count ($units);
            $qtyCount = count ($quantities);
            if ($unitCount == 0 || $unitCount != $qtyCount){
                $this->errorMsg = "Please, select at least one unit and enter its corresponding quantity.";
                throw new Exception();
            }
            
            $child = 0;
            for ($counter = 0; $counter < $unitCount; $counter++){
                $disUnit = (int)$units[$counter];
                $disQty = (int)$quantities[$counter];
                if ($disUnit != 0 && $disQty != 0){
                    $query = "INSERT INTO inventory_unit_items
                                SET inventory_items_id = '$itemID',
                                    inventory_unit_id = '$disUnit',
                                    inventory_units_items_child_id = '$child',
                                    inventory_units_items_child_quantity = '$disQty',
                                    inventory_units_items_dateadded = NOW(),
                                    user_id = '$userID'";
                    $result = $this->conn->run($query);
                    
                    //If insert was successful, get the insert ID to serve as child ID for the next unit
                    if ($this->conn->hasRows($result)){
                        $child = mysql_insert_id($this->conn->getConnectionID());
                    }
                }   //END if unit & quantity are both valid
            }   //END for
            
        } catch (Exception $e) {
            $this->error = true;
        }
        return $this->error;
    }
    
    public function getItemUnits($id){
        global $inventory_dbconnnect_error;
        global $inventory_none_in_existence;
        $query=$this->conn->run("select * from inventory_unit_items iut where iut.inventory_items_id='$id'");
        if(!$query){
            $report=$inventory_dbconnnect_error;
        }elseif(mysql_num_rows($query)<1){
            $report=$inventory_none_in_existence;
        }else{
            $reportArray=array();
            while($row=  mysql_fetch_assoc($query)){
                $reportArray[]=$row;
            }
            $report=$reportArray;
        }
        return $report;
    }
    
    public function getUnitName($unitId){
        global $inventory_dbconnnect_error;
        global $inventory_none_in_existence;
        $query=$this->conn->run("select inventory_unit_name from inventory_units where inventory_unit_id='".$unitId."'");
        if(!$query){
            $report=$inventory_dbconnnect_error;
        }elseif(mysql_num_rows($query)<1){
            $report=$inventory_none_in_existence;
        }else{
            $name=mysql_fetch_object($query);
            $report=$name->inventory_unit_name;
        }
        return $report;
    }
    
      
}   //END class
?>
