<?php
/**
 * Handles "Inpatient Summary" report
 * 
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */

/**
 * Handles "Inpatient Summary" report
 *
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */
class report_monthlystats_inpatsummary{
    /**
     * @var object Connection object
     */
	public $conn;

    /**
     * @var object report_common class object
     */
	protected $common;


/**
 * Class constructor
 *
 * @param int $month        The number of the month for which the report would be generated (01 - 12)
 * @param int $year         The year for which the report would be generated
 * @param date $fromDate    The lower bound of the date range for which the report would be generated
 * @param date $toDate      The upper bound of the date range for which the report would be generated
 */
	public function __construct($month, $year, $fromDate, $toDate){
		$this->conn = new DBConf();
		$this->common = new report_common();
		$this->days = $this->common->getDaysInMonth($year, $month);
		$this->month = $month;
		$this->year = $year;
		$this->periodStart = "$year-$month-01 00:00:00";
		$this->periodEnd = "$year-$month-" . $this->days . " 23:59:59";
	}   //END __construct()




/**
 * Gets the average number of beds available daily
 * @return float     The average number of beds available daily
 */
	public function getAvgDailyBedsAvailable(){
		$query = "SELECT get_avgbeds_available (" . $this->month . ", " . $this->year . ") average";
		$result = $this->conn->execute ($query);
		if ($result && mysql_affected_rows ($this->conn->getConnectionID()) > 0){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			$average = $row["average"];
		} else $average = 0;
		return $average;
	}   //END getAvgDailyBedsAvailable()




/**
 * Gets the average number of occupied beds available daily
 * @return float     The average number of occupied beds available daily
 */
	public function getAvgDailyBedsOccupied(){
		$query = "SELECT get_avgbeds_occupied (" . $this->month . ", " . $this->year . ") average";
		$result = $this->conn->execute ($query);
		if ($result && mysql_affected_rows ($this->conn->getConnectionID()) > 0){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			$average = $row["average"];
		} else $average = 0;
		return $average;
	}   //END getAvgDailyBedsOccupied()



/**
 * Gets the average length of time a patient stays in the ward
 * @return float     The average length of time a patient stays in the ward
 */
	public function getAvgStayLength(){
		$query = "SELECT SUM(DATEDIFF(inp_datedischarged, inp_dateattended)) / COUNT(*) average FROM inpatient_admission
					WHERE inp_dateattended BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'";// echo "<pre>$query</pre>";
		$result = $this->conn->execute ($query);
		if ($result && mysql_affected_rows ($this->conn->getConnectionID()) > 0){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			$average = $row["average"];
		} else $average = 0;
		return $average;
	}   //END getAvgStayLength()



/**
 * Gets the total number of admissions for all wards per month
 * @return int     The total number of admissions for all wards
 */
	public function getMonthAdmission(){
		$query = "SELECT COUNT(*) patients FROM inpatient_admission
					WHERE inp_dateattended BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'";
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows ($this->conn->getConnectionID()) > 0){
			$row = mysql_fetch_row ($result);
			$retVal = $row[0];
		} else $retVal = 0;
		return $retVal;
	}   //END getMonthAdmission()



/**
 * Gets the total number of patients discharged from all wards per month
 * @return int     The total number of patients discharged from all wards
 */
	public function getMonthDischarges(){
		$dead = $this->common->getDeadID();
		$query = "SELECT COUNT(*) patients FROM inpatient_admission inp
					WHERE inp_datedischarged BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
					AND inp.inp_conddischargedid <> '$dead'";// echo "<pre>$query</pre>";
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows ($this->conn->getConnectionID()) > 0){
			$row = mysql_fetch_row ($result);
			$retVal = $row[0];
		} else $retVal = 0;
		return $retVal;
	}   //END getMonthDischarges()




/**
 * Gets the total number of patients that died in all wards per month
 * @return int     The total number of patients that died in all wards
 */
	public function getMonthDeaths(){
		$dead = $this->common->getDeadID();
		$query = "SELECT COUNT(*) patients FROM inpatient_admission
					WHERE ct_id = '$dead' AND inp_datedischarged BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'";
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows ($this->conn->getConnectionID()) > 0){
			$row = mysql_fetch_row ($result);
			$retVal = $row[0];
		} else $retVal = 0;
		return $retVal;
	}   //END getMonthDeaths()




/**
 * Gets the total number of patients in all wards at the beginning of the month under consideration
 * @return int     The total number of patients in all wards at the beginning of the month under consideration
 */
	public function getPatsMonthBegin(){
		$dead = $this->common->getDeadID();
		$query = "SELECT COUNT(*) patients FROM inpatient_admission
					WHERE inp_dateattended < '" . $this->periodStart . "' AND (inp_datedischarged IS NULL OR inp_datedischarged = '0000-00-00 00:00:00' OR inp_datedischarged > '" . $this->periodStart . "')";
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows ($this->conn->getConnectionID()) > 0){
			$row = mysql_fetch_row ($result);
			$retVal = $row[0];
		} else $retVal = 0;
		return $retVal;
	}   //END getPatsMonthBegin()




/**
 * Gets the total number of patients in all wards at the end of the month under consideration
 * @return int     The total number of patients in all wards at the beginning of the month under consideration
 */
	public function getPatsMonthEnd(){
		$dead = $this->common->getDeadID();
		$query = "SELECT COUNT(*) patients FROM inpatient_admission
					WHERE inp_dateattended < '" . $this->periodEnd . "' AND (inp_datedischarged IS NULL OR inp_datedischarged = '0000-00-00 00:00:00' OR inp_datedischarged > '" . $this->periodEnd . "')";
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows ($this->conn->getConnectionID()) > 0){
			$row = mysql_fetch_row ($result);
			$retVal = $row[0];
		} else $retVal = 0;
		return $retVal;
	}   //END getPatsMonthEnd()


/**
 * Gets the actual value for the number of admissions, discharges or deaths
 * @param string $query The query to fetch the required value
 * @return array        An array containing the fetched value for different genders & their total
 */
	protected function getDAD($query){
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			$total = 0;
			$retVal = array();
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$retVal[$row["sex"]] = $row["patients"];
				$total += $row["patients"];
			}

			if (empty($retVal["0"]))
				$retVal["0"] = 0;

			if (empty($retVal["1"]))
				$retVal["1"] = 0;

			$retVal["total"] = $total;
		} else $retVal = array ("0" => 0, "1" => 0, "total" => 0);
		return $retVal;
	}   //END getDAD()



/**
 * Gets the number of admissions for all wards per month per gender and the total for all genders
 * @return array    An array containing the fetched value for different genders & their total
 */
	public function getAdmissions(){
		$query = "SELECT reg.reg_gender sex, COUNT(*) patients
					FROM inpatient_admission inp INNER JOIN patient_admission pat INNER JOIN registry reg
					ON inp.patadm_id = pat.patadm_id AND pat.reg_hospital_no = reg.reg_hospital_no
					WHERE inp.inp_dateattended BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
					GROUP BY reg.reg_gender
					ORDER BY reg.reg_gender";
		return $this->getDAD($query);
	}   //END getAdmissions()




/**
 * Gets the number of deaths for all wards per month per gender and the total for all genders
 * @return array    An array containing the fetched value for different genders & their total
 */
	public function getDeaths(){
		$dead = $this->common->getDeadID();
		$query = "SELECT reg.reg_gender sex, COUNT(*) patients
					FROM inpatient_admission inp INNER JOIN patient_admission pat INNER JOIN registry reg
					ON inp.patadm_id = pat.patadm_id AND pat.reg_hospital_no = reg.reg_hospital_no
					WHERE ct_id = '$dead' AND inp_datedischarged BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
					GROUP BY reg.reg_gender
					ORDER BY reg.reg_gender";
		return $this->getDAD($query);
	}   //END getDeaths()




/**
 * Gets the number of discharges for all wards per month per gender and the total for all genders
 * @return array    An array containing the fetched value for different genders & their total
 */
	public function getDischarges(){
		$dead = $this->common->getDeadID();
		$query = "SELECT reg.reg_gender sex, COUNT(*) patients
					FROM inpatient_admission inp INNER JOIN patient_admission pat INNER JOIN registry reg
					ON inp.patadm_id = pat.patadm_id AND pat.reg_hospital_no = reg.reg_hospital_no
					WHERE ct_id <> '$dead' AND inp_datedischarged BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
					GROUP BY reg.reg_gender
					ORDER BY reg.reg_gender";
		return $this->getDAD($query);
	}   //END getDischarges()



/**
 * Gets the percentage of occupancy for the month under consideration
 * @param int $occupied     The number of occupied beds for the period under consideration
 * @return float            The percentage of occupancy
 */
	public function getOccuPerc($occupied){
		$query = "SELECT (SUM(ward_bedspaces) + SUM(ward_siderooms)) totalbeds FROM wards";
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows ($this->conn->getConnectionID()) > 0){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			if ($row["totalbeds"] != 0)
				$percent = ((float)$occupied / (float)$row["totalbeds"]) * 100;
			else $percent = 0;
		} else $percent = 0;
		return $percent;
	}   //END getOccuPerc()

}
?>