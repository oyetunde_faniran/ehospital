<?php
/**
 * Handles "In-patient and Out-Patient Attendances" report
 * 
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */

/**
 * Handles "In-patient and Out-Patient Attendances" report
 *
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */
class report_monthlystats_inoutdepts{
    /**
     * @var object Connection class object
     */
	public $conn;

    /**
     * @var object report_common class object
     */
	protected $common;

    /**
     * @var int The number of the month for which the report would be generated
     */
	protected $month;

    /**
     * @var int The year for which the report would be generated
     */
	protected $year;


/**
 * Class constructor
 *
 * @param int $month    The number of the month for which the report would be generated (01 - 12)
 * @param int $year     The year for which the report would be generated
 */
	public function __construct($month, $year){
		$this->conn = new DBConf();
		$this->common = new report_common();
		$this->month = $month;
		$this->year = $year;
		$this->days = $this->common->getDaysInMonth($year, $month);
		$this->periodStart = "$year-$month-01 00:00:00";
		$this->periodEnd = "$year-$month-" . $this->days . " 23:59:59";
	}   //END __construct()


/**
 * Runs a query and returns a single value
 * @param string $query The query to execute
 * @return int          The value retrieved on running the query
 */
	protected function getValue($query){
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			$retVal = $row["patients"];
		} else $retVal = 0;
		return $retVal;
	}   //END getValue()
	

/**
 * Gets the number of new patients in all in-patient & out-patient clinics
 * @param string $type     Can be either "in" or "out" for inpatient and outpatient respectively
 * @param int $sex         0 for male 1 for female
 * @return int             The value returned by getValue()
 */
	protected function getNewValue($type, $sex){
		$table_inPeriod = "temp_inperiod_" . $type;
		$table_notInPeriod = "temp_notinperiod_" . $type;
		$query = "SELECT COUNT(intable.patadm_id) patients FROM $table_inPeriod intable
					WHERE intable.patadm_id NOT IN
					(
						SELECT * FROM $table_notInPeriod
					)
					AND intable.sex = '$sex'";
		return $this->getValue($query);
	}   //END getNewValue()



/**
 * Gets the total number of patients in all in-patient & out-patient clinics within the specified time frame
 * @param string $type     Can be either "in" or "out" for inpatient and outpatient respectively
 * @param int $sex         0 for male 1 for female
 * @return int             The total value as returned by getValue()
 */
	protected function getTotalValue($type, $sex){
		$table = "temp_inperiod_" . $type;
		$query = "SELECT COUNT(patadm_id) patients
					FROM $table t
					WHERE t.sex = '$sex'";
		return $this->getValue($query);
	}   //END getTotalValue()



/**
 * Generates the actual report by calling all other appropriate protected methods to get all the required values and then generate the required HTML with it
 * @param int $dept                 The ID of the department under consideration
 * @param string $totalInWords      The string literal "Total" in the currently selected language
 * @param string $gTotalInWords     The string literal "Grand Total" in the currently selected language
 * @return string                   The generated HTML
 */
	public function getResults($dept, $totalInWords, $gTotalInWords){
		//Drop temporary tables & indexes created if existing
		$query = "DROP TABLE IF EXISTS temp_notinperiod_in";
		$this->conn->execute ($query);
		$query = "DROP TABLE IF EXISTS temp_inperiod_in";
		$this->conn->execute ($query);
		$query = "DROP TABLE IF EXISTS temp_notinperiod_out";
		$this->conn->execute ($query);
		$query = "DROP TABLE IF EXISTS temp_inperiod_out";
		$this->conn->execute ($query);

		//INPATIENTS: Create a temporary table and store a distinct list of all the transactions not in the month under consideration
		$query = "CREATE TEMPORARY TABLE temp_notinperiod_in
					SELECT DISTINCT patadm_id
					FROM inpatient_admission
					WHERE inp_dateattended < '" . $this->periodStart . "'";
		$this->conn->execute ($query);// echo "<pre>$query</pre>";

		//INPATIENTS: Create a temporary table and store a distinct list of all the transactions in the month under consideration
		$query = "CREATE TEMPORARY TABLE temp_inperiod_in
					SELECT inp.patadm_id, reg.reg_gender sex
					FROM inpatient_admission inp 
						INNER JOIN patient_admission pat 
						INNER JOIN registry reg
						INNER JOIN wards w
						INNER JOIN clinic c
					ON inp.patadm_id = pat.patadm_id 
						AND pat.reg_hospital_no = reg.reg_hospital_no
						AND inp.ward_id = w.ward_id
						AND w.clinic_id = c.clinic_id
					WHERE inp_dateattended BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
						AND c.dept_id = '$dept'";
		$this->conn->execute ($query);

		//OUTPATIENTS: Create a temporary table and store a distinct list of all the transactions not in the month under consideration
		$query = "CREATE TEMPORARY TABLE temp_notinperiod_out
					SELECT DISTINCT app.patadm_id FROM appointment app
					WHERE app.app_starttime < '" . $this->periodStart . "'";
		$this->conn->execute ($query);

		//OUTPATIENTS: Create a temporary table and store a distinct list of all the transactions in the month under consideration
		$query = "CREATE TEMPORARY TABLE temp_inperiod_out
					SELECT app.patadm_id, reg.reg_gender sex
					FROM appointment app 
						INNER JOIN patient_admission pat 
						INNER JOIN registry reg
					ON app.patadm_id = pat.patadm_id 
						AND pat.reg_hospital_no = reg.reg_hospital_no
					WHERE app.app_starttime BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
						AND app.dept_id = '$dept'";
		$this->conn->execute ($query);

		//Create indexes on the temp tables above
		$query = "CREATE INDEX patadm_id ON temp_notinperiod_in (patadm_id)";
		$this->conn->execute ($query);
		$query = "CREATE INDEX patadm_id ON temp_inperiod_in (patadm_id)";
		$this->conn->execute ($query);
		$query = "CREATE INDEX patadm_id ON temp_notinperiod_out (patadm_id)";
		$this->conn->execute ($query);
		$query = "CREATE INDEX patadm_id ON temp_inperiod_out (patadm_id)";
		$this->conn->execute ($query);

		//New
		$newM_in = $this->getNewValue("in", 0);			//New in-patients, male
		$newF_in = $this->getNewValue("in", 1);			//New in-patients, female
		$new_in_total = $newM_in + $newF_in;

		$newM_out = $this->getNewValue("out", 0);		//New out-patients, male
		$newF_out = $this->getNewValue("out", 1);		//New out-patients, female
		$new_out_total = $newM_out + $newF_out;

		//Total
		$totalM_in = $this->getTotalValue("in", 0);		//Total in-patients, male
		$totalF_in = $this->getTotalValue("in", 1);		//Total in-patients, female
		$total_in_total = $totalM_in + $totalF_in;
		
		$totalM_out = $this->getTotalValue("out", 0);	//Total out-patients, male
		$totalF_out = $this->getTotalValue("out", 1);	//Total out-patients, female
		$total_out_total = $totalM_out + $totalF_out;

		//Old
		$oldM_in = $totalM_in - $newM_in;
		$oldF_in = $totalF_in - $newF_in;
		$old_in_total = $oldM_in + $oldF_in;

		$oldM_out = $totalM_out - $newM_out;
		$oldF_out = $totalF_out - $newF_out;
		$old_out_total = $oldM_out + $oldF_out;
		
		//Grand Total
		$gTotalNewM = $newM_in + $newM_out;
		$gTotalNewF = $newF_in + $newF_out;
		$gTotalNewTotal = $gTotalNewM + $gTotalNewF;

		$gTotalOldM = $oldM_in + $oldM_out;
		$gTotalOldF = $oldF_in + $oldF_out;
		$gTotalOldTotal = $gTotalOldM + $gTotalOldF;

		$gTotalTotalM = $totalM_in + $totalM_out;
		$gTotalTotalF = $totalF_in + $totalF_out;
		$gTotalTotal = $gTotalTotalM + $gTotalTotalF;

		//Drop the temporary tables created
		$query = "DROP TABLE temp_inperiod_out";
		$this->conn->execute ($query);
		$query = "DROP TABLE temp_notinperiod_out";
		$this->conn->execute ($query);
		$query = "DROP TABLE temp_inperiod_in";
		$this->conn->execute ($query);
		$query = "DROP TABLE temp_notinperiod_in";
		$this->conn->execute ($query);

		$result = "<tr class=\"tr-row\">
						<td>In-Patient</td>
			
						<td>$newM_in</td>
						<td>$newF_in</td>
						<td>$new_in_total</td>

						<td>$oldM_in</td>
						<td>$oldF_in</td>
						<td>$old_in_total</td>

						<td>$totalM_in</td>
						<td>$totalF_in</td>
						<td>$total_in_total</td>
					</tr>
					<tr class=\"tr-row2\">
						<td>Out-Patient</td>
			
						<td>$newM_out</td>
						<td>$newF_out</td>
						<td>$new_out_total</td>

						<td>$oldM_out</td>
						<td>$oldF_out</td>
						<td>$old_out_total</td>

						<td>$totalM_out</td>
						<td>$totalF_out</td>
						<td>$total_out_total</td>
					</tr>
					<tr>
						<td>$gTotalInWords</td>
			
						<td>$gTotalNewM</td>
						<td>$gTotalNewF</td>
						<td>$gTotalNewTotal</td>

						<td>$gTotalOldM</td>
						<td>$gTotalOldF</td>
						<td>$gTotalOldTotal</td>

						<td>$gTotalTotalM</td>
						<td>$gTotalTotalF</td>
						<td>$gTotalTotal</td>
					</tr>";

		return $result;

	}	//END getResults()

}
?>