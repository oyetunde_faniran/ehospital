<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventoryCreationFormProcessingClass
 *
 * @author upperlink
 */
class inventory_units_class extends inventoryGeneralClass{
    public $conn;
    
    public function __construct() {
        $conn = new DBConf();
        $this->conn = $conn->mysqliConnect();
        //$this->conn=new mysqli('localhost','root','','mediluth_skye');
    }
    
        public function addNewUnit($name,$parent){
        global $report;
        $report = "";
        global $inventoryUnitCreationSuccessNews;
        global $inventoryUnitCreationFailureNews;
        global $inventory_no_unitName_error_msg;
        if (strlen($name) < 1) {
            $report.=$inventory_no_unitName_error_msg;
        }
        if ($report != "") {
            return $report;
        }
        $createUnit = $this->conn->query("insert into inventory_units (inventory_unit_name, inventory_unit_parent, inventory_unit_date_created, user_id) values ('".$name."','".$parent."',CURRENT_TIMESTAMP, '" .$_SESSION[session_id()."userID"]."')");
        if (!$createUnit) {
            $report.=$inventoryUnitCreationFailureNews;
        } else {
            $report.=$inventoryUnitCreationSuccessNews;
        }
        return $report;
    }
    
    public function itIsParent($id){
        global $report;
        global $inventorySubunitSystemFailureMsg;
        $found=$this->conn->query("select * from inventory_units where inventory_unit_parent='".$id."' order by inventory_unit_name asc");
        if(!$found){
			$report=$inventorySubunitSystemFailureMsg;
            return $report;
        }
        if($found->num_rows<1){
        return FALSE;
        }
        return $found;
    }
    
        public function getUnitName($id){
        global $report;
        $catnames = $this->conn->query("select inventory_unit_name from inventory_units where inventory_unit_id='" . $id . "'");
        if (!$catnames) {
            $report = $inventory_dbconnect_error;
            return $report;
    }
    if($catnames->num_rows<1){
        $report=$none_in_existence;
        return $report;
        }
        $catname=$catnames->fetch_object();
        $name=$catname->inventory_unit_name;
        return $name;
    }
    
    public function getUnitProperties($id){
        global $report;
        $properties = $this->conn->query("select * from inventory_units where inventory_unit_id='" . $id . "'");
        if (!$properties) {
            $report = "System Error: Unable to check unit name this time. Please retry";
            return $report;
    }
    if($properties->num_rows<1){
        $report="No such unit found. Please retry!";
        return $report;
        }
        $property=$properties->fetch_array();
        return $property;
    }
    
  
    
    public function removeUnit($id){
        global $report;
        global $inventory_unit_delete_success_msg;
        global $inventory_unit_delete_error_msg;
        if($this->itIsParent($id)){
            $report="This unit has sub units and so cannot be removed until its children units have all been removed";
            return $report;
        }
        
        $removed=  $this->conn->query("delete from inventory_units where inventory_unit_id='".$id."'");
        if(!$removed){
            $report=$inventory_unit_delete_error_msg;
            return $report;
            }else{
                $report=$inventory_unit_delete_success_msg;
                return $report;
            }
    }
   
    public function countChildren($id){
        $childrenCount=$this->conn->query("select * from inventory_units where inventory_unit_parent='".$id."'");
        if(!$childrenCount){
            return FALSE;
        }
        return $childrenCount->num_rows;
    }
   
    
    public function listUnits($id) {
        global $report;
        global $click_to_edit;
        global $click_to_remove;
        global $click_to_add_unit;
        global $inventory_unit;
        global $inventory_no_unit_msg;
        global $inventorySubunitSystemFailureMsg;
        $units = $this->conn->query("select * from inventory_units where inventory_unit_parent='" . $id . "' order by inventory_unit_name");
        if (!$units) {
            $report = $inventorySubunitSystemFailureMsg;
            return $report;
        }
        if ($units->num_rows < 1) {
            $report=$inventory_no_unit_msg;
            return $report;
            }
        return $units;
        }
      
// public function showAllUnits(){
//        global $report;
//        global $click_to_edit;
//        global $click_to_remove;
//        global $click_to_add_unit;
//        global $inventorySubunitSystemFailureMsg;
//        $units=$this->conn->query("select * from inventory_units where inventory_unit_parent='0' order by inventory_unit_name");
//        if(!$units){
//        $report=$inventorySubunitSystemFailureMsg;    
//            return $report;
//              }
//        if($units->num_rows<1){
//            return FALSE;
//        }
//        echo "<ul class=\"unitsList\">";
//        while($unit=$units->fetch_assoc()){
//            global $count;
//            $count=  $this->countChildren($unit['inventory_unit_id']);
//            echo "<li class=\"closed\" onClick=\"javascript:toggleDisplay(this)\">".$unit['inventory_unit_name']."( " .$count." )<a href=\"index.php?p=inv_index&m=pharmacy&folder=inventory_list&&inventoryAction=units&edit=".$unit['inventory_unit_id']."\"><img src=\"images/btn_edit.gif\" title=\"".$click_to_edit."\" /></a>&nbsp;&nbsp;<a href=\"index.php?p=inv_index&m=pharmacy&folder=inventory_new&&inventoryAction=new unit&add=".$unit['inventory_unit_id']."\"><img src=\"images/btn_add_02.gif\" title=\"".$click_to_add_unit."\" /></a>&nbsp;&nbsp;";
//        if($this->itIsParent($unit['inventory_unit_id'])){
//                $this->showSubUnits($unit['inventory_unit_id']);
//            }else{ 
//          echo "&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 
//           <a href=\"index.php?p=inv_index&m=pharmacy&folder=inventory_list&&inventoryAction=units&remove=".$unit['inventory_unit_id']."\" onClick=\"javascript:confirm_to_delete_inventory_unit(this.href); return false;\"><img src=\"images/b_drop.png\" title=\"".$click_to_remove."\" /></a>";}
//            echo "</li>";
//        }
//        echo "</ul>";
//    }
//
// public function showSubUnits($id){
//        global $result;
//        global $click_to_remove;
//        global $click_to_edit;
//        global $click_to_add_unit;
//        $subcats=$this->conn->query("select * from inventory_units where inventory_unit_parent='".$id."' order by inventory_unit_name");
//        if(!$subcats){$result=$inventorySubunitSystemFailureMsg;
//        return $result;
//        }
//       if($subcats->num_rows>0){
        /*?>
      <!-- <ul class="unitsList">-->
       //<?php
       //while($subcat=$subcats->fetch_assoc()){
//           global $count;
//            $count=  $this->countChildren($subcat['inventory_unit_id']);
//           echo "<li class=\"closed\" onClick=\"javascript:toggleDisplay(this)\" title=\"".$subcat['inventory_unit_description']."\"><table width=\"100%\" align=\"left\" border=\"0\"><tr><td align=\"left\" width=\"150px\">".$subcat['inventory_unit_name']."( ".$count." )</td><td align=\"left\" width=\"50px\"><a href=\"index.php?p=inv_index&m=pharmacy&folder=inventory_list&&inventoryAction=units&edit=".$subcat['inventory_unit_id']."\"><img src=\"images/btn_edit.gif\" title=\"".$click_to_edit."\" /></a>&nbsp;&nbsp;</td><td align=\"left\" width=\"50px\"><a href=\"index.php?p=inventory_index&m=pharmacy&folder=inventory_list&&inventoryAction=new unit&add=".$subcat['inventory_unit_id']."\"><img src=\"images/btn_add_02.gif\" title=\"".$click_to_add_unit."\" /></a>&nbsp;</td>";
//           if($this->itIsParent($subcat['inventory_unit_id'])){
//               $this->showSubUnits($subcat['inventory_unit_id']);
//                }else{echo "<td align=\"left\" width=\"50px\">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <a href=\"index.php?p=inv_index&m=pharmacy&folder=inventory_list&&inventoryAction=units&remove=".$subcat['inventory_unit_id']."\" onClick=\"javascript:confirm_to_delete(this.href,'unit'); return false;\"><img src=\"images/b_drop.png\" title=\"".$click_to_remove."\" /></a></td>";}
//                echo "</tr></table></li>";
//            }echo "</ul>";
//        }
//    }        
//        */
}
?>
