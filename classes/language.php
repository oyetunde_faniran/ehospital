<?
/**

 * @package language
*
 * @author ahmed rufai
 *
 * Create Date: 7-11-2009
 *
 *

 *
 */
require_once './classes/DBConf.php';

Class language {

/**
 *
 * @var array fields in the language table that holds language text
 */
public $lang=array() ;
/**
 *
 * @var araay values for the corresponding fields in the table
 *
 */
public $lang2=array() ;
/**
 * class constructor
 */
	
	 function  __construct() {
        $this->conn = new DBConf();
    }
/**
 * returns text corresponding to the supplied row ID
 * @param integer $id row ID in the language table
* @param string $curLnag_field column in the language table that contains current language text
* @param string $langtb  language table
 * @return string
 *
 */
function getlang_content($id,$curLnag_field,$langtb) {
        try {
           $sql = 'Select '.$curLnag_field.'  from '. $langtb.'  where langcont_id='.$id;
		   
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			
			return stripslashes($row[$curLnag_field]);
			//return $sql;
		} catch (Exception $e) {


        }
    }

/**
 * serverside form validator
 * @param string $xlp_formfield form fields
 * @param string $xlp_fieldmessage error message
 * @return <type>
 */
function xlpildator($xlp_formfield,$xlp_fieldmessage) {
        try {
           $xlp_error = 0;
		   $list =array();

					$xlp_error_message = '';
				
					 $numarg = count($xlp_formfield);

					 for($i=0; $i < $numarg; $i++){

					if((!isset($_POST[$xlp_formfield[$i]])) or $_POST[$xlp_formfield[$i]] == '' or $_POST[$xlp_formfield[$i]] == " "){

						$xlp_error_message .= $xlp_fieldmessage[$i]."<br>";

						$xlp_error = $i + 1;

						} 				

 					}
			
			$list[1]=$xlp_error_message;
			$list[2]=$xlp_error;
			return $list;
			//echo $list[1];
		//	exit();
		} catch (Exception $e) {


        }
    }


	
	}
?>
