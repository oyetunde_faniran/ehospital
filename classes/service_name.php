<?php
/** Contains variables, methods and other functionalitieds to manage service names
*@author ahmed rufai
 * @package service name
 * Create Date: 26-08-2009
 */
require_once './classes/DBConf.php';

Class service_name {
 /**
     *
     * @var array holds array of fields in the servicename table
     */
    public $sname=array() ;
   /**
     * @var array holds values to be stored in the servicename table field
       */
    public $sname2=array() ;
	public $sername_id; //int(10) unsigned
	public $langcont_id; //int(10) unsigned
	    public $conn;
	
	
	 function  __construct() {
     $this->conn = new DBConf();
    }
	
    /**
     * New object to the class. Don�t forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New(); 
     *
     */
	public function New_service_name($langcont_id){
		$this->langcont_id = $langcont_id;
	}

    /**
     * Load one row into class variables, using specified row key
     *
     * @param int $key_row
     * 
     */
	public function Load_from_key($key_row){
		$result = $this->conn->execute("Select * from service_name where sername_id = \"$key_row\" ");
		while($row = mysql_fetch_array($result)){
			$this->sername_id = $row["sername_id"];
			$this->langcont_id = $row["langcont_id"];
		}
	}

   
	/**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     *
     */
	public function Delete_row_from_key($key_row){
		$this->conn->execute("DELETE FROM service_name WHERE sername_id = $key_row");
	}

    /**
     * Update the active row on table
     */
	 
public function Save_Active_Row($lang_id,$id,$tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("UPDATE drugcategory set drugcat_name = \"$this->drugcat_name\" where drugcat_id = \"$this->drugcat_id\"");*/
		
		try {
		$q=count($this->sname);
		
		
		for ($i = 0; $i < $q; $i++) {
			if($this->sname[$i]=='langcont_id')
		//	$qq="UPDATE $langtb set $curLnag_field ='".$this->sname2[$i]."' where langcont_id = \"$lang_id\"";
				
		$this->conn->execute("UPDATE $langtb set $curLnag_field ='".$this->sname2[$i]."' where langcont_id = \"$lang_id\"");

		}
		
			
	$sql = "UPDATE $tablename SET ";	
	
				$qq=count($this->sname);
				$q=count($this->sname);

			for ($i = 0; $i < $qq; $i++) {
				//$this->sname[$i].'-'.$this->sname2[$i];
				
	  
	  				if(($q-1)==0){
						if($this->sname[$i]=='langcont_id'){
						     $sql.=    $this->sname[$i] .'="'.$lang_id.'" ';
							 }
						else
         			  		  $sql.=    $this->sname[$i] .'="'.$this->sname2[$i].'" ';
			 
					 }
					 else{
					     if($this->sname[$i]=='langcont_id')
						     $sql.=      $this->sname[$i] .'="'.$lang_id.'" ,';
						 else
						     $sql.=      $this->sname[$i] .'="'.$this->sname2[$i].'" ,';
			
					 }
				
  				 $q--;
 			}
			//echo $sql; 
			//exit();
			$result2 =$this->conn->execute("SELECT * from $tablename");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
		//echo $sql;
	//exit();
         $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		
		
		
		
	}

    /**
     * Save the active class vars as a new row on table
     */
	public function Save_Active_Row_as_New($tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("Insert into drugcategory (drugcat_name) values (\"$this->drugcat_name\"");*/
		try {
		
		// server validation begins
					$getLang= new language();		
					$xlp_formfield =$this->sname;
					$xlp_fieldmessage = array('Please specify Service name');
		            $xlp_error=$getLang->xlpildator($xlp_formfield,$xlp_fieldmessage);
					if (!empty($xlp_error[2]))
					throw new Exception;
	   //sever validation ends
		$q=count($this->sname);
		for ($i = 0; $i < $q; $i++) {
			if($this->sname[$i]=='langcont_id'){
			$qq="Insert into $langtb ($curLnag_field) values ('".$this->sname2[$i]."')";
			//echo $qq;
			    $this->conn->execute($qq);
				$content_id= mysql_insert_id();
			}	
		}

		
		$sql = "INSERT INTO $tablename SET ";	
	
				$qq=count($this->sname);
				//$q=count($this->sname);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->sname[$i]=='langcont_id')
						     $sql.=    $this->sname[$i] .'="'.$content_id.'" ';
						else
         			  		  $sql.=    $this->sname[$i] .'="'.$this->sname2[$i].'" ';
			 
					 }
					 else{
					     if($this->sname[$i]=='langcont_id')
						     $sql.=      $this->sname[$i] .'="'.$content_id.'" ,';
						 else
						     $sql.=      $this->sname[$i] .'="'.$this->sname2[$i].'" ,';
			    	 }
  				 $q--;
 			}
		//	$sql.=" ,drugtype_order=".$this->getNext_order();
		$result=$this->conn->execute($sql);
				 $list=array();
				if($this->conn->hasRows($result,  0)){
					 $list[3]=true;
					return $list;	 
					}
        } catch (Exception $e) {
            return $xlp_error;
        }
	}
	
	public function GetSearchFields(){
		echo 	"<option value='-1'>Select Field</option>";	
		$result =$this->conn->execute("SELECT * from service_name order by sername_id");
				$i = 0;
				
			while ($i < mysql_num_fields($result)) {
 //   echo "Information for column $i:<br />\n";
	  		  $meta = mysql_fetch_field($result);
	
 		   if (!$meta) {
      //  echo "No information available<br />\n";
  		  }
  //  echo "$meta->name";
  
		if($meta->name=='langcont_id')
	echo 	"<option value=". $meta->name.">".'service name'."</option>";
   else
	echo 	"<option value=". $meta->name.">".$meta->name."</option>";
	   
	   
	   
	   
	    $i++;
}
/*mysql_free_result($result);
	return $keys;*/
	}
	/**
     *returns drug name/desc
     * @param int $id
     * @param string $curLnag_field
     * @param string $langtb
     * @return string
     *
     */
	function getdrug_name($id,$curLnag_field,$langtb) {
        try {
		  $getLang= new language();
           $sql = 'Select * from service_name  where sername_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
		return	$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
        //    return $row['drug_desc'];
				 
            
			
        } catch (Exception $e) {


        }
    }
	
	
	
/*function getlang_content($id,$curLnag_field,$langtb) {
        try {
           $sql = 'Select '.$curLnag_field.'  from '. $langtb.'  where langcont_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			return $row[$curLnag_field];
		} catch (Exception $e) {


        }
    }*/
    /**
     *populates option tags with servicename from the serviceame table; highlight option that matches supplied ID
     * @param int $id
       * @param string $curLnag_field
     * @param string $langtb
     */
function getservice_name2($id,$curLnag_field,$langtb) {
        try {
		    $getLang= new language();
			$sql2 = 'select sername_id from service_name  where sername_id ='.$id;
			
				 $res2 = $this->conn->execute($sql2);
				 if( $row2 = mysql_fetch_array($res2))
				  $id2=$row2['sername_id'];
		
            $sql = 'select sername_id,langcont_id from service_name';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
			$sel= ($id2==$row['sername_id'])? "selected":" ";
			
                echo '<option value ="'.$row['sername_id'] .'">'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {


        }
    }

	/**
     *populates option tags with servicename from the serviceame table
       * @param string $curLnag_field
     * @param string $langtb
     */
function getservice_name($curLnag_field,$langtb) {
        try {
		    $getLang= new language();
            $sql = 'select sername_id,langcont_id from service_name';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                echo '<option value ="'.$row['sername_id'] .'">'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
               }
        } catch (Exception $e) {


        }
    }
/**
     * list all rows from a specified table
     * @param string $curLnag_field
     * @param string $langtb
     */
	
function allrows($curLnag_field,$langtb) {
        
		try {
	 $getlang= new language();
	$db2 = new DBConf();
		 $sql = 'SELECT
*
FROM
service_name 
';
 $res = $this->conn->execute($sql);
 if($this->conn->hasRows($res)){
 	//die ((string)mysql_affected_rows($this->conn->getConnectionID()));
	$pageindex='servicename';
	$pager = new PS_Pagination($db2,$sql,100,10,$pageindex);
	$rs = $pager->paginate();
     $offset=$pager->offset;       

	   // $res = $this->conn->execute($sql);
		$i=1;
	   while ($row = mysql_fetch_array($rs)) {
				 
				// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
				 if ($i%2 ==0) {$bgcolor = "tr-row";} else {$bgcolor = "tr-row2";} 
			    echo' <tr class="'.$bgcolor.'">
					
			    <td>'.(++$offset).'.</td>
		   
			<td><a href = "./index.php?p=editservicename&sername_id='.$row["sername_id"].'&langid='.$row["langcont_id"] .'">'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</a></td>
				
			
			</tr>';
			$i++;
		}	//END while
	} else echo "No service has been added yet.";	//END if
?>
		 <tr>
            <td  colspan="5"><?php if (!empty($pager)) echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
        } catch (Exception $e) {
	
        }

  
    }	//END function
	/**
     * list rows that match specified criteria
     * @param string $search_field
     * @param string $search_value
     * @param string $curLnag_field
     * @param string $langtb
     */
	function rowSearch($search_field,$search_value,$curLnag_field,$langtb){
		 try {
		  $getlang= new language();
		 $db2 = new DBConf();
		$sql = "Select * from  service_name where  ".$search_field."='".$search_value."'" ;
		$res =$this->conn->execute($sql);
		
$pageindex='servicename';
$pager = new PS_Pagination($db2,$sql,2,10,$pageindex);
$rs = $pager->paginate();
	  // echo "entered";
	     $i=1;
		while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["drugcat_id"].'" />'.$ii.'</td>
        <td>'.$row["sername_id"].'</td>
		<td>'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</td>
		       
        <td>'.'<a href = "./index.php?jj=delete&p=servicename&id='.$row["sername_id"] .'"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td><td>'.'<a href = "./index.php?p=editservicename&sername_id='.$row["sername_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
		} catch (Exception $e) {
        }
	}
	

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->conn->execute("SELECT sername_id from service_name order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["sername_id"];
				$i++;
			}
	return $keys;
	}

    /**
     * Close mysql connection
     */
	public function endservice_name(){
		$this->connection->CloseMysql();
	}

}