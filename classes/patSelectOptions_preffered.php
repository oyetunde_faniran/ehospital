<?php
/**
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */

/**
 * Generates the HTML for the drop-down list for different entities
 *
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */
class patSelectOptions {


    public $select = array();
    function  __construct() {
        $this->conn = new DBConf();
    }
	
	
    function selectNationality($nationality_id = 0) {
        try {
            $query = "SELECT * FROM nationality" ;
			if($nationality_id != 0){
			$query .= " WHERE nationality_id='".$nationality_id."'";
			}
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
				 extract($row);
				 $select = array('nationality_id' => $nationality_id,
				 				 'nationality_name' => $nationality_name);
				return $select;
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }


    function selectState($state_id=0) {
        try {
            $query = "SELECT state_id,  state_name FROM state" ;
			if($state_id != 0){
			$query .= " WHERE state_id='".$v."'";
			}
			
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
				 extract($row);
				 $select = array('state_id' => $v,
				 				 'state_name' => $state_name);
            		}
					return $select;
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }


    function selectRetainer($retainership_id = 0, $isretainer) {
        try {
            $query = "SELECT * FROM retainership WHERE retainer_isretainer='".$isretainer."'" ;
			if($retainership_id != 0){
			$query .= " AND retainership_id='".$retainership_id."'";
			}
			
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
				 extract($row);
				 $select = array('retainership_id' => $retainership_id,
				 				 'retainership_company' => $retainership_company);
            		}
					return $select;
            		}
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }


 function selectCondition($ct_type) {
        try {
            $query = "SELECT * 
					 FROM 
					 	conditiontype as c, language_available as l, language_content as cl 
					 WHERE 
						cl.langcont_id=c.langcont_id AND cl.lang_id = l.lang_id AND c.ct_type='".$ct_type."'" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
				 $lang_field = $row["lang_field"];
				 $select = array('ct_id' => $row['ct_id'],
				 				 'ct_name' => $row["$lang_field"]);
            		}
					return $select;
            		}
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }
	
   function selectDepartment($dept_id=0) {
        try {
            $query = "SELECT * 
					 FROM 
					 	department as d, language_available as l, language_content as cl 
					 WHERE 
						cl.langcont_id=d.langcont_id AND cl.lang_id = l.lang_id" ;
			if($dep_id != 0){
			$query .= " AND dept_id='".$dept_id."'";
			}
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
				 $lang_field = $row["lang_field"];
				 $select = array('reg_dept' => $row['dept_id'],
				 				 'dept_name' => $row["$lang_field"]);
            		}
					return $select;
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }


   function selectClinic($clinic_id = 0) {
        try {
            $query = "SELECT * 
					 FROM 
					 	clinic as c, language_available as l, language_content as cl 
					 WHERE 
						cl.langcont_id=c.langcont_id AND cl.lang_id = l.lang_id" ;
			if($clinic_id != 0){
			$query .= " WHERE clinic_id='".$clinic_id."'";
			}
			
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
				 $lang_field = $row["lang_field"];
				 $select = array('reg_clinic' => $row['clinic_id'],
				 				 'clinic_name' => $row["$lang_field"]);
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }

 
   function selectJqueryDepartAndClinic() {
        try {
            $query = "SELECT * 
					 FROM 
					 	department as d, language_available as l, language_content as cl 
					 WHERE 
						cl.langcont_id=d.langcont_id AND cl.lang_id = l.lang_id" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     $i = 0;
			     while ($row = mysql_fetch_array($res)) {
				 $lang_field = $row["lang_field"];
				 $deptid = $row['dept_id'];
				 if($i==0){
                  $selectoptions = '"'.$row["$lang_field"].'":{
				  					"key":'.$row['dept_id'].',
									"values" : {';
				  }else{
                  $selectoptions = ',"'.$row["$lang_field"].'":{
				  					"key":'.$row['dept_id'].',
									"values" : {';				   
				   } 
					$i +=1;				
				$query2 = "SELECT * 
					 FROM 
					 	clinic as c, language_available as l, language_content as cl 
					 WHERE 
						cl.langcont_id=c.langcont_id AND cl.lang_id = l.lang_id AND c.dept_id='$deptid'" ;

            $res2 = $this->conn->execute($query2);
				$q = 0;
			     while ($row2 = mysql_fetch_array($res2)) {
				 $lang_field2 = $row2["lang_field"];
				 
				 if($q==0){
                  $selectoptions .= '"'.$row2["$lang_field2"].'" :'.$row2['clinic_id'];
				  }else{
                  $selectoptions .= ',"'.$row2["$lang_field2"].'" :'.$row2['clinic_id'];
				  }
				  $q +=1;

            		}//end second while loop
			
				$selectoptions .= ' 	}
             						 }';
			echo $selectoptions;
			
            		}//end first while loop
					
            }//end if
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }


    function selectReferrer() {
        try {
            $query = "SELECT * 
					 FROM 
					 	patient_referrer as p, language_available as l, language_content as cl 
					 WHERE 
						cl.langcont_id=p.langcont_id AND cl.lang_id = l.lang_id" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
				 $lang_field = $row["lang_field"];
 				 $select = array('refferer_id' => $row['refferer_id'],
				 				 'refferer_name' => $row["$lang_field"]);
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }


  function selectConsultant() {
        try {
            $query = "SELECT * 
					 FROM 
					 	consultant as c, staff as s 
					 WHERE 
						c.staff_employee_id=s.staff_employee_id" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
				
                  echo '<option value='.$row['consultant_id'].'>'.$row["staff_surname"].' '.$row["staff_othernames"].'</option>';
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }
	


	
	
	
	
}

?>
