<?php
/**
 * Description of TreatmentQuestions
 *
 * @author Tunde
 */
class TreatmentQuestions {
    /**
     * @var object An object of the DBConf class
     */
	protected $conn;




/**
 * Class Constructor
 * @param string The name of the column in the language table that corresponds to the currently-selected language.
 * @param int The clinic ID of the clinic under consideration
 */
	function __construct (){
		$this->conn = new DBConf();
	}   //END __construct()
    
    
    
    
    public function getTreatmentQuestions($field_code, $parent_id, $include_general_questions = true){
        $parent_id = (int)$parent_id;
        $clinic_id = isset($_SESSION[session_id() . 'clinicID']) ? $_SESSION[session_id() . 'clinicID'] : 0;
        $clinic_extra = " OR clinic_id = '0' ";
        $query = "SELECT tq.* 
                    FROM treatment_questions tq
                    INNER JOIN treatment_fields tf ON tq.treatfield_id = tf.treatfield_id
                    WHERE tf.treatfield_code = '$field_code'
                        AND quest_parent_id = '$parent_id'
                        AND (clinic_id = '$clinic_id' $clinic_extra)";
		$result = $this->conn->run($query);
        $retVal = array();
		if ($this->conn->hasRows($result)){
			while ($row = mysql_fetch_array ($result, MYSQL_ASSOC)){
                $retVal[] = $row;
            }
		}
		return $retVal;
    }   //END getTreatmentQuestions()
    
    
    
    public function doDiagnosisSearch($search_text){
        $search_text = admin_Tools::doEscape($search_text, $this->conn);
        $retVal = array();
        $selected_ids = '';
        
        //Firstly, get entries beginning with the search text.
        $query = "SELECT * FROM diagnosis
                  WHERE diagnosis_name LIKE '$search_text%'
                  ORDER BY diagnosis_name
                  LIMIT 0,20";
		$result = $this->conn->run($query);
		if ($this->conn->hasRows($result)){
			while ($row = mysql_fetch_array ($result, MYSQL_ASSOC)){
                $retVal[] = $row;
                $selected_ids .= $row['diagnosis_id'] . ',';
            }
		}
        $final_selected_ids = trim($selected_ids, ',');
        
        //Now, get those with the search text any where in it
        $extra_where = !empty($selected_ids) ? " AND diagnosis_id NOT IN ($final_selected_ids) " : '';
        $query2 = "SELECT * FROM diagnosis
                  WHERE diagnosis_name LIKE '%$search_text%' #$extra_where
                  ORDER BY diagnosis_name
                  LIMIT 0,20";
		$result2 = $this->conn->run($query2);
		if ($this->conn->hasRows($result)){
			while ($row = mysql_fetch_array ($result2, MYSQL_ASSOC)){
                $retVal[] = $row;
            }
		}
        
		return $retVal;
    }   //END doDiagnosisSearch()
    
    
    
    public function addTreatmentQuestion($question, $treatment_field, $clinic, $question_parent_id = 0, $is_question = 1){
        $question = admin_Tools::doEscape($question, $this->conn);
        $question_parent_id = (int)$question_parent_id;
        $treatment_field = (int)$treatment_field;
        $clinic = (int)$clinic;
        $is_question = $is_question == 1 ? 1 : 0;
        $query = "INSERT INTO treatment_questions
                    SET clinic_id = '$clinic',
                        treatfield_id = '$treatment_field',
                        quest_parent_id = '$question_parent_id',
                        user_id = '" . $_SESSION[session_id() . 'userID'] . "',
                        quest_question = '$question',
                        quest_isquestion = '$is_question'";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $ret_val = mysql_insert_id($this->conn->getConnectionID());
        } else {
            $ret_val = 0;
        }
        return $ret_val;
    }   //END addTreatmentQuestion()
    
    
    
    public function deleteTreatmentQuestion($question_id){
        $question_id = (int)$question_id;
        $clinic_id = isset($_SESSION[session_id() . 'clinicID']) ? $_SESSION[session_id() . 'clinicID'] : 0;
        $query = "DELETE FROM treatment_questions
                    WHERE quest_id = '$question_id'
                        AND clinic_id IN ('0', '$clinic_id')";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $ret_val = 1;
        } else {
            $ret_val = 0;
        }
        return $ret_val;
    }   //END deleteTreatmentQuestion()
    
    
    
    
    public function updateTreatmentQuestion($question_id, $question, $treatment_field, $clinic, $question_parent_id = 0, $is_question = 1){
        $question = admin_Tools::doEscape($question, $this->conn);
        $question_parent_id = (int)$question_parent_id;
        $treatment_field = (int)$treatment_field;
        $clinic = (int)$clinic;
        $user_clinic_id = isset($_SESSION[session_id() . 'clinicID']) ? $_SESSION[session_id() . 'clinicID'] : 0;
        $question_id = (int)$question_id;
        $is_question = $is_question == 1 ? 1 : 0;
        $query = "UPDATE treatment_questions
                    SET clinic_id = '$clinic',
                        treatfield_id = '$treatment_field',
                        quest_parent_id = '$question_parent_id',
                        quest_question = '$question',
                        quest_isquestion = '$is_question'
                    WHERE quest_id = '$question_id'
                        AND clinic_id IN ('0', '$user_clinic_id')";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $ret_val = 1;
        } else {
            $ret_val = 0;
        }
        return $ret_val;
    }   //END updateTreatmentQuestion()
    
    
    
    
}   //END class