<?php
/**
 * Description of patTreatmentSheetUpload
 *
 * @author Tunde
 */
class patTreatmentSheetUpload {

    function __construct(){

    }

    /**
     * The contents of this function should actually be in the block case 'pat_treatment', but was moved to this function
     * so that it can be called from the FTP Treatment Upload Sheet Module
     */
    public function doPatTreatment($ftp_mode = FALSE){
//        die ("<p>POST (Inside switch): <br /><pre>" . print_r($_POST, true) . "</pre></p>");
			//die ("<p>FILES: <br /><pre>" . print_r($_FILES, true) . "</pre></p>");
//            echo '<p>About to start inserting data in patTreatmentSheetUpload::doPatTreatment</p>';
            $treats = new patTreatment;
            $treats->treat = $_POST['treat'];
            $treats->hist = $_POST['hist'];
            $treats->lab = $_POST['lab'];
            $treats->pres = $_POST['pres'];
            $treats->treatment = $_POST['treatment'];

            $casenote_id = $treats->insertCasenote();

            //***Check length before insertion

            //*BEGIN HISTORY
            $hcheck = "";
            foreach($treats->hist as $value){
                $hcheck .= str_replace (" ", "", $value);
            }
            $hlenght = strlen($hcheck);
            if($hlenght != 0){
                $treats->insertHistory($casenote_id);
            }
            //*END HISTORY


            //*BEGIN LAB

            //Record the casenote_id against any test request so as to be able to retrieve the results later
            $labTest = new LabTestRequest();
            $patAdmID = isset($_POST["treat"]["patadm_id"]) ? (int)$_POST["treat"]["patadm_id"] : "0";
            $tests = $labTest->updateRequest($casenote_id, $patAdmID, $_SESSION[session_id() . "clinicID"]);
            /*
            $lcheck = "";
            foreach($treats->lab as $value){
                $lcheck .= str_replace (" ", "", $value);
            }
            $llenght = strlen($lcheck);

            if($llenght != 0){
                $treats->insertLabtest($casenote_id);
            }
            */
            //*END LAB

            $pcheck = "";
            foreach($treats->pres as $value){
                $pcheck .= str_replace (" ", "", $value);
            }
            $plenght = strlen($pcheck);
            if($plenght !=0){
                $alldrugs =  preg_split('/\n/', $treats->pres['desc'], -1, PREG_SPLIT_NO_EMPTY);
                foreach($alldrugs as $value){
                    $arr_drug=explode("[",$value);
                    $arr_drug[1]=substr($arr_drug[1],0,strlen($arr_drug[1])-2);
                    $drugname = $arr_drug[0];
                    $drugdosage = $arr_drug[1];
                    $treats->insertPrescription($casenote_id, $drugname, $drugdosage);
                }
            }

			$tcheck = "";
			foreach($treats->treatment as $value){
                $tcheck .= str_replace (" ", "", $value);
			}
			$tlenght = strlen($tcheck);

			if($tlenght !=0){
                $treat_id = $treats->insertTreatment($casenote_id);
			}
			$treats->updateAppointmentStatus($treats->treat['app_id'], $treats->treat['patadm_id']);
			$app_id = $treats->treat['app_id'];

			//Handle uploaded case note files
			if (isset($_FILES["docs"]["error"])){
				$docsCount = count($_FILES["docs"]["error"]);
				for ($counter = 0; $counter < $docsCount; $counter++){
					if ($_FILES["docs"]["error"][$counter] == 0){
						//Resize the main file
						$fileName = $app_id . "_" . $_SESSION[session_id() . "userID"] . "_" . ($counter + 1);
						$newFile = "modules/patient_care/casenotes/$fileName";
						$thumbFile = "modules/patient_care/casenotes/thumbs/$fileName";
						$uploadHandler = new FileUploader($_FILES["docs"]["tmp_name"][$counter], $newFile);
						$uploadHandler->saveImage($_FILES["docs"]["type"][$counter]);
						$uploadHandler->logCaseNote($app_id, $_POST["docsname"][$counter], $fileName . "." . $uploadHandler->getImageExtension($_FILES["docs"]["type"][$counter]));

						//Create a thumbnail also
						$uploadHandler->newImageLoc = $thumbFile;
						$uploadHandler->saveImage($_FILES["docs"]["type"][$counter], true, 100, 100);
					}
				}
			}


            $custom_entries = isset($_POST['formbuilder_custom']) ? $_POST['formbuilder_custom'] : '';
            if (!empty($custom_entries)){
                $fBuilder = new FormBuilder();
                $fBuilder->saveFormEntry($custom_entries, $_SESSION[session_id() . "clinicID"], 1, $casenote_id, $_SESSION[session_id() . 'userID']);
            }


            if (!$ftp_mode){
                header("Location:index.php?p=pat_viewtreatment&m=patient_care&a=".$app_id."&b=".$casenote_id);
            }
    }   //END doPatTreatment()
}   //END class

?>
