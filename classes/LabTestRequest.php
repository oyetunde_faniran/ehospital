<?php
/**
 * Contains methods that save / retrieve the details of all lab test requests sent to the laboratory module by the doctor.
 * 
 * @author Faniran, Oyetunde
 *
 * @package Patient_Care
 */

/**
 * Contains methods that save / retrieve the details of all lab test requests sent to the laboratory module by the doctor.
 *
 * @author Faniran, Oyetunde
 *
 * @package Patient_Care
 */
class LabTestRequest{
    /**
     * @var object An object of the DBConf class
     */
	protected $conn;


/**
 * Class Constructor
 */
	function __construct(){
		$this->conn = new DBConf();
	}   //END __construct()
	
	
/**
 * Saves the details of a test request instance
 *
 * @param int $testID:		The unique ID of this test request instance in the laboratory module
 * @param string $hospitalNo:	The hospital number of the patient under consideration
 * @param int $patAdmID:		The patadm_id for this medical transaction for this patient
 * @param string $testName:		The name of the test that is being requested
 */
	public function saveRequest($testID, $hospitalNo, $patAdmID, $clinicID, $testName, $serviceID){
		$testID = admin_Tools::doEscape($testID, $this->conn);
		$hospitalNo = admin_Tools::doEscape($hospitalNo, $this->conn);
		$patAdmID = admin_Tools::doEscape($patAdmID, $this->conn);
		$clinicID = admin_Tools::doEscape($clinicID, $this->conn);
		$testName = admin_Tools::doEscape($testName, $this->conn);
        $serviceID = admin_Tools::doEscape($serviceID, $this->conn);
//		$query = "INSERT INTO casenote_labtestrequest(reg_hospital_no, patadm_id, clinic_id, caselabreq_testid, caselabreq_testname, caselabreq_datetime, service_id)
//					VALUES ('$hospitalNo', '$patAdmID', '$clinicID', '$testID', '$testName', NOW(), '$serviceID')";
		$query = "INSERT INTO casenote_labtest(reg_hospital_no, patadm_id, clinic_id, caselabreq_testid, caselabreq_testname, caselabreq_datetime, service_id)
					VALUES ('$hospitalNo', '$patAdmID', '$clinicID', '$testID', '$testName', NOW(), '$serviceID')";
		$result = $this->conn->execute($query);
		$insertID = mysql_insert_id($this->conn->dbh);
		return $insertID;
		//die ("TESTID: $testID -- HOSP NO: $hospitalNo -- PATADMID: $patAdmID -- TEST NAME: $testName");
	}   //END saveRequest()




/**
 * Updates test request records by saving the attached casenote_id against these records
 *
 * @param int $caseNoteID:          The id of the casenote table row the affected test requests are to be attached
 * @param int $patAdmID             The row ID in the patient_admission table (case ID) for this treatment instance
 * @param int $clinicID             The ID of the clinic to which the patient is attached
 */
	public function updateRequest($caseNoteID, $patAdmID, $clinicID){
//		$caseNoteID = admin_Tools::doEscape($caseNoteID, $this->conn);
//		$patAdmID = admin_Tools::doEscape($patAdmID, $this->conn);
//		$clinicID = admin_Tools::doEscape($clinicID, $this->conn);
		$query = "UPDATE casenote_labtest
					SET casenote_id = '$caseNoteID'
					WHERE patadm_id = '$patAdmID'
						AND clinic_id = '$clinicID'
						AND CURDATE() = DATE(caselabreq_datetime)";// die ($query);
		$result = $this->conn->execute($query);
	}   //END updateRequest()



/**
 * Gets all the lab tests attached to a particular case note ID
 * @param int $cNoteID      Case Note ID
 * @return string           The generated HTML table that contains the details of the tests with clickable links to get full details
 */
	public function getAllLabTests($cNoteID){
		$cNoteID = admin_Tools::doEscape($cNoteID, $this->conn);
		$query = "SELECT caselabreq_testid, caselabreq_testname, reg_hospital_no 
					FROM casenote_labtest
					WHERE casenote_id = '$cNoteID'"; //die ($query);
		$result = $this->conn->execute ($query);
		if ($this->conn->hasRows ($result)){
			$retVal = "<p>Please, click the name of a laboratory test below to see the result.</p>
						<table cellpadding='3' cellspacing='3'>";
			while ($row = mysql_fetch_array ($result, MYSQL_ASSOC)){
				$retVal .= "<tr>
								<td>
									<a target=\"_blank\" href=\"index.php?p=doc_details&m=lab&transid={$row['caselabreq_testid']}\">" . stripslashes($row["caselabreq_testname"]) . "</a>
								</td>
							</tr>";
			}
			$retVal .= "</table>";
		} else $retVal = "Sorry! The laboratory test result you are trying to view was not found.";
		return $retVal;
	}   //END getAllLabTests()


	
}	//END class
?>