<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormBuilder
 *
 * @author Tunde
 */
class FormBuilder {

    public $errorMsg;

    function __construct(){
        $this->conn = new DBConf();
        $this->errorMsg = '';
    }


    public function getFormElements(){
        $query = "SELECT * FROM customfields_type"
                . " WHERE custype_enabled = '1'";
        $result = $this->conn->runQuery($query);
        $ret_val = array();
        if ($this->conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $ret_val[] = $row;
            }
        }
        return $ret_val;
    }   //END getFormElements()



    public function saveFormElement($user_id, $field_name, $field_type, $clinic_id, $page_id, $options_array = array()){
//        echo ("$user_id, $field_name, $field_type, $clinic_id, $page_id, $options_array");
//        die('<pre>OPTIONS ARRAY:' . print_r($options_array, true));
        try {
            //Confirm that at least 2 options were supplied for field types DROP-DOWN & MULTI-SELECT DROP-DOWN
            $option_count = count($options_array);

            //Start Transaction
            $this->conn->startTransaction();

            //Get the max order
            $order = ($this->getMaxClinicFormElementOrder($clinic_id, $page_id)) + 1;

            //Insert the main custom field record
            $query = "INSERT INTO customfields\n"
                    . " SET clinic_id = '$clinic_id',\n"
                    . "     custype_id = '$field_type',\n"
                    . "     custpage_id = '$page_id',\n"
                    . "     customfield_order = '$order',\n"
                    . "     customfield_name = '$field_name',\n"
                    . "     customfield_dateadded = NOW(),\n"
                    . "     user_id = '$user_id'";
            $result = $this->conn->runQuery($query);

            //Confirm that the insertion was successful
            if (!$this->conn->hasRows($result, 1)){
                throw new Exception('Unable to save custom field details.');
            }

            //Insert options if available
            if (($field_type == 3 || $field_type == 4) && $option_count > 0){
                $custom_id = mysql_insert_id(); //Get the custom field ID
                $this->saveFormElementOptions($custom_id, $options_array, $user_id);
            }

            //Commit the transaction
            $this->conn->commitTransaction();

            $ret_val = true;

        } catch (Exception $e) {
            //Roll-back the transaction
            $this->conn->rollBackTransaction();
            $this->errorMsg = $e->getMessage();

            $ret_val = false;
        }
        return $ret_val;
    }   //END saveFormElement()




    protected function getMaxClinicFormElementOrder($clinic_id, $page_id){
        $query = "SELECT MAX(customfield_order) 'dis_order' FROM customfields"
                . " WHERE clinic_id = '$clinic_id'"
                . "     AND custpage_id = '$page_id'";
        $result = $this->conn->runQuery($query);
        if ($this->conn->hasRows($result)){
            $row = mysql_fetch_assoc($result);
            $ret_val = $row['dis_order'];
        } else {
            $ret_val = 0;
        }
        return $ret_val;
    }   //END getMaxClinicFormElementOrder()




    protected function saveFormElementOptions($custom_id, $options_array, $user_id){
        if (!is_array($options_array)){
            throw new Exception('At least 2 options are required for the selected form field type. (2)');
        }
        $option_query = "INSERT INTO customfields_options (customfield_id, customfieldopt_name, customfieldopt_dateadded, user_id) \nVALUES";
        $good_option_count = 0;
        foreach ($options_array as $option){
            $option = trim($option);
            if (!empty($option)){
                $option_query .= "\n('$custom_id', '$option', NOW(), '$user_id'),";
                $good_option_count++;
            }
        }
        $final_option_query = trim($option_query, ',');
        if ($good_option_count < 2){
            throw new Exception('At least 2 options are required for the selected form field type. (3)');
        }
        $option_result = $this->conn->runQuery($final_option_query);
        if (!$this->conn->hasRows($option_result)){
            throw new Exception('Unable to save custom field options....');
        }
    }   //END saveFormElementOptions()




    public function getClinicFormElements($clinic_id, $page_id, $enabled_only = true){
        $where_more = $enabled_only ? " AND customfield_enabled = '1' " : '';
        $query = "SELECT c.*, ct.custype_name, GROUP_CONCAT(copt.customfieldopt_name ORDER BY copt.customfieldopt_name) 'options' FROM customfields c\n"
                . " INNER JOIN customfields_type ct ON c.custype_id = ct.custype_id\n"
                . " LEFT JOIN customfields_options copt ON c.customfield_id = copt.customfield_id\n"
                . " WHERE clinic_id = '$clinic_id'"
                . "     AND custpage_id = '$page_id'" . $where_more
                . " \nGROUP BY c.customfield_id\n"
                . " ORDER BY customfield_order, customfield_name\n";
        $result = $this->conn->runQuery($query);
        $ret_val = array();
        if ($this->conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $ret_val[$row['customfield_id']] = $row;
            }
        }
//        echo('<pre>' . print_r($ret_val, true));
        return $ret_val;
    }   //END getFormElements()




    public function getForm($clinic_id, $page_id, $enabled_only = true, $table_enclosure = true, $form_deets = array()){
        if (empty($form_deets)){
            $form_deets = $this->getClinicFormElements($clinic_id, $page_id, $enabled_only);
        }
        try{
            if (empty($form_deets)){
                throw new Exception('No custom form elements added yet.');
            }
            $ret_val = $table_enclosure ? '<table cellpadding="3" cellspacing="3" border="0" width="100%">' : '';
            foreach ($form_deets as $elem){
                if (!empty($elem['options'])){
                    $options = '';
                    $options_array = explode(',', $elem['options']);
                    foreach ($options_array as $option){
                        $options .= '<option value="' . $option . '">' . $option . '</option>';
                    }
                }
                if ($elem['custype_id'] == 4){
                    $name_and_id = ' name="formbuilder_custom[' . $elem['customfield_id'] . '][]" id="formbuilder_custom_' . $elem['customfield_id'] . '" ';
                } else {
                    $name_and_id = ' name="formbuilder_custom[' . $elem['customfield_id'] . ']" id="formbuilder_custom_' . $elem['customfield_id'] . '" ';
                }
                switch ($elem['custype_id']){
                    case 2: //Textarea
                        $dis_element = '<textarea rows="7" cols="40" ' . $name_and_id . '></textarea>';
                        break;
                    case 3: //Single Select Drop-Down
                        $dis_element = '<select style="width: auto;" ' . $name_and_id . '>
                                            <option value="0">--Select--</option>
                                            ' . $options . '
                                        </select>';
                        break;
                    case 4: //Multi-Select Drop-Down
                        $dis_element = '<select style="width: auto;" multiple="multiple" ' . $name_and_id . '>
                                            <option>--Select--</option>
                                            ' . $options . '
                                        </select>';
                        break;
                    /*case 8: //Optical Measurement. This requires 3 textboxes to take measurements for the left eye, the right eye and both eyes.
                    case 7: //Dental Quadrant
                        $dis_sub_name = 'formbuilder_custom[' . $elem['customfield_id'] . '][%s][%s]';
                        $dis_element = '<table style="width: auto;" cellspacing="3" cellpadding="3">
                                            <tr>
                                                <td>
                                                    <table style="width: auto;" cellspacing="3" cellpadding="3">
                                                        <tr>
                                                            <td colspan="8"><strong>Upper Right</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>18</td> <td>17</td> <td>16</td> <td>15</td>
                                                            <td>14</td> <td>13</td> <td>12</td> <td>11</td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" name="' . sprintf($dis_sub_name, '') . '" /></td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td>
                                                </td>
                                            </tr>
                                        </table>';
                        break;*/
                    case 1: //Textbox
                    default:
                        $dis_element = '<input style="width: 200px;" type="text" ' . $name_and_id . ' />';
                        break;
                }   //END switch()

                if ($elem['custype_id'] == 5){  //Section Title
                    $ret_val .= '<tr>
                                     <td colspan="2" class="form-builder-section-title">' . strtoupper($elem['customfield_name']) . '</td>
                                 </tr>';
                } elseif ($elem['custype_id'] == 6){  //Instruction (Ordinary text)
                    $ret_val .= '<tr>
                                     <td colspan="2">' . strtoupper($elem['customfield_name']) . '</td>
                                 </tr>';
                } else {
                    $ret_val .= '<tr>
                                     <td width="30%" valign="top"><strong>' . strtoupper($elem['customfield_name']) . ':</strong></td>
                                     <td>' . $dis_element . '</td>
                                 </tr>';
                }
            }
            $ret_val .= $table_enclosure ? '</table>' : '';
        } catch (Exception $ex) {
            $ret_val = $ex->getMessage();
        }
        return $ret_val;
    }   //END getForm()




    public function saveNewOrder($order_string){
        $order_array = explode(',', $order_string);
        try {
            if (!count($order_array)){
                throw new Exception('No order to save.');
            }

            $this->conn->startTransaction();
            foreach ($order_array as $order => $customfield_id){
                $query = 'UPDATE customfields'
                        . " SET customfield_order = '$order'"
                        . " WHERE customfield_id = '$customfield_id'";
                $this->conn->runQuery($query);
            }

            $this->conn->commitTransaction();
            $ret_val = true;
        } catch (Exception $ex) {
            $this->conn->rollBackTransaction();
            $ret_val = false;
        }
        return $ret_val;
    }   //END saveNewOrder()



    public function deleteFormElement($customfield_id){
        $customfield_id = (int)$customfield_id;
        $query = "DELETE FROM customfields"
                . " WHERE customfield_id = '$customfield_id'";
        $query2 = "DELETE FROM customfields_options"
                . " WHERE customfield_id = '$customfield_id'";
        $this->conn->runQuery($query);
        $this->conn->runQuery($query2);
    }   //END deleteFormElement()




    public function saveFormEntry($field_array, $clinic_id, $page_id, $unique_id, $user_id){
        $elems = $this->getClinicFormElements($clinic_id, $page_id);
        $ret_val = true;
        foreach ($field_array as $customfield_id => $value){
            $final_value = is_array($value) ? implode("\n", $value) : $value;   //For multi-select drop-down, the value would be an array
            $customfield_name = isset($elems[$customfield_id]['customfield_name']) ? $elems[$customfield_id]['customfield_name'] : '';
            $query = "INSERT INTO customfields_entry
                        SET customentry_casenote_vital_id = '$unique_id',
                            customfield_id = '$customfield_id',
                            custpage_id = '$page_id',
                            customfield_name = '$customfield_name',
                            customentry_value = '$final_value',
                            user_id = '$user_id'";
            $this->conn->runQuery($query);
        }
        return $ret_val;
    }   //END saveFormEntry()


    public function getFormEntries($form_unique_id, $custompage_id){
        $form_unique_id = (int)$form_unique_id;
//        $query = "SELECT * FROM customfields_entry ce
//                    LEFT JOIN customfields c ON ce.customfield_id = c.customfield_id
//                    WHERE customentry_casenote_vital_id = '$form_unique_id'";
        $query = "SELECT ce.*, c.*, GROUP_CONCAT(copt.customfieldopt_name) 'options'
                    FROM customfields_entry ce
                        LEFT JOIN customfields c ON ce.customfield_id = c.customfield_id
                        LEFT JOIN customfields_options copt ON c.customfield_id = copt.customfield_id
                    WHERE customentry_casenote_vital_id = '$form_unique_id'
                        AND ce.custpage_id = '$custompage_id'
                    GROUP BY c.customfield_id
                    ORDER BY c.customfield_order, c.customfield_name";
//        die("<pre>$query");
        $result = $this->conn->runQuery($query);
        $ret_val = $this->conn->getResultArray($result);
        return $ret_val;
    }




}   //END class



/*
 *******TO SHOW FORM ENTRIES******
//Get custom fields if any
        $fBuilder = new FormBuilder();
        $entries = $fBuilder->getFormEntries($vitalSignsID, 1);
        $custom_entries = '';
//        die('<pre>' . print_r($entries, true));
        if (!empty($entries)){
            $row_style1 = 'tr-row';
            $row_style2 = 'tr-row2';
            $change_style = true;
            foreach ($entries as $entry){
                $row_style = $change_style ? $row_style1 : $row_style2;
                $change_style = !$change_style;
                $custom_entries .= "<tr class=\"$row_style\">
                                        <td><strong>" . strtoupper($entry['customfield_name']) . "</strong>:</td>
                                        <td>" . nl2br($entry['customentry_value']) . "</td>
                                    </tr>";
            }
        }
 */