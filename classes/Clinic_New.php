<?php
/**
 * Contains diverse methods that can be used from different classes in the System_Administration package (and even other packages)
 * 
 * @author Faniran, Oyetunde
 *
 * @package Patient_Care
 */

/**
 * Contains diverse methods that can be used from different classes in the System_Administration package (and even other packages)
 *
 * @author Faniran, Oyetunde
 *
 * @package Patient_Care
 */
class Clinic_New {
    /**
     * @var object An object of the DBConf class
     */
	protected $conn;

    /**
     * @var int Stores the clinic ID of the clinic under consideration
     */
	protected $clinicID;

    /**
     * @var string Stores the name of the column in the language table that corresponds to the currently-selected language.
     */
	protected $curLangField;
	



/**
 * Class Constructor
 * @param string The name of the column in the language table that corresponds to the currently-selected language.
 * @param int The clinic ID of the clinic under consideration
 */
	function __construct ($curLangField = "lang1", $clinicID = 0){
		$this->clinicID = (int)$clinicID;
		$this->curLangField = $curLangField;
		$this->conn = new DBConf();
	}   //END __construct()
	
	



/**
 * Gets the list of all clinics alongside their IDs for display in a drop-down box
 * @param int $selected The ID of the currently selected clinic
 * @return string       The generated HTML that contains all the clinics
 */
	public function getAllClinics4DD($selected = 0){
		$query = "SELECT c.clinic_id, lc.lang1 'clinicname' 
					FROM clinic c INNER JOIN language_content lc INNER JOIN department d
					ON c.langcont_id = lc.langcont_id AND c.dept_id = d.dept_id
					WHERE d.dept_isclinical = '1'
					ORDER BY clinicname";
		$result = $this->conn->execute ($query);
		$retVal = "";
		if ($this->conn->hasRows($result)){
			while ($row = mysql_fetch_array ($result, MYSQL_ASSOC)){
				if ($row["clinic_id"] == $selected)
					$retVal .= "<option value=\"{$row['clinic_id']}\" selected=\"selected\">" . stripslashes ($row["clinicname"]) . "</option>";
				else $retVal .= "<option value=\"{$row['clinic_id']}\">" . stripslashes ($row["clinicname"]) . "</option>";
			}	//END while
		}	//END if
		return $retVal;
	}   //END getAllClinics4DD()
	
	



/**
 * Gets the name of a clinic from the ID
 * @param int $clinicID     The clinic ID of the clinic under consideration.
 * @return string           The name of the clinic whose ID was supplied
 */
	public function getClinicName($clinicID){
		$clinicID = (int)$clinicID;
		$query = "SELECT lc.lang1 'clinicname' 
					FROM clinic c INNER JOIN language_content lc
					ON c.langcont_id = lc.langcont_id
					WHERE c.clinic_id = '$clinicID'";
		$result = $this->conn->execute ($query);
		if ($this->conn->hasRows($result, 1)){
			$retVal = mysql_fetch_array ($result, MYSQL_ASSOC);
			$retVal = $retVal["clinicname"];
		} else $retVal = "";
		return $retVal;
	}   //END getClinicName()
	
    

    /**
     * Gets the maximum number of appointments that can be scheduled for a day in a clinic
     * @return int Returns the maximum number of appointments that can be scheduled for a day in a clinic
     */
    public function getMaxAppointment(){
        $query = "SELECT clinichrs_max_appointment 'max' FROM clinic_hours
                    WHERE clinic_id = '" . $this->clinicID . "'";
        $result = $this->conn->execute ($query);
		if ($this->conn->hasRows($result)){
			$retVal = mysql_fetch_array ($result, MYSQL_ASSOC);
			$retVal = $retVal["max"];
		} else $retVal = 0;
        return $retVal;
    }   //getMaxAppointment



	
}	//END class
?>