<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventoryCreationFormProcessingClass
 *
 * @author upperlink
 */
class inventory_item_class extends inventoryGeneralClass{
    public $conn;
    public $itemID = 0;
    
//    public function __construct() {
//        $conn = new DBConf();
//        $this->conn = $conn->mysqliConnect();
//        //$this->conn=new mysqli('localhost','root','','mediluth_skye');
//    }
    
   
    public function updateCategories($item,$cats){
        global $inventory_categories_successfully_updated;
        $report=FALSE;
        if(!empty($cats) && is_array($cats)){
            $error="";
            $this->conn->autocommit(FALSE);
            $clearCats=$this->conn->query("delete from inventory_item_categories where inventory_item_id='".$item."'");
            if(!$clearCats){
                $error.="Unable to clear old categorization";
            }
            foreach($cats as $cat){
                $updated=$this->conn->query("insert into inventory_item_categories (inventory_item_id,inventory_category_id) values('".$item."','".$cat."')");
                if(!$updated){
                    $error.="Unable to update category ".$cat."<br />";
                }
             
            }
               if($error!=""){
                    $this->conn->rollback();
                    $report=$error;
                }else{
                    $this->conn->commit();
                    $report=$inventory_categories_successfully_updated;
                }
        }
        return $report;
    }
    
    public function updateItemProperties($itemId,$commercialName, $medicalName, $form, $strength, $presentation, $manufacturer, $nafdac, $sideeffects){
        global $inventory_dbconnnect_error;
        global $inventory_unable_to_update_item_extra_info;
        global $inventory_unable_to_update_item_basic_info;
        global $inventory_item_successfully_updated;
        
        $this->conn->autocommit(FALSE);
        $error="";
        $updateQuery=$this->conn->query("update drug set drug_name='".$commercialName."', drug_desc='".$medicalName."',drug_manufacturer='".$manufacturer."',drug_dosageform='".$form."',drug_strength='".$strength."',drug_presentation='".$presentation."' where drug_id='".$itemId."'");
        if(!$updateQuery){
            $error=$inventory_unable_to_update_item_basic_info;
        }else{
            $xtraUpdate=$this->conn->query("update inventory_item_extras set inventory_item_nafdac='".$nafdac."', inventory_item_side_effects='".$sideeffects."' where inventory_item_id='".$itemId."'");
            if(!$xtraUpdate){
                $error=$inventory_unable_to_update_item_extra_info;
            }
            if($error!=""){
                $this->conn->rollback();
                $report=$error;
            }else{
                $this->conn->commit();
                $report=$inventory_item_successfully_updated;
            }
        }
        return $report;
    }
    
    
    public function addNewItem($commercialName, $medicalName, $reorderLevel, $categories, $form, $strength, $presentation, $manufacturer, $nafdac, $locations/*, $sellingprice, $markup, $pricetouse*/, $sideeffects) {
        global $report;
        global $inventoryItemExtraCreationFailureNews;
        global $inventoryItemCreationSuccessNews;
        global $inventoryItemCreationFailureNews;
        global $inventory_no_itemName_error_msg;
        global $inventory_no_medicalName_error_msg;
        global $inventory_incomplete_field_msg;

        if (empty($commercialName)){
            $report=$inventory_no_itemName_error_msg;
            return $report;
        } 
        
        if(empty($medicalName)){
            $report=$inventory_no_medicalName_error_msg;
            return $report;
        }
        
        if(empty($categories)) {
            $report=$inventory_noCategory_msg;
            return $report;
            }
        
        $createItem = $this->conn->query("insert into drug (drug_name, drug_desc,drug_reorderlevel,drug_manufacturer, drug_dosageform,drug_strength,drug_presentation) values ('".$commercialName."','".$medicalName."','".$reorderLevel."','".$manufacturer."','".$form."','".$strength."','".$presentation."')");
        
        if(!$createItem){
            $report=$inventoryItemCreationFailureNews;
            return $report;
        }
        
        $this->itemID = $id = $this->conn->insert_id;
        
        $disSQL = "INSERT INTO inventory_item_extras(inventory_item_id,inventory_item_nafdac,inventory_item_date_created, inventory_item_side_effects, user_id)
                    VALUES('" . $id . "', '" . $nafdac . "', CURRENT_TIMESTAMP, '$sideeffects', '" . $_SESSION[session_id() . "userID"] . "')";
        $createItemExtra=$this->conn->query($disSQL);
        //$createItemExtra=$this->conn->query("insert into inventory_item_extras(inventory_item_id,inventory_item_nafdac,inventory_item_date_created,user_id)values('".$id."','".$nafdac."',CURRENT_TIMESTAMP,'".$_SESSION[session_id() . "userID"]."')");
        
        if (!$createItemExtra) {
            $report=$inventoryItemExtraCreationFailureNews;
            return $report;
        } 

        
        //Insert pricing details
//        $sellingprice = (double)$sellingprice;
//        $markup = (double)$markup;
//        $pricetouse = (int)$pricetouse;
//        $disSQL = "INSERT INTO inventory_item_prices
//                    SET drug_id = '$id',
//                        inventory_location_id = '0',
//                        inventory_item_prices_sellingprice = '$sellingprice',
//                        inventory_item_prices_markup = '$markup',
//                        inventory_item_prices_which_to_use = '$pricetouse',
//                        inventory_item_dateupdated = NOW(),
//                        user_id = '" . $_SESSION[session_id() . "userID"] . "'";
//        $this->conn->query($disSQL);
        
        
  if(is_array($categories) && (!empty($categories))){
      foreach ($categories as $catid){
          $categorized=$this->conn->query("insert into inventory_item_categories (inventory_item_id,inventory_category_id)values('".$id."','".$catid."')");
          if(!$categorized){
              $report=$inventory_categorizing_failure_msg;
              return $report;
          }
      }
  }      
      
//            if(!empty($locations)){
//                foreach ($locations as $location=>$qty){
//                    $qtyQuery=$this->conn->query("insert into inventory_item_qties_by_location (inventory_location_id,inventory_item_id,inventory_item_qty_in_stock,inventory_stock_last_updated) values ('".$location."','".$id."','".$qty."',CURRENT_TIMESTAMP)");
//                    if(!$qtyQuery){
//                        $report=$inventoy_problem_inserting_begining_quantities;
//                        return $report;
//                    }
//                }
//            }
  
            return $inventoryItemCreationSuccessNews;
     }
    
     public function updateQties($qty){
         global $inventory_unit_quantities_successfully_updated;
         global $inventory_not_successfully_updated;
         global $inventory_no_unit_qty_selected;
         
         if(!is_array($qty) ||  empty($qty)){
             $report=$inventory_no_unit_qty_selected;
         }else{
             $this->conn->autocommit(FALSE);
            $error="";
             foreach($qty as $a=>$b){
                 $updated=$this->conn->query("update inventory_unit_items set inventory_units_items_child_quantity ='".$b."' where inventory_unit_items_id='".$a."'");
                 if(!$updated){
                     $error.=$a." ".$inventory_not_successfully_updated;
                 }
                 if($error!=""){
                     $this->conn->rollback();
                     $report=$error;
                 }else{
                     $this->conn->commit();
                     $report=$inventory_unit_quantities_successfully_updated;
                 }
            }
        }
        return $report;
     }
     
     public function removeUnits($units){
         if(is_array($units)&& !empty($units)){
             $this->conn->autocommit(FALSE);
            $error="";
             foreach($units as $del){
                 $deletion=$this->conn->query("delete from inventory_unit_items where inventory_unit_items_id='".$del."'");
                 if(!$deletion){
                     $error.="Unable to delete unit number ".$del."<br />";
                 }
                 if($error!=""){
                     $this->conn->rollback();
                     $report=$error;
                 }else{
                     $this->conn->commit();
                     $report="Selected units successfully removed";
                 }
             }
         }
         return $report;
     }
     
     
     public function listItems($categoryId, $getTotalOnly = false) {
        global $report;
        global $click_to_edit;
        global $click_to_remove;
        global $inventory_item;
        global $inventory_no_item_found_msg;
        global $limitStart, $recsPerPage;
        if (!isset($categoryId) || (empty($categoryId))) {
            if ($getTotalOnly){
                $disQuery = "SELECT COUNT(*) 'totalCount' FROM drug";
            } else {
                 $disQuery = "SELECT drug.* FROM drug ORDER BY drug.drug_name
                                LIMIT $limitStart, $recsPerPage";
            }
        } else {
            if ($getTotalOnly){
                $disQuery = "SELECT COUNT(*) 'totalCount'
                                FROM drug d
                                    INNER JOIN inventory_item_categories iic ON d.drug_id = iic.inventory_item_id
                                WHERE iic.inventory_category_id = '$categoryId'";
            } else {
                $disQuery = "SELECT IFNULL(SUM(ibd.inventory_batchdetails_quantity), 0) AS bal, d.*
                                FROM drug d
                                    INNER JOIN inventory_item_categories iic ON d.drug_id = iic.inventory_item_id
                                    LEFT JOIN inventory_batch_details ibd ON d.drug_id =ibd.drug_id
                                WHERE iic.inventory_category_id = '$categoryId'
                                GROUP BY d.drug_id
                                ORDER BY d.drug_name ASC
                                LIMIT $limitStart, $recsPerPage";
            }
        }
        $items = $this->conn->query($disQuery);

        if ($getTotalOnly){
            $disCount = $items->fetch_assoc();
            $items = $disCount["totalCount"];
        } else {
            if (!$items) {
                $report = $inventory_drug_search_failure_msg;
                return $report;
            }
            if ($items->num_rows < 1) {
                $report = $inventory_no_item_found_msg;
                return $report;
            }
        }
        return $items;
    }

public function showItem($id){
    global $report;
    if(is_numeric($id)){
        $itemQuery=  $this->conn->query("SELECT drug.*,
	inventory_item_extras. *, sum(inventory_batch_details.inventory_batchdetails_quantity) as bal
						FROM 
						inventory_batch_details, 
						inventory_item_extras, 
						drug
							WHERE 
							drug.drug_id = inventory_batch_details.drug_id
							AND inventory_item_extras.inventory_item_id = drug.drug_id
							AND drug.drug_id = '".$id."'");
        if(!$itemQuery){
            $report=$inventory_dbconnnect_error;
            return $report;
        }
        
        if($itemQuery->num_rows!=1){
            $report=$inventory_nothing_found;
            return $report;
        }
        
        $result=$itemQuery->fetch_assoc();
        return $result;
    }
    $report=$inventory_invalid_operation;
    return $report;
}

public function getItemForEdit($itemId){
    global $inventory_dbconnnect_error;
    global $inventory_none_in_existence;
    $editQuery=$this->conn->query("select drug.*,inventory_item_extras.* from drug, inventory_item_extras where drug.drug_id='".$this->conn->real_escape_string($itemId)."' and drug.drug_id=inventory_item_extras.inventory_item_id");
    if(!$editQuery){
        $report=$inventory_dbconnnect_error;
    }elseif($editQuery->num_rows!='1'){
        $report=$inventory_none_in_existence;
    }else{
        $report=$editQuery->fetch_assoc();
    }
    return $report;
}

public function qtyInTransit($id){
    $qtyQuery=$this->conn->query("select IFNULL(sum(itd.inventory_item_total_qty),0) as bal2 from inventory_transfer_details itd where itd.inventory_item_id='".$id."' and itd.inventory_item_item_status='1'");
    if(!$qtyQuery){
        $report="System error! Unable to find the quantity of this item in transit";
    }else{
        $report=$qtyQuery->fetch_assoc();
    }
    return $report['bal2'];
}

public function findItemCategories($itemId){
    global $inventory_nothing_found;
    global $inventory_dbconnnect_error;
    $query=$this->conn->query("select inventory_category_id from inventory_item_categories where inventory_item_id='".$itemId."'");
    if(!$query){
        $report=$inventory_dbconnnect_error;
    }elseif($query->num_rows<1){
        $report=$inventory_nothing_found;
    }else{
        $catArray=array();
    while($row=$query->fetch_assoc()){
        $catArray[]=$row["inventory_category_id"];
        }
        $report=$catArray;
    }
    return $report;
}


public function getItemCategories($id){
    global $report;
    global $inventory_nothing_found;
    global $inventory_dbconnnect_error;
    if(is_numeric($id)){
        $categoriesResource=$this->conn->query("SELECT inventory_item_categories . * , inventory_categories.inventory_category_name
FROM inventory_item_categories, inventory_categories
WHERE inventory_categories.inventory_category_id = inventory_item_categories.inventory_category_id
AND inventory_item_categories.inventory_item_id = '".$id."'");
        if(!$categoriesResource){
            $report=$inventory_dbconnnect_error;
            return $report;
        }
        if($categoriesResource->num_rows<1){
            $report=$inventory_nothing_found;
            return $report;
        }
        while($categoriesArray=$categoriesResource->fetch_assoc()){
        echo "<a href=\"index.php?p=inventory show item category&m=pharmacy&categoryId=".$categoriesArray['inventory_category_id']."\">".$categoriesArray['inventory_category_name']."</a><br /><br />";
       
        }
     return;
    }
}

public function showItemLocationsDistribution($id){
    global $report;
    global $inventory_dbconnnect_error;
    global $inventory_none_in_existence;
    $distributionsQuery=$this->conn->query("SELECT inventory_item_qties_by_location. * , drug. * , inventory_locations. *
FROM inventory_item_qties_by_location, inventory_locations, drug
WHERE inventory_item_qties_by_location.inventory_location_id = inventory_locations.inventory_location_id
AND inventory_item_qties_by_location.inventory_item_id = drug.drug_id and inventory_item_qties_by_location.inventory_item_qty_in_stock>0
AND inventory_item_qties_by_location.inventory_item_id = '".$id."'
GROUP BY inventory_item_qties_by_location.inventory_location_id
ORDER BY inventory_locations.inventory_location_name ASC");
    
    if(!$distributionsQuery){
        $report=$inventory_dbconnnect_error;
        return $report;
    }
    
    if($distributionsQuery->num_rows<1){
        $report=$inventory_none_in_existence;
        return $report;
    }
    
    return $distributionsQuery;
}

public function findItemTotalQty($id){
    $totalQuery=$this->conn->query("select sum(inventory_item_qty_in_stock) as total from inventory_item_qties_by_location where inventory_item_id='".$id."'");
    if(!$totalQuery){
        return False;
    }
    $total=$totalQuery->fetch_object();
    return $total->total;
}

public function findLocationStock($id){
        global $report;
        global $inventory_none_in_existence;
        global $inventory_dbconnnect_error;
        $locationStockQuery=$this->conn->query("SELECT inventory_item_qties_by_location. * , drug. * , inventory_locations. * , sum(inventory_item_qties_by_location.inventory_item_qty_in_stock ) AS total
FROM inventory_item_qties_by_location, drug, inventory_locations
WHERE inventory_item_qties_by_location.inventory_location_id = inventory_locations.inventory_location_id
AND inventory_item_qties_by_location.inventory_item_id = drug.drug_id
AND inventory_item_qties_by_location.inventory_location_id = '".$id."'
AND inventory_item_qties_by_location.inventory_item_qty_in_stock >0
GROUP BY drug.drug_id,inventory_locations.inventory_location_id
ORDER BY drug.drug_name ASC");
        
        if(!$locationStockQuery){
            $report=$inventory_dbconnnect_error;
            return $report;
        }
     if($locationStockQuery->num_rows<1){
         $report=$inventory_none_in_existence;
         return $report;
     }   
     
     return $locationStockQuery;
}

public function findLocationTotalQty($id){
    $locationTotalQuery=$this->conn->query("SELECT sum( inventory_item_qties_by_location.inventory_item_qty_in_stock )as total
FROM inventory_item_qties_by_location
WHERE inventory_location_id ='".$id."'");
    if($locationTotalQuery){
        $total=$locationTotalQuery->fetch_object();
        return $total->total;
    }
}

public function getDrug($drugID){
    $conn = new DBConf();
    $drugID = (int)$drugID;
    $query = "SELECT * FROM drug
                WHERE drug_id = '$drugID'";
    $result = $conn->run($query);
    if ($conn->hasRows($result)){
        $retVal = mysql_fetch_assoc($result);
    } else $retVal = "";
    return $retVal;
}   //END getDrug()

//        public function get_existing_items($table,$orderCollumn,$orderStyle){
//             global $items;
//            global $report;
//            $items=$this->conn->query("select * from $table order by $orderCollumn $orderStyle");
//          
//            if(!$items){
//                 
//                return FALSE;
//                }
//                if($items->num_rows<1){
//                    $report=$none_in_existence;
//                    return $report;
//                }
//                return $items;
//        }
//        

//// public function getChildren($parentID){
////        $retVal = "";
////        $disChildren = $prodCat->getChildCategories($parentID);
////        if ($startRowStyle == " class=\"tr-row\" "){
////            $rowStyle1 = " class=\"tr-row2\" ";
////            $rowStyle2 = " class=\"tr-row\" ";
////        } else {
////            $rowStyle1 = " class=\"tr-row\" ";
////            $rowStyle2 = " class=\"tr-row2\" ";
////        }
////		$juggleRows = true;
////        $sNo = 0;
////        foreach ($disChildren as $cat){
////			$sNo++;
////            $currentCount++;
////			$rowStyle = $juggleRows ? $rowStyle1 : $rowStyle2;
////			$juggleRows = !$juggleRows;
////			$retVal .= "<tr $rowStyle>
////							<td>" . $sNoPrefix . $sNo . "</td>
////							<td id=\"payment_item_" . $cat["prodcat_id"] . "\"><label for=\"category_" . $cat["prodcat_id"] . "\">$prefix" . stripslashes($cat["prodcat_name"]) . "</label></td>
////							<td align=\"center\"><input type=\"checkbox\" name=\"category[]\" id=\"category_" . $cat["prodcat_id"] . "\" value=\"" . $cat["prodcat_id"] . "\" /></td>
////						</tr>";
////            $sNoPrefixNew = $sNoPrefix . $sNo . ".";
////            $retVal .= getChildren($cat["prodcat_id"], $institutionID, $prefix . "<span style=\"color: #AAA;\">------&raquo;</span> ", $sNoPrefixNew, $rowStyle);
////		}   //END foreach
////        
////        return $retVal;
////        
////    }   //END getChildren()
//}
//
//
//
//
//	$display = "<table cellpadding=\"3\" cellspacing=\"3\" style=\"border: 5px solid #CCC\">
//                                        <tr>
//                                            <td><strong>S/NO</strong></td>
//                                            <td><strong>PRODUCT CATEGORY</strong></td>
//                                            <td>&nbsp;</td>
//                                        </tr>";
//        $display .= getChildren(0, $_SESSION['institution_id'], "", "", " class=\"odd\" ");
//        $display .= "</table>";
//	echo $display;
//            

}
?>