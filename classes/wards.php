<?php
/** Contains variables, methods and other functionalitieds to manage ward table
 *@author ahmed rufai
 * @package Wards
 * Create Date: 26-08-2009
 */
require_once './classes/DBConf.php';

Class wards {
    /**
     *
     * @var array holds array of fields in the ward table
     */
    public $ward=array() ;
    /**
     * @var array holds values to be stored in the ward table fields
       */
    public $ward2=array() ;
	public $ward_id; //int(10)
	public $clinic_id; //int(10)
	public $ward_name; //varchar(255)
	public $ward_bedspaces; //varchar(255)
	public $ward_siderooms; //varchar(255)
	public $connection;
    public $conn;
    public $qq=NULL;
    /**
     * class constructor
     */
	 function  __construct() {
        $this->conn = new DBConf();
    }
   /**
     * Load one row into class variables, using specified row key
     *
     * @param int $key_row
     *
     */
	 
function New_wards($clinic_id,$ward_name,$ward_bedspaces,$ward_siderooms){
		
		$this->clinic_id = $clinic_id;
		$this->langcont_id = $langcont_id;
		$this->ward_bedspaces = $ward_bedspaces;
		$this->ward_siderooms = $ward_siderooms;
		//$this->Save_Active_Row_as_New();
	}


   /**
     * list rows that match specified criteria
     * @param string $search_field
     * @param string $search_value
     * @param string $curLnag_field
     * @param string $langtb
     */
	 
function rowSearch($search_field,$search_value,$curLnag_field,$langtb){
		 try {
		 $getlang= new language();
		$clinics = new clinic();
		$sql = "Select * from wards where  ".$ward_field."='".$ward_value."'" ;
		$res =$this->conn->execute($sql);
	  // echo "entered";
	     $i=1;
		 while ($row = mysql_fetch_array($res)) {
 			$clinic=$clinics->getClinic_name($row["clinic_id"],$curLnag_field,$langtb)  ;
			   echo ' <tr>
        <td><input type="checkbox" class="checkbox" name="chkwardID[]" value="'.$row["ward_id"].'" /></td>
        <td>'.$row["ward_id"].'</td>  
		<td>'.$clinic.'</td>
         <td>'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</td>
        <td>'.$row["ward_bedspaces"] .'</td>
       	<td>'.$row["ward_siderooms"].'</td>
        <td>'.'<a href = "./index.php?jj=delete&p=ward&id='.$row["ward_id"] .'"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td> <td>'.'<a href = "./index.php?editward=upd&p=ward&ward_id='.$row["ward_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
		} catch (Exception $e) {
        }
	}
	/**
     * list all rows from a specified table
     * @param string $curLnag_field
     * @param string $langtb
     */
function allrows($curLnag_field,$langtb) {
        try {
		$db2 = new DBConf();
		$getlang= new language();
		$clinics = new clinic();
            $sql = 'select * from wards';
			$pagelink32='ward';
$pager = new PS_Pagination($db2,$sql,2,10,$pagelink32);
$rs = $pager->paginate();
 $offset=$pager->offset; 
         //$rs = $this->conn->execute($sql);
			$i=1;
            while ($row = mysql_fetch_array($rs)) {
              $clinic=$clinics->getClinic_name($row["clinic_id"],$curLnag_field,$langtb)  ;
			   if ($i%2 ==0) {$bgcolor = "tr-row";} else {$bgcolor = "tr-row2";} 
			    echo' <tr class="'.$bgcolor.'">
        <td>'.(++$offset).'.</td>
 		<td><a href = "./index.php?p=editward&ward_id='.$row["ward_id"] .'">'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</a></td>
		<td>'.$clinic.'</td>
        <td align=center>'.$row["ward_bedspaces"] .'</td>
       	<td align=center>'.$row["ward_siderooms"].'</td>
        
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
        } catch (Exception $e) {
	
        }


    }
    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @param string $langtb language table
     * @param int $lang_id id in the ward table that matches one in the lang table
     *
     */
	public function Delete_row_from_key($lang_id,$key_row,$langtb){
	 	try {
		
           	$this->conn->execute("DELETE FROM wards WHERE ward_id = $key_row");
		    $this->conn->execute("DELETE FROM $langtb WHERE langcont_id = $lang_id");
           
		   
		   if ($res) {
         //  return $lang_mesg_update;   
            }
        } catch (Exception $e) {
		 echo 'ERROR : Failed deletion '.$e->getMessage();

           
        }
	
	}

    /**
     * Update the active class variables on a specified table row
       * @param int $id row id
     * @param string $tablename table name
     * @paramstring $curLnag_field
     * @param string $langtb language table
     * @param int $lang_id id in the ward table that matches one in the lang table
     */
     
	public function Save_Active_Row($lang_id,$id,$tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("UPDATE drugcategory set drugcat_name = \"$this->drugcat_name\" where drugcat_id = \"$this->drugcat_id\"");*/
		
		try {
		$q=count($this->ward);
		

		for ($i = 0; $i < $q; $i++) {
			if($this->ward[$i]=='langcont_id'){
			$sql="UPDATE $langtb set $curLnag_field ='".$this->ward2[$i]."' where langcont_id = '$lang_id'";
					  $res=$this->conn->execute($sql);	
		}
  //
	}
	
			
	$sql = "UPDATE $tablename SET ";	
	
				$qq=count($this->ward);
				$q=count($this->ward);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->ward[$i]=='langcont_id')
						     $sql.=    $this->ward[$i] .'="'.$lang_id.'" ';
						else
         			  		  $sql.=    $this->ward[$i] .'="'.$this->ward2[$i].'" ';
			 
					 }
					 else{
					     if($this->ward[$i]=='langcont_id')
						     $sql.=      $this->ward[$i] .'="'.$lang_id.'" ,';
						 else
						     $sql.=      $this->ward[$i] .'="'.$this->ward2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			$result2 =$this->conn->execute("SELECT * from $tablename");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
		 $oldTotal=$this->getBedspaces_totalonewardb4($id);
			  	
       $res=$this->conn->execute($sql);
      if (mysql_affected_rows()==1){
	 				     $newTotal=$this->getBedspaces_totaloneward($id);
						$diff=$newTotal-$oldTotal;
						 $sql3="UPDATE wards set ward_availablebedspaces=ward_availablebedspaces+'$diff' WHERE ward_id='$id'";
						$res3=$this->conn->execute($sql3);
				        
		} 
		 
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		
		
		
		
	}

    /**
     *Save the active class vars  as a new row on table
     * @param int $tablename
     * @param string $curLnag_field
     * @param string $langtb
     * @return array
     */
    
	public function Save_Active_Row_as_New($tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("Insert into drugcategory (drugcat_name) values (\"$this->drugcat_name\"");*/
		try {
		
		// server validation begins
					$getLang= new language();		
					$xlp_formfield =$this->ward;
					$xlp_fieldmessage = array('Specify Clinic Name','Specify Ward Name','Specify Bedspace');
		            $xlp_error=$getLang->xlpildator($xlp_formfield,$xlp_fieldmessage);
					if (!empty($xlp_error[2]))
					throw new Exception;
	   //sever validation ends
		$q=count($this->ward);
		for ($i = 0; $i < $q; $i++) {
			if($this->ward[$i]=='langcont_id'){
			$sql3="Insert into $langtb ($curLnag_field) values ('".$this->ward2[$i]."')";
			  echo $sql3;
			   $this->conn->execute($sql3);
				$content_id= mysql_insert_id();
			}	
		}
		
		$sql = "INSERT INTO $tablename SET ";	
	
				$qq=count($this->ward);
				//$q=count($this->ward);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->ward[$i]=='langcont_id')
						     $sql.=    $this->ward[$i] .'="'.$content_id.'" ';
						else
         			  		  $sql.=    $this->ward[$i] .'="'.$this->ward2[$i].'" ';
			 
					 }
					 else{
					     if($this->ward[$i]=='langcont_id')
						     $sql.=      $this->ward[$i] .'="'.$content_id.'" ,';
						 else
						     $sql.=      $this->ward[$i] .'="'.$this->ward2[$i].'" ,';
			
					 }
  				 $q--;
 			}
		
		  $result=$this->conn->execute($sql);
				 $list=array();
				if($this->conn->hasRows($result,  0)){
					 $list[3]=true;
					return $list;	
				}
        } catch (Exception $e) {
             return $xlp_error;
        }
		
		
		
		
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result =$this->conn->execute("SELECT ward_id from wards order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["ward_id"];
				$i++;
			}
	return $keys;
	}
	
	/**
     * get bed spaces for a specified ward
     * @param int $id ward id
     * @return <type>
     */
	function getWard_bedspaces($id) {
        try {
            $sql = "select * from wards where 
			ward_id =".$id ;
          
		    $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
            return $row['ward_bedspaces'];

        } catch (Exception $e) {

        }
    }
	/**
     * get bed spaces for a particular ward siderooms inclusive
     * @param int $id ward id
     * @return int
     */
	function getBedspaces_totaloneward($id) {
        try {
            $sql = "select ward_bedspaces+ward_siderooms as totbedspaces from wards where 
			ward_id =".$id ;
          
		    $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
            return $row['totbedspaces'];

        } catch (Exception $e) {

        }
    }
    /**
     * get bed spaces for a particular ward siderooms inclusive before allocation
     * @param int $id ward id
     * @return int
     */
	function getBedspaces_totalonewardb4($id) {
        try {
           $sql = "select ward_bedspaces+ward_siderooms as totbedspaces from wards where 
			ward_id =".$id ;
          
		    $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
            return $row['totbedspaces'];

        } catch (Exception $e) {

        }
    }
    /**
     *
     * @param int $id ward id
     * @return int
     */
	function getWard_siderooms($id) {
        try {
            $sql = "select * from wards where 
			ward_id =".$id ;
          
		    $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
            return $row['ward_siderooms'];

        } catch (Exception $e) {
		
        }
    }
	
	/**
     *
     * @param int $id ward id
     * @param string $curLnag_field column in lang table that holds current lang text
     * @param string $langtb
     * @return string
     */
	
	function getWard_name($id,$curLnag_field,$langtb) {
        try {
		$getlang= new language();
            $sql = "select * from wards where 
			ward_id =".$id ;
          
		    $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
			return $getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
        } catch (Exception $e) {

	
        }
    }
    /**
     * populate option tags with ward names from the ward table
     * @param string $curLnag_field column in lang table that holds current lang text
     * @param string $langtb
     */
	function getWard_id($curLnag_field,$langtb) {
        try {
		    $getlang= new language();
            $sql = 'select *  from wards ';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                echo '<option value ="'.$row['ward_id'] .'">'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {


        }
    }
    /**
     * populate option tags with wards from a particular clinic
     * @param string $curLnag_field column in lang table that holds current lang text
     * @param string $langtb
     * @param int $id clinic id

     */
	function getWard_id3($id,$curLnag_field,$langtb) {
        try {
		    $getlang= new language();
            $sql = 'SELECT  * FROM wards WHERE clinic_id='.$id;
            $res = $this->conn->execute($sql);
			echo '<option value ="-1">---</option>';
            while ($row = mysql_fetch_array($res)) {
			$bedspaces=$row['ward_bedspaces']+$row['ward_siderooms'];
			$availspaces=$bedspaces;
			                echo '<option value ="'.$row['ward_id'] .'">'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {


        }
    }
    /**
     * populate option tags with wards from a particular clinic
     * @param string $table table name
     * @param string $curLnag_field column in lang table that holds current lang text
     * @param string $langtb
     * @param int $id inp_id represents admission id
     */
	function getWard_id2($id,$table,$curLnag_field,$langtb) {
        try {
		    $getLang= new language();
			$sql2 = 'select ward_id from '. $table.' where inp_id ='.$id;
			
				 $res2 = $this->conn->execute($sql2);
                  if($res2){
                     $row2 = mysql_fetch_array($res2);
                     $id2=$row2['ward_id'];
                   }
            $sql = 'select * from wards ';
            $res = $this->conn->execute($sql);
			echo '<option value ="-1">---</option>';
            while ($row = mysql_fetch_array($res)) {
			$sel= ($id2==$row['ward_id'])? "selected":" ";
                echo '<option value ="'.$row['ward_id'].'"  '. $sel.'>'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {


        }
    }
	
	
	
/* function editWard($id,$clinic_id,$ward_name,$ward_bedspaces,$ward_siderooms) {
        try {
            $query = "UPDATE wards
                  SET   ward_name = '".$ward_name."',
                        clinic_id  = '".$clinic_id."',
                        ward_bedspaces  = '".$ward_bedspaces."',
                        ward_siderooms   = '".$ward_siderooms."' where ward_id ='".$id."'" ;
						//echo $query;
						//exit;
            $res = $this->conn->execute($query);
            if ($res) {
//return $lang_mesg_update;
            }
        } catch (Exception $e) {
            echo 'ERROR : Update failed '.$e->getMessage();
        }
    }*/
	/**
     * Load one row into class variables, using specified row key
     *
     * @param int $key_row
     *
     */
public function Load_from_key($key_row){
		$result = $this->conn->execute("Select * from wards where ward_id = \"$key_row\" ");
		while($row = mysql_fetch_array($result)){
		$this->ward_id = $row["ward_id"];
		$this->clinic_id = $row["clinic_id"];
		$this->langcont_id = $row["langcont_id"];
		$this->ward_bedspaces = $row["ward_bedspaces"];
		$this->ward_siderooms = $row["ward_siderooms"];
		$this->langcont_id = $row["langcont_id"];
		}
	}
	public function GetWardFields(){
	echo 	"<option value='-1'>Select Field</option>";	
		$result =$this->conn->execute("SELECT * from wards order by ward_id");
				$i = 0;
				
while ($i < mysql_num_fields($result)) {
 //   echo "Information for column $i:<br />\n";
    $meta = mysql_fetch_field($result);
    if (!$meta) {
      //  echo "No information available<br />\n";
    }
  //  echo "$meta->name";

		
	echo 	"<option value=". $meta->name.">".$meta->name."</option>";
	    $i++;
}
/*mysql_free_result($result);
	return $keys;*/
	}
    /**
     * Close mysql connection
     */
	public function endwards(){$this->conn->CloseMysql();
	}

}

	/*public $ward_id; //int(10)
	public $clinic_id; //int(10)
	public $ward_name; //varchar(255)
	public $ward_bedspaces; //varchar(255)
	public $ward_siderooms; //varchar(255)*/
	
// ------------------------------------------------------------------------

