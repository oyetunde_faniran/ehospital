<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventoryCreationFormProcessingClass
 *
 * @author upperlink
 */
class inventory_transfer_class  extends inventoryGeneralClass{
    
    public function getItemStockBatches($itemId){
        global $inventory_dbconnnect_error;
        global $inventory_none_in_existence;
        
        $query=$this->conn->query("select inventory_batch_details.inventory_batch_id, DATE_FORMAT(inventory_batch_details.inventory_batchdetails_expirydate,'%D %b, %Y, %h:%s') as expiryDate from inventory_batch_details,inventory_batch where inventory_batch_details.drug_id='".$itemId."' and inventory_batch_details.inventory_batch_id=inventory_batch.inventory_batch_id and inventory_batch.inventory_location_id='".$_SESSION[session_id() . "inventory_locationID"]."' group by inventory_batch_details.inventory_batch_id order by inventory_batch_details.inventory_batchdetails_expirydate asc ");
        
        if(!$query){
            $report=$inventory_dbconnnect_error;
        }elseif($query->num_rows<1){
            $report=$inventory_none_in_existence;
        }else{
            $report=$query;
        }
        return $report;
    }
 
    public function getAllLocationsExceptDis($dis){
        $othersQuery=$this->conn->query("select * from inventory_locations where inventory_location_id !='".$dis."'");
        if(!$othersQuery){
            return FALSE;
        }
        if($othersQuery->num_rows<1){
            $report=$none_in_existence;
            return $report;
        }
        return $othersQuery;
    }
    
    public function listItemsInStock($id, $getTotalOnly = false){
        global $report;
        global $inventory_none_in_existence;
        global $inventory_dbconnnect_error;
        global $limitStart, $recsPerPage;
        if ($getTotalOnly){
            $disQuery = "SELECT COUNT(DISTINCT d.drug_id) 'totalCount'
                            FROM drug d
                                INNER JOIN inventory_batch_details ibd
                                INNER JOIN inventory_batch ib
                            ON d.drug_id = ibd.drug_id
                                AND ibd.inventory_batch_id = ib.inventory_batch_id
                            WHERE ib.inventory_location_id = '$id'";
            $total = $this->conn->query($disQuery);
            $total = $total->fetch_assoc();
            $report = $total["totalCount"];
        } else {
            $disQuery = "select sum(bd.inventory_batchdetails_quantity) as totalQty,
                                d.*,
                                bd.inventory_batchdetails_expirydate,
                                bd.inventory_batch_id

                            from drug d, inventory_batch b, inventory_batch_details bd

                            where bd.inventory_batch_id = b.inventory_batch_id
                                and b.inventory_location_id = '".$id."'
                                and bd.drug_id = d.drug_id
                            group by d.drug_id
                            order by d.drug_name asc
                            LIMIT $limitStart, $recsPerPage";
            $allItemsQuery=$this->conn->query($disQuery);

            if(!$allItemsQuery){
                $report=$inventory_dbconnnect_error;
            }elseif($allItemsQuery->num_rows<1){
                $report=$inventory_none_in_existence;
            }else{
                $report=$allItemsQuery;
            }
        }
        
        return $report;
    }

    public function withdrawTransfer($transferId){
        global $report;
        global $inventory_dbconnnect_error;
        global $inventory_none_in_existence;
        $itemUpdateError=0;
        $withdrawalQuery=$this->conn->query("select inventory_batchdetails_id, inventory_item_total_qty from inventory_transfer_details where inventory_transfer_id='".$transferId."' group by inventory_item_id");
        if(!$withdrawalQuery){
            $report=$inventory_dbconnnect_error;
        }elseif($withdrawalQuery->num_rows<1){
            $report=$inventory_none_in_existence;
        }else{
            $this->conn->autocommit(FALSE);
            $error="";
            while($row=$withdrawalQuery->fetch_assoc()){
$quantityRestored=$this->conn->query("update inventory_batch_details 
    set inventory_batchdetails_quantity=inventory_batchdetails_quantity+'".$row['inventory_item_total_qty']."' 
        where 
        inventory_batchdetails_id='".$row['inventory_batchdetails_id']."'");
if(!$quantityRestored){
    $error.="The qty for ".$row['inventory_batchdetails_id']." not restored<br />";
}
            }
            if($error!=""){
                $error="<div class=\"comments\">Error restoriing quantities</div>";
            }else{
                $removalQuery=$this->conn->query("delete from inventory_transfer_details where inventory_transfer_id='".$transferId."'");
                if(!$removalQuery){
                    $error.="<div class=\"comments\">Error removing transfer details</div>";
                }else{
                    $deleteTransfer=$this->conn->query("delete from inventory_transfers where inventory_transfer_id='".$transferId."'");
                    if(!$deleteTransfer){
                        $error.="<div class=\"comments\">Unable to remove transfer</div>";
                    }
                }
            }
            if($error==""){
                $this->conn->commit();
                $report="Transfer successfully withdrawn";
            }else{
                $this->conn->rollback();
                $report="System Error! Unable to withdraw transfer";
            }
        }
        return $report;
    }
    
    public function initiateTransfer($source,$destination,$qty){
        global $report;
        global $inventory_dbconnnect_error;
        global $inventory_transfer_details_db_error_msg;
        global $inventory_transfer_details_success_msg;
        global $inventory_transfer_initiate_success_msg;
        if(isset($source)&& is_numeric($source)&& isset($destination)&& is_numeric($destination)){
            $transferStartQuery=$this->conn->query(
                    "insert into inventory_transfers (inventory_transfer_source,inventory_transfer_dest,inventory_transfer_date_sent,inventory_transfer_status_id,inventory_transfer_source_staff) values ('".$source."','".$destination."',CURRENT_TIMESTAMP,'4','".$_SESSION[session_id() . "userID"]."')"
                    );
            if(!$transferStartQuery){
                $report=$inventory_dbconnnect_error;
            }else{
            $report=$inventory_transfer_initiate_success_msg;
            }
            $transferId=$this->conn->insert_id;
            
            $invUnit = new inventory_units();
            $totalQtyInLowestUnit = 0;
            if (is_array($qty)){
                           foreach ($qty as $disItemID => $disUnitArray){
                    foreach ($disUnitArray as $disUnitID => $disQty){
                        $qtyInLowestUnit = $invUnit->convertToLowestUnit($disUnitID, $disItemID, $disQty);
                        $totalQtyInLowestUnit += $qtyInLowestUnit;
            
                
                
                    $transferDetailsQuery=$this->conn->query("insert into inventory_transfer_details (inventory_transfer_id,inventory_item_id,inventory_item_total_qty,inventory_item_status) values ('".$transferId."','".$disItemID."','".$totalQtyInLowestUnit."','4')");
                         }
                    }
                }
                    if(!$transferDetailsQuery){
                        $detailsReport=$inventory_transfer_details_db_error_msg;
                    }else{
                        $detailsReport=$inventory_transfer_details_success_msg;
                    }
                
            
            $report=$report."&nbsp;<br />".$detailsReport;
            return $report;
            }
        }
    
        public function getItemQtyForBatch($itemId,$batchId){
            $itemQty=0;
            $query=$this->conn->query("select inventory_batchdetails_quantity from inventory_batch_details where inventory_batch_id='".$batchId."' and drug_id='".$itemId."'");
          if(($query)&&($query->num_rows>0)){
                $itemQty=$query->fetch_assoc();
            }
            return $itemQty['inventory_batchdetails_quantity'];
        }
        
          public function getBatchDetailsInfo($itemId,$batchId){
            $itemQty=0;
            $query=$this->conn->query("select inventory_batchdetails_id,inventory_batchdetails_expirydate from inventory_batch_details where inventory_batch_id='".$batchId."' and drug_id='".$itemId."'");
          if(($query)&&($query->num_rows>0)){
                $itemQty=$query->fetch_assoc();
            }
            return $itemQty;
        }
        
        
        public  function record_transfer($source,$destination){
        global $report;
        global $inventory_dbconnnect_error;
        global $inventory_transfer_details_db_error_msg;
        global $inventory_transfer_initiate_success_msg;
            $recordQuery=$this->conn->query("insert 
                into inventory_transfers
                (inventory_transfer_source,
                inventory_transfer_dest,
                inventory_transfer_source_staff,
                inventory_transfer_date_sent,
                inventory_transfer_status_id)
                values 
                ('".$source."','".$destination."','".$_SESSION[session_id() . "userID"]."', CURRENT_TIMESTAMP,'1')");
            if(!$recordQuery){
                $report=$inventory_dbconnnect_error;
            }else{
                $report=$this->conn->insert_id;
                }
                return $report;
        }
        
        public function recordTransferDetails($transId,$itemId,$batchDetailsId,$batchDetailsExpirydate,$totalQty,$status){
            global $report;
            global $inventory_dbconnnect_error;
            global $inventory_transfer_details_record_failure_msg;
            global $inventory_transfer_details_success_msg;
            $recordQuery=$this->conn->query("
                insert into inventory_transfer_details(
                inventory_transfer_id,
                inventory_item_id,
                inventory_batchdetails_id,inventory_item_expirydate,
                inventory_item_total_qty, 
                inventory_item_item_status
                )values('".$transId."','".$itemId."','".$batchDetailsId."','".$batchDetailsExpirydate."','".$totalQty."','".$status."')");
            if(!$recordQuery){
                $report=$inventory_transfer_details_record_failure_msg;
            }else{
                $report=$inventory_transfer_details_success_msg;
                $deductQuery=$this->conn->query("update inventory_batch_details set inventory_batchdetails_quantity=inventory_batchdetails_quantity-".$totalQty." where inventory_batchdetails_id='".$batchDetailsId."'");
                if(!$deductQuery){
                    $report.="<br />System Error! Stock balances were not properly deducted";
                }else{
                    $report.="<br />Stock balances adequately recalculated";
                }
            }
            return $report;
        }
        
      public function record_receipt($transferId,$locationId){
          $receiptQuery=$this->conn->query("update inventory_transfers set inventory_transfer_dest_staff='".$_SESSION[session_id() . "userID"]."',inventory_transfer_date_received=CURRENT_TIMESTAMP,inventory_transfer_status_id='3' where inventory_transfer_id='".$transferId."'");
          if(!$receiptQuery){
              $report="Failure to record stock receipt";
          }else{
              $batchQuery=  $this->conn->query("insert into inventory_batch (inventory_location_id,inventory_batch_datetime_received, inventory_batch_source,user_id) values ('".$locationId."',CURRENT_TIMESTAMP,'2','".$_SESSION[session_id() . "userID"]."')");
              if(!$batchQuery){
                  $report="Failure to record batch info";
              }else{
                  $batch_id=$this->conn->insert_id;
                  $report=  $batch_id;
              }
          }
          return $report;
      }
        
      
      public function getItemsInDisTransfer($transferID){
          $getItemsQuery=$this->conn->query("select inventory_item_id from inventory_transfer_details where inventory_transfer_id='".$transferID."'");
          if(!$getItemsQuery){
              $report="System Error! Unable to get the items involved in this transfer";
              }elseif($getItemsQuery->num_rows<1){
                  $report="No items involved in this transfer";
              }else{
                  $report=array();
                  while($row=$getItemsQuery->fetch_assoc()){
                      array_push($report,$row);
                  }
              }
          return $report;
      }
      
      public function record_receipt_details($itemId,$transferId,$batchId){
          $detailsQuery=$this->conn->query("select * from inventory_transfer_details where inventory_transfer_id='".$transferId."' and inventory_item_id='".$itemId."'");
          if(!$detailsQuery){
              $report="Unable to get enough details for this item in this transfer";
          }else{
              $details=$detailsQuery->fetch_assoc();
              $qty=$details['inventory_item_total_qty'];
              $batchDetailsId=$details['inventory_batchdetails_id'];
              $expiryDateQuery=$this->conn->query("select * from inventory_batch_details where inventory_batchdetails_id='".$batchDetailsId."'");
              if(!$expiryDateQuery){
                  $report="Unable to get the expiry date and cost price of this item in this transfer";
              }else{
                  $expiryDateArray=$expiryDateQuery->fetch_assoc();
                  $expiryDate=$expiryDateArray['inventory_batchdetails_expirydate'];
                  $costPrice=$expiryDateArray['inventory_batchdetails_costprice'];
                  
                  $updateTransferDetailsQuery=$this->conn->query("update inventory_transfer_details set inventory_item_item_status='3' where inventory_batchdetails_id='".$batchDetailsId."'");
                  if(!$updateTransferDetailsQuery){
                      $report="Unable to update transfer details table";
                  }else{
                      $populateBatchDetailsQuery=  $this->conn->query("insert into inventory_batch_details (inventory_batch_id,drug_id,inventory_batchdetails_received_quantity,inventory_batchdetails_quantity,inventory_batchdetails_costprice,inventory_batchdetails_expirydate ) values ('".$batchId."','".$itemId."','".$qty."','".$qty."','".$costPrice."','".$expiryDate."')");
                      if(!$populateBatchDetailsQuery){
                          $report="Unable to record batch details for this transfer";
                      }else{
                          $report="<br />Transfer successfully completed!";
                      }
                  }
              }
          }
          return $report;
      }



      public function checkPost($array){
            $unitsClass=new inventory_units;
            $warning=FALSE;
                foreach ($array['qty'] as $itemId=>$unitId){
         $qtyForUse=0;
         $batchId=$array['batch'][$itemId];
//         echo "batch Id =".$batchId;
         
        foreach ($unitId as $qtity=>$value){
           
            $lowestQty=$unitsClass->convertToLowestUnit($qtity, $itemId, $value);
            $qtyForUse+=$lowestQty;
            
//            echo "Item Id =".$itemId." <br />Unit Id=".$qtity." <br />Qty to be transfered =".$lowestQty."<br /> while the transfer Id Id=".$genRec."<br />The source Location =".$inventory_locationID."<br />Destination location=".$_POST['destLocation']."<br /><hr style=\"border:dashed 1px #CCC\" />";
        }
        $batchDetailsInfo=$this->getBatchDetailsInfo($itemId, $batchId);
        $batchDetailsId=$batchDetailsInfo['inventory_batchdetails_id'];
        $batchDetailsExpiryDate=$batchDetailsInfo['inventory_batchdetails_expirydate'];
        $allowableQty=$this->getItemQtyForBatch($itemId,$batchId);
        if($qtyForUse>$allowableQty){
            $warning= "You have chosen a total transfer quantity fro one or more items greater than the available quantity for such item for the chosen batch. Please choose valid transfer quantites and retry<br />";
            }
        }
    return $warning;
    }
    
    public function listAllTransfers($locationId){
        global $inventory_dbconnnect_error;
        global $report;
        global $inventory_none_in_existence;
        $query=$this->conn->query("
            select *,DATE_FORMAT(inventory_transfer_date_sent,'%D %b, %Y, %h:%i %p') as sentDate,DATE_FORMAT(inventory_transfer_date_received,'%D %b, %Y, %h:%i %p') as receivedDate from inventory_transfers 
            where inventory_transfer_source='".$locationId."' or 
                inventory_transfer_dest='".$locationId."'
                order by inventory_transfer_date_sent 
                desc");
        if(!$query){
            $report=$inventory_dbconnnect_error;
        }elseif($query->num_rows<1){
            $report=$inventory_none_in_existence;
        }else{
            $report=$query;
        }
        return $report;
    }
    
     public function listTransfersToDisLocation($locationId){
        global $inventory_dbconnnect_error;
        global $report;
        global $inventory_none_in_existence;
        $query=$this->conn->query("
            select *,DATE_FORMAT(inventory_transfer_date_sent,'%D %b, %Y, %h:%i %p') as sentDate,DATE_FORMAT(inventory_transfer_date_received,'%D %b, %Y, %h:%i %p') as receivedDate from inventory_transfers 
            where  
                inventory_transfer_dest='".$locationId."'
                order by inventory_transfer_date_sent 
                desc");
        if(!$query){
            $report=$inventory_dbconnnect_error;
        }elseif($query->num_rows<1){
            $report=$inventory_none_in_existence;
        }else{
            $report=$query;
        }
        return $report;
    }
    
    public function getTransferGeneralInfo($id){
        global $inventory_dbconnnect_error;
        global $report;
        global $inventory_none_in_existence;
        $query=$this->conn->query("select inventory_transfers.*,inventory_transfer_details.*,DATE_FORMAT(inventory_transfers.inventory_transfer_date_sent,'%D %b, %Y, %h:%i %p') as sentDate from inventory_transfers,inventory_transfer_details where inventory_transfers.inventory_transfer_id=inventory_transfer_details.inventory_transfer_id and inventory_transfers.inventory_transfer_id='".$id."' group by inventory_transfers.inventory_transfer_id");
        if(!$query){
            $report=$inventory_dbconnnect_error;
        }elseif($query->num_rows<1){
            $report=$inventory_none_in_existence;
        }else{
            $report=$query->fetch_assoc();
            }
        return $report;
    }
    
    public function getTransferDetails($id){
        global $report;
        global $inventory_none_in_existence;
        $detailsQuery=$this->conn->query("select 
            inventory_transfers.*, 
            inventory_transfer_details.*, 
            drug.*,DATE_FORMAT(inventory_transfer_details.inventory_item_expirydate,'%D %b, %Y') as expiryDate
            from 
            inventory_transfers, 
            inventory_transfer_details, 
            drug,
            inventory_batch_details 
            where 
            inventory_transfers.inventory_transfer_id=inventory_transfer_details.inventory_transfer_id 
            and inventory_transfer_details.inventory_batchdetails_id=inventory_batch_details.inventory_batchdetails_id 
            and inventory_transfer_details.inventory_item_id=inventory_batch_details.drug_id and inventory_transfer_details.inventory_item_id=drug.drug_id 
            and inventory_transfers.inventory_transfer_id='".$id."' 
                order by drug.drug_name asc");
        if(!$detailsQuery){
            $report=$inventory_dbconnnect_error;
            }elseif($detailsQuery->num_rows<1){
                  $report=$inventory_none_in_existence;
            }else{
                $report=$detailsQuery;
            }
            return $report;
    }
    
}
?>
