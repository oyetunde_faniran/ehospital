<?php
class Paginator{
	public function __construct (){
	}


	public static function doPagination($args){
/*
* [I] = Compulsory

	$arg is an array that should contain the following elements
	- total:				[I]The total number of records
	- recordsPerPage:		[I]The number of records to show per page
	- baseURL:				[I]The url of the page on which the results would be viewed

	- pageText:				The variable name to be used for page in query strings
	- recordsPerPageText:	The variable name to be used for records-per-page in query strings
	- curPage:				A value specifying the current page
	- aTagParent			The tag to be used to enclose the <a> tag (for styling purposes)
	- curATagParent			The tag to be used to enclose the <a> tag for the current page (for styling purposes)
*/
		$total = 				!empty($args["total"]) ? (int)$args["total"] : 0;
		$recordsPerPage = 		!empty($args["recordsPerPage"]) ? (int)$args["recordsPerPage"] : 20;
		$baseURL = 				!empty($args["baseURL"]) ? $args["baseURL"] : "";
		$pageText = 			!empty($args["pageText"]) ? $args["pageText"] : "recordspage";
		$recordsPerPageText = 	!empty($args["recordsPerPageText"]) ? $args["recordsPerPageText"] : "records";
		$curPage = 				!empty($args["curPage"]) ? $args["curPage"] : 1;
		$aTagParent = 			!empty($args["aTagParent"]) ? $args["aTagParent"] : "";
		$curATagParent = 		!empty($args["curATagParent"]) ? $args["curATagParent"] : "";

		//Get the total number of pages that is needed based on the current total number of records and the number of records to show per page
		if ($recordsPerPage > 0)
			$maxPage = ceil($total / $recordsPerPage);
		else $maxPage = 0;

		//Continue with the pagination process only if more than a page is needed to view the records
		if ($maxPage > 1){
		
			//***Create tags to be used to enclose all <a> tags and the <a> tag for the current page
				//Others
				if (!empty($aTagParent)){
					$aFrontTag = "<$aTagParent>";
					$aBackTag = "</$aTagParent>";
				} else {
						$aFrontTag = "";
						$aBackTag = "";
				}
				
				//Current
				if (!empty($curATagParent)){
					$curAFrontTag = "<$curATagParent>";
					$curABackTag = "</$curATagParent>";
				} else {
						$curAFrontTag = "";
						$curABackTag = "";
				}
			//***
				
				//if the base url contains any query string, maintain it and append "&" to it, else append a "?" to it
				$urlParts = explode ("?", $baseURL);
				if (count($urlParts) > 1)	//It contains a query string already
					$baseURL .= "&";
				else $baseURL .= "?";	//No query string. So, append ? to it
		
				//Create Next & Previous links if need be
				if ($curPage > 1 && $maxPage > 1)
					$previousLink = $baseURL . $pageText . "=" . ($curPage - 1) . "&" . $recordsPerPageText . "=" . $recordsPerPage;
				else $previousLink = "";
				if ($curPage < $maxPage && $maxPage > 1)
					$nextLink = $baseURL . $pageText . "=" . ($curPage + 1) . "&" . $recordsPerPageText . "=" . $recordsPerPage;
				else $nextLink = "";
		
		
			//***Now, form a string containing the paginated navigation bar
				//Add a previous link if any
				if (!empty($previousLink))
					$retVal = $aFrontTag . "<a href=\"$previousLink\">&laquo; Previous</a>" . $aBackTag;
				else $retVal = "";
				
				//Add other page numbers
				for ($k=1; $k<=$maxPage; $k++){
					//Form the link (href attribute)
					$link = $baseURL . $pageText . "=" . $k . "&" . $recordsPerPageText . "=" . $recordsPerPage;
					if ($k == $curPage)
						$retVal .= $curAFrontTag . "$k" . $curABackTag;
					else $retVal .= $aFrontTag . "<a href=\"$link\">$k</a>" . $aBackTag;
				}
				
				//Add a next link if any
				if (!empty($nextLink))
					$retVal .= $aFrontTag . "<a href=\"$nextLink\">Next &raquo;</a>" . $aBackTag;
				
				/*$retVal .= "<form>
								<input type=\"hidden\" name=\"recspage\" value=\"$pageText\" />
								<input type=\"hidden\" name=\"recspage\" value=\"$pageText\" />
							</form>";*/
			//***
		} else $retVal = "";	//END if $maxPage > 1
		//At last, something to return
		return $retVal;
	}	//END doPagination()

}	//END class
?>