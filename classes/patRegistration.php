<?php
/**
 * Handles the registration of patients
 * 
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */

/**
 * Handles the registration of patients
 *
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */
class patRegistration {
public $reg  = array();
public $temp;
public $dest;
public $regextra;
public $conn;
public $nhisDeets;
public $retDeets;

private $randStringLength;
private  $reg_id;
private  $reg_hospital_no;
private  $reg_hospital_oldno;
private  $reg_surname;
private  $reg_othernames;
private  $reg_address;
private  $reg_dob;
private  $reg_phone;
private  $reg_email;
private  $nationality_id;
private  $state_id;
private  $reg_civilstate;
private  $reg_occupation;
private  $reg_placeofwork;
private  $reg_religion;
private  $reg_nextofkin;
private  $reg_nokrelationship;
private  $reg_nokaddress;
private  $reg_nokphone;
private  $reg_nokemail;
private  $reg_bloodgrp;
private  $reg_genotype;
private  $reg_labrefno;
private  $reg_rh;
private  $reg_hb;
private  $reg_condition;
private  $reg_xrayno;
private  $reg_status;
private  $reg_passport;
private  $retainership_id;
private  $reg_date;
private  $user_id;
private  $reg_gender;
private  $reg_isdependant;
private	 $reg_employee;
private	 $reg_idcardno;
private	 $reg_cardno;
private	 $nhis_id;
private	 $reg_matbookingcat;
private  $reg_age;



/**
 * Class Constructor
 */
    function  __construct() {
        $this->conn = new DBConf();
    }
	



/**
 * Registers a patient
 * @return int  Returns the registration ID of the patient on success
 */
    function registerPatient() {
        try {

		 $newname = $this->randomString(12);  
			if (function_exists("imagegif")) {
				$newname = $newname.".gif";
			} elseif (function_exists("imagejpeg")) {
				$newname = $newname.".jpg";
			} elseif (function_exists("imagepng")) {
				$newname = $newname.".png";
			}else{
				$newname = "";
			}
			
			//Do passport upload
			if (!empty($newname) && !empty($_FILES['passport']['tmp_name']) && $_FILES['passport']['error'] == 0){	
				 move_uploaded_file($_FILES['passport']['tmp_name'], "modules/".$this->temp.$newname);
				 $passport =  "modules/".$this->temp.$newname; 
				 $thumb = new uploadImage();
				 $thumb -> Thumbsize = 150;
				 $thumb -> Thumblocation = 'modules/'.$this->dest;
				 $thumb -> Thumbprefix = $this->reg['surname'];
				 $thumb -> Createthumb("$passport","file");
				 $newname = $this->reg['surname'].$newname;
			 } else {
				 $newname = "";
			 }
							  


			//Get the highest hospital number and form a new one based on that
			$sql = "SELECT MAX(reg_hospital_no) AS hospital_no FROM registry";
			//die ("<pre>$sql</pre>");
			$result =  $this->conn->execute($sql);
			$hospital_no = '';
			if ($this->conn->hasRows($result, 1)){
				$row = mysql_fetch_assoc($result);
				$hospital_no = (int)$row["hospital_no"] + 1;	//Since the hospital no is a number stored as a string, cast to int so as to be able to increment it
			} else $hospital_no = 1;
                        $hospital_no = str_pad($hospital_no, HOSP_NUMBER_XTER_COUNT, "0", STR_PAD_LEFT);
			
			//$hospital_no = sprintf("%07d",$hospital_no);
                         
            //Pad the entered hospital number to the required number of characters
            //$hospital_no = str_pad((int)$this->reg['hospital_oldno'], 6, "0", STR_PAD_LEFT);
			
            $query = "INSERT INTO registry
						SET   `reg_hospital_no` = '".$hospital_no."',
							  `reg_hospital_oldno` = '" . $this->reg['hospital_oldno'] . "',
							  `reg_surname` = '".$this->reg['surname']."',
							  `reg_othernames` = '".$this->reg['othernames']."',
							  `reg_address` = '".$this->reg['address']."',
							  `reg_dob` = '".$this->reg['dob']."',
							  `reg_gender` = '".$this->reg['gender']."',
							  `reg_phone` = '".$this->reg['phone']."',
							  `reg_email` = '".$this->reg['email']."',
							  `reg_date`  = NOW(),
							  `reg_passport`  = '".$newname."',
							  `user_id`  = '".$this->reg['user_id']."'
							  ";
			//die ("<pre>$query</pre>");
							  
            $res = $this->conn->execute($query);

            if (!$this->conn->hasRows($res)){
                throw new Exception();
            }

            if ($res) {
				$reg_id = mysql_insert_id();
				if (isset($this->regextra)){
					$this->insertRegextra($reg_id);
				}
            }
			
			return $reg_id;
        } catch (Exception $e) {
            return 0;
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }




/**
 * Insert extra information about the patient into the registry_extra table
 * @param int $reg_id   The registration ID of the patient in the registry table
 */
	function insertRegextra($reg_id){
		try{
		
/*            $query = "INSERT INTO registry_extra 
							SET `reg_id` = '".$reg_id."',
								`nationality_id` = '".$this->reg['nationality']."',
								`state_id` = '".$this->reg['state']."',
								`regextra_civilstate` = '".$this->reg['civilstate']."',
								`regextra_occupation` = '".$this->reg['occupation']."',
								`regextra_placeofwork` = '".$this->reg['placeofwork']."',
								`regextra_religion` = '".$this->reg['religion']."',
								`regextra_nextofkin` = '".$this->reg['nextofkin']."',
								`regextra_nokaddress` = '".$this->reg['nokaddress']."',
								`regextra_nokphone` = '".$this->reg['nokphone']."',
								`regextra_nokemail` = '".$this->reg['nokemail']."',
								`regextra_nokrelationship` = '".$this->reg['nokrelationship']."',
								`retainership_id` = '".$this->reg['retainership_id']."',
								`regextra_isdependant` = '".$this->reg['isdependant']."',
								`regextra_employee` = '".$this->reg['employee']."',
								`regextra_idcardno` = '".$this->reg['idcardno']."',
								`regextra_cardno` = '".$this->reg['cardno']."',
								`nhis_id` = '".$this->reg['nhis_id']."',
								`regextra_matbookingcat` = '".$this->reg['matbookingcat']."'
							  " ;*/

            $query = "INSERT INTO registry_extra 
							SET `reg_id` = '".$reg_id."',
								`nationality_id` = '".$this->reg['nationality']."',
								`state_id` = '".$this->reg['state']."',
								`regextra_civilstate` = '".$this->reg['civilstate']."',
								`regextra_occupation` = '".$this->reg['occupation']."',
								`regextra_placeofwork` = '".$this->reg['placeofwork']."',
								`regextra_religion` = '".$this->reg['religion']."',
								`regextra_nextofkin` = '".$this->reg['nextofkin']."',
								`regextra_nokaddress` = '".$this->reg['nokaddress']."',
								`regextra_nokphone` = '".$this->reg['nokphone']."',
								`regextra_nokemail` = '".$this->reg['nokemail']."',
								`regextra_nokrelationship` = '".$this->reg['nokrelationship']."'";
								/*`retainership_id` = '".$this->reg['retainership_id']."',
								`regextra_isdependant` = '".$this->reg['isdependant']."',
								`regextra_employee` = '".$this->reg['employee']."',
								`regextra_idcardno` = '".$this->reg['idcardno']."',
								`regextra_cardno` = '".$this->reg['cardno']."',
								`nhis_id` = '".$this->reg['nhis_id']."',
								`regextra_matbookingcat` = '".$this->reg['matbookingcat']."'*/

							  
            $res = $this->conn->execute($query);
			
			
			//Insert retainership details if any
			if (!empty($this->reg['retainership_id']) && !empty($this->reg['idcardno']) && !empty($this->reg['ret_plan'])){
				$query = "INSERT INTO registry_retainership
							VALUES ('0', '$reg_id', 
									'" . $this->reg["retainership_id"] . "', 
									'" . $this->reg["ret_plan"] . "', 
									'" . $this->reg["idcardno"] . "',
									'" . $this->reg["employee"] . "',
									'" . $this->reg["isdependant"] . "'
							)";
				$this->conn->execute($query);
			}


			//Insert NHIS details if any
			if (!empty($this->reg['nhis_id']) && !empty($this->reg['nhis_plan']) && !empty($this->reg['cardno'])){
				$query = "INSERT INTO registry_nhis
							VALUES ('0', '$reg_id', 
									'" . $this->reg["nhis_id"] . "', 
									'" . $this->reg["nhis_plan"] . "', 
									'" . $this->reg["cardno"] . "'
							)";
				$this->conn->execute($query);
			}
//		die("<pre>" . print_r($this->reg, true) . "</pre>");
		}catch (Exception $e){
		
			set_error_handler (array("Error_handler", "my_error_handler"));
			
		}
	}





/**
 * Updates the details of a patient
 * @return int The registration ID of the patient in the registry table
 */
    function updatePatient() {
//		die("<pre>" . print_r($this->reg, true) . "</pre>");
        try {
		 $newname = $this->randomString(12);  
			if (function_exists("imagegif")) {
				$newname = $newname.".gif";
			} elseif (function_exists("imagejpeg")) {
				$newname = $newname.".jpg";
			} elseif (function_exists("imagepng")) {
				$newname = $newname.".png";
			}else{
				$newname = "";
			}

			if (!empty($newname) && !empty($_FILES['passport']['tmp_name']) && $_FILES['passport']['error'] == 0){	
				 move_uploaded_file($_FILES['passport']['tmp_name'], "modules/".$this->temp.$newname);
				 $passport =  "modules/".$this->temp.$newname; 
				 $thumb = new uploadImage();
				 $thumb -> Thumbsize = 300;
				 $thumb -> Thumblocation = 'modules/'.$this->dest;
				 $thumb -> Thumbprefix = $this->reg['surname'];
				 $thumb -> Createthumb("$passport","file");
				 $newname = $this->reg['surname'].$newname;
			 } else {
			 		$newname = "";
			 }
							  
            $query = "UPDATE registry
						SET   `reg_hospital_oldno` = '".$this->reg['hospital_oldno']."',
							  `reg_surname` = '".$this->reg['surname']."',
							  `reg_othernames` = '".$this->reg['othernames']."',
							  `reg_address` = '".$this->reg['address']."',
							  `reg_dob` = '".$this->reg['dob']."',
							  `reg_gender` = '".$this->reg['gender']."',
							  `reg_phone` = '".$this->reg['phone']."',
							  `reg_email` = '".$this->reg['email']."',
							  `reg_date`  = NOW()
						  " ;
			if($newname !="")
				$query .=	" ,`reg_passport`  = '".$newname."'";
			$query .=	" WHERE `reg_id`  = '".$this->reg['reg_id']."'";
							  
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
						
			return $this->reg['reg_id'];
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }





/**
 * Updates the details of a patient in the registry_exra table
 * @param int The registration ID of the patient in the registry table
 */
	function updateRegextra($reg_id){
		try{
		
            $query = "UPDATE registry_extra 
							SET `reg_id` = '".$reg_id."',
								`nationality_id` = '".$this->reg['nationality']."',
								`state_id` = '".$this->reg['state']."',
								`regextra_civilstate` = '".$this->reg['civilstate']."',
								`regextra_occupation` = '".$this->reg['occupation']."',
								`regextra_placeofwork` = '".$this->reg['placeofwork']."',
								`regextra_religion` = '".$this->reg['religion']."',
								`regextra_nextofkin` = '".$this->reg['nextofkin']."',
								`regextra_nokaddress` = '".$this->reg['nokaddress']."',
								`regextra_nokphone` = '".$this->reg['nokphone']."',
								`regextra_nokemail` = '".$this->reg['nokemail']."',
								`regextra_nokrelationship` = '".$this->reg['nokrelationship']."'
								WHERE `reg_id`  = '".$this->reg['reg_id']."'
							  " ;
								/*`retainership_id` = '".$this->reg['retainership_id']."',
								`regextra_isdependant` = '".$this->reg['isdependant']."',
								`regextra_employee` = '".$this->reg['employee']."',
								`regextra_idcardno` = '".$this->reg['idcardno']."',
								`regextra_cardno` = '".$this->reg['cardno']."',
								`nhis_id` = '".$this->reg['nhis_id']."',
								`regextra_matbookingcat` = '".$this->reg['matbookingcat']."'*/
            $res = $this->conn->execute($query);


			//Update retainership details if need be
			if (!empty($this->reg['retainership_id']) && !empty($this->reg['idcardno']) && !empty($this->reg['ret_plan'])){
				//Make sure the value is either 0 or 1 so as to prevent any error from the DB server
				$this->reg["isdependant"] = empty($this->reg["isdependant"]) ? "0" : "1";
				$query = "UPDATE registry_retainership
							SET 
								retainership_id = '" . $this->reg["retainership_id"] . "', 
								retplan_id = '" . $this->reg["ret_plan"] . "', 
								regret_idcardno = '" . $this->reg["idcardno"] . "',
								regret_employee_name = '" . $this->reg["employee"] . "',
								regret_isdependant = '" . $this->reg["isdependant"] . "'
							WHERE reg_id = '" . $this->reg['reg_id'] . "'";
				$this->conn->execute($query);
			}


			//Update NHIS details if need be
			if (!empty($this->reg['nhis_id']) && !empty($this->reg['nhis_plan']) && !empty($this->reg['cardno'])){
				$query = "UPDATE registry_nhis
							SET
								retainership_id = '" . $this->reg["nhis_id"] . "', 
								nhisplan_id = '" . $this->reg["nhis_plan"] . "', 
								regnhis_cardno = '" . $this->reg["cardno"] . "'
							WHERE reg_id = '" . $this->reg['reg_id'] . "'";
				$this->conn->execute($query);
			}


            $res = $this->conn->execute($query);

		}catch (Exception $e){
		
			set_error_handler (array("Error_handler", "my_error_handler"));
			
		}
	}

/**
 * Creates a new medical transaction for a patient by inserting into the patient_admission table
 * @param string $reg_hospital_no   The hospital number of the patient
 * @return int                      Returns the insert ID from the patient_admission table
 */
function insertPatadm($reg_hospital_no)
{
try{
			$query = "INSERT INTO patient_admission
						SET `reg_hospital_no` = '".$reg_hospital_no."'";
						
            $res = $this->conn->execute($query);
		 if ($res) {
		 	$patadm_id = mysql_insert_id();
			return $patadm_id;
			}
	}
	catch (Exception $e){
	
			set_error_handler (array("Error_handler", "my_error_handler"));
	}

}



/**
 * Saves the referrer details of a patient
 * @param int $patadmid     The ID of this medical transaction in the patient_admission table
 * @param string $tablename The table name for referrer
 * @param string $prefix    The string to be used as prefix for column names in the table
 */
function insertReferrer($patadmid, $tablename, $prefix)
{
try{
			$query = "INSERT INTO $tablename
						SET `patadm_id` = '".$patadmid."',
							`ct_id` = '".$this->reg['ct_id']."',
							`refferer_id` = '".$this->reg['referrer']."',
							`".$prefix."_reffererdesc` = '".$this->reg['referrerdesc']."',
							`".$prefix."_dateattended` = NOW(),
							`consultant_id` = '".$this->reg['consultant_id']."'";
						
            $res = $this->conn->execute($query);
		 if ($res) {
			}
	}
	catch (Exception $e){
	
			set_error_handler (array("Error_handler", "my_error_handler"));
	}

}




/**
 * Gets the consultant attached to a clinic
 * @param int $clinic_id    The ID of the clinic under consideration
 * @return <type>
 */
function getConsultant($clinic_id){
	try{
		$query = "SELECT clinic_id from consultant
					WHERE clinic_id = '".$clinic_id."'";
					
		$res = $this->conn->execute($query);
		if($res){
			while($row = mysql_fetch_array($res)){
				return $row['clinic_id'];
			} 
		}
	} catch (Exception $e) {
			set_error_handler (array("Error_handler", "my_error_handler"));
	}

}



function insertAppointment($patadmid)
{
try{
			$query = "INSERT INTO appointment
						SET `patadm_id` = '".$patadmid."',
							`consultant_id` = '".$this->reg['consultant_id2']."',
							`user_id` = '".$this->reg['user_id']."',
							`app_starttime` = '".$this->reg['app_starttime']."',
							`app_endtime` = '".$this->reg['app_endtime']."',
							`dept_id` = '".$this->reg_dept."',
							`clinic_id` = '".$this->reg_clinic."',
							`app_status` = '0'";
						
            $res = $this->conn->execute($query);
	}
	catch (Exception $e){
	
			set_error_handler (array("Error_handler", "my_error_handler"));
	}

}

		function checkHospitalNo($hospital_no)
			{
				try{
				$query = "SELECT * FROM registry 
							WHERE reg_hospital_no = '".$hospital_no."'";
				$res = $this->conn->execute($query);
				
				 if ($res) {
				 $num = mysql_num_rows($res);
				 return $num;
										
					}
					}
					catch (Exception $e){
					
							set_error_handler (array("Error_handler", "my_error_handler"));
					}
		
			}

function Load_from_key($reg_id)
{
try{

$query = "SELECT * FROM registry 
			LEFT JOIN registry_extra 
			ON registry.reg_id=registry_extra.reg_id
			WHERE registry.reg_id = '".$reg_id."'";
$res = $this->conn->execute($query);

 if ($res) {
	while($row=mysql_fetch_assoc($res)){
		$this->reg_hospital_no = $row["reg_hospital_no"];
		$this->reg_hospital_oldno = $row["reg_hospital_oldno"];
		$this->reg_surname = $row["reg_surname"];
		$this->reg_othernames = $row["reg_othernames"];
		$this->reg_address = $row["reg_address"];
		$this->reg_dob = $row["reg_dob"];
		$this->reg_phone = $row["reg_phone"];
		$this->reg_email = $row["reg_email"];
		$this->reg_date = $row["reg_date"];
		$this->user_id = $row["user_id"];
		$this->reg_gender = $row["reg_gender"];
		$this->reg_passport = $row["reg_passport"];
		
		$this->nationality_id = $row["nationality_id"];
		$this->state_id = $row["state_id"];
		$this->reg_civilstate = $row["regextra_civilstate"];
		$this->reg_occupation = $row["regextra_occupation"];
		$this->reg_placeofwork = $row["regextra_placeofwork"];
		$this->reg_religion = $row["regextra_religion"];
		$this->reg_nextofkin = $row["regextra_nextofkin"];
		$this->reg_nokrelationship = $row["regextra_nokrelationship"];
		$this->reg_nokaddress = $row["regextra_nokaddress"];
		$this->reg_nokphone = $row["regextra_nokphone"];
		$this->reg_nokemail = $row["regextra_nokemail"];
/*		$this->retainership_id = $row["retainership_id"];
		$this->reg_isdependant = $row["regextra_isdependant"];
		$this->reg_employee = $row["regextra_employee"];
		$this->reg_idcardno = $row["regextra_idcardno"];
		$this->reg_cardno = $row["regextra_cardno"];
		$this->nhis_id = $row["nhis_id"];*/
		$this->reg_matbookingcat = $row["regextra_matbookingcat"];
	}
	
	
	//Get patient's retainership deets if any
	$query = "SELECT * FROM registry_retainership regret INNER JOIN retainership ret INNER JOIN retainership_plan retplan
				ON regret.retainership_id = ret.retainership_id AND regret.retplan_id = retplan.retplan_id
				WHERE regret.reg_id = '" . $reg_id . "'";
	$result = $this->conn->execute($query);
	if ($this->conn->hasRows($query, 1)){
		$this->retDeets = mysql_fetch_array ($result, MYSQL_ASSOC);
	} else $this->retDeets = array();


	//Get patient's NHIS deets if any
	$query = "SELECT * FROM registry_nhis nhis INNER JOIN retainership ret INNER JOIN nhis_plan nhisplan
				ON nhis.retainership_id = ret.retainership_id AND nhis.nhisplan_id = nhisplan.nhisplan_id
				WHERE nhis.reg_id = '" . $reg_id . "'";
	$result = $this->conn->execute($query);
	if ($this->conn->hasRows($query, 1)){
		$this->nhisDeets = mysql_fetch_array ($result, MYSQL_ASSOC);
	} else $this->nhisDeets = array();
	
} else{
		$this->reg_hospital_no = "";
		$this->reg_hospital_oldno = "";
		$this->reg_surname = "";
		$this->reg_othernames = "";
		$this->reg_address = "";
		$this->reg_dob = "";
		$this->reg_phone = "";
		$this->reg_email = "";
		$this->reg_date = "";
		$this->user_id = "";
		$this->reg_gender = "";
		$this->reg_passport = "";
		
		$this->nationality_id = "";
		$this->state_id = "";
		$this->reg_civilstate = "";
		$this->reg_occupation = "";
		$this->reg_placeofwork = "";
		$this->reg_religion = "";
		$this->reg_nextofkin = "";
		$this->reg_nokrelationship = "";
		$this->reg_nokaddress = "";
		$this->reg_nokphone = "";
		$this->reg_nokemail = "";
		$this->retainership_id = "";
		$this->reg_isdependant = "";
		$this->reg_employee = "";
		$this->reg_idcardno = "";
		$this->reg_cardno = "";
		$this->nhis_id = "";
		$this->reg_matbookingcat = "";
	}
		

 } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
}


function loadPatientDetails($hospital_no)
{
try{

$query = "SELECT *, YEAR(CURDATE()) - YEAR(registry.reg_dob) AS age FROM registry 
			LEFT JOIN registry_extra 
			ON registry.reg_id=registry_extra.reg_id
			WHERE registry.reg_hospital_no = '".$hospital_no."'";
$res = $this->conn->execute($query);

 if ($res) {
	while($row=mysql_fetch_assoc($res)){
		$this->reg_hospital_no = $row["reg_hospital_no"];
		$this->reg_hospital_oldno = $row["reg_hospital_oldno"];
		$this->reg_surname = $row["reg_surname"];
		$this->reg_othernames = $row["reg_othernames"];
		$this->reg_address = $row["reg_address"];
		$this->reg_dob = $row["reg_dob"];
		$this->reg_phone = $row["reg_phone"];
		$this->reg_email = $row["reg_email"];
		$this->reg_date = $row["reg_date"];
		$this->user_id = $row["user_id"];
		$this->reg_gender = $row["reg_gender"];
		$this->reg_passport = $row["reg_passport"];
		
		$this->nationality_id = $row["nationality_id"];
		$this->state_id = $row["state_id"];
		$this->reg_civilstate = $row["regextra_civilstate"];
		$this->reg_occupation = $row["regextra_occupation"];
		$this->reg_placeofwork = $row["regextra_placeofwork"];
		$this->reg_religion = $row["regextra_religion"];
		$this->reg_nextofkin = $row["regextra_nextofkin"];
		$this->reg_nokrelationship = $row["regextra_nokrelationship"];
		$this->reg_nokaddress = $row["regextra_nokaddress"];
		$this->reg_nokphone = $row["regextra_nokphone"];
		$this->reg_nokemail = $row["regextra_nokemail"];
		/*$this->retainership_id = $row["retainership_id"];
		$this->reg_isdependant = $row["regextra_isdependant"];
		$this->reg_employee = $row["regextra_employee"];
		$this->reg_idcardno = $row["regextra_idcardno"];
		$this->reg_cardno = $row["regextra_cardno"];
		$this->nhis_id = $row["nhis_id"];*/
		$this->reg_matbookingcat = $row["regextra_matbookingcat"];
		$this->reg_age = $row["age"];
	}
} else{
		$this->reg_hospital_no = "";
		$this->reg_hospital_oldno = "";
		$this->reg_surname = "";
		$this->reg_othernames = "";
		$this->reg_address = "";
		$this->reg_dob = "";
		$this->reg_phone = "";
		$this->reg_email = "";
		$this->reg_date = "";
		$this->user_id = "";
		$this->reg_gender = "";
		$this->reg_passport = "";
		
		$this->nationality_id = "";
		$this->state_id = "";
		$this->reg_civilstate = "";
		$this->reg_occupation = "";
		$this->reg_placeofwork = "";
		$this->reg_religion = "";
		$this->reg_nextofkin = "";
		$this->reg_nokrelationship = "";
		$this->reg_nokaddress = "";
		$this->reg_nokphone = "";
		$this->reg_nokemail = "";
		$this->retainership_id = "";
		$this->reg_isdependant = "";
		$this->reg_employee = "";
		$this->reg_idcardno = "";
		$this->reg_cardno = "";
		$this->nhis_id = "";
		$this->reg_matbookingcat = "";
		$this->reg_age = "";
	}
		

 } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
}


public  function getReg_age()
{
return $this->reg_age;
} // end getReg_age()

public  function getReg_hospital_no()
{
return $this->reg_hospital_no;
} // end getReg_hospital_no()

public  function getReg_hospital_oldno()
{
return $this->reg_hospital_oldno;
} // end getReg_hospital_oldno()

public  function getReg_surname()
{
return $this->reg_surname;

} // end getReg_surname()

public  function getReg_othernames()
{
return $this->reg_othernames;

} // end getReg_othernames()

public  function getReg_address()
{
return $this->reg_address;

} // end getReg_address()

public  function getReg_dob()
{
return $this->reg_dob;

} // end getReg_dob()

public  function getReg_phone()
{
return $this->reg_phone;

} // end getReg_phone()

public  function getReg_email()
{
return $this->reg_email;

} // end getReg_email()

public  function getNationality_id()
{
return $this->nationality_id;

} // end getNationality_id()

public  function getState_id()
{
return $this->state_id;

} // end getState_id()

public  function getReg_civilstate()
{
return $this->reg_civilstate;

} // end getReg_civilstate()

public  function getReg_occupation()
{
return $this->reg_occupation;

} // end getReg_occupation()

public  function getReg_placeofwork()
{
return $this->reg_placeofwork;

} // end getReg_placeofwork()

public  function getReg_religion()
{
return $this->reg_religion;

} // end getReg_religion()

public  function getReg_nextofkin()
{
return $this->reg_nextofkin;

} // end getReg_nextofkin()

public  function getReg_nokrelationship()
{
return $this->reg_nokrelationship;

} // end getReg_nokrelationship()

public  function getReg_nokaddress()
{
return $this->reg_nokaddress;

} // end getReg_nokaddress()

public  function getReg_nokphone()
{
return $this->reg_nokphone;

} // end getReg_nokphone()

public  function getReg_nokemail()
{
return $this->reg_nokemail;

} // end getReg_nokemail()


public  function getReg_bloodgrp()
{
return $this->reg_bloodgrp;

} // end getReg_bloodgrp()

public  function getReg_genotype()
{
return $this->reg_genotype;

} // end getReg_genotype()

public  function getReg_labrefno()
{
return $this->reg_labrefno;

} // end getReg_labrefno()

public  function getReg_rh()
{
return $this->reg_rh;

} // end getReg_rh()

public  function getReg_hb()
{
return $this->reg_hb;

} // end getReg_hb()

public  function getReg_condition()
{
return $this->reg_condition;

} // end getReg_condition()

public  function getReg_xrayno()
{
return $this->reg_xrayno;

} // end getReg_xrayno()

public  function getReg_status()
{
return $this->reg_status;

} // end getReg_status()

public  function getReg_passport()
{
return $this->reg_passport;

} // end getReg_passport()

public  function getRetainership_id()
{
return $this->retainership_id;

} // end getRetainership_id()

public  function getReg_date()
{
return $this->reg_date;

} // end getReg_date()

public  function getUser_id()
{
return $this->user_id;

} // end getUser_id()

public  function getReg_gender()
{
return $this->reg_gender;

} // end getReg_gender()


public  function getReg_isdependant()
{
return $this->reg_isdependant;

} // end getReg_isdependant()

public  function getReg_idcardno()
{
return $this->reg_idcardno;

} // end getReg_idcardno()

public  function getReg_employee()
{
return $this->reg_employee;

} // end getReg_employee()

public  function getReg_cardno()
{
return $this->reg_cardno;

} // end getReg_cardno()

public  function getNhis_id()
{
return $this->nhis_id;

} // end getNhis_id()

public  function getReg_matbookingcat()
{
return $this->reg_matbookingcat;

} // end getReg_matbookingcat()




function simpleSearch(){

try{
 
}catch(Exception $e){

			set_error_handler (array("Error_handler", "my_error_handler"));

}

}



function randomString($randStringLength){
	try{

	$timestring = microtime();
	$secondsSinceEpoch=(integer) substr($timestring, strrpos($timestring, " "), 100);
	$microseconds=(double) $timestring;
	$seed = mt_rand(0,1000000000) + 10000000 * $microseconds + $secondsSinceEpoch;
	mt_srand($seed);
	$randstring = "";
	for($i=0; $i < $randStringLength; $i++)
		{
		$randstring .= mt_rand(0, 9);
		}
	return($randstring);
	} catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }


/**
 * Saves the transaction details of an action on a patient that requires payment
 * @param int $patadmid The ID of the current medical transaction of the patient in the patient_admission table
 */
    function insertTransaction($patadmid)
    {
        try{
                //Get the service ID that corresponds to registration in the clinic of the patient
                $trav = new Pat_Service_Traverser($this->reg_clinic, "Unit", "registration", "Servicename");
                $serviceID = $trav->getServiceID();
                $serviceName = $trav->getServiceName($serviceID);
                $price = $trav->getServicePrice($serviceID);
                $trans_no = $this->randomString(rand(12, 20));
                $hospitalNo = $this->reg_hospital_no = isset($this->reg_hospital_no) ? $this->reg_hospital_no : $this->getHospitalNoFromPatAdm($patadmid);
                /*$query = "INSERT INTO pat_transtotal
                            SET `patadm_id` = '" . $patadmid."',
                                `pattotal_transno` = '".$trans_no."',
                                `pattotal_status` = '1',
                                `pattotal_date` = NOW(),
                                `pattotal_newstatus` = '0',
                                `dept_id` = '" . $this->reg_dept . "',
                                `clinic_id` = '" . $this->reg_clinic . "'";
                 $result = $this->conn->execute($query);
                 */

                //Enclose this operation in a database transaction
                $this->conn->startTransaction();
                
                //Insert into the main transaction table
                $transHandler = new TransactionHandler();
                $inserted = $transHandler->insertTransactionTotal($patadmid, $hospitalNo, $trans_no, $price, $price, 0, 3, '', $this->reg_dept, $this->reg_clinic, $_SESSION[session_id() . "userID"], 0);
//                $query = "INSERT INTO pat_transtotal
//                            SET `patadm_id` = '" . $patadmid . "',
//                                `reg_hospital_no` = '" . $hospitalNo . "',
//                                `pattotal_transno` = '".$trans_no."',
//                                `pattotal_totalamt` = '$price',
//                                `pattotal_totalamt_nhis` = '$price',
//                                `pattotal_status` = '0',    #Patient has not paid
//                                `pattotal_date` = NOW(),
//                                `pattotal_newstatus` = '0',
//                                `pattotal_invoice_type` = '3',
//                                `dept_id` = '" . $this->reg_dept . "',
//                                `clinic_id` = '" . $this->reg_clinic . "',
//                                `user_id` = '" . $_SESSION[session_id() . "userID"] . "'";
//                //echo ("<p><pre>$query</pre></p>");
//
//                $result = $this->conn->execute($query);

                //Insert into the transaction details table if the initial insertion was successful
                if ($inserted){
                    //Get the insert ID above
                    $transID = $transHandler->getInsertID();

                    //Now insert into the 2 transaction details table
                    $query = "INSERT INTO pat_serviceitem (patservice_type, patservice_itemid, patservice_name)
                                VALUES ('1', '$serviceID', '$serviceName')";
                    //echo ("<p><pre>$query</pre></p>");
                    $result = $this->conn->execute($query);

                    if ($this->conn->hasRows($result)){
                        //Get the patservice_id from the last query
                        $patservice_id = mysql_insert_id($this->conn->getConnectionID());

                        //Now, carry out the final insertion
                        $inserted = $transHandler->insertTransactionItems(array($price), array($price), array($patservice_id), 3, $transID, array(1));
//                        $query = "INSERT INTO pat_transitem
//                                    SET pattotal_id = '$transID',
//                                        patitem_amount = '$price',
//                                        patservice_id = '$patservice_id',
//                                        patitem_totalqty = '1',
//                                        patitem_paymentsource = '3'";
//                        //echo ("<p><pre>$query</pre></p>");
//                        $result = $this->conn->execute($query);

                        //Commit or roll-back depending on the result of insertion
                        if ($inserted)
                            $this->conn->commitTransaction();
                        else $this->conn->rollBackTransaction();

                    } else $this->conn->rollBackTransaction();

                } else $this->conn->rollBackTransaction();
        } catch (Exception $e){
            //set_error_handler (array("Error_handler", "my_error_handler"));
        }
        //die ("Done with trans insertion");
    }   //END insertTransaction()



    public function getHospitalNoFromPatAdm($patAdmID){
        $patAdmID = (int)$patAdmID;
        $query = "SELECT reg_hospital_no FROM patient_admission
                    WHERE patadm_id = '$patAdmID'";
        $result = $this->conn->execute($query);
        if ($this->conn->hasRows($result, 1)){
            $retVal = mysql_fetch_array ($result, MYSQL_ASSOC);
            $retVal = $retVal["reg_hospital_no"];
        } else $retVal = "";
        return $retVal;
    }   //END getHospitalNoFromPatAdm()




}   //END class

?>
