<?php
/**
 * Deals with Vital Signs issues like saving, retrieving and editing
 *
 * @author Faniran, Oyetunde
 *
 * @package Patient_Care
 */

/**
 * Deals with Vital Signs issues like saving, retrieving and editing
 *
 * @author Faniran, Oyetunde
 *
 * @package Patient_Care
 */
class patVitalSigns {
    /**
     * @var DBConf  An object of the DBConf class
     */
	protected $conn;

	function __construct (){
		$this->conn = new DBConf();
	}   //END __construct()


/**
 * Saves the vital signs of patients as entered by a nurse
 * @param array $postArray  A copy of the $_POST array that would be generated whenever the Vital Signs form is submitted by a nurse
 * @return boolean          Returns true on success or false on failure
 */
	public function saveVitalSigns($postArray, $clinic_id){
		//Escape the posted array
		$postArray = admin_Tools::doEscape($postArray, $this->conn);

        $userID = $_SESSION[session_id() . "userID"];
		//Convert the elements in the array to variables
		foreach ($postArray as $k=>$v){
			$$k = $v;
        }

		if (isset($temp, $pulse, $resp, $bp, $weight, $others, $pid, $aid, $ed)){
			//Confirm that the appID is valid and corresponds to the given patAdmID
			$query = "SELECT * FROM appointment
						WHERE app_id = '$aid' AND patadm_id = '$pid'";
			$result = $this->conn->execute($query);

			//If row found then carry on else abort
			if ($this->conn->hasRows($result, 1)){
                $temp_unit = ($temp_unit == "C" || $temp_unit == "F") ? $temp_unit : "C";
				$query = "INSERT INTO patient_vitalsigns (patadm_id, user_id, patvital_temp, patvital_temp_unit, patvital_pulse,
                                patvital_resp, patvital_bp, patvital_weight, patvital_others, patvital_datetaken)
							VALUES('$pid', '$userID', '$temp', '$temp_unit', '$pulse', '$resp', '$bp', '$weight', '$others', NOW())";
				$result = $this->conn->execute($query);
				if ($this->conn->hasRows($result, 1)){
					$vitalsigns_id = $retVal = mysql_insert_id();
                    //Save custom field entries, if any
                    $custom_entries = isset($postArray['formbuilder_custom']) ? $postArray['formbuilder_custom'] : '';
                    if (!empty($custom_entries)){
                        $fBuilder = new FormBuilder();
                        $fBuilder->saveFormEntry($custom_entries, $clinic_id, 1, $vitalsigns_id, $userID);
                    }
                } else {
                    $retVal = false;
                }
			} else $retVal = false;
		} else $retVal = false;

		return $retVal;

	}   //END saveVitalSigns()




/**
 * Updates the vital signs of a patient
 * @param array $postArray  The $_POST array generated on submission of the Edit Vital Signs form
 * @return boolean          Returns true on success or false on failure
 */
	public function editVitalSigns($postArray){
		//Escape the posted array
		$postArray = admin_Tools::doEscape($postArray, $this->conn);

		//Convert the elements in the array to variables
		foreach ($postArray as $k=>$v)
			$$k = $v;

		if (isset($temp, $temp_unit, $pulse, $resp, $bp, $weight, $others, $pid, $aid, $ed, $vsi, $sButton)){
            $temp_unit = ($temp_unit == "C" || $temp_unit == "F") ? $temp_unit : "C";
			$query = "UPDATE patient_vitalsigns
						SET patvital_temp = '$temp',
                            patvital_temp_unit = '$temp_unit',
							patvital_pulse = '$pulse',
							patvital_resp = '$resp',
							patvital_bp = '$bp',
							patvital_weight = '$weight',
							patvital_others = '$others',
							patvital_datetaken = NOW()
						WHERE patvital_id = '$vsi' AND patadm_id = '$pid'";
			$result = $this->conn->execute($query);
			if ($this->conn->hasRows($result, 1))
				$retVal = true;
			else $retVal = false;
		} else $retVal = false;

		return $retVal;

	}   //END editVitalSigns()





/**
 * Determines if the vital signs of a patient has been taken on the current date or not
 * @param int $patAdmID     The current medical transaction ID of the patient (from the patient_admission table)
 * @return boolean          Returns true if the vital signs of a patient has already been taken today else false
 */
	public function getVitalSignsStatus($patAdmID){
		$query = "SELECT COUNT(*) FROM patient_vitalsigns
					WHERE patadm_id = '$patAdmID' AND DATE(patvital_datetaken) = CURDATE()"; //die ($query);
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			$row = mysql_fetch_array ($result);
			if ($row[0] != "0")
				$retVal = true;
			else $retVal = false;
		} else $retVal = false;
		return $retVal;
	}   //END getVitalSignsStatus()





/**
 * Retrieves the details of the vital signs of a patient that was taken on the current date
 * @param int $patAdmID The current medical transaction ID of the patient (from the patient_admission table)
 * @return mixed        Returns an array containing the details or false on failure
 */
	public function getVitalSignsDeets($patAdmID){
		$query = "SELECT * FROM patient_vitalsigns
					WHERE patadm_id = '$patAdmID'                   #If it was recorded for the current medical transaction of the patient
                        AND DATE(patvital_datetaken) = CURDATE()    #If it was taken on the current date";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			$retVal = mysql_fetch_array ($result, MYSQL_ASSOC);
		} else {
            $retVal = false;
        }
		return $retVal;
	}   //END getVitalSignsDeets()



/**
 * Checks if a doctor has made use of the vital signs taken by a nurse. If a doctor has made use of the vital signs of a patient,
 * no nurse should be allowed to edit it again. So, this method can be used to determine whether a doctor has made use of a vital signs
 * so as to know whether it can be edited or not.
 * @param int $patAdmID The current medical transaction ID of the patient (from the patient_admission table)
 * @return mixed        Returns true if it has been used by a doctor, false otherwise
 */
	public function usedByDoctor($patAdmID){
		$query = "SELECT patvital_usedbydoctor 'used' FROM patient_vitalsigns
					WHERE patadm_id = '$patAdmID'                   #If it was recorded for the current medical transaction of the patient
                        AND DATE(patvital_datetaken) = CURDATE()    #If it was taken on the current date";
        //die ("<pre>$query</pre>");
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
            $retVal = $row["used"] == "1" ? true : false;
		} else $retVal = false;
		return $retVal;
	}   //END usedByDoctor()



/**
 * Sets the a vital signs measurement to used so that no nurse would be able to edit it again.
 * @param <type> $vitalsID
 */
    public function setToUsed($vitalsID){
        $vitalsID = (int)$vitalsID;
        $query = "UPDATE patient_vitalsigns
                    SET patvital_usedbydoctor = '1'
                    WHERE patvital_id = '$vitalsID'";
        $this->conn->execute($query);
    }   //END setToUsed()




}	//END class
?>