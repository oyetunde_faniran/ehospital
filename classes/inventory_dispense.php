<?php
/**
 * Description of inventory_dispense
 *
 * @author Faniran, Oyetunde
 */
class inventory_dispense {

    /**
     * @var DBConf object   An object of the DBConf class, which handles communication with the DB
     */
    protected $conn;
    /**
     *
     * @var boolean = true whenever an error occurs during any operation
     */
    public $error;
    /**
     *
     * @var string  Contains the error message of the error that occurred during the last operation if any
     */
    public $errorMsg;
    
    public $dispenseID;

    /**
     * Class Constructor
     */
    function __construct() {
        $this->conn = new DBConf();
        $this->error = false;
        $this->errorMsg = "";
        $dispenseID = 0;
    }   //END __construct
    
    
    
    
    /**
     * Searches all available drugs for any appearance of the $searchText
     * @param string $searchText    The text to search for
     * @param int $searchField      When = 0, the commercial name is to be used for the search, else the medical name is used
     * @param int $locationID       The ID of the location to be searched
     * @return array                Returns an array containing all found drugs or an empty array if none was found.
     */
    public function doDrugSearch($searchText, $searchField, $locationID){
        $retVal = array();
        $locationID = (int)$locationID;
        try {
            $searchText = admin_Tools::doEscape($searchText, $this->conn);
            $searchField = $searchField == 1 ? "medname.inventory_medical_name" : "d.drug_name";
            if (empty($searchText)){
                throw new Exception();
            }
//            $query = "SELECT SUM(ibd.inventory_batchdetails_quantity) 'quantity_in_stock', d.*
//                        FROM drug d
//                            INNER JOIN inventory_batch_details ibd
//                            INNER JOIN inventory_batch ib
//                        ON d.drug_id = ibd.drug_id
//                        	AND ibd.inventory_batch_id = ib.inventory_batch_id
//                        WHERE LOWER($searchField) = LOWER('$searchText')
//                            AND ibd.inventory_batchdetails_quantity > 0
//                            AND ib.inventory_location_id = '$locationID'
//                        GROUP BY d.drug_id
//
//                     UNION ALL
//
//                     SELECT SUM(ibd.inventory_batchdetails_quantity) 'quantity_in_stock', d.*
//                        FROM drug d
//                            INNER JOIN inventory_batch_details ibd
//                            INNER JOIN inventory_batch ib
//                        ON d.drug_id = ibd.drug_id
//                        	AND ibd.inventory_batch_id = ib.inventory_batch_id
//                        WHERE LOWER($searchField) LIKE LOWER('%$searchText%')
//                            AND LOWER($searchField) <> LOWER('$searchText')
//                            AND ibd.inventory_batchdetails_quantity > 0
//                            AND ib.inventory_location_id = '$locationID'
//                        GROUP BY d.drug_id";




            $query = "SELECT SUM(ibd.inventory_batchdetails_quantity) 'quantity_in_stock',
                        d.*,
                        medname.inventory_medical_name 'drug_desc',
                        f.inventory_form_name 'drug_dosageform',
                        p.inventory_presentation_name 'drug_presentation',
                        m.inventory_manufacturer_name 'drug_manufacturer'
                     FROM drug d
                        INNER JOIN inventory_medical_names medname
                        INNER JOIN inventory_forms f
                        INNER JOIN inventory_presentations p
                        INNER JOIN inventory_manufacturers m
                        INNER JOIN inventory_batch_details ibd
                        INNER JOIN inventory_batch ib
                     ON d.drug_desc = medname.inventory_medical_name_id
                        AND d.drug_dosageform = f.inventory_form_id
                        AND d.drug_presentation = p.inventory_presentation_id
                        AND d.drug_manufacturer = m.inventory_manufacturer_id
                        AND d.drug_id = ibd.drug_id
                        AND ibd.inventory_batch_id = ib.inventory_batch_id
                     WHERE LOWER($searchField) = LOWER('$searchText')
                        AND ibd.inventory_batchdetails_quantity > 0
                        AND ib.inventory_location_id = '$locationID'
                     GROUP BY d.drug_id

                     UNION ALL

                     SELECT SUM(ibd.inventory_batchdetails_quantity) 'quantity_in_stock',
                        d.*,
                        medname.inventory_medical_name 'drug_desc',
                        f.inventory_form_name 'drug_dosageform',
                        p.inventory_presentation_name 'drug_presentation',
                        m.inventory_manufacturer_name 'drug_manufacturer'
                     FROM drug d
                        INNER JOIN inventory_medical_names medname
                        INNER JOIN inventory_forms f
                        INNER JOIN inventory_presentations p
                        INNER JOIN inventory_manufacturers m
                        INNER JOIN inventory_batch_details ibd
                        INNER JOIN inventory_batch ib
                     ON d.drug_desc = medname.inventory_medical_name_id
                        AND d.drug_dosageform = f.inventory_form_id
                        AND d.drug_presentation = p.inventory_presentation_id
                        AND d.drug_manufacturer = m.inventory_manufacturer_id
                        AND d.drug_id = ibd.drug_id
                        AND ibd.inventory_batch_id = ib.inventory_batch_id
                     WHERE LOWER($searchField) LIKE LOWER('%$searchText%')
                        AND LOWER($searchField) <> LOWER('$searchText')
                        AND ibd.inventory_batchdetails_quantity > 0
                        AND ib.inventory_location_id = '$locationID'
                     GROUP BY d.drug_id";
            //die ("<pre>$query</pre>");
            $result = $this->conn->run($query);
            if ($this->conn->hasRows($result)){
                while ($row = mysql_fetch_assoc($result)){
                    $retVal[] = $row;
                }   //END while
            }   //END if
        } catch (Exception $e) {}
        return $retVal;
    }   //END doDrugSearch()
    
    
    
    
    /**
     * Gets all the properties of a drug in preparation for dispensing it
     * @param int $drugID           The ID of the drug whose properties would be fetched
     * @param int $locationID       The ID of the location to be searched
     * @param int $batchDetailsID   The ID of the supply batch of items from which quantities should be fetched
     * @return mixed                Returns an array if the drug was found, else an empty string 
     */
    public function getDrugForDispense($drugID, $locationID, $batchDetailsID = 0){
        $drugID = (int)$drugID;
        $locationID = (int)$locationID;
        $batchDetailsID = (int)$batchDetailsID;
        $retVal = "";
        $addQuery = !empty($batchDetailsID) ? " AND ibd.inventory_batchdetails_id = '$batchDetailsID' " : "";
        $query = "SELECT SUM(ibd.inventory_batchdetails_quantity) 'quantity_in_stock', d.*,
                        mnames.inventory_medical_name 'drug_desc', m.inventory_manufacturer_name 'drug_manufacturer'
                    FROM drug d
                        INNER JOIN inventory_medical_names mnames
                        INNER JOIN inventory_manufacturers m
                        INNER JOIN inventory_batch_details ibd
                        INNER JOIN inventory_batch ib
                    ON d.drug_id = ibd.drug_id
                        AND d.drug_desc = mnames.inventory_medical_name_id
                        AND d.drug_manufacturer = m.inventory_manufacturer_id
                        AND ibd.inventory_batch_id = ib.inventory_batch_id
                    WHERE d.drug_id = '$drugID'
                        AND ib.inventory_location_id = '$locationID'
                        AND ibd.inventory_batchdetails_quantity > 0
                        $addQuery";
        $query = "SELECT SUM(ibd.inventory_batchdetails_quantity) 'quantity_in_stock',
                        d.*,
                        mnames.inventory_medical_name 'drug_desc',
                        m.inventory_manufacturer_name 'drug_manufacturer',
                        f.inventory_form_name 'drug_dosageform',
                        p.inventory_presentation_name 'drug_presentation'
                    FROM drug d
                        INNER JOIN inventory_medical_names mnames
                        INNER JOIN inventory_manufacturers m
                        INNER JOIN inventory_forms f
                        INNER JOIN inventory_presentations p
                        INNER JOIN inventory_batch_details ibd
                        INNER JOIN inventory_batch ib
                    ON d.drug_id = ibd.drug_id
                        AND d.drug_desc = mnames.inventory_medical_name_id
                        AND d.drug_manufacturer = m.inventory_manufacturer_id
                        AND d.drug_dosageform = f.inventory_form_id
                        AND d.drug_presentation = p.inventory_presentation_id
                        AND ibd.inventory_batch_id = ib.inventory_batch_id
                    WHERE d.drug_id = '$drugID'
                        AND ib.inventory_location_id = '$locationID'
                        AND ibd.inventory_batchdetails_quantity > 0
                        $addQuery";
        //die ("<pre>$query</pre>");
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $retVal =  mysql_fetch_assoc($result);
        }
        return $retVal;
    }   //END getDrugForDispense()
    
    
    
    public function doDispense($dispenseArray, $drugLabelArray, $batchDetailsArray, $hospitalNumber, $locationID, $userID){
        $dispenseArray = admin_Tools::doEscape($dispenseArray, $this->conn);
        $drugLabelArray = admin_Tools::doEscape($drugLabelArray, $this->conn);
        $batchDetailsArray = admin_Tools::doEscape($batchDetailsArray, $this->conn);
        $userID = (int)$userID;
        $locationID = (int)$locationID;
        $this->error = false;
        $this->errorMsg = "";
        try{
            $this->conn->startTransaction();
            //Create an instance of dispense in the DB first
            $query = "INSERT INTO inventory_dispense_item
                        SET inventory_location_id = '$locationID',
                            inventory_dispense_datetime = NOW(),
                            inventory_dispensed_to = '$hospitalNumber',
                            user_id = '$userID'";
            $result = $this->conn->run($query);
            if (!$this->conn->hasRows($result)){
                $this->errorMsg = "A problem occurred. Please, try again later.";
                throw new Exception();
            }
            $this->dispenseID = $dispenseID = mysql_insert_id($this->conn->getConnectionID());
            $invUnit = new inventory_units();
            foreach ($dispenseArray as $itemID => $unitArray){
                $drugLabel = isset($drugLabelArray[$itemID]) ? $drugLabelArray[$itemID] : "";
                $batchID = isset($batchDetailsArray[$itemID]) ? $batchDetailsArray[$itemID] : 0;
                $totalQuantity = 0;
                foreach ($unitArray as $unitID => $quantity){
                    $disQuantity = $invUnit->convertToLowestUnit($unitID, $itemID, $quantity);
                    $totalQuantity += $disQuantity;
                }
                
                if ($totalQuantity == 0){
                    $this->errorMsg = "No quantity was entered for at least one item.";
                    throw new Exception();
                }
                
                //Get the quantity of this item available in that location
                $availableQty = $this->getItemStockAtLocation($itemID, $locationID, $batchID);
                
                
                //Confirm that there's enough stock available before dispensing
                if ($availableQty >= $totalQuantity){
                    //Deplete the stock by the dispensed quantity
                    $this->updateItemStockAtLocation($itemID, $locationID, $totalQuantity, "-", $batchID);
                    
                    //Enter this item dispense into the details table
                    $query = "INSERT INTO inventory_dispense_item_details
                                SET inventory_dispense_id = '$dispenseID',
                                    drug_id = '$itemID',
                                    inventory_batchdetails_id = '$batchID',
                                    inventory_dispense_deets_quantity = '$totalQuantity',
                                    inventory_dispense_deets_label = '$drugLabel'";
                    $result = $this->conn->run($query);
                } else {
                    $this->errorMsg = "The quantity entered for at least one item is greater than the quantity in stock.";
                    throw new Exception();
                }
            }   //END foreach
            
            $this->conn->commitTransaction();
            
        } catch (Exception $e) {
            $this->error = true;
            $this->conn->rollBackTransaction();
        }
        return $this->error;
    }   //END doDispense()
    
    
    
    
    public function getItemStockAtLocation($itemID, $locationID, $batchID = 0){
        $itemID = (int)$itemID;
        $batchID = (int)$batchID;
        $locationID = (int)$locationID;
        $this->error = false;
        $this->errorMsg = "";
        $retVal = 0;
        try{
            $addQuery = !empty($batchID) ? " AND ibd.inventory_batchdetails_id = '$batchID' " : "";
            $query = "SELECT SUM(ibd.inventory_batchdetails_quantity) 'quantity'
                        FROM inventory_batch_details ibd INNER JOIN inventory_batch ib
                        ON ibd.inventory_batch_id = ib.inventory_batch_id
                        WHERE ibd.drug_id = '$itemID'
                            AND ibd.inventory_batchdetails_quantity > 0
                            $addQuery";
            //die ("<pre>$query</pre>");
            $result = $this->conn->run($query);
            if ($this->conn->hasRows($result)){
                $row = mysql_fetch_assoc($result);
                $retVal = $row["quantity"];
            }
        } catch (Exception $e) {
            $this->error = true;
        }
        return $retVal;
    }   //END getItemStockAtLocation();
    
    
    
    
    //$operation should be "+" or "-"
    public function updateItemStockAtLocation($itemID, $locationID, $quantity, $operation, $batchID = 0){
        $itemID = (int)$itemID;
        $locationID = (int)$locationID;
        $batchID = (int)$batchID;
        $quantity = (double)$quantity;
        $this->error = false;
        $this->errorMsg = "";
        $retVal = 0;
        $operation = $operation == "+" ? "+" : "-";
        try {
            $query = "UPDATE inventory_batch_details
                        SET inventory_batchdetails_quantity = (inventory_batchdetails_quantity $operation $quantity)
                        WHERE inventory_batchdetails_id = '$batchID'";
            //die ("<pre>$query</pre>");
            $result = $this->conn->run($query);
            if (!$this->conn->hasRows($result)){
                $this->errorMsg = "Update of stock failed. Please, try again.";
                throw new Exception();
            }
        } catch (Exception $e) {
            $this->error = true;
        }
        return $retVal;
    }   //END getItemStockAtLocation();

    
    
    /**
     * Gets the list of supply batches in which an item is
     * @param int $itemID               The ID of the item in question
     * @param int $locationID           The location in question
     * @param boolean $notEmptyOnly     If true, then only batches in which the quantity of the item > 0 would be fetched, else all would be fetched
     * @return array                    An array containing all batches matching the search criteria
     */
    public function getBatchesForItem($itemID, $locationID, $notEmptyOnly = true){
        $itemID = (int)$itemID;
        $locationID = (int)$locationID;
        $retVal = array();
        $query = "SELECT ibd.*, DATE_FORMAT(ibd.inventory_batchdetails_expirydate, '%b %d, %Y') 'expiryDate'
                    FROM inventory_batch_details ibd INNER JOIN inventory_batch ib
                    ON ibd.inventory_batch_id = ib.inventory_batch_id
                    WHERE ibd.drug_id = '$itemID'
                        AND ib.inventory_location_id = '$locationID'";
        $query .= $notEmptyOnly ? " AND ibd.inventory_batchdetails_quantity > 0 " : "";
        $query .= " ORDER BY ibd.inventory_batchdetails_expirydate ASC ";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $retVal[] = $row;
            }
        }
        return $retVal;
    }

    
    
    
    
    
    public function getDispenseInstance($dispenseID, $locationID){
        $dispenseID = (int)$dispenseID;
        $locationID = (int)$locationID;
        $query = "SELECT idi.*,
                        CONCAT_WS(' ', r.reg_surname, r.reg_othernames) 'patientName',
                        CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) 'staffName',
                        DATE_FORMAT(idi.inventory_dispense_datetime, '%M %d, %Y') 'dispenseDate',
                        DATE_FORMAT(idi.inventory_dispense_datetime, '%H : %i : %s') 'dispenseTime'
                    FROM inventory_dispense_item idi
                        INNER JOIN registry r
                        INNER JOIN staff s
                    ON idi.inventory_dispensed_to = r.reg_hospital_no
                        AND idi.user_id = s.user_id
                        AND idi.inventory_dispense_id = '$dispenseID'
                        AND idi.inventory_location_id = '$locationID'"; //echo ("<pre>$query</pre>");
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $retVal = mysql_fetch_assoc($result);
        } else $retVal = "";
        return $retVal;
    }   //END getDispenseInstance()
    
    
    
    
    
    public function getDispenseInstanceDetails($dispenseID, $locationID){
        $dispenseID = (int)$dispenseID;
        $locationID = (int)$locationID;
        $retVal = array();
        $query = "SELECT idid.*, d.drug_desc, d.drug_name, iie.inventory_item_side_effects
                    FROM inventory_dispense_item_details idid
                    	INNER JOIN inventory_dispense_item idi
                        INNER JOIN drug d
                        INNER JOIN inventory_item_extras iie
                    ON idid.inventory_dispense_id = idi.inventory_dispense_id
						AND idid.drug_id = d.drug_id
                        AND idid.drug_id = iie.inventory_item_id
                    WHERE idid.inventory_dispense_id = '$dispenseID'
                    	AND idi.inventory_location_id = '$locationID'"; //die ("<pre>$query</pre>");
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $retVal[] = $row;
            }
        }
        return $retVal;
    }   //END getDispenseInstanceDetails()
    
    
    
    
    
}   //END class
?>
