<?php
/**
* Contains class variables & methods to select, save, update and delete rows from drug table
 * @package Drugs
 * @author ahmed rufai
 *
 * Create Date: 7-11-2009
 *
 *

 *
 */
require_once './classes/DBConf.php';

Class drug {
    /**
 *
 * @var holds array of fields in the drugs table
 */
 public $drugs=array() ;
   /**
 *
 * @var array values to be stored in the drugs table field
 */

public $drugs2=array() ;
/**
 *
 * @var $drug_id
 */
 
 
	public $drug_id; //int(10)$
	public $drug_item_no;
	public $drugcat_id; //int(10)
	public $drug_desc; //varchar(255)
	public $drug_expirydate; //varchar(255)
	public $drug_reorderlevel; //varchar(255)
	public $drug_manufacturer; //varchar(255)
	public $drug_unitpack; //varchar(255)
	public $drug_price; //varchar(255)
	public $drug_qtyinstock; //varchar(255)
	public $drugtype_id;
	public $connection;
    public $conn;
		 
		 
		 function  __construct() {
        $this->conn = new DBConf();
    }
    /**
     * New object to the class. Don�t forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New(); 
     *
     */
	public function New_drug($drugcat_id,$drug_desc,$drug_expirydate,$drug_reorderlevel,$drug_manufacturer,$drug_unitpack,$drug_price,$drug_qtyinstock){
		$this->drugcat_id = $drugcat_id;
		$this->drug_desc = $drug_desc;
		$this->drug_expirydate = $drug_expirydate;
		$this->drug_reorderlevel = $drug_reorderlevel;
		$this->drug_manufacturer = $drug_manufacturer;
		$this->drug_unitpack = $drug_unitpack;
		$this->drug_price = $drug_price;
		$this->drug_qtyinstock = $drug_qtyinstock;
	}

    /**
     * Load one row from the database table into class variables. To use the vars use for exemple echo Load_from_key->getVar_name($key_row)
     * ;
     *
     * @param int $key_row
     * 
     */
	public function Load_from_key($key_row){
		$result = $this->conn->execute("Select * from drug where drug_id = \"$key_row\" ");
		while($row = mysql_fetch_array($result)){
			$this->drug_id = $row["drug_id"];
			$this->drugcat_id = $row["drugcat_id"];
			$this->drug_desc = $row["drug_desc"];
			$this->drug_expirydate = $row["drug_expirydate"];
			$this->drug_reorderlevel = $row["drug_reorderlevel"];
			$this->drug_manufacturer = $row["drug_manufacturer"];
			$this->drug_unitpack = $row["drug_unitpack"];
			$this->drug_price = $row["drug_price"];
			$this->drug_qtyinstock = $row["drug_qtyinstock"];
			$this->drugtype_id = $row["drugtype_id"];
			$this->drug_item_no = $row["drug_item_no"];
		}
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     *
     */
	public function Delete_row_from_key($key_row){
		$this->conn->execute("DELETE FROM drug WHERE drug_id = $key_row");
	}

    /**
     * Update the active row on table using the key as arg
     * @param int $id row ID
     * @param string $tablename
     */
	public function Save_Active_Row($id,$tablename){
		/*$this->conn->execute("UPDATE drugs set drugcat_name = \"$this->drugcat_name\" where drugcat_id = \"$this->drugcat_id\"");*/
		
		try {
	
	$sql = "UPDATE $tablename SET ";	
	
				$qq=count($this->drugs);
				$q=count($this->drugs);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
			
         			    $sql.=    $this->drugs[$i] .'="'.$this->drugs2[$i].'" ';
			 
					 }
					 else{
						  $sql.=      $this->drugs[$i] .'="'.$this->drugs2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			$result2 =$this->conn->execute("SELECT * from $tablename");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
	//echo $sql;
         $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
	}

    /**
     * Save the active row as a new row on table
     * @retrun array
     */
	public function Save_Active_Row_as_New(){
		/*$this->conn->execute("Insert into drugs (drugcat_name) values (\"$this->drugcat_name\"");*/
		try {
	
			// server validation begins
					$getLang= new language();		
					$xlp_formfield =$this->drugs;
					$xlp_fieldmessage = array('Specify Drug Item No ','Specify Drug Sub Category','Specify Drug Description','Specify Drug Exp. Date','Specify Drug Reorder Level','Specify Drug Manufacturer','Specify Drug Form','Specify Drug Unit Pack','Specify Drug Price','Specify Qty in Stock');
		            $xlp_error=$getLang->xlpildator($xlp_formfield,$xlp_fieldmessage);
					if (!empty($xlp_error[2]))
					throw new Exception;
					//sever validation ends		
		$sql = "INSERT INTO drug SET ";	
	            				$qq=count($this->drugs);
				$q=count($this->drugs);

			for ($i = 0; $i < $qq; $i++) {
					if(($q-1)==0){
					    $sql.=    $this->drugs[$i] .'="'.$this->drugs2[$i].'" ';
					 }
					 else{
						  $sql.=      $this->drugs[$i] .'="'.$this->drugs2[$i].'" ,';
						 }
  				 $q--;
 			}
		
		  $result=$this->conn->execute($sql);
				 $list=array();
				if($this->conn->hasRows($result,  0)){
					 $list[3]=true;
					return $list;	 
					}	
        } catch (Exception $e) {
           return $xlp_error;
        }
	}
	
	
	public function GetSearchFields(){
	echo 	"<option value='-1'>Select Field</option>";	
		$result =$this->conn->execute("SELECT * from drug order by drug_id");
				$i = 0;
				
				while ($i < mysql_num_fields($result)) {
				 //   echo "Information for column $i:<br />\n";
					$meta = mysql_fetch_field($result);
					
					if (!$meta) {
					  //  echo "No information available<br />\n";
					}
  //  echo "$meta->name";

		
						echo 	"<option value=". $meta->name.">".$meta->name."</option>";
					    $i++;
				}
/*mysql_free_result($result);
	return $keys;*/
	}
	
	/**
     * list all rows in the specified table
     */
	
	function allrows() {
        
		try {
						$db2 = new DBConf();
							 $sql = 'SELECT *
					FROM
					drug
					
					';
					$pageindex='pdrug';
					$pager = new PS_Pagination($db2,$sql,50,10,$pageindex);
					$rs = $pager->paginate();
					 $offset=$pager->offset; 
							   // $res = $this->conn->execute($sql);
								$i=1;
							   while ($row = mysql_fetch_array($rs)) {
											// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
									 if ($i%2 ==0) {$bgcolor = "tr-row";} else {$bgcolor = "tr-row2";} 
			   					 echo' <tr class="'.$bgcolor.'">
							    <td>'.(++$offset).'.</td>
							<td><a href = "./index.php?p=editpdrug&drug_id='.$row["drug_id"] .'">'.$row["drug_desc"].'</a></td>
							  <td>'.$row["drug_expirydate"].'</td>  
							   <td align=center>'.$row["drug_reorderlevel"].'</td>   
							   <td>'.$row["drug_manufacturer"].'</td> 
							
							<td>'.'<a href = "./index.php?p=drug">Add Drug Category</a></td>
							</tr>';
							$i++;
								}
								?>
							 <tr>
								<td  colspan="5"><?php echo $pager->renderFullNav();?></td>
							</tr></table>
							<?
        } catch (Exception $e) {
	
        }

  
    }
	/**
     * list rows that match specified condition
     * @param string $search_field
     * @param string $search_value
     */
	function rowSearch($search_field,$search_value){
		 try {
		 $db2 = new DBConf();
		$sql = "Select * from  drug where  ".$search_field."='".$search_value."'" ;
		$res =$this->conn->execute($sql);
		
$pageindex='pdrug';
$pager = new PS_Pagination($db2,$sql,200,10,$pageindex);
$rs = $pager->paginate();
	  // echo "entered";
	     $i=1;
		while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["drug_id"].'" /></td>
        <td>'.$row["drug_id"].'</td>
		<td>'.$row["drug_desc"].'</td>
		  <td>'.$row["drug_expirydate"].'</td>  
		   <td>'.$row["drug_reorderlevel"].'</td>   
		   <td>'.$row["drug_manufacturer"].'</td> 
		       
        <td>'.'<a href = "./index.php?jj=delete&p=pdrug&id='.$row["drugcat_id"] .'"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td><td>'.'<a href = "./index.php?p=editpdrug&drug_id='.$row["drug_id"] .'"><img src="./images/btn_edit.gif" style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr>
		<?
		} catch (Exception $e) {
        }
	}
	

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->conn->execute("SELECT drug_id from drug order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["drug_id"];
				$i++;
			}
	return $keys;
	}
	
	function getDrugRow($id) {
        try {
           $sql = 'Select * from drug  where drug_id='.$id;
            $res = $this->conn->execute($sql);
            if($res){
			$row = mysql_fetch_array($res);
			
			
            return $row;
				 }
            
			
        } catch (Exception $e) {


        }
    }
    /**
     *
     * populates option tags with drug description
     * @param integer $id drug interaction ID
     */
function getdrug2($id) {
        try {
			$sql2 = 'select drug_id from drug_interaction  where drugint_id ='.$id;
				 $res2 = $this->conn->execute($sql2);
                 $row2 = mysql_fetch_array($res2);
				 if($row2)
				  $id2=$row2['drug_id'];
		
            $sql = 'select drug_id,drug_desc from drug ';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
			$sel= ($id2==$row['drug_id'])? "selected":" ";
			
                echo '<option value ="'.$row['drug_id'] .'{$sel}">'.$row['drug_desc'].'</option>';
   
            }
        } catch (Exception $e) {


        }
    }
     /**
     * populates option tags with drug description
     */
	function getdrug() {
        try {
            $sql = 'select drug_id,drug_desc from drug ';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                echo '<option value ="'.$row['drug_id'] .'">'.$row['drug_desc'].'</option>';
   
            }
        } catch (Exception $e) {


        }
    }

    /**
     * Close mysql connection
     */
	public function enddrug(){
		$this->connection->CloseMysql();
	}

}