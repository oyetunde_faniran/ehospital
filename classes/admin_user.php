<?php
/**
 * Handles all operations that has to do with page menu
 *
 * @author Faniran, Oyetunde
 *
 * @package System_Administration
 */
class admin_user {
    /**
     * @var object An object of the DBConf class
     */
	protected $conn;

    /**
     * @var string The username entered by a user during a login attempt
     */
	protected $username;

    /**
     * @var string The password entered by a user during a login attempt
     */
	protected $password;

    /**
     * @var array   Stores the $_POST array after a form has been submitted
     */
	protected $postArray;

    /**
     * @var string Stores the name of the current column from which content would be fetched from the language table
     */
	protected $langField;

    /**
     * @var string  Contains the HTML for the menu that would be displayed to the user
     */
	public $menu;

    /**
     * @var array  An array that would contain the list of all the menu IDs contained in a user's rights
     */
	protected $menuArray;

    /**
     * @var array   An array that contains the pages a user is allowed to visit with each element in the array being an array containing the allowed query string and the name of the page
     */
	public $urlArray;

    /**
     * @var int  The user ID of the currently logged-in user
     */
    public $userID;

    /**
     * @var int  The ID of the group a user belongs to
     */
	public $groupID;

    /**
     * @var string  The employee ID of the currently logged-in user
     */
	public $staffID;

    /**
     * @var int  The department ID of the currently logged-in user
     */
	public $deptID;

    /**
     * @var int  The unit ID of the currently logged-in user
     */
	public $unitID;

    /**
     * @var int  The clinic ID of the currently logged-in user (if the user is attached to a clinic)
     */
	public $clinicID;

    /**
     * @var int  The ID of the currently logged-in user in the nurses table (if the user is a nurse)
     */
	public $nurseID;

    /**
     * @var int  The ID of the currently logged-in user in the consultant table (if the user is a consultant)
     */
	public $consultantID;

    /**
     * @var string  The full name of the currently logged-in user
     */
	public $staffName;

    /**
     * @var int The staff type of the user (0=Clinical, 1=Non-Clinical)
     */
	public $staffType;

    /**
     * @var boolean =true if an error occurred during the last operation in the class else =false
     */
	public $error;

    /**
     * @var string  Would containing the error message of any error that occurs during the course of performing an operation in the class.
     */
	public $errorMsg;

    /**
     * @var int  The ID of the currently logged-in user in the bank tellers table (if the user is a bank teller)
     */
	public $bankTellerID;

    /**
     * @var string  The name of the bank a bank teller is attached to (if the user is a bank teller)
     */
	public $bank;



/**
 * Class Constructor
 * @param int $userID       The ID of a user in the users table
 * @param array $postArray  The $_POST array of any submitted form that would be handled by this class
 * @param string $langField The name of the column in the language table corresponding to the currently selected language
 */
	public function __construct($userID=0, $postArray=0, $langField=""){
		//Logging in?
		$this->conn = new DBConf();

		if (is_array($_POST)){
			$this->postArray = admin_Tools::doEscape ($postArray, $this->conn);	//Escape the posted form
		}

		if (!empty($userID))
			$this->userID = (int)$userID;

		if (!empty ($langField))
			$this->langField = $langField;
	}   //END __construct()


/**
	Handles login of users and also gets all values to be stored in session variables
*/
	public function doLogin(){
		$retVal = false;
		try{
			if (empty($this->postArray["username"]) || empty($this->postArray["password"]))
				throw new Exception;

			$this->username = $this->postArray["username"];
			$this->password = $this->postArray["password"];

			//Try hospital staff login
			$query = "SELECT u.user_id, u.group_id, u.staff_employee_id, s.staff_type, s.dept_id, s.unit_id, CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) staffname
						FROM users u INNER JOIN staff s
						ON u.staff_employee_id = s.staff_employee_id
						WHERE BINARY(u.user) = BINARY('" . $this->username . "')
							AND u.pass = MD5('" . $this->password . "')
							AND u.user_status = '1'
							AND u.user_type= '0'";	//user_type = 0 for normal hospital staff
							//die ("<pre>$query</pre>");
			$result = $this->conn->execute($query);
			if ($this->conn->hasRows($result, 1)){
				$row = mysql_fetch_array ($result, MYSQL_ASSOC);
				$this->userID = $row["user_id"];
				$this->groupID = $row["group_id"];
				$this->staffID = $row["staff_employee_id"];
				$this->deptID = $row["dept_id"];
				$this->unitID = $row["unit_id"];
				$this->staffName = $row["staffname"];
				$this->staffType = $row["staff_type"];

				$CNClinicID = $this->getCNClinicID();	//Get the clinicID / consultantID / nurseID if available

				$this->clinicID = isset($CNClinicID["clinicID"]) ? $CNClinicID["clinicID"] : 0;
				$this->nurseID = isset($CNClinicID["nurseID"]) ? $CNClinicID["nurseID"] : 0;
				$this->consultantID = isset($CNClinicID["consultantID"]) ? $CNClinicID["consultantID"] : 0;
				$this->bankTellerID = 0;
				$this->bank = 0;
				$retVal = true;
				//die ("gatch ya");
			} else {	//User/pass is wrong or user is not a normal hospital staff. So, check if the guy's a bank teller
					$query = "SELECT u.user_id, u.group_id, u.staff_employee_id,
								CONCAT_WS(' ', b.teller_title, b.teller_surname, b.teller_othernames) tellername, b.bank
								FROM users u INNER JOIN bank_tellers b
								ON u.staff_employee_id = b.teller_id
								WHERE BINARY(u.user) = BINARY('" . $this->username . "')
									AND u.pass = MD5('" . $this->password . "')
									AND u.user_status = '1'
									AND u.user_type= '1'";	//user_type = 1 for bank tellers	//die ("<pre>$query</pre>");
					$result = $this->conn->execute($query);
					if ($this->conn->hasRows($result, 1)){
						$row = mysql_fetch_array ($result, MYSQL_ASSOC);
						$this->userID = $row["user_id"];
						$this->groupID = $row["group_id"];
						$this->staffType = 1;
						$this->bankTellerID = $row["staff_employee_id"];
						$this->bank = $row["bank"];
						$this->staffID = 0;
						$this->deptID = 0;
						$this->unitID = 0;
						$this->staffName = $row["tellername"];
						$this->clinicID = 0;
						$this->nurseID = 0;
						$this->consultantID = 0;
						$retVal = true;
					}
				}
		} catch (Exception $e) {
				//die (print_r($e, true));
				$this->error = true;
				$this->errorMsg = "Undefined username & password.";
			}
		return $retVal;
	}	//END doLogin()




/**
 * Stores the details of this user login in the users_session table, which would be useful in knowing who is online and available for chat
 * @param int $userID   The ID of the user under consideration
 * @return boolean
 */
    public function loginSession_Store($userID){
        $userID = (int)$userID;
        try {
            if (empty($userID)){
                throw new Exception();
            }

            //Remove redundant rows
            $this->loginSession_RemoveRedundant();

            $query = "REPLACE INTO users_session
                        SET user_id = '$userID',
                            usersession_loggedintime = NOW(),
                            usersession_lastactivity = NOW()";
            $result = $this->conn->execute($query);
        } catch (Exception $e) {}
        return true;
    }   //END loginSession_Store




/**
 * Deletes the details of this user login from the users_session table, which would be useful in knowing who is online and available for chat
 * @param int $userID   The ID of the user under consideration
 * @return boolean
 */
    public function loginSession_Delete($userID){
        $userID = (int)$userID;
        try {
            if (empty($userID)){
                throw new Exception();
            }
            $query = "DELETE FROM users_session
                        WHERE user_id = '$userID'";
            $result = $this->conn->execute($query);
        } catch (Exception $e) {}
        return true;
    }   //END loginSession_Delete




/**
 * Update the details of this user login in the users_session table, which would be useful in knowing who is online and available for chat
 * @param int       $userID   The ID of the user under consideration
 * @param boolean   $idle     Set to true if the user is idle, else false
 * @return boolean
 */
    public function loginSession_Update($userID, $idle){
        $userID = (int)$userID;
        try {
            if (empty($userID)){
                throw new Exception();
            }
            $setClause = "";
            if ($idle == false){    //User is not idle, but still active
                $setClause = " usersession_lastactivity = NOW(), usersession_idle = '0' ";
            } else {    //User is now idle
                $setClause = " usersession_idle = '1' ";
            }
            $query = "UPDATE users_session
                        SET $setClause
                        WHERE user_id = '$userID'";
            $result = $this->conn->execute($query);
        } catch (Exception $e) {}
        return true;
    }   //END loginSession_Update




/**
 * Removes redundant rows that are at least one day old from the users_session table.
 * @return boolean
 */
    public function loginSession_RemoveRedundant(){
        $query = "DELETE FROM users_session
                    WHERE TIMESTAMPDIFF(HOUR, usersession_lastactivity, NOW()) >= 1";
        $result = $this->conn->execute($query);
        return true;
    }   //END loginSession_RemoveRedundant






/**
 * Gets the list of online users to be used for chat
 * @param int $currentUserID    The user ID of the currently logged-in user
 * @return array                An array containing rows of online user's info
 */
    public function getOnlineUsers4Chat($currentUserID = 0, $ignoreCurrentUser = true){
        $retVal = array();
        $addQuery = $ignoreCurrentUser ? " AND u.user_id <> '$currentUserID' " : "";
        $query = "SELECT s.*, u.*
                    FROM staff s INNER JOIN users_session u
                    ON s.user_id = u.user_id
                    WHERE true $addQuery
                    ORDER BY s.staff_title, s.staff_surname, s.staff_othernames";
        $result = $this->conn->execute($query);
        if ($this->conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $retVal[] = $row;
            }
        }
        return $retVal;
    }   //END loginSession_RemoveRedundant









/**
Gets the clinic_id of the user (if attached to a clinic)
			nurse_id of the user (if a nurse)
			consultant_id of the user (if a consultant)
*/
	protected function getCNClinicID(){
		//ASSUMPTION: The user is a consultant
		$query = "SELECT ext.clinic_id clinicID, ext.consultant_id consultantID FROM consultant ext INNER JOIN staff s
					ON ext.staff_employee_id = s.staff_employee_id
					WHERE ext.staff_employee_id = '" . $this->staffID . "'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result, 1)){
			$retVal = mysql_fetch_array($result, MYSQL_ASSOC);
		} else {
				//ASSUMPTION: The user is a nurse
				$query = "SELECT ext.clinic_id clinicID, ext.nurse_id nurseID FROM nurses ext INNER JOIN staff s
							ON ext.staff_employee_id = s.staff_employee_id
							WHERE ext.staff_employee_id = '" . $this->staffID . "'";
				$result = $this->conn->execute($query);
				if ($this->conn->hasRows($result, 1))
					$retVal = mysql_fetch_array($result, MYSQL_ASSOC);
				else $retVal = array();	//User not a user or a consultant
			}
		return $retVal;
	}   //END getCNClinicID()


/**
 * Gets the list of all members of staff, which is used on the user management page
 *
 * @param array $headerArray    An array containing the list of all the table headers in the staff list
 * @param int $deptid           The ID of a department in the department table. If this is specified, then the list of staff is limited to those in that department.
*/
	public function getStaffList($headerArray, $deptid=0){
		$query = "SELECT u.user, s.staff_id, s.staff_employee_id, CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) staffname, lcd." . $this->langField . " dept, lcg." . $this->langField . " groupname

					FROM staff s
						INNER JOIN department sd
						INNER JOIN language_content lcd
						INNER JOIN users u
						INNER JOIN groups g
						INNER JOIN language_content lcg
					ON s.dept_id = sd.dept_id
						AND sd.langcont_id = lcd.langcont_id
						AND s.user_id = u.user_id
						AND u.group_id = g.group_id
						AND g.langcont_id = lcg.langcont_id";
		if (!empty ($deptid))
			$query .= " WHERE dept_id = '$deptid'";
		$query .= " ORDER BY groupname, staff_surname, staff_othernames, staff_title";
		$result = $this->conn->execute ($query);
		if ($this->conn->hasRows($result)){
			$juggleRows = false;
			$retVal = "<table class=\"userTable\">\n
							<thead class=\"title-row\">
								<th>{$headerArray['sno']}</th>
								<th>{$headerArray['staffNo']}</th>
								<th>{$headerArray['staffName']}</th>
								<th>{$headerArray['dept']}</th>
								<th>{$headerArray['userGroup']}</th>
                                <th>USERNAME</th>
							</thead>
							<tbody>";
			$counter = 0;
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$editURL = "index.php?p=usermanagement_edit&m=sysadmin&d={$row['staff_id']}";
				$rowClass = $juggleRows ? " class=\"tr-row2\"" : " class=\"tr-row\"";
				$juggleRows = !$juggleRows;
				$retVal .= "<tr $rowClass>\n
								<td align=\"left\">\n" . (++$counter) . ".</td>\n
								<td align=\"left\">\n{$row['staff_employee_id']}</td>\n
								<td>\n
									<a href=\"$editURL\">{$row['staffname']}</a>\n
								</td>\n
								<td align=\"left\">\n{$row['dept']}</td>\n";
				if ($row['groupname'] == "Default User")
					$retVal .= "<td align=\"left\">\n<span class=redText>{$row['groupname']}</span></td>\n";
				else $retVal .= "<td align=\"left\">\n{$row['groupname']}</td>\n";
                $retVal .= "<td align=\"left\">\n{$row['user']}</td>\n";
				$retVal .=	"</tr>\n";
			}
			$retVal .= "</tbody>
						</table>";
		} else $retVal = "";
		return $retVal;
	}   //END getStaffList()



    /**
     * Gets the list of all bank tellers attached to the hospital, which is used on the user management page
     *
     * @param array $headerArray    An array containing the list of all the table headers in the staff list
     * @param int $bankID           The ID of a bank. If the ID is specified, then only bank tellers attached to this bank would be in the list
     * @return string               The generated HTML (with <table> as the parent tag) that contains the list
     */
	public function getBankTellerList($headerArray, $bankID = 0){
		$query = "SELECT u.user, b.teller_id, CONCAT_WS(' ', b.teller_title, b.teller_surname, b.teller_othernames) staffname, b.bank, lcg." . $this->langField . " groupname
					FROM bank_tellers b
						INNER JOIN users u
						INNER JOIN groups g
						INNER JOIN language_content lcg
					ON b.user_id = u.user_id
						AND u.group_id = g.group_id
						AND g.langcont_id = lcg.langcont_id";
		if (!empty ($bankID))
			$query .= " WHERE bank = '$bankID'";
		$query .= " ORDER BY teller_surname, teller_othernames, teller_title";
		$result = $this->conn->execute ($query);
		if ($this->conn->hasRows($result)){
			$retVal = "<table class=\"userTable\">\n
							<thead class=\"title-row\">
								<th>{$headerArray['sno']}</th>
								<th>{$headerArray['staffName']}</th>
								<th>{$headerArray['bank']}</th>
								<th>{$headerArray['userGroup']}</th>
                                <th>USERNAME</th>
							</thead>
							<tbody>";
			$juggleRows = false;
			$counter = 0;
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$rowClass = $juggleRows ? " class=\"tr-row2\"" : " class=\"tr-row\"";
				$juggleRows = !$juggleRows;
				$editURL = "index.php?p=usermanagement_bankedit&m=sysadmin&d={$row['teller_id']}";
				$retVal .= "<tr $rowClass>\n
								<td align=\"left\">\n" . (++$counter) . ".</td>\n
								<td>\n
									<a href=\"$editURL\">{$row['staffname']}</a>\n
								</td>\n
								<td align=\"left\">\n{$row['bank']}</td>\n";
				if ($row['groupname'] == "Default User")
					$retVal .= "<td align=\"left\">\n<span class=redText>{$row['groupname']}</span></td>\n";
				else $retVal .= "<td align=\"left\">\n{$row['groupname']}</td>\n";
                $retVal .= "<td align=\"left\">\n{$row['user']}</td>\n";
				$retVal .=	"</tr>\n";
			}
			$retVal .= "</tbody>
						</table>";
		} else $retVal = "";
		return $retVal;
	}   //END getBankTellerList()


    /**
     * Gets the details of a user (hospital staff) like userID, groupID & status (active or not)
     *
     * @return array An array containing the user details
     */
	public function getUserDeets(){
		//*** $this->userID is actually staff_id from the staff table
		try {
			$row = "";
			$query = "SELECT CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) staffname,
						s.staff_employee_id staffid, lc." . $this->langField . " dept, u.user_id, u.user 'username'
						FROM staff s
                            INNER JOIN department sd ON s.dept_id = sd.dept_id
                            INNER JOIN language_content lc ON sd.langcont_id = lc.langcont_id
                            INNER JOIN users u ON s.user_id = u.user_id
						WHERE staff_id = '" . $this->userID . "'";
//			die ("<pre>$query</pre>");
			$result = $this->conn->execute ($query);
			if (!$this->conn->hasRows ($result, 1))
				throw new Exception;
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);

			//get username & group_id
			$query = "SELECT u.user, u.user_status, u.group_id FROM users u
						WHERE user_id = '" . $row["user_id"] . "'";
			$result = $this->conn->execute ($query);
			if ($this->conn->hasRows($result, 1)){
				$userRow = mysql_fetch_array ($result, MYSQL_ASSOC);
				$row["user"] = $userRow["user"];
				$row["userID"] = $row["user_id"];
				$row["status"] = $userRow["user_status"];
				$row["groupID"] = $userRow["group_id"];
			}
		} catch (Exception $e) {
				$this->error = true;
			}
		return $row;
	}   //END getUserDeets()


/**
	Gets the details of a user (Bank Teller) like userID, groupID & status (active or not)
*/
/**
 * Gets the details of a user (Bank Teller) like userID, groupID & status (active or not)
 *
 * @return array An array containing the bank teller details
 */
	public function getBankTellerDeets(){
		//*** $this->userID is actually staff_id from the staff table
		try {
			$row = "";
			$query = "SELECT CONCAT_WS(' ', b.teller_title, b.teller_surname, b.teller_othernames) staffname, b.user_id, b.bank
						FROM bank_tellers b
						WHERE teller_id = '" . $this->userID . "'"; //echo ("<pre>$query</pre>");
			//die ("<pre>$query</pre>");
			$result = $this->conn->execute ($query);
			if (!$this->conn->hasRows ($result, 1))
				throw new Exception;
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);

			//get username & group_id
			$query = "SELECT u.user, u.user_status, u.group_id FROM users u
						WHERE user_id = '" . $row["user_id"] . "'";//die ("<pre>$query</pre>");
			$result = $this->conn->execute ($query);
			if ($this->conn->hasRows($result, 1)){
				$userRow = mysql_fetch_array ($result, MYSQL_ASSOC);
				$row["user"] = $userRow["user"];
				$row["userID"] = $row["user_id"];
				$row["status"] = $userRow["user_status"];
				$row["groupID"] = $userRow["group_id"];
			}
		} catch (Exception $e) {
				$this->error = true;
			}
		return $row;
	}   //END getBankTellerDeets()


/**
 * updates the details of a user in the users table
 *
 * @param int $staffID      The ID of the user in the staff table
 * @param array $errorMsgs  An array containing all the messages (in the currently selected language) that may be displayed to the user after this operation
 * @return boolean          The error state. true if an error occurred while trying to update the users' details, false otherwise.
 */
	public function updateUser($staffID, $errorMsgs){
		try{
			if (!is_array($this->postArray))
				throw new Exception;
			//Clean-up
			$postArray = admin_Tools::doEscape($this->postArray, $this->conn);

			//More Validation
			if (isset($postArray["username"])){
				if(empty($postArray["username"])){
					$this->errorMsg = $errorMsgs["admin_usermanagement_error_user"];
					throw new Exception;
				} elseif (strlen($postArray["username"]) < 8 || strlen($postArray["username"]) > 30){
						$this->errorMsg = $errorMsgs["admin_usermanagement_error_userpass"];
						throw new Exception;
					}
			}

			if (isset($postArray["password"])){
				if(empty($postArray["password"])){
					$this->errorMsg = $errorMsgs["admin_usermanagement_error_pass1"];
					throw new Exception;
				} elseif (strlen($postArray["password"]) < 8 || strlen($postArray["password"]) > 30){
						$this->errorMsg = $errorMsgs["admin_usermanagement_error_userpass"];
						throw new Exception;
					} elseif (empty($postArray["cpassword"])) {
							$this->errorMsg = $errorMsgs["admin_usermanagement_error_cpass"];
							throw new Exception;
						} elseif ($postArray["cpassword"] != $postArray["password"]){
								$this->errorMsg = $errorMsgs["admin_usermanagement_error_passcpass"];
								throw new Exception;
							}
			}

			if (empty($postArray["group"]) || !is_numeric($postArray["group"])){
				$this->errorMsg = $errorMsgs["admin_usermanagement_error_group"];
				throw new Exception;
			}
			$status = isset($postArray["enabled"]) ? 1 : 0;
			$empID = $this->empIDfromStaffID($staffID);

			//GOOD 2 GO
			//User is just been created
			if (isset($postArray["username"], $postArray["password"])){
				$query = "INSERT INTO users
							VALUES ('0', '" . $postArray["group"] . "', '$empID', '" . $postArray["username"] . "',
							MD5('" . $postArray["password"] . "'), NOW(), '$status', '0')";
				$result = $this->conn->execute ($query);
				//Save the user_id in the staff table if the query above was successful
				if ($this->conn->hasRows($result, 1)) {
					$query = "UPDATE staff
								SET user_id = '" . mysql_insert_id ($this->conn->getConnectionID()) . "'
								WHERE staff_id = '$staffID'";
					$this->conn->execute ($query);
					if (!$this->conn->hasRows($result, 1))
						throw new Exception;
				} else throw new Exception;
			} else {
					//User data is being updated
					if (empty($postArray["u"]) || !is_numeric(($postArray["u"])))
						throw new Exception;
					else $userID = (int)$postArray["u"];

					$query = "UPDATE users
								SET group_id = '" . $postArray["group"] . "',
									user_status = '$status'
								WHERE user_id = '$userID'"; //die ($query);
					$result = $this->conn->execute ($query);
					/*if (!$this->conn->hasRows($result, 1))
						throw new Exception;*/

					//If the user is a nurse or doctor then add the user to the nurses / doctors table
					switch ($postArray["group"]){
						//Consultant
						case "3":

						//Lab Consultant
						case "11":	$this->addNurseDoctor($userID, "consultant");
									break;

						//Nurse
						case "4":	$this->addNurseDoctor($userID, "nurse");
									break;
					}

					//Now, notify the user via SMS
					//$this->sendGroupChangeNotification($userID);

				}
		} catch (Exception $e) {
				//echo $this->errorMsg;
				//echo "<pre>" . print_r ($e, true) . "</pre>";
				$this->error = true;
			}
		return $this->error;
	}   //END updateUser()




/**
 * Sends SMS notification to a user when his / her user group is modified.
 *
 * @param int $userID   The ID of the user in the users table.
 */
	protected function sendGroupChangeNotification($userID){
		try {
			//Get the user details
			$query = "SELECT CONCAT_WS(' ', s.staff_othernames, s.staff_surname) AS 'name', s.staff_gsm, u.group_id
						FROM staff s INNER JOIN users u
						ON s.user_id = u.user_id
						WHERE s.user_id = '$userID'"; //die ($query);
			$result = $this->conn->execute ($query);
			if ($this->conn->hasRows($result, 1)){
				$row = mysql_fetch_array ($result, MYSQL_ASSOC);
				$staffName = $row["name"];
				$phone = $row["staff_gsm"];
				$group = $this->getDCUGName($row["group_id"], "group");

				//Form all the message sending parameters & send the message
				$msg = "Congratulations $staffName, you have been confirmed as a member of the user group '$group' on " . ORGANISATION_NAME . ". You can now start using the system.";
				$sender = SMS_SENDER_NAME;
				$sms = new SMSer(0);
				$sms->sendSMS ($phone, $sender, $msg);
			} else throw new Exception();
		} catch (Exception $e) {}
	}   //END sendGroupChangeNotification()




/**
 * Adds a user to the consultant or nurse table
 * @param int $userID       The user ID of the user to add to either the consultant or the nurses table
 * @param string $userType  = "consultant" if the user should be added to the consultant table, = "nurse" if the user should be added to the nurses table
 */
	protected function addNurseDoctor ($userID, $userType){
		//die ("$userID -- $userType");
		try {
			//Set the table to communicate with
			switch ($userType){
				case "consultant":	$table = "consultant";
									break;
				case "nurse":		$table = "nurses";
									break;
			}
			if (empty($table))
				throw new Exception();

			//Get this user's staffID & clinicID
			$query = "SELECT staff_employee_id, unit_id FROM staff
						WHERE user_id = '$userID'";
			$result = $this->conn->execute ($query);
			if ($this->conn->hasRows($result, 1)){
				$row = mysql_fetch_array ($result, MYSQL_ASSOC);
				$staffID = $row["staff_employee_id"];
				$clinicID = $row["unit_id"];
			} else throw new Exception();

			//Confirm that this user has not been added before
			$query = "SELECT * FROM `$table`
						WHERE staff_employee_id = '$staffID'
							AND clinic_id = '$clinicID'";
			$result = $this->conn->execute ($query);
			if ($this->conn->hasRows($result))
				throw new Exception();


			//Now add this user to the appropriate table
			$query = "INSERT INTO `$table` (staff_employee_id, clinic_id)
						VALUES ('$staffID', '$clinicID')";//die ($query);
			$result = $this->conn->execute ($query);

		} catch (Exception $e) {}
	}   //END addNurseDoctor()




/**
 * Updates the details of a user (Bank Teller) in the users table
 *
 * @param int $tellerID     The ID of the user in the bank teller (bank_tellers) table
 * @param array $errorMsgs  An array containing all the error messages that may be displayed to the user in the course of this operation or after
 * @return boolean          The error status at the end of the operation. =true if an error occurred, else false.
 */
	public function updateBankTeller($tellerID, $errorMsgs){
		try{
			if (!is_array($this->postArray))
				throw new Exception;
			//Clean-up
			$postArray = admin_Tools::doEscape($this->postArray, $this->conn);

			//More Validation
			if (isset($postArray["username"])){
				if(empty($postArray["username"])){
					$this->errorMsg = $errorMsgs["admin_usermanagement_error_user"];
					throw new Exception;
				} elseif (strlen($postArray["username"]) < 8 || strlen($postArray["username"]) > 30){
						$this->errorMsg = $errorMsgs["admin_usermanagement_error_userpass"];
						throw new Exception;
					}
			}

			if (isset($postArray["password"])){
				if(empty($postArray["password"])){
					$this->errorMsg = $errorMsgs["admin_usermanagement_error_pass1"];
					throw new Exception;
				} elseif (strlen($postArray["password"]) < 8 || strlen($postArray["password"]) > 30){
						$this->errorMsg = $errorMsgs["admin_usermanagement_error_userpass"];
						throw new Exception;
					} elseif (empty($postArray["cpassword"])) {
							$this->errorMsg = $errorMsgs["admin_usermanagement_error_cpass"];
							throw new Exception;
						} elseif ($postArray["cpassword"] != $postArray["password"]){
								$this->errorMsg = $errorMsgs["admin_usermanagement_error_passcpass"];
								throw new Exception;
							}
			}

			if (empty($postArray["group"]) || !is_numeric($postArray["group"])){
				$this->errorMsg = $errorMsgs["admin_usermanagement_error_group"];
				throw new Exception;
			}
			$status = isset($postArray["enabled"]) ? 1 : 0;
			//$empID = $this->empIDfromStaffID($staffID);

			//GOOD 2 GO
			//User is just been created
			if (isset($postArray["username"], $postArray["password"])){
				$query = "INSERT INTO users
							VALUES ('0', '" . $postArray["group"] . "', '$tellerID', '" . $postArray["username"] . "',
							MD5('" . $postArray["password"] . "'), NOW(), '$status', '1')";
				$result = $this->conn->execute ($query);
				//Save the user_id in the staff table if the query above was successful
				if ($this->conn->hasRows($result, 1)) {
					$query = "UPDATE bank_tellers
								SET user_id = '" . mysql_insert_id ($this->conn->getConnectionID()) . "'
								WHERE teller_id = '$tellerID'";
					$this->conn->execute ($query);
					if (!$this->conn->hasRows($result, 1))
						throw new Exception;
				} else throw new Exception;
			} else {
					//User data is being updated
					if (empty($postArray["u"]) || !is_numeric(($postArray["u"])))
						throw new Exception;
					else $userID = (int)$postArray["u"];

					$query = "UPDATE users
								SET group_id = '" . $postArray["group"] . "',
									user_status = '$status'
								WHERE user_id = '$userID'";
					$result = $this->conn->execute ($query);
					if (!$this->conn->hasRows($result, 1))
						throw new Exception;
				}
		} catch (Exception $e) {
				//echo $this->errorMsg;
				//echo "<pre>" . print_r ($e, true) . "</pre>";
				$this->error = true;
			}
		return $this->error;
	}   //END updateBankTeller()



/**
 * Get the staff_employee_id using staff_id from the staff table
 *
 * @param int $staffID  The row ID of the user in the staff table
 * @return string       Returns the actual employee code of the user whose staff ID was supplied. If not found, zero (0) is returned.
 */
	public function empIDfromStaffID($staffID){
		$query = "SELECT staff_employee_id staffid FROM staff
					WHERE staff_id = '$staffID'";
		$result = $this->conn->execute ($query);
		if ($this->conn->hasRows ($result, 1)){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			$retVal = $row["staffid"];
		} else $retVal = 0;
		return $retVal;
	}   //END empIDfromStaffID()



/**
 * Gets the menu (shown on the top navigation bar) that a user is allowed to see & visit
 *
 * @return string   Returns the generated HTML (with <div> as the parent tag), which contains the menu of all the pages a user is allowed to visit
 */
	public function getUserMenu(){
		$this->menuArray = array();
		$query = "SELECT menu_id FROM rights
					WHERE group_id = '" . $this->groupID . "'";//	echo "<p>getUserMenu:<pre>$query</pre></p>";
		$result = $this->conn->execute ($query);
		if ($this->conn->hasRows($result)){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				array_push($this->menuArray, $row["menu_id"]);

			$this->urlArray = $this->getURLs();

			$this->menu = "<div id=\"nav-bar\" class=\"width specialLinks\">
								<div id=\"nav-box\">
									<div id=\"menu-bg\">
										<ul class=\"pureCssMenu pureCssMenum\" id=\"nav\" name=\"nav\">";
			$this->menu .= $this->getVisualMenu();
			$this->menu .= "			</ul>
									</div>
								</div>
							</div>";
		} else {
				$this->menuArray = "";
				$this->menu = "<div id=\"nav-bar\" class=\"width\">
									<div id=\"nav-box\">
										<div id=\"menu-bg\">
											<ul class=\"pureCssMenu pureCssMenum\" id=\"nav\" name=\"nav\">";
				$this->menu .= "";
				$this->menu .= "			</ul>
										</div>
									</div>
								</div>";
			}
		return $this->menu;
	}   //END getUserMenu()



/**
 * Gets an array containing the details of all the pages a user is allowed to visit (stored in $this->menuArray)
 *
 * @return string Returns an array containing the details of all the pages a user is allowed to visit
 */
	protected function getURLs(){
		$ids = implode(",", $this->menuArray);
		$retVal = array();
		//Get the details of all the menu items the currently logged-in user is allowed to access
		$query = "SELECT * FROM menu
					WHERE menu_id IN ($ids)";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			//Pick the main page (e.g. index.php) and the value of 'p' in the query string
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$url = self::parseURL($row["menu_url"]);
				array_push ($retVal, $url);
			}
		}
		//die ("<pre>" . print_r($retVal, true) . "</pre>");
		return $retVal;
	}   //END getURLs()




/**
 * Parses the URL of the page a user is visiting as shown in the example below
    Example argument is "index.php?p=sysadmin&m=sysadmin"
    Returns array (
                "page" => "index.php",
                "p" => "p=sysadmin"
            )
 * @param string $url   The URL to be parsed it must be in the following format <filename>?<querystring>, without the protocol & domain
 * @return array        An array with the following elements "page": The actual file requested for, "p": The value of variable "p" in the query string if available
 * @deprecated          deprecated since version 1.0
 */
	protected static function parseURL_old($url){
		$retVal["page"] = $retVal["p"] = "";
		$components = explode ("?", $url);	//$url: e.g index.php?p=pop&m=123
		if (count($components) > 1){
			$retVal["page"] = $components[0];	//e.g. index.php
			$components = explode ("&", $components[1]);	//Explode the query string in the url to gain access to the variables in it
			//die ("<pre>" . print_r ($components, true) . "</pre>");

			//Now, check each var in the query string and look for "p="
			foreach ($components as $u){
				if (strpos(strtolower(trim($u)), "p=") == 0){
					$retVal["p"] = $u;
					break;
				}
			}

		} else {
				$retVal["page"] = $components[0];
			}
		//die ("<pre>" . print_r ($retVal, true) . "</pre>");
		return $retVal;
	}   //END parseURL_old()




/**
 * Parses the URL of the page a user is visiting as shown in the example below
    Example argument is "index.php?p=sysadmin&m=sysadmin"
    Returns array (
                "page" => "index.php",
                "p" => "p=sysadmin"
            )
 * @param string $url   The URL to be parsed it must be in the following format <filename>?<querystring>, without the protocol & domain
 * @return array        An array with the following elements "page": The actual file requested for, "p": The value of variable "p" in the query string if available
 */
	public static function parseURL($url){
		$retVal["page"] = $retVal["p"] = "";
		$components = explode ("?", $url);	//$url: e.g index.php?p=pop&m=123
		if (count($components) > 1){
			$retVal["page"] = $components[0];	//e.g. index.php
			parse_str($components[1]);
			if (isset($p))
				$retVal["p"] = "p=$p";
			else $retVal["p"] = "";
		} else {
				$retVal["page"] = $components[0];
			}
		return $retVal;
	}   //END parseURL()




/**
 * Checks if a user is allowed to visit the current page by searching for the current URL in the list of allowed URLs
 * @param mixed $allowed    Not Use
 * @return boolean          Returns true if the user is allowed to visit the page being requested, false otherwise
 */
	public static function isAllowedURL($allowed){
		//return tru if url doesnt contain index.php
		if(!strstr($_SERVER['REQUEST_URI'],'index.php?')) {
			return true;
		}

		//Explode the requested page to get the actual page & query string

		//Because of the fact that some query strings may have "/" in them, explode with "?" as the delimiter first, then "/" as the delimiter
        //to get the actual file being requested for, then append the query string to it before calling UserMenu::parseURL(...)
        $parts = explode("?", $_SERVER['REQUEST_URI']);
		$request = explode("/", $parts[0]);
		$request = end($request) . "?" . $_SERVER['QUERY_STRING'];
		$request = self::parseURL($request);
		$retVal = false;
		$result = array_search($request, $_SESSION[session_id() . "urlArray"]);
		if (!($result > 0 || $result === 0)) {
			//No perfect match was found for both "p" & "page". So, move to the next level of validation in which the user may
			//not be accessing index.php in which case, "p" is optional
			foreach ($_SESSION[session_id() . "urlArray"] as $uDeets){
				if (strtolower($uDeets["page"]) != "index.php"){
					if ($uDeets["page"] == $request["page"]){
						$retVal = true;
						break;
					}
				}
			}
		} else $retVal = true;
		return $retVal;
	}   //END isAllowedURL()




/**
 * Checks if a user is allowed to visit the current page by searching for the current URL in the list of allowed URLs
 * @param mixed $allowed    Not Use
 * @return boolean          Returns true if the user is allowed to visit the page being requested, false otherwise
 * @deprecated              Deprecated since version 1.0
 */
	public static function isAllowedURL_old($allowed){
		//Explode the requested page to get the actual page & query string
		$request = explode("/", $_SERVER['REQUEST_URI']);
		$request = end($request);
		$request = self::parseURL($request);
		$retVal = false;
		$result = array_search($request, $_SESSION[session_id() . "urlArray"]);
		if (!($result > 0 || $result === 0)) {
			//No perfect match was found for both "p" & "page". So, move to the next level of validation in which the user may
			//not be accessing index.php in which case, "p" is optional
			foreach ($_SESSION[session_id() . "urlArray"] as $uDeets){
				if (strtolower($uDeets["page"]) != "index.php"){
					if ($uDeets["page"] == $request["page"]){
						$retVal = true;
						break;
					}
				}
			}
		} else $retVal = true;
		return $retVal;
	}   //END isAllowedURL_old()



/**
 * Forms the HTML lists containing the links to the pages a user is allowed to visit
 * @return string   The generated HTML that would form the menu to be displayed to the user
*/
	protected function getVisualMenu(){
		$where = implode(",", $this->menuArray);
		$query = "DROP TABLE IF EXISTS tempmenu";
		$this->conn->execute($query);
		$retVal = "";

		//Create a temp table to store all the menu to be considered in forming the top nav bar
		$query = "CREATE TEMPORARY TABLE tempmenu
					SELECT m.*, lc." . $this->langField . " menuname
					FROM menu m INNER JOIN language_content lc
					ON m.langcont_id = lc.langcont_id
					WHERE menu_publish = '1'
						AND menu_dropdown = '1'
						AND menu_id IN ($where)";
		$this->conn->execute($query);

		//Get all menu items that would appear in the main menu
		$query = "SELECT * FROM tempmenu
					WHERE menu_parentid = '1'
					ORDER BY menu_order, menu_date DESC";	//echo "<p>getVisualMenu:<pre>$query</pre></p>";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				//The style for menu items with children is different from those without children
				if ($this->hasTempMenuChildren($row["menu_id"])) {
					if (!empty($row["menu_url"]))	//Insert the <a> tag if there's a url attached
						$retVal .= "<li class=\"pureCssMenui\"><a href=\"{$row['menu_url']}\" class=\"pureCssMenui\"><span>{$row['menuname']}</span></a>";
					else $retVal .= "<li class=\"pureCssMenui\"><a class=\"pureCssMenui\"><span>{$row['menuname']}</span></a>";
					$retVal .= $this->getTempMenuChildren($row["menu_id"]);
					$retVal .= "</li>";
				} else {
						if (!empty($row["menu_url"]))
							$retVal .= "<li class=\"pureCssMenui\"><a href=\"{$row['menu_url']}\" class=\"pureCssMenui\">{$row['menuname']}</a></li>";
						else $retVal .= "<li class=\"pureCssMenui\"><a class=\"pureCssMenui\">{$row['menuname']}</a></li>";
					}
			}	//END while
		}	//END if has results
		return $retVal;
	}   //END getVisualMenu()


/**
 * Checks if a menu has sub-menus
	(This is only done for menus a user is allowed to visit as stored in the temp table named 'tempmenu', which is created in protected getVisualMenu())
 * @param int $menuID   The menu ID of the menu item under consideration
 * @return Boolean      Returns true if the menu item whose ID was supplied has children, false otherwise
*/
	protected function hasTempMenuChildren($menuID){
		$query = "SELECT * FROM tempmenu
					WHERE menu_parentid = '$menuID'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result))
			$retVal = true;
		else $retVal = false;
		return $retVal;
	}   //END hasTempMenuChildren()


/**
 * Gets the sub-menus of a menu from the 'tempmenu' table created in protected getVisualMenu()'
 *
 * @param int $menuID   The menu ID of the menu item under consideration
 * @return string       The generated HTML that would form the children of the menu under consideration
*/
	public function getTempMenuChildren($menuID){
		$query = "SELECT * FROM tempmenu
					WHERE menu_parentid = '$menuID'
					ORDER BY menu_order, menu_date DESC";	//echo "<p>getTempMenuChildren:<pre>$query</pre></p>";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			$retVal = "<ul class=\"pureCssMenum\">";
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				//The style for menu items with children is different from those without children
				if ($this->hasTempMenuChildren($row["menu_id"])) {
					if (!empty($row["menu_url"]))	//Insert the <a> tag if there's a url attached
						$retVal .= "<li class=\"pureCssMenui\"><a href=\"{$row['menu_url']}\" class=\"pureCssMenui\"><span>{$row['menuname']}</span></a>";
					else $retVal .= "<li class=\"pureCssMenui\"><a class=\"pureCssMenui\"><span>{$row['menuname']}</span></a>";
					$retVal .= $this->getTempMenuChildren($row["menu_id"]);
					$retVal .= "</li>";
				} else {
						if (!empty($row["menu_url"]))
							$retVal .= "<li class=\"pureCssMenui\"><a href=\"{$row['menu_url']}\">{$row['menuname']}</a></li>";
						else $retVal .= "<li class=\"pureCssMenui\"><a class=\"pureCssMenui\">{$row['menuname']}</a></li>";
					}
			}	//END while
			$retVal .= "</ul>";
		} else $retVal = "";
		return $retVal;
	}   //END getTempMenuChildren()


/**
	PARAMS:
		$id is deptID, unitID or clinicID, groupID
		$type is "dept", "unit" or "clinic", "group" depending on the id sent in $id
	 RETURNS:
	 	the dept name, unit name, clinic name or group name as indicated in $type
 * Gets the actual name (in the currently selected language) of a department, unit, clinic or user group
 *
 * @param int $id       This is the ID of a department, unit, clinic or user group in their respective tables
 * @param string $type  This specifies the type of entity whose name is to be fetched. It can be one of the following (dept, unit, clinic or group)
 * @return string       The name of the specified entity
*/
	public function getDCUGName($id, $type){
		switch ($type){
			case "dept":
					$query = "SELECT lc." . $this->langField . " FROM department dept INNER JOIN language_content lc
								ON lc.langcont_id = dept.langcont_id
								WHERE dept.dept_id = '$id'";
					break;
			case "unit":
					$query = "SELECT lc." . $this->langField . " FROM units u INNER JOIN language_content lc
								ON lc.langcont_id = u.langcont_id
								WHERE unit_id = '$id'";
					break;
			case "clinic":
					$query = "SELECT lc." . $this->langField . " FROM clinic c INNER JOIN language_content lc
								ON lc.langcont_id = c.langcont_id
								WHERE clinic_id = '$id'";
					break;
			case "group":
					$query = "SELECT lc." . $this->langField . " FROM groups g INNER JOIN language_content lc
								ON lc.langcont_id = g.langcont_id
								WHERE group_id = '$id'";
					break;
			default:
				$query = "";
		}

		if (!empty($query)){
			$result = $this->conn->execute($query);
			if ($this->conn->hasRows($result)){
				$row = mysql_fetch_array ($result, MYSQL_ASSOC);
				$retVal = $row[$this->langField];
			} else $retVal = "";
		} else $retVal = "";

		return $retVal;

	}	//END  getDCUName()


/**
 * Gets the language in use and the name of the column in language_content table, which stores all values corresponding to the current language
 * @return array    An array containing information about the currently selected language. It would have the following elements "lang_field" & "lang_name"
*/
	public function getCurLangFields(){
		$query = "SELECT lang_field, lang_name FROM language_available
					WHERE lang_current = '1'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			$retVal = $row;
		} else $retVal = "";
		return $retVal;
	}   //END getCurLangFields()


/**
 * Stores the staff details entered on the staff registration page
 *
 * @param array $errMsgs        An array containing all the error messages that may be displayed to the user during the course of this operation / after
 * @return boolean              Returns the error status. true if an error occurred, else false.
*/
	public function registerStaff($errMsgs){
		$this->postArray = admin_Tools::doEscape($this->postArray, $this->conn);

		$this->error = false;
		try{
			//Turn keys in the POST array to vars
			foreach ($this->postArray as $key => $value){
				if (!empty($value))
					$$key = $value;
				elseif ($key == "staffclinic")
						$$key = $value;
					else {
							$this->errorMsg = $errMsgs["allfields"];
							throw new Exception;
					}
			}

			//Further validation
			if (!$this->isUniqueEmployeeID($staffempid)){
				$this->errorMsg = $errMsgs["uniqueID"];
				throw new Exception;
			}

			if (empty($username) || empty($password)){
				$this->errorMsg = $errMsgs["emptyUP"];
				throw new Exception;
			}

			if (strlen($username) < 8 || strlen($username) > 30 || strlen($password) < 8 || strlen($password) > 30){
				$this->errorMsg = $errMsgs["2shortUP"];
				throw new Exception;
			}

			if (!$this->isUniqueUsername($username)){
				$this->errorMsg = $errMsgs["uniqueuser"];
				throw new Exception;
			}

			if ($password != $cpassword){
				$this->errorMsg = $errMsgs["cpass"];
				throw new Exception;
			}

			//Prepend 234 to the phone number
			$phone = ltrim($phone, "0");
			$phone = "234" . $phone;

			//Good 2 go
			$query = "INSERT INTO staff (staff_employee_id, unit_id, staff_title, staff_surname, staff_othernames, staff_gender, dept_id, staff_gsm, staff_email)
						VALUES ('$staffempid', '$staffclinic', '$title', '$sname', '$onames', '0', '$staffdept', '$phone', '$email')";
			$result = $this->conn->execute($query);

			if (!$this->conn->hasRows($result))
				throw new Exception;

			$staffID = mysql_insert_id($this->conn->getConnectionID());

			//INSERT into staff table successful. Now, do for user table
			$query = "INSERT INTO users
						VALUES ('0', DEFAULT, '$staffempid', '$username', MD5('$password'), NOW(), '1', '0')";
			$result = $this->conn->execute($query);

			if (!$this->conn->hasRows($result))
				throw new Exception;

			//INSERT into users table successful. Now, update the row in the staff table to reflect the user_id
			$query = "UPDATE staff
						SET user_id = '" . mysql_insert_id($this->conn->getConnectionID()) . "'
						WHERE staff_id = '$staffID'";
			$result = $this->conn->execute($query);

		} catch (Exception $e) {
				$this->error = true;
			}
		return $this->error;
	}	//END registerStaff()



/**
 * Stores the bank teller details entered on the staff registration page
 *
 * @param array $errMsgs        An array containing all the error messages that may be displayed to the user during the course of this operation / after
 * @return boolean              Returns the error status. true if an error occurred, else false.
*/
	public function registerBankTeller($errMsgs){
		$this->postArray = admin_Tools::doEscape($this->postArray, $this->conn);

		$this->error = false;
		try{
			//Turn keys in the POST array to vars
			foreach ($this->postArray as $key => $value){
				if (!empty($value))
					$$key = $value;
				else {
						$this->errorMsg = $errMsgs["allfields"];
						throw new Exception;
				}
			}

			//Further validation
			if (empty($username) || empty($password)){
				$this->errorMsg = $errMsgs["emptyUP"];
				throw new Exception;
			}

			if (strlen($username) < 8 || strlen($username) > 30 || strlen($password) < 8 || strlen($password) > 30){
				$this->errorMsg = $errMsgs["2shortUP"];
				throw new Exception;
			}

			if (!$this->isUniqueUsername($username)){
				$this->errorMsg = $errMsgs["uniqueuser"];
				throw new Exception;
			}

			if ($password != $cpassword){
				$this->errorMsg = $errMsgs["cpass"];
				throw new Exception;
			}

			//Good 2 go
			$query = "INSERT INTO bank_tellers (teller_title, teller_surname, teller_othernames, teller_gsm, teller_email, bank)
						VALUES ('$title', '$sname', '$onames', '$phone', '$email', '$bank')";// die ($query);
			$result = $this->conn->execute($query);

			if (!$this->conn->hasRows($result))
				throw new Exception;

			$tellerID = mysql_insert_id($this->conn->getConnectionID());

			//INSERT into bank_tellers table successful. Now, do for user table
			$query = "INSERT INTO users
						VALUES ('0', DEFAULT, '$tellerID', '$username', MD5('$password'), NOW(), '1', '1')";
			$result = $this->conn->execute($query);

			if (!$this->conn->hasRows($result))
				throw new Exception;

			//INSERT into users table successful. Now, update the row in the bank_tellers table to reflect the user_id
			$query = "UPDATE bank_tellers
						SET user_id = '" . mysql_insert_id($this->conn->getConnectionID()) . "'
						WHERE teller_id = '$tellerID'";
			$result = $this->conn->execute($query);

		} catch (Exception $e) {
				$this->error = true;
			}
		return $this->error;
	}	//END registerBankTeller()




/**
 * Checks if a username has been used before or not
 * @param string $username      The username entered by a user during registration or during a username / password change
 * @param int $userID           If login details are being modified, then the user ID of the current user should be supplied so that the user's row would not be included in the search
 * @return boolean              Returns true if no other match was found for the username, false otherwise
 */
	protected function isUniqueUsername($username, $userID = 0){
		$query = "SELECT * FROM users
					WHERE BINARY(user) = BINARY('$username')";
		if (!empty($userID))
			$query .= " AND user_id <> '$userID'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result))
			$retVal = false;
		else $retVal = true;
		return $retVal;
	}   //END isUniqueUsername()



/**
 * Checks to confirm that the employee code a user supplied while trying to register has not been use before. i.e. is unique
 * @param string $staffEmpID        The employee code to be confirmed unique or not
 * @return boolean                  Returns true if no other match was found for the employee code, false otherwise
 */
	protected function isUniqueEmployeeID($staffEmpID){
		$query = "SELECT * FROM staff
					WHERE staff_employee_id = $staffEmpID";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result))
			$retVal = false;
		else $retVal = true;
		return $retVal;
	}   //END isUniqueEmployeeID()




/**
 * Handles the change of login details by a user
 * @return boolean      Returns the error status. true if an error occurred, false otherwise
 */
	public function changePass(){
		$this->error = false;
		$postArray = admin_Tools::doEscape($this->postArray, $this->conn);
		foreach ($postArray as $key=>$value)
			$$key = $value;
		try{
			//Is the current username & password entered by the user correct
			if (!$this->currentLoginOK($currentuser, $currentpass, $_SESSION[session_id() . "userID"])){
				$this->errorMsg = "Your current username and password are incorrect.";
				throw new Exception;
			}

			//Is there any other person using the new username apart from this user
			if (!$this->isUniqueUsername($currentuser, $_SESSION[session_id() . "userID"])){
				$this->errorMsg = "The username you entered is already in use by somebody else. Please, enter another one.";
				throw new Exception;
			}

			//username && password length between 8 & 30 xters?
			if (strlen($newuser) < 8 || strlen($newuser) > 30 || strlen($newpass) < 8 || strlen($newpass) > 30){
				$this->errorMsg = "Your username and password must be between 8 and 30 characters.";
				throw new Exception;
			}

			//password == confirmation pass ?
			if ($newpass != $newcpass){
				$this->errorMsg = "Your password is not the same as your confirmation password.";
				throw new Exception;
			}

			$query = "UPDATE users
						SET user = '$newuser',
							pass = MD5('$newpass')
						WHERE user_id = '" . $_SESSION[session_id() . "userID"] . "'";
			$result = $this->conn->execute($query);
			if (!$this->conn->hasRows($result, 1)){
				$this->errorMsg = "Your new username and password are the same as the initial. So, no changes were made to your login details.";
				throw new Exception;
			}
		} catch (Exception $e) {
				$this->error = true;
			}
		return $this->error;
	}   //END changePass()



/**
 * Checks supplied username and password if it's correct or not (used during login details change)
 * @param string $user      The username supplied by the user
 * @param string $pass      The password supplied by the user
 * @param int $userID    The user ID of the user that is making a login details change attempt
 * @return boolean          Returns true if the login details are OK, false otherwise.
 */
	protected function currentLoginOK($user, $pass, $userID){
		$query = "SELECT * FROM users
					WHERE BINARY(user) = BINARY('$user') AND pass = MD5('$pass') AND user_id = '$userID'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result, 1))
			$retVal = true;
		else $retVal = false;
		return $retVal;
	}   //END currentLoginOK()



/**
 * Gets a drop-down list of all the clinics under a particular department
 * @param int $deptID   The dept ID of the department whose clinics are to be displayed
 * @return string       The generated HTML with each clinic enclosed in an <option> tag so that they can fit into a <select> tag
 */
	public function getClinics4DropDown($deptID, $selected_clinic = 0){
		$deptID = (int)$deptID;
		$query = "SELECT c.clinic_id, lc.lang1 'clinic'
					FROM clinic c INNER JOIN language_content lc
					ON c.langcont_id = lc.langcont_id
					WHERE dept_id = '$deptID'
					ORDER BY `clinic`"; //die ("<pre>$query</pre>");
		$result = $this->conn->execute ($query);
		$retVal = "";
		if ($this->conn->hasRows($result)){
			while ($row = mysql_fetch_array ($result, MYSQL_ASSOC)){
                $selected_text = $selected_clinic == $row["clinic_id"] ? ' selected="selected" ' : '';
				$retVal .= "<option $selected_text value=\"" . $row["clinic_id"] . "\">" . stripslashes($row["clinic"]) . "</option>";
			}
		}
		return $retVal;
	}   //END getClinics4DropDown()




    public function getClinicConsultants4DD($clinicID){
		$clinicID = (int)$clinicID;
		$query = "SELECT c.consultant_id, CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) 'consultant'
                    FROM consultant c INNER JOIN staff s
                    ON c.staff_employee_id = s.staff_employee_id
                    WHERE c.clinic_id = '$clinicID'
					ORDER BY consultant"; //die ("<pre>$query</pre>");
		$result = $this->conn->execute ($query);
		$retVal = '<option value="0">--Select Consultant--</option>';
		if ($this->conn->hasRows($result)){
			while ($row = mysql_fetch_array ($result, MYSQL_ASSOC)){
				$retVal .= "<option value=\"" . $row["consultant_id"] . "\">" . stripslashes($row["consultant"]) . "</option>";
			}
		}
		return $retVal;
	}   //END getClinics4DropDown()




    /**
     * Fetches the list of users that belong to the department whose IDs are in $deptsArray
     * @param mixed $deptsArray     An array containing the IDs of the departments or a single value if only one department is being considered
     * @return array                Returns an array containing the info of the users or an empty array if no users were found in that department
     */
    public function getUsersByDept($deptsArray){
        $retVal = array();
        $deptsArray = admin_Tools::doEscape($deptsArray, $this->conn);
        $deptsArray = is_array($deptsArray) ? $deptsArray : array($deptsArray);
        $depts = "";
        foreach ($deptsArray as $dept){
            $depts = (int)$dept . ",";
        }
        $depts = rtrim ($depts, ",");
        $query = "SELECT s.*, u.*
                    FROM staff s INNER JOIN users u
                    USING (user_id)
                    WHERE s.dept_id IN ($depts)";
        $result = $this->conn->execute($query);
        if ($this->conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $retVal[] = $row;
            }
        }
        return $retVal;
    }   //END getUsersByDept()




    /**
     * Gets the details of a user for "Location" configuration in the Pharmacy Inventory module
     * @param int $userID               The ID of the user whose details would be fetched
     * @param int $currentUserDeptID    The department ID of the currently logged-in user
     * @return array                    The details of the user if found or an empty array otherwise.
     */
    public function getUserForLocationConfig($userID, $currentUserDeptID){
        $userID =(int)$userID;
        $currentUserDeptID = (int)$currentUserDeptID;
        $query = "SELECT s.*, u.*
                    FROM staff s INNER JOIN users u
                    USING (user_id)
                    WHERE s.user_id = '$userID'
                        AND s.dept_id = '$currentUserDeptID'";
        $result = $this->conn->execute($query);
        if ($this->conn->hasRows($result)){
            $retVal = mysql_fetch_assoc($result);
        } else $retVal = array();
        return $retVal;
    }   //END getUserForLocationConfig()



    /**
     * Gets the details the location(s) currently assigned to a user
     * @param int $userID               The ID of the user whose locations would be fetched
     * @param int $currentUserDeptID    The department ID of the currently logged-in user
     * @return array                    The location of the user if found or an empty array otherwise.
     */
    public function getCurrentUserLocations($userID, $currentUserDeptID){
        $userID =(int)$userID;
        $currentUserDeptID = (int)$currentUserDeptID;
        $query = "SELECT l.inventory_location_id, loc.inventory_location_name
                    FROM staff s
                        INNER JOIN users u
                        INNER JOIN inventory_locations_users l
                        INNER JOIN inventory_locations loc
                    ON s.user_id = u.user_id
                        AND s.user_id = l.user_id
                        AND l.inventory_location_id = loc.inventory_location_id
                    WHERE s.user_id = '$userID'
                        AND s.dept_id = '$currentUserDeptID'";
        //die ("<pre>$query</pre>");
        $result = $this->conn->execute($query);
        $retVal = array();
        if ($this->conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $retVal[$row["inventory_location_id"]] = $row["inventory_location_name"];
            }
        }
        return $retVal;
    }   //END getCurrentUserLocations()




    public function updateLocations($userID, $locationsArray){
        $userID =(int)$userID;
        $locationsArray = is_array($locationsArray) ? $locationsArray : array();

        //Remove currently assigned locations first
        $query = "DELETE FROM inventory_locations_users
                    WHERE user_id = '$userID'"; echo $query;
        $result = $this->conn->execute($query);

        //Now, add the new ones
        foreach ($locationsArray as $loc){
            $loc = (int)$loc;
            if (!empty($loc)){
                $query = "REPLACE INTO inventory_locations_users
                            SET inventory_location_id = '$loc',
                                user_id = '$userID'";
                $result = $this->conn->execute($query);
            }
        }
        return true;
    }   //END updateLocations()





}	//END class
?>