<?php
/**
 * Description of Patient
 *
 * @author Faniran, Oyetunde
 */
class Patient {

    /**
     * @var DBConf object   An object of the DBConf class, which handles communication with the DB
     */
    protected $conn;
    /**
     * @var int The ID of the patient under consideration
     */
    protected $patientID;
    /**
     * @var int The hospital number of the patient under consideration
     */
    protected $hospitalNo;
    /**
     *
     * @var boolean = true whenever an error occurs during any operation
     */
    public $error;
    /**
     *
     * @var string  Contains the error message of the error that occurred during the last operation if any
     */
    public $errorMsg;

    /**
     * Class Constructor
     * @param int $patientID        The ID of the Patient under consideration
     * @param string $hospitalNo    The hospital number of the patient under consideration
     */
    function __construct($patientID = 0, $hospitalNo = "") {
        $this->conn = new DBConf();
        $this->patientID = (int) $patientID;
        $this->hospitalNo = admin_Tools::doEscape($hospitalNo, $this->conn);
        $this->error = false;
        $this->errorMsg = "";
    }   //END __construct



    /**
     * Gets the full info about a patient
     * @param mixed $patIDNo    The hospital number of the patient or the patient ID in table "registry"
     * @param string $type      The type of value in $patIDNo - "patient-id" for patient ID or "hospital-number" for hospital number
     * @return array
     */
    public function getPatientInfo($patIDNo, $type = "hospital-number"){
        $patIDNo = admin_Tools::doEscape($patIDNo, $this->conn);
        $column = $type == "hospital-number" ? "reg_hospital_no" : "reg_id";
        $query = "SELECT r.*, rex.*
                    FROM registry r INNER JOIN registry_extra rex
                    USING (reg_id)
                    WHERE $column = '$patIDNo'";
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $retVal = mysql_fetch_assoc($result);
        } else $retVal = "";
        return $retVal;
    }   //END getPatientInfo()



    public function getFullPatientInfo($patIDNo, $type = "hospital-number"){
        $patIDNo = admin_Tools::doEscape($patIDNo, $this->conn);
        $column = $type == "hospital-number" ? "reg_hospital_no" : "reg_id";
        $query = "SELECT r.*, rex.*, DATE_FORMAT(r.reg_dob, '%M %d, %Y') 'dob',
                        n.nationality_name 'nationality', s.state_name 'state',
                        CASE (r.reg_gender)
                            WHEN '0' THEN 'Male'
                            WHEN '1' THEN 'Female'
                        END AS 'sex',
                        CASE (rex.regextra_civilstate)
                            WHEN '0' THEN 'Single'
                            WHEN '1' THEN 'Married'
                            WHEN '2' THEN 'Divorced'
                            WHEN '3' THEN 'Widowed'
                        END AS 'maritalStatus'
                    FROM registry r
                          LEFT JOIN registry_extra rex ON r.reg_id = rex.reg_id
                          LEFT JOIN nationality n ON rex.nationality_id = n.nationality_id
                          LEFT JOIN state s ON rex.state_id = s.state_id
                    WHERE r.$column = '$patIDNo'";
        //die ("<pre>$query</pre>");
        $result = $this->conn->run($query);
        if ($this->conn->hasRows($result)){
            $retVal = mysql_fetch_assoc($result);
        } else $retVal = "";
        return $retVal;
    }   //END getPatientInfo()



    /**
     * Gets the list of all patients. If $searchArray is not empty, then the list is limited to those whose profile matches the params in the array.
     * @param array $searchArray    An array containing the parameters to be used in filtering / sorting the list.
     * @return array                An array containing the list of patients.
     */
    public function getPatients4Preview($searchArray, $getTotalOnly = false){
        //Check if there are filters to be applied and apply them
        $finalSearchParams = $this->setSearchParams4PatientPreview($searchArray);

        if ($getTotalOnly){
            //Get the total count of records
            $query = "SELECT COUNT(*) 'totalCount' FROM registry r
                      WHERE true $finalSearchParams";
            //echo ("<pre>$query</pre>");
            $result = $this->conn->run($query);
            if ($this->conn->hasRows($result)){
                $retVal = mysql_fetch_assoc($result);
                $retVal = $retVal["totalCount"];
            } else $retVal = 0;
        } else {
            global $limitStart, $recsPerPage;
            $searchArray = admin_Tools::doEscape($searchArray, $this->conn);
            $sortColumn = isset($searchArray["sort"]) ? $searchArray["sort"] : 0;
            $sortDirection = isset($searchArray["sort-direction"]) ? $searchArray["sort-direction"] : 0;

            //Get the sort direction
            $sortDirection = $sortDirection == 2 ? " DESC " : " ASC ";

            //Get the actual column to be used in sorting
            switch ($sortColumn){
                case 2: $sortColumn = "reg_surname";
                        break;
                case 3: $sortColumn = "reg_othernames";
                        break;
                case 4: $sortColumn = "reg_gender";
                        break;
                case 5: $sortColumn = "reg_dob";
                        break;
                case 6: $sortColumn = "reg_phone";
                        break;
                case 1:
                default:
                        $sortColumn = "reg_hospital_no";
                        break;
            }   //END switch
            $query = "SELECT rex.*, r.*, DATE_FORMAT(r.reg_dob, '%M %d, %Y') 'dob'
                        FROM registry r LEFT JOIN registry_extra rex
                        USING (reg_id)
                        WHERE true $finalSearchParams
                        ORDER BY $sortColumn $sortDirection
                        LIMIT $limitStart, $recsPerPage";
            //echo ("<pre>$query</pre>");
            $retVal = array();
            $result = $this->conn->run($query);
            if ($this->conn->hasRows($result)){
                while ($row = mysql_fetch_assoc($result)){
                    $retVal[] = $row;
                }
            }
        }   //END else part of if $getTotalOnly

        return $retVal;
    }   //END getPatients4Preview()



    private function setSearchParams4PatientPreview($searchArray){
        //echo "<pre>" . print_r ($searchArray, true) . "</pre>";
        $retVal = "";

        //Confirm that the needed variables are available
        $searchArray = isset($searchArray["getArray"]) ? $searchArray["getArray"] : "";
        $hospitalnumber = isset($searchArray["hospitalnumber"]) ? trim($searchArray["hospitalnumber"]) : "";
        $surname = isset($searchArray["surname"]) ? trim($searchArray["surname"]) : "";
        $othernames = isset($searchArray["othernames"]) ? trim($searchArray["othernames"]) : "";
        $dobfrom = isset($searchArray["dobfrom"]) ? trim($searchArray["dobfrom"]) : "";
        $dobto = isset($searchArray["dobto"]) ? trim($searchArray["dobto"]) : "";
        $gender = isset($searchArray["gender"]) ? trim($searchArray["gender"]) : "";
        $phone = isset($searchArray["phone"]) ? trim($searchArray["phone"]) : "";
        $email = isset($searchArray["email"]) ? trim($searchArray["email"]) : "";
        $address = isset($searchArray["address"]) ? trim($searchArray["address"]) : "";
        $nationality = isset($searchArray["nationality"]) ? trim($searchArray["nationality"]) : "";
        $state = isset($searchArray["state"]) ? trim($searchArray["state"]) : "";
        $civilstate = isset($searchArray["civilstate"]) ? trim($searchArray["civilstate"]) : "";
        $religion = isset($searchArray["religion"]) ? trim($searchArray["religion"]) : "";
        $occupation = isset($searchArray["occupation"]) ? trim($searchArray["occupation"]) : "";
        $placeofwork = isset($searchArray["placeofwork"]) ? trim($searchArray["placeofwork"]) : "";
        $nextofkin = isset($searchArray["nextofkin"]) ? trim($searchArray["nextofkin"]) : "";
        $nokrelationship = isset($searchArray["nokrelationship"]) ? trim($searchArray["nokrelationship"]) : "";
        $nokphone = isset($searchArray["nokphone"]) ? trim($searchArray["nokphone"]) : "";
        $nokemail = isset($searchArray["nokemail"]) ? trim($searchArray["nokemail"]) : "";
        $nokaddress = isset($searchArray["nokaddress"]) ? trim($searchArray["nokaddress"]) : "";

        //Form the query for each available parameter
        $retVal .= !empty($hospitalnumber) ? " AND r.reg_hospital_no LIKE '%$hospitalnumber%' " : "";
        $retVal .= !empty($surname) ? " AND r.reg_surname LIKE '%$surname%' " : "";
        $retVal .= !empty($othernames) ? " AND r.reg_othernames LIKE '%$othernames%' " : "";
        $retVal .= !empty($dobfrom) && !empty($dobto) ? " AND r.reg_dob BETWEEN '$dobfrom' AND '$dobto' " : "";
        $retVal .= !empty($gender) ? " AND r.reg_gender = '$gender' " : "";
        $retVal .= !empty($phone) ? " AND r.reg_phone LIKE '%$phone%' " : "";
        $retVal .= !empty($email) ? " AND r.reg_email LIKE '%$email%' " : "";
        $retVal .= !empty($address) ? " AND r.reg_address LIKE '%$address%' " : "";
        $retVal .= !empty($nationality) ? " AND rex.nationality_id = '$nationality' " : "";
        $retVal .= !empty($state) ? " AND rex.state_id = '$state' " : "";
        $retVal .= !empty($civilstate) ? " AND rex.regextra_civilstate = '$civilstate' " : "";
        $retVal .= !empty($religion) ? " AND rex.regextra_religion LIKE '%$religion%' " : "";
        $retVal .= !empty($occupation) ? " AND rex.regextra_occupation LIKE '%$occupation%' " : "";
        $retVal .= !empty($placeofwork) ? " AND rex.regextra_placeofwork LIKE '%$placeofwork%' " : "";
        $retVal .= !empty($nextofkin) ? " AND rex.regextra_nextofkin LIKE '%$nextofkin%' " : "";
        $retVal .= !empty($nokrelationship) ? " AND rex.regextra_nokrelationship LIKE '%$nokrelationship%' " : "";
        $retVal .= !empty($nokphone) ? " AND rex.regextra_nokphone LIKE '%$nokphone%' " : "";
        $retVal .= !empty($nokemail) ? " AND rex.regextra_nokemail LIKE '%$nokemail%' " : "";
        $retVal .= !empty($nokaddress) ? " AND rex.regextra_nokaddress LIKE '%$nokaddress%' " : "";

        return $retVal;

    }   //END setSearchParams4PatientPreview()



}   //END class
?>
