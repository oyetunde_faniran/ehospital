<?php
/**
 * Handles all actions that has to do with the treatment of the patient by a doctor especially those initiated at the doctor's GUI
 *
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */

/**
 * Handles all actions that has to do with the treatment of the patient by a doctor especially those initiated at the doctor's GUI
 *
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */
class patTreatment {

/**
 * @var array Stores the main treatment part of a patient's treatment
 */
public $treat = array();

/**
 * @var array Stores the history part of a patient's treatment
 */
public $hist = array();

/**
 * @var array Stores the lab test part of a patient's treatment
 */
public $lab = array();

/**
 * @var array Stores the prescription part of a patient's treatment
 */
public $pres = array();

/**
 * @var array Stores the treatment part of a patient's treatment
 */
public $treatment = array();


/**
 * Class Constructor
 */
    function  __construct() {
        $this->conn = new DBConf();
    }




/**
 * Gets the patient details to be shown to the doctor on the treatment GUI
 * @param int $app_id               The ID of the appointment that led to the treatment
 * @param int $consultant_id        The ID of the consultant in the consultant table that is attending to the patient
 * @return mixed                    An array containing the patient details or false on failure
 */
	function getPatientDetails($app_id, $consultant_id){

		try{
					$query = "SELECT pa.reg_hospital_no as hospital_no,
								CONCAT_WS(' ', UPPER(reg.reg_surname), LOWER(reg.reg_othernames)) AS name,
							    reg.reg_gender AS sex,
								YEAR(CURDATE()) - YEAR(reg.reg_dob) AS age,
								app.app_type AS appoint_type,
								reg.reg_passport as passport,
								DATE(app.app_starttime) as appdate,
								CURDATE() as presdate,
								TIME_FORMAT(TIME(app.app_starttime),'%r') as starttime,
								TIME_FORMAT(TIME(app.app_endtime),'%r') as endtime,
								pa.patadm_id as patadm_id, pt.pattotal_status as paystatus, app.app_status as app_status,
                                reg.reg_phone AS 'patient_phone'
							  FROM appointment app
							  INNER JOIN patient_admission pa
							   LEFT JOIN pat_transtotal pt ON (pt.patadm_id = app.patadm_id
							  							   AND pt.pattotal_newstatus != ''
														   AND pt.pattotal_servicerendered = '0')
							  INNER JOIN registry reg ON app.patadm_id = pa.patadm_id
							  						AND reg.reg_hospital_no = pa.reg_hospital_no
							  WHERE app.app_id = '$app_id'
								AND app.consultant_id = '$consultant_id'"; //die ("<pre>$query</pre>");
					$res = $this->conn->execute($query);
					if($res){
						while ($row = mysql_fetch_array($res)){
						extract($row);
							$final = array( 'name' => $name,
											'hospital_no' => $hospital_no,
											'sex' => $sex,
											'age' => $age,
											'appoint_type' => $appoint_type,
											'passport' => $passport,
											'appdate' => $appdate,
											'starttime' => $starttime,
											'endtime' => $endtime ,
											'presdate' => $presdate,
											'patadmid' => $patadm_id,
											'paystatus' => $paystatus,
											'app_status' => $app_status
							);
							//die ("<pre>" . print_r ($final, true) . "</pre>");
							return $final;
						}
					}else return false;

		}catch(Exception $e){
					set_error_handler (array("Error_handler", "my_error_handler"));

		}

	}   //END getPatientDetails()


/**
 * Gets the details of any other appointment a patient has
 * @param string $hospital_no       The patient's hospital number
 * @param int $consultant_id        The ID of the consultant
 * @return mixed                    The generated HTML of the details or false on failure
 */
	function getOtherAppointment($hospital_no, $consultant_id = 0){
		try{
            $query = "SELECT DISTINCT c.casenote_diagnosis as diagnosis,
                        ct.casetreat_desc as treatment,
                        c.casenote_id as case_id,
                        DATE_FORMAT(app.app_starttime, '%M %d, %Y') as appdate,
                        cl.caselab_desc as labtest
                   FROM casenote c
                   INNER JOIN patient_admission pa ON pa.patadm_id = c.patadm_id
                   INNER JOIN appointment app ON (app.patadm_id = pa.patadm_id)
                   LEFT JOIN casenote_labtest cl ON c.casenote_id = cl.casenote_id
                   LEFT JOIN casenote_treatment ct ON c.casenote_id = ct.casenote_id
                   WHERE pa.reg_hospital_no = '$hospital_no'
                        #AND app.consultant_id = '$consultant_id'
                        AND app.app_status = '1'
                   ORDER BY c.casenote_date DESC";

            $res = $this->conn->execute($query);
            if($res){
                $dee = "";
                $num = mysql_num_rows($res);
                if($num !=0){
                    while ($row = mysql_fetch_array($res)){
                        extract($row);
                        $dee .= "<tr>
                                    <td valign='top'>$appdate</td>
                                    <td valign='top'>$diagnosis</td>
                                    <td valign='top'><table width='0' border='0' cellpadding='0'>".$this->selectPrescription($case_id)."</table></td>
                                    <td valign='top'>" . $this->getCaseNoteLabTests($case_id) . "</td>
                                    <td valign='top'>$treatment</td>
                                    <td valign='top'><a href='index.php?p=viewappdetails&m=patient_care&a=$case_id' target='_blank'>View Details</a></td>
                                  </tr>";
                    }   //END while()
                } else {}
                return $dee;
            } else return false;

		} catch(Exception $e) {
			set_error_handler (array("Error_handler", "my_error_handler"));
		}   //END try..catch

	}   //END getOtherAppointment()




/**
 * Gets all the lab test attached to a particularr casenote
 * @param int $casenoteID   The ID of the casenote  in the main casenote table
 * @return string           The generated HTML table containing a list of lab tests
 */
    protected function getCaseNoteLabTests($casenoteID){
        $query = "SELECT caselabreq_testname labtest FROM casenote_labtest
                    WHERE casenote_id = '$casenoteID'";
        $result = $this->conn->execute($query);
        if ($this->conn->hasRows($result)){
            $retVal = "<table cellspacing=\"3\" cellpadding=\"3\" border=\"0\">";
            $sNo = 0;
            while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
                $retVal .= "<tr>
                                <td>" . (++$sNo) . ".</td>
                                <td>" . stripslashes($row["labtest"]) . "</td>
                            </tr>";
            }   //END while()
            $retVal .= "</table>";
        } else $retVal = "None";
        return $retVal;
    }   //END getCaseNoteLabTests()




/**
 * Gets the ward name and id of an admitted patient
 * @param int $adm_id           The ID of the admission recommendation instance in the recommended admissions table (pat_admrecommend)
 * @param string $curLangField  The name of the column in the language table that correponds to the currently-selected language
 * @return array                The details of the ward
 */
	   function viewAdmission($adm_id, $curLangField) {
        try {
            $query = "SELECT p.ward_id as id, lc.$curLangField as name
					  FROM pat_admrecommend p
					  INNER JOIN wards w
					  INNER JOIN language_content lc
					  ON w.langcont_id = lc.langcont_id
					  AND p.ward_id = w.ward_id
					  WHERE  p.patadmrec_id = '$adm_id' AND p.patadmrec_flag = '0'";

            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
     	    $num = mysql_num_rows($res) ;
				if($num>0){
					while($row = mysql_fetch_assoc($res)){
					extract($row);
					$treat =  array('ward_id' => $id,
								  'ward' => $name
									  );
						 }
				}else{
					$treat =  array('ward_id' => 0,
								  'ward' => ""
									  );
				}
			return $treat;
            }else return false;
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END viewAdmission()



/**
 * Saves a patient admission entry
 * @return mixed    The ID of the admission recommendation in the admission recommendation table
 */
	function insertAdmission(){
		try{
            $query = "INSERT INTO `pat_admrecommend` SET `patadm_id` = '".$this->treat['patadm_id']."',
												 	`consultant_id` = '".$this->treat['consultant_id']."',
												 	`ward_id` = '".$this->treat['ward_id']."',
													`patadmrec_date` = NOW(),
												 	`patadmrec_flag` = '0'
													";

            $res = $this->conn->execute($query);
			if($res){
			$id = mysql_insert_id();
			return $id;

			}else return FALSE;

		}catch (Exception $e){

			set_error_handler (array("Error_handler", "my_error_handler"));

		}
	}   //END insertAdmission()



/**
 * Updates the details of a recommended admission
 * @param int $adm_id       The ID of the admission recommendation in the admission recommendation table
 * @return boolean          true on success, false on failure
 */
	function updateAdmission($adm_id){
		try{
            $query = "UPDATE `pat_admrecommend` SET `ward_id` = '".$this->treat['ward_id']."',
													`patadmrec_date` = NOW(),
												 	`patadmrec_flag` = '0'
											WHERE `patadmrec_id` = '".$adm_id."'";

            $res = $this->conn->execute($query);
			if($res){
			return TRUE;

			}else return FALSE;

		}catch (Exception $e){

			set_error_handler (array("Error_handler", "my_error_handler"));

		}
	}   //END updateAdmission()





/**
 * Checks if a patient was admitted by a consultant for a given medical transaction
 * @param <type> $patadm_id
 * @param <type> $consultant_id
 * @return <type>
 */
   function checkAdmission($patadm_id,$consultant_id) {
        try {
            $query = "SELECT patadmrec_id
					  FROM pat_admrecommend
					  WHERE patadm_id = '$patadm_id' AND DATE(patadmrec_date)= CURDATE() AND consultant_id = '$consultant_id'";

            //  $res = mysql_query($query);
//die("<pre>" . print_r($this->treat, true) . "<br>$query</pre>");
            $res = $this->conn->execute($query);

            if ($res) {
     	    $num = mysql_num_rows($res) ;
				if($num>0){
					while($row = mysql_fetch_assoc($res)){
					$patadmrec_id = $row["patadmrec_id"];
				}
				}else{
				$patadmrec_id =0;
				}
				return $patadmrec_id;
				}else return false;
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END checkAdmission()





/**
 * Saves part of a patient's treatment details into the main casenote table (casenote)
 * @return mixed Returns the insert ID on success or false on failure
 */
	function insertCasenote(){
	try{
			$query ="INSERT INTO casenote
                      SET `patadm_id` = '".$this->treat['patadm_id']."',
                          `casenote_date` = NOW(),
                          `casenote_genexam` = '".$this->treat['genexam']."',
                          `casenote_sysexam` = '".$this->treat['sysexam']."',
                          `casenote_diagnosis` = '".$this->treat['diagnosis']."',
                          `casenote_diffdiagnosis` = '".$this->treat['diffdiagnosis']."',
                          `app_id` = '".$this->treat['app_id']."',
                          `user_id` = '".$this->treat['user_id']."'";

            $res = $this->conn->execute($query);
            if ($res) {
				$id = mysql_insert_id();
				return $id;
            }else return FALSE;


	}catch (Exception $e) {

			set_error_handler (array("Error_handler", "my_error_handler"));

        }
	}   //END insertCasenote()



/**
 * Saves part of a patient's treatment details tagged "history" into the casenote history table (casenote_history)
 * @param int $casenote_id  The ID of the row inserted into the casenote table
 * @return boolean          Returns true on success or false on failure
 */
	function insertHistory($casenote_id){
	try{
			$query ="INSERT INTO casenote_history
								SET   `casenote_id` = '".$casenote_id."',
									  `casehist_complaint` = '".$this->hist['complaint']."',
									  `casehist_complainthist` = '".$this->hist['complainthist']."',
									  `casehist_medhist` = '".$this->hist['medhist']."',
									  `casehist_sochist` = '".$this->hist['sochist']."',
									  `casehist_drughist` = '".$this->hist['drughist']."',
									  `casehist_others` = '".$this->hist['sysreview']."',
									  `casehist_sysreview` = '".$this->hist['others']."'";

            $res = $this->conn->execute($query);
            if ($res) {
                return true;
            }else return false;


	}catch (Exception $e) {

			set_error_handler (array("Error_handler", "my_error_handler"));

        }
	}   //END insertHistory()




/**
 * Saves the prescription part of a patient's treatment details into the casenote prescription table (casenote_prescription)
 * @param int $casenote_id  The ID of the row inserted into the casenote table
 * @return boolean          Returns true on success or false on failure
 */
	function insertPrescription($casenote_id, $drug, $dosage){
	try{
			$query ="INSERT INTO casenote_prescription
								SET   `casenote_id` = '".$casenote_id."',
									  `casepres_drugname` = '".$drug."',
									  `casepres_drugdesc` = '".$dosage."'";

            $res = $this->conn->execute($query);
            if ($res) {
			return true;
            }else return FALSE;


	}catch (Exception $e) {

			set_error_handler (array("Error_handler", "my_error_handler"));

        }
	}   //END insertPrescription()



/**
 * Saves the lab test part of a patient's treatment details into the casenote labtest table (casenote_labtest)
 * @param int $casenote_id  The ID of the row inserted into the casenote table
 * @return boolean          Returns true on success or false on failure
 */
	function insertLabtest($casenote_id){
        return true;
        try{
            //die ("All Lab Test contains -->" . $this->lab['investigation']);
            $query ="INSERT INTO casenote_labtest
                        SET `casenote_id` = '".$casenote_id."',
                            `caselab_desc` = '".$this->lab['investigation']."'";

            $res = $this->conn->execute($query);
            if ($res) {
                return true;
            } else return false;

        } catch (Exception $e) {
            set_error_handler (array("Error_handler", "my_error_handler"));
        }
	}   //END insertLabtest()



/**
 * Saves the main treatment part of a patient's treatment details into the casenote treatment table (casenote_treatment)
 * @param int $casenote_id  The ID of the row inserted into the casenote table
 * @return boolean          Returns true on success or false on failure
 */
	function insertTreatment($casenote_id){
	try {
			$query ="INSERT INTO casenote_treatment
								SET   `casenote_id` = '".$casenote_id."',
									  `casetreat_desc` = '".$this->treatment['treatment']."'";

            $res = $this->conn->execute($query);
            if ($res) {
                return true;
            } else return false;


	} catch (Exception $e) {
			set_error_handler (array("Error_handler", "my_error_handler"));
        }
	}   //END insertTreatment()


	//The third param here ($newApp) & the "if (!$newApp)" block was added by TUNDE so that the contents of the if block would not be executed when scheduling a new follow-up appointment
/**
 * Updates the status of appointments to show that it was not missed by the patient
 * @param int $app_id       The ID of the appointment in the appointment table
 * @param int $patadm_id    The ID of the current medical transaction for a particular patient. That is, the ID from "patient_admission" table
 * @param boolean $newApp   Defaults to false, but when specified as true, the appointment is treated as a new one
 * @return <type>
 */
	function updateAppointmentStatus($app_id, $patadm_id, $newApp = false){
		try{
			if (!$newApp){
				$query = "UPDATE `appointment` SET `app_status` = '1'
												WHERE app_id = '".$app_id."'";

				$res = $this->conn->execute($query);
			} else $res = false;
			if($res){
				$querys = "UPDATE `pat_transtotal` SET `pattotal_receipt` = '1'
											WHERE patadm_id = '".$patadm_id."'
											AND (pattotal_newstatus='0' OR pattotal_newstatus='1' OR pattotal_newstatus='2')
											AND pattotal_status='1'";
			} else return FALSE;

		}catch (Exception $e){

			set_error_handler (array("Error_handler", "my_error_handler"));

		}
	}   //END updateAppointmentStatus()



/**
 * Retrieves the prescription details of a casenote instance
 * @param int $casenote_id      The ID of the casenote instance in the casenote table
 * @return string               Returns generated HTML table rows (without a containing table tag) of the prescription details
 */
	function selectPrescription($casenote_id){
	try{
			$query ="SELECT casepres_drugname as drug,  casepres_drugdesc as dosage
					FROM casenote_prescription WHERE `casenote_id` = '".$casenote_id."'";

            $res = $this->conn->execute($query);
            if ($res) {
                 $dee = "";
                 $k = 0;
                 while ($row = 	mysql_fetch_array($res)){
                    $k +=1;
                    extract($row);
                     $dee .= " <tr>
                                    <td align=\"left\" valign=\"top\">$k. </td>
                                    <td align=\"left\" valign=\"top\">$drug</td>
                                    <td align=\"left\" valign=\"top\">$dosage</td>
                                  </tr>";
                 }
                 return  $dee;
            } else return false;


	}catch (Exception $e) {

			set_error_handler (array("Error_handler", "my_error_handler"));

        }
	}   //END selectPrescription()





/**
 * Retrieves the lab test details of a casenote instance
 * @param int $casenote_id      The ID of the casenote instance in the casenote table
 * @return string               Returns a string containing the lab test on success or false on failure
 */
	function selectLabtest($casenoteID){
	/*
    try{
			$query ="SELECT caselab_desc FROM casenote_labtest WHERE `casenote_id` = '".$casenote_id."'";

            $res = $this->conn->execute($query);
            if ($res) {
                $num = mysql_num_rows($res);
                if($num > 0){
                    while($row = mysql_fetch_array($res)){
                        $final = $row['caselab_desc'];
                    }
                } else $final = 0;
                return $final;
            } else return false;


	}catch (Exception $e) {

			set_error_handler (array("Error_handler", "my_error_handler"));

        }
     *
     */
        //http://localhost/ehospital/index.php?p=doc_details&m=lab&transid=121
        $query ="SELECT caselabreq_testid 'testTransID', caselabreq_testname 'testname' FROM casenote_labtest
                    WHERE `casenote_id` = '" . $casenoteID . "'";

        $result = $this->conn->execute($query);
        $retVal = "";
        if ($this->conn->hasRows($result)) {
            $sNo = 0;
            $juggleRows = true;
            $retVal = "<table cellspacing=\"2\" cellpadding=\"2\" border=\"1\" class=\"reportTables\">";
            while($row = mysql_fetch_array($result)){
                $rowClass = $juggleRows ? " class=\"tr-row\" " : " class=\"tr-row2\" ";
                $juggleRows = !$juggleRows;
                $link = "index.php?p=doc_details&m=lab&transid=" . $row["testTransID"];
                $retVal .= "<tr $rowClass>
                                <td>" . (++$sNo) . ".</td>
                                <td><a href=\"$link\" target=\"_blank\" title=\"Click to view results\">{$row['testname']}</a></td>
                            </tr>";
            }   //END while
            $retVal .= "</table>";
        }   //END if
        return $retVal;
	}   //END selectLabtest()






/**
 * Retrieves the main treatment details of a casenote instance
 * @param int $casenote_id      The ID of the casenote instance in the casenote table
 * @return string               Returns a string containing the lab test on success or false on failure
 */
	function selectTreatment($casenote_id){
        try{
                $query ="SELECT casetreat_desc FROM casenote_treatment WHERE `casenote_id` = '".$casenote_id."'";

                $res = $this->conn->execute($query);
                if ($res) {
                    $num = mysql_num_rows($res);
                    if($num > 0){
                        while($row = mysql_fetch_array($res)){
                            $final = $row['casetreat_desc'];
                        }
                    } else $final = 0;
                    return $final;
                } else return false;
        } catch (Exception $e) {
                set_error_handler (array("Error_handler", "my_error_handler"));

        }
	}   //END selectTreatment()




/**
 * Retrieves the history part of treatment details for a particular casenote instance
 * @param int $casenote_id      The ID of the casenote instance in the casenote table
 * @return array                Returns an array containing the details on success or an array containing the required keys, but with empty values if the retrieval fails
 */
	function getHistory($casenote_id){
		try{
				$query = "SELECT * FROM casenote_history WHERE `casenote_id` = '".$casenote_id."'";
				$res = $this->conn->execute($query);
				$num = mysql_num_rows($res);

				if($num > 0){
					while($row = mysql_fetch_array($res)){
                        extract($row);
                        $final =  array('casehist_complaint' => $casehist_complaint,
                                        'casehist_complainthist' => $casehist_complainthist,
                                        'casehist_medhist' => $casehist_medhist,
                                        'casehist_sochist' => $casehist_sochist,
                                        'casehist_drughist' => $casehist_drughist,
                                        'casehist_others' => $casehist_others,
                                        'casehist_sysreview' => $casehist_sysreview
                                    );
                    }
				} else {
					$final =  array('casehist_complaint' => "",
								  	'casehist_complainthist' => "",
									'casehist_medhist' => "",
									'casehist_sochist' => "",
									'casehist_drughist' => "",
									'casehist_others' => "",
									'casehist_sysreview' => ""
								);
				};
				return $final;

		} catch (Exception $e){

			set_error_handler (array("Error_handler","my_error_handler"));

		}

	}   //END getHistory()





/**
 * Retrieves the main casenote details of treatment details for a particular casenote instance
 * @param int $casenote_id      The ID of the casenote instance in the casenote table
 * @return array                Returns an array containing the details on success or an array containing the required keys, but with empty values if the retrieval fails
 */
	function getCasenote($casenote_id){
		try{
				$query = "SELECT * FROM casenote WHERE `casenote_id` = '".$casenote_id."'";
				$res = $this->conn->execute($query);
				$num = mysql_num_rows($res);

				if($num>0){
					while($row = mysql_fetch_array($res)){
                        extract($row);
                        $final =  array('casenote_date' => $casenote_date,
                                        'casenote_genexam' => $casenote_genexam,
                                        'casenote_sysexam' => $casenote_sysexam,
                                        'casenote_diagnosis' => $casenote_diagnosis,
                                        'casenote_diffdiagnosis' => $casenote_diffdiagnosis,
                                        'user_id' => $user_id,
                                        'app_id' => $app_id
                                    );
					}
				} else {
					$final =  array('casenote_date' => "",
								  	'casenote_genexam' => "",
									'casenote_sysexam' => "",
									'casenote_diagnosis' => "",
									'casenote_diffdiagnosis' => "",
									'user_id' => "",
									'app_id' => ""
								);
				}
				return $final;

		} catch (Exception $e) {

			set_error_handler (array("Error_handler","my_error_handler"));

		}

	}   //END getCasenote()





/**
 * Sets the current condition type of a patient (In-patient, Out-Patient) for easy tracking
 * @param int $patadm_id      The ID of the current medical transaction for a particular patient. That is, the ID from "patient_admission" table
 * @return boolean            Returns true on success, else false
 */
	function insertConditiontrack($patadm_id, $ctc_id){
        try {
                $query ="INSERT INTO pat_conditiontrack
                            SET `patadm_id` = '".$patadm_id."',
                                `ctc_id` = '".$ctc_id."',
                                `pct_condition_date` = NOW()";

                $res = $this->conn->execute($query);
                if ($res) {
                    return true;
                } else return false;


        } catch (Exception $e) {
            set_error_handler (array("Error_handler", "my_error_handler"));
        }
	}   //END insertConditiontrack()




/**
 * Gets the ID of the next appointment of a patient
 * @param string $hospital_no       The patient's hospital number
 * @return mixed                    The appointment ID on success or false on failure
 */
	function getAppID($hospital_no){
        try{
                $query = "SELECT MIN(app_id) as app_id FROM appointment app, registry reg, patient_admission p
                            WHERE p.patadm_id = app.patadm_id AND reg.reg_hospital_no = p.reg_hospital_no AND app.app_starttime >= CURDATE() AND p.reg_hospital_no='$hospital_no'";
                $res = $this->conn->execute($query);
                //$num = mysql_num_rows($res);
                if ($res) {
                    while($row = mysql_fetch_array($res)){
                        return $row['app_id'];
                    }
                } else return false;
        } catch (Exception $e) {
            set_error_handler (array("Error_handler", "my_error_handler"));
        }
	}   //END getAppID()






/**
 * Gets past visits of a patient to a particular consultant
 * @param string $hospital_no       The hospital number of the patient under consideration
 * @param int $consultant_id        The ID of the consultant being considered
 * @return string                   The generated HTML table rows (without an enclosing table tag) containing part of the details of the treatment
 */
	function getCases($hospital_no, $consultant_id){
		try{
            $query = "SELECT DISTINCT c.casenote_diagnosis as diagnosis, c.casenote_diffdiag as diffdiag,
                            ct.casetreat_desc as treatment,
                            c.casenote_id as case_id,
                            DATE(app.app_starttime) as appdate,
                            cl.caselab_desc as labtest
                       FROM casenote c
                       INNER JOIN patient_admission pa ON pa.patadm_id = c.patadm_id
                       INNER JOIN appointment app ON (app.patadm_id = pa.patadm_id)
                       LEFT JOIN casenote_labtest cl ON c.casenote_id = cl.casenote_id
                       LEFT JOIN casenote_treatment ct ON c.casenote_id = ct.casenote_id
                       WHERE pa.reg_hospital_no = '$hospital_no'
                            AND app.consultant_id = '$consultant_id'
                            AND app.app_status = '1'";

 					$res = $this->conn->execute($query);
					if($res){
                        $dee = "";
                        $num = mysql_num_rows($res);
                        if($num !=0){
                            while ($row = mysql_fetch_array($res)){
                                extract($row);
                                $dee .= "<tr>
                                            <td valign='top'>$appdate</td>
                                            <td valign='top'>$diagnosis</td>
                                            <td valign='top'><table width='0' border='0' cellpadding='0'>".$this->selectPrescription($case_id)."</table></td>
                                            <td valign='top'>$labtest</td>
                                            <td valign='top'>$treatment</td>
                                            <td valign='top'><a href='index.php?p=viewappdetails&m=patient_care&a=$case_id' target='_blank'>View Detail</a></td>
                                          </tr>";
                            }
                        }
						return $dee;
					} else return false;

		} catch(Exception $e) {
			set_error_handler (array("Error_handler", "my_error_handler"));
		}
	}   //END getCases()



/**
 * Gets the list of wards in a drop-down. If a clinic ID is provided, then only wards in that clinic would be shown
 * @param int $clinicID The ID of the clinic whose wards are to be shown
 * @param int $selected The ID of the currently selected item in the list
 * @return string       The generated HTML containing the list of wards for display in a drop-down box
 */
    public function getWards4DropDown($clinicID = 0, $selected = 0){
        $clinicID = (int)$clinicID;
        $query = "SELECT w.ward_id 'id', lc.lang1 'ward' FROM wards w INNER JOIN language_content lc
                    ON w.langcont_id = lc.langcont_id ";
        $query .= !empty($clinicID) ? " WHERE w.clinic_id ='$clinicID' " : "";
        $query .= " ORDER BY ward ";
        $result = $this->conn->execute($query);
        $retVal = "";
        if ($this->conn->hasRows($result)){
            while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
                if ($row["id"] == $selected){
                    $retVal .= "<option value=\"" . $row["id"] . "\" selected=\"selected\">" . stripslashes($row["ward"]) . "</option>";
                } else {
                    $retVal .= "<option value=\"" . $row["id"] . "\">" . stripslashes($row["ward"]) . "</option>";
                }   //END else part of if ($row["id"] == $selected)
            }   //END while
        }   //END if has result has rows
        return $retVal;
    }




}	//END class

?>