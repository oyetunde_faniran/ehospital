<?php
/* Contains variables, methods and other functionalitieds to manage service fees
@author ahmed rufai
 * @package service
 * Create Date: 26-08-2009
 */
require_once './classes/DBConf.php';

Class service {
      /**
     *
     * @var array holds array of fields in the drugs category table
     */
    public $service=array() ;
     /**
     * @var array holds values to be stored in the drug type table field
       */
    public $service2=array() ;
	public $service_id; //int(10)
	public $servicecat_id; //int(10)
	public $dusc_id; //int(10)
	public $servamount_amount; //int(10)
	public $service_parentid ;
	public $service_desc ;
	public $conn;
	public $retVal="" ;
	public $dcontent="";
		   
		/**
         * class constrctor
         */
		 function  __construct() {
        $this->conn = new DBConf();
    }
 
 		
 
 /**
  * populate option tags with service sub items
  * @param int $disItem
   * @param string $curLnag_field
     * @param string $langtb
  * @return string
  */
    function getChildren4DropDown($disItem,$curLnag_field,$langtb){
	$retVal = "";
 $getlang= new language();
/*	$query = "SELECT service_id, service_name FROM service
				WHERE service_parentid = '$disItem'";*/
	$query = "SELECT n.service_id, n.dusc_id, a.servamount_amount, n.service_type 
				FROM service n LEFT JOIN service_amount a
				ON n.service_id = a.service_id
				WHERE n.service_parentid = '$disItem' AND a.servamount_amount IS NULL";//	die ($query);


	$result = $this->conn->execute($query);
	if ($this->conn->hasRows($result)){
		while ($row = mysql_fetch_array ($result, MYSQL_ASSOC)){
		$hint=$row['service_type'];
		if($hint!='Servicename'){
		$dcontent=$this->getTablename($hint,$row['dusc_id'],$curLnag_field,$langtb);
		//$dcontent=$this->getTablename($hint,$row['dusc_id'],$curLnag_field,$langtb);
			$retVal .= "<option value=\"{$row['service_id']}\">{$dcontent}</option>\n";
			
			}
			}
	} else $retVal = "<option disabled=\"disabled\">No Children</option>";
	return $retVal;
}
/**
 *
 * @param string $hint
 * @param int $id
 * @param string $curLnag_field
 * @param string $langtb
 * @return string
 */
 function getTablename($hint,$id,$curLnag_field,$langtb) {
        try {
             //$getlang= new language();
            if ($hint=='Dept') {
				$query = "SELECT langcont_id FROM department
				WHERE dept_id =". $id." LIMIT 1";
				$flag='no';	
				}		
				
			elseif($hint=='Unit'){
				$query = "SELECT langcont_id FROM clinic
				WHERE clinic_id =". $id." LIMIT 1";
				//echo $query;
				$flag='no';	
				}
			elseif($hint=='Servicename'){
				$query = "SELECT langcont_id FROM service_name
				WHERE sername_id =". $id." LIMIT 1";
				//echo $query;
				$flag='no';	
				}
			elseif($hint=='Servicelab'){
				
				$query = "SELECT langcont_id FROM lab_test
				WHERE lab_test_id =". $id." LIMIT 1";
				//
				$flag='no';	
				}
			elseif ($hint=='Constraint'){
					$query = "SELECT langcont_id FROM service_constraint
						WHERE servcon_id =". $id." LIMIT 1";
					$flag='no';	
				}else{
					$query = "SELECT * FROM service
						WHERE service_id ='1'";
					$flag='yes';	
				}
				
				
				//echo $query;
				//exit();
			
			 if($hint=='Servicelab'){
			
				 $this->conn->labConnect();
				 $result = $this->conn->execute($query);
			
				
				}else{
				$result = $this->conn->execute($query);
				
				}
			
			if ($this->conn->hasRows($result, 1)){
					//die("$query");
                    if($flag!='yes'){
						$row = mysql_fetch_array ($result, MYSQL_ASSOC);
						 
						$getlang= new language();
						$langcont=$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
							//die("$query".$langcont);
							}else{
				$row = mysql_fetch_array ($result, MYSQL_ASSOC);
						$langcont=$row['service_type'];
								}
								
			}
			
			if(isset($langcont))
			return $langcont;
        } catch (Exception $e) {
			//$err = new Error_handler();
			//set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }
    /**
  *returns service items with their apropriate fees
  * @param int $disItem
   * @param string $curLnag_field
   * @param string $langtb
  * @return string
  */
function getChildren($disItem,$curLnag_field,$langtb){
	$retVal = "";
 $getlang= new language();
	$query = "SELECT n.service_id, n.dusc_id, a.servamount_amount, n.service_type 
				FROM service n LEFT JOIN service_amount a
				ON n.service_id = a.service_id
				WHERE n.service_parentid = '$disItem'";//	die ($query);
	$result = $this->conn->execute($query);
	
	if ($this->conn->hasRows($result)){
	  
		while ($row = mysql_fetch_array ($result, MYSQL_ASSOC)){
		
		  $hint=$row['service_type'];
			if (empty($row["servamount_amount"])){
			   	$dcontent=$this->getTablename($hint,$row['dusc_id'],$curLnag_field,$langtb);
				$retVal .= "<a href=\"?paret=$disItem&i={$row['service_id']}&p=service\">{$dcontent}</a>";
				if ($hint!='Unit' && $hint!='Dept' && $hint!='Constraint')
				$retVal.=' >>'."<a href=\"?paret=$disItem&i={$row['service_id']}&p=editservice&edit=yes&du={$row['dusc_id']}\">Edit</a>";
				$retVal.= "<BR />";
			} else{
			$dcontent=$this->getTablename($hint,$row['dusc_id'],$curLnag_field,$langtb);
			 $retVal .= "<a href=\"?paret=$disItem&i={$row['service_id']}&p=editservice&edit=yes\">{$dcontent}</a> (&#8358;" . number_format($row['servamount_amount'], 2) . ")"."<BR />";
			 }
					}
		//die("end");
	} else $retVal .= "<em>NONE</em>";
	return $retVal;
}
/**
 * generate lineage of a particular fee structure
 * @param int $parent
 * @param int $disItem
   * @param string $curLnag_field
   * @param string $langtb
  * @return string
 */
function getLineage($parent, $disItem,$curLnag_field,$langtb){
	 $getlang= new language();
	$retVal = "";
	$query = "SELECT dusc_id,service_type FROM service
				WHERE service_id = '$disItem'";
	$result = $this->conn->execute($query);
	if ($this->conn->hasRows($result, 1)){
		$row = mysql_fetch_array ($result, MYSQL_ASSOC);
		$hint=$row['service_type'];
		$dcontent=$this->getTablename($hint,$row['dusc_id'],$curLnag_field,$langtb);
		$retVal .= $dcontent;
		$counter = 0;
		$dis = $parent;
		//Get the lineage of the current item up to 5 previous generations
		do {
			$parent = $this->getParent($dis,$curLnag_field,$langtb);
			if (count($parent) > 0){
				$retVal = "<a href=\"?paret={$parent['parent']}&i={$parent['disID']}&p=service\">{$parent['disName']}</a> &raquo; " . $retVal;
				$hasParent = true;
				$dis = $parent["parent"];
			} else $hasParent = false;
//			echo "<pre>";
//			print_r($parent);
//			echo "</pre>";
//			exit();
			$counter++;
		} while ($hasParent && $counter < 5);
	}
	return $retVal;
}

/**
 * get parent of a particular fee item
 * @param int $disItem
 * @param string $curLnag_field
 * @param string $langtb
 * @return string
 */
function getParent($disItem,$curLnag_field,$langtb){
	$retVal = array();
	$getlang= new language();
	$query = "SELECT * FROM service
				WHERE service_id = '$disItem'";
	$result = $this->conn->execute($query);
	if ($this->conn->hasRows($result, 1)){
		$row = mysql_fetch_array ($result, MYSQL_ASSOC);
		$hint=$row['service_type'];
		$dcontent=$this->getTablename($hint,$row['dusc_id'],$curLnag_field,$langtb);
		$retVal = array (
					"parent" => $row["service_parentid"],
					"disID" => $disItem,
					"disName" => $dcontent
					);
	}
	return $retVal;
}

/**
 * returns item description
 * @param int $disItem
 * @param string $curLnag_field
 * @param string $langtb
 * @return string
 */
function getItemName($disItem,$curLnag_field,$langtb){
	$getlang= new language();
	$retVal = "";
	$query = "SELECT dusc_id,service_type FROM service
				WHERE service_id = '$disItem' ";
			//	die("$query");
	$result = $this->conn->execute($query);
	if ($this->conn->hasRows($result, 1)){
		$row = mysql_fetch_array ($result, MYSQL_ASSOC);
		$hint=$row['service_type'];
		$dcontent=$this->getTablename($hint,$row['dusc_id'],$curLnag_field,$langtb);
		$retVal = $dcontent;
	}
	return $retVal;
}
/**
 * returns item description
 * @param int $disItem
 * @param string $curLnag_field
 * @param string $langtb
 * @return string
 */
function getItemName2($disItem,$curLnag_field,$langtb){
	$getlang= new language();
	$retVal = "";
	$query = "SELECT dusc_id,service_type FROM service
				WHERE service_id = '$disItem' ";
	$result = $this->conn->execute($query);
	if ($this->conn->hasRows($result, 1)){
		$row = mysql_fetch_array ($result, MYSQL_ASSOC);
		$hint=$row['service_type'];
		$dcontent=$this->getTablename($hint,$row['dusc_id'],$curLnag_field,$langtb);
		$retVal =$row['service_type'];
	}
	return $retVal;
}

/**
 *
 * @param array $_POST
 * @param int $disItem
 * @param string $curLnag_field
 * @param string $langtb
 * @return string
 */
function addItem($_POST,$curLnag_field,$langtb){
	$retVal = false;
	//$getlang= new language();
	foreach ($_POST as $key=>$value){
		if($key=='p' or  $key=='sButton' or $key=='opt_id'){
			
		}else
		$$key = mysql_real_escape_string($value);
    }
	//$sql3="Insert into $langtb ($curLnag_field) values ('".$dusc_id."')";
			 // echo $sql3;
			 //  $this->conn->execute($sql3);
				//$content_id= mysql_insert_id();
		$query = "INSERT INTO service
				VALUES ('0', '$parent', '$dusc_id', '$service_type')";
				//echo 'first'.$query;
	$result = $this->conn->execute ($query);
	if ($this->conn->hasRows($result, 1)){
		//final node with amount attached so save the amount
		if (isset($_POST['itemamount']) && !empty($_POST['itemamount'])){
			$disItem = mysql_insert_id();
			$query = "INSERT INTO service_amount
						VALUES ('0', '$disItem', '0', '0', '$itemamount')";
						//echo 'second'.$query;
			$result = $this->conn->execute($query);
			if ($this->conn->hasRows($result, 1)){
				$retVal[0] = true;
				 $retVal[1] =$disItem ;  //service ID to be inserted in the labtest table
				 $retVal[2] =$dusc_id ;  // lab_test ID in the lab_test table
				 }
		} else $retVal = false;
	}
	return $retVal;
}

	/** Updates service ID in the labtest table
         *
         */
 function updateServiceID_inLabtestTB($labtest_id,$service_id) {
        try {
           $this->conn->labConnect();
		    $sql ="UPDATE lab_test SET service_id='$service_id' where lab_test_id='$labtest_id'";
		   $update=$this->conn->execute($sql);
			//die("$sql");
		  
            
			if(mysql_affected_rows())	 
            	return true ;
			 else return false ;		
        } catch (Exception $e) {


        }
    } //ends update serviceID

	

	public function New_service($servicecat_id,$dusc_id,$service_amount){
		$this->servicecat_id = $servicecat_id;
		$this->dusc_id = $dusc_id;
		$this->service_amount = $service_amount;
	}

    /**
     * Load one row into class variables using specified row key.
     * @param string $curLnag_field
     * @param string $langtb
     * 
     */
	

	public function Load_from_key($key_row,$curLnag_field,$langtb){
		$result =  $this->conn->execute("SELECT n.service_id, n.dusc_id, a.servamount_amount ,n.service_parentid,n.service_type
				FROM service n LEFT JOIN service_amount a
				ON n.service_id = a.service_id
				WHERE n.service_id = '$key_row'");
		while($row =mysql_fetch_array($result)){
			$this->service_id = $row["service_id"];
			$this->service_parentid = $row["service_parentid"];
			$this->dusc_id = $row["dusc_id"];
			$this->servamount_amount = $row["servamount_amount"];
			$this->service_type = $row["service_type"];
		}
	}
    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @param $lang_id id in the service table that matches one in the lang table
     *
     */
	public function Delete_row_from_key($lang_id,$key_row,$langtb){
		$this->conn->execute("DELETE FROM service WHERE service_id = $key_row");
		$this->conn->execute("DELETE FROM $langtb WHERE dusc_id = $lang_id");
	}

    /**
     *  Update the active row on table using supplied row key
     * @param array $POST
     * @param int $id
     * @param string $tablename
     * @paramstring $tablename1
     * @param string $curLnag_field
     * @param string $langtb
     */
     
	public function Save_Active_Row($POST,$id,$tablename,$tablename1,$curLnag_field,$langtb){
		/*$this->conn->execute("UPDATE drugcategory set drugcat_name = \"$this->drugcat_name\" where drugcat_id = \"$this->drugcat_id\"");*/
		
		try {
		$q=count($this->service);
		
		
		/*for ($i = 0; $i < $q; $i++) {
			if($this->service[$i]=='dusc_id')
	//	$quu="UPDATE $langtb set $curLnag_field ='".$this->service2[$i]."' where dusc_id = \"$lang_id\"";
				
		$this->conn->execute("UPDATE $langtb set $curLnag_field ='".$this->service2[$i]."' where dusc_id = \"$lang_id\"");

		}*/
		
			
	$sql = "UPDATE $tablename SET ";	
	
				$qq=count($this->service);
				$q=count($this->service);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
					$sql.=    $this->service[$i] .'="'.$this->service2[$i].'" ';
						//if($this->service[$i]=='dusc_id')
//						     $sql.=    $this->service[$i] .'="'.$lang_id.'" ';
//						else
//         			  		  $sql.=    $this->service[$i] .'="'.$this->service2[$i].'" ';
//			 
					 }
					 else{
					      $sql.=      $this->service[$i] .'="'.$this->service2[$i].'" ,';
					    // if($this->service[$i]=='dusc_id')
//						     $sql.=      $this->service[$i] .'="'.$lang_id.'" ,';
//						 else   
//						     $sql.=      $this->service[$i] .'="'.$this->service2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			$result2 =$this->conn->execute("SELECT * from $tablename");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
		 
		 if (isset($_POST['excep'])&& $_POST['excep']=='yes')
 				   $this->conn->execute($sql);
		 if (isset($_POST['servamount_amount']) or !empty($_POST['servamount_amount']))
			$this->conn->execute("UPDATE $tablename1 SET servamount_amount=".$_POST['servamount_amount']." WHERE service_id = '$id'");
		 
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		}

/**
 * save new row on table
 *  @param string $tablename
* @param string $curLnag_field
* @param string $langtb


 */
public function Save_Active_Row_as_New($tablename,$curLnag_field,$langtb){
		
		try {
		$q=count($this->service);
		
		
		for ($i = 0; $i < $q; $i++) {
			if($this->service[$i]=='dusc_id'){
			$qq="Insert into $langtb ($curLnag_field) values ('".$this->service2[$i]."')";
			//echo $qq;
			    $this->conn->execute($qq);
				$content_id= mysql_insert_id();
			}	
		}
		
		$sql = "INSERT INTO $tablename SET ";	
	
				$qq=count($this->service);
				//$q=count($this->service);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->service[$i]=='dusc_id')
						     $sql.=    $this->service[$i] .'="'.$content_id.'" ';
						else
         			  		  $sql.=    $this->service[$i] .'="'.$this->service2[$i].'" ';
			 
					 }
					 else{
					     if($this->service[$i]=='dusc_id')
						     $sql.=      $this->service[$i] .'="'.$content_id.'" ,';
						 else
						     $sql.=      $this->service[$i] .'="'.$this->service2[$i].'" ,';
			
					 }
  				 $q--;
 			}
		//echo $sql;
		//exit();
		 $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
	}

public function GetSearchFields(){
	echo 	"<option value='-1'>Select Field</option>";	
		$result =$this->conn->execute("SELECT * from service order by service_id");
				$i = 0;
				
while ($i < mysql_num_fields($result)) {
 //   echo "Information for column $i:<br />\n";
    $meta = mysql_fetch_field($result);
	
    if (!$meta) {
      //  echo "No information available<br />\n";
    }
  //  echo "$meta->name";
  
if($meta->name=='dusc_id')
	echo 	"<option value=". $meta->name.">".'Service'."</option>";
   else
	echo 	"<option value=". $meta->name.">".$meta->name."</option>";
	   
	   
	   
	   
	    $i++;
}
/*mysql_free_result($result);
	return $keys;*/
	}
/**
     * list all rows from a specified table
     * @param string $curLnag_field
     * @param string $langtb
     */
function allrows($curLnag_field,$langtb) {
        
		try {
	 $getlang= new language();
	$db2 = new DBConf();
		 $sql = 'SELECT
*
FROM
service
';
$pageindex='service';
$pager = new PS_Pagination($db2,$sql,2,10,$pageindex);
$rs = $pager->paginate();
           

           // $res = $this->conn->execute($sql);
			$i=1;
           while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["service_id"].'" /></td>
        <td>'.$row["service_id"].'</td>
		  <td>'.$getlang->getlang_content($this->getServcat_cont_id($row["service_id"]),$curLnag_field,$langtb).'</td>
		  <td>'.$getlang->getlang_content($row['dusc_id'],$curLnag_field,$langtb).'</td>
		    <td>'.$row["service_amount"].'</td>
		
        <td>'.'<a href = "./index.php?jj=delete&p=service&id='.$row["service_id"].'&langid='.$row["dusc_id"].'"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td><td>'.'<a href = "./index.php?p=editservice&service_id='.$row["service_id"].'&langid='.$row["dusc_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
        } catch (Exception $e) {
	
        }

  
    }
	
	function getServcat_cont_id($id) {
        try {
           $sql = 'Select dusc_id from servicecat  where servicecat_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
            return $row['dusc_id'];
				 
            
			
        } catch (Exception $e) {


        }
    }
	/**
     * gets item id in the service table
     * @param <type> $id
     * @param <type> $curLnag_field
     * @param <type> $langtb
     * @return <type>
     *
     */
	function getItem_no($id,$curLnag_field,$langtb) {
        try {
           $sql = 'Select dusc_id from service where service_id='.$id;
            $res = $this->conn->execute($sql);
			if($res){
            $row = mysql_fetch_array($res);
			//echo $sql ;
			//exit();
           $thisitem=$row['dusc_id'];
		   
		    return $thisitem;
				 }
            
			
        } catch (Exception $e) {


        }
    }
	

    /**
     * list rows that match specified criteria
     * @param string $search_field
     * @param string $search_value
     * @param string $curLnag_field
     * @param string $langtb
     */
    
	function rowSearch($search_field,$search_value,$curLnag_field,$langtb){
		 try {
		  $getlang= new language();
		 $db2 = new DBConf();
		$sql = "Select * from  service  where  ".$search_field."='".$search_value."'" ;
		$res =$this->conn->execute($sql);
		
$pageindex='service';
$pager = new PS_Pagination($db2,$sql,2,10,$pageindex);
$rs = $pager->paginate();
	  // echo "entered";
	     $i=1;
		while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["service_id"].'" />'.$ii.'</td>
        <td>'.$row["service_id"].'</td>
		 <td>'.$row["servicecat_id"].'</td>
		<td>'.$getlang->getlang_content($row['dusc_id'],$curLnag_field,$langtb).'</td>
		       
        <td>'.'<a href = "./index.php?jj=delete&p=service&id='.$row["service_id"] .'"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td><td>'.'<a href = "./index.php?p=editservice&service_id='.$row["service_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
		} catch (Exception $e) {
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result =  $this->conn->execute("SELECT service_id from service order by $column $order");
			while($row = mysql_fetch_array($result)){
				$keys[$i] = $row["service_id"];
				$i++;
			}
	return $keys;
	}

    /**
     * Close mysql connection
     */
	public function endservice(){
		$this->connection->CloseMysql();
	}
	/**
     * populate option tags with department namaes
     */
	 function deptName4service() {
            try {
            $sql = 'select dept_id, dept_name from department';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                  echo '<option value='.$row['dept_id'].'>'.$row['dept_name'].'</option>';
            }
        } catch (Exception $e) {
		set_error_handler (array("Error_handler", "my_error_handler"));
        }
    }

		/** Updates service ID in the labtest table
         *
         */
// function updateServiceID_inLabtestTB($labtest_id,$service_id) {
//        try {
//           $this->conn=new DBConf();
//			$this->conn->labConnect();
//		   $sql ="UPDATE lab_test SET service_id='$service_id' where labtest_id='$labtest_id'";
//            $update=$this->conn->execute($sql);
//			if (mysql_affected_rows($update)){
//				return true;
//			} else {
//					return false;
//				}
//        } catch (Exception $e) {
//
//
//        }
//    } //ends update serviceID

}