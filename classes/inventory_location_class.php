<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventoryCreationFormProcessingClass
 *
 * @author upperlink
 */
class inventory_location_class extends inventoryGeneralClass {

    public $conn;

    public function __construct() {
        $conn = new DBConf();
        $this->conn = $conn->mysqliConnect();
        //$this->conn=new mysqli('localhost','root','','mediluth_skye');
    }

    public function createNewLocation($name, $description) {
        global $report;
        $report = "";
        global $inventoryLocationCreationSuccessNews;
        global $inventoryLocationCreationFailureNews;
        global $inventory_no_locationName_error_msg;
        if (strlen($name) < 1) {
            $report.=$inventory_no_locationName_error_msg;
        }
        if ($report != "") {
            return $report;
        }
        $createLocation = $this->conn->query("insert into inventory_locations (inventory_location_name, inventory_location_description, inventory_location_date_created, user_id) values ('" . $name . "','" . $description . "',CURRENT_TIMESTAMP, '" . $_SESSION[session_id() . "userID"] . "')");
        if (!$createLocation) {
            $report.=$inventoryLocationCreationFailureNews;
        } else {
            $report.=$inventoryLocationCreationSuccessNews;
        }
        return $report;
    }

    public function get_existing_items($table, $orderCollumn, $orderStyle) {
        global $items;
        global $report;
        $items = $this->conn->query("select * from $table order by $orderCollumn $orderStyle");

        if (!$items) {

            return FALSE;
        }
        if ($items->num_rows < 1) {
            $report = $none_in_existence;
            return $report;
        }
        return $items;
    }

    public function listLocations() {
        global $report;
        global $inventory_location_dbconnnect_error;
        $findLocationQuery = $this->conn->query("Select * from inventory_locations order by inventory_location_name");
        if (!$findLocationQuery) {
            $report = $inventory_location_dbconnnect_error;
            return $report;
        }
        if ($findLocationQuery->num_rows < 1) {
            $report = $inventory_no_location_msg;
            return $report;
        }
        return $findLocationQuery;
    }

    public function getLocationProperties($id) {
        global $report;
        $locationPropertyQuery = $this->conn->query("select * from inventory_locations where inventory_location_id='" . $id . "'");
        if (!$locationPropertyQuery) {
            $report = $inventory_location_dbconnnect_error;
            return $report;
        }
        if ($locationPropertyQuery->num_rows != 1) {
            $report = $inventory_location_notFound_msg;
            return $report;
        }
        $locationProperty = $locationPropertyQuery->fetch_assoc();
        return $locationProperty;
    }

    public function updateLocation($id, $name, $desc, $author) {
        global $report;
        $updateQuery = $this->conn->query("update inventory_locations set inventory_location_name='" . $name . "', inventory_location_description='" . $desc . "', user_id='" . $author . "' where inventory_location_id='" . $id . "'");
        if (!$updateQuery) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function removeLocation($id) {
        global $report;
        global $inventory_location_delete_success_msg;
        global $inventory_location_delete_error_msg;

        $removed = $this->conn->query("delete from inventory_locations where inventory_location_id='" . $id . "'");
        if (!$removed) {
            $report = $inventory_location_delete_error_msg;
            return $report;
        } else {
            $report = $inventory_location_delete_success_msg;
            return $report;
        }
    }
    
    
    
    /**
     * Gets all available locations into an array
     * @return array    An array containing all available locations. Returns an empty array if no location's available 
     */
    public function getLocations(){
        $conn = new DBConf();
        $query = "SELECT * FROM inventory_locations
                    ORDER BY inventory_location_name";
        $result = $conn->execute($query);
        $retVal = array();
        if ($conn->hasRows($result)){
            while ($row = mysql_fetch_assoc($result)){
                $retVal[] = $row;
            }
        }
        return $retVal;
    }   //END getLocations()
    
    
    

}   //END class

//END class
?>
