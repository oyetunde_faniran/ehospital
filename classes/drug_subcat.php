<?php
/*
 *Contains class variables & methods to select, save, update and delete rows from drug table
 *@package drugsub_cat
 *@author: Rufai Ahmed
 *Create Date: 26-08-2009
 */
require_once './classes/DBConf.php';

Class drug_subcat {
 /**
 *
 * @var array holds array of fields in the drugs subcategory table

 */
    public $dsubcat=array() ;
    /**
     * @var array values to be stored in the drugs table field
       */
    public $dsubcat2=array() ;
    /**
     *
     * @var integer
     */
	public $drugsub_id; //int(10)
    /**
     *
     * @var integer
     */
	public $langcont_id;
    /**
     *
     * @var integer database connection handle
     */
	public $connection;
    public $conn;
    /**
     * class constructor
     */
	 function  __construct() {
     $this->conn = new DBConf();
    }
    

    /**
     * Load one row into class variables
     *
     * @param integer $key_row
     * 
     */
	public function Load_from_key($key_row){
		$result = $this->conn->execute("Select * from drug_subcat where drugsub_id = \"$key_row\" ");
		while($row = mysql_fetch_array($result)){
					$this->drugsub_id = $row["drugsub_id"];
			$this->drugcat_id = $row["drugcat_id"];
			$this->langcont_id = $row["langcont_id"];
			
		}
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param integer $key_row
     * @param integer $lang_id ID of the row in the language table
     * @param string $langtb
     *
     *
     */
	public function Delete_row_from_key($lang_id,$key_row,$langtb){
		$this->conn->execute("DELETE FROM drug_subcat WHERE drugsub_id = $key_row");
		$this->conn->execute("DELETE FROM $langtb WHERE langcont_id = $lang_id");
	}

    /**
     * Update the active row on table using row key as argument
     * @param integer $lang_id
     * @param int  $id
     * @param string $tablename
     * @param string $curLnag_field field in the lang table that holds current lang text
     * @param string $langtb lang table name
     */
    
public function Save_Active_Row($lang_id,$id,$tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("UPDATE drug_subcat set drugcat_name = \"$this->drugcat_name\" where drugsub_id = \"$this->drugsub_id\"");*/
		
		try {
		$q=count($this->dsubcat);
		
		
		for ($i = 0; $i < $q; $i++) {
			if($this->dsubcat[$i]=='langcont_id')
		//	$qq="UPDATE $langtb set $curLnag_field ='".$this->dsubcat2[$i]."' where langcont_id = \"$lang_id\"";
				
		$this->conn->execute("UPDATE $langtb set $curLnag_field ='".$this->dsubcat2[$i]."' where langcont_id = \"$lang_id\"");

		}
		
			
	$sql = "UPDATE $tablename SET ";	
	
				$qq=count($this->dsubcat);
				$q=count($this->dsubcat);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->dsubcat[$i]=='langcont_id')
						     $sql.=    $this->dsubcat[$i] .'="'.$lang_id.'" ';
						else
         			  		  $sql.=    $this->dsubcat[$i] .'="'.$this->dsubcat2[$i].'" ';
			 
					 }
					 else{
					     if($this->dsubcat[$i]=='langcont_id')
						     $sql.=      $this->dsubcat[$i] .'="'.$lang_id.'" ,';
						 else
						     $sql.=      $this->dsubcat[$i] .'="'.$this->dsubcat2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			$result2 =$this->conn->execute("SELECT * from $tablename");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
		//echo $sql;
	//exit();
         $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
	}

    /**
     *Save the active row as a new row on table
     * @param string $tablename
     * @param string $curLnag_field
     * @param string $langtb
     * @return array
     */
    
	public function Save_Active_Row_as_New($tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("Insert into drug_subcat (drugcat_name) values (\"$this->drugcat_name\"");*/
		try {
		
		// server validation begins
					$getLang= new language();		
					$xlp_formfield =$this->dsubcat;
					$xlp_fieldmessage = array('Please specify Drug Sub category name','Please Select Category Name');
		            $xlp_error=$getLang->xlpildator($xlp_formfield,$xlp_fieldmessage);
					if (!empty($xlp_error[2]))
					throw new Exception;
					//sever validation ends
		
		$q=count($this->dsubcat);
		
		for ($i = 0; $i < $q; $i++) {
			if($this->dsubcat[$i]=='langcont_id'){
			admin_Tools::doEscape($this->dsubcat2, $this->conn);
			$qq="Insert into $langtb ($curLnag_field) values ('".$this->dsubcat2[$i]."')";
			//echo $qq;
			    $this->conn->execute($qq);
				$content_id= mysql_insert_id();
			}	
		}
		
		$sql = "INSERT INTO $tablename SET ";	
	
				$qq=count($this->dsubcat);
				//$q=count($this->dsubcat);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->dsubcat[$i]=='langcont_id')
						     $sql.=    $this->dsubcat[$i] .'="'.$content_id.'" ';
						else
         			  		  $sql.=    $this->dsubcat[$i] .'="'.$this->dsubcat2[$i].'" ';
			 
					 }
					 else{
					     if($this->dsubcat[$i]=='langcont_id')
						     $sql.=      $this->dsubcat[$i] .'="'.$content_id.'" ,';
						 else
						     $sql.=      $this->dsubcat[$i] .'="'.$this->dsubcat2[$i].'" ,';
			
					 }
  				 $q--;
 			}

		 $result=$this->conn->execute($sql);
				 $list=array();
				if($this->conn->hasRows($result,  0)){
					 $list[3]=true;
					return $list;	 
					}			 
        } catch (Exception $e) {
            return $xlp_error;
        }
		
		
		
		
	}
    /**
     * get fields from the database table
     */
	
	public function GetSearchFields(){
	echo 	"<option value='-1'>Select Field</option>";	
		$result =$this->conn->execute("SELECT * from drug_subcat order by drugsub_id");
				$i = 0;
				
while ($i < mysql_num_fields($result)) {
 //   echo "Information for column $i:<br />\n";
    $meta = mysql_fetch_field($result);
	
    if (!$meta) {
      //  echo "No information available<br />\n";
    }
  //  echo "$meta->name";
  
if($meta->name=='langcont_id')
	echo 	"<option value=". $meta->name.">".'drug category name'."</option>";
   else
	echo 	"<option value=". $meta->name.">".$meta->name."</option>";
	   
	   
	   
	   
	    $i++;
}
/*mysql_free_result($result);
	return $keys;*/
	}

    /**
     * returns drug sub category desc for the specified row ID
     * @param int $id
     * @return string
     */
	
	function getsubdrug_name($id) {
        try {
           $sql = 'Select * from drug_subcat  where drugsub_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
            return $row['drug_desc'];
				 
            
			
        } catch (Exception $e) {


        }
    }
/*function getlang_content($id,$curLnag_field,$langtb) {
        try {
           $sql = 'Select '.$curLnag_field.'  from '. $langtb.'  where langcont_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			return $row[$curLnag_field];
		} catch (Exception $e) {


        }
    }*/

    /**
     *populates option tags with drug sub category description highlighting active option that matches supplied ID
     * @param int $id
     * @param string $curLnag_field
     * @param string $langtb
     */
function getsubdrug2($id,$curLnag_field,$langtb) {
        try {
		    $getLang= new language();
			$sql2 = 'select drugsub_id from drug where drug_id ='.$id;
			
				 $res2 = $this->conn->execute($sql2);
                  if($res2){
				$row2 = mysql_fetch_array($res2);
				  $id2=$row2['drugsub_id'];
                 }
            $sql = 'select drugsub_id,langcont_id from drug_subcat ';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
			$sel= ($id2==$row['drugsub_id'])? "selected":" ";
			
                echo '<option value ="'.$row['drugsub_id'] .'"'. $sel.'>'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {


        }
    }

	/**
     *
     * @param string $curLnag_field
     * @param string $langtb
     */
function getsubdrugcat($curLnag_field,$langtb) {
        try {
		    $getLang= new language();
            $sql = 'select drugsub_id,langcont_id from drug_subcat';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                echo '<option value ="'.$row['drugsub_id'] .'">'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
               }
        } catch (Exception $e) {


        }
    }

	/**
     * list all rows in the specified table
     * @param string $curLnag_field
     * @param string $langtb
     */

function allrows($curLnag_field,$langtb) {
        
		try {
	 $getlang= new language();
	 $dcat = new drugcategory();
	$db2 = new DBConf();
		 $sql = 'SELECT
*
FROM
drug_subcat
';
$pageindex='drug';
$pager = new PS_Pagination($db2,$sql,200,10,$pageindex);
$rs = $pager->paginate();
 $offset=$pager->offset;         

           // $res = $this->conn->execute($sql);
			$i=1;
           while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    if ($i%2 ==0) {$bgcolor = "tr-row";} else {$bgcolor = "tr-row2";} 
			    echo' <tr class="'.$bgcolor.'">
        <td>'.(++$offset).'.</td>
       
		<td>'.$dcat ->getdrugcat_name($row["drugcat_id"],$curLnag_field,$langtb).'</td> 
		<td><a href = "./index.php?p=editdrugsub&drugsub_id='.$row["drugsub_id"].'&langid='.$row["langcont_id"] .'">'.htmlentities($getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb)).'</a></td>
		      
        
		<td>'.'<a href = "./index.php?p=drug&id='.$row["drugcat_id"].'&langid='.$row["langcont_id"].'">Add Drug Category</a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
        } catch (Exception $e) {
	
        }

  
    }
	/**
     * returns rows that match specified criteria
     * @param string $search_field
     * @param string $search_value
      *@param string $curLnag_field
     * @param string $langtb
     */
	function rowSearch($search_field,$search_value,$curLnag_field,$langtb){
		 try {
		  $getlang= new language();
		 $db2 = new DBConf();
		$sql = "Select * from  drug_subcat  where  ".$search_field."='".$search_value."'" ;
		$res =$this->conn->execute($sql);
		
$pageindex='drug';
$pager = new PS_Pagination($db2,$sql,200,10,$pageindex);
$rs = $pager->paginate();
	  // echo "entered";
	     $i=1;
		while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["drugsub_id"].'" />'.$ii.'</td>
        <td>'.$row["drugsub_id"].'</td>
		<td>'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</td>
		       
        <td>'.'<a href = "./index.php?jj=delete&p=editdrug&id='.$row["drugsub_id"] .'"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td><td>'.'<a href = "./index.php?p=editsubdrug&drugsub_id='.$row["drugsub_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
		} catch (Exception $e) {
        }
	}
	
	 
	
	

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->conn->execute("SELECT drugsub_id from drug_subcat order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["drugsub_id"];
				$i++;
			}
	return $keys;
	}

    /**
     * Close mysql connection
     */
	public function enddrug_subcat(){
		$this->connection->CloseMysql();
	}

}

// ------------------------------------------------------------------------

