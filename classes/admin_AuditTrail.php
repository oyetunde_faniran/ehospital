<?php
/**
 * Handles all Audit Trail functions
 * 
 * @author Faniran, Oyetunde
 * 
 * @package System_Administration
 */

/**
 * Handles all Audit Trail functions
 *
 * @author Faniran, Oyetunde
 *
 * @package System_Administration
 */
class admin_AuditTrail{
    /**
     * @var object An object of the DBConf class
     */
	protected $conn;

    /**
     * @var string Stores the name of the current column from which content would be fetched from the language table
     */
	protected $langField;


/**
 * The constructor of the class
 * 
 * @param string $langField This should contain the name of the current column from which content would be fetched from the language table
 */
	public function __construct($langField){
		$this->conn = new DBConf();
		$this->langField = $langField;
	}


/**
 * Logs every page request
 *
 * @param array $logArray This contains the  data to be logged and it should contain the following elements user (The user ID of the logged-in user
 *      action (0=No form was submitted with the request, 1=Form was submitted)), post (The serialized post array, if a form was submitted),
 *      menuIDs (All the IDs from the menu table whose URL matches the requested page), url (The requested URL)
 *
 * @param tinyint $allowed when = 1, the user is allowed to visit the requested page, else, when = 0, the user is not allowed to visit that page
 */
	public function logVisit($logArray, $allowed){
		/*
		$logArray structure
		Array
		(
			[user] => 12
			[action] => 1
			[post] => 
			[menuIDs] => Array
				(
				)
			[post] => 
			[url] => index.php
		)
		*/
		foreach ($logArray as $key=>$value)
			$$key = $value;
		//INSERT into the main audit trail table
		$query = "INSERT INTO audittrail
					VALUES ('0', '$user', DEFAULT, '$action', '$url', '$post', '$allowed', NOW())";
		$this->conn->execute($query);
		
		//Insert into the menu audit trail table
		if (count($logArray["menuIDs"]) > 0){
			$auditID = mysql_insert_id ($this->conn->getConnectionID());
			$query = "INSERT INTO audittrail_menu (menu_id, audit_id)
						VALUES ";
			foreach ($logArray["menuIDs"] as $v)
				$query .= "('$v', '$auditID'),";
			
			$query = rtrim($query, ",");
			$this->conn->execute($query);
		}
	}   //logVisit()


/**
 * Gets the action ID from the actions table based on the type of request a iser made
 *
 * @param tinyint $action   =1 if a form was submitted (POST), else (GET) 0
 *
 * @return int  The row ID from the action table that corresponds to the kind of request (POST / GET) made by the user
 */
	public function getActionID($action){
		$query = "SELECT action_id FROM action
					WHERE action_type = '$action'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			$retVal = $row["action_id"];
		} else $retVal = 0;
		return $retVal;
	}   //END getActionID()



/**
 * Gets a list of all the users in a particular user group with the name of the usr group shown in an h3 tag and the table concerning the list of users in the group
 * enclosed in a div tag. A check box is also attached to each user for selection purposes. The list is generated such that it can be used in a JQuery accordion.
 *
 * @param array $textArray  This array contains all the list of text (in the current language) that may be needed for the display. It must contain the following elements
 *          sno (Serial Number), staffno (Staff Number), name (Name), nouser (The text to be shown if there is no user in a particular user group), dept (Department)
 *
 * @return string   The generated HTML containing the list of users in each available user group for use in the div tag of a JQuery accordion
 */
	public function getUsersInGroups($textArray){
		/*$query = "SELECT u.group_id, 
						lc." . $this->langField . " groupname,
						u.user_id,
						s.staff_employee_id,
						CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) staffname
					FROM staff s INNER JOIN users u INNER JOIN groups g INNER JOIN language_content lc
					ON s.user_id = u.user_id AND u.group_id = g.group_id AND g.langcont_id = lc.langcont_id
					ORDER BY u.group_id, s.staff_surname, s.staff_othernames, s.staff_title";*/
		$query = "SELECT u.group_id, 
						lc1." . $this->langField . " groupname,
						u.user_id,
						s.staff_employee_id,
						lc2." . $this->langField . " deptname,
						CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) staffname
					FROM staff s 
						INNER JOIN users u 
						INNER JOIN groups g 
						INNER JOIN language_content lc1 
						INNER JOIN department dept 
						INNER JOIN language_content lc2
					ON s.user_id = u.user_id 
						AND u.group_id = g.group_id 
						AND g.langcont_id = lc1.langcont_id 
						AND s.dept_id = dept.dept_id
						AND dept.langcont_id = lc2.langcont_id
					ORDER BY u.group_id, s.staff_surname, s.staff_othernames, s.staff_title";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			$retVal = "";
			$lastGroup = 0;
			$isFirstRow = true;
			$sNo = 0;
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				if ($lastGroup != $row["group_id"]){
					if ($isFirstRow){
						$retVal .= "<h3><a href=\"#\">{$row['groupname']}</a></h3>
									<div>
										<table id=\"{$row['groupname']}\">
											<thead>
												<th>&nbsp;</th>
												<th>{$textArray['sno']}</th>
												<th>{$textArray['staffno']}</th>
												<th>{$textArray['name']}</th>
												<th>{$textArray['dept']}</th>
											</thead>
											<tbody>";
					} else {
							$sNo = 0;
							$retVal .= "  	</tbody>
											</table>
										</div>
										<h3><a href=\"#\">{$row['groupname']}</a></h3>
										<div>
											<table id=\"{$row['groupname']}\">
											<thead>
												<th>&nbsp;</th>
												<th>{$textArray['sno']}</th>
												<th>{$textArray['staffno']}</th>
												<th>{$textArray['name']}</th>
												<th>{$textArray['dept']}</th>
											</thead>
											<tbody>";
						}	//END if (isFirstRow) / else
					$retVal .= "<tr>
									<td><input type=\"checkbox\" name=\"users[]\" value=\"{$row['user_id']}\" /></td>
									<td>" . (++$sNo) . ".</td>
									<td>{$row['staff_employee_id']}</td>
									<td>{$row['staffname']}</td>
									<td>{$row['deptname']}</td>
								</tr>";
				} else {
						$retVal .= "<tr>
										<td><input type=\"checkbox\" name=\"users[]\" value=\"{$row['user_id']}\" /></td>
										<td>" . (++$sNo) . ".</td>
										<td>{$row['staff_employee_id']}</td>
										<td>{$row['staffname']}</td>
										<td>{$row['deptname']}</td>
									</tr>";
					}
				$isFirstRow = false;
				$lastGroup = $row["group_id"];
			}	//END while
			$retVal .= "</tbody>
							</table>
							</div>";
		} else $retVal = $textArray['nouser'];
		return $retVal;
	}	//END getUsersInGroups()



/**
 * Gets the list of all possible page names matching the pages attached to the supplied audit trail ID
 *
 * @param int $auditID  The row ID of an audit trail instance in the audit trail table (audittrail)
 * 
 * @return string   The list of all possible page names with each one enclosed in a div tag.
 */
	protected function getPossiblePageNames($auditID){
		$retVal = "";
		$query = "SELECT lc." . $this->langField . " pagename
					FROM audittrail_menu atm INNER JOIN menu_id m INNER JOIN language_content lc
					ON atm.menu_id = m.menu_id AND m.langcont_id = lc.langcont_id
					WHERE atm.audit_id = '$auditID'
					ORDER BY pagename";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			while ($pageName = mysql_fetch_row($result))
				$retVal .= "<div>$pageName</div>";
		}
		return $retVal;
	}   //END getPossiblePageNames()




/**
 * Gets a list showing the audit trail based on the search criteria selected / entered by a user
 * @param array $post   The post array generated from the audit trail search page
 * @param string $noResult  The text to show if no audit trail record was found for the entered search criteria
 * @return string   The HTML (with pagination) containing the audit trail search result
 */
	public function getAuditTrail($post, $noResult){
		$retVal = "";

		//Escape the submitted data
		$post = admin_Tools::doEscape($post, $this->conn);

		//***START QUERY FORMATION

		//Add a page criteria to the search if selected by the user
		if (isset($post["pages"])){
			$pageLimit = implode(", ", $post["pages"]);
		}
		if (isset($pageLimit)){
			$query = "SELECT CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) AS 'STAFF NAME',
							lc1." . $this->langField . " AS 'DEPARTMENT',
							atr.audit_url AS 'ACTUAL URL',
							CONCAT(
								IFNULL(
											(
												SELECT lc_in." . $this->langField . " FROM audittrail_menu atm_in INNER JOIN menu m_in INNER JOIN language_content lc_in
												  ON atm_in.menu_id = m_in.menu_id AND m_in.langcont_id = lc_in.langcont_id
												  WHERE atm_in.audit_id = atr.audit_id
												  LIMIT 1
											 )
											, 'Unknown'
										),
								' <br /><a target=\"_blank\" href=\"index.php?p=audittrail_allpagenames&m=sysadmin&d=', atr.audit_id, '\">View All</a>'
							) AS 'POSSIBLE PAGE NAME',
							(
								CASE atr.audit_action
									WHEN 1 THEN 'Not Available'
									WHEN 2 THEN CONCAT(' <br /><a target=\"_blank\" href=\"index.php?p=audittrail_post&m=sysadmin&d=', atr.audit_id, '\">View Data</a>')
								END
							) AS 'SUBMITTED DATA',
							atr.audit_date 'DATE / TIME'
						FROM audittrail atr
							INNER JOIN audittrail_menu atm
							INNER JOIN users u
							INNER JOIN staff s
							INNER JOIN department d 
							INNER JOIN language_content lc1 
						ON atr.audit_id = atm.audit_id
							AND atr.user_id = u.user_id
							AND u.staff_employee_id = s.staff_employee_id
							AND s.dept_id = d.dept_id
							AND d.langcont_id = lc1.langcont_id
						WHERE atm.menu_id IN ($pageLimit) AND";
		} else {
				$query = "SELECT CONCAT_WS(' ', s.staff_title, s.staff_surname, s.staff_othernames) AS 'STAFF NAME',
							lc1." . $this->langField . " AS 'DEPARTMENT',
							atr.audit_url AS 'ACTUAL URL',
							CONCAT(
								IFNULL(
											(
												SELECT lc_in." . $this->langField . " FROM audittrail_menu atm_in INNER JOIN menu m_in INNER JOIN language_content lc_in
												  ON atm_in.menu_id = m_in.menu_id AND m_in.langcont_id = lc_in.langcont_id
												  WHERE atm_in.audit_id = atr.audit_id
												  LIMIT 1
											 )
											, 'Unknown'
										),
								' <br /><a target=\"_blank\" href=\"index.php?p=audittrail_allpagenames&m=sysadmin&d=', atr.audit_id, '\">View All</a>'
							) AS 'POSSIBLE PAGE NAME',
							(
								CASE atr.audit_action
									WHEN 1 THEN 'Not Available'
									WHEN 2 THEN CONCAT(' <br /><a target=\"_blank\" href=\"index.php?p=audittrail_post&m=sysadmin&d=', atr.audit_id, '\">View Data</a>')
								END
							) AS 'SUBMITTED DATA',
							atr.audit_date 'DATE / TIME'
						FROM audittrail atr
							INNER JOIN users u
							INNER JOIN staff s
							INNER JOIN department d 
							INNER JOIN language_content lc1 
						ON atr.user_id = u.user_id
							AND u.staff_employee_id = s.staff_employee_id
							AND s.dept_id = d.dept_id
							AND d.langcont_id = lc1.langcont_id
						WHERE ";
			}
		if (isset($post["fromdate"]) && isset($post["todate"])){
			$query .= " (DATE(atr.audit_date) BETWEEN '" . $post["fromdate"] . "' AND '" . $post["todate"] . "' ";
			$query .= " OR DATE(atr.audit_date) BETWEEN '" . $post["todate"] . "' AND '" . $post["fromdate"] . "') ";
		} else {	//If no date interval was chosen, then default to three days ago to now
				$query .= " DATE(atr.audit_date) BETWEEN DATE_ADD(CURDATE(), INTERVAL -3 DAY); AND NOW() ";
			}
		//Add users criteria to the search if selected by the user
		if (isset($post["users"])){
			$users = implode(", ", $post["users"]);
			$query .= " AND atr.user_id IN ($users)";
		}
		
		$query .= " ORDER BY u.user_id, atr.audit_date DESC";
		//***FINISH QUERY FORMATION

//die ("<pre>$query</pre>");
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			$tBuilder = new tableBuilder($query);
			$start = isset($_GET["start"]) ? (int)$_GET["start"] : 0;
			$retVal = $tBuilder->buildTable(true, $start);
		} else $retVal = $noResult;

//		die ("<pre>$query</pre>");
		return $retVal;
	}   //END getAuditTrail()




/**
 * Gets all the page names attached to a particular audit trail instance
 * @param int $auditID      The row ID of the audit trail instance under consideration
 * @param string $story     A brief description of the page
 * @param string $are       Contains the English word "are" in the curretly selected language
 * @param string $noResult  The text to show (in the current language) if no page name was found for the selected audit trail instance
 * @return string   The generated HTML list of all the page names attached to an audit trail instance
 */
	public function getPageNames($auditID, $story, $are, $noResult){
		$query = "SELECT lc_in." . $this->langField . " pagename 
					FROM audittrail_menu atm_in INNER JOIN menu m_in INNER JOIN language_content lc_in
					ON atm_in.menu_id = m_in.menu_id AND m_in.langcont_id = lc_in.langcont_id
					WHERE atm_in.audit_id = '$auditID'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			$retVal = "<ul>";
			while ($page = mysql_fetch_row($result))
				$retVal .= "<li>{$page[0]}</li>";
			$retVal .= "</ul>";
			$retVal = "<h3>$story <em>'" . $this->getURLFromAuditID($auditID) . "'</em> $are</h3>" . $retVal;	//The possible names for the URL _____ are ...
		} else $retVal = $noResult;
		return $retVal;
	}   //END getPageNames()



/**
 * Gets the data that was posted during a logged page request if any
 * @param int $auditID  The audit trail ID of the audit trail instance under consideration
 * @param string $text  An array containing the text (in the current language) that would be shown to the user. Must contain the following elements
 *              fieldName (the text "Name" in the current language), fieldValue (the text "Value" in the current language), story (a brief description of the contents of the page)
 * @return string   The key - value pair of any posted data for the audit trail instance under consideration
 */
	public function getPostedData($auditID, $text){
		$query = "SELECT audit_post FROM audittrail
					WHERE audit_id = '$auditID'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			$page = mysql_fetch_row($result); //die ($page[0]);
			$data = @unserialize($page[0]);
			if (is_array($data)){
				$retVal = "<table>
								<thead>
									<th>{$text['fieldName']}</th>
									<th>{$text['fieldValue']}</th>
								</thead>
								<tbody>";
				/*foreach ($data as $key=>$value)
					$retVal .= "<tr>
									<td><strong>$key</strong></td>
									<td>$value</td>
								</tr>";*/
				$retVal .= $this->getPostedData4Display($data, $text);
				$retVal .= "</tbody>
								</table>";
				$retVal = "<h3>{$text['story']}</h3>" . $retVal;
			} else $retVal = $text["noPost"];
		} else $retVal = $noResult;
		return $retVal;
	}   //END getPostedData()


/**
 * Organises the posted data into HTML table cells for display. It's called from getPostedData()
 * @param array $data   An array containing the posted data
 * @param string $text  The text to be shown to the user as a description of the page
 * @return string       The HTML containing the key - value pait list of posted data
 */
	protected function getPostedData4Display($data, $text){
		$retVal = "";
		foreach ($data as $key=>$value){
			if (!is_array($value)){
				$retVal .= "<tr>
								<td valign=\"top\"><strong>$key</strong></td>
								<td>$value</td>
							</tr>";
			} else {
					$retVal .= "<tr>
									<td valign=\"top\"><strong>$key -></strong></td>
									<td>";
					$retVal = "<table>
								<thead>
									<th>{$text['fieldName']}</th>
									<th>{$text['fieldValue']}</th>
								</thead>
								<tbody>";
					$retVal .= $this->getPostedData4Display($value, $text);
					$retVal .= "</tbody>
								</table>
									
									</td>
								</tr>";
				}
		}
		return $retVal;
	}   //END getPostedData4Display()


/**
 * Gets the URL attached to an audit trail instance
 * @param int $auditID  The audit trail instance ID under consideration
 * @return string   The URL attached to an audit trail instance
 */
	protected function getURLFromAuditID($auditID){
		$query = "SELECT audit_url FROM audittrail
					WHERE audit_id = '$auditID'";
		$result = $this->conn->execute($query);
		$retVal = "";
		if ($this->conn->hasRows($result)){
			$row = mysql_fetch_row($result);
			$retVal = $row[0];
		}
		return $retVal;
	}   //END getURLFromAuditID()

}	//END class
?>