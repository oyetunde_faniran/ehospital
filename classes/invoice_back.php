<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 7-11-2009
 * 
 * Version of MYSQL_to_PHP: 1.0
 * 
 * License: LGPL 
 * 
 */
require_once './classes/DBConf.php';

Class invoice {
 
    public $sname=array() ;
    public $sname2=array() ;
	public $sername_id; //int(10) unsigned
	public $langcont_id; //int(10) unsigned
	    public $conn;
	
	
	 function  __construct() {
     $this->conn = new DBConf();
    }
	
    /**
     * New object to the class. Don�t forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New(); 
     *
     */
	public function New_service_name($langcont_id){
		$this->langcont_id = $langcont_id;
	}

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param key_table_type $key_row
     * 
     */
	public function Load_from_key($key_row){
		$result = $this->conn->execute("Select * from service_name where sername_id = \"$key_row\" ");
		while($row = mysql_fetch_array($result)){
			$this->sername_id = $row["sername_id"];
			$this->langcont_id = $row["langcont_id"];
		}
	}



    public function insertInvoice4service($_POST,$theitemamounts,$theitemids,$theitemnames,$curLnag_field,$langtb){
   
   
      try {
					$servicename= new patient_admission();
					$service= new service();
				   $patadm_id=$_POST['patadm_id'];
				   $department_id=$servicename->getdepartment_id2($_POST['hosp_id'],$curLnag_field,$langtb);
				   $sql_total = "INSERT INTO pat_transtotal SET 
						pattotal_date=now(),
						patadm_id='".$_POST['patadm_id']."',
						reg_hospital_no='".$_POST['hosp_id']."',
						pattotal_transno='".$_POST['transno']."',
						pattotal_totalamt='".$_POST['itemtotal']."',
						pattotal_status='".$_POST['paystatus']."',
						pattotal_invoice_type='".$_POST['pattotal_invoice_type']."',
						pattotal_paymthd='".$_POST['pattotal_paymthd']."',
						dept_id='".$department_id."',
						clinic_id='".$_POST['clinic_id']."',
						user_id='".$_POST['user_id']."'";
				  //echo $sql_total;
				 //
				 // exit();
				   $restotal = $this->conn->execute($sql_total);
				  
				  $thispat_total_id=mysql_insert_id();
				  
				   $patservice_type=$_POST['patservice_type'];
				   
				  // $sql = " UPDATE patinp_account SET 
//		   		
//     			patinpacc_amount='".$_POST['patinpacc_amount']."'
//				WHERE patadm_id='$patadm_id'";
//					
//					  $res = $this->conn->execute($sql);

					$subcount=count($theitemids);
					//print_r($theitemids);
					//exit();
					  for ($no = 1; $no <= $subcount; $no++) {
				
						$sql_patservice = "INSERT INTO pat_serviceitem SET 
						patservice_type='$patservice_type',
						patservice_itemid='$theitemids[$no] ',
						patservice_name='$theitemnames[$no]'
						 ";
					//   echo $sql_patservice ;
				 
						$respservice = $this->conn->execute($sql_patservice);
						$thispat_serviceitem[$no]=mysql_insert_id();
					}
		
					for ($no = 1; $no <= $subcount; $no++) {
						//$theserviceIDs[$no] = $_POST["chkID$no"];
					//$theDiscounts[$no] =$_POST["discount$no"];
					//$thediscountamts[$no] =$_POST["discountamt$no"];
					$theitemamount =$theitemamounts[$no]; 
					//$theitemids =$theitemids[$no];
					$theitemname =$theitemnames[$no]; 
						$sql_pat_transitem = "INSERT INTO pat_transitem SET 
						patitem_amount='$theitemamount ',
						patservice_id='$thispat_serviceitem[$no]',
						pattotal_id='$thispat_total_id'
						 ";
					   //echo $sql_pat_transitem ;
				 
					$resFinal = $this->conn->execute($sql_pat_transitem);
					}
					//exit();
					
				//	exit();
					//for ($no = 1; $no <= $subcount; $no++) {
//					
//					$item_no=$servicename->getItem_no($theitemids[$no],$curLnag_field,$langtb);
//					$sql2 = " UPDATE casenote_labtest SET 
//									
//						casenote_labtest.payflag='".$item_no."'
//						WHERE casenote.patadm_id='$patadm_id' AND casenote_labtest.casenote_id=casenote.casenote_id AND casenote_labtest.payflag=0";
//							echo $sql2;
//							exit();
//					}
			if($resFinal){
				
						  for ($no = 1; $no <= $subcount; $no++) {
							$item_no=$servicename->getItem_no($theitemids[$no],$curLnag_field,$langtb);
							$sql2 = " UPDATE casenote_labtest,casenote SET 
									
						casenote_labtest.payflag=1
							WHERE casenote.patadm_id='$patadm_id' AND 
							casenote_labtest.casenote_id=casenote.casenote_id AND
							  casenote_labtest.sername_id='".$item_no."'
							 
							 ";
							//echo $sql2;
							  $res = $this->conn->execute($sql2);
							}
				
				//exit();
		
			   $list[1]=true;
			   $list[2]=$thispat_total_id;
			   return $list;
			}else			 
				return false;	
				
        } catch (Exception $e) {

   
        }
	}
   
   
public function insertInvoice4drug($_POST,$subcount,$sumoftheamount, $thedrugqty,$thedrug_price,$thedrugid,$thedrug_desc,$curLnag_field,$langtb){
   
   
      try {
	  
	  
	       // echo "enter";
					$servicename= new service();
				   $patadm_id=$_POST['patadm_id'];
				   admin_Tools::doEscape($_POST,$this->conn);
				   $sql_total = "INSERT INTO pat_transtotal SET 
						pattotal_date=now(),
						patadm_id='".$_POST['patadm_id']."',
						reg_hospital_no='".$_POST['hosp_id']."',
						pattotal_transno='".$_POST['transno']."',
						pattotal_totalamt='".$sumoftheamount."',
						pattotal_status='".$_POST['paystatus']."',
						pattotal_invoice_type='".$_POST['pattotal_invoice_type']."',
						pattotal_paymthd='".$_POST['pattotal_paymthd']."',
						dept_id='".$_POST['deptid']."',
						clinic_id='".$_POST['clinic_id']."',
						user_id='".$_POST['user_id']."'";
			 //echo $sql_total;
				 //
				  //exit();
				   $restotal = $this->conn->execute($sql_total);
				  
				  $thispat_total_id=mysql_insert_id();
				  
				   $patservice_type=$_POST['patservice_type'];
				   
				  // $sql = " UPDATE patinp_account SET 
//		   		
//     			patinpacc_amount='".$_POST['patinpacc_amount']."'
//				WHERE patadm_id='$patadm_id'";
//					
//					  $res = $this->conn->execute($sql);

					$subcount=count($thedrugid);
					//print_r($thedrugid);
					//exit();
					  for ($no = 1; $no < $subcount; $no++) {
				
						$sql_patservice = "INSERT INTO pat_serviceitem SET 
						patservice_type='$patservice_type',
						patservice_itemid='$thedrugid[$no] ',
						patservice_name='$thedrug_desc[$no]'
						 ";
				//	  echo $sql_patservice ;
				
						$respservice = $this->conn->execute($sql_patservice);
						$thispat_serviceitem[$no]=mysql_insert_id();
					}
		//exit();
					for ($no = 1; $no < $subcount; $no++) {
						//$theserviceIDs[$no] = $_POST["chkID$no"];
					//$theDiscounts[$no] =$_POST["discount$no"];
					//$thediscountamts[$no] =$_POST["discountamt$no"];
					$theitemamount =$thedrug_price[$no]; 
					//$theitemids =$theitemids[$no];
					$theitemname =$thedrug_desc[$no]; 
						$sql_pat_transitem = "INSERT INTO pat_transitem SET 
						patitem_amount='$theitemamount ',
						patitem_totalqty='$thedrugqty[$no]',
						patservice_id='$thispat_serviceitem[$no]',
						pattotal_id='$thispat_total_id'
						 ";
					//   echo $sql_pat_transitem ;
				 
					$resFinal = $this->conn->execute($sql_pat_transitem);
					}
					//exit();
					
				//	exit();
					//for ($no = 1; $no <= $subcount; $no++) {
//					
//					$item_no=$servicename->getItem_no($theitemids[$no],$curLnag_field,$langtb);
//					$sql2 = " UPDATE casenote_labtest SET 
//									
//						casenote_labtest.payflag='".$item_no."'
//						WHERE casenote.patadm_id='$patadm_id' AND casenote_labtest.casenote_id=casenote.casenote_id AND casenote_labtest.payflag=0";
//							echo $sql2;
//							exit();
//					}
//exit();
			if($resFinal){
				 		  for ($no = 1; $no < $subcount; $no++) {
							//$item_no=$servicename->getItem_no($theitemids[$no],$curLnag_field,$langtb);
							$sql2 = " UPDATE casenote_prescription,casenote SET 
									
						casenote_prescription.payflag='1'
							WHERE casenote.patadm_id='$patadm_id' AND 
							casenote_prescription.casenote_id=casenote.casenote_id 
							  ";
						//		echo $sql2;
							  $res = $this->conn->execute($sql2);
							}
				
				
		
			   $list[1]=true;
			   $list[2]=$thispat_total_id;
			   return $list;
			}else{
					//exit();	 
				return false;	
				}
        } catch (Exception $e) {

   
        }
	}


   
   public function insertInvoice($_POST,$subcount, $theDiscounts,$theitemamounts,$theitemids,$theitemnames,$curLnag_field,$langtb){
   
   
      try {
					$servicename= new service();
				   $patadm_id=$_POST['patadm_id'];
				   $sql_total = "INSERT INTO pat_transtotal SET 
						pattotal_date=now(),
						patadm_id='".$_POST['patadm_id']."',
						reg_hospital_no='".$_POST['hosp_id']."',
						pattotal_transno='".$_POST['transno']."',
						pattotal_totalamt='".$_POST['patinpacc_amount']."',
						pattotal_status='".$_POST['paystatus']."',
						pattotal_paymthd='".$_POST['pattotal_paymthd']."',
						dept_id='".$_POST['deptid']."',
						clinic_id='".$_POST['clinic_id']."',
						user_id='".$_POST['user_id']."'";
				 // echo $sql_total;
				 //
				 // exit();
				   $res = $this->conn->execute($sql_total);
				  
				  $thispat_total_id=mysql_insert_id();
				  
				   $patservice_type=$_POST['patservice_type'];
				   
				   $sql = " UPDATE patinp_account SET 
		   		
     			patinpacc_amount='".$_POST['patinpacc_amount']."'
				WHERE patadm_id='$patadm_id'";
					
					  $res = $this->conn->execute($sql);
					
					  for ($no = 1; $no <= $subcount; $no++) {
				
						$sql_patservice = "INSERT INTO pat_serviceitem SET 
						patservice_type='$patservice_type',
						patservice_itemid='$theitemids[$no] ',
						patservice_name='$theitemnames[$no]'
						 ";
					   // echo $sql_patservice ;
				 
						$res = $this->conn->execute($sql_patservice);
						$thispat_serviceitem[$no]=mysql_insert_id();
					}
		
					for ($no = 1; $no <= $subcount; $no++) {
						$theserviceIDs[$no] = $_POST["chkID$no"];
					$theDiscounts[$no] =$_POST["discount$no"];
					$thediscountamts[$no] =$_POST["discountamt$no"];
					$theitemamounts[$no] =$_POST["itemamount$no"]; 
					$theitemids[$no] =$_POST["itemid$no"];
					$theitemnames[$no] =$_POST["itemname$no"]; 
						$sql_pat_transitem = "INSERT INTO pat_transitem SET 
						patitem_discount='$thediscountamts[$no]',
						patitem_discounttype='$theDiscounts[$no] ',
						patitem_amount='$theitemamounts[$no] ',
						patservice_id='$thispat_serviceitem[$no]',
						pattotal_id='$thispat_total_id'
						 ";
					//   echo $sql_pat_transitem ;
				 
					$resFinal = $this->conn->execute($sql_pat_transitem);
					
					
					
					
					}
		
			if($resFinal){
			   $list[1]=true;
			   $list[2]=$thispat_total_id;
			   return $list;
			}else			 
				return false;	
				
        } catch (Exception $e) {

   
        }
	}
   
  // function getdrug_name($id,$curLnag_field,$langtb) {
//        try {
//		  $getLang= new language();
//           $sql = 'Select * from service_name  where sername_id='.$id;
//            $res = $this->conn->execute($sql);
//            $row = mysql_fetch_array($res);
//			//echo $sql ;
//		return	$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
//        //    return $row['drug_desc'];
//				 
//            
//			
//        } catch (Exception $e) {
//
//
//        }
//    }
	
   
	/**
     * Delete the row by using the key as arg
     *
     * @param key_table_type $key_row
     *
     */
	public function Delete_row_from_key($key_row){
		$this->conn->execute("DELETE FROM service_name WHERE sername_id = $key_row");
	}

    /**
     * Update the active row table on table
     */
	 /**
     * Update the active row table on table
     */
public function Save_Active_Row($lang_id,$id,$tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("UPDATE drugcategory set drugcat_name = \"$this->drugcat_name\" where drugcat_id = \"$this->drugcat_id\"");*/
		
		try {
		$q=count($this->sname);
		
		
		for ($i = 0; $i < $q; $i++) {
			if($this->sname[$i]=='langcont_id')
		//	$qq="UPDATE $langtb set $curLnag_field ='".$this->sname2[$i]."' where langcont_id = \"$lang_id\"";
				
		$this->conn->execute("UPDATE $langtb set $curLnag_field ='".$this->sname2[$i]."' where langcont_id = \"$lang_id\"");

		}
		
			
	$sql = "UPDATE $tablename SET ";	
	
				$qq=count($this->sname);
				$q=count($this->sname);

			for ($i = 0; $i < $qq; $i++) {
				//$this->sname[$i].'-'.$this->sname2[$i];
				
	  
	  				if(($q-1)==0){
						if($this->sname[$i]=='langcont_id'){
						     $sql.=    $this->sname[$i] .'="'.$lang_id.'" ';
							 }
						else
         			  		  $sql.=    $this->sname[$i] .'="'.$this->sname2[$i].'" ';
			 
					 }
					 else{
					     if($this->sname[$i]=='langcont_id')
						     $sql.=      $this->sname[$i] .'="'.$lang_id.'" ,';
						 else
						     $sql.=      $this->sname[$i] .'="'.$this->sname2[$i].'" ,';
			
					 }
				
  				 $q--;
 			}
			//echo $sql; 
			//exit();
			$result2 =$this->conn->execute("SELECT * from $tablename");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
		//echo $sql;
	//exit();
         $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		
		
		
		
	}

    /**
     * Save the active var class as a new row on table
     */
	public function Save_Active_Row_as_New($tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("Insert into drugcategory (drugcat_name) values (\"$this->drugcat_name\"");*/
		try {
		$q=count($this->sname);
		
		
		for ($i = 0; $i < $q; $i++) {
			if($this->sname[$i]=='langcont_id'){
			$qq="Insert into $langtb ($curLnag_field) values ('".$this->sname2[$i]."')";
			//echo $qq;
			    $this->conn->execute($qq);
				$content_id= mysql_insert_id();
			}	
		}

		
		$sql = "INSERT INTO $tablename SET ";	
	
				$qq=count($this->sname);
				//$q=count($this->sname);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->sname[$i]=='langcont_id')
						     $sql.=    $this->sname[$i] .'="'.$content_id.'" ';
						else
         			  		  $sql.=    $this->sname[$i] .'="'.$this->sname2[$i].'" ';
			 
					 }
					 else{
					     if($this->sname[$i]=='langcont_id')
						     $sql.=      $this->sname[$i] .'="'.$content_id.'" ,';
						 else
						     $sql.=      $this->sname[$i] .'="'.$this->sname2[$i].'" ,';
			
					 }
  				 $q--;
 			}
		//	$sql.=" ,drugtype_order=".$this->getNext_order();
		
		 $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		
		
		
		
	}
	
	public function GetSearchFields(){
		echo 	"<option value='-1'>Select Field</option>";	
		$result =$this->conn->execute("SELECT * from service_name order by sername_id");
				$i = 0;
				
			while ($i < mysql_num_fields($result)) {
 //   echo "Information for column $i:<br />\n";
	  		  $meta = mysql_fetch_field($result);
	
 		   if (!$meta) {
      //  echo "No information available<br />\n";
  		  }
  //  echo "$meta->name";
  
		if($meta->name=='langcont_id')
	echo 	"<option value=". $meta->name.">".'service name'."</option>";
   else
	echo 	"<option value=". $meta->name.">".$meta->name."</option>";
	   
	   
	   
	   
	    $i++;
}
/*mysql_free_result($result);
	return $keys;*/
	}
	
	function getdrug_name($id,$curLnag_field,$langtb) {
        try {
		  $getLang= new language();
           $sql = 'Select * from service_name  where sername_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
		return	$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
        //    return $row['drug_desc'];
				 
            
			
        } catch (Exception $e) {


        }
    }
	
	function getDiscount($pattotal_id) {
        try {
		   $sql = "Select sum(patitem_discount) as total_discounts from   pat_transitem pitem inner join pat_serviceitem pservice 
		   on
		 
       	   pitem.patservice_id=pservice.patservice_id  
		   where pitem.pattotal_id='$pattotal_id'";
			 $res = $this->conn->execute($sql);
            if($res){
			
			$row = mysql_fetch_array($res);
			
	         return	 $row['total_discounts'];
			
			}	 
            
			
        } catch (Exception $e) {


        }
    }
	function updatePattotal($_POST){
        try {
		$pattotal_id=$_POST['pattotal_id'];
		$hosp_id=$_POST['hosp_id'];
		$patadm_id=$_POST['patadm_id'];
		   $sql2 = " UPDATE pat_transtotal SET 
		   		pattotal_receiptdate='".$_POST['pattotal_receiptdate']."',
		   	    pattotal_receiptno='".$_POST['transno']."',
				pattotal_tellerno='".$_POST['pattotal_tellerno']."',
				pattotal_acctno='".$_POST['pattotal_acctno']."',
				pattotal_status='".$_POST['pay_status']."',
				pattotal_receiptmanual='".$_POST['pattotal_receiptmanual']."'				
				WHERE pattotal_id='$pattotal_id'";
			  
			 
			 //$resUpdate1 = $this->conn->execute($sql2);
			 $resUpdate=mysql_affected_rows();
         //  if($resUpdate1){
			  
			   $sql = " UPDATE patinp_account SET 
		   		
		   	    pattrans_no='".$_POST['transno']."'
				
				WHERE patadm_id='$patadm_id'";
				 $res = $this->conn->execute($sql);
			   $list[1]=true;
			   $list[2]=$pattotal_id;
			   $list[3]=$hosp_id;
			   return $list;
			//}else			 
				return false;	
            
			
        } catch (Exception $e) {


        }
    }
	
			
function getTotalpay($id) {
        try {
		  $getLang= new language();
           $sql = "Select * from pat_transtotal where pattotal_id='$id'";
            $res = $this->conn->execute($sql);
            if($res){
			
			$row = mysql_fetch_array($res);
			
	         return	 $row;
			
			}
			
		      } catch (Exception $e) {


        }
    }
	
	function confirmPay($id) {
        try {
		  $getLang= new language();
           $sql = "Select pattotal_status from pat_transtotal where patadm_id='$id'";
           // echo $sql;
			//exit();
			$res = $this->conn->execute($sql);
            if($res){
			
			$row = mysql_fetch_array($res);
			
	         return	 $row['pattotal_status'];
			
			}
			
		      } catch (Exception $e) {


        }
    }
/*function getlang_content($id,$curLnag_field,$langtb) {
        try {
           $sql = 'Select '.$curLnag_field.'  from '. $langtb.'  where langcont_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			return $row[$curLnag_field];
		} catch (Exception $e) {


        }
    }*/
function getservice_name2($id,$curLnag_field,$langtb) {
        try {
		    $getLang= new language();
			$sql2 = 'select sername_id from service_name  where sername_id ='.$id;
			
				 $res2 = $this->conn->execute($sql2);
				 if( $row2 = mysql_fetch_array($res2))
				  $id2=$row2['sername_id'];
		
            $sql = 'select sername_id,langcont_id from service_name';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
			$sel= ($id2==$row['sername_id'])? "selected":" ";
			
                echo '<option value ="'.$row['sername_id'] .'">'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {


        }
    }

	
function getservice_name($curLnag_field,$langtb) {
        try {
		    $getLang= new language();
            $sql = 'select sername_id,langcont_id from service_name';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                echo '<option value ="'.$row['sername_id'] .'">'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
               }
        } catch (Exception $e) {


        }
    }

	
function allrows($curLnag_field,$langtb) {
        
		try {
	 $getlang= new language();
	$db2 = new DBConf();
		 $sql = 'SELECT
*
FROM
service_name 
';
$pageindex='servicename';
$pager = new PS_Pagination($db2,$sql,10,10,$pageindex);
$rs = $pager->paginate();
           

           // $res = $this->conn->execute($sql);
			$i=1;
           while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["sername_id"].'" /></td>
       
		<td>'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</td>
		    
        <td>'.'<a href = "./index.php?jj=delete&p=servicename&id='.$row["sername_id"].'&langid='.$row["langcont_id"].'"></a></td><td>'.'<a href = "./index.php?p=editservicename&sername_id='.$row["sername_id"].'&langid='.$row["langcont_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
        } catch (Exception $e) {
	
        }

  
    }
	function hasinvoiced($patadm_id){
        try {
		   $sql = "SELECT  patinpacc_amount FROM  patinp_account WHERE
					     patadm_id='$patadm_id' AND patinpacc_amount <=0";
			$res = $this->conn->execute($sql);
				
				
			if($this->conn->hasRows($res)){
            $row = mysql_fetch_array($res);
			//$cnt=mysql_num_rows($res);
//			
//			$patinpacc_amount=$row['patinpacc_amount'];
//			if($patinpacc_amount=="")
		return true;
			}else
			return false;
			
				
        } catch (Exception $e) {
        }
    }
	function hasinvoicedservice($patadm_id){
        try {
		    	   
		   $sql = "SELECT * FROM casenote_labtest inner join casenote  
									ON 
						casenote_labtest.casenote_id=casenote.casenote_id
							WHERE casenote.patadm_id='$patadm_id'  
							 AND casenote_labtest.payflag='0'";
			$res = $this->conn->execute($sql);
				
				//echo $sql;
				//exit();
			if($this->conn->hasRows($res)){
            $row = mysql_fetch_array($res);
			//$cnt=mysql_num_rows($res);
//			
//			$patinpacc_amount=$row['patinpacc_amount'];
//			if($patinpacc_amount=="")
		return true;
			}else
			return false;
			
				
        } catch (Exception $e) {
        }
    }
	
	function haspaid($patadm_id){
        try {
		   $sql = "SELECT  pattrans_no FROM  patinp_account WHERE
					     patadm_id='$patadm_id'";
			$res = $this->conn->execute($sql);
				if($res){
            $row = mysql_fetch_array($res);
		$cnt=mysql_num_rows($res);
			$pattrans_no=$row['pattrans_no'];
		if(empty($pattrans_no))
			return false;
			else
			return true;
			
				}
        } catch (Exception $e) {
        }
    }
	
	function rowSearch($search_field,$search_value,$curLnag_field,$langtb){
		 try {
		  $getlang= new language();
		 $db2 = new DBConf();
		$sql = "Select * from  service_name where  ".$search_field."='".$search_value."'" ;
		$res =$this->conn->execute($sql);
		
$pageindex='servicename';
$pager = new PS_Pagination($db2,$sql,2,10,$pageindex);
$rs = $pager->paginate();
	  // echo "entered";
	     $i=1;
		while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["drugcat_id"].'" />'.$ii.'</td>
        <td>'.$row["sername_id"].'</td>
		<td>'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</td>
		       
        <td>'.'<a href = "./index.php?jj=delete&p=servicename&id='.$row["sername_id"] .'"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td><td>'.'<a href = "./index.php?p=editservicename&sername_id='.$row["sername_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
		} catch (Exception $e) {
        }
	}
	

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->conn->execute("SELECT sername_id from service_name order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["sername_id"];
				$i++;
			}
	return $keys;
	}

    /**
     * Close mysql connection
     */
	public function endservice_name(){
		$this->connection->CloseMysql();
	}

}