﻿<?php
	
	// PHP5 Implementation - uses MySQLi.
	// mysqli('localhost', 'yourUsername', 'yourPassword', 'yourDatabase');
    include_once ("DBConf.php");
    $conn = new DBConf();
    $db = new mysqli($conn->getDBHost(), $conn->getUsername(), $conn->getPassword(), $conn->getDBName());
	
	if(!$db) {
		// Show error if we cannot connect.
		echo 'ERROR: Could not connect to the database.';
	} else {
		// Is there a posted query string?
		if(isset($_POST['queryString'])) {
			$queryString = $db->real_escape_string($_POST['queryString']);
			
			// Is the string length greater than 0?
			
			if(strlen($queryString) >0) {
				// Run the query: We use LIKE '$queryString%'
				// The percentage sign is a wild-card, in my example of countries it works like this...
				// $queryString = 'Uni';
				// Returned data = 'United States, United Kindom';
				
				// YOU NEED TO ALTER THE QUERY TO MATCH YOUR DATABASE.
				// eg: SELECT yourColumnName FROM yourTable WHERE yourColumnName LIKE '$queryString%' LIMIT 10
				
				//$query = $db->query("SELECT CONCAT_WS(' ', drug_desc, drug_unitpack) as drug_desc FROM drug WHERE drug_desc LIKE '$queryString%' LIMIT 7");
                //$query = $db->query("SELECT drug_desc FROM drug WHERE drug_desc LIKE '$queryString%' LIMIT 7");
                $disSQL = "SELECT inventory_medical_name 'generic_name' FROM inventory_medical_names WHERE inventory_medical_name LIKE '$queryString%' LIMIT 7";
				$query = $db->query($disSQL);
				if($query) {
					// While there are results loop through them - fetching an Object (i like PHP5 btw!).
					while ($result = $query ->fetch_object()) {
					
						// Format the results, im using <li> for the list, you can change it.
						// The onClick function fills the textbox with the result.
						
						// YOU MUST CHANGE: $result->value to $result->your_colum
						 $drugname = str_replace("\n","", $result->generic_name);
						 $drugname = str_replace("\r","", $drugname); 
						 $drugname = str_replace("\t","", $drugname); 
						 $drugname = trim($drugname);
						 //$drugname = addslashes($drugname);
	         			echo '<li onClick="fill(\''.$drugname.'\');">'.$drugname.'</li>';
	         		}
				} else {
					echo 'ERROR: There was a problem with the query.';
				}
			} else {
				// Dont do anything.
			} // There is a queryString.
		} else {
			echo 'There should be no direct access to this script!';
		}
	}
?>