<?php
/**
 * Handles "In-Patient Unit-by-Unit Analysis" report
 * 
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */

/**
 * Handles "In-Patient Unit-by-Unit Analysis" report
 *
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */
class report_monthlystats_inpatunitbyunit{
    /**
     * @var object Connection object
     */
	public $conn;

    /**
     * @var object report_common class object
     */
	protected $common;

    /**
     * @var int The number of the month for which the report would be generated
     */
	protected $month;

    /**
     * @var int The year for which the report would be generated
     */
	protected $year;


/**
 * Class constructor
 *
 * @param int $month    The number of the month for which the report would be generated (01 - 12)
 * @param int $year     The year for which the report would be generated
 */
	public function __construct($month, $year){
		$this->conn = new DBConf();
		$this->common = new report_common();
		$this->month = $month;
		$this->year = $year;
		$this->days = $this->common->getDaysInMonth($year, $month);
		$this->periodStart = "$year-$month-01 00:00:00";
		$this->periodEnd = "$year-$month-" . $this->days . " 23:59:59";
	}   //END __construct()



/**
 * Forms an array of values from the result set of running the query stored in $query
 *
 * @param string $query     The query to be executed to form the array
 * @return array            Returns the generated array with the following elements "clinic_id" => "patients"
 */
	protected function getArray($query){
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			//Store all in an array
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$newArray[$row["clinic_id"]] = $row["patients"];
		} else $newArray = array ();
		return $newArray;
	}   //END getArray()
	


/**
 * Gets the list of old patients per clinic
 * @param int $sex  0 = Male, 1 = Female
 * @return array    An array containing the clinic IDs against the number of old patients in each
 */
	protected function getOldPats($sex){
		$period = "'". $this->year . "-" . $this->month . "-01 00:00:00'";
		$query = "SELECT COUNT(tin.patadm_id) patients, tin.clinic_id
					FROM temp_inperiod tin INNER JOIN temp_notinperiod tnot INNER JOIN patient_admission pat INNER JOIN registry reg
					ON tin.patadm_id = tnot.patadm_id
						AND tin.clinic_id = tnot.clinic_id
						AND tin.patadm_id = pat.patadm_id
						AND pat.reg_hospital_no = reg.reg_hospital_no
					WHERE reg.reg_gender = '$sex'
					GROUP BY clinic_id";//die ("<pre>$query</pre>");
		return $this->getArray($query);
	}   //END getOldPats()



/**
 * Gets the total number of patients per clinic
 * @param int $sex  0 = Male, 1 = Female
 * @return array    An array containing the clinic IDs against the number of total patients in each
 */
	protected function getTotalPats($sex){
		$query = "SELECT DISTINCT COUNT(inp.patadm_id) patients, c.clinic_id 
					FROM inpatient_admission inp INNER JOIN wards w INNER JOIN clinic c INNER JOIN patient_admission pat INNER JOIN registry reg
					ON inp.ward_id = w.ward_id
						AND w.clinic_id = c.clinic_id
						AND inp.patadm_id = pat.patadm_id 
						AND pat.reg_hospital_no = reg.reg_hospital_no
					WHERE inp.inp_dateattended BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
						AND reg.reg_gender = '$sex'
					GROUP BY clinic_id";//die ("<pre>$query</pre>");
		return $this->getArray($query);
	}   //END getTotalPats()




/**
 * Generates the actual report to be displayed to the user
 * 
 * @param string $clinicQuery       The query that would be used to generate the list of all clinic and their IDs
 * @param int $dept                 The ID of the department under consideration
 * @param string $totalInWords      The string "Total" in the currently selected language
 * @param string $gTotalInWords     The string "Grand Total" in the currently selected language
 * @return string                   The generated HTML for the report
 */
	public function getResults($clinicQuery, $dept, $totalInWords, $gTotalInWords){
		//Get the list of all the depts available in the hospital
		$result = $this->conn->execute ($clinicQuery);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			//Store all in an array
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$clinicArray[$row["clinic_id"]] = $row["clinics"];
		} else $clinicArray = array();

		//Drop temporary tables & indexes created if existing
		$query = "DROP TABLE IF EXISTS temp_notinperiod";
		$this->conn->execute ($query);
		$query = "DROP TABLE IF EXISTS temp_inperiod";
		$this->conn->execute ($query);

		//Create a temporary table and store a distinct list of all the transactions not in the month under consideration
		$query = "CREATE TEMPORARY TABLE temp_notinperiod
					SELECT DISTINCT inp.patadm_id, c.clinic_id 
					FROM inpatient_admission inp INNER JOIN wards w INNER JOIN clinic c
					ON inp.ward_id = w.ward_id AND w.clinic_id = c.clinic_id
					WHERE inp.inp_dateattended < '" . $this->periodStart . "' AND c.dept_id = '$dept'";//die ("<pre>$query</pre>");
		$this->conn->execute ($query);

		//Create a temporary table and store a distinct list of all the transactions in the month under consideration
		$query = "CREATE TEMPORARY TABLE temp_inperiod
					SELECT DISTINCT inp.patadm_id, c.clinic_id 
					FROM inpatient_admission inp INNER JOIN wards w INNER JOIN clinic c
					ON inp.ward_id = w.ward_id AND w.clinic_id = c.clinic_id
					WHERE inp.inp_dateattended BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
						AND c.dept_id = '$dept'";
		$this->conn->execute ($query);

		//Create indexes on the 2 temp tables above
		$query = "CREATE INDEX patadm_id ON temp_notinperiod (patadm_id)";
		$this->conn->execute ($query);
		$query = "CREATE INDEX patadm_id ON temp_inperiod (patadm_id)";
		$this->conn->execute ($query);

		//Get a list of old patients & total attendances into an array
		//PARAMS: 0 for male, 1 for female
		$oldFArray = $this->getOldPats(1);
		$oldMArray = $this->getOldPats(0);
		$totalFArray = $this->getTotalPats(1);
		$totalMArray = $this->getTotalPats(0);

		//Drop the temporary tables created
		$query = "DROP TABLE temp_inperiod";
		$this->conn->execute ($query);
		$query = "DROP TABLE temp_notinperiod";
		$this->conn->execute ($query);

		//Init needed vars
		$counter = 0;
		$grandTotal = 0;
		$newMTotal = 0;
		$newFTotal = 0;
		$oldMTotal = 0;
		$oldFTotal = 0;
		$totalMTotal = 0;
		$totalFTotal = 0;
		$sessionsTotal = 0;
		$result = "";
		$juggleRows = false;

		foreach ($clinicArray as $deptid=>$dept){
			//Get the figures to be shown
			$counter++;
			$totalMale = array_key_exists($deptid, $totalMArray) ? $totalMArray[$deptid] : 0;
			$totalFemale = array_key_exists($deptid, $totalFArray) ? $totalFArray[$deptid] : 0;
			$oldMale = array_key_exists($deptid, $oldMArray) ? $oldMArray[$deptid] : 0;
			$oldFemale = array_key_exists($deptid, $oldFArray) ? $oldFArray[$deptid] : 0;
			$newMale = $totalMale - $oldMale;
			$newFemale = $totalFemale - $oldFemale;
			$sessions = $totalMale + $totalFemale;

			//Calculate the total for each category
			$newMTotal += $newMale;
			$newFTotal += $newFemale;
			$oldMTotal += $oldMale;
			$oldFTotal += $oldFemale;
			$totalMTotal += $totalMale;
			$totalFTotal += $totalFemale;
			$sessionsTotal += $sessions;
			$rowClass = $juggleRows ? " class=\"tr-row2\"" : " class=\"tr-row\"";
			$juggleRows = !$juggleRows;

			$result .= "<tr $rowClass>\n
							<th align=\"left\">$counter.</th>\n
							<th align=\"left\">$dept</th>\n
							<td align=\"right\">$newMale</td>\n
							<td align=\"right\">$newFemale</td>\n
							<td align=\"right\">$oldMale</td>\n
							<td align=\"right\">$oldFemale</td>\n
							<td align=\"right\">$totalMale</td>\n
							<td align=\"right\">$totalFemale</td>\n
							<td align=\"right\">$sessions</td>\n
						</tr>\n";
		}	//END foreach

		if (!empty($result)){
			//Add the row containing totals to the result
			$result .= "<tr>\n
							<td>&nbsp;</td>\n
							<th align=\"left\">" . strtoupper($totalInWords) . "</th>\n
							<th align=\"right\">$newMTotal</th>\n
							<th align=\"right\">$newFTotal</th>\n
							<th align=\"right\">$oldMTotal</th>\n
							<th align=\"right\">$oldFTotal</th>\n
							<th align=\"right\">$totalMTotal</th>\n
							<th align=\"right\">$totalFTotal</th>\n
							<th align=\"center\" rowspan=\"2\"><h1>$sessionsTotal</h1></th>\n
						</tr>\n";

			//Grand total
			$newTotal = $newMTotal + $newFTotal;
			$oldTotal = $oldMTotal + $oldFTotal;
			$totalTotal = $totalMTotal + $totalFTotal;
			$result .= "<tr>\n
							<td>&nbsp;</td>\n
							<td align=\"left\"><h2>" . strtoupper($gTotalInWords) . "</h2></td>\n
							<td align=\"right\" colspan=\"2\"><h2 align=\"center\">$newTotal</h2></td>\n
							<td align=\"right\" colspan=\"2\"><h2 align=\"center\">$oldTotal</h2></td>\n
							<td align=\"right\" colspan=\"2\"><h2 align=\"center\">$totalTotal</h2></td>\n
						</tr>\n";
		}

		return $result;

	}	//END getResults()

}   //END class
?>