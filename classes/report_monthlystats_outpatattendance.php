<?php
/**
 * Handles "Attendances at Out-Patient Clinic (Consultant)" report
 * 
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */

/**
 * Handles "Attendances at Out-Patient Clinic (Consultant)" report
 *
 * @author Faniran, Oyetunde
 *
 * @package Reports
 */
class report_monthlystats_outpatattendance{
    /**
     * @var object Connection object
     */
	public $conn;

    /**
     * @var object report_common class object
     */
	protected $common;

    /**
     * @var int The number of the month for which the report would be generated
     */
	protected $month;

    /**
     * @var int The year for which the report would be generated
     */
	protected $year;


/**
 * Class constructor
 *
 * @param int $month    The number of the month for which the report would be generated (01 - 12)
 * @param int $year     The year for which the report would be generated
 */
	public function __construct($month, $year){
		$this->conn = new DBConf();
		$this->common = new report_common();
		$this->month = $month;
		$this->year = $year;
		$this->days = $this->common->getDaysInMonth($year, $month);
		$this->periodStart = "$year-$month-01 00:00:00";
		$this->periodEnd = "$year-$month-" . $this->days . " 23:59:59";
	}   //END __construct()




/**
 * Forms an array of values from the result set of running the query stored in $query
 *
 * @param string $query     The query to be executed to form the array
 * @return array            Returns the generated array with the following elements "dept_id" => "patients"
 */
	protected function getArray($query){
		$result = $this->conn->execute($query);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			//Store all in an array
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$newArray[$row["dept_id"]] = $row["patients"];
		} else $newArray = array ();
		return $newArray;
	}   //END getArray()
	



/**
 * Gets the number of old patients per department
 * @param int $sex  0 = Male, 1 = Female
 * @return array    Returns the generated array with the following elements "dept_id" => "patients"
 */
	protected function getOldPats($sex){
		$period = "'". $this->year . "-" . $this->month . "-01 00:00:00'";
		$query = "SELECT COUNT(tin.patadm_id) patients, tin.dept_id
					FROM temp_inperiod tin INNER JOIN temp_notinperiod tnot INNER JOIN patient_admission pat INNER JOIN registry reg
					ON tin.patadm_id = tnot.patadm_id
						AND tin.dept_id = tnot.dept_id
						AND tin.patadm_id = pat.patadm_id
						AND pat.reg_hospital_no = reg.reg_hospital_no
					WHERE reg.reg_gender = '$sex'
						AND tin.app_status = '1'
					GROUP BY dept_id";
		return $this->getArray($query);
	}   //END getOldPats()



/**
 * Gets the total number of patients per department
 * @param int $sex  0 = Male, 1 = Female
 * @return array    Returns the generated array with the following elements "dept_id" => "patients"
 */
	protected function getTotalPats($sex){
		$query = "SELECT DISTINCT COUNT(app.patadm_id) patients, dept_id 
					FROM appointment app INNER JOIN patient_admission pat INNER JOIN registry reg
					ON app.patadm_id = pat.patadm_id 
						AND pat.reg_hospital_no = reg.reg_hospital_no
					WHERE app.app_starttime BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "' 
						AND reg.reg_gender = '$sex'
						AND app.app_status = '1'
					GROUP BY dept_id";
		return $this->getArray($query);
	}   //END getTotalPats()





/**
 * Handles the actual report generation by calling all the required methods and generating the HTML to be displayed to the user
 * @param string $deptQuery         The query that would be used to get the list of all departments alongside their IDs
 * @param string $report_total      The string "Total" in the currently selected language
 * @param string $report_gTotal     The string "Grand Total" in the currently selected language
 * @return string                   The generated HTML for the report
 */
	public function getResults($deptQuery, $report_total, $report_gTotal){
		//Get the list of all the depts available in the hospital
		$result = $this->conn->execute ($deptQuery);
		if ($result && mysql_affected_rows($this->conn->getConnectionID()) > 0){
			//Store all in an array
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$deptArray[$row["dept_id"]] = $row["dept"];
		} else $deptArray = array();

		//Drop temporary tables & indexes created if existing
		$query = "DROP TABLE IF EXISTS temp_notinperiod";
		$this->conn->execute ($query);
		$query = "DROP TABLE IF EXISTS temp_inperiod";
		$this->conn->execute ($query);

		//Create a temporary table and store a distinct list of all the transactions not in the month under consideration
		$query = "CREATE TEMPORARY TABLE temp_notinperiod
					SELECT DISTINCT patadm_id, dept_id FROM appointment
					WHERE app_starttime < '" . $this->periodStart . "'
						AND app_status = '1'";
		$this->conn->execute ($query);

		//Create a temporary table and store a distinct list of all the transactions in the month under consideration
		$query = "CREATE TEMPORARY TABLE temp_inperiod
					SELECT patadm_id, dept_id, app_status FROM appointment
					WHERE app_starttime BETWEEN '" . $this->periodStart . "' AND '" . $this->periodEnd . "'
						AND app_status = '1'";
		$this->conn->execute ($query);

		//Create indexes on the 2 temp tables above
		$query = "CREATE INDEX patadm_id ON temp_notinperiod (patadm_id)";
		$this->conn->execute ($query);
		$query = "CREATE INDEX patadm_id ON temp_inperiod (patadm_id)";
		$this->conn->execute ($query);

		//Get a list of old patients & total attendances into an array (***All vars with the "new" in then should actually be "old")
		//PARAMS: 0 for male, 1 for female
		$oldFArray = $this->getOldPats(1);
		$oldMArray = $this->getOldPats(0);
		$totalFArray = $this->getTotalPats(1);
		$totalMArray = $this->getTotalPats(0);

		//Drop the temporary tables created
		$query = "DROP TABLE temp_inperiod";
		$this->conn->execute ($query);
		$query = "DROP TABLE temp_notinperiod";
		$this->conn->execute ($query);

		//Init needed vars
		$counter = 0;
		$grandTotal = 0;
		$newMTotal = 0;
		$newFTotal = 0;
		$oldMTotal = 0;
		$oldFTotal = 0;
		$totalMTotal = 0;
		$totalFTotal = 0;
		$sessionsTotal = 0;
		$result = "";
		$rowsArray = $colsArray = array();
		$juggleRows = false;
		
		foreach ($deptArray as $deptid=>$dept){
			//Get the figures to be shown
			$counter++;
			$totalMale = array_key_exists($deptid, $totalMArray) ? $totalMArray[$deptid] : 0;
			$totalFemale = array_key_exists($deptid, $totalFArray) ? $totalFArray[$deptid] : 0;
			$oldMale = array_key_exists($deptid, $oldMArray) ? $oldMArray[$deptid] : 0;
			$oldFemale = array_key_exists($deptid, $oldFArray) ? $oldFArray[$deptid] : 0;
			$newMale = $totalMale - $oldMale;
			$newFemale = $totalFemale - $oldFemale;
			$sessions = $totalMale + $totalFemale;
			$rowClass = $juggleRows ? " class=\"tr-row\"" : " class=\"tr-row2\"";
			$juggleRows = !$juggleRows;

			//Calculate the total for each category
			$newMTotal += $newMale;
			$newFTotal += $newFemale;
			$oldMTotal += $oldMale;
			$oldFTotal += $oldFemale;
			$totalMTotal += $totalMale;
			$totalFTotal += $totalFemale;
			$sessionsTotal += $sessions;

			$result .= "<tr $rowClass>\n
							<th align=\"left\">$counter.</th>\n
							<th align=\"left\">$dept</th>\n
							<td align=\"right\">$newMale</td>\n
							<td align=\"right\">$newFemale</td>\n
							<td align=\"right\">$oldMale</td>\n
							<td align=\"right\">$oldFemale</td>\n
							<td align=\"right\">$totalMale</td>\n
							<td align=\"right\">$totalFemale</td>\n
							<td align=\"right\">$sessions</td>\n
						</tr>\n";
			$rowsArray[$counter] = array ($counter. ".", $dept, $newMale, $newFemale, $oldMale, $oldFemale, $totalMale, $totalFemale, $sessions);
		}	//END foreach

		if (!empty($result)){
			//Add the row containing totals to the result
			$result .= "<tr>\n
							<td>&nbsp;</td>\n
							<th align=\"left\">$report_total</th>\n
							<th align=\"right\">$newMTotal</th>\n
							<th align=\"right\">$newFTotal</th>\n
							<th align=\"right\">$oldMTotal</th>\n
							<th align=\"right\">$oldFTotal</th>\n
							<th align=\"right\">$totalMTotal</th>\n
							<th align=\"right\">$totalFTotal</th>\n
							<th align=\"center\" rowspan=\"2\"><h1>$sessionsTotal</h1></th>\n
						</tr>\n";
			$rowsArray[$counter + 1] = array ("", $dept, $newMale, $newFemale, $oldMale, $oldFemale, $totalMale, $totalFemale, $sessions);

			//Grand total
			$newTotal = $newMTotal + $newFTotal;
			$oldTotal = $oldMTotal + $oldFTotal;
			$totalTotal = $totalMTotal + $totalFTotal;
			$result .= "<tr>\n
							<td>&nbsp;</td>\n
							<td align=\"left\"><h2>$report_gTotal</h2></td>\n
							<td align=\"right\" colspan=\"2\"><h2 align=\"center\">$newTotal</h2></td>\n
							<td align=\"right\" colspan=\"2\"><h2 align=\"center\">$oldTotal</h2></td>\n
							<td align=\"right\" colspan=\"2\"><h2 align=\"center\">$totalTotal</h2></td>\n
						</tr>\n";
			$rowsArray[$counter + 2] = array ("", $report_gTotal, "", $newTotal, "", $oldTotal, "", $totalTotal);
		}

		return  array($result, $rowsArray);

	}	//END getResults()

}
?>