<?php
/**
 * Handles the registration of patients
 * 
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */

/**
 * Handles the registration of patients
 *
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */
class Registration {

    /**
     * @var array   Stores part of the $_POST array that corresponds to form elements with the name reg[]
     */
    public $reg  = array();

    /**
     * @var string  Used to temporarily store a patient's passport
     */
    public $temp;

    /**
     * @var string  Used to store the new name / location of a patient's passport
     */
    public $dest;
//private $randStringLength;



/**
 * Class Constructor
 */
    function  __construct() {
        $this->conn = new DBConf();
    }   //END __construct()
	




/**
 * Handles the actual registration of patients
 */
    function registerPatient() {
        try {
			 $newname = $this->randomString(12);  
			if (function_exists("imagegif")) {
				$newname = $newname.".gif";
			} elseif (function_exists("imagejpeg")) {
				$newname = $newname.".jpg";
			} elseif (function_exists("imagepng")) {
				$newname = $newname.".gif";
			}else{
			}		
             move_uploaded_file($_FILES['passport']['tmp_name'], "modules/".$this->temp.$newname);
    		 $passport =  "modules/".$this->temp.$newname; 
			 $thumb = new uploadImage();
			 $thumb -> Thumbsize = 300;
			 $thumb -> Thumblocation = 'modules/'.$this->dest;
			 $thumb -> Thumbprefix = $this->reg['surname'];
			 $thumb -> Createthumb("$passport","file");
			 $newname = $this->reg['surname'].$newname;

	
			
            $query = "INSERT INTO registry
						SET   `reg_surname` = '".$this->reg['surname']."',
							  `reg_othernames` = '".$this->reg['othernames']."',
							  `reg_address` = '".$this->reg['address']."',
							  `reg_dob` = '".$this->reg['dob']."',
							  `reg_gender` = '".$this->reg['gender']."',
							  `reg_phone` = '".$this->reg['phone']."',
							  `reg_email` = '".$this->reg['email']."', 
							  `nationality_id` = '".$this->reg['nationality']."', 
							  `state_id` = '".$this->reg['state']."',
							  `reg_civilstate` = '".$this->reg['civilstate']."',
							  `reg_occupation`  = '".$this->reg['occupation']."',
							  `reg_placeofwork`  = '".$this->reg['placeofwork']."',
							  `reg_religion`  = '".$this->reg['religion']."',
							  `reg_nextofkin`  = '".$this->reg['nextofkin']."',
							  `reg_nokrelationship`  = '".$this->reg['nokrelationship']."',
							  `reg_nokaddress`  = '".$this->reg['nokaddress']."',
							  `reg_nokphone`  = '".$this->reg['nokphone']."',
							  `reg_nokemail`  = '".$this->reg['nokemail']."',
							  `reg_bloodgrp`  = '".$this->reg['bloodgrp']."',
							  `reg_genotype`  = '".$this->reg['genotype']."',
							  `reg_labrefno`  = '".$this->reg['labrefno']."',
							  `reg_rh`  = '".$this->reg['rh']."',
							  `reg_hb`  = '".$this->reg['hb'].
							  "',
							  `reg_condition`  = '".$this->reg['condition']."',
							  `reg_xrayno`  = '".$this->reg['xrayno']."',
							  `reg_status`  = '".$this->reg['status']."',
							  `reg_passport`  = '".$newname.
							  "',
							  `retainership_id`  = '".$this->reg['retainership'].
							  "',
							  `reg_date`  = NOW(),
							  `user_id`  = 1
							  " ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END registerPatient()




/**
 * Generates a drop-down list of all the states of the federation
 */
    function selectState() {
        try {
            $query = "SELECT * FROM state" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
                  echo '<option value='.$row['state_id'].'>'.$row['state_name'].'</option>';
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectState()




/**
 * Generates a drop-down list of all retainers
 */
    function selectRetainer() {
        try {
            $query = "SELECT * FROM retainership" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);

            if ($res) {
			     while ($row = mysql_fetch_array($res)) {
                  echo '<option value='.$row['retainership_id'].'>'.$row['retainership_company'].'</option>';
            		}
            }
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END selectRetainer()



/**
 * Generates a random string of length $randStgringLength
 * @param int $randStringLength     The length of the random string to be generated
 * @return string                   The generated random string
 */
    function randomString($randStringLength){
        $randstring = "";
        try{
            $timestring = microtime();
            $secondsSinceEpoch = (integer) substr($timestring, strrpos($timestring, " "), 100);
            $microseconds = (double) $timestring;
            $seed = mt_rand(0,1000000000) + 10000000 * $microseconds + $secondsSinceEpoch;
            mt_srand($seed);
            for($i=0; $i < $randStringLength; $i++){
                $randstring .= mt_rand(0, 9);
            }
        } catch (Exception $e) {
                set_error_handler (array("Error_handler", "my_error_handler"));
        }
        return($randstring);
    }   //END randomString()

}   //END class

?>
