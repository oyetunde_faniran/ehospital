<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inventory_vendor_class
 *
 * @author upperlink
 */
class inventory_stock_class extends inventoryGeneralClass {

    public function receiveStock_old($itemArray, $locationID, $vendorID, $userID){
        //die ("<pre>" . print_r ($itemArray, true) . "'$locationID', '$vendorID', '$userID'</pre>");
        $conn = new DBConf();
        $locationID = (int)$locationID;
        $vendorID = (int)$vendorID;
        $userID = (int)$userID;
        $itemArray = admin_Tools::doEscape($itemArray, $conn);
        $itemArray = is_array($itemArray) ? $itemArray : array();
        $error = false;
        try {
            $conn->startTransaction();
            $query = "INSERT INTO inventory_received_items
                        SET inventory_location_id = '$locationID',
                            inventory_vendor_id = '$vendorID',
                            user_id = '$userID',
                            time_received = NOW()";
            $result = $conn->run($query);
            if (!$conn->hasRows($result)){
                throw new Exception();
            }
            $restockID = mysql_insert_id($conn->getConnectionID());
            $invUnit = new inventory_units();
            foreach ($itemArray as $itemID => $unitArray){
                $totalQuantity = 0;
                foreach ($unitArray as $unitID => $quantity){
                    $disQuantity = $invUnit->convertToLowestUnit($unitID, $itemID, $quantity);
                    $totalQuantity += $disQuantity;
                }   //END foreach()
                
                if ($totalQuantity > 0){
                    $query = "INSERT INTO inventory_stock_received_details
                                SET stock_received_id = '$restockID',
                                    drug_id = '$itemID',
                                    item_qty = '$totalQuantity'";
                    //echo "<pre>$query</pre>";
                    $result = $conn->run($query);
                    if (!$conn->hasRows($result)){
                        throw new Exception();
                    }

                    $query = "UPDATE inventory_item_qties_by_location
                                SET inventory_item_qty_in_stock = (inventory_item_qty_in_stock + $totalQuantity)
                                WHERE inventory_item_id = '$itemID'
                                    AND inventory_location_id = '$locationID'";
                    //die ("<pre>$query</pre>");
                    $result = $conn->run($query);
                    if (!$conn->hasRows($result)){
                        //throw new Exception();
                    }
                }   //END if quantity > 0
            }   //END foreach()
            
            $conn->commitTransaction();
            
        } catch (Exception $e) {
            $conn->rollBackTransaction();
            $error = true;
        }   //END try..catch
        return $error;
    }   //END receiveStock_old()
    
    
    public function updateReorderLevel($itemId,$qty,$location){
        $id=((int)$itemId);
        global $inventory_dbconnnect_error;
        global $inventory_reorder_level_successfully_updated;
        $reorderlevel_exists=$this->conn->query("select inventory_reorder_level_id from inventory_items_reorder_level where inventory_item_id='".$itemId."' and inventory_location_id='".$location."'");
        if(!$reorderlevel_exists){
            $report=$inventory_dbconnnect_error;
        }else{
            if($reorderlevel_exists->num_rows>0){
            $clearLevel=$this->conn->query("delete from inventory_items_reorder_level where inventory_item_id='".$itemId."' and inventory_location_id='".$location."'");
            if(!$clearLevel){
                $report="System Error! Unable to reset existing reorder level";
            }else{
                $report="Existing reorder level successfully removed";
            }
            
            }
        
            $setLevel=$this->conn->query("insert into inventory_items_reorder_level(
                inventory_item_id,
                inventory_location_id,
                inventory_reorder_level,
                user_id,date_created
                )values (
                '".$itemId."','".$location."','".$qty."','".$_SESSION[session_id() . "userID"]."',CURRENT_TIMESTAMP)");
            if(!$setLevel){
                $report="System Error! Unable to update level at this time. Please retry";
            }else{
              $report="Reorder level successfully updated";
            }
        }
       
        return $report;
    }
    
    public function receiveStock($itemArray, $locationID, $vendorID, $userID, $costPrice, $expiryDate){
        //die ("<pre>" . print_r ($itemArray, true) . "'$locationID', '$vendorID', '$userID', " . print_r ($costPrice, true) . "," . print_r ($expiryDate, true) . "</pre>");
        $conn = new DBConf();
        $locationID = (int)$locationID;
        $vendorID = (int)$vendorID;
        $userID = (int)$userID;
        $itemArray = admin_Tools::doEscape($itemArray, $conn);
        $costPrice = admin_Tools::doEscape($costPrice, $conn);
        $expiryDate = admin_Tools::doEscape($expiryDate, $conn);
        
        $itemArray = is_array($itemArray) ? $itemArray : array();
        $costPrice = is_array($costPrice) ? $costPrice : array();
        $expiryDate = is_array($expiryDate) ? $expiryDate : array();
        $error = false;
        try {
            $conn->startTransaction();
            $query = "INSERT INTO inventory_batch
                        SET inventory_location_id = '$locationID',
                            inventory_vendor_id = '$vendorID',
                            inventory_batch_datetime_received = NOW(),
                            inventory_batch_source = '1',
                            user_id = '$userID'";
            //die ("<pre>$query</pre>");
            $result = $conn->run($query);
            if (!$conn->hasRows($result)){
                throw new Exception();
            }
            $restockID = mysql_insert_id($conn->getConnectionID());
            $invUnit = new inventory_units();
            foreach ($itemArray as $itemID => $unitArray){
                $totalQuantity = 0;
                foreach ($unitArray as $unitID => $quantity){
                    $disQuantity = $invUnit->convertToLowestUnit($unitID, $itemID, $quantity);
                    $totalQuantity += $disQuantity;
                }   //END foreach()
                
                if ($totalQuantity > 0){
                    $disCostPrice = isset($costPrice[$itemID]) ? $costPrice[$itemID] : 0;
                    $disExpiryDate = isset($expiryDate[$itemID]) ? $expiryDate[$itemID] : "";
                    $query = "INSERT INTO inventory_batch_details
                                SET inventory_batch_id = '$restockID',
                                    drug_id = '$itemID',
                                    inventory_batchdetails_received_quantity = '$totalQuantity',
                                    inventory_batchdetails_quantity = '$totalQuantity',
                                    inventory_batchdetails_costprice = '$disCostPrice',
                                    inventory_batchdetails_expirydate = '$disExpiryDate'";
                    //die ("<pre>$query</pre>");
                    $result = $conn->run($query);
                    if (!$conn->hasRows($result)){
                        throw new Exception();
                    }

//                    $query = "UPDATE inventory_item_qties_by_location
//                                SET inventory_item_qty_in_stock = (inventory_item_qty_in_stock + $totalQuantity)
//                                WHERE inventory_item_id = '$itemID'
//                                    AND inventory_location_id = '$locationID'";
//                    //die ("<pre>$query</pre>");
//                    $result = $conn->run($query);
//                    if (!$conn->hasRows($result)){
//                        //throw new Exception();
//                    }
                }   //END if quantity > 0
            }   //END foreach()
            
            $conn->commitTransaction();
            
        } catch (Exception $e) {
            $conn->rollBackTransaction();
            $error = true;
        }   //END try..catch
        return $error;
    }   //END receiveStock()
    
    
 
    public function getAllStockItems_old($locationID = 0){
        $report="";
        $locationID = (int)$locationID;
        global $inventory_dbconnnect_error;
        global $inventory_none_in_existence;
        if ($locationID != 0){
            $query = "select d.*, qty.* from drug d, inventory_item_qties_by_location qty where qty.inventory_item_id=d.drug_id AND qty.inventory_location_id = '$locationID' order by d.drug_name asc";
            $allItemsQuery=$this->conn->query($query);
        } else {
            $query = "select d.*, SUM(qty.inventory_item_qty_in_stock) AS 'inventory_item_qty_in_stock' from drug AS d, inventory_item_qties_by_location AS qty where qty.inventory_item_id=d.drug_id GROUP BY d.drug_id order by d.drug_name asc";
            $allItemsQuery=$this->conn->query($query);
        }
        //die ($query);
        //die ("select * from drug, inventory_item_qties_by_location where inventory_item_qties_by_location.inventory_item_id=drug.drug_id order by drug_name asc");
        if(!$allItemsQuery){
            $report=$inventory_dbconnnect_error;
        }
        if($allItemsQuery->num_rows<1){
            $report=$inventory_none_in_existence;
        }
        $report=$allItemsQuery;
        return $report;
    }   //END getAllStockItems_old()
    
    
    
    
    public function getAllStockItems($locationID = 0, $getTotalOnly = false){
        $report="";
        $locationID = (int)$locationID;
        global $inventory_dbconnnect_error;
        global $inventory_none_in_existence;
        global $limitStart, $recsPerPage;
        if ($locationID != 0){
            if ($getTotalOnly){
                $query = "SELECT COUNT(*) 'totalCount' FROM drug";
            } else {
                $query = "SELECT IFNULL(SUM(ibd.inventory_batchdetails_quantity), 0) 'inventory_item_qty_in_stock', d.*
                            FROM drug d
                                LEFT JOIN inventory_batch_details ibd ON d.drug_id = ibd.drug_id
                                LEFT JOIN inventory_batch ib ON ibd.inventory_batch_id = ib.inventory_batch_id
                            #WHERE d.drug_id = (
                            #                    SELECT DISTINCT iui.inventory_items_id FROM inventory_unit_items iui
                            #                    WHERE iui.inventory_items_id = d.drug_id
                            #                  )
                            #    AND ib.inventory_location_id = '$locationID'
                            WHERE ib.inventory_location_id = '$locationID'
                            GROUP BY d.drug_id
                            LIMIT $limitStart, $recsPerPage";
            }
        } else {
            if ($getTotalOnly){
                $query = "SELECT COUNT(*) 'totalCount' FROM drug";
            } else {
                $query = "SELECT IFNULL(SUM(ibd.inventory_batchdetails_quantity), 0) 'inventory_item_qty_in_stock', d.*
                            FROM drug d LEFT JOIN inventory_batch_details ibd
                            ON d.drug_id = ibd.drug_id
                            #WHERE d.drug_id = ( #Confirm that the drug has units configured
                            #                         SELECT DISTINCT iui.inventory_items_id FROM inventory_unit_items iui
                            #                         WHERE iui.inventory_items_id = d.drug_id
                            #                  )
                            GROUP BY d.drug_id
                            LIMIT $limitStart, $recsPerPage";
            }
        }
        //die ("<pre>$query</pre>");
        $allItemsQuery=$this->conn->query($query);

        if ($getTotalOnly){
            $disCount = $allItemsQuery->fetch_assoc();
            $report = $disCount["totalCount"];
        } else {
            if(!$allItemsQuery){
                $report=$inventory_dbconnnect_error;
            }
            if($allItemsQuery->num_rows<1){
                $report=$inventory_none_in_existence;
            }
            $report=$allItemsQuery;
        }
        return $report;
    }   //END getAllStockItems()
    
    
    
    public function getItemBalStockByLocation($itemId,$locationId){
        global $report;
        global $inventory_dbconnnect_error;
        $itemId = (int)$itemId;
        $locationId = (int)$locationId;
        $sql = "SELECT SUM(bd.inventory_batchdetails_quantity) AS total
                FROM inventory_batch b INNER JOIN inventory_batch_details bd
                USING (inventory_batch_id)
                WHERE bd.drug_id = '$itemId' AND b.inventory_location_id = '$locationId'";
        $query=$this->conn->query($sql);
        //$query=$this->conn->query("select sum(inventory_item_qty_in_stock) as total from inventory_item_qties_by_location where inventory_item_id='".$itemId."' and inventory_location_id='".$locationId."'");
 if(!$query){
     $report=$inventory_dbconnnect_error;
     return $report;
        }
        if($query->num_rows>0){
            $bals=$query->fetch_object();
            return $bals->total;
        }
    }
    
    public function getAllSupplies_old(){
        $report="";
        global $inventory_dbconnnect_error;
        global $inventory_none_in_existence;
        $suppliesQuery=$this->conn->query("select *,DATE_FORMAT(time_received,'%W, %D %b, %Y, %h:%s %p') as supplyDate from inventory_received_items order by time_received desc");
        if(!$suppliesQuery){
            $report=$inventory_dbconnnect_error;
        }
         if($suppliesQuery->num_rows<1){
            $report=$inventory_none_in_existence;
         }
         if($report==""){
             $report=$suppliesQuery;
         }
         return $report;
    }   //END getAllSupplies_old()
    
    
    public function getAllSupplies($locationID){
        $report="";
        global $inventory_dbconnnect_error;
        global $inventory_none_in_existence;
        $locationID = (int)$locationID;
        $query = "SELECT COUNT(bd.inventory_batch_id) 'itemCount', b.*, DATE_FORMAT(b.inventory_batch_datetime_received,'%W, %D %b, %Y, %h:%s %p') AS supplyDate
                    FROM inventory_batch b INNER JOIN inventory_batch_details bd
                    USING (inventory_batch_id)
                    WHERE inventory_location_id = '$locationID'
                        AND inventory_batch_source = '1'
                    GROUP BY (b.inventory_batch_id)
                    ORDER BY inventory_batch_datetime_received DESC";
        $suppliesQuery=$this->conn->query($query);
        //$suppliesQuery=$this->conn->query("select *,DATE_FORMAT(time_received,'%W, %D %b, %Y, %h:%s %p') as supplyDate from inventory_received_items order by time_received desc");
        if(!$suppliesQuery){
            $report=$inventory_dbconnnect_error;
        }
         if($suppliesQuery->num_rows<1){
            $report=$inventory_none_in_existence;
         }
         if($report==""){
             $report=$suppliesQuery;
         }
         return $report;
    }
    
    
    public function getSupplyDetails($supplyId){
        $supplyId =(int)$supplyId;
        $report="";
        global $inventory_dbconnnect_error;
        global $inventory_none_in_existence;
        //$supplydeatilsQuery=$this->conn->query("select * from inventory_stock_received_details where stock_received_id='".$supplyId."'");
        $query = "SELECT * FROM inventory_batch_details
                    WHERE inventory_batch_id = '$supplyId'";
        //die ($query);
        $supplydeatilsQuery=$this->conn->query($query);
        if(!$supplydeatilsQuery){
            $report=$inventory_dbconnnect_error;
        }elseif ($supplydeatilsQuery->num_rows<1){
            $report=$inventory_none_in_existence;
        }else{
        $report=$supplydeatilsQuery;
        return $report;
            }
        }
    }

?>
