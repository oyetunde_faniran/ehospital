<?php
/**
 * Handles all operations that has to do with page menu
 * 
 * @author Faniran, Oyetunde
 *
 * @package System_Administration
 */

/**
 * Handles all operations that has to do with page menu
 *
 * @author Faniran, Oyetunde
 *
 * @package System_Administration
 */
class admin_menu {
	protected $conn;
	protected $langField;
	protected $postArray;
	protected $menuID;
	public $errorMsg;
	public $error;


/**
 * Class Constructor
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param <type> $id            The ID in the menu table of the current menu under consideration. Defaults to zero (0);
 */
	public function __construct($langField, $id = 0){
		$this->conn = new DBConf();
		$this->langField = $langField;
		$this->error = false;
		if (!empty($id))
			$this->menuID = $id;
	}



/**
 * Adds a new menu item
 * @param array $posted             The $_POST array from the menu creation page
 * @param string $allFieldsError    Contains the error message to show if the operation failed
 * @return boolean                  Returns the error status at the end of the operation. true if an error occurred, false otherwise.
 */
	public function addMenuItem($posted, $allFieldsError){
		$msg = "";
		//die ("ready");
		try{
			if (!is_array($posted))
				throw new Exception;
			$posted = admin_Tools::doEscape ($posted, $this->conn);
			$publish = isset($posted["publish"]) ? $posted["publish"] : 0;
			$dropdown = isset($posted["dropdown"]) ? $posted["dropdown"] : 0;

			//Confirm that all compulsory fields were filled
			foreach ($posted as $key=>$value){
				/*if ($key != "module" && empty($value)){
					$this->errorMsg = $allFieldsError;
					throw new Exception;
				} else*/ ${$key . "_luth"} = $value;
			}
			
			//More clean up
			if (empty ($menu_luth)){
				$this->errorMsg = $allFieldsError;
				throw new Exception;
			}
			$module_luth = empty($module_luth) ? 0 : (int)$module_luth;
			$order_luth = empty($order_luth) ? 0 : (int)$order_luth;
			$parentmenu_luth = empty($parentmenu_luth) ? 0 : (int)$parentmenu_luth;
			//$module_luth = $this->getMenuModuleID($module_luth);
			
			//Insert the menu name into the language table
			$query = "INSERT INTO language_content (" . $this->langField . ")
						VALUES ('$menu_luth')";
			$result = $this->conn->execute($query);
			if ($result)
				$langContID = mysql_insert_id ($this->conn->getConnectionID());
			else throw new Exception;

			$query = "INSERT INTO menu
						VALUES ('0', '$module_luth', '$langContID', '$url_luth', '$publish', NOW(), '$order_luth', '$parentmenu_luth', '$dropdown')";
			$result = $this->conn->execute($query);
			if (!$result)
				throw new Exception;

		} catch (Exception $e) {
				//die ("<pre>" . print_r($e, true) . "</pre>");
				$this->error = true;
			}
		return $this->error;
	}	//END method addMenuItem()




/**
 * Gets the ID of the module to which a menu item is attached
 * @param int $menuID   The ID of the menu for which the module is to be fetched
 * @return int          The module ID (from the modules table) of menu item with ID $menuID, 0 if not found
 */
	public function getMenuModuleID($menuID){
		if ($menuID == 0)
			$retVal = 0;
		else {
			$query = "SELECT module_id FROM menu
						WHERE menu_id = '$menuID'";
			$result = $this->conn->execute($query);
			if ($this->conn->hasRows($result, 1)){
				$row = mysql_fetch_array ($result, MYSQL_ASSOC);
				$retVal = $row["module_id"];
			} else $retVal = 0;
		}
		return $retVal;
	}	//END getMenuModuleID()




/**
 * Updates the information about a menu item
 * @param array $posted             The $_POST array generated from the edit menu page, when the form is submitted
 * @param string $allFieldsError    The error message to show if the operation fails
 * @return boolean                  Returns the error status at the end of the operation. true if an error occurred, false otherwise.
 */
	public function editMenuItem($posted, $allFieldsError){
		try{
			if (!is_array($posted) || $this->menuID <= 0)
				throw new Exception;
			$posted = admin_Tools::doEscape ($posted, $this->conn);
			$publish = isset($posted["publish"]) ? $posted["publish"] : 0;
			$dropdown = isset($posted["dropdown"]) ? $posted["dropdown"] : 0;

			//Confirm that all compulsory fields were filled
			foreach ($posted as $key=>$value){
				/*if ($key != "module" && empty($value)){
					$this->errorMsg = $allFieldsError; //die ("throwing exception");
					throw new Exception;
				} else*/ ${$key . "_luth"} = $value;
			}
			
			//More clean up
			$order_luth = empty($order_luth) ? 0 : (int)$order_luth;
			$parent_luth = empty($parent_luth) ? 0 : (int)$parent_luth;
			$module_luth = empty($module_luth) ? 0 : (int)$module_luth;

			//Update the menu name
			$query = "UPDATE language_content
						SET " . $this->langField . " = '$menu_luth'
						WHERE langcont_id = 
							(
								SELECT langcont_id FROM menu
								WHERE menu_id = '" . $this->menuID . "'
							)";
			$this->conn->execute($query);

			//Update other details (the module_id is set to that of the parent)
			$query = "UPDATE menu m1
						SET m1.menu_url = '$url_luth',
							m1.menu_order = '$order_luth',
							m1.menu_publish = '$publish',
							m1.menu_parentid = '$parent_luth',
							m1.menu_dropdown = '$dropdown',
							m1.module_id = '$module_luth',
							m1.menu_date = NOW()
						WHERE menu_id = '" . $this->menuID . "'"; //die ("<pre>$query</pre>");
			$this->conn->execute($query);
		} catch (Exception $e) {
				$this->error = true;
			}
		return $this->error;
	}	//END editMenuItem()


/**
 * Generates the HTML for a tree of all menus (which can be traversed via AJAX). It is used on the menu edit page to navigate / traverse the menu tree for the whole application.
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param int $parent           The ID of the menu item whose children sub-tree would be shown.
 * @param string $noMenu        The message to show if no menu was found under the specified parent
 * @return string               The generated HTML for the menu tree traversal.
 */
	public static function getMenu($langField, $parent, $noMenu){
		$conn = new DBConf();
		$query = "SELECT m.menu_id, lc.$langField menu, m.menu_publish 
					FROM menu m INNER JOIN language_content lc
					ON m.langcont_id = lc.langcont_id
					WHERE m.menu_parentid = '$parent'
					ORDER BY menu_order, menu_date DESC";
		$result = $conn->execute($query);
		if ($conn->hasRows($result)){
			$retVal = "<div id=\"ajaxLoader\"></div>
						<table cellspacing=\"5\" cellpadding=\"5\" border=\"0\" class=\"menuDisplay\">\n";
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$url = "index.php?p=ajax&m=sysadmin&t=menu&d={$row['menu_id']}";
				$editURL = "index.php?p=menumanagement_edit&m=sysadmin&d={$row['menu_id']}";
				$children = self::getChildrenNo($row['menu_id'], $conn);
				$retVal .= "<tr>\n
								<td id=\"td{$row['menu_id']}\">\n
									<span class=\"treeImage\"><img id=\"img{$row['menu_id']}\" title=\"Expand\" onclick=\"processSubs('$url', '{$row['menu_id']}');\" src=\"images/expand.jpg\" border=\"0\" /></span>\n
									<a href=\"$editURL\">{$row['menu']} ($children)</a>\n
									<span class='contstore' id='ispan{$row['menu_id']}'></span>
									<span id='span{$row['menu_id']}'></span>
								</td>\n
							</tr>\n";
			}
			$retVal .= "</table>\n";
		} else $retVal = $noMenu;
		return $retVal;
	}	//END getMenu()



/**
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param int $parent            The ID of the menu item whose children sub-tree would be shown.
 * @param boolean $showParentID  If true, $parent is prepended to the final string to be returned.
 * @return string                The generated HTML for the menu tree traversal.
 */
	public static function getSubMenu($langField, $parent, $showParentID = true){
		$conn = new DBConf();
		$query = "SELECT m.menu_id, lc.$langField menu, m.menu_publish 
					FROM menu m INNER JOIN language_content lc
					ON m.langcont_id = lc.langcont_id
					WHERE m.menu_parentid = '$parent'
					ORDER BY menu_order, menu_date DESC";
		$result = $conn->execute($query);
		if ($conn->hasRows($result)){
			$retVal = $showParentID ? $parent : "";
			$retVal .= "<table cellspacing=\"5\" cellpadding=\"5\" border=\"0\" class=\"menuDisplay\">\n";
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$url = "index.php?p=ajax&m=sysadmin&t=menu&d={$row['menu_id']}";
				$editURL = "index.php?p=menumanagement_edit&m=sysadmin&d={$row['menu_id']}";
				$children = self::getChildrenNo($row['menu_id'], $conn);
				$retVal .= "<tr>\n
								<td id=\"td{$row['menu_id']}\">\n
									<span class=\"treeImage\"><img id=\"img{$row['menu_id']}\" title=\"Expand\" onclick=\"processSubs('$url', '{$row['menu_id']}');\" src=\"images/expand.jpg\" border=\"0\" /></span>\n
									<a href=\"$editURL\">{$row['menu']} ($children)</a>\n
									<span class='contstore' id='ispan{$row['menu_id']}'></span>
									<span id='span{$row['menu_id']}'></span>
								</td>\n
							</tr>\n";
			}
			$retVal .= "</table>\n";
		} else $retVal = "";
		return $retVal;
	}	//END getSubMenu()



/**
 * Generates the HTML for a tree of all menus (which can be traversed via AJAX). It is used on the rights edit page to navigate / traverse the menu tree for the whole application.
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param int $parent           The ID of the menu item whose children sub-tree would be shown.
 * @param int $groupID       The ID of the user group whose list of rights is being edited
 * @param array $text           An array elements containing string literals that would be shown in the generated HTML
 * @return string               Contains the generated HTML
 */
	public static function getMenu4RightsEdit($langField, $parent, $groupID, $text){
		$conn = new DBConf();
		$query = "SELECT m.menu_id, lc.$langField menu, m.menu_publish ,
					(SELECT r.menu_id FROM rights r WHERE m.menu_id = r.menu_id AND r.group_id = '$groupID') added
					FROM menu m INNER JOIN language_content lc
					ON m.langcont_id = lc.langcont_id
					WHERE m.menu_parentid = '$parent'
					ORDER BY menu_order, menu_date DESC"; //die ("<pre>$query</pre>");
		$result = $conn->execute($query);
		if ($conn->hasRows($result)){
			$retVal = "<div id=\"ajaxLoader\"></div>
						<table cellspacing=\"5\" cellpadding=\"5\" border=\"0\" class=\"menuDisplay\">\n";
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$url = "index.php?p=ajax&m=sysadmin&t=rights&d={$row['menu_id']}&d1=$groupID";
				$addURL = "index.php?p=ajax&m=sysadmin&t=rights_add&d={$row['menu_id']}&d1=$groupID";
				$addcURL = "index.php?p=ajax&m=sysadmin&t=rights_addc&d={$row['menu_id']}&d1=$groupID";
				$removeURL = "index.php?p=ajax&m=sysadmin&t=rights_remove&d={$row['menu_id']}&d1=$groupID";
				$children = self::getChildrenNo($row['menu_id'], $conn);
				
				//if the current menu is part of the rights of the current group show 'remove' else 'add'
				if ($row["added"]){
					$retVal .= "<tr id=\"tr{$row['menu_id']}\">\n
									<td id=\"td{$row['menu_id']}\">\n
										<span class=\"treeImage\"><img id=\"img{$row['menu_id']}\" title=\"Expand\" onclick=\"processSubs('$url', '{$row['menu_id']}');\" src=\"images/expand.jpg\" border=\"0\" /></span>\n
										<span class=\"rightsText\">{$row['menu']} ($children)</span> \n
									</td>\n
									<td><span class=\"rightsText\">{$text['added']}</span></td>\n
									<td><span id=\"remover{$row['menu_id']}\" class=\"rightsLinkRemove\" onclick=\"confirmRemove('$removeURL', '{$row['menu_id']}', 'remover" . $row["menu_id"] . "', '" . $text["removing"] . "');\">{$text['remove']}</span></td>\n
								</tr>\n";
				} else {
						$retVal .= "<tr id=\"tr{$row['menu_id']}\">\n
										<td id=\"td{$row['menu_id']}\">\n
											{$row['menu']} ($children) \n
										</td>\n
										<td><span id=\"adder{$row['menu_id']}\" class=\"rightsLink\" onclick=\"processMe('$addURL', '{$row['menu_id']}', 'adder" . $row["menu_id"] . "', '" . $text["adding"] . "');\">{$text['add']}</span></td>\n
										<td><span id=\"cadder{$row['menu_id']}\" class=\"rightsLink\" onclick=\"processMe('$addcURL', '{$row['menu_id']}', 'cadder" . $row["menu_id"] . "', '" . $text["adding"] . "');\">{$text['addc']}</span></td>\n
									</tr>\n";
					}
				$retVal .= "<tr>\n
								<td colspan=\"3\">\n
									<span class='contstore' id='ispan{$row['menu_id']}'></span>\n
									<span id='span{$row['menu_id']}'></span>\n
								</td>\n
							</tr>\n";
			}	//END while row
			$retVal .= "</table>\n";
		} else $retVal = $text["nomenu"];
		return $retVal;
	}	//END getMenu4RightsEdit()



/**
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param int $parent           The ID of the menu item whose children sub-tree would be shown.
 * @param int $groupID          The ID of the user group whose list of rights is being edited
 * @param array $text           An array with elements containing different string literals to be displayed to the user
 * @param boolean $showParentID If true, $parent is prepended to the final string to be returned.
 * @return string               The generated HTML for the menu tree traversal.
 */
	public static function getSubMenu4RightsEdit($langField, $parent, $groupID, $text, $showParentID = true){
		$conn = new DBConf();
		$query = "SELECT m.menu_id, lc.$langField menu, m.menu_publish,
					IFNULL((SELECT r.menu_id FROM rights r WHERE m.menu_id = r.menu_id AND r.group_id = '$groupID'), 0) added
					FROM menu m INNER JOIN language_content lc
					ON m.langcont_id = lc.langcont_id
					WHERE m.menu_parentid = '$parent'
					ORDER BY menu_order, menu_date DESC";
			
			//die ("<pre>$query</pre>");

		$result = $conn->execute($query);
		if ($conn->hasRows($result)){
			$retVal = $showParentID ? $parent : "";
			$retVal .= "<table cellspacing=\"5\" cellpadding=\"5\" border=\"0\" class=\"menuDisplay\">\n";
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$url = "index.php?p=ajax&m=sysadmin&t=rights&d={$row['menu_id']}&d1=$groupID";
				$addURL = "index.php?p=ajax&m=sysadmin&t=rights_add&d={$row['menu_id']}&d1=$groupID";
				$addcURL = "index.php?p=ajax&m=sysadmin&t=rights_addc&d={$row['menu_id']}&d1=$groupID";
				$removeURL = "index.php?p=ajax&m=sysadmin&t=rights_remove&d={$row['menu_id']}&d1=$groupID";
				$children = self::getChildrenNo($row['menu_id'], $conn);
				
				//if the current menu is part of the rights of the current group show 'remove' else 'add'
				if ($row["added"]){
					$retVal .= "<tr id=\"tr{$row['menu_id']}\">\n
									<td id=\"td{$row['menu_id']}\">\n
										<span class=\"treeImage\"><img id=\"img{$row['menu_id']}\" title=\"Expand\" onclick=\"processSubs('$url', '{$row['menu_id']}');\" src=\"images/expand.jpg\" border=\"0\" /></span>\n
										<span class=\"rightsText\">{$row['menu']} ($children)</span> \n
									</td>\n
									<td><span class=\"rightsText\">{$text['added']}</span></td>\n
									<td><span id=\"remover{$row['menu_id']}\" class=\"rightsLinkRemove\" onclick=\"confirmRemove('$removeURL', '{$row['menu_id']}', 'remover" . $row["menu_id"] . "', '" . $text["removing"] . "');\">{$text['remove']}</span></td>\n
								</tr>\n";
				} else {
						$retVal .= "<tr id=\"tr{$row['menu_id']}\">\n
										<td id=\"td{$row['menu_id']}\">\n
											{$row['menu']} ($children) \n
										</td>\n
										<td><span id=\"adder{$row['menu_id']}\" class=\"rightsLink\" onclick=\"processMe('$addURL', '{$row['menu_id']}', 'adder" . $row["menu_id"] . "', '" . $text["adding"] . "');\">{$text['add']}</span></td>\n
										<td><span id=\"cadder{$row['menu_id']}\" class=\"rightsLink\" onclick=\"processMe('$addcURL', '{$row['menu_id']}', 'cadder" . $row["menu_id"] . "', '" . $text["adding"] . "');\">{$text['addc']}</span></td>\n
									</tr>\n";
					}
				$retVal .= "<tr>
								<td colspan=\"3\">
									<span class='contstore' id='ispan{$row['menu_id']}'></span>
									<span id='span{$row['menu_id']}'></span>
								</td>
							</tr>";
			}	//END while
			$retVal .= "</table>\n";
		} else $retVal = "";	//END if hasrows
		return $retVal;
	}	//END getSubMenu4RightsEdit()





/**
 * Used to display main menus in the menu tree on the page select section for AUDIT TRAIL
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param int $parent           The ID of the menu item whose children sub-tree would be shown.
 * @param string $noMenu        The error message to show if no menu was found
 * @return string               The generated HTML for the menu tree traversal.
 */
	public static function getMenu4AuditTrail($langField, $parent, $noMenu){
		$conn = new DBConf();
		$query = "SELECT m.menu_id, lc.$langField menu, m.menu_publish 
					FROM menu m INNER JOIN language_content lc
					ON m.langcont_id = lc.langcont_id
					WHERE m.menu_parentid = '$parent'
						AND menu_publish = '1'
						AND menu_dropdown = '1'
					ORDER BY menu_order, menu_date DESC";
		$result = $conn->execute($query);
		if ($conn->hasRows($result)){
			$retVal = "<div id=\"ajaxLoader\"></div>
						<table cellspacing=\"5\" cellpadding=\"5\" border=\"0\" class=\"menuDisplay\">\n";
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$url = "index.php?p=ajax&m=sysadmin&t=audittrail&d={$row['menu_id']}";
				//$editURL = "index.php?p=menumanagement_edit&m=sysadmin&d={$row['menu_id']}";
				$children = self::getChildrenNo4AuditTrail($row['menu_id'], $conn);
				$retVal .= "<tr>\n
								<td id=\"td{$row['menu_id']}\">\n
									<span class=\"treeImage\"><img id=\"img{$row['menu_id']}\" title=\"Expand\" onclick=\"processSubs('$url', '{$row['menu_id']}');\" src=\"images/expand.jpg\" border=\"0\" /></span>\n
									<input name=\"pages[]\" type=\"checkbox\" value=\"{$row['menu_id']}\" />\n
									{$row['menu']} ($children)\n
									<span class='contstore' id='ispan{$row['menu_id']}'></span>
									<span id='span{$row['menu_id']}'></span>
								</td>\n
							</tr>\n";
			}
			$retVal .= "</table>\n";
		} else $retVal = $noMenu;
		return $retVal;
	}	//END getMenu4AuditTrail()





/**
 * Used to display sub-menus in the menu tree on the page select page for AUDIT TRAIL
 * 
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param int $parent           The ID of the menu item whose children sub-tree would be shown.
 * @param boolean $showParentID If true the parent ID ($parent) is prepended to the menu tree
 * @return string               The generated HTML for the menu tree traversal.
 */
	public static function getSubMenu4AuditTrail($langField, $parent, $showParentID = true){
		$conn = new DBConf();
		$query = "SELECT m.menu_id, lc.$langField menu, m.menu_publish 
					FROM menu m INNER JOIN language_content lc
					ON m.langcont_id = lc.langcont_id
					WHERE m.menu_parentid = '$parent'
						AND menu_publish = '1'
						AND menu_dropdown = '1'
					ORDER BY menu_order, menu_date DESC";
		$result = $conn->execute($query);
		if ($conn->hasRows($result)){
			$retVal = $showParentID ? $parent : "";
			$retVal .= "<table cellspacing=\"5\" cellpadding=\"5\" border=\"0\" class=\"menuDisplay\">\n";
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$url = "index.php?p=ajax&m=sysadmin&t=audittrail&d={$row['menu_id']}";
				//$editURL = "index.php?p=menumanagement_edit&m=sysadmin&d={$row['menu_id']}";
				$children = self::getChildrenNo4AuditTrail($row['menu_id'], $conn);
				$retVal .= "<tr>\n
								<td id=\"td{$row['menu_id']}\">\n
									<span class=\"treeImage\"><img id=\"img{$row['menu_id']}\" title=\"Expand\" onclick=\"processSubs('$url', '{$row['menu_id']}');\" src=\"images/expand.jpg\" border=\"0\" /></span>\n
									<input name=\"pages[]\" type=\"checkbox\" value=\"{$row['menu_id']}\" />\n
									{$row['menu']} ($children)\n
									<span class='contstore' id='ispan{$row['menu_id']}'></span>
									<span id='span{$row['menu_id']}'></span>
								</td>\n
							</tr>\n";
			}
			$retVal .= "</table>\n";
		} else $retVal = "";
		return $retVal;
	}	//END getSubMenu4AuditTrail()




/**
 * Get the number of direct children a menu has
 * @param int $id       The ID (in the menu table) of the menu item under consideration
 * @param object $conn  A DBConf object
 * @return int          The number of children a menu item has
 */
	public static function getChildrenNo($id, $conn){
		$query = "SELECT COUNT(*) howmany FROM menu
					WHERE menu_parentid = '$id'";
		$result = $conn->execute($query);
		if ($conn->hasRows($result)){
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			$retVal = $row["howmany"];
		} else $retVal = 0;
		return $retVal;
	}	//END getChildrenNo()



//
//*
/**
 * Gets the number of direct children a menu has (limited to the ones that are published and shown in the nav bar)
 * USED ON THE PAGE SELECTION SECTION OF AUDIT TRAIL
 *
 * @param int $id       The ID (in the menu table) of the menu item under consideration
 * @param object $conn  A DBConf object
 * @return int          The number of children a menu item has. Only menu items that are published and allowed to show in the top navigation bar are counted.
 */
	public static function getChildrenNo4AuditTrail($id, $conn){
		$query = "SELECT COUNT(*) howmany FROM menu
					WHERE menu_parentid = '$id'
						AND menu_publish = '1'
						AND menu_dropdown = '1'";
		$result = $conn->execute($query);
		if ($conn->hasRows($result)){
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			$retVal = $row["howmany"];
		} else $retVal = 0;
		return $retVal;
	}	//END getChildrenNo()



/**
 * Gets all the properties of a menu item
 * 
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param int $id               The ID (in the menu table) of the menu item under consideration
 * @return array                An array continaing the properties of the menu item.
 */
	public static function getMenuProps($langField, $id){
		$conn = new DBConf();
		$query = "SELECT m.*, lc.$langField FROM menu m INNER JOIN language_content lc
					ON m.langcont_id = lc.langcont_id
					WHERE m.menu_id = '$id'";
		$result = $conn->execute($query);
		if ($conn->hasRows($result, 1)){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			$retVal = $row;
		} else $retVal = "";
		return $retVal;
	}	//END getMenuProps()



/**
 * Gets all available menus into a drop-down list
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param int $selected         The menu ID of the currently selected menu item. Needed so that the selected="selected" attribute can be added to the currently selected menu item
 * @return string               The HTML containing the drop-down list
 */
	public static function getMenu4DropDown($langField, $selected){
		$conn = new DBConf();
		$query = "SELECT m.menu_id, lc.$langField menuitem FROM menu m INNER JOIN language_content lc
					ON m.langcont_id = lc.langcont_id
					ORDER BY menuitem";
		$result = $conn->execute($query);
		$retVal = "";
		if ($conn->hasRows($result)){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				if ($row["menu_id"] == $selected)
					$retVal .= "<option value=\"{$row['menu_id']}\" selected=\"selected\">{$row['menuitem']}</option>";
				else $retVal .= "<option value='{$row['menu_id']}'>{$row['menuitem']}</option>";
			}
		}
		return $retVal;
	}	//END getMenu4DropDown()



/**
 * Gets a list of all the available modules in the application
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param mixed $all            Not used
 * @param int $selected         The module ID of the currently selected module. Needed so that the selected="selected" attribute can be added to the currently selected module
 * @return string               The HTML containing the drop-down list
 */
	public static function getModules4DropDown ($langField, $all, $selected){
		$conn = new DBConf();
		$query = "SELECT m.module_id, lc.$langField module FROM modules m INNER JOIN language_content lc
					ON m.langcont_id = lc.langcont_id
					ORDER BY module";
		$result = $conn->execute($query);
		$retVal = "";
		if ($conn->hasRows($result)){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				if ($row["module_id"] == $selected)
					$retVal .= "<option value=\"{$row['module_id']}\" selected=\"selected\">{$row['module']}</option>";
				else $retVal .= "<option value=\"{$row['module_id']}\">{$row['module']}</option>";
			}
		}
		return $retVal;
	}	//END getModules4DropDown()




/**
 * Gets a drop-down list of all menu items under a module if $module != -1 else, it gets all menu items
 *
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param int $module           The module ID under consideration
 * @return string               The HTML containing the drop-down list
 */
	public static function getMenu4Modules($langField, $module){
		$conn = new DBConf();
		$query = "SELECT m.menu_id, lc.$langField menuitem FROM menu m INNER JOIN language_content lc
					ON m.langcont_id = lc.langcont_id";
		if ($module != -1)
			$query .= " WHERE m.module_id = '$module'";
		$query .= " ORDER BY m.module_id, menuitem";
		$result = $conn->execute($query);
		$retVal = "";
		if ($conn->hasRows($result)){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$retVal .= "<option value=\"{$row['menu_id']}\">{$row['menuitem']}</option>";
			}
		}
		return $retVal;
	}	//END getMenu4Modules()



/**
 * Gets the menu ID attached to a page request
 *
 * @param string $p         A part of the query string for a page request which contains only the variable "p" and its value like "p=value"
 * @param string $page      The actual page a visitor requested for e.g. "index.php"
 * @return <type>
 */
	public static function getMenuIDFromP($p, $page){	//$p is of the form "p=xxxxxx"
		$conn = new DBConf();

		//Escape SQL wildcards
		$p = str_replace("%", "\%", $p);
		$p = str_replace("_", "\_", $p);

		$query = "SELECT menu_id FROM menu
					WHERE menu_url LIKE '%?$p'
						OR menu_url LIKE '%?$p&%'
						OR menu_url LIKE '%&$p&%'
						OR menu_url LIKE '%&$p'"; //echo ("<p><pre>$query</pre></p>");
		$result = $conn->execute($query);
		$retVal = array();
		if ($conn->hasRows($result)){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				array_push ($retVal, $row["menu_id"]);
			}
		} elseif ($page == "index.php"){
				$query = "SELECT menu_id FROM menu
							WHERE menu_url = 'index.php'";
				$result = $conn->execute($query);
				if ($conn->hasRows($result)){
					while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
						array_push ($retVal, $row["menu_id"]);
					}
				}
			} elseif (strlen($page) > 0 && $page != "index.php"){
					$query = "SELECT menu_id FROM menu
								WHERE menu_url LIKE '$page%'";
					$result = $conn->execute($query);
					if ($conn->hasRows($result)){
						while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
							array_push ($retVal, $row["menu_id"]);
						}
					}
				}

		//self::tempMenuName($retVal, $conn);
		return $retVal;
	}	//END method getMenuIDFromP()


}   //END class
?>