<?php
/**
 *@package drug_category
 *@author Ahmed Rufai
 * 
 * Create Date: 26-08-2009
 * License: LGPL 
 * 
 */
require_once './classes/DBConf.php';

Class drugcategory {
    /**
     *
     * @var array holds array of fields in the drugs category table
     */
    public $dcat=array() ;
     /**
     * @var array holds values to be stored in the drug type table field
       */
    public $dcat2=array() ;
	public $drugcat_id; //int(10)
	public $langcont_id; //varchar(255)
	public $connection;
    public $conn;
	 function  __construct() {
     $this->conn = new DBConf();
    }
    /**
       temporarily holds intemediate values from the database table or other script in the class
     */
	public function New_drugcategory($drugcat_name){
		$this->langcont_id = $langcont_id;
	}

    /**
     * Load one row into class variable. To use the vars use for exemple echo $class->getVar_name;
     *
     * @param int $key_row
     * 
     */
	public function Load_from_key($key_row){
		$result = $this->conn->execute("Select * from drugcategory where drugcat_id = \"$key_row\" ");
		while($row = mysql_fetch_array($result)){
			$this->drugcat_id = $row["drugcat_id"];
			$this->langcont_id = $row["langcont_id"];
		}
	}

    /**
     * Delete the row by using the key as arg
     * @param int $lang_id ID of a row in the language table that holds lang text
     * @param int $key_row
     * @param int $langtb
     */

    
	public function Delete_row_from_key($lang_id,$key_row,$langtb){
		$this->conn->execute("DELETE FROM drugcategory WHERE drugcat_id = $key_row");
		$this->conn->execute("DELETE FROM $langtb WHERE langcont_id = $lang_id");
	}

    /**
     *Update the active row  on table
   @param int $lang_id ID of a row in the language table that holds lang text
     * @param int $id row ID
     * @param int $tablename
     * @param string $curLnag_field
     * @param string $langtb
     */
     
public function Save_Active_Row($lang_id,$id,$tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("UPDATE drugcategory set drugcat_name = \"$this->drugcat_name\" where drugcat_id = \"$this->drugcat_id\"");*/
		
		try {
		$q=count($this->dcat);
		
		
		for ($i = 0; $i < $q; $i++) {
			if($this->dcat[$i]=='langcont_id')
		//	$qq="UPDATE $langtb set $curLnag_field ='".$this->dcat2[$i]."' where langcont_id = \"$lang_id\"";
				
		$this->conn->execute("UPDATE $langtb set $curLnag_field ='".$this->dcat2[$i]."' where langcont_id = \"$lang_id\"");

		}
		
			
	$sql = "UPDATE $tablename SET ";	
	
				$qq=count($this->dcat);
				$q=count($this->dcat);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->dcat[$i]=='langcont_id')
						     $sql.=    $this->dcat[$i] .'="'.$lang_id.'" ';
						else
         			  		  $sql.=    $this->dcat[$i] .'="'.$this->dcat2[$i].'" ';
			 
					 }
					 else{
					     if($this->dcat[$i]=='langcont_id')
						     $sql.=      $this->dcat[$i] .'="'.$lang_id.'" ,';
						 else
						     $sql.=      $this->dcat[$i] .'="'.$this->dcat2[$i].'" ,';
			
					 }
  				 $q--;
 			}
			$result2 =$this->conn->execute("SELECT * from $tablename");
			$i=0;			
		while ($i < mysql_num_fields($result2)) {
					$meta = mysql_fetch_field($result2);
					if($meta->primary_key==1) $key1=$meta->name;
					 $i++;
		}	
	     $sql.="  WHERE  ".$key1."=".$id;
		//echo $sql;
	//exit();
         $this->conn->execute($sql);
        } catch (Exception $e) {
            echo 'ERROR : i cant insert values '.$e->getMessage();
        }
		
		
		
		
	}

    /**
     *Save as a new row on table
     * @param string $tablename
     * @param string $curLnag_field
     * @param string $langtb
     * @return array
     */
    
	public function Save_Active_Row_as_New($tablename,$curLnag_field,$langtb){
		/*$this->conn->execute("Insert into drugcategory (drugcat_name) values (\"$this->drugcat_name\"");*/
		try {
				// server validation begins
					$getLang= new language();		
					$xlp_formfield =$this->dcat;
					$xlp_fieldmessage = array('Please specify Drug Category Name');
		            $xlp_error=$getLang->xlpildator($xlp_formfield,$xlp_fieldmessage);
					if (!empty($xlp_error[2]))
					throw new Exception;
					//sever validation ends
		$q=count($this->dcat);
		for ($i = 0; $i < $q; $i++) {
			if($this->dcat[$i]=='langcont_id'){
			$qq="Insert into $langtb ($curLnag_field) values ('".$this->dcat2[$i]."')";
			//echo $qq;
			    $this->conn->execute($qq);
				$content_id= mysql_insert_id();
			}	
		}
		
		$sql = "INSERT INTO $tablename SET ";	
	
				$qq=count($this->dcat);
				//$q=count($this->dcat);

			for ($i = 0; $i < $qq; $i++) {

	  
	  				if(($q-1)==0){
						if($this->dcat[$i]=='langcont_id')
						     $sql.=    $this->dcat[$i] .'="'.$content_id.'" ';
						else
         			  		  $sql.=    $this->dcat[$i] .'="'.$this->dcat2[$i].'" ';
			 
					 }
					 else{
					     if($this->dcat[$i]=='langcont_id')
						     $sql.=      $this->dcat[$i] .'="'.$content_id.'" ,';
						 else
						     $sql.=      $this->dcat[$i] .'="'.$this->dcat2[$i].'" ,';
			
					 }
  				 $q--;
 			}
		//echo $sql;
		//exit();
		 $this->conn->execute($sql);
        } catch (Exception $e) {
           return $xlp_error;
        }
		
		
		
		
	}
	
	public function GetSearchFields(){
	echo 	"<option value='-1'>Select Field</option>";	
		$result =$this->conn->execute("SELECT * from drugcategory order by drugcat_id");
				$i = 0;
				
while ($i < mysql_num_fields($result)) {
 //   echo "Information for column $i:<br />\n";
    $meta = mysql_fetch_field($result);
	
    if (!$meta) {
      //  echo "No information available<br />\n";
    }
  //  echo "$meta->name";
  
if($meta->name=='langcont_id')
	echo 	"<option value=". $meta->name.">".'drug category name'."</option>";
   else
	echo 	"<option value=". $meta->name.">".$meta->name."</option>";
	   
	   
	   
	   
	    $i++;
}
/*mysql_free_result($result);
	return $keys;*/
	}
	
	function getdrugcat_name($id,$curLnag_field,$langtb) {
        try {
		    $getLang= new language();
           $sql = 'Select * from drugcategory  where drugcat_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			//echo $sql ;
            return $getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb);
				 
            
			
        } catch (Exception $e) {


        }
    }
/*function getlang_content($id,$curLnag_field,$langtb) {
        try {
           $sql = 'Select '.$curLnag_field.'  from '. $langtb.'  where langcont_id='.$id;
            $res = $this->conn->execute($sql);
            $row = mysql_fetch_array($res);
			return $row[$curLnag_field];
		} catch (Exception $e) {


        }
    }*/
function getdrug2($id,$curLnag_field,$langtb) {
        try {
		    $getLang= new language();
			$sql2 = 'select drugcat_id from drug_subcat  where drugsub_id ='.$id;
			
				 $res2 = $this->conn->execute($sql2);
				 if( $row2 = mysql_fetch_array($res2))
				  $id2=$row2['drugcat_id'];
		
            $sql = 'select drugcat_id,langcont_id from drugcategory ';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
			$sel= ($id2==$row['drugcat_id'])? "selected":" ";
			
                echo '<option value ="'.$row['drugcat_id'] .'"'. $sel.'>'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
   
            }
        } catch (Exception $e) {


        }
    }

	
function getdrugcat($curLnag_field,$langtb) {
        try {
		    $getLang= new language();
            $sql = 'select drugcat_id,langcont_id from drugcategory';
            $res = $this->conn->execute($sql);
            while ($row = mysql_fetch_array($res)) {
                echo '<option value ="'.$row['drugcat_id'] .'">'.$getLang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</option>';
               }
        } catch (Exception $e) {


        }
    }

	/**
     * list all rows from a specified table
     * @param string $curLnag_field
     * @param string $langtb
     */
function allrows($curLnag_field,$langtb) {
        
		try {
	 $getlang= new language();
	$db2 = new DBConf();
		 $sql = 'SELECT
*
FROM
drugcategory
';
$pageindex='drug';
$pager = new PS_Pagination($db2,$sql,200,10,$pageindex);
$rs = $pager->paginate();
$offset=$pager->offset;           

           // $res = $this->conn->execute($sql);
			$i=1;
           while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    if ($i%2 ==0) {$bgcolor = "tr-row";} else {$bgcolor = "tr-row2";} 
			    echo' <tr class="'.$bgcolor.'">
         <td>'.(++$offset).'.</td>
        <td><a href = "./index.php?p=editdrug&drugcat_id='.$row["drugcat_id"].'&langid='.$row["langcont_id"] .'">'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</a></td>
		<td>'.'<a href = "./index.php?jj=delete&p=drug&id='.$row["drugcat_id"].'&langid='.$row["langcont_id"].'"></a></td>
		    
        <td>'.'<a href = "./index.php?p=drugsub&id='.$row["drugcat_id"].'&langid='.$row["langcont_id"].'">Add Sub-Category</a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
        } catch (Exception $e) {
	
        }

  
    }
	
	function rowSearch($search_field,$search_value,$curLnag_field,$langtb){
		 try {
		  $getlang= new language();
		 $db2 = new DBConf();
		$sql = "Select * from  drugcategory  where  ".$search_field."='".$search_value."'" ;
		$res =$this->conn->execute($sql);
		
$pageindex='drug';
$pager = new PS_Pagination($db2,$sql,200,10,$pageindex);
$rs = $pager->paginate();
	  // echo "entered";
	     $i=1;
		while ($row = mysql_fetch_array($rs)) {
             
			// $cat_name=$this->getContypecat_name($row["ctc_id"])  ;
			    echo' <tr>
        <td><input type="checkbox" class="checkbox" name="chkID[]" value="'.$row["drugcat_id"].'" />'.$ii.'</td>
        <td>'.$row["drugcat_id"].'</td>
		<td>'.$getlang->getlang_content($row['langcont_id'],$curLnag_field,$langtb).'</td>
		       
        <td>'.'<a href = "./index.php?jj=delete&p=drug&id='.$row["drugcat_id"] .'"><img src="./images/btn_delete_02.gif"  style="border: none"/></a></td><td>'.'<a href = "./index.php?p=editdrug&drugcat_id='.$row["drugcat_id"] .'"><img src="./images/btn_edit.gif"  style="border: none"/></a></td>
        </tr>';
		$i++;
            }
			?>
		 <tr>
            <td  colspan="5"><?php echo $pager->renderFullNav();?></td>
        </tr></table>
		<?
		} catch (Exception $e) {
        }
	}
	
	 
	
	

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     */
	public function GetKeysOrderBy($column, $order){
		$keys = array(); $i = 0;
		$result = $this->conn->execute("SELECT drugcat_id from drugcategory order by $column $order");
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$keys[$i] = $row["drugcat_id"];
				$i++;
			}
	return $keys;
	}

    /**
     * Close mysql connection
     */
	public function enddrugcategory(){
		$this->connection->CloseMysql();
	}

}

// ------------------------------------------------------------------------

