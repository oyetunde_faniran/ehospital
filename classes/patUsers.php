<?php
/**
 * Handles the retrieval of user info as it concerns Patient Care module
 * 
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */

/**
 * Handles the retrieval of user info as it concerns Patient Care module
 *
 * @author Bilewomo, Desola
 *
 * @package Patient_Care
 */
class patUsers{
public $user = array();



/**
 * Class Constructor
 */
    function  __construct() {
        $this->conn = new DBConf();
    }





/**
 * Gets the name of a staff, employee number and staff type (clinical / non-clinical)
 * @param int $user_id  The user ID of the user in the users table
 * @return mixed        Returns an array containing the required details or an integer to show the kind of error that was encountered
 */
  function getPatUser($user_id) {
        try {
            $query = "SELECT users.staff_employee_id, staff_type, staff_title, staff_surname, staff_othernames
					    FROM users 
				   LEFT JOIN staff 
				          ON users.staff_employee_id = staff.staff_employee_id
					   WHERE users.user_id = '".$user_id."'" ;
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);
			$num = mysql_num_rows($res);

            if ($res) {
				if($num>0){
					 while ($row = mysql_fetch_array($res)) {
					 extract($row);				 
					 $user = array( 'patuser_name' => $staff_title.' '.$staff_surname.' '.$staff_othernames,
									'patuser_empid' => $staff_employee_id,
									'patuser_stafftype' => $staff_type
									);
					 
            	}
					$msg = $user;
				}else{
					$msg = 2;
				}
            }else{
			$msg = 1;
			}
			return $msg;
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END getPatUser()




/**
 * Gets the staff type ((non-)clinical) of a user
 * @param int $user_id  The user ID of the user under consideration
 * @return int          Returns an integer that represents the type of staff
 */
  function getStaffType($user_id) {
        try {
            $query = "SELECT staff_type FROM users LEFT JOIN staff 
				          ON users.staff_employee_id = staff.staff_employee_id
					   WHERE users.user_id = '".$user_id."'" ; //die ($query);
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);
			$num = mysql_num_rows($res);

            if ($res) {
				if($num > 0){
					while ($row = mysql_fetch_array($res)) {
						$user =  $row['staff_type'];
            		}
					$msg = $user;
				} else {
					$msg = 2;
				}
            } else {
					$msg = 1;
			}
			return $msg;
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END getStaffType()




/**
 * Gets the clinic, name and consultant ID of a user if the user is a consultant
 * @param string $staff_employee_id     The employee ID of the staff under consideration
 * @return mixed                        An array containing the details on success or an integer that determines the kind of message to show to the user on failure
 * @deprecated                          Deprecated since version 1.0 in favour of isConsultant4rmConsultantID($consultant_id), which uses the consultant ID to get the details instead of the staff employee ID
 */
  function isConsultant($staff_employee_id) {
        try {
			$query = "SELECT c.clinic_id, c.consultant_id,  
						CONCAT_WS(' ',s.staff_title, s.staff_surname, s.staff_othernames) as consult_name
						FROM consultant c INNER JOIN staff s
						ON c.staff_employee_id = s.staff_employee_id
						WHERE c.staff_employee_id = '" . $staff_employee_id."'";
			//die ("<pre>$query</pre>");
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);
			$num = mysql_num_rows($res);

            if ($res) {
				if($num>0){
					while ($row = mysql_fetch_assoc($res)){
						extract($row);
                        $user = array ('consult_clinic' => $clinic_id,
                                       'consult_id' => $consultant_id,
                                       'consult_name' => $consult_name);
					}
					$msg = $user;
				} else {
						$msg = 4;
					}
            } else {
				$msg = 2;
			}
			return $msg;
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END isConsultant()





/**
 * Gets the clinic, name and consultant ID of a consultant
 * @param int $consultant_id    The consultant ID
 * @return mixed                An array containing the details on success or an integer that determines the kind of message to show to the user on failure
 */
  function isConsultant4rmConsultantID($consultant_id) {
	  $consultant_id = (int)$consultant_id;
        try {
			$query = "SELECT c.clinic_id, c.consultant_id,  
						CONCAT_WS(' ',s.staff_title, s.staff_surname, s.staff_othernames) as consult_name
						FROM consultant c INNER JOIN staff s
						ON c.staff_employee_id = s.staff_employee_id
						WHERE c.consultant_id = '" . $consultant_id . "'";
			//die ("<pre>$query</pre>");
            //  $res = mysql_query($query) or die (mysql_errno());
            $res = $this->conn->execute($query);
			$num = mysql_num_rows($res);

            if ($res) {
				if($num>0){
					while ($row = mysql_fetch_assoc($res)){
						extract($row);
							$user = array ('consult_clinic' => $clinic_id, 
										   'consult_id' => $consultant_id,
										   'consult_name' => $consult_name);
					}
					$msg = $user;
				} else {
						$msg = 4;
					}
            } else {
				$msg = 2;
			}
			return $msg;
        } catch (Exception $e) {
			//$err = new Error_handler();
			set_error_handler (array("Error_handler", "my_error_handler"));
		//echo $err;
        }
    }   //END isConsultant4rmConsultantID()





/**
 * Gets the staff employee ID from a users' user ID
 * @param int $userID   The user ID of the user under consideration
 * @return string       The staff employee ID
 */
	public function getEmployeeIDfromUserID($userID){
		$userID = (int)$userID;
		$query = "SELECT staff_employee_id staffID FROM staff
					WHERE user_id = '$userID'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result, 1)){
			$row = mysql_fetch_array ($result, MYSQL_ASSOC);
			$retVal = $row["staffID"];
		} else $retVal = 0;
		return $retVal;
	}   //END getEmployeeIDfromUserID()

}
?>