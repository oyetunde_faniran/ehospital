<?php
/**
 * Handles all operations that has to do with page menu
 * 
 * @author Faniran, Oyetunde
 *
 * @package System_Administration
 */

/**
 * Handles all operations that has to do with page menu
 *
 * @author Faniran, Oyetunde
 *
 * @package System_Administration
 */
class admin_rights {
    /**
     * @var object An object of the DBConf class
     */
	protected $conn;

    /**
     * @var string Stores the name of the current column from which content would be fetched from the language table
     */
	protected $langField;

    /**
     * @var int The ID of the current group
     */
	protected $groupID;

    /**
     * @var int The ID of the current menu item
     */
	protected $menuID;

    /**
     * @var array   An array containing all the error messages that may be generated in some methods
     */
	protected $errorMsgs;

    /**
     * @var string  Would containing the error message of any error that occurs during the course of performing an operation in the class.
     */
	public $errorMsg;

    /**
     * @var boolean =true if an error occurred during the last operation in the class else =false
     */
	public $error;



/**
 * Class Constructor
 * @param string $langField     The name of the column in the language table corresponding to the currently selected language
 * @param int $groupID      The ID of the user group under consideration
 * @param int $menuID       The menu ID of the menu item under consideration
 * @param array $errorMsgs  An array of error messages that can be generated
 */
	public function __construct($langField, $groupID, $menuID, $errorMsgs){
		$this->conn = new DBConf();
		$this->langField = $langField;
		$this->error = false;

		if (is_array($errorMsgs))
			$this->errorMsgs = $errorMsgs;
		else $errorMsgs = array (
								"invalidGM" => "RIGHTS EDIT FAILED! Invalid group and / or menu"
							);
		try {
			if (empty($groupID) || empty($menuID) || !is_numeric($groupID) || !is_numeric($menuID))
				throw new Exception;
			$this->groupID = $groupID;
			$this->menuID = $menuID;
		} catch (Exception $e) {
				$this->error = true;
				$this->errorMsg = $this->errorMsgs["invalidGM"];
			}
	}   //END __construct()



/**
 * Adds the menu ID specified in the constructor to the list of rights of the group specified in the constructor
 * @param array $text           An array of all the string literals that can be generated during the operation
 * @param boolean $addChildren  = true if all the direct children of the menu item with the specified menu ID should be added to the list of rights of the specified group
 * @return string               Generated HTML table cells that would fit into the menu tree and showing the current item with a style that depicts it as added to the list of rights
 */
	public function addRights($text, $addChildren = false){
		//die ("adding rights");
		$query = "SELECT * FROM rights
					WHERE menu_id = '" . $this->menuID . "' AND group_id = '" . $this->groupID . "'";
		$result = $this->conn->execute ($query);
		if ($this->conn->hasRows($result))
			$existing = true;
		else $existing = false;
		
		if (!$existing){
			$query = "INSERT INTO rights 
						VALUES ('0', '" . $this->menuID . "', '" . $this->groupID . "')";
			$result = $this->conn->execute ($query);
			if ($this->conn->hasRows($result, 1))
				$inserted = true;
			else $inserted = false;
		} else $inserted = false;

		$children = admin_menu::getChildrenNo($this->menuID, $this->conn);
		$menu = admin_menu::getMenuProps($this->langField, $this->menuID);
		$menu = is_array ($menu) ? $menu[$this->langField] : "";
		$url = "index.php?p=ajax&m=sysadmin&t=rights&d=" . $this->menuID . "&d1=" . $this->groupID;
		$retVal = "";
		if ($inserted || $existing){

			//This block only affects the link "Add & Add Direct Sub-Menu"
			if ($addChildren)
				$this->addChildrenRights($this->menuID);

			$removeURL = "index.php?p=ajax&m=sysadmin&t=rights_remove&d=" . $this->menuID . "&d1=" . $this->groupID;
			$retVal .= $this->menuID . "<td id=\"td" . $this->menuID . "\">\n
							<span class=\"treeImage\"><img id=\"img" . $this->menuID . "\" title=\"Expand\" onclick=\"processSubs('$url', '" . $this->menuID . "');\" src=\"images/expand.jpg\" border=\"0\" /></span>\n
							<span class=\"rightsText\">$menu ($children)</span> \n
						</td>\n
						<td><span class=\"rightsText\">{$text['added']}</span></td>\n
						<td><span id=\"remover" . $this->menuID . "\" class=\"rightsLinkRemove\" onclick=\"confirmRemove('$removeURL', '" . $this->menuID . "', 'remover" . $this->menuID . "', '" . $text["removing"] . "');\">{$text['remove']}</span></td>\n";
		} else {
				$addURL = "index.php?p=ajax&m=sysadmin&t=rights_add&d=" . $this->menuID . "&d1=" . $this->groupID ;
				$addcURL = "index.php?p=ajax&m=sysadmin&t=rights_addc&d=" . $this->menuID . "&d1=" . $this->groupID;
				$retVal .= $this->menuID . "<td id=\"td" . $this->menuID . "\">\n
									$menu ($children) \n
							</td>\n
							<td><span id=\"adder" . $this->menuID . "\" class=\"rightsLink\" onclick=\"processMe('$addURL', '" . $this->menuID . "', 'adder" . $this->menuID . "', '" . $text["adding"] . "');\">{$text['add']}</span></td>\n
							<td><span id=\"cadder" . $this->menuID . "\" class=\"rightsLink\" onclick=\"processMe('$addcURL', '" . $this->menuID . "', 'cadder" . $this->menuID . "', '" . $text["adding"] . "');\">{$text['addc']}</span> {$text['addFailed']}</td>\n";
			}
		return $retVal;
	}   //END addRights()



/**
 * Adds the children of a menu to the rights of the group specified in the constructor
 * @param int $menuID   The ID of the menu item whose children would be added to the list of rights of the specified group
 */
	public function addChildrenRights($menuID){
		$query = "SELECT menu_id FROM menu
					WHERE menu_parentid = '$menuID'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$childMenu = $row["menu_id"];
				$query = "INSERT INTO rights 
							VALUES ('0', '$childMenu', '" . $this->groupID . "')";
				$this->conn->execute($query);
			}
		}
	}   //END addChildrenRights()



/**
 * Removes the menu ID specified in the constructor from the list of rights of the group specified in the constructor
 * @param array $text           An array of all the string literals that can be generated during the operation
 * @return string               Generated HTML table cells that would fit into the menu tree and showing the current item with a style that depicts it as added to the list of rights
 */
	public function removeRights($text){
		$query = "DELETE FROM rights
					WHERE menu_id = '" . $this->menuID . "' AND group_id = '" . $this->groupID . "'";

		$result = $this->conn->execute ($query);
		if ($this->conn->hasRows($result, 1)){ 
		//echo "<h1>DELETE CHILDREN</h1>";
			$this->removeChildrenRights($this->menuID);	//Remove all the children under this menu too
		}
		$children = admin_menu::getChildrenNo($this->menuID, $this->conn);
		$menu = admin_menu::getMenuProps($this->langField, $this->menuID);
		$menu = is_array ($menu) ? $menu[$this->langField] : "";
		$url = "index.php?p=ajax&m=sysadmin&t=rights&d=" . $this->menuID;
		$retVal = "";
		if ($this->conn->hasRows($result, 1)){
			$addURL = "index.php?p=ajax&m=sysadmin&t=rights_add&d=" . $this->menuID . "&d1=" . $this->groupID ;
			$addcURL = "index.php?p=ajax&m=sysadmin&t=rights_addc&d=" . $this->menuID . "&d1=" . $this->groupID;
			$retVal .= $this->menuID . "<td id=\"td" . $this->menuID . "\">\n
								$menu ($children) \n
						</td>\n
						<td><span id=\"adder" . $this->menuID . "\" class=\"rightsLink\" onclick=\"processMe('$addURL', '" . $this->menuID . "', 'adder" . $this->menuID . "', '" . $text["adding"] . "');\">{$text['add']}</span></td>\n
						<td><span id=\"cadder" . $this->menuID . "\" class=\"rightsLink\" onclick=\"processMe('$addcURL', '" . $this->menuID . "', 'cadder" . $this->menuID . "', '" . $text["adding"] . "');\">{$text['addc']}</span></td>\n";
		} else {
				$removeURL = "index.php?p=ajax&m=sysadmin&t=rights_remove&d=" . $this->menuID . "&d1=" . $this->groupID;
				$retVal .= $this->menuID . "<td id=\"td" . $this->menuID . "\">\n
								<span class=\"treeImage\"><img id=\"img" . $this->menuID . "\" title=\"Expand\" onclick=\"processSubs('$url', '" . $this->menuID . "');\" src=\"images/expand.jpg\" border=\"0\" /></span>\n
								<span class=\"rightsText\">$menu ($children)</span> \n
							</td>\n
							<td><span class=\"rightsText\">{$text['added']}</span></td>\n
							<td><span id=\"remover" . $this->menuID . "\" class=\"rightsLinkRemove\" onclick=\"confirmRemove('$removeURL', '" . $this->menuID . "', 'remover" . $this->menuID . "', '" . $text["removing"] . "');\">{$text['remove']} {$text['removeFailed']}</span></td>\n";
			}
		return $retVal;
	}   //END removeRights()

/**
 * Removes the children of a menu from the rights of the group specified in the constructor
 * @param int $menuID   The ID of the menu item whose children would be removed from the list of rights of the specified group
 */
	private function removeChildrenRights($menuID){
		$query = "SELECT menu_id FROM menu
					WHERE menu_parentid = '$menuID'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result)){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				$this->removeChildrenRights ($row["menu_id"]);
		}
		$query = "DELETE FROM rights
					WHERE menu_id = '$menuID' AND group_id = '" . $this->groupID . "'";
		$result = $this->conn->execute($query);
	}   //END removeChildrenRights()

 
}	//END class
?>