<?php
/**
 * Handles all operations that has to do with user groups
 * 
 * @author Faniran, Oyetunde
 *
 * @package System_Administration
 */

/**
 * Handles all operations that has to do with user groups
 *
 * @author Faniran, Oyetunde
 * 
 * @package System_Administration
 */
class admin_group {

    /**
     * @var object An object of the DBConf class
     */
	protected $conn;

    /**
     * @var string Stores the name of the current column from which content would be fetched from the language table
     */
	protected $langField;

    /**
     * @var int The ID of the current group
     */
	protected $groupID;

    /**
     * @var string  Would containing the error message of any error that occurs during the course of performing an operation in the class.
     */
	public $errorMsg;

    /**
     * @var boolean =true if an error occurred during the last operation in the class else =false
     */
	public $error;



/**
 * Class constructor
 * @param string $langField     A string containing the name of the column in the language table corresponding to the currently selected language
 * @param int $id               The ID of the current group in the groups table (groups). Has a default value of 0.
 */
	public function __construct($langField, $id = 0){
		$this->conn = new DBConf();
		$this->langField = $langField;
		$this->error = false;
		if (!empty($id))
			$this->groupID = $id;
	}   //END __construct()



/**
 * Creates a new User Group
 * @param array $posted             The posted array from the create groups page, which must contain the following elements
 *                                  group (The name of the new group to be created), groupdesc (A brief description of the new group)
 * @param string $allFieldsError    The error message to show if any error occurrs during the operation
 * @return boolean                  Returns the error status at the end of the operation
 */
	public function createGroup($posted, $allFieldsError){
		$posted = admin_Tools::doEscape($posted, $this->conn);
		try {
			if (empty ($posted["group"])){
				$this->errorMsg = $allFieldsError;
				throw new Exception;
			}
			$langID = admin_Tools::insert2Lang($posted["group"], $this->langField);
			$query = "INSERT INTO groups
						VALUES ('0', '$langID', '" . $posted["groupdesc"] . "')";
			$this->conn->execute ($query);
		} catch (Exception $e) {
				$this->error = true;
			}
		return $this->error;
	}   //END createGroup()



/**
 * Modifies the properties of a particular user group
 * @param array $posted             The posted array from the edit groups page, which must contain the following elements
 *                                  group (The name of the new group to be created), groupdesc (A brief description of the new group)
 * @param string $allFieldsError    The error message to show if any error occurrs during the operation
 * @return boolean                  Returns the error status at the end of the operation
 */
	public function editGroup($posted, $allFieldsError){
		$posted = admin_Tools::doEscape($posted, $this->conn);
		try {
			if (empty ($posted["group"])){
				$this->errorMsg = $allFieldsError;
				throw new Exception;
			}

			$langID = admin_Tools::updateLang($this->groupID, $posted["group"], $this->langField, "groups", "group_id");
			$query = "UPDATE groups
						SET group_desc = '" . $posted["groupdesc"] . "'
						WHERE group_id = '" . $this->groupID . "'";
			$this->conn->execute ($query);
		} catch (Exception $e) {
				$this->error = true;
			}
		return $this->error;
	}   //END editGroup()



/**
 * Gets a list of all user groups on an HTML table with clickable links the URL specified in $url
 * @param string $langField     A string containing the name of the column in the language table corresponding to the currently selected language
 * @param string $noGroup       The error message to show if no user group was found
 * @param string $url           The URL to connect to when the link is clicked
 * @return string               The generated HTML containing the list of all user groups with links to the URL specified in $url
 */
	public static function getGroup($langField, $noGroup, $url){
		$conn = new DBConf();
		$query = "SELECT g.group_id, lc.$langField groupname, g.group_desc
					FROM groups g INNER JOIN language_content lc
					ON g.langcont_id = lc.langcont_id
					ORDER BY groupname";// echo "<pre>$query</pre>";
		$result = $conn->execute($query);
		if ($conn->hasRows($result)){
			$counter = 0;
			$retVal = "<table class=\"padMe\" border=\"0\">\n";
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$editURL = $url . "&d={$row['group_id']}";
				$retVal .= "<tr>\n
								<td align=\"left\">\n" . (++$counter) . ".</td>\n
								<td>\n
									<a href=\"$editURL\">{$row['groupname']}</a>\n
								</td>\n
								<td>\n
									{$row['group_desc']}
								</td>\n
							</tr>\n";
			}
			$retVal .= "</table>\n";
		} else $retVal = $noGroup;
		return $retVal;
	}   //END getGroup()




/**
 * Gets a list of all user groups in a drop-down list format
 * @param string $langField     A string containing the name of the column in the language table corresponding to the currently selected language
 * @param int $chosen           The ID of the currently selected item so that the selected="selected" attribute can be added to it
 * @return string               Returns a drop-down list of all user groups
 */
	public static function getGroups4DropDown($langField, $chosen = 0){
		$conn = new DBConf();
		$query = "SELECT g.group_id, lc.$langField groupname
					FROM groups g INNER JOIN language_content lc
					ON g.langcont_id = lc.langcont_id
					ORDER BY groupname";// echo "<pre>$query</pre>";
		$result = $conn->execute($query);
		$retVal = "";
		if ($conn->hasRows($result)){
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				if ($row['group_id'] == $chosen)
					$retVal .= "<option value=\"{$row['group_id']}\" selected=\"selected\">{$row['groupname']}</option>";
				else $retVal .= "<option value=\"{$row['group_id']}\">{$row['groupname']}</option>";
			}
		}
		return $retVal;
	}   //END getGroups4DropDown()



/**
 * Gets the properties of a user group whose id was specified during the instantiation of this class. That is, in the class constructor
 * @return mixed    Returns an array containing the details if found else false.
 */
	public function getGroupDeets(){
		$query = "SELECT lc." . $this->langField . " groupname, g.group_desc
					FROM groups g INNER JOIN language_content lc
					ON g.langcont_id = lc.langcont_id
					WHERE g.group_id = '" . $this->groupID . "'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result, 1))
			$retVal = mysql_fetch_array ($result, MYSQL_ASSOC);
		else $retVal = false;
		return $retVal;
	}   //END getGroupDeets()



/**
 * Gets the default page that users in a particular user group (whose ID was specified while calling this class' constructor) should be redirected to on logging-in
 * @return string The URL of the default page for the specified user group
 */
	public function getDefaultGroupPage(){
		$query = "SELECT m.menu_url FROM menu m INNER JOIN groups_defaultpage gdp
					ON m.menu_id = gdp.menu_id
					WHERE gdp.group_id = '" . $this->groupID . "'";
		$result = $this->conn->execute($query);
		if ($this->conn->hasRows($result, 1)){
			$retVal = mysql_fetch_array ($result, MYSQL_ASSOC);
			$retVal = $retVal["menu_url"];
		} else $retVal = "";
		return $retVal;
	}   //END getDefaultGroupPage()


}   //END class
?>