<?php
	require_once ('./includes/config.inc.php');
	$type = isset($_GET["t"]) ? $_GET["t"] : "";
	$id = isset($_GET["d"]) ? (int)$_GET["d"] : 0;
	$result = 0;

	switch ($type) {
		case "menu":
				$imgSrc = isset($_GET["it"]) ? $_GET["it"] : 0;
				$result = admin_menu::getSubMenu($curLangField, $id);
				break;
		case "audittrail":
				$imgSrc = isset($_GET["it"]) ? $_GET["it"] : 0;
				$result = admin_menu::getSubMenu4AuditTrail($curLangField, $id);
				break;
		case "rights":
				$groupID = isset($_GET["d1"]) ? (int)$_GET["d1"] : 0;
				$imgSrc = isset($_GET["it"]) ? $_GET["it"] : 0;
				$text = array (
							"add"		=> $admin_rightmanagement_add,
							"addc"		=> $admin_rightmanagement_addc,
							"remove"	=> $admin_rightmanagement_remove,
							"added"		=> $admin_rightmanagement_added,
							"nomenu"	=> $admin_menumanagement_nomenu,
							"adding"	=> $admin_rightmanagement_adding,
							"removing"	=> $admin_rightmanagement_removing
						);
				$result = admin_menu::getSubMenu4RightsEdit($curLangField, $id, $groupID, $text, true);
				break;
		case "rights_add":
		case "rights_addc":
				$text = array (
								"add"		=> $admin_rightmanagement_add,
								"addc"		=> $admin_rightmanagement_addc,
								"remove"	=> $admin_rightmanagement_remove,
								"added"		=> $admin_rightmanagement_added,
								"addFailed"	=> $admin_rightmanagement_addfailed,
								"adding"	=> $admin_rightmanagement_adding,
								"removing"	=> $admin_rightmanagement_removing
							);
				$errorMsgs = array (
									"invalidGM" => $admin_rightmanagement_editerror
								);

				$groupID = isset($_GET["d1"]) ? (int)$_GET["d1"] : 0;
				$right = new admin_rights($curLangField, $groupID, $id, $errorMsgs);	//$id = menuID
				if ($type == "rights_addc")
					$result = $right->addRights($text, true);
				else $result = $right->addRights($text);
				break;

		case "rights_remove":
				$text = array (
								"add"			=> $admin_rightmanagement_add,
								"addc"			=> $admin_rightmanagement_addc,
								"remove"		=> $admin_rightmanagement_remove,
								"added"			=> $admin_rightmanagement_added,
								"addFailed"		=> $admin_rightmanagement_addfailed,
								"removeFailed"	=> $admin_rightmanagement_removefailed,
								"adding"	=> $admin_rightmanagement_adding,
								"removing"	=> $admin_rightmanagement_removing
							);
				$errorMsgs = array (
									"invalidGM" => $admin_rightmanagement_editerror
								);
				$groupID = isset($_GET["d1"]) ? (int)$_GET["d1"] : 0;
				$right = new admin_rights($curLangField, $groupID, $id, $errorMsgs);	//$id = menuID
				$result = $right->removeRights($text);
				break;
		case "menu4modules":
				$result = admin_menu::getMenu4Modules($curLangField, $id);
				break;
		case "deptclinics":
				$user = new admin_user();
				$result = $user->getClinics4DropDown($id);
				break;




/*BEGIN patient care ajax stuffs*/
		case "retainerplans":
				$selector = new patSelectOptions();
				$result = $selector->selectRetainerPlans("retainer", $id);
				break;
		case "schedule-new-appointment":
				/***	COPIED FROM HOW DESOLA DID IT IN patnewwindow_index.php	//Simply changed all echo statements to assignment into variable $result	***/
				//Make sure the request method is POST
				if ($_POST){
					$appoint = new patAppointment();
					$treat = new patTreatment();

//                    echo ('<pre>BEFORE: ' . print_r($_POST["appoint"], true) . '</pre>');

                    //If the patadm_id was not supplied, then get the last one for that patient
                    if(empty($_POST['appoint']['patadm_id'])){
                        $_POST['appoint']['patadm_id'] = $appoint->getLastPatAdmID($_POST['hospital_no']);
                    }

//                    die ('<pre>AFTER' . print_r($_POST["appoint"], true) . '</pre>');

					$appoint->appoint  = $_POST["appoint"];

					$app_id = $appoint->checkAppointment($appoint->appoint['patadm_id'], '', $appoint->appoint['consultant_id']);
					if ($app_id === FALSE){
						$result = $error_msg_5;
						exit();
					} else {
						if($app_id == 0) {
                            $clinicObj = new Clinic_New($curLangField, $appoint->appoint['clinic_id']);
                            $appCount = $appoint->getAppointmentCount();
                            $maxAppCount = $clinicObj->getMaxAppointment();


                            if ($appCount < $maxAppCount){
                                $app_id = $appoint->insertAppointment();

                                //The last arg here (true) would make the method not to set the appointment status to attended
                                $treat->updateAppointmentStatus($app_id, $appoint->appoint['patadm_id'], true);
                                $result = "Appointment scheduled successfully.";
                            } else $result = "Maximum number of appointments exceeded for the chosen date.";
						} else $result = $future_appointment_label;
					}
				} else $result = "Sorry! Action Failed";
                //$result = "APP COUNT: '$appCount' -- MAX: '$maxAppCount'";
                //$result .= " ---> <pre>" . print_r ($_POST["appoint"], true) . "</pre>";
				break;
		case "admit-patient":
				/***	COPIED FROM HOW DESOLA DID IT IN patnewwindow_index.php	//Simply changed all echo statements to assignment into variable $result	***/
				//Make sure the request method is POST
				if ($_POST){
					//Necessary init.
					$treat = new patTreatment();
					$treat->treat  = $_POST["treat"];

					//Check if the patient under consideration has been recommended for admission already
					$treat_id = $treat->checkAdmission($treat->treat['patadm_id'], $treat->treat['consultant_id']);

					//If not yet recommended, recommend, else notify
					if($treat_id == 0){
						$treat_id = $treat->insertAdmission();
						$result = "Action Successful.";
					} else $result = "Patient recommended for admission already.";
				} else $result = "Sorry! Action Failed";
				break;
/*END patient care ajax stuffs*/


/*BEGIN chat ajax stuffs*/
		case "get-online-users":
				$user = new admin_user();
				$onlineUsers = $user->getOnlineUsers4Chat($_SESSION[session_id() . "userID"]);
                if (is_array($onlineUsers)){
                    $jsonData = "";
                    foreach ($onlineUsers as $disUser){
                        $disName = $disUser["staff_title"] . " " . $disUser["staff_surname"] . " " . $disUser["staff_othernames"];
                        $chatName = admin_Tools::cleanUp4Chat($disName);
                        $status = $disUser["usersession_idle"] == 1 ? "idle" : "online";
                        $jsonData .= "{
                            \"displayName\": \"" . addslashes($disName) . "\",
                            \"name\": \"" . addslashes($chatName) . "\",
                            \"status\": \"" . $status . "\"
                        },";
                    }
                }
                $jsonData = rtrim($jsonData, ",");
                $result = "{\"onlineusers\": [$jsonData]}";
                header('Content-type: application/json');
				break;
/*END chat ajax stuffs*/


/*BEGIN Invoice Module stuffs*/
        case "drugs-dispense-search":
            $searchText = isset($_GET["q"]) ? $_GET["q"] : "";
            $disType = $id == 1 ? 1 : 0;
            $invDispense = new inventory_dispense();
            $allDrugs = $invDispense->doDrugSearch($searchText, $disType, $inventory_locationID);
            $result = "";
            $sNo = 0;
            $juggleRows = true;
            $rowStyle1 = " class=\"tr-row\" ";
            $rowStyle2 = " class=\"tr-row2\" ";
            $invUnit = new inventory_units();
            foreach ($allDrugs as $drug){
                //Alternating row styles management
                $rowStyle = $juggleRows ? $rowStyle1 : $rowStyle2;
                $juggleRows = !$juggleRows;

                //Style the search text differently in the result
                $medicalName = stripslashes($drug["drug_desc"]);
                $commName = stripslashes($drug["drug_name"]);
                switch ($id){
                    case 1:     $medicalName = str_replace($searchText, "<span class=\"text-searched-for \">$searchText</span>", $medicalName); break;
                    case 0:     $commName = str_replace($searchText, "<span class=\"text-searched-for \">$searchText</span>", $commName); break;
                }

                //Get all the units associated with this drug
                $allUnits = $invUnit->showInAllUnits($drug["quantity_in_stock"], $drug["drug_id"]);
                $disDispenseQuantity = $disQuantity = "<table cellspacing=\"2\" cellpadding=\"2\">";
                foreach ($allUnits as $unit){
                    //Used to display the quantity in stock per unit of the drug
                    $disQuantity .= "<tr>
                                        <td>" . stripslashes($unit["name"]) . ":</td>
                                        <td>" . number_format($unit["quantity"], 0) . "</td>
                                     </tr>";

                    //Used to display each unit of the drug alongside a text field to specify the quantity to be dispensed
                    $disDispenseQuantity .= "<tr>
                                                <td>" . stripslashes($unit["name"]) . ":</td>
                                                <td><input type=\"text\" size=\"5\" name=\"dispensequantity[" . $drug["drug_id"] . "][" . $unit["id"] . "]\" /></td>
                                             </tr>";
                }
                $disQuantity .= "</table>";
                $disDispenseQuantity .= "</table>";

                //Get all batches / expiry dates for this drug
                $batchExpiryDates = "<select name=\"batchexpirydateview\" id=\"batch-expiry-date-view-" . $drug["drug_id"] . "\" onchange=\"getBatchQuantity(this, " . $drug["drug_id"] . ")\">
                                        <option value=\"0\">--All--</option>";
                $batches = $invDispense->getBatchesForItem($drug["drug_id"], $inventory_locationID);
                foreach ($batches as $batch){
                    $batchExpiryDates .= "<option value=\"" . $batch["inventory_batchdetails_id"] . "\">Batch " . $batch["inventory_batch_id"] . " / " . $batch["expiryDate"] ."</option>";
                }
                $batchExpiryDates .= "</select>";


                //The final result for this row
                $result .= "<tr $rowStyle>
                                <td>" . (++$sNo) . ".</td>
                                <td>" . $medicalName . "</td>
                                <td>" . $commName . "</td>
                                <td>" . stripslashes($drug["drug_dosageform"]) . "</td>
                                <td>" . stripslashes($drug["drug_presentation"]) . "</td>
                                <td>" . stripslashes($drug["drug_strength"]) . "</td>
                                <td>" . stripslashes($drug["drug_manufacturer"]) . "</td>
                                <td id=\"quantity-cell-" . $drug["drug_id"] . "\">" . $disQuantity . "</td>
                                <td>" . $batchExpiryDates . "</td>
                                <td>
                                    <img src=\"images/dispense.png\" style=\"cursor: pointer;\" onclick=\"doDispense(" . $drug["drug_id"] . ");\" />
                                </td>
                            </tr>";
            }   //END foreach
            break;





        case "get-item-for-dispense":
            $batchID = isset($_GET["bid"]) ? (int)$_GET["bid"] : 0;
            $invDispense = new inventory_dispense();
            $disDrug = $invDispense->getDrugForDispense($id, $inventory_locationID, $batchID);

            if (is_array($disDrug)){
                //die ("<pre>" . print_r ($disDrug, true) . "</pre>");
                //Get all the units associated with this drug
                $invUnit = new inventory_units();
                $allUnits = $invUnit->showInAllUnits($disDrug["quantity_in_stock"], $disDrug["drug_id"]);
                $disDispenseQuantity = "<table cellspacing=\"0\" cellpadding=\"0\">";
                foreach ($allUnits as $unit){
                    //die ("<pre>" . print_r ($allUnits, true) . "</pre>");
                    //Used to display each unit of the drug alongside a text field to specify the quantity to be dispensed
                    $disDispenseQuantity .= "<tr>
                                                <td>" . stripslashes($unit["name"]) . ":</td>
                                                <td>[" . stripslashes($unit["quantity"]) . "]</td>
                                                <td><input type=\"text\" size=\"5\" name=\"dispensequantity[" . $disDrug["drug_id"] . "][" . $unit["id"] . "]\" value=\"0\" /></td>
                                             </tr>";
                }
                $disDispenseQuantity .= "</table>";
                $result = "<tr id=\"dispense-row-" . $disDrug["drug_id"] . "\" style=\"border-bottom: 1px solid #CCC;\">
                                <td>" . stripslashes($disDrug["drug_desc"]) . "</td>
                                <td>" . stripslashes($disDrug["drug_name"]) . "</td>
                                <td>" . stripslashes($disDrug["drug_dosageform"]) . "</td>
                                <td>" . stripslashes($disDrug["drug_presentation"]) . "</td>
                                <td>" . stripslashes($disDrug["drug_strength"]) . "</td>
                                <td>" . stripslashes($disDrug["drug_manufacturer"]) . "</td>
                                <td>" . $disDispenseQuantity . "</td>
                                <td>
                                    <textarea rows=\"10\" cols=\"10\" name=\"druglabel[" . $disDrug["drug_id"] . "]\" id=\"druglabel-" . $disDrug["drug_id"] . "\" ></textarea>
                                    <input type=\"hidden\" name=\"batchdetails[" . $disDrug["drug_id"] . "]\" value=\"$batchID\" />
                                </td>
                                <td>
                                    <img src=\"images/b_drop.png\" style=\"cursor: pointer;\" onclick=\"undoDispense(" . $disDrug["drug_id"] . ");\" />
                                </td>
                            </tr>";
            }   //END if the drug info was successfully retrieved
            break;







        case "get-batch-quantites":
            $batchID = isset($_GET["bid"]) ? (int)$_GET["bid"] : 0;
            $drugID = $id;
            //die ("Batch ID: $batchID ... Drug ID: $drugID");
            $invDispense = new inventory_dispense();
            $invUnit = new inventory_units();
            $disDrug = $invDispense->getDrugForDispense($drugID, $inventory_locationID, $batchID);
            $allUnits = $invUnit->showInAllUnits($disDrug["quantity_in_stock"], $disDrug["drug_id"]);
            $disDispenseQuantity = "<table cellspacing=\"2\" cellpadding=\"2\">";
            foreach ($allUnits as $unit){
                //die ("<pre>" . print_r ($allUnits, true) . "</pre>");
                //Used to display each unit of the drug alongside a text field to specify the quantity to be dispensed
                $disDispenseQuantity .= "<tr>
                                            <td>" . stripslashes($unit["name"]) . ":</td>
                                            <td>" . number_format($unit["quantity"], 0) . "</td>
                                         </tr>";
            }
            $disDispenseQuantity .= "</table>";
            $result = $disDispenseQuantity;
            break;


        case 'diagnosis_search':
            $search_text = isset($_GET["q"]) ? $_GET["q"] : "";
            $treat = new TreatmentQuestions();
            $diseases = $treat->doDiagnosisSearch($search_text);
            if (!empty($diseases)){
                $result = '<table cellspacing="2" cellpadding="2">
                                <tr>
                                    <th>S/NO</th>
                                    <th>CODE</th>
                                    <th>NAME</th>
                                    <th>&nbsp;</th>
                                </tr>';
                $s_no = 0;
                foreach ($diseases as $d){
                    $code = $d['diagnosis_code'];
                    $name = stripslashes($d['diagnosis_name']);
                    $value = "$name [$code]";
                    $row_id = 'diagnosis-search-result-row-' . $d['diagnosis_id'];
                    $result .= '<tr id="' . $row_id . '">
                                    <td>' . (++$s_no) . '.</td>
                                    <td>' . $code . '</td>
                                    <td>' . $name . '</td>
                                    <td><input type="button" class="btn" value="Select" onclick="doDiagnosisSelect(\'' . $row_id . '\', \'' . $value . '\', '. $d['diagnosis_id'] . ')" /></td>
                                </tr>';
                }
                $result .= '<table>';
            } else {
                $result = 'No matching result found.';
            }
            break;

        case "new-checkbox-entry":
            $question = isset($_POST['entry']) ? $_POST['entry'] : '';
            $treatment_field = isset($_POST['field']) ? (int)$_POST['field'] : '';
            $clinic = isset($_POST['clinic']) ? (int)$_POST['clinic'] : '';
            $treat = new TreatmentQuestions();
            $question_id = $treat->addTreatmentQuestion($question, $treatment_field, $clinic);
            $result = $question_id;
            break;

        case "new-checkbox-entry-update":
            $question_id = isset($_POST['id']) ? $_POST['id'] : '';
            $question = isset($_POST['entry']) ? $_POST['entry'] : '';
            $treatment_field = isset($_POST['field']) ? (int)$_POST['field'] : '';
            $clinic = isset($_POST['clinic']) ? (int)$_POST['clinic'] : '';
            $treat = new TreatmentQuestions();
            $status = $treat->updateTreatmentQuestion($question_id, $question, $treatment_field, $clinic);
            $result = $status;
            break;


        case "new-checkbox-entry-delete":
            $question_id = isset($_POST['id']) ? $_POST['id'] : '';
            $clinic = isset($_POST['clinic']) ? (int)$_POST['clinic'] : '';
            $treat = new TreatmentQuestions();
            $status = $treat->deleteTreatmentQuestion($question_id);
            $result = $status;
            break;


        case "get-consultants-in-clinic":
            $clinic_id = isset($_GET['id']) ? $_GET['id'] : '';
            $user = new admin_user();
            $result = $user->getClinicConsultants4DD($clinic_id);
            break;




/*END Invoice Module stuffs*/


/*BEGIN: Form Builder Thinz*/
        case "formbuilder-sort":
            $order = isset($_GET['order']) ? $_GET['order'] : '';
            $fb = new FormBuilder();
            $ordering_result = $fb->saveNewOrder($order);
//            file_put_contents('./debug/formbuilder-sort-post.txt', $order);
            $result = $ordering_result ? 1 : 0;
            break;
        case "formbuilder-get-preview":
            $clinic = isset($_GET['clinic']) ? (int)$_GET['clinic'] : 0;
            $page = isset($_GET['page']) ? (int)$_GET['page'] : 0;
            $fb = new FormBuilder();
            $form_preview = $fb->getForm($clinic, $page);
            $result = $form_preview;
            break;
/*END: Form Builder Thinz*/

	}   //END switch
	//header ("Content-type: text/html");
	echo $result;