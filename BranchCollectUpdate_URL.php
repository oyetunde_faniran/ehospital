<?php
require_once ('./includes/config.inc.php');
$transact_no = isset($_REQUEST['transact_no']) ? $_REQUEST['transact_no'] : ""; //transaction no
$rcd = isset($_REQUEST['rcd']) ? $_REQUEST['rcd'] : 0; //response code
$final_rcd = isset($_REQUEST['final_rcd']) ? $_REQUEST['final_rcd'] : 0; //final response code (for successful transactions returns- 100)
$message = isset($_REQUEST['message']) ? $_REQUEST['message'] : ""; //response code description
$final_message = isset($_REQUEST['final_message']) ? $_REQUEST['final_message'] : ""; // final response code description (for successful transactions returns- Payment Successfully Made)
$pay_ref = isset($_REQUEST['pay_ref']) ? $_REQUEST['pay_ref'] : ""; //payment reference no
$bank_name = isset($_REQUEST['bank_name']) ? $_REQUEST['bank_name'] : ""; // bank name where payment was made or is to be made
$bctoken = isset($_REQUEST['bctoken']) ? $_REQUEST['bctoken'] : ""; // use for transaction validation
$bc_url = isset($_REQUEST['bc_url']) ? $_REQUEST['bc_url'] : ""; // BranchCollect redirect url
?>
<!-- Checks the validity of transaction -->
<?php 
//require ("validityoftransaction.php"); // this file will be given to you by BranchCollect alongside a folder named scopbin. please do not rename it ?>

<!-- connects to your database -->

<?php //require ("examsandrecords/config.php"); // this is your database connection file, you can manipulate ?>

<?php
$Dbconnect=new DBConf();
$invoice2 = new invoice();

if ($final_rcd == "100") {
//update your payment database here
$med_id=$invoice2->getPatadm_id($transact_no);
$invoicetype=$invoice2->getInvoicetype($transact_no);
$payment_method = "BranchCollect";
$trans_status = "1";
$update="UPDATE  pat_transtotal  SET  pattotal_receiptdate=DATE_ADD( NOW(), INTERVAL 0 hour), pattotal_status='$trans_status', pattotal_paymthd='$payment_method', pattotal_date=DATE_ADD( NOW(), INTERVAL 0 hour)
	WHERE pattotal_transno='$transact_no'";
$results = $Dbconnect->execute($update);
$updated = $Dbconnect->hasRows( $results, 1);

 if($updated && $invoicetype=='1'){
			  
			   $sql = " UPDATE patinp_account SET 
		   		
		   	    pattrans_no='$transact_no'
				
				WHERE patadm_id='$med_id'";
				 $res = $Dbconnect->execute($sql);
}

//echo "Trying Checks... ";
if($updated && $invoicetype=='4'){
     $payment_array = $invoice2->getTestId_4paidItem_array($transact_no);
     include_once ('./library/lab/checks.php');
     $check=new checks();
     $check->sendLabPayment($payment_array);
}// else die ("NOT LAB RELATED!!!");


//if update was successful, do this
$client_resp = 1; //Successful
header("Refresh: 0; URL=$bc_url?transact_no=$transact_no&rcd=$rcd&final_rcd=$final_rcd&client_resp=$client_resp");
exit;
}
?>