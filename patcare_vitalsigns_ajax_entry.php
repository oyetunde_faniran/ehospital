<?php
	require_once ('./includes/config.inc.php');
	$vitalSigns = new patVitalSigns();
	$_POST["userID"] = $_SESSION[session_id() . "userID"];
	$_POST["ed"] = isset($_POST["ed"]) ? $_POST["ed"] : 0;
    $clinic_id = isset($_GET['cid'])? (int)$_GET['cid'] : 0;
	//die ("<pre>" . print_r ($_POST, true) . "</pre>");
	if ($_POST["ed"] == "1") {
		$saved = $vitalSigns->editVitalSigns($_POST);
	} else {
        $saved = $vitalSigns->saveVitalSigns($_POST, $clinic_id);
    }
    echo ($saved ? $saved : 0);