function doajax(){
if (window.XMLHttpRequest)
  {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
if (window.ActiveXObject)
  {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
return null;
}

var ajaxObject=doajax();

function loadDest(){
    if(ajaxObject!=null){
        var url;
        var c;
        var d;
        c=document.getElementById('transferSource');
        d=c.value;
        url = "index.php?p=inventory_ajax&m=pharmacy";
        url = url + '&sid=' + Math.random();
        url = url + '&source=' + d;
        ajaxObject.onreadystatechange=sortDestinations;
        ajaxObject.open('GET',url,true);
        ajaxObject.send(null);
    }
}

function sortDestinations(){
    if (ajaxObject.readyState == 4) {
        if (ajaxObject.status == 200) {
document.getElementById('destLocationDiv').innerHTML=ajaxObject.responseText;
    }
        } else {document.getElementById('destLocationDiv').innerHTML="<img src='images/_loader.gif'>";
}
    }

currentItemID = 0;

function showItemStockBal(item,batch){
     if(ajaxObject!=null){
        var url;
        currentItemID = 'splitQties'+item;
        url = "index.php?p=inventory_ajax&m=pharmacy";
        url = url + '&sid=' + Math.random();
        url = url + '&item_id=' + item;
        url = url + '&batch_id=' + batch;
        ajaxObject.onreadystatechange=showQuantities;
        ajaxObject.open('GET',url,true);
        ajaxObject.send(null);
    }
}

function showQuantities(){
    if(ajaxObject.readyState==4){
        if(ajaxObject.status==200){
            document.getElementById(currentItemID).innerHTML=ajaxObject.responseText;
            }
        }
    }
    
function confirmDest(){
 var selected;
//    selected=document.transferForm.destLocation.selectedIndex;
    selected=document.getElementById('destLocation').value;
//        if(document.transferForm.destLocation.options[selected].value=="0"){
        if(selected=="0"){
            alert('Select destination');
            return false;
        }else{
            return true;
        }
    }

function confirmTransferWithdrawal(url){
    var confirmed;
    confirmed=confirm("Cancel and withdraw this transfer?");
    if(confirmed){
        window.location=url;
        }
    }