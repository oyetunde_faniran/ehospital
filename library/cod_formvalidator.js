/**************************************************************************************************************************

CoDezyner Solutions							***BEGIN CoD's FORM VALIDATION CODE***
http://www.codezyner.com

USAGE:
1.	Populate array "skippable" in function canSkip, with the name of fields that are not compulsory.
2.	Carry out any other custom validation in function "extraValidation", which takes a reference to the form as a parameter.
3.	Call function "validateMe" (which takes a reference to the form as a parameter) to validate the form.

****************************************************************************************************************************/
var curForm = "";

function isEmpty(s){
	s = String (s);
	s = s.toLowerCase();
	if (s.search(/^[a-z]/) == -1)
		return true;
	else return false;
}

function canSkip(s){
	var skippable = Array();
	if (curForm == "menuaddform")
		skippable = ["module"];
	else if (curForm == "groupaddform")
			skippable = ["groupdesc"];
	x = skippable.join(",");
	if (x.indexOf(s) == -1)
		return false;
	else return true;
}

function isEmail(s){
	if (s.search(/^[0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\.[a-z]{2,4}$/) == -1)
		return false;
	else return true;
}

function isPhone(s){
	if (isNaN(s) || s.length < 7)
		return false;
	else return true;
}

function isEmailField(s){
	if (s.indexOf("email") == -1)
		return false;
	else return true;
}

function isPhoneField(s){
	if (s.indexOf("phone") == -1)
		return false;
	else return true;
}

function isEmpty(s){
	if (s.search(/[a-zA-Z]/) != -1){
		return false;
	} else return true;
}

function extraValidation(disForm){
	g = true;

	if(disForm.room){
		if (isEmpty(disForm.room.value)){
			alert ("Please, enter a chat room name that contains at least one alphabet.");
			g = false;
		}
	}

	if(g && disForm.private){
		if (disForm.private.checked && disForm.code.value == ""){
			if (!confirm ("You have specified this room to be a private room, but you did not enter an entry code.\nDo you want to create the room with an empty entry code?"))
				g = false;
		}
	}

	return g;
}

function validateMe(disForm){
	var disName;
	total = disForm.length;
	good2Go = true;
	curForm = disForm.name;
	for (k=0; k<total; k++){
		if (disForm.elements[k]){
			disName = new String (disForm.elements[k].name);
			if (!canSkip(disName)){
				if (isPhoneField(disName)){
					if (!isPhone(disForm.elements[k].value))
						good2Go = false;
				} else if (isEmailField(disName)){
							if (!isEmail(disForm.elements[k].value))
								good2Go = false;
					} else if (isEmpty(disForm.elements[k].value)){
							good2Go = false;
						}
				if (!good2Go){
					if (disForm.elements[k].select) disForm.elements[k].select();
					alert ("Please, fill the field '" + disForm.elements[k].name + "' correctly.");
					break;
				}
			} //END if (canSkip...) 
		}	// END if (element)
	}	//END for

	if (good2Go && extraValidation(disForm))
		alert ("Good 2 go");
		//disForm.submit();
}

/************************************************************
				END CoD's FORM VALIDATION CODE
************************************************************/