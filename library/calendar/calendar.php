<script type="text/javascript" language="javascript" src="jcalendar.js"></script>
<script type="text/javascript" language="javascript">
//<![CDATA[
<?php
	$month = array ("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	$day_of_week = array ("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
?>

var month_names = new Array("<?php echo implode('","', $month); ?>");
var day_names = new Array("<?php echo implode('","', $day_of_week); ?>");
var submit_text = "Go";
//]]>
</script>
<style>
/* Calendar */
body{
	font-family: Tahoma, Arial, Verdana, Helvetica, sans-serif;
}

table.calendar {
	font-size: 12px;
    width:              100%;
}
table.calendar td {
    text-align:         center;
}
table.calendar td a {
    display:            block;
}

table.calendar td a:hover {
    background-color:   #CCFFCC;
}

table.calendar th {
    background-color:   #00BBFF;
}

table.calendar th a{
    color: #990000;
}

table.calendar th a:hover{
    color: #FFFFFF;
}

table.calendar td.selected {
    background-color:   #FFCC99;
}

img.calendar {
    border:             none;
}
form.clock {
    text-align:         center;
}
/* end Calendar */

</style>
<title>Calendar</title>
</head>
<body onLoad="initCalendar();">
<div id="calendar_data"></div>
<div id="clock_data"></div>
</body>
</html>
