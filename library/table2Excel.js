function pushTable2Excel(tableID, formID){
	try{
		table = document.getElementById(tableID);	//Get the table whose cells would be exported
		if (table){	//alternate data transfer format used for this page
			form = document.getElementById(formID);	//Get form for transport init
			tRows = table.rows;						//Get a reference to all the rows in the table
			allRows = tRows.length;					//Get the number of rows in the table
	
			//Traverse all rows
			for (rowCounter = 0; rowCounter < allRows; rowCounter++){
				disRow = tRows[rowCounter];
				allCells = disRow.cells.length;
				inputName = "row" + rowCounter;
				
				//Traverse all the cells in the current row
				for (cellCounter = 0; cellCounter < allCells; cellCounter++){
					disValue = cleanUp(disRow.cells[cellCounter].innerHTML);	//Needed Value
					disInput = "<input type=\"hidden\" name=\"" + inputName + "[]\" value=\"" + disValue + "\" />";	//Create an input tag containing this value to be inserted into the form
					form.innerHTML = form.innerHTML + disInput;		//Insert into the form
				}
			}
	
			//Submit the form if there's data
			if (allRows > 0)
				form.submit();
		} else {
			form = document.getElementById("xpform");
			if (form)
				 form.submit();
			else throw 0;
		}

	} catch (e) {
		alert ("It seems Microsft Excel Format Export is not available for this report.");
		//alert (e);
	}
}



function cleanUp(s){
	str = new String(s);
	str = str.replace(/&/, "&amp;");
	str = str.replace(/</, "&lt;");
	str = str.replace(/>/, "&gt;");
	str = str.replace(/"/, "&quot;");
	str = str.replace(/'/, "&#039;");
	return str;
}