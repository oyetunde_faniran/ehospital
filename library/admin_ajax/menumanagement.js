function processSubs(url, id){
	try{
		img = document.getElementById("img" + id);
		keeper = document.getElementById("ispan" + id);
		s = document.getElementById("span" + id);
		if (img.title == "Collapse"){
			keeper.innerHTML = s.innerHTML;
			s.innerHTML = "";
			img.title = "Expand";
			img.src = "images/expand.jpg";
			document.getElementById("ajaxLoader").innerHTML = "";
		} else {
				document.getElementById("ajaxLoader").innerHTML = "<img src='images/loader.gif' /> LOADING...";
				img.title = "Collapse";
				img.src = "images/collapse.jpg";
				if (keeper.innerHTML != ""){
					s.innerHTML = keeper.innerHTML;
					keeper.innerHTML = "";
				}
				url = url + "&it=" + img.title;
				img.title = "Collapse";
				img.src = "images/collapse.jpg";
				ajaxMachine = initAjax();
				if (ajaxMachine){
					ajaxMachine.open("GET", url, true);
					ajaxMachine.onreadystatechange = processDoc;
					ajaxMachine.setRequestHeader("Content-Type", "text/html");
					ajaxMachine.send("");
				}
			}
	} catch (e) {
			document.getElementById("ajaxLoader").innerHTML = "";
		}
}



function processDoc(){
	if (ajaxMachine.readyState == 4 && ajaxMachine.status == 200) {
//		var xmlDoc = ajaxMachine.responseXML;
		var txtDoc = ajaxMachine.responseText;
		if (txtDoc){
			try {
				current = "";
				counter = 0;
				response = new String(txtDoc);

				while (!isNaN(response.charAt(counter))){
					current = current + response.charAt(counter);
					counter++;
				}
				dFinal = response.substr(counter);

				cont = document.getElementById("span" + current);
				cont.innerHTML = dFinal;
				document.getElementById("ajaxLoader").innerHTML = "";
			} catch (e) {
					document.getElementById("ajaxLoader").innerHTML = "";
					if (confirm ("Unable to carry out your request. You may have been logged out due to a period of inactivity.\n\nPlease, click OK to refresh the page and try again."))
						window.location = window.location;
				}
		} else document.getElementById("ajaxLoader").innerHTML = "";
	}
}



function loadParents(d){
	try{
		d = d.value;
		ajaxMachine = initAjax();
		document.getElementById ("parentmenu").innerHTML = "<option value=''></option>";
		url = "index.php?p=ajax&m=sysadmin&t=menu4modules&d=" + d;
		if (ajaxMachine){
			ajaxMachine.open("GET", url, true);
			ajaxMachine.onreadystatechange = processMenu4Modules;
			ajaxMachine.setRequestHeader("Content-Type", "text/html");
			ajaxMachine.send("");
		}
	} catch (e) {}
}


function processMenu4Modules(){
	if (ajaxMachine.readyState == 4 && ajaxMachine.status == 200) {
		var txtDoc = ajaxMachine.responseText;

		if (txtDoc){
			try{
				document.getElementById("parentmenu").innerHTML = txtDoc;
			} catch (e) {
					document.getElementById("ajaxLoader").innerHTML = "";
					if (confirm ("Unable to carry out your request. You may have been logged out due to a period of inactivity.\n\nPlease, click OK to refresh the page and try again."))
						window.location = window.location;
				}
		}
	}
}	//END function processMenu4Modules


function processMe(url, d, c, t){
	try{
		c = document.getElementById(c);
		p = c.parentNode;
		p.removeChild(c);
		p.innerHTML = t;
		ajaxMachine = initAjax();
		if (ajaxMachine){
			ajaxMachine.open("GET", url, true);
			ajaxMachine.onreadystatechange = processRightsChange;
			ajaxMachine.setRequestHeader("Content-Type", "text/html");
			ajaxMachine.send("");
		}
	} catch (e) {}
}


function processRightsChange(){
	if (ajaxMachine.readyState == 4 && ajaxMachine.status == 200) {
		var txtDoc = ajaxMachine.responseText;

		if (txtDoc){
			try {
				current = "";
				counter = 0;
				response = new String(txtDoc);

				while (!isNaN(response.charAt(counter))){
					current = current + response.charAt(counter);
					counter++;
				}

				dFinal = response.substr(counter);
				cont = document.getElementById("tr" + current);
				cont.innerHTML = dFinal;
			} catch (e) {
					document.getElementById("ajaxLoader").innerHTML = "";
					if (confirm ("Unable to carry out your request. You may have been logged out due to a period of inactivity.\n\nPlease, click OK to refresh the page and try again."))
						window.location = window.location;
				}
		} else document.getElementById("ajaxLoader").innerHTML = "";	
	}
}



function confirmRemove(url, id, c, t){
	if (confirm("Are you sure you want to remove this menu and every sub-menu under it?")){
		img = document.getElementById("img" + id);
		img.title = "Expand";
		img.src = "images/expand.jpg";
		document.getElementById("ispan" + id).innerHTML = "";
		document.getElementById("span" + id).innerHTML = "";
		processMe(url, id, c, t);
	}
}