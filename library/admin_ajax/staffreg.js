function processClinics4Depts(s, planS){
	try{
		selector = document.getElementById(s);	//select tag
		planSelect = document.getElementById(planS);	//plans select tag
		id = selector.options[selector.selectedIndex].value;	//selected element
		if (id > 0){
			loader = document.getElementById("retplan_loader");
			loader.style.display = "inline-block";	//Give user rest of mind
			planSelect.innerHTML = "<option value=''>Please wait...</option>";
			//url = "index.php?p=ajax&m=sysadmin&t=deptclinics&d=" + id; alert (url);
			url = "staffreg_ajax.php?d=" + id;// alert (url);
			ajaxMachine = initAjax();
			if (ajaxMachine){
				ajaxMachine.open("GET", url, true);
				ajaxMachine.onreadystatechange = processClinics;
				ajaxMachine.setRequestHeader("Content-Type", "text/html");
				ajaxMachine.send("");
			}
		} else planSelect.innerHTML = "<option value=''>---Select Clinic---</option>";

	} catch (e) {
//			alert (e);
			document.getElementById("retplan_loader").style.display = "none";
		}
}



function processClinics(){
	if (ajaxMachine.readyState == 4 && ajaxMachine.status == 200) {
//		var xmlDoc = ajaxMachine.responseXML;
		var txtDoc = ajaxMachine.responseText;
		selector = document.getElementById("staffclinic");
		if (txtDoc){
			//Hide the loading text
			document.getElementById("retplan_loader").style.display = "none";
			
			//Show the koko
			selector.innerHTML = "<option selected='selected' value=''>---Select Clinic---</option>";
			selector.innerHTML = selector.innerHTML + txtDoc;
		} else {
			selector.innerHTML = "<option selected='selected' value=''>---Select Clinic---</option>";
			document.getElementById("retplan_loader").style.display = "none";
		}
	}
}