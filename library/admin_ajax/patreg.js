/*BEGIN LOAD RETAINER PLANS ON THE REGISTRATION PAGE*/

function processPlans4Retainers(s, planS){
	try{
		selector = document.getElementById(s);	//select tag
		planSelect = document.getElementById(planS);	//plans select tag
		id = selector.options[selector.selectedIndex].value;	//selected element
		if (id > 0){
			loader = document.getElementById("retplan_loader");
			loader.style.display = "inline-block";	//Give user rest of mind
			planSelect.innerHTML = "<option value=''>Please wait...</option>";
			url = "index.php?p=ajax&m=sysadmin&t=retainerplans&d=" + id;
			ajaxMachine = initAjax();
			if (ajaxMachine){
				ajaxMachine.open("GET", url, true);
				ajaxMachine.onreadystatechange = processPlans;
				ajaxMachine.setRequestHeader("Content-Type", "text/html");
				ajaxMachine.send("");
			}
		} else planSelect.innerHTML = "<option value=''>Select Plan</option>";

	} catch (e) {
			alert (e);
			document.getElementById("retplan_loader").style.display = "none";
		}
}


function processPlans(){
	if (ajaxMachine.readyState == 4 && ajaxMachine.status == 200) {
//		var xmlDoc = ajaxMachine.responseXML;
		var txtDoc = ajaxMachine.responseText;
		if (txtDoc){
			//Hide the loading text
			document.getElementById("retplan_loader").style.display = "none";
			
			//Show the koko
			selector = document.getElementById("retplans");
			selector.innerHTML = "<option selected='selected' value=''>Select Plan</option>";
			selector.innerHTML = selector.innerHTML + txtDoc;
		}
	}
}

/*END LOAD RETAINER PLANS ON THE REGISTRATION PAGE*/