function initAjax() {
	if (window.XMLHttpRequest){ // branch for native XMLHttpRequest object
		try {
			ajaxMachine = new XMLHttpRequest();
		} catch(e) {
			ajaxMachine = null;
		}
	} else if (window.ActiveXObject){ // branch for IE/Windows ActiveX version
			try {
				ajaxMachine = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				try {
					ajaxMachine = new ActiveXObject("Microsoft.XMLHTTP");
				} catch(e) {
					ajaxMachine = null;
				}
			}
	} //END else if
	return ajaxMachine;
} //END function