<?php
class checks{
public $request;
public $conn;
public $insertid;
public $hospitalno;
public $testname;
public $returned_lab_no;
public $query;
public $userrecordsmodifiable=false;

public $bad_reg_hospital_no=false;
public $bad_lab_pin=false;
public $fatalerror=false;
public $bad_parameters=false;
public $maindbname="mediluth_skye";
public $bad_transid=false;
//public $currentUser=$_SESSION[session_id() . "userID"];
public $currentUser;


public function __construct($doVarRequest=TRUE){
	$this->currentUser = $_SESSION[session_id() . "userID"];
	if($doVarRequest){$this->getvars();}
}

public function dbCleanup($string){
try{
	if (get_magic_quotes_gpc()){
		return trim($string);}
	else {return addslashes(trim($string));}
	}
catch (Exception $e) {
			set_error_handler (array("Error_handler", "my_error_handler"));
        }
}

public function removeslashes($arrayvar){
try{
	foreach($arrayvar as $var => $val){
	$var=stripslashes($val);
	return $arrayvar;}
}
catch (Exception $e) {
			set_error_handler (array("Error_handler", "my_error_handler"));
        }

}

public function getvars(){
try{
foreach($_REQUEST as $var => $val){
	$$var=$this->dbCleanup(trim($val));}
	}

catch (Exception $e) {
			set_error_handler (array("Error_handler", "my_error_handler"));
        }
}



public function sendLabRequest($lab_test_id="", //required
							   $lab_pin="", //optional when hospital no is supplied
							   $reg_hospital_no="", //optional when lab_pin is supplied
							   $staff_employee_id="", //required
							   $comments="", //optional
							   $analytes_array=array(), //optional but recommended
							   $other_specimen="", //optional
							   $specimen_array=array() //optional but recommended
							   ){
try{
	$this->conn=new DBConf();
	$this->conn->labConnect();
//data Validation
	if(func_num_args()<7 || func_num_args()>8 || (empty($reg_hospital_no) && empty($lab_pin) )){$this->bad_parameters=1; $this->fatalerror=1;}

//check validity of reg_hospital no and lab_pin from their respective databases
	if($reg_hospital_no!=""){
			$this->hospitalno=$reg_hospital_no;
			$this->request=$this->conn->execute("SELECT reg_hospital_no FROM mediluth_skye.registry where reg_hospital_no='$reg_hospital_no'");
			if (mysql_num_rows($this->request)<1){$this->bad_reg_hospital_no=1; $this->fatalerror=1;}
	}

	if($lab_pin!=""){
			$this->request=$this->conn->execute("SELECT lab_pin FROM lab_registry where lab_pin='$lab_pin'");
			if (mysql_num_rows($this->request)<1){$this->bad_lab_pin=1; $this->fatalerror=1;}
	}

//check if doctor is requesting for an unregistered lab patient i.e no Lab PIN, then register the user silently
if ($reg_hospital_no!="" && !$this->fatalerror){
	//die ("I'm here");
	$this->request=$this->conn->execute("SELECT lab_pin FROM lab_registry where reg_hospital_no='$reg_hospital_no'");
	if (mysql_num_rows($this->request)<1){//unregistered in the lab yet, so do register
		$this->request=$this->conn->execute("INSERT INTO lab_registry (reg_hospital_no, lab_date, staff_employee_id) VALUES ('$reg_hospital_no', curdate(), {$this->currentUser})");
	}
}// else die ("Not in there");


if (!$this->fatalerror){
//if reg_hospital_no is empty, try to fetch from lab_registry, if lab_pin is empty, do same
	if(empty($reg_hospital_no) && $lab_pin!=""){
			$this->request=$this->conn->execute("SELECT reg_hospital_no FROM lab_registry where lab_pin='$lab_pin'");
			if (mysql_num_rows($this->request)>0){$reg_hospital_no=mysql_result($this->request,0);}
			$this->hospitalno=$reg_hospital_no;
	}

//	same as above for lab_pin
	if(empty($lab_pin) && $reg_hospital_no!=""){
			$this->request=$this->conn->execute("SELECT lab_pin FROM lab_registry where reg_hospital_no='$reg_hospital_no'");
			if (mysql_num_rows($this->request)>0){$lab_pin=mysql_result($this->request,0);}
	}

//MAIN TEST REQUEST DB UPDATE
	$this->query="INSERT into lab_trans (lab_pin, reg_hospital_no, staff_employee_id, comments, request_date) VALUES ('$lab_pin', '$reg_hospital_no', '$staff_employee_id', '$comments', NOW())";
	$this->request=$this->conn->execute($this->query);
	$this->insertid = mysql_insert_id($this->conn->getConnectionID()); //get trans id and use to build lab no for the test

//	Generate lab no using the algorithm, then update last insert's lab no with the generated one
	 $testquery="SELECT lang1 as lab_test_name FROM {$this->maindbname}.language_content as ml, mediluth_lab.lab_test where lab_test_id={$lab_test_id} AND ml.langcont_id=lab_test.langcont_id;";
     $testsearchres=$this->conn->execute($testquery);
	 $thetest=trim(mysql_result($testsearchres,0,'lab_test_name'));
	$datepart=date('dmy');
	$testtitlepart=strtoupper(substr($thetest,0,1));
	$insertidpart=$this->insertid;
	$this->returned_lab_no="$testtitlepart/$datepart/$insertidpart";
	$this->conn->execute("UPDATE lab_trans set lab_no='{$this->returned_lab_no}' WHERE lab_trans_id='{$this->insertid}'");


	if(is_array($analytes_array) && !empty($analytes_array)){//update test_results with analytes for this test
		foreach ($analytes_array as $var => $val){
			$this->conn->execute("INSERT INTO lab_results (lab_trans_id, analyte_id) VALUES ('{$this->insertid}', '$val')");
		}
	}//update test_results with analytes for this test


	if(is_array($specimen_array) && !empty($specimen_array)){//update lab_trans_specimen with specimen for this test
		foreach ($specimen_array as $var => $val){
			$this->conn->execute("INSERT INTO lab_trans_specimen(specimen_id, lab_trans_id) VALUES ('$val','{$this->insertid}')");
		}
	}//update test_results with specimens for this test
	if(!empty($other_specimen)){//also update other_specimen in the same table in case that was specified in the form
		$this->conn->execute("INSERT INTO lab_trans_specimen(other, lab_trans_id) VALUES ('$other_specimen','{$this->insertid}')");
	}

	//return test info to doctor's request for saving
	$dsquery="select DISTINCT analytes.lab_test_id, lab_test.dept_id, lab_test.service_id, lang1 as lab_test_name from analytes, lab_results, lab_test, mediluth_skye.language_content l, lab_dept where l.langcont_id=lab_test.langcont_id AND lab_trans_id={$this->insertid} AND lab_results.analyte_id=analytes.analyte_id AND lab_test.lab_test_id=analytes.lab_test_id";
    $testquery=mysql_query($dsquery);
	$this->testname = mysql_result($testsearchres, 0, 'lab_test_name');
	$service_id = mysql_result($testquery, 0, 'service_id');
	$pid = isset($_GET["pid"]) ? (int)$_GET["pid"] : 0;		//patadm_id of the patient
	$cid = isset($_GET["cid"]) ? (int)$_GET["cid"] : 0;		//clinic id where the patient is currently receiving treatment
	$ltReq = new LabTestRequest();
	$ltReq->saveRequest($this->insertid, $this->hospitalno, $pid, $cid, $this->testname, $service_id);


}//end if no fatal error check
}//end try

catch (Exception $e) {
			die ("<pre>" . print_r ($e, true) . "</pre>");
			set_error_handler (array("Error_handler", "my_error_handler"));
        }
}//end sendLabRequest



public function retrieveLabResult($trans_id){//............................................................Retrieve Results
try{
	$this->conn=new DBConf();
	$this->conn->labConnect();
//data Validation
	if(empty($trans_id)){$this->bad_parameters=1; $this->fatalerror=1;}
//check if trans_id is valid
	$transidcheck=mysql_query("SELECT * FROM lab_trans WHERE lab_trans_id='$trans_id'");
	if(!$transidcheck OR mysql_num_rows($transidcheck)!=1){$this->bad_transid=1; $this->fatalerror=1;}

	if(!$this->fatalerror){//Start Test Result Info Retrieval
			$userquery="select lab_trans.lab_trans_id as the_lab_trans_id, lab_trans.lab_pin as thelabpin, lab_no, comments, staff_employee_id, DATE_FORMAT(request_date,'%D %b %Y') as thedate, DATE_FORMAT(request_date,'%l:%i %p') as thetime, status, lab_trans.reg_hospital_no as thehospitalno
				FROM lab_trans
				WHERE lab_trans.lab_trans_id='{$_REQUEST['transid']}'";
			$searchres=$labdb->execute($userquery);
			$details=mysql_fetch_array($searchres);
	}
}//end try

catch (Exception $e) {
			set_error_handler (array("Error_handler", "my_error_handler"));
        }
}//end Retrieve Result



public function sendLabPayment($payment_array){//......................................Perform lab test payment update
try{
	$this->conn=new DBConf();
	$this->conn->labConnect();
//data Validation
	if(!is_array($payment_array) || empty($payment_array)){$this->bad_parameters=1; $this->fatalerror=1;}

//check if trans_id is valid then update its record in DB
foreach($payment_array as $var=>$val){
	  if(!is_int($val)){$this->bad_transid=1; $this->fatalerror=1; continue;} //particular element causes an error if not INTEGER
	  $transidcheck=mysql_query("SELECT * FROM lab_trans WHERE lab_trans_id='$val'");
	  if(mysql_num_rows($transidcheck)<1){$this->bad_transid=1; $this->fatalerror=1; continue;} //particular element causes an error if not in DB

	//Start Test Result payment update
	if(!$this->fatalerror){
			$updateQ="UPDATE lab_trans SET paid=1 WHERE lab_trans_id=$val";
			$searchres=$this->conn->execute($updateQ);
	}
}//end foreach
if(!$this->fatalerror){return true;} else{return false;}
}//end try

catch (Exception $e) {
			set_error_handler (array("Error_handler", "my_error_handler"));
        }
}//end test payment update


}//end class
?>