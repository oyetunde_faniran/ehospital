function registerCheck(){
	if(document.getElementById('surname').value=="" && document.getElementById('othernames').value=="" && document.getElementById('reg_hospital_no').value==""){
		alert('Please fill in identification details for either luth or non-luth patients');
		return false;
		}
	document.getElementById('checked').value="true";
	return true;
}

function searchCheck(){
	if(document.getElementById('search_lab_pin').value==""){
		alert('Search field cannot be empty!');
		return false;
		}
	document.getElementById('checked').value=true;
	return true;
}

function confirmDeleteUser(){
	if(confirm("Are you sure? Lab User will be deleted!")){return true;} else {return false;}
	}
	
function testCheck(){
	if(document.getElementById('lab_test_id').value=="0"){
		alert('Please select a test to proceed!');
		return false;
		}
	document.getElementById('getanalytesform').submit();
	return true;
}

function searchTestCheck(){
	if(document.getElementById('search_lab_test_text').value==""){
		alert('Please fill in keywords to search!');
		return false;
		}
	document.getElementById('checked').value="true";
	return true;
}

function checkResult(){
	if(confirm("Please confirm all result entries. Test STATUS will be changed! Are you sure you want to record this result?")){return true;} else {return false;}
	}