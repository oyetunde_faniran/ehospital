<?php
/*
 * Array
(
    [prices] => Array
        (
            [124] => Array
                (
                    [1] => 0
                    [6] => 0
                    [3] => 0
                    [4] => 2
                    [5] => 0
                )

            [127] => Array
                (
                    [1] => 0
                    [7] => 3
                    [5] => 0
                )

        )

    [hosp_id] => 0000003
    [p] => invoicedrug
    [invoicetype] => 2
    [patadm_id] => 28
    [doctorpresciption] => 1. PARACETAMOL TABLET 
	500mg 7/7
2. Aspirin 
	200mg 2/12
 
    [hosp] => aspiri
    [select30] => Array
        (
            [0] => 124
            [1] => 127
        )

    [invoicedrugs] => Open Invoice
    [sButton] => Save Invoice
)
 */
    //echo ("<pre>" . print_r ($_POST, true) . "</pre>");
    require_once ('./includes/config.inc.php');
    $conn = new DBConf();
    $_POST = admin_Tools::doEscape($_POST, $conn);
    $totalAmount = 0;
    $patAdmID = isset($_POST["patadm_id"]) ? (int)$_POST["patadm_id"] : 0;
    $hospitalNo = isset($_POST["hosp_id"]) ? $_POST["hosp_id"] : 0;
    $invoiceType = isset($_POST["invoicetype"]) ? $_POST["invoicetype"] : 0;
    $tries = 0;
    $done = false;

    
    
    //Get the current department / clinic of the patient
    $query = "SELECT * FROM appointment
                WHERE patadm_id = '$patAdmID'
                ORDER BY app_id DESC
                LIMIT 0,1";
    $result = $conn->run($query);
    if ($conn->hasRows($result)){
        $row = mysql_fetch_assoc($result);
        $deptID = $row["dept_id"];
        $clinicID = $row["clinic_id"];
    } else {
        $deptID = 0;
        $clinicID = 0;
    }

    
    
    $conn->startTransaction();
    $msg = "";
    try {
        //Create the transaction instance. Try to generate a unique transaction ID in 10 tries max.
        do {
            $transNo = admin_Tools::getRandNumber(rand (10, 20));
            $query = "INSERT INTO pat_transtotal
                        SET pattotal_transno = '$transNo',
                            pattotal_date = NOW(),
                            pattotal_totalamt = '$totalAmount',
                            pattotal_totalamt_nhis = '$totalAmount',
                            pattotal_status = '0',
                            patadm_id = '$patAdmID',
                            reg_hospital_no = '$hospitalNo',
                            dept_id = '$deptID',
                            clinic_id = '$clinicID',
                            user_id = '" . $_SESSION[session_id() . "userID"] . "',
                            pattotal_invoice_type = '$invoiceType'";
            $result = $conn->run($query);
            if ($conn->hasRows($result)){
                $done = true;
            }
            $tries++;
        } while (!$done && $tries < 10);
        
        if(!$done){
            throw new Exception();
        }
        
        $transTotalID = mysql_insert_id($conn->getConnectionID());
        
        //Save the doctor's prescription
        $query = "INSERT INTO pat_transtotal_extra
                    SET pattotal_id = '$transTotalID',
                        pattotalextra_doctorprescription = '" . $_POST["doctorpresciption"] . "'";
        $result = $conn->run($query);
        
        
        //Insert each individual item
        $drugIDs = isset($_POST["select30"]) ? $_POST["select30"] : array();
        $totalAmount = 0;
        $itemObj = new inventory_item_class();
        $unitObj = new inventory_units();
        $manObj = new inventory_manufacturer_class();
        //die ("Now here 456");
        foreach ($drugIDs as $drugID){
            $drugID =(int)$drugID;
            $drugUnitPrices = isset($_POST["prices"][$drugID]) ? $_POST["prices"][$drugID] : array();
            
            //Get the service ID for this item from pat_serviceitem table
//            $query = "SELECT patservice_id FROM pat_serviceitem
//                        WHERE patservice_itemid = '$drugID'
//                            AND patservice_type = '0'";
//            $result = $conn->run($query);
//            if ($conn->hasRows($result)){
//                $serviceInfo = mysql_fetch_assoc($result);
//                $serviceID = $serviceInfo["patservice_id"];
//            } else $serviceID = 0;
            
            foreach ($drugUnitPrices as $unitID => $quantity){
                if (!empty($quantity)){
                    //Get the price for this unit
                    $query = "SELECT * FROM inventory_unit_items
                                WHERE inventory_unit_id = '$unitID'
                                    AND inventory_items_id = '$drugID'";
                    $result = $conn->run($query);
                    if ($conn->hasRows($result)){
                        $row = mysql_fetch_assoc($result);
                        $disPrice = $row["inventory_units_items_price"];
                        $disItemUnitID = $row["inventory_unit_items_id"];
                        $totalAmount += $disPrice * $quantity;
                    }
                    //die ("After Here");

                    
                    
                    //Insert into the table saving the names of transaction items
                    $disDrugInfo = $itemObj->getDrug($drugID);
                    $unitInfo = $unitObj->getUnit($unitID);
                    $unitName = $unitInfo["inventory_unit_name"];
                    $disManufacturer = $manObj->getManufacturer($disDrugInfo["drug_manufacturer"]);   //Get info about the manufacturer of this drug
                    $disManufacturerName = is_array($disManufacturer) ? $disManufacturer["inventory_manufacturer_name"] : "";
                    $serviceName = $disDrugInfo["drug_name"] . " / " . $unitName . " / " . $disDrugInfo["drug_strength"] . " / " . $disManufacturerName;
                    $query = "INSERT INTO pat_serviceitem
                                SET patservice_type = '0',
                                    patservice_itemid = '$drugID',
                                    patservice_name = '$serviceName'";
                    $result = $conn->run($query);
                    if (!$conn->hasRows($result)){
                        throw new Exception();
                    }
                    $serviceItemID = mysql_insert_id($conn->getConnectionID());
                    
                    //Insert into the transaction details table
                    $query = "INSERT INTO pat_transitem
                                SET pattotal_id = '$transTotalID',
                                    patitem_amount = '$disPrice',
                                    patitem_amount_nhis = '$disPrice',
                                    patservice_id = '$serviceItemID',
                                    patitem_totalqty = '$quantity'";
                    $result = $conn->run($query);
                    //die ("Before Here");
                    if (!$conn->hasRows($result)){
                        throw new Exception();
                    }
                    
                    //Get the insert ID
                    $disTransItemID = mysql_insert_id($conn->getConnectionID());
                    
                    //Now, insert extra info
                    $query = "INSERT INTO pat_transitem_extra
                                SET pattrans_id = '$disTransItemID',
                                    drug_id = '$drugID',
                                    inventory_unit_items_id = '$disItemUnitID'";
                    $result = $conn->run($query);
                    if (!$conn->hasRows($result)){
                        throw new Exception();
                    }
                    
                }
            }   //END foreach drug unit / quantity
            $query = "";
        }   //END foreach drug
        
        
        if (TRANSACTION_COST > 0){
            $query = "INSERT INTO pat_transitem
                        SET pattotal_id = '$transTotalID',
                            patitem_amount = '" . TRANSACTION_COST . "',
                            patitem_amount_nhis = '" . TRANSACTION_COST . "',
                            patservice_id = '" . TRANSACTION_COST_SERVICE_ID . "',
                            patitem_totalqty = '1'";
            $result = $conn->run($query);
            $totalAmount = $totalAmount + TRANSACTION_COST;
        }
        
        
        //Update the trans total table with the total amount
        $query = "UPDATE pat_transtotal
                    SET pattotal_totalamt = '$totalAmount',
                        pattotal_totalamt_nhis = '$totalAmount'
                    WHERE pattotal_id = '$transTotalID'";
        $result = $conn->run($query);
        
        
        //Set this prescription status to "used"
        $query = "UPDATE casenote_prescription
                    SET payflag = '1'
                    WHERE casenote_id = (
                                            SELECT MAX(cp.casenote_id) FROM casenote cp WHERE cp.patadm_id = '$patAdmID'
                                        )";
        $result = $conn->run($query);
        
        
        
        
        
        $conn->commitTransaction();
    } catch (Exception $e) {
        //die ("'<pre>$query</pre>' failed");
        $conn->rollBackTransaction();
    }
    
    $url = "index.php?p=viewinvoice4drug&c=Success&hosp_id=$hospitalNo&pattotal_id=$transTotalID";
    header("Location: $url");
    //die ("Transaction Complete! ... $url");
?>