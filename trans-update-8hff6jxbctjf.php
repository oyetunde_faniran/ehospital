<?php
//file_put_contents('debug-payment-update-page.txt', print_r ($_POST, TRUE));
//die ("<pre>" . print_r ($_POST, true) . "</pre>");
require_once ('./includes/config.inc.php');
$transact_no = isset($_POST['btrans_no']) ? $_POST['btrans_no'] : ""; //isset($_REQUEST['transact_no']) ? $_REQUEST['transact_no'] : ""; //transaction no
$rcd = isset($_REQUEST['rcd']) ? $_REQUEST['rcd'] : 0; //response code
$final_rcd = ($_POST["paymentmode"] == 1 || $_POST["paymentmode"] == 2 || $_POST["paymentmode"] == 3) ? 100 : 0; //isset($_REQUEST['final_rcd']) ? $_REQUEST['final_rcd'] : 0;//isset($_REQUEST['final_rcd']) ? $_REQUEST['final_rcd'] : 0; //final response code (for successful transactions returns- 100)
$message = isset($_REQUEST['message']) ? $_REQUEST['message'] : ""; //response code description
$final_message = isset($_REQUEST['final_message']) ? $_REQUEST['final_message'] : ""; // final response code description (for successful transactions returns- Payment Successfully Made)
$pay_ref = isset($_REQUEST['pay_ref']) ? $_REQUEST['pay_ref'] : ""; //payment reference no
$bank_name = isset($_REQUEST['bank_name']) ? $_REQUEST['bank_name'] : ""; // bank name where payment was made or is to be made
$bctoken = isset($_REQUEST['bctoken']) ? $_REQUEST['bctoken'] : ""; // use for transaction validation
$bc_url = isset($_REQUEST['bc_url']) ? $_REQUEST['bc_url'] : ""; // BranchCollect redirect url
//die ($transact_no);

$Dbconnect = new DBConf();
$invoice2 = new invoice();

//die ("<pre>" . print_r ($_POST, true) . "</pre>");

if ($final_rcd == "100") {
    //update your payment database here
    $med_id = $invoice2->getPatadm_id($transact_no);
    $invoicetype = $invoice2->getInvoicetype($transact_no);
    $pos_tid = isset($_REQUEST['pos_trans_id']) ? $_REQUEST['pos_trans_id'] : "";

    //$payment_method = "Cash";
    //$payment_method = $_POST["paymentmode"] == 1 ? "Cash" : "POS";
    switch ($_POST["paymentmode"]){
        case 3:
            $payment_method = 'Mobile Money';
            $pos_tid = '';
            break;
        case 2:
            $payment_method = 'POS';
            break;
        case 1:
        default:
            $payment_method = 'Cash';
            $pos_tid = '';
            break;
    }
    
    
    $trans_status = "1";
    $update = "UPDATE  pat_transtotal
                SET pattotal_receiptdate = DATE_ADD( NOW(), INTERVAL 0 hour),
                    pattotal_status = '$trans_status',
                    pattotal_paymthd = '$payment_method',
                    #pattotal_date = DATE_ADD( NOW(), INTERVAL 0 hour),
                    user_id = '" . $_SESSION[session_id() . "userID"] . "',
                    patinv_id = '" . admin_Tools::doEscape($pos_tid, $Dbconnect) . "'
				WHERE pattotal_transno = '$transact_no'";


    $results = $Dbconnect->execute($update, TRUE);
    $paymentUpdated = $Dbconnect->hasRows($results, 1);



    //***BEGIN: BranchCollect Mini DB Updating
	if ($paymentUpdated){
        $paymentGood = true;
		
		//***Do this to save the payment details in Branch Collect Mini's DB
		
		//Get the params to use
		/*
		 *                              - amount
         *                              - transID
         *                              - custNo
         *                              - othernames
         *                              - lastname
         *                              - staffName
		*/
		$query = "SELECT * FROM pat_transtotal pt INNER JOIN registry reg
					USING (reg_hospital_no)
					WHERE pattotal_transno = '$transact_no'";
		$result = $Dbconnect->execute($query);
		if ($Dbconnect->hasRows($results, 1)){
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			$transDetails = array(
								  "amount" => $row["pattotal_totalamt"],
								  "transID" => $transact_no,
								  "custNo" => $row["reg_hospital_no"],
								  "othernames" => $row["reg_othernames"],
								  "lastname" => $row["reg_surname"],
								  "staffName" => $_SESSION[session_id() . "staffName"]
							);
			//die ($_SESSION[session_id() . "userID"] . "--<pre>" . print_r ($transDetails, true) . "</pre>");
			$payTracker = new Payment($_SESSION[session_id() . "userID"]);
			$payTracker->processPayment($transDetails);
            $daysAmount = $payTracker->getTotalDayPayment();
		}
	} else {
        $paymentGood = false;
        $daysAmount = 0;
    }   //END if .. else
    //***END: BranchCollect Mini DB Updating




    if ($paymentUpdated && $invoicetype == '1') {

        $sql = " UPDATE patinp_account SET
		   		
		   	    pattrans_no='$transact_no'
				
				WHERE patadm_id='$med_id'";
        $res = $Dbconnect->execute($sql);
        
    }

    if ($paymentUpdated && $invoicetype == '4') {
        $payment_array = $invoice2->getTestId_4paidItem_array($transact_no);
        include_once ('./library/lab/checks.php');
        $check = new checks();
        $check->sendLabPayment($payment_array);
    }

//if update was successful, do this
    $client_resp = 1; //Successful
    //header("Refresh: 0; URL=$bc_url?transact_no=$transact_no&rcd=$rcd&final_rcd=$final_rcd&client_resp=$client_resp");
    //exit();
    $message = $paymentGood ? $daysAmount : "--1--0--0--1--";
    die ($message);
}
?>