<?php
class Clinic_New {
	protected $conn;
	protected $clinicID;
	
	
	function __construct ($clinicID = 0){
		$this->clinicID = (int)$clinicID;
		$this->conn = new DBConf();
	}
	
	
	
	function getAllClinics4DD($selected = 0){
		$query = "SELECT c.clinic_id, lc.lang1 'clinicname' 
					FROM clinic c INNER JOIN language_content lc INNER JOIN department d
					ON c.langcont_id = lc.langcont_id AND c.dept_id = d.dept_id
					WHERE d.dept_isclinical = '1'
					ORDER BY clinicname";
		$result = $this->conn->execute ($query);
		$retVal = "";
		if ($this->conn->hasRows($query)){
			while ($row = mysql_fetch_array ($result, MYSQL_ASSOC)){
				if ($row["clinic_id"] == $selected)
					$retVal = "<option value=\"{$row['clinic_id']}\" selected=\"selected\">" . stripslashes ($row["clinicname"]) . "</option>";
				else $retVal = "<option value=\"{$row['clinic_id']}\">" . stripslashes ($row["clinicname"]) . "</option>";
			}	//END while
		}	//END if
		return $retVal;
	}
	
	
}	//END class
?>