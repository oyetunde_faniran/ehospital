<?php
    //Include the main config file
    ini_set('display_errors', 'off');
//    ini_set('max_execution_time', '120');
    $root_dir = dirname(__FILE__);
    require_once ($root_dir . './includes/config.inc.php');
    
    
    
    
    //The function that parses downloaded XML
    function doDataInsert($xml_file, $pdf_file){
        $xml = file_get_contents($xml_file);
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $obj = simplexml_load_string($xml, NULL, LIBXML_NOCDATA);


        $treat =
        $app_date =
        $hist =
        $treatment =
        $lab = 
        $pres = array();

        foreach ($obj->EDWPFormExtractInstance->EDWPFormInstance->EDWPPageInstance->EDWPFieldInstance as $field_info){
            switch ($field_info->FieldInstanceFieldName){
                case 'Investigation':
                        $lab['investigation'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Dosage_2':
                        $pres['desc']['Dosage_2'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Prescription_3':
                        $pres['desc']['Prescription_3'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Prescription_1':
                        $pres['desc']['Prescription_1'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Differential_Diagnosis':
                        $treat['diffdiagnosis'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'General_Physical_Examination':
                        $treat['genexam'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Review_Systems':
                        $hist['sysreview'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Family_Social_History':
                        $hist['sochist'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'History_Presenting_Complaint':
                        $hist['complainthist'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Appointment_Date_YYYY':
                        $app_date['year'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Appointment_Date_DD':
                        $app_date['day'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'PT_Hospital_Number':
                        $pat_no = (string)$field_info->FieldInstanceFinalResult;
                        $pat_no = !empty($pat_no) ? $pat_no : '0000001';
                        break;
                case 'Appointment_Date_MM':
                        $app_date['month'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Presenting_Complaint':
                        $hist['complaint'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Past_Medical_History':
                        $hist['medhist'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Drug_History':
                        $hist['drughist'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Others':
                        $hist['others'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Systemic_Examiniation':
                        $treat['sysexam'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Diagnosis':
                        $treat['diagnosis'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Prescription_2':
                        $pres['desc']['Prescription_2'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Dosage_1':
                        $pres['desc']['Dosage_1'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
                case 'Dosage_3':
                        $pres['desc']['Dosage_3'] = (string)$field_info->FieldInstanceFinalResult;
                        break;
            }   //END switch

        }   //END foreach


        //Get the properties of the appointment for this patient
        $app_date_full = $app_date['year'] . '-' . $app_date['month'] . '-' . $app_date['day'];
        $db = new DBConf();
        $query = "SELECT a.*
                    FROM appointment a INNER JOIN patient_admission p
                    ON a.patadm_id = p.patadm_id
                    WHERE p.reg_hospital_no = '$pat_no'
                        AND a.app_starttime LIKE '$app_date_full%' # CONCAT(CURDATE(), '%')
                        AND a.app_status = '0'";
        $result = $db->execute($query);
        if ($db->hasRows($result)){
            $row = mysql_fetch_assoc($result);
            $treat['patadm_id'] = $row['patadm_id'];
            $treat['app_id'] = $row['app_id'];
            $treat['user_id'] = 13;
        } else return 0; //die ('Update Failed because no matching appointment was found.');

        //Format prescriptions
        $prescription = '';
        if (!empty($pres['desc']['Prescription_1'])){
            $prescription .= $pres['desc']['Prescription_1'] . ' [' . $pres['desc']['Dosage_1'] . "]\n";
        }
        if (!empty($pres['desc']['Prescription_2'])){
            $prescription .= $pres['desc']['Prescription_2'] . ' [' . $pres['desc']['Dosage_2'] . "]\n";
        }
        if (!empty($pres['desc']['Prescription_3'])){
            $prescription .= $pres['desc']['Prescription_3'] . ' [' . $pres['desc']['Dosage_3'] . "]";
        }
        $pres['desc'] = $prescription;

        //Put into $_POST
        $_POST['treat'] = $treat;
        $_POST['hist'] = $hist;
        $_POST['pres'] = $pres;
        $_POST['lab'] = $lab;
        $_POST['treatment']['treatment'] = '';

        //Save the files permanently & delete the temporary ones
        $xml_file_final = FTP_TREATMENT_FILES_DIR . $treat['app_id'] . '.xml';
        $pdf_file_final = FTP_TREATMENT_FILES_DIR . $treat['app_id'] . '.pdf';
        copy($xml_file, $xml_file_final);
        copy($pdf_file, $pdf_file_final);
        unlink ($xml_file);
        unlink ($pdf_file);

        
        //Now save the extracted XML data
        $patDataUploader = new patTreatmentSheetUpload();
        $patDataUploader->doPatTreatment(TRUE);
    }   //END doDataInsert()





    
    
    //Connect to the FTP server to download XML & PDF files to be saved
    try {
        $host = 'ftp.expedata.net';
        $username = 'MedisoftRX';
        $password = 'MedMed#1';
        $ftp = ftp_connect($host);
        $logged_in = ftp_login($ftp, $username, $password);

        if (!$logged_in){
            throw new Exception('Login Failed');
        }
        
        ftp_pasv($ftp, true);
        $cur_dir = ftp_pwd($ftp) . '/Treatment_Sheet/';
        ftp_chdir($ftp, $cur_dir);
        $dir_list_raw = ftp_rawlist($ftp, $cur_dir);
        
        $dir_array = array();
        $year = date('Y');
        $month = date('M');
        $day = date('d');
        foreach ($dir_list_raw as $dir_props){
            //Deal only with directories
            if (substr($dir_props, 0, 1) == 'd'){
                
                //REmove all repetitive spaces
                $length = strlen($dir_props);
                $position = 1;
                $prev_char = $new_dir_props = substr($dir_props, 0, 1);
                while ($position < $length){
                    $char = substr($dir_props, $position++, 1);
                    if ($char == ' ' && $prev_char == ' '){
                        continue;
                    }
                    $new_dir_props .= $prev_char = $char;
                }   //END while
                
                //Select only the ones modified today
                $final_dir_props = explode(' ', $new_dir_props);
                if ($final_dir_props[5] == $month
                        && $final_dir_props[6] == $day
                        && ($final_dir_props[7] == $year || strpos($final_dir_props[7], ':'))
                        && $final_dir_props[8] != '.'
                        && $final_dir_props[8] != '..'){
                    $dir_array[] = $final_dir_props[8];
                }
            }   //END if it's a directory
        }   //END foreach
        

        foreach ($dir_array as $dir){
            $dis_dir_list = ftp_nlist($ftp, "$cur_dir" . $dir);
            $final_dir = end($dis_dir_list);
            $dis_file_list = ftp_nlist($ftp, "$cur_dir" . $dir . "/$final_dir");
            $xml_file = $pdf_file = '';
            foreach ($dis_file_list as $filex){
                $remote_file = $cur_dir . $dir . "/$final_dir/$filex";
                $ext = substr($filex, -3);
                
                //Download the file
//                $dis_name = admin_Tools::getRandNumber(rand(10, 20));
                $dis_name = rand(10000, 200000000);
                switch($ext){
                    case 'xml':
                            $xml_file = "./tempexport/{$dis_name}.xml";
                            $dis_file = $xml_file;
                            break;
                    case 'pdf':
                            $pdf_file = "./tempexport/{$dis_name}.pdf";
                            $dis_file = $pdf_file;
                            break;
                }   //END switch
                $local_file = fopen($dis_file, 'w');
                $file_downloaded = ftp_fget($ftp, $local_file, $remote_file, FTP_BINARY);
                file_put_contents('./debug/files-downloaded.txt', $remote_file . "\n\n\n", FILE_APPEND);
                if(!$file_downloaded){
                   throw new Exception ("Download of $ext file failed!!!");
                }
                fclose($local_file);
            }   //END foreach file
            //die ('<pre>' . print_r ($dis_file_list, TRUE));
//            die ('Done!!!');
            //Now, extract the data in the retrieved files and save
            doDataInsert($xml_file, $pdf_file);
            
            
            
        }   //END foreach directory
        
        ftp_close($ftp);
    } catch (Exception $e) {
        //echo $e->getMessage;
    }
?>
