<?php # Script 2.4 - index.php

	/*
	 *	This is the main page.
	 *	This page includes the configuration file,
	 *	the templates, and any content-specific modules.
	 */

	// Require the configuration file before any PHP code:
    //die (utf8_decode ("<script>"));
    $root_dir = dirname(__FILE__);
	require_once ('./includes/config.inc.php');
//die ("<pre>" . print_r ($_SESSION, true) . "</pre>");
	//admin_rights::removeChildrenRights(1);
	//exit();
	// Validate what page to show:
	if (isset($_GET['p'])) {
		$p = $_GET['p'];
	} elseif (isset($_POST['p'])) { // Forms
		$p = $_POST['p'];
	} else {
		$p = NULL;
	}

	if (isset($_GET['m'])) {
		$m = $_GET['m'];
	} elseif (isset($_POST['m'])) { // Modules
		$m = $_POST['m'];
	} else {
		$m = NULL;
	}


	 /*$payment_array = array (20000, 21122);
	 include ('./library/lab/checks.php');
     echo ("Included... ");
     $check=new checks();
     echo (" Instantiated...");
     $check->sendLabPayment($payment_array);
	 die (" Done sending...");*/


//    $invUnit = new inventory_units();
//    //$unitID = 5;
//    $itemID = 122;
//    $quantity = 6000;
//    $allUnitsRep = $invUnit->showInAllUnits($quantity, $itemID);
//    die ("<pre>" . print_r ($allUnitsRep, true) . "</pre>");
//
//    $newQty = $invUnit->convertToLowestUnit($unitID, $itemID, $quantity);
//    die ("--> $newQty Tablets");



	// Determine what page to display:
if ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"]){

	include_once ("./includes/switch.php");

} else {	//END / ELSE if logged in
		//Staff Registration page
		if ($disPage == "index.php" && stristr($_SERVER['QUERY_STRING'], "p=staffregister&m=sysadmin")){
			if ($_POST){
				if (file_exists("./modules/$m/staffregister_post.php"))
					include_once ("./modules/$m/staffregister_post.php");
			}
			$page = $m.'/staffregister.php';
			$page_title = $staffreg_title;// die ("$staffreg_title");
		} else {
			//index page
			if ($_POST)
				include_once ("./modules/main.inc_post.php");
			$page = 'main.inc.php';
			$page_title = $home_pageTitle;
		}
	}

	// Make sure the file exists:
	if (!file_exists('./modules/' . $page)) {
		$page = 'main.inc.php';
		$page_title = 'Welcome';
	}

	include_once ("./includes/header.php");

?>




<body>
<?php
	//Display user information if logged in
	if ($_SESSION[session_id() . "luth_loggedin"]){
        $chatInitLink = !$_SESSION[session_id() . "luth_idle"] ? "<span>[ <a href=\"#\" id=\"show-those-online-for-chat\">Who's Online?</a> ]</span>" : "";
		$userDetails = "<div class=\"width user-strip\">
							<div class=\"login-details\">
                                $chatInitLink
                                <p>$home_loggedinuser:</p><span>" . $_SESSION[session_id() . "staffName"] . "</span>";
		if (!empty($_SESSION[session_id() . "deptName"]))
			$userDetails .= "<p>$home_dept:</p><span>" . $_SESSION[session_id() . "deptName"] . "</span>";
		elseif (!empty($_SESSION[session_id() . "bank"]))
				$userDetails .= "<p>$home_bank:</p><span>" . $_SESSION[session_id() . "bank"] . "</span>";
		$userDetails .= "<p>$home_usergroup:</p><span>" . $_SESSION[session_id() . "groupName"] . "</span>
							</div>
						</div>";
		echo $userDetails;
	}
	$headerID = $_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"] ? "header-logged-in" : "header";
?>
<div id="<?php echo $headerID; ?>" class="width"></div>
<!-- BEGIN Top Navigation Bar-->
<!--<div id="nav-bar" class="width">
<?php
	//include_once ("includes/links.php");
?>
</div>-->
<?php
	if ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"]){
		echo $_SESSION[session_id() . "menu"];
	}
?>
<!-- END Top Navigation Bar-->



<!-- BEGIN Div for Main Body Content-->
<div class="content width">

<!-- BEGIN Main Body-->
  <div class="content-column">
        <span class="contentColumnSpan <?php echo ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"] && !empty($subLinks)) ? "" : " splash-width";	//Echo style rule that would stretch the width of this column if there are no sub-links?>">
<?php
    //If the current module is pharmacy, then show the currrent location
    //die ("'$m'");
    if ($m == "pharmacy" && !empty($inventory_locationName)){
        $page_title .= " <em>[Location: <a href=\"index.php?p=inventory-select-location&m=pharmacy\" title=\"Click to change\">$inventory_locationName</a>]</em> ";
    }
    echo $page_title;
?>
        </span>

        <div class="splash-box <?php echo ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"] && !empty($subLinks)) ? "" : " splash-width";	//Echo style rule that would stretch the width of this column if there are no sub-links?>">
<?php
	  /*$trav = new Pat_Service_Traverser(1, "Unit", "registration", "Servicename");
	  $serviceID = $trav->getServiceID();
	  $serviceName = $trav->getServiceName(18);
	  $price = $trav->getServicePrice(44);
	  echo ("<p>SERVICE ID: $serviceID</p>
			<p>SERVICE NAME: $serviceName</p>
			<p>PRICE: $price</p>");*/
	include_once ("./modules/" . $page);
?>
        </div>
  </div>
<!-- END Main Body-->


<!-- BEGIN Related Links / Sub-Links-->
<?php
	include_once ("./includes/sublinks.php");
?>
<!-- END Related Links / Sub-Links-->


</div>
<!-- END Div for Main Body Content-->



<!-- BEGIN Footer-->
<?php
	include_once ("./includes/footer.php");
    if (!in_array($p, $jquery191_nochat_array)){
        include_once ("./includes/chat-kd748dyah.php");
    }
?>
<!-- END Footer-->
</body>
</html>
