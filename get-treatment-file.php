<?php
    $root_dir = dirname(__FILE__);
    require_once ($root_dir . './includes/config.inc.php');
    $app_id = isset($_GET['i']) ? (int)base64_decode($_GET['i']) : 0;
    $dis_file = FTP_TREATMENT_FILES_DIR . "{$app_id}.pdf";
    if (file_exists($dis_file)){
        $file_contents = file_get_contents($dis_file);
        header ('Content-Type: application/pdf');
        echo $file_contents;
    }
?>
