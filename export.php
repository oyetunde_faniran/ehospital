<?php
	include_once ("includes/config.inc.php");
	include_once ("classes/report_PDFExcel.php");

/*	if ($_POST){
		echo "<pre>" . print_r ($_POST, true) . "</pre>";
		exit();
	}*/

	//Get the export type
	$action = !empty($_GET["action"]) ? $_GET["action"] : "";

/*	$p = !empty($_GET['p']) ? $_GET['p'] : "";
	$mainArray = !empty($_SERVER[session_id() . "_$p"]) ? $_SERVER[session_id() . "_$p"] : "";// die ("<pre>" . print_r ($_SERVER[session_id() . "_$p"], true) . "</pre>");
	$title = !empty($mainArray["title"]) ? $mainArray["title"] : "[UNKNOWN TITLE]";
	$others = !empty($mainArray["others"]) ? $mainArray["others"] : array();
	$chart = !empty($mainArray["chart"]) ? $mainArray["chart"] : array();*/


	//Confirm that all required variables are available and init them
	if ($action != "print"){
		if (isset($_POST["xp"])) {
			//First option used in monthlystats_inpatsummary.php && monthlystats_deptact.php (arrays formed on server side)
			$mainArray = unserialize(base64_decode($_POST["xp"]));
			if (isset($mainArray["title"], $mainArray["others"], $mainArray["others"]["contents"], $mainArray["others"]["options"])){
				$title = !empty($mainArray["title"]) ? $mainArray["title"] : $report_unknownTitle;
				$others = !empty($mainArray["others"]) ? $mainArray["others"] : array();
				if (isset($others["contents"], $others["options"])){
					$contents = $others["contents"];
					$options = $others["options"];
				} else die ($report_exportNotAvailable);
			} else die ($report_exportNotAvailable);
		} elseif (isset($_POST["t"], $_POST["v"], $_POST["h"], $_POST["row0"])) {	//Second Option (Array generated with javascript from the table used for display)
				$title = strip_tags($_POST["t"]);	//Report Title
				$v = strip_tags($_POST["v"]);		//"Make Column Headers bold" option
				$h = strip_tags($_POST["h"]);		//"Make Row Headers bold" option
				$options = array (
								"rowHeader" => $h,
								"colHeader" => $v,
							);
				//Clean-up and organise report contents into a multi-dimensional array
				$contents = array();
				$rowCounter = 0;
				do{
					$disRow = cleanUp($_POST["row" . $rowCounter]);
					array_push($contents, $disRow);
					$rowCounter++;
				} while (isset($_POST["row" . $rowCounter]));
		} else die ($report_exportNotAvailable);
	}	//END if action != print



	switch ($action){
		case "excel":	//Necessary for the PHP2Excel Classes to function call one another successfully
						set_include_path("library/phpexcel");
						include 'PHPExcel.php';
						include 'PHPExcel/IOFactory.php';
						$exporter = new report_PDFExcel($title, $contents, $options);
						$exportFile = "tempexport/" . substr(md5(microtime()), rand(0,5), rand(16, 26)) . ".xls";
						$type = "2003";	//The version of the final Excel file (2003 / 2007)
						$done = $exporter->doExcelExport($type, $exportFile);
						
						//Make the file available for download if the export was successful
						if ($done){
							$title = str_replace(" ", "_", $title);
							$title = str_replace(",", "_", $title);
							header ("Content-type: application/vnd.ms-excel");
							header ("Content-Disposition: attachment; filename=\"" . strip_tags($title) . ".xls\"");
							$fileContents = file_get_contents($exportFile);
							unlink ($exportFile);
							echo $fileContents;
						} else die ($report_failedExport);
						break;
						
		/*case "pdf":		set_include_path("library/phpexcel");
						include 'PHPExcel.php';
						include 'PHPExcel/IOFactory.php';
						$exporter = new report_PDFExcel($title, $others["contents"], $others["options"]);
						$exportFile = "tempexport/" . substr(md5(time()), rand(0,5), rand(16, 26)) . ".pdf";
						$type = "2003";	//The version of the final Excel file (2003 / 2007)
						$pdfDone = $exporter->doPDFExport($type, $exportFile);
						if ($pdfDone){
							header ("Content-type: application/pdf");
							header ("Content-Disposition: attachment; filename=\"" . strip_tags($title) . ".pdf\"");
							$fileContents = file_get_contents($exportFile);
							unlink ($exportFile);
							echo $fileContents;
						} else die ("An error occurred while trying to complete the export. Please, contact the support group of this application.");
						break;
		case "chart":	if (array_key_exists("available", $chart) && $chart["available"]){
							if (array_key_exists("available", $chart));
						} else echo "<p>Sorry! Charts are not available for the chosen report.</p>
										<p><a href=\"" . $_SERVER['HTTP_REFERER'] . "\">&laquo; Go Back</a></p>";
						break;*/
		case "print":
/*		default:	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
							<html xmlns=\"http://www.w3.org/1999/xhtml\">
							<head>
							<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
							<title>Reports - Print View</title>
							<style>
								body	{ font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 12px; }
								table	{ border-collapse: collapse; border-color:#CCCCCC; }
								td 		{ padding: 3px; }
							</style>
							</head>
							
							<body>
								<h3>$title</h3>
								$print
								<p><img style=\"border: 0px;\" src=\"images/export_print.jpg\" /> <a href=\"javascript: window.print();\">Print Now</a></p>
								<p><a href=\"" . $_SERVER['HTTP_REFERER'] . "\">&laquo; Go Back</a></p>
							</body>
							</html>";*/
		default:	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
							<html xmlns=\"http://www.w3.org/1999/xhtml\">
							<head>
							<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
							<title>Reports - Print View</title>

							<link href=\"css/luth.css\" rel=\"stylesheet\" type=\"text/css\" />
							</head>
							<body class=\"print-preview\">
								<div id=\"container\"></div>
								<script language=\"javascript\" type=\"text/javascript\">
									try{
										document.getElementById(\"container\").innerHTML = opener.document.getElementById(\"resultContainer\").innerHTML + '<div>&nbsp;</div><p><img style=\"border: 0px;\" src=\"images/export_print.jpg\" /> <a href=\"javascript: window.print();\">Print Now</a></p>';
									} catch (e) {
										document.getElementById(\"container\").innerHTML = \"$report_unknownReport\";
									}
								</script>
							</body>
							</html>";
	}	//END switch


	function cleanUp($subject){
		if (is_array($subject)){
			foreach ($subject as $key=>$value){
				if (is_array($value))
					$subject[$key] = cleanUp($value);
				else $subject[$key] = strip_tags($value);
				if ($subject[$key] == "&nbsp;")
					$subject[$key] = "";
			}
			$retVal = $subject;
		} else $retVal = strip_tags((string)$subject);
		return $retVal;
	}

?>