<?php # Script 2.4 - index.php

	/*
	 *	This is the main page.
	 *	This page includes the configuration file,
	 *	the templates, and any content-specific modules.
	 */

	// Require the configuration file before any PHP code:
	require_once ('./includes/config.inc.php');
	//admin_rights::removeChildrenRights(1);
	//exit();
	$myUserid = $_SESSION[session_id() . "userID"];
	$myDeptid = $_SESSION[session_id() . "deptID"];
	$myClinicid = $_SESSION[session_id() . "clinicID"];
	$myStaffid = $_SESSION[session_id() . "staffID"];
	$myConsultantid = $_SESSION[session_id() . "consultantID"];
	$myStaffname = $_SESSION[session_id() . "staffName"];

	// Validate what page to show:
	if (isset($_GET['m'])) {
		$m = $_GET['m'];
	} elseif (isset($_POST['m'])) { // Modules
		$m = $_POST['m'];
	} else {
		$m = NULL;
	}
if (isset($_GET['a'])) {
		$aa = $_GET['a'];
	}else{
	$aa = "";
	}
	if (isset($_GET['b'])) {
		$bb = $_GET['b'];
	}else{
	$bb = "";
	} 
	if (isset($_GET['c'])) {
		$cc = $_GET['c'];
	}else{
	$cc = "";
	}
	if (isset($_GET['d'])) {
		$dd = $_GET['d'];
	}else{
	$dd = "";
	} 
	if (isset($_GET['q'])) {
		$qq = $_GET['q'];
	}else{
	$qq = "";
	} 
	$select = new patSelectOptions;
 	$dept="";
	$conn = new DBConf();
	$query = "SELECT dept_id from clinic WHERE clinic_id='$dd'";
 	$res = $conn->execute($query);
	while($row = mysql_fetch_assoc($res)){
	$dept = $row['dept_id'];	
	};			


switch ($qq) {
	case 'transfer01':
		$vv='transfer02';
		$trans_remark = "";
		$m = "patient_care";
	
			$page = $m.'/pattransfer_form_inc.php';
			$page_title = 'Transfer Patient';	
	break;
	
	case 'transfer02':
		$m = "patient_care";
			$page_title = 'Transfer Patient';	
			$page = $m.'/pattransfer_page_inc.php';
		$vv='transfer03';
		$trans = new patTransfer;
		$treat = new patTreatment;
		
		$trans->trans  = $_POST["trans"];
		
		$trans_id = $trans->checkTransfer($trans->trans['patadm_id'],$trans->trans['consultant_id']);
		if($trans_id === FALSE){
		echo $error_msg_5;
		exit;
		}else{
		if($trans_id == 0){
		$trans_id = $trans->insertTransfer();
		}
		else
			echo "<p>".$future_appointment_label."</p>";
		}
	break;
	
	case 'transfer03':
		$m = "patient_care";
			$page_title = 'Transfer Patient';	
			$page = $m.'/pattransfer_form_inc.php';
		$vv='transfer04';
		$trans = new patTransfer;
		$gg = $_GET['g'];
		$trans_details = $trans->viewTransfer($gg);
		if($trans_details === FALSE){
				echo $error_msg_5;
				exit;
		}else{
		extract($trans_details);
		$dept = $to_dept;
		$dd = $to_clinic;
		}
	
	break;
	
	case 'transfer04':
		$m = "patient_care";
		$vv='transfer02';
			$page_title = 'Transfer Patient';	
			$page = $m.'/pattransfer_page_inc.php';
		$trans = new patTransfer;
		$trans->trans  = $_POST["trans"];
		$trans_id = $_POST['trans_id'];
		$update = $trans->updateTransfer($trans_id);
		
		if($update===FALSE) {
		echo $error_msg_5;
		exit;
		}
	break;

	case 'app01':
		$m = "patient_care";
		$vv='app02';
			$page_title = 'Schedule Appoinment';	
			$page = $m.'/patappointment_form_inc.php';
		$appstart = '08:00';
		$append = '09:00';
		$appdate = date("Y-m-") . str_pad((date("d")+1), 2, "0", STR_PAD_LEFT);
		$apptype = "2";
	
	break;
	
	case 'app02': //die ("<pre>" . print_r ($_POST, true) . "</pre>");
		$m = "patient_care";
		$page_title = 'Schedule Appoinment';	
		$page = $m.'/patappointment_page_inc.php';
		$vv='app03';
		$appoint = new patAppointment();
		$treat = new patTreatment();
		
		$appoint->appoint  = $_POST["appoint"];
		
		$app_id = $appoint->checkAppointment($appoint->appoint['patadm_id'], '', $appoint->appoint['consultant_id']);
		if ($app_id === FALSE){
			echo $error_msg_5;
			exit();
		} else {
			if($app_id == 0) {
				$app_id = $appoint->insertAppointment();
				
				//The last arg here (true) would make the method not to set the appointment status to attended
				$treat->updateAppointmentStatus($app_id, $appoint->appoint['patadm_id'], true);
			} else echo "<p>" . $future_appointment_label . "</p>";
		}
		//die ("Appointment Created Successfully");
		break;
	
	case 'app03':
		$m = "patient_care";
		$vv='app04';
			$page_title = 'Schedule Appoinment';	
			$page = $m.'/patappointment_form_inc.php';
		$appoint = new patAppointment;
		$gg = $_GET['g'];
		$app_detalis = $appoint->viewAppointment($gg);
		if($app_detalis === FALSE){
				echo $error_msg_5;
				exit;
		}else{
		extract($app_detalis);
		$appstart = $app_start;
		$append = $app_end;
		$appdate = $app_date;
		$apptype = $app_type;
		}
	
	break;
	
	case 'app04':
		$m = "patient_care";
		$vv='app02';
			$page_title = 'Schedule Appoinment';	
			$page = $m.'/patappointment_page_inc.php';
		$appoint = new patAppointment;
		$appoint->appoint  = $_POST["appoint"];
		$app_id = $_POST['app_id'];
		$update = $appoint->updateAppointment($app_id);
		
		if($update===FALSE){ echo $error_msg_5;
		exit;
		}
	break;

	case 'admit01':
		$m = "patient_care";
		$vv='admit02';
			$page_title = 'Patient Admission';	
			$page = $m.'/patadmission_form_inc.php';
		$ward_id = "";
	break;
	
	case 'admit02':
		$m = "patient_care";
			$page_title = 'Patient Admission';	
			$page = $m.'/patadmission_page_inc.php';
		$vv='admit03';
		$treat = new patTreatment;
		
		$treat->treat  = $_POST["treat"];
		
		$treat_id = $treat->checkAdmission($treat->treat['patadm_id'],$treat->treat['consultant_id']);

		if($treat_id == 0){
		$treat_id = $treat->insertAdmission();
		//$treat->insertConditiontrack($treat->treat['patadm_id'],'3');
		}
		else
			echo "<p>".$future_appointment_label."</p>";
	break;
	
	case 'admit03':
		$m = "patient_care";
		$vv='admit04';
			$page_title = 'Patient Admission';	
			$page = $m.'/patadmission_form_inc.php';
		$treat = new patTreatment;
		$gg = $_GET['g'];
		$treat_details = $treat->viewAdmission($gg, $curLangField);
		if($treat_details === FALSE){
				echo $error_msg_5;
				exit;
		}else{
		extract($treat_details);

		}
	
	break;
	
	case 'admit04':
		$m = "patient_care";
		$vv='admit02';
			$page_title = 'Patient Admission';	
			$page = $m.'/patadmission_page_inc.php';
		$treat = new patTreatment;
		$treat->treat  = $_POST["treat"];
		$treat_id = $_POST['treat_id'];
		$update = $treat->updateAdmission($treat_id);
		
		if($update===FALSE){ echo $error_msg_5;
		exit;
		}
	break;

	case 'treat01':
		$m = "patient_care";
		$page_title = 'Print Treatment Sheet';	
			$page = $m."/patpatientplan.php";
			$casenote_id =  $_GET['e']? $_GET['e'] : 0;
		$page = $m.'/patprinttreatment_inc.php';
		
		$treat = new patTreatment;
	break;

	default:
			include_once ("./modules/main.inc_post.php");
			$page = 'main.inc.php';
			$page_title = $home_pageTitle;
			break;
	
	break;
}



	// Determine what page to display:
if ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"]){

	//include_once ("./includes/switch.php");

} else {	//END / ELSE if logged in
		die ("Please, <a href=\"index.php\">click here to login</a> before visiting this page.");
	}

	// Make sure the file exists:
	if (!file_exists('./modules/' . $page)) {
		$page = 'main.inc.php';
		$page_title = 'Welcome To LUTH Hospital Management System';
	}

	include_once ("./includes/header.php");

?>




<body>
<?php
	//Display user information if the logged in
	if ($_SESSION[session_id() . "luth_loggedin"]){
		$userDetails = "<div class=\"width user-strip\">
							<div class=\"login-details\">
							<p>$home_loggedinuser:</p><span>" . $_SESSION[session_id() . "staffName"] . "</span>";
		if (!empty($_SESSION[session_id() . "deptName"]))
			$userDetails .= "<p>$home_dept:</p><span>" . $_SESSION[session_id() . "deptName"] . "</span>";
		elseif (!empty($_SESSION[session_id() . "bank"]))
				$userDetails .= "<p>$home_bank:</p><span>" . $_SESSION[session_id() . "bank"] . "</span>";
		$userDetails .= "<p>$home_usergroup:</p><span>" . $_SESSION[session_id() . "groupName"] . "</span>
							</div>
						</div>";
		echo $userDetails;
	}
	$headerID = $_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"] ? "header-logged-in" : "header";
?>
<div id="<?php echo $headerID; ?>" class="width"></div>
<!-- BEGIN Top Navigation Bar-->
<!--<div id="nav-bar" class="width">
<?php
	//include_once ("includes/links.php");
?>
</div>-->
<?php
	if ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"]){
		echo $_SESSION[session_id() . "menu"];
	}
?>
<!-- END Top Navigation Bar-->



<!-- BEGIN Div for Main Body Content-->
<div class="content width">

<!-- BEGIN Main Body-->
  <div class="content-column">
        <span class="contentColumnSpan <?php echo ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"] && !empty($subLinks)) ? "" : " splash-width";	//Echo style rule that would stretch the width of this column if there are no sub-links?>"><?php echo $page_title; ?></span>

        <div class="splash-box <?php echo ($_SESSION[session_id() . "luth_loggedin"] && !$_SESSION[session_id() . "luth_idle"] && !empty($subLinks)) ? "" : " splash-width";	//Echo style rule that would stretch the width of this column if there are no sub-links?>">
			<script type="text/javascript" src="./library/doubleSelect.1.2/jquery-1.3.2.min.js"></script>
            <script type="text/javascript" src="./library/doubleSelect.1.2/jquery.doubleSelect.min.js"></script>
            <link rel="stylesheet" type="text/css" href="library/pat_datepicker/src/css/jscal2.css" />
            <link rel="stylesheet" type="text/css" href="library/pat_datepicker/src/css/border-radius.css" />
            <link rel="stylesheet" type="text/css" href="library/pat_datepicker/src/css/win2k/win2k.css" />
            <script type="text/javascript" src="js/patient_tabs.js"></script>
            <script src="library/pat_datepicker/src/js/jscal2.js"></script>
            <script src="library/pat_datepicker/src/js/lang/en.js"></script>
<?php
	include_once ("./modules/" . $page);
?>
        </div>
  </div>
<!-- END Main Body-->


<!-- BEGIN Related Links / Sub-Links-->
<?php
	include_once ("./includes/sublinks.php");
?>
<!-- END Related Links / Sub-Links-->


</div>
<!-- END Div for Main Body Content-->



<!-- BEGIN Footer-->
<?php
	include_once ("./includes/footer.php");
?>
<!-- END Footer-->
</body>
</html>
